classdef file_DAP < file
  % classdef file_DAP < file
  %
  % Class for reading DAP header files and associated data
  %
  % Written By: Damon Hyde
  % Last Edited: Dec 15, 2015
  % Part of the cnlEEG Project
  %
  
  properties
    
  end
  
  methods
    
    function obj = file_DAP(fname,fpath)
    end
    
    function read(obj)
    end
    
    function write(obj,fname,fpath)
    end
    
  end
end

    