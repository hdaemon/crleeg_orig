  % NRRD raw file reader
% -------------------------------------------------------------------------
% Description:
%	 Read raw data in NRRD file.
% 
% Usage: 
% 
% Input:
%   - headerInfo: image metadata & raw image filename
%   - forceTypeCast: 'true' or 'false' (keep original data type)
% 
% Output:
%   - headerInfo: image data in field 'data'
%
% Comments:
% 
% Notes:
%	Returns 3D matrix data at 'headerInfo.data'
%
% -------------------------------------------------------------------------

function read( nrrdData,forceTypeCast)


currentDIR = pwd;
cd(nrrdData.fpath);

if nargin<2, forceTypeCast = false; end

% open NRRD data file
bSuccess = fopen( nrrdData.fname , 'r' );
if( bSuccess < 1 )
    mydisp(['Warning: Failed to open NRRD data file ' nrrdData.fname ]);
    cd(currentDIR);
    return
end

% get file identifier
fid = bSuccess;

% get corresponding matlab type
matlabtype = 'float'; % default type 
nrrdtype = lower(nrrdData.dataType);

if     any(strcmpi(nrrdtype, {'signed char' 'int8' 'in8_t'})) 
    matlabtype = 'schar';   
elseif any(strcmpi(nrrdtype, {'uchar' 'unsigned char' 'uint8' 'uint8_t'}))  
    matlabtype = 'uchar';    
elseif any(strcmpi(nrrdtype, {'short' 'short int' 'signed short' 'signed short int' 'int16' 'int16_t'})) 
    matlabtype = 'int16';
elseif any(strcmpi(nrrdtype, {'ushort' 'unsigned short' 'unsigned short int' 'uint16' 'uint16_t'}))  
    matlabtype = 'uint16';
elseif any(strcmpi(nrrdtype, {'int' 'signed int' 'int32' 'int32_t'}))
    matlabtype = 'int32';
elseif any(strcmpi(nrrdtype, {'uint' 'unsigned int' 'uint32' 'uint32_t'}))  
    matlabtype = 'uint32';
elseif any(strcmpi(nrrdtype, {'longlong' 'long long' 'long long int' 'signed long long' 'signed long long int' 'int64' 'int64_t'}))  
    matlabtype = 'int64';
elseif any(strcmpi(nrrdtype, {'ulonglong' 'unsigned long long' 'unsigned long long int' 'uint64' 'uint64_t'}))  
    matlabtype = 'uint64';    
elseif ( strcmp( nrrdtype, 'float' ) )
    matlabtype = 'float32';    
elseif ( strcmp( nrrdtype, 'double' ) )
    matlabtype = 'double';
end

if forceTypeCast, 
    matlabtype = [matlabtype,'=>',matlabtype];
end

% read raw data
data = fread(fid,prod( nrrdData.sizes ),matlabtype); 

bSuccess = fclose(fid);
if( bSuccess < 0 )
    fprintf('Warning: Failed to close NRRD data file %s!\n', nhdrFileName );
end
      
% reshape the output data
data = reshape( data, nrrdData.sizes );
nrrdData.data = data;

nrrdData.hasData = true;

cd(currentDIR);
return

