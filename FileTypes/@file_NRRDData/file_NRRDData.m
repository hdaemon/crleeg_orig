classdef file_NRRDData < file & matlab.mixin.Copyable
  % classdef file_NRRDData < file & matlab.mixin.Copyable
  %
  % file_NRRDData is a copyable handle type file object for storing
  % information about a raw NRRD datafile.
  %
  % Properties
  %  readOnly : Flag to define the file as read only and prevent writing
  %               Default: FALSE
  %  saveData : Flag to define whether data should be saved when saving
  %               .mat files. If not saved, upon loading the object tried
  %               to reload the data from disk. 
  %               Default: FALSE
  %  sizes    : Size of the data
  %  dataType : Type of data to be read in. Default: 'float'
  %  hasData  : Flag identifying whether object currently has data
  %  data     : Field containing the actual data.  This field will stay
  %               empty until an attempt to access it is made, at which
  %               point the object will attempt to read the data from
  %               disk.
  
  
    
  properties
    readOnly = false;    
    saveData = false;
    sizes = [0 0 0];
    dataType = 'float';
  end;
  
  properties (GetAccess=public, SetAccess=protected)
    hasData = false;
  end
  
  properties (Transient=true)    
    data = '???';
  end
    
  properties (Hidden = true)
    % savedData is only used as a temporary storage place for data if the
    % saveData flag is set to TRUE when saving out a .mat file.
    savedData = [];
  end
  
  methods
    function obj = file_NRRDData(fname,fpath,sizes,dataType,readonly)
      
      obj = obj@file;
      obj.validExts = {'.raw'};
      
      if nargin>0
        % Set some defaults
        if ~exist('fname','var'),    fname = [];       end;
        if ~exist('fpath','var'),    fpath = '';     end;
        if ~exist('readonly','var'), readonly = false;    end;
        if ~exist('dataType','var'), dataType = 'float';  end;
                        
        if ~exist('sizes','var'),
          error('Must define sizes (and preferrably dataType as well) of NRRD data file');
        end;
        
        % Initialize Superclass
        obj.fpath = fpath;
        obj.fname = fname;
        
        % Set class properties
        obj.sizes = sizes;
        obj.dataType = dataType;
        obj.readOnly = readonly;
        
      end;
    end
    
    % Overloaded Disp function for NRRD Data
    function disp(obj)
      disp(['        fname: ' obj.fname]);
      disp(['        fpath: ' obj.fpath]);
      disp(['     dataType: ' obj.dataType]);
      disp(['        sizes: ' num2str(obj.sizes)]);
      disp(['      hasData: ' BoolToString(obj.hasData)]);
      disp(['     saveData: ' BoolToString(obj.saveData)]);
      disp(['     readOnly: ' BoolToString(obj.readOnly)]);
      disp([' existsOnDisk: ' BoolToString(obj.existsOnDisk)]);
    end
      
        
    function out = get.data(obj)      
      if ~obj.hasData
        try
          mydisp(['Reading Data for ' obj.fname ]);
          obj.read;
          obj.hasData = true;
        catch
          mydisp('NOTE: File Does Not Currently Exist');
          keyboard;
          obj.data = [];
        end;
      end
      out = obj.data;
    end
    
    function set.data(obj,val)
      % function set.data(obj,val)
      %
      % Overloaded set function for file_NRRDData.data.  This does a bunch
      % of data checking to make sure that the NRRD isn't getting too
      % screwed up.  
      % 
      % Allows:
      %  1) Clearing of data, ie: obj.data = [];
      %  2) Setting of data to the default value. ie: obj.data = '???';
      %  3) Setting of data, as long as the size of the new value matches
      %        obj.sizes.
      %
      dataSize = size(val);
      if all(dataSize==0)
        mydisp('Clearing data field');
        obj.data = '???';
        obj.hasData = false;
      elseif strcmpi(val,'???');
        mydisp('Setting default data field');
        obj.data = val;
        obj.hasData = false;
      elseif (numel(dataSize)==numel(obj.sizes))&&all(dataSize==obj.sizes)
        obj.data = val;     
        obj.hasData = true;
      else
        error('Attempting to change the size of the NRRD data');
      end;
    end
       
    function purgeData(obj)      
      % function purgeData(obj)      
      %
      % Clear the obj.data field.  This lets the object stay around without
      % retaining all of the data, which can get unwieldy.
      obj.data = [];
      obj.hasData = false;
    end
    
    read(nrrdObj,forceTypeCast);
    write(nrrdObj);
        
    function S = saveobj(obj)
      % function S = saveobj(obj)
      %
      % Method called when saving the object to file.  This just properly
      % structures everything 
      S.fname = obj.fname;
      S.fpath = obj.fpath;
      S.readOnly = obj.readOnly;
      S.saveData = obj.saveData;
      S.sizes = obj.sizes;
      S.dataType = obj.dataType;
      if S.saveData
        S.savedData = obj.data;
      else
        S.savedData = [];
      end
      
    end;
    
    function obj = reload(obj,S)
        obj.fpath = S.fpath;
        obj.fname = S.fname;
        
        obj.readOnly = S.readOnly;
        
        obj.saveData = S.saveData;
        obj.sizes = S.sizes;
        obj.dataType = S.dataType;
        if obj.saveData
            obj.data = S.savedData;
            obj.hasData = true;
        else
            obj.hasData = false;
        end;
    end
    
  end
  
%   methods (Access=private)
%     readData(nrrdObj,forceTypeCast);
%     writeData(nrrdObj);
%   end;
  
  methods (Static=true)
    function obj = loadobj(S)
      
      obj = file_NRRDData;
      obj = reload(obj,S);
     
    end
  end
end
