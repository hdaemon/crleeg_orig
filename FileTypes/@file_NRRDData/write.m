% NRRD raw file writer
% -------------------------------------------------------------------------
% Description:
%	 Write raw data in NRRD file.
% 
% Usage: 
% 
% Input:
%   - headerInfo: image metadata & raw image filename
%   - forceTypeCast: 'true' or 'false' (keep original data type)
% 
% Output:
%   - headerInfo: image data in field 'data'
%
% Comments:
% 
% Notes:
%	Returns 3D matrix data at 'headerInfo.data'
% 
%   Matlab types:
%     logical Logical array of true and false values
%     char Character array
%     int8 8-bit signed integer array
%     uint8 8-bit unsigned integer array
%     int16 16-bit signed integer array
%     uint16 16-bit unsigned integer array
%     int32 32-bit signed integer array
%     uint32 32-bit unsigned integer array
%     int64 64-bit signed integer array
%     uint64 64-bit unsigned integer array
%     single Single-precision floating-point number array
%     double Double-precision floating-point number array
%
% -------------------------------------------------------------------------

function write(nrrdData)
% global matpath
% 
% [pathstr,matFileName,ext] = fileparts( nhdrFileName );
% 
% % append suffix
% if strcmp(pathstr,'')
%     nhdrFileName = [matpath,nhdrFileName,'.raw'];
% else
%     nhdrFileName = [nhdrFileName,'.raw'];    
% end
% open NRRD data file


mydisp(['Writing data for ' nrrdData.fname]);

returnDIR = pwd;
cd(nrrdData.fpath);

if nrrdData.readOnly
  warning(['File ' nrrdData.fname ' is flagged readOnly. Skipping write']);
  return;
end;

bSuccess = fopen( nrrdData.fname , 'w+' );
if( bSuccess < 1 )
  fid_test = fopen(nrrdData.fname,'r');
  if (fid_test==-1)
    fprintf('Warning: Failed to open NRRD data file %s!\n', nrrdData.fname );
    return
  else
    warning(['File ' nrrdData.fname ' exists but appears to be read-only.' ...
              ' Skipping file write.']);
    return;
  end
end

% get file identifier
fid = bSuccess;
% get corresponding matlab type
matlabtype = class(nrrdData.data); % default type 
nrrdtype = lower(nrrdData.dataType);

if     any(strcmpi(nrrdtype, {'signed char' 'int8' 'in8_t'})) 
    matlabtype = 'int8';   
elseif any(strcmpi(nrrdtype, {'uchar' 'unsigned char' 'uint8' 'uint8_t'}))  
    matlabtype = 'uint8';    
elseif any(strcmpi(nrrdtype, {'short' 'short int' 'signed short' 'signed short int' 'int16' 'int16_t'})) 
    matlabtype = 'int16';
elseif any(strcmpi(nrrdtype, {'ushort' 'unsigned short' 'unsigned short int' 'uint16' 'uint16_t'}))  
    matlabtype = 'uint16';
elseif any(strcmpi(nrrdtype, {'int' 'signed int' 'int32' 'int32_t'}))
    matlabtype = 'int32';
elseif any(strcmpi(nrrdtype, {'uint' 'unsigned int' 'uint32' 'uint32_t'}))  
    matlabtype = 'uint32';
elseif any(strcmpi(nrrdtype, {'longlong' 'long long' 'long long int' 'signed long long' 'signed long long int' 'int64' 'int64_t'}))  
    matlabtype = 'int64';
elseif any(strcmpi(nrrdtype, {'ulonglong' 'unsigned long long' 'unsigned long long int' 'uint64' 'uint64_t'}))  
    matlabtype = 'uint64';    
elseif ( strcmp( nrrdtype, 'float' ) )
    matlabtype = 'single';    
elseif ( strcmp( nrrdtype, 'double' ) )
    matlabtype = 'double';
end


% write raw data
try
fwrite(fid,cast(nrrdData.data,matlabtype),matlabtype);
catch
  keyboard;
end;

bSuccess = fclose(fid);
if( bSuccess < 0 )
    fprintf('Warning: Failed to close NRRD data file %s!\n', nrrdData.fname );
end

cd(returnDIR);

end
