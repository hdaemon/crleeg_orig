classdef file_ASCII < file
  % FILE_ASCII class for ASCII formatted data
  %
  
  properties
    data
  end
  
  methods
    
    function obj = file_ASCII(fname,fpath)
      obj = obj@file;
      obj.validExts = {'.txt'};
      if nargin>0
        if ~exist('fpath','var'),fpath =''; end;
        obj.fpath = fpath;
        obj.fname = fname;
        
        if obj.existsOnDisk
          obj.read;
        end
      end                    
    end
    
    function read(obj)
      obj.data = dlmread(fullfile(obj.fpath,obj.fname));      
    end
    
    function write(obj)
      error('Writing ASCII data not supported');
    end
    
  end
  
end