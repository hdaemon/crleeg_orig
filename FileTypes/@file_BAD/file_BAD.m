classdef file_BAD < file
  % classdef file_BAD < file
  %
  % Object class for .BAD files.  These are spit out by BESA and other EEG
  % software as an ASCII file defining which electrodes in the associated
  % dataset are bad.
  %
  % USAGE: obj = file_BAD(fname,fpath)
  %
  % Inherited Properties:
  %   fname        : Name of EDF File
  %   fpath        : Path to EDF File
  %   validExts    : Cell array of valid EDF extensions (Just .edf)
  %   existsOnDisk : Boolean value, true if file exists in defined location
  %
  % Properties:
  %   badLabels : List of bad labels identified from the file
  %
  % Written By: Damon Hyde
  % Last Edited: Jan 22, 2015
  % Part of the cnlEEG Project
  %
  
  properties
    badLabels
  end
  
  methods
    
    function obj = file_BAD(fname,fpath)
      obj = obj@file;
      obj.validExts = {'.bad'};
      if nargin>0
        if ~exist('fname','var')||isempty(fname), fname = 'tmpBAD.bad'; end;
        if ~exist('fpath','var'), fpath = ''; end;
        obj.fpath = fpath;
        obj.fname = fname;
        
        if obj.existsOnDisk
          obj.read;
        end
        
      end
    end
    
    function read(obj)
      tmpDir = pwd;
      returnToDir = onCleanup(@() cd(tmpDir));
      cd(obj.fpath);
                  
      fid = fopen(obj.fname,'r');
      
      if fid<0, warning(['File "',obj.fname,'" does not exist!']),
        keyboard, end
      line = fgetl( fid ); % read first line
      
      %% Parse first line of BAD file
      parsed = regexp(line,'\s','split','once');
      assert(isequal(lower(parsed{1}(1:7)),'noofbad'),...
          'Doesn''t appear to be a BAD file')
      nBad = str2num(parsed{2});
        
        
      line = fgetl( fid );
      
      % loop through file
      labels = {};
      while line~=-1
        labels = {labels{:},line};
        %cell = textscan(line,'%s');         % convert line data
        %if length(cell{1})==1
        %  labels = {labels{:},cell2mat(cell{1})};
        %end;
        line = fgetl(fid);                            % read new line
      end
      fclose(fid); %             
      
      obj.badLabels = [];
      for i = 1:length(labels)
        if strcmp(labels{i}(end),'*')
          obj.badLabels{end+1}  = labels{i}(1:end-1);
        elseif ~isempty(labels{i})
          obj.badLabels{end+1} = labels{i}(1:end);
        end;
      end;
      
    end
    
    function write(obj)
      tmpDir = pwd;
      returnToDir = onCleanup(@() cd(tmpDir));
      cd(obj.fpath);
      
      fid = fopen(obj.fname,'w+');
      closeFile = onCleanup(@() fclose(fid));
      
      nBad = numel(obj.badLabels);
      fprintf(fid,['NoOfBad_2.1 ' num2str(nBad) '\n']);
      
      for idx = 1:nBad
        fprintf(fid,[obj.badLabels{idx} '\n']);
      end
            
    end
    
    
  end
  
end
