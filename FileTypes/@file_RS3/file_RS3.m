classdef file_RS3 < file
  % classdef file_DAP < file
  %
  % Class for reading DAP header files and associated data
  %
  % Written By: Damon Hyde
  % Last Edited: Dec 15, 2015
  % Part of the cnlEEG Project
  %
  
  properties
    labels
  end
  
  methods
    
    function obj = file_RS3(varargin)
      
      p = inputParser;
      p.addOptional('fname',[],@(x) ischar(x));
      p.addOptional('fpath',[],@(x) ischar(x))
      p.parse(varargin{:});
      
      
      obj = obj@file(p.Results.fname,p.Results.fpath);
      if obj.existsOnDisk
        obj.read;
      end;
    end
    
    function read(obj)
      
      currDIR = pwd;
      cd(obj.fpath);
      
      fid = fopen(obj.fname,'r');
      
      eListOut = cell(0);
      
      while ~feof(fid)
        currLine = fgetl(fid);
        
        if obj.foundKeyword('LABELS START_LIST',currLine)
          while ~obj.foundKeyword('LABELS END_LIST',currLine)&&~feof(fid)
            currLine = fgetl(fid);
            if ~obj.foundKeyword('LABELS END_LIST',currLine)
              eListOut{end+1} = currLine;
            end;
          end;
        end
        
        if obj.foundKeyword('LABELS_OTHERS START_LIST',currLine)
          while ~obj.foundKeyword('LABELS_OTHERS END_LIST',currLine)&&~feof(fid)
            currLine = fgetl(fid);
            if ~obj.foundKeyword('LABELS_OTHERS END_LIST',currLine)
              eListOut{end+1} = currLine;
            end;
          end;
        end
        
      end
      
      obj.labels = eListOut;
      fclose(fid);
      cd(currDIR);
      
    end
    
    function write(obj,fname,fpath)
      error('Writing DAP files not currently supported');
    end
    
  end
  
  methods (Access=protected, Static=true)
    function fk = foundKeyword( keyWord, cs )
      
      csU = upper( cs );
      len = length( csU );
      lenKeyword = length( keyWord );
      
      keyWordU = upper( keyWord );
      
      fk = 0;
      
      if ( len<lenKeyword )
        fk = 0;
        return
      end
      
      if ( strcmp( csU(1:lenKeyword),keyWordU ) )
        fk = 1;
        return
      else
        fk = 0;
        return
      end
      
    end
  end
end

