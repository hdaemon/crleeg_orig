classdef file_TRI < file
  % classdef file_TRI < file
  %
  % Object class for trisurf files
  %
  % Written By: Damon Hyde
  % Last Edited: Oct 19, 2015
  % Part of the cnlEEG Project
  %

  properties
    Nodes
    Triangles
  end
  
  properties (Dependent=true)
    nNodes
    nTriangles
  end
  
  methods
    function obj = file_TRI(fname,fpath)
      obj = obj@file;
      obj.validExts = {'.tri'};
      
      if nargin>0
        if ~exist('fpath','var'),fpath = ''; end;
        obj.fpath = fpath;
        obj.fname = fname;
        
        if obj.existsOnDisk;
          obj.read;
        end;
      end

    end
    
    function out = get.nNodes(obj)
      out = size(obj.Nodes,1);
    end
    
    function out = get.nTriangles(obj)
      out = size(obj.Triangles,1);
    end
    
    function read(obj)
      returnDIR = pwd;
      cd(obj.fpath);
      fid = fopen(obj.fname,'r');
      
     if fid<0, 
       warning(['File "',obj.fname,'" does not exist!']),
       keyboard, 
     end;
     
      % Read First Line
      line = fgetl( fid ); % read first line           
      cell = textscan(line,'%s%d');
      
      % Read Node Locations
      nNode = cell{2};      
      Nodes = zeros(nNode,6);      
      for i = 1:nNode
        line = fgetl(fid);
        Nodes(i,:) = cell2mat(textscan(line,'%f %f %f %f %f %f'));
      end
      
      % Read descriptor for triangles
      line = fgetl(fid);
      cell = textscan(line,'%s %d %d %d');
      
      % Read in triangle
      nTri = cell{2};
      for i = 1:nTri
        line = fgetl(fid);
        Tri(i,:) = cell2mat(textscan(line,'%f %f %f'));
      end
            
      fclose(fid); % c
      
      obj.Nodes = Nodes;
      obj.Triangles = Tri;
      
      cd(returnDIR);
                 
    end
    
    function write(obj)
      
      fid = fopen(obj.fname,'w');
      if fid<0
        error('Couldn''t open TRI file for writing');
      end
      
      fprintf(fid,['- ' num2str(obj.nNodes) '\n']);
      
      for i = 1:obj.nNodes
        fprintf(fid,['%f %f %f %f %f %f\n'],...
          obj.Nodes(i,1),obj.Nodes(i,2),obj.Nodes(i,3),...
          obj.Nodes(i,4),obj.Nodes(i,5),obj.Nodes(i,6));
      end
      
      fprintf(fid,['- ' num2str(obj.nTriangles) ' ' num2str(obj.nTriangles) ' ' num2str(obj.nTriangles) '\n']);
      
      for i = 1:obj.nTriangles
        fprintf(fid,['%d %d %d\n'],obj.Triangles(i,1),obj.Triangles(i,2),obj.Triangles(i,3));
      end;
        
      fclose(fid);
    end;
    
    function out = isinside(obj,pt)
      % function out = isinside(obj,pt)
      %
      % Check if a point is inside the surface
      %
       if ~all(size(pt)==[1 3]), error('FOO!'); end;
       
       dist = obj.Nodes(:,1:3) - repmat(pt,obj.nTriangles,1);
       dist = sqrt(sum(dist.^2,2));
       
       closest = find(dist==min(dist));
       
       vec = pt -obj.Nodes(closest,1:3);
       test = vec*obj.Nodes(closest,4:6)';
       
       if test>=0
         out = false;
       else
         out = true;
       end;
      
    end
          
    function centers = computeTriCenters(obj)
      loc = obj.Nodes(:,1:3);
      pts = loc(obj.Triangles'+1,:);
      tmp = reshape(pts,3,obj.nTriangles,3);
      tmp = permute(tmp,[1 3 2]);
      centers = squeeze(mean(tmp,1));
      centers = centers';
    end
    
    function normals = computeTriNormals(obj)
      normals = zeros(obj.nTriangles,3);
      for idxT = 1:obj.nTriangles
        pts = obj.Nodes(obj.Triangles(idxT,:)+1,1:3);
        v1 = pts(2,:)-pts(1,:);
        v2 = pts(3,:)-pts(2,:);
        normals(idxT,:) = cross(v1,v2);        
      end
    end
    
    function varargout = plot(obj)
      tmp = quiver3(obj.Nodes(:,1),obj.Nodes(:,2),obj.Nodes(:,3),obj.Nodes(:,4),obj.Nodes(:,5),obj.Nodes(:,6));
      if nargout>0
        varargout{1} = tmp;
      end;
    end
  end

end
