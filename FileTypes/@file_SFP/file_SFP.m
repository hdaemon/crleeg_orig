classdef file_SFP < file
  
  properties (Dependent=true)
    ElecLoc
    ElecLabel
    FidLoc
    FidLabel    
  end
  
  properties (SetAccess=private,GetAccess=public)
    data
    header
  end
  
  methods
    
    function obj = file_SFP(fname,fpath)
      obj = obj@file;
      obj.validExts = {'.sfp'};
      if nargin>0
      if ~exist('fpath','var'), fpath = ''; end;
      obj.fpath = fpath;
      obj.fname = fname;      
      if ~strcmpi(obj.fext,'.sfp')
        error('File is not an SFP file');
      end;
      end;
    end
        
    function read(obj)
      currentDIR = pwd;
      cd(obj.fpath);
      [obj.data obj.header] = sload(obj.fname);            
      cd(currentDIR);
    end
        
    
    function LocOut = get.ElecLoc(SFPFile)
      ind = getIndex(SFPFile,'electrodes');
      LocOut = SFPFile.data(ind,:);
    end
    
    function LabOut = get.ElecLabel(SFPFile)
      ind = getIndex(SFPFile,'electrodes');
      ind = find(ind);
      for i = 1:length(ind)
        LabOut{i} = strtrim(SFPFile.header.Label{ind(i)});
      end;
    end
    
    function LocOut = get.FidLoc(SFPFile)
      ind = getIndex(SFPFile,'fiducials');
      LocOut = SFPFile.data(ind,:);
    end
    
    function LabOut = get.FidLabel(SFPFile)
      ind = getIndex(SFPFile,'fiducials');
      ind = find(ind);
      for i = 1:length(ind)
        LabOut{i} = strtrim(SFPFile.header.Label{ind(i)});
      end;
    end
    
    function ind = getIndex(SFPFile,type)
      switch lower(type)
        case 'electrodes'
          % Find just those points whose description starts with 'E'
          ind = zeros(size(SFPFile.data,1),1);
          for i = 1 : numel(ind)
            % check description of data (if E... its the electrode)
            desc = strtrim(SFPFile.header.Label{i}); % convert to string (class: char)
            if(desc(1) == 'E'), ind(i) = 1; end
          end
        case 'fiducials'
          ind = zeros(size(SFPFile.data,1),1);
          for i = 1 : numel(ind)
            % check description of data (if E... its the electrode)
            desc = strtrim(SFPFile.header.Label{i}); % convert to string (class: char)
            if(desc(1) ~= 'E'), ind(i) = 1; end
          end
      end
      ind = logical(ind);
    end
    
    
    
    function write(obj)
      error('Writing of SFP files not currently supported');
    end
    
    function out = get.data(obj)
      if isempty(obj.data)
        obj.read;
      end      
      out = obj.data;
    end
    
    function out = get.header(obj)
      if isempty(obj.header)
        obj.read;
      end;
      out = obj.header;
    end
    
  end
  
end