classdef file_DAT < file
  % FILE_DAT Class for .dat ASCII/Binary Data Files
  %
  % classdef file_DAT < file
  %
  % Usage:
  %    obj = file_DAT(fname,fpath,varargin)
  %
  % Inputs:
  %    fname = Filename of DAT file to read
  %    fpath = Path of file. Defaults to ./
  %
  % Written By: Damon Hyde
  % Last Edited: Nov 6, 2015
  % Part of the cnlEEG Project
  %
  
  properties
    data
    datatype = 'ASCII';
    datasize 
  end
  
  methods
   
    function obj = file_DAT(fname,fpath,type,size)
      obj = obj@file;
      obj.validExts = {'.dat'}; 
      if nargin>0
        if ~exist('fpath','var'),fpath =''; end;
        obj.fpath = fpath;
        obj.fname = fname;
        
        if exist('type','var'),
          obj.datatype = type;
        end;
        
        if exist('size','var'),
          obj.datasize = size;                  
        end;
        
        if obj.existsOnDisk
          obj.read;
        end;
      end
      
    end
    
    function read(obj)
      switch obj.datatype
        case 'ASCII'     
          obj.data = dlmread([obj.fpath obj.fname]);
          obj.datasize = size(obj.data);
        case 'FLOAT'
          fid = fopen([obj.fpath obj.fname]);
          tmp = fread(fid,inf,'float32');
          if ~isempty(obj.datasize)
          obj.data = reshape(tmp,obj.datasize);
          else
            obj.data = tmp;
          end;
          fclose(fid);
        otherwise
          error('Unknown data type');
      end;
    end
    
    function write(obj)
      error('Writing of .dat files not currently supported');
      dlmwrite([obj.fpath obj.fname],obj.data);
    end
    
  end
  
end