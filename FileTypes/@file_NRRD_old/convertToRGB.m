function nrrdOut = convertToRGB(nrrdIn,fname,fpath)
%  function nrrdOut = convertToRGB(nrrdIn,fname,fpath)
%
%  NOTE: interface is not stable for this.  Additional optional parameters
%  to be added
%
%  Author:  D. Hyde
%  Date: 10/28/2014
%

if ~exist('fname','var'), fname = ['RGB_' nrrdIn.fname]; end;
if ~exist('fpath','var'), fpath = './'; end;

% Put some error checking in here somewhere
if ~nrrdIn.isScalar,
  error('convertToRGB method requires a scalar NRRD');
end;

nrrdOut = clone(nrrdIn,fname,fpath);
nrrdOut.dimension = 4; % Should be changed, eventually
nrrdOut.kinds = {'RGB-color' 'domain' 'domain' 'domain'};
nrrdOut.sizes = [3 nrrdOut.sizes];
nrrdOut.type  = 'unsigned char';

whichMap = 'hot';
switch whichMap
  case 'hot', CMap = hot(256);
  case 'jet', CMap = jet(256);
end;

dataout = nrrdIn.data;

absVal = true;
if absVal
  dataout = abs(dataout);
  lo = 0;
  hi = max(dataout(:));
else
  lo = -max([abs(min(dataout(:))) max(dataout(:))]);
  hi =  max([abs(min(dataout(:))) max(dataout(:))]); 
end;
delta = hi-lo;

dataout = dataout - lo;
dataout = dataout./delta;
dataout = 255*dataout;
dataout = round(dataout);
dataout = dataout+1;

tmp = CMap(dataout,:);
tmp = reshape(tmp',[3 size(dataout)]);

nrrdOut.data    = round(255*tmp);

return;