%%
close all
clear all
clear classes
clc

test = file_NRRD('AvgSpikeRecon.nhdr','/common/projects2/epilepsysurgicalplanning/processed/Case164/20140106/sourceanalysis/eeg');
% foo = test.viewslice(1,40);
% currSize = get(foo.figure,'Position');
% currSize = currSize + [ 0 0 0 60];
% set(foo.figure,'Position',currSize);
% currPos = get(foo.panel,'Position');
% currPos = currPos + [ 0 60 0 0 ];
% set(foo.panel,'Position',currPos);

testviewer(test);

% type = 3;
% cmap = jet(256);
% sliceImg(1) = uitools.cnlSliceView('parent',figure,...
%   'title',test.fname,...
%   'fhnd',@(a,b)test.getSlice_RGB(a,b,type,cmap));
% currSize = get(gcf,'Position');
% currSize = currSize + [ 0 0 0 60];
% set(gcf,'Position',currSize);
% currPos = get(sliceImg(1).slicePanel,'Position');
% currPos = currPos + [0 60 0 0 ];
% set(sliceImg(1).slicePanel,'Position',currPos);
% sliceImg(2) = uitools.cnlSliceView('parent',gcf,...
%   'title',test.fname, ...
%   'fhnd',@(a,b)test.getSlice_RGB(a,b,3,hot(256)),...
%     'origin', [550 60]);
% 
% control = uitools.cnlSliceControl(gcf,test.sizes);
% control.updatefunction = @(a,b)sliceImg(1).updateSelections(a,b);

%test.viewslice(1,45,'figure',gcf,'origin',[550 0]);
%test.viewslice(2,50,'figure',gcf,'origin',[1100 0]);
