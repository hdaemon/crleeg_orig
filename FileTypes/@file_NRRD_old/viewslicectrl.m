function controlsOut = viewslicectrl(nrrdObj,varargin)

  warning('Pretty sure this should be deprecated');
  keyboard;
  
p = inputParser;
p.addParamValue('parent',[]);
p.addParamValue('axis',[]);
p.addParamValue('origin',[0 0]);
p.addParamValue('ctrlsize',[320 40]);
parse(p,varargin{:});

parent = p.Results.parent;
origin = p.Results.origin;
ctrlsize = p.Results.ctrlsize;
axesHandle = p.Results.axis;
border = 10;

imageSize = nrrdObj.sizes(nrrdObj.domainDims);
aspect = nrrdObj.getAspect;

% Set up the uiPanel
if isempty(parent), parent = gcf; end;
controlPanel = uipanel('Title', 'Slice Selection', 'Parent',parent,...
           'Units','pixels', 'Position', [origin 0 0 ] + [0 0 ctrlsize] + border);

controlsOut.parent = parent;
controlsOut.panel = controlPanel;

controlsOut.axSel = uibuttongroup( 'Parent',controlPanel,'Units','Pixels',...
                               'Position', [5 5 100 27],...
                               'SelectionChangeFcn', @selectAxes);
 controlsOut.b1 = uicontrol(controlsOut.axSel,'Style','radiobutton', 'Units','Pixels',...
                   'String', 'X', 'Position', [3 2 33 22],'Value',1);
 controlsOut.b2 = uicontrol(controlsOut.axSel,'Style','radiobutton', ...
                   'String', 'Y', 'Position', [33 2 33 22],'Value',2);
controlsOut.b3 = uicontrol(controlsOut.axSel,'Style','radiobutton', ...
                  'String', 'Z', 'Position', [63 2 33 22],'Value',3);
                set(controlsOut.axSel,'Visible','on')

controlsOut.currAxes = 1;                
                
  function selectAxes(h,EventData)    
    switch get(EventData.NewValue,'String')
      case 'X', controlsOut.currAxes = 1;
      case 'Y', controlsOut.currAxes = 2;
      case 'Z', controlsOut.currAxes = 3;
    end
    disp(['controlsOut.currAxes = ' num2str(controlsOut.currAxes)]);    
    
    setSliceSelRange;
    setSliceLabel(1);
    aspect = nrrdObj.getAspect;
    setAspect(aspect(1),aspect(3));
   % updateImage;
  end                
                
controlsOut.sliceSel = uicontrol('Style','slider',...
  'Parent', controlPanel, ...
  'Max',imageSize(1),'Min',1,'Value',1,...
  'SliderStep',[1/imageSize(1) 10*(1/imageSize(1))],...
  'Callback',@changeSlice, ...
  'Position', [115 5 150 22]);

controlsOut.maxSliceLabel = uicontrol('Style','text',...
  'Parent',controlPanel,...
  'String',[num2str(round(get(controlsOut.sliceSel,'Value'))) '/' num2str(imageSize(1))],...
  'Position', [265 5 60 22]);

  function setSliceSelRange
    set(controlsOut.sliceSel,'Max',imageSize(controlsOut.currAxes));
    set(controlsOut.sliceSel,'Value',1);
    set(controlsOut.sliceSel,'SliderStep',[1/imageSize(controlsOut.currAxes) 10*(1/imageSize(controlsOut.currAxes))]);    
  end

 function setSliceLabel(slice)
    set(controlsOut.maxSliceLabel,'String',[num2str(slice) '/' num2str(imageSize(controlsOut.currAxes))]);
 end

  function setAspect(xScale,yScale)
    set(axesHandle,'DataAspectRatio',[xScale yScale 1]);
  end

  function out = currSlice
    if exist('controlsOut','var')
      if isfield(controlsOut,'sliceSel')
    out = round(get(controlsOut.sliceSel,'Value'));
      end;
    else
      slice = 1;
    end;
  end

  function changeSlice(h,EventData)    
    set(controlsOut.maxSliceLabel,'String',[num2str(currSlice) '/' num2str(imageSize(controlsOut.currAxes))]);
    disp('Make Call To Update Function');
   % updateImage;    
  end

end

% % Add Radio Buttons for Axis selection         
% controlsOut.axSel(1) = uicontrol('Style', 'radiobutton', ...
%   'Parent', controlPanel,...
%   'Callback',@selectX, ...
%   'Units', 'pixels', ...
%   'Position', [10 5 80 22], ...
%   'String', 'X', ...
%   'Value', 1);
% controlsOut.axSel(2) = uicontrol('Style','radiobutton', ...
%   'Parent', controlPanel,...
%   'Callback',@selectY,...
%   'Units', 'pixels', ...
%   'Position', [40 5 80 22], ...
%   'String' ,'Y', ...
%   'Value', 0);
% controlsOut.axSel(3) = uicontrol('Style','radiobutton', ...
%   'Parent', controlPanel,...
%   'Callback',@selectZ,...
%   'Units', 'pixels', ...
%   'Position',  [70 5 80 22], ...
%   'String' ,'Z', ...
%   'Value', 0);
% controlsOut.currAxes = 1;

% %% Radio call buttons for Axis Selection
%   function selectX(h,EventData)
%     set(controlsOut.axSel(2:3),'Value',0);
%     controlsOut.currAxes = 1;
%     setSliceSelRange;
%     setSliceLabel(1);
%     setAspect(aspect(2),aspect(3));
%    % updateImage;
%   end
%   
%   function selectY(h,EventData)
%     set(controlsOut.axSel([1 3]),'Value',0);
%     controlsOut.currAxes = 2;
%     setSliceSelRange;
%     setSliceLabel(1);
%     aspect = nrrdObj.getAspect;
%     setAspect(aspect(1),aspect(3));
%    % updateImage;
%   end
%   
%   function selectZ(h,EventData)
%     set(controlsOut.axSel(1:2),'Value',0);
%     controlsOut.currAxes = 3;
%     setSliceSelRange;
%     setSliceLabel(1);
%     setAspect(aspect(1),aspect(2));
%    % updateImage;
%   end