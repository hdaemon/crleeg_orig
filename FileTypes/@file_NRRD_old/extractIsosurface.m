function [surfOut] =  extractIsosurface(hdrMRI,ds,isoLevel)
% function [surfaceStruct] =  ExtractIsosurface(nrrdIn,ds,isoLevel)
%
% Given an input of type file_NRRD, identify an isosurface within the
% image.

warning('This functionality will be changed soon');  
  
% Default Downsampling
if ~exist('ds','var')||isempty(ds)
  ds = 2; 
end;

if length(ds)==1
  ds = ones(1,3)*ds;
end;

% Default Isolevel
if ~exist('isoLevel')
  isoLevel = 0.5;
  mydisp('Defaulting to an isolevel of 0.5');
end;

D = hdrMRI.data(1:ds(1):end,1:ds(2):end,1:ds(3):end); % resample data for preview
Ds = smooth3(D); % smooth data

%[xs ys zs] = hdrMRI.sizes;
% x =  [ (-xs/2+1) : xs/2 ]; % x-sampling grid
% y =  [ (-ys/2+1) : ys/2 ]; % y-sampling grid
% z =  [ (-zs/2+1) : zs/2 ]; % z-sampling grid

% Get indices for the voxels
x = (1:hdrMRI.sizes(1)) - 1;
y = (1:hdrMRI.sizes(2)) - 1;
z = (1:hdrMRI.sizes(3)) - 1;

%x =  [(-hdrMRI.sizes(1)/2+1) : hdrMRI.sizes(1)/2];
%y =  [(-hdrMRI.sizes(2)/2+1) : hdrMRI.sizes(2)/2];
%z =  [(-hdrMRI.sizes(3)/2+1) : hdrMRI.sizes(3)/2];

% Find their location in the MRI coordinate space
aspect = hdrMRI.getAspect;
X = aspect(1) .* x(1:ds(1):end) ;%+ hdrMRI.gridSpace.origin(1); % x
Y = aspect(2) .* y(1:ds(2):end) ;%+ hdrMRI.gridSpace.origin(2); % y
Z = aspect(3) .* z(1:ds(3):end) ;%+ hdrMRI.gridSpace.origin(3); % z

% Extract Isosurface
[faces,vert] = isosurface(X,Y,Z,permute(Ds,[2 1 3]),isoLevel); % isosurface
%keyboard
%[faces,vert] = isosurface(X,Y,Z,Ds,isoLevel); % isosurface

vert = vert*diag(1./aspect);
vert = hdrMRI.spacedirections*vert';
vert = vert' + repmat(hdrMRI.gridSpace.origin,size(vert,2),1);

%vert = [vert(:,2),vert(:,1),vert(:,3)]; % remap vertices for proper preview

surfaceStruct.faces  = faces;
surfaceStruct.vert   = vert;
surfaceStruct.aspect = aspect;
surfaceStruct.X      = X;
surfaceStruct.Y      = Y;
surfaceStruct.Z      = Z;
surfaceStruct.Xpix   = x;
surfaceStruct.Ypix   = y;
surfaceStruct.Zpix   = z;

surfOut = cnlIsosurface(surfaceStruct);
end