function [RGBout] = getSlice_RGB(nrrd,varargin)
% GETSLICE_RGB Get an RGB image of a slice from a NRRD
%
% function [RGBout] = GETSLICE_RGB(nrrd,axis,slice,type)
%
% file_NRRD method to 
%
% Written By: Damon Hyde
% Last Edited: March 2015
% Part of the cnlEEG Project
%
% DEPRECATED : April 2015
%

% Input Parsing
p = inputParser;
p.addParamValue('type',2);
p.addParamValue('cmap',jet(256));
p.addParamValue('axis',1);
p.addParamValue('slice',1);
p.addParamValue('otherDim',[]);
parse(p,varargin{:});

type = p.Results.type;
cmap = p.Results.cmap;
axis = p.Results.axis;
slice = p.Results.slice;
otherDim = p.Results.otherDim;

% Get the appropriate modified slice
[slice, imgMin, imgMax] = nrrd.getSlice_Modified('type',type,...
  'axis',axis,'slice',slice,'otherDim',otherDim);

% Orient image appropriately (this needs some fixing)
slice = orientimage(slice,axis);

% Change things so they look right.
if imgMax~=0
  RGBout = (slice-imgMin)*(size(cmap,1)-1)/(imgMax-imgMin);
else
  RGBout = zeros(size(slice));
end;
RGBout = round(RGBout);
RGBout = RGBout+1;

RGBout = cmap(RGBout,:);
RGBout = reshape(RGBout,[size(slice) 3]);

end

function slice = orientimage(slice,axis)
 slice = permute(slice,[2 1]);
 if isempty(which('flip'))
   % Compatibility with older matlab versions
   slice = flipdim(slice,1);
 else
 slice = flip(slice,1);
 end;
end