function [slice, varargout] = getSlice_Modified(nrrd,varargin)
% function slice = GETSLICE_MODIFIED(nrrd,varargin)
%
% Pull a slice from nrrd, and process it a bit.  This is mostly applicable
% for NRRDS where there is more than a single scalar value at each voxel,
% as it allows the 
%
% Written By: Damon Hyde
% Last Edited: March 2015
% Part of the cnlEEG Project
%

% Input parsing
p = inputParser;
p.addParamValue('type','true');
p.addParamValue('axis',1);
p.addParamValue('slice',1);
p.addParamValue('otherDim',[]);
parse(p,varargin{:});

type     = p.Results.type;
axis     = p.Results.axis;
slice    = p.Results.slice;
otherDim = p.Results.otherDim;

% Fetch the requested slice
slice = nrrd.getSlice('axis',axis,'slice',slice,'otherDim',otherDim);

% Modify that slice as desired.
switch type
  case 'true'
    if ~ismatrix(slice)
      slice = (sum(slice,1));
      tmp = sum(nrrd.data,1);
    else
      tmp = nrrd.data;
    end;
  case 'norm'
    if ~ismatrix(slice)
      slice = sqrt(sum(slice.^2,1));
      tmp = sqrt(sum(nrrd.data.^2,1));
    else
      tmp = nrrd.data;
    end
  case 'abs'
    if ~ismatrix(slice)
      slice = sum(abs(slice),1);
      tmp = sum(abs(nrrd.data),1);
    else
      slice = abs(slice);
      tmp = abs(nrrd.data);
    end;
  case 'exp'
    if ~ismatrix(slice)
      slice = sum(exp(slice),1);
      tmp = sum(exp(nrrd.data),1);
    else
      slice = exp(slice);
      tmp = exp(nrrd.data);
    end;
     case 'log'
    if ~ismatrix(slice)      
      slice = sum(log10(abs(slice)),1);
      slice(isnan(slice)) = 0;
      
      tmp = sum(log10(abs(nrrd.data)),1);
      tmp(isnan(slice)) = 0;
    else
      slice = log10(abs(slice));
      slice(isnan(slice)) = 0;
      tmp = log10(abs(nrrd.data));
      tmp(isnan(tmp)) = 0;
    end;
  case 'parcel'
    if ismatrix(slice)      
      nParcel = max(slice(:));
      
      imgOut = zeros(size(slice));
      for i = 1:nParcel
        val = mod(i,20);
        imgOut(slice==i) = 5+val;
      end
      slice = imgOut;
      tmp = slice;
    else
      tmp = 0;
    end;
end

imgMax = max(tmp(:));
imgMin = min(tmp(:));

slice = squeeze(slice);

if nargout>=2, varargout{1} = imgMin; end;
if nargout>=3, varargout{2} = imgMax; end;
  
end