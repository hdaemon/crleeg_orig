function sliceOut = viewslice(nrrdObj,axisSel,sliceSel,varargin)
% function sliceOut = viewslice(nrrdObj,axisSel,slice,varargin)
%
% What exactly is this method doing?  Is it even used?
%

warning('Are you actually using this?  What for?');
keyboard;

% Parse Inputs
p = inputParser;
p.addParamValue('figure',[],@(h) ishandle(h));
p.addParamValue('parent',[],@(h) ishandle(h));
p.addParamValue('origin',[0 0],@(x) isvector(x) && numel(x)==2);
p.addParamValue('size',[500 500],@(x) isvector(x) && numel(x)==2);
p.addParamValue('type',1);
p.addParamValue('cmap',jet(256));
parse(p,varargin{:});

currFig   = p.Results.figure;
parentObj = p.Results.parent;
origin    = p.Results.origin;
imgsize   = p.Results.size;
type = p.Results.type;
CMap = p.Results.cmap;

% Move to appropriate figure and resize if necessary
if isempty(currFig), figure; else figure(currFig); end; currFig = gcf;
uitools.setMinFigSize(currFig,origin,imgsize+[0 25]);

% Parent to current figure if no parent provided
if isempty(parentObj), parentObj = gcf; end;

% Initialize uiPanel for drawing
slicePanel = uipanel('Title',nrrdObj.fname,'Parent',parentObj,...
  'Units','pixels','Position', [origin 0 0 ] + [0 0 imgsize+20] + 5);

% Add NRRD data, axis selection, and slice selection to UserData
sliceOut.figure = gcf;
sliceOut.panel = slicePanel;
sliceOut.nrrdObj = nrrdObj;
sliceOut.axisSel = axisSel;
sliceOut.sliceSel = sliceSel;
sliceOut.type = type;
sliceOut.CMap = CMap;

% Initialize the Drawing Axis
sliceAxes  = axes('Parent',slicePanel,'Units','Pixels','Position',[10 10 imgsize]);
set(sliceAxes,'Units','normalized');
sliceOut.axes = sliceAxes;


sliceOut.fhnd_redraw = @(x)redraw(x);
redraw(sliceOut);

end

function redraw(sliceObj)
RGBout = prepSlice(sliceObj);
img = sliceObj.CMap(RGBout,:);
img = reshape(img,[size(RGBout) 3]);

sliceAxes = sliceObj.axes;
newImg = image(img,'Parent',sliceAxes,'ButtonDownFcn',get(sliceAxes,'ButtonDownFcn'));
set(sliceAxes,'ButtonDownFcn',get(newImg,'ButtonDownFcn'));
axis off;
end

