function [varargout] = view(nrrdObj,varargin)
  % VIEW View slices from a file_NRRD object.
  %
  % function nrrdView = view(nrrdObj,varargin)
  %
  % A basic slice viewer for visualizing NRRDS.
  %
  % Should probably be ported over to be a handle object, so things stay nice
  % and consistent when used in larger viewers
  %
  
  p = inputParser;
  p.addOptional('nrrdOverlay',[],@(x)isa(x,'file_NRRD')||isa(x,'function_handle'));
  p.addParamValue('parent',[],@(h) ishandle(h))
  p.addParamValue('origin',[0 0],@(x) isvector(x) && numel(x)==2);
  p.addParamValue('size',[530 530]);
  p.addParamValue('cmap',[]);
  p.addParamValue('disptype',[]);
  p.addParamValue('overalpha',0.5);
  parse(p,varargin{:});
  
  % Parse Inputs
  parentH     = p.Results.parent;
  origin      = p.Results.origin;
  imageSize   = nrrdObj.sizes(nrrdObj.domainDims);
  nrrdOverlay = p.Results.nrrdOverlay;
  
  % If no parent provided, open a new figure
  if isempty(parentH)
    parentH = figure;
    origin = [ 0 0 ];
  end;
  
  % Set up cnlImageViewer, and bind the update function to
  % nrrdObj.getSlice_RGB
  viewer = uitools.cnlImageViewer(nrrdObj.sizes,'Parent',parentH,...
    'Size',0.75*[530 700]);
  viewer.title = nrrdObj.fname;
  viewer.fhnd_getImgSlice = @(a,b)nrrdObj.getSlice...
                              ('axis',a,'slice',b);%,...
                              % 'type',viewer.colorControl.currType,...
                              % 'cmap',viewer.colorControl.currCMap);
  viewer.sliceImg.imgRange = ...
    @()nrrdObj.getImageRange(viewer.colorControl.currType);
  viewer.aspectRatio = nrrdObj.getAspect;
                              
                              
  % If an overlay NRRD or function handle is present, configure it.
  viewer.alphaControl.selectedValue = 1;
  if isa(nrrdOverlay,'file_NRRD')
    viewer.fhnd_getOverlaySlice = @(a,b)nrrdOverlay.getSlice...
                              ('axis',a,'slice',b);%,...
                               %'type',viewer.overlayColor.currType,...
                               %'cmap',viewer.overlayColor.currCMap);
    viewer.sliceImg.overlayRange = ...
            @()nrrdOverlay.getImageRange(viewer.overlayColor.currType);                           
    viewer.alphaControl.selectedValue = 0.5;
  elseif isa(nrrdOverlay,'function_handle')
    error('Functionality not yet implemented');
    viewer.fhnd_getOverlaySlice;
  end
  
  % Update view controls
  viewer.setUILocations;
  
  % Set the viewer to occupy the entire figure.
  viewer.units = 'normalized';
  viewer.position = [0.01 0.01 0.98 0.98];
  
  if nargout>0
    varargout{1} = viewer;
  end;
  
  function updateAlpha(val)   
    sliceImg.alphaOverlay = val;
  end
  
end

