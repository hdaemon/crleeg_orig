function controlsOut = drawSliceControl(parent,origin,ctrlsize)

if ~exist('parent','var')||isempty(parent)
  parent = gcf;
end;

if ~exist('origin','var')||isempty(origin)
  origin = [0 0];
end;

if ~exist('ctrlsize','var')||isempty(ctrlsize)
  ctrlsize = [320 40];
end;

border = 10;

controlPanel = uipanel('Title', 'Slice Selection', 'Parent',parent,...
           'Units','pixels', 'Position', [origin 0 0 ] + [0 0 ctrlsize] + border);

controlsOut.parent = parent;
controlsOut.panel = controlPanel;

controlsOut.axSel = uibuttongroup( 'Parent',controlPanel,'Units','Pixels',...
                               'Position', [5 5 100 27]);
controlsOut.b1 = uicontrol(controlsOut.axSel,'Style','radiobutton', 'Units','Pixels',...
                   'String', 'X', 'Position', [3 2 33 22]);
controlsOut.b2 = uicontrol(controlsOut.axSel,'Style','radiobutton', ...
                   'String', 'Y', 'Position', [33 2 33 22]);
controlsOut.b3 = uicontrol(controlsOut.axSel,'Style','radiobutton', ...
                  'String', 'Z', 'Position', [63 2 33 22]);
                set(controlsOut.axSel,'Visible','on')

controlsOut.sliceSelect = uicontrol('Style','slider',...
  'Parent', controlPanel, ...
  'Max',imageSize(1),'Min',1,'Value',1,...
  'SliderStep',[1/imageSize(1) 10*(1/imageSize(1))],...  
  'Position', [115 5 150 22]);

controlsOut.maxSliceLabel = uicontrol('Style','text',...
  'Parent',controlPanel,...
  'String',[num2str(round(get(controlsOut.sliceSel,'Value'))) '/' num2str(imageSize(1))],...
  'Position', [265 5 60 22]);                
                
end