
classdef file_NRRD < handle & matlab.mixin.Copyable
  %  function obj = file_NRRD(filename,filepath,readonly)
  %
  %  file_NRRD is a copyable handle object that acts as a composition of
  %  the file_NRRDHeader and file_NRRDData object types.  NRRDs using this
  %  toolbox must have separate header and data files.  If file_NRRD is
  %  passed a .nrrd file, the first thing it will attempt to do is to use
  %  the UNU toolbox to convert the .nrrd file into a .nhdr header and .raw
  %  uncompressed data file.
  %
  %  This object class is a rewrite of my original file_NRRD class. It
  %  adds class hierarchy, separating the header and data files into their
  %  own object classes.
  %
  %  By splitting the functionality into multiple objects and then
  %  recombining them in the file_NRRD class, consistency is maintained
  %  with the file class and a lot of the other code ends up getting
  %  simplified.  Validation of file paths is done at the level of the file
  %  class.
  %
  %  Properties
  %    hdrfile  : A file_NRRDHeader Object
  %    datafile : A file_NRRDData object
  %
  %  Dependent Properties
  %    fname  : Filename for NRRD Header
  %    fpath  : Path for NRRD Header
  %    data_fname: Filename for NRRD Data
  %
  %  Dependent Properties (Extracted from file_NRRDHeader)
  %    content
  %    type
  %    dimension
  %    space
  %    endian
  %    spaceorigin
  %    kinds
  %    thicknesses
  %    spacedirections
  %    spaceunits
  %    centerings
  %    measurementframe
  %
  %  Dependent Properties (Extracted from file_NRRDData)
  %    data 
  %    readOnly
  %    saveData
  %    dataType
  %    hasData
  %    
  %
  
  properties
    hdrfile  % NRRD Header File
    datafile % NRRD Data File
  end
  
  properties (Dependent = true, Transient=true)
    % File Name stuff
    fname
    fpath
    data_fname
    
    % From file_NRRDHeader
    content
    type
    dimension
    space
    sizes
    endian
    encoding
    spaceorigin
    kinds
    thicknesses
    spacedirections
    spaceunits
    centerings
    measurementframe
    
    
    % From file_NRRDData
    data
    readOnly
    saveData
    dataType
    hasData
    
    existsOnDisk
  end
  
  properties (Hidden)
    imgRanges
  end
  
  properties (Dependent = true, Transient = true, SetAccess = private)
    gridSpace      % Gridspace associated with the NRRD.
    domainDims     % Dimensions of obj.data associated with spatial directions
    nonZeroVoxels  % List of voxels with nonzero value
    zeroVoxels
    nVoxels        % Total number of voxels in the space
    isScalar
  end
  
  
  methods
    
    function obj = file_NRRD(filename,filepath,readonly)
      % function obj = file_NRRD(filename,filepath,readonly)
      %
      %
      
      if nargin>0
        
        mydisp('Constructing NRRD object',5);
        
        % Set defaults
        if ~exist('filename','var'), filename = 'NewNRRD.nhdr'; end;
        if ~exist('filepath','var'), filepath = ''; end;
        if ~exist('readonly','var'), readonly = false; end;
        
        % If we were handeded a file_NRRD, just return the same file_NRRD.
        if isa(filename,'file_NRRD')
          obj.hdrfile = filename.hdrfile;
          obj.datafile = filename.datafile;
          return;
        end;
        
        obj.loadFromFile(filename,filepath,readonly);
        
        mydisp('Completed construction of NRRD object',5);
        
      else
        % Just generate a default NRRD file in the current directory
        obj.hdrfile  = file_NRRDHeader('NewNRRD.nhdr');
        obj.datafile = file_NRRDData('NewNRRD.raw',[],[0 0 0]);
      end
    end
    
    
    function loadFromFile(obj,filename,filepath,readonly)
      
      if ~exist('readonly','var'), readonly = false; end;
      
      % Split data and header if we were given a .NRRD file
      [~, name, ext] = fileparts(filename);
      if strcmpi(ext,'.nrrd')
        dirForReturn = pwd;
        if exist(filepath,'dir')          
          cd(filepath);
        end;
        
          if exist(['./' filename],'file')
            
            mydisp(['Automatically converting ' filename ' from NRRD to NHDR and RAW format']);

            system(['Convert.sh ' name]);
            filename = [name '.nhdr'];
          else
            error('file_NRRD:cantlocate','Trying to load a .nrrd file and cannot locate it');
          end
        
      end
      
      % Initialize Header File And Read
      obj.hdrfile = file_NRRDHeader(filename,filepath);
      
      % Initialize Data File
      obj.datafile  = file_NRRDData(obj.data_fname,filepath,obj.sizes,obj.type,readonly);
    end;
    
    function out = get.gridSpace(obj)
      % function out = get.gridSpace(obj)
      %
      % Returns the cnlGridSpace associated with the NRRD.  This is
      % equivalent to calling
      % out=cnlGridSpace(sizes,obj.spaceorigin,obj.spacedirections)
      % where sizes has been extracted from obj.sizes to include only spatial
            % domain dimensions.
      sizes = obj.sizes(obj.domainDims);
      out = cnlGridSpace(sizes,obj.spaceorigin,obj.spacedirections);
    end
    
    
    %%
    
    function disp(obj)
      for idx = 1:numel(obj)
      obj(idx).hdrfile.disp;
      disp(['         ReadOnly: ' BoolToString(obj(idx).datafile.readOnly)]);
      disp(['          hasData: ' BoolToString(obj(idx).datafile.hasData)]);
      disp(' ');
      end;
    end;
    
    % Saves before purging
    function saveAndPurge(obj)
      error('Not yet implemented');
    end;
    
    function changeFileName(obj,filename)
      % function changeFileName(obj,filename)
      %
      % Changes both the header and data filenames to match the input value.
      % The header gets the .nhdr extention, while the data files gets the
      % .raw extension.
      warning('Functionality is deprecated');
      [~, name, ext] = fileparts(filename);
      if strcmpi(ext,'.nhdr')
        obj.fname = [name '.nhdr'];
        obj.data_fname = [name '.raw'];
      else
        error('Filename must end in .nhdr');
      end;
    end;
    
    function objOut = clone(obj,fname,fpath)
      % function objOut = clone(obj,fname,fpath)
      %
      % Uses the abilities of matlab.mixin.Copyable to create a copy of a
      % file_NRRD object.  Does both a shallow copy of the object, and a
      % deep copy of both the header and data files.
      
      if ~exist('fname','var'), fname = 'tmp.nhdr'; end;
      if ~exist('fpath','var'), fpath = './'; end;
      
      % Construct a fresh file_NRRD
      objOut = copy(obj); % First copy the NRRD
     
      % Cloned NRRDs are by default write-capable
      objOut.readOnly = false;
 
      % Change file names
      objOut.fname = fname;
      objOut.fpath = fpath;
      
      if ~obj.hasData&(obj.datafile.existsOnDisk)
        obj.datafile.read;
      end;
      
      %Copy the datafile and rename appropriately;
      [~,fname,~] = fileparts(fname);
      objOut.data_fname = [fname '.raw'];
      objOut.datafile = obj.datafile.copy;
      objOut.datafile.fname = objOut.data_fname;
      objOut.datafile.fpath = objOut.fpath;
      
    end
    
    function save(obj)
      % function save(obj)
      %
      % Write a file_NRRD to disk.
      
      mydisp(['Saving ' obj.fname ]);
      
      % Write out the file
      if ~obj.datafile.readOnly
        if obj.datafile.hasData
          try
          obj.hdrfile.write;
          obj.datafile.write;
          catch
            mydisp(['Tried to save ' num2str(obj.fname) '' ... 
              'but it failed... continuing... ']);
          end;
        else

          error('NRRD Has No Data');
        end;
      else
        error('NRRD is Flagged Read Only');
      end;
    end
    
    function reload(obj)
      % function reload(obj)
      %
      % Reload the NRRD header and data from disk.
      obj.hdrfile.read;
      obj.datafile.read;
    end;
        
    function import(obj,nrrd)
      % function import(obj,nrrd)
      %
      % Import an old Nrrd Structure into the new nrrd object
      mydisp('Importing data from old NRRD object',5)
      if ~(isempty(obj.fname)||isempty(obj.fpath)||isempty(obj.datafile))
        if exist('nrrd','var')&&isstruct(nrrd)
          fields = fieldnames(nrrd);
          for i = 1:length(fields)
            if ~(any(strcmp(fields{i},{'filename' 'filepath' 'datafile'})) || ...
                isempty(obj.(fields{i})))
              obj.(fields{i}) = nrrd.(fields{i});
              
              % Default file naming?  Not sure I want this here.
              if strcmpi(fields{i},'content')&&isempty(obj.fname)
                obj.fname = [nrrd.content '.nhdr'];
              end;
              
              % Handle data
              if strcmpi(fields{i},'data')&&~isempty(nrrd.data)
                obj.hasData = true;
                obj.data = nrrd.data;
              end;
            end;
          end;
        else
          error('Need to input an old nrrd structure');
        end;
      else
        error('Please set filename, filepath, and datafile before importing');
      end;
      mydisp('Completed import of old NRRD object',5);
    end
    
    function out = get.domainDims(obj)
      % function out = get.domainDims(obj)
      %
      %
      out = false(1,length(obj.sizes));
      for i = 1:length(obj.sizes)
        if strcmpi(obj.kinds{i},'domain');
          out(i) = true;
        end
      end
    end
    
    function out = get.zeroVoxels(obj)
      % function out = get.zeroVoxels(obj)
      %
      % Return list of indices into domain dimensions of voxels with a
      % nonzero value in any of the associated non-domain dimensions.
      %
      if ~strcmpi(obj.sizes,'???')
        domainDims = obj.domainDims;
        sizes   = obj.sizes;
        nVoxels = prod(sizes(domainDims));
        nData   = prod(sizes(~domainDims));
        
        tmp = reshape(obj.data,[nData nVoxels]);
        tmp = sum(tmp,1);
        out = find(tmp==0);
      else
        warning(sprintf(['Requested zeroVox from a file_NRRD with undefined sizes.\n' ...
          'Returning an empty vector']))
        out = [];
      end;
    end
    
    
    function out = get.nonZeroVoxels(obj)
      % function out = get.nonZeroVoxels(obj)
      %
      % Return list of indices into domain dimensions of voxels with a
      % nonzero value in any of the associated non-domain dimensions.
      %
      if ~strcmpi(obj.sizes,'???')
        domainDims = obj.domainDims;
        sizes   = obj.sizes;
        nVoxels = prod(sizes(domainDims));
        nData   = prod(sizes(~domainDims));
        
        tmp = reshape(obj.data,[nData nVoxels]);
        tmp = sum(tmp,1);
        out = find(tmp~=0);
      else
        warning(sprintf(['Requested nonZeroVox from a file_NRRD with undefined sizes.\n' ...
          'Returning an empty vector']))
        out = [];
      end;
    end
    
    function out = get.nVoxels(obj)
      % function out = get.nVoxels(obj)
      %
      % Get the total number of voxels in the volume.
      if ~strcmpi(obj.sizes,'???')
        domainDims = obj.domainDims;
        out = prod(obj.sizes(domainDims));
      else
        out = [];
      end;
    end
    
    function out = get.isScalar(nrrdIn)
      % function out = isScalar(nrrdIn)
      %
      % Returns a boolean true value if all members of the kinds field are
      % "domain".  Returns boolean false otherwise.      
      out = false;      
      if all(strcmp(nrrdIn.kinds,'domain'));
        out = true;
      end;      
    end
    
    function aspect = getAspect(nrrdIn)
      % function aspect = getAspect(nrrdIn)
      %
      % Get the voxel aspect ratio, computed as the norm of each column of
      % nrrdIn.spacedirections
      aspect = sqrt(sum(nrrdIn.spacedirections.^2,1));
      
    end;
    
    function normalizeVectors(nrrdIn)
      % function normalizeVectors(nrrdIn)
      %
      % Normalize the lengths in a vectorfield
      if strcmpi(nrrdIn.kinds{1},'covariant-vector')
        Norms = sqrt(squeeze(sum(nrrdIn.data.^2,1)));
        
        foo = zeros([3 size(Norms)]);
        foo(1,:,:,:) = Norms;
        foo(2,:,:,:) = Norms;
        foo(3,:,:,:) = Norms;
        Q = find(foo>0);
        nrrdIn.data(Q) = nrrdIn.data(Q)./foo(Q);
      else
        warning('NRRD Is not a vector image');
      end;
    end;
    
    function out = saveobj(obj)
      % We don't actually have to do anything here
      out = obj;
    end
    
    %%
    %% Set and Get methods for properties in the file_NRRDHeader Class
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    function out = get.content(obj)
      out = obj.hdrfile.content;
    end;
    
    function set.content(obj,val)
      obj.hdrfile.content = val;
    end;
    
    function out = get.type(obj)
      out = obj.hdrfile.type;
    end;
    
    function set.type(obj,val)
      obj.hdrfile.type = val;
      obj.datafile.dataType = val;
    end
    
    function out = get.dimension(obj)
      out = obj.hdrfile.dimension;
    end
    
    function set.dimension(obj,val)
      obj.hdrfile.dimension = val;
    end;
    
    function out = get.space(obj)
      out = obj.hdrfile.space;
    end
    
    function set.space(obj,val)
      obj.hdrfile.space = val;
    end
    
    function out = get.sizes(obj)
      out = obj.hdrfile.sizes;
    end
    
    function set.sizes(obj,val)
      obj.hdrfile.sizes = val;
      obj.datafile.sizes = val;
    end
    
    function out = get.endian(obj)
      out = obj.hdrfile.endian;
    end
    
    function set.endian(obj,val)
      obj.hdrfile.endian = val;
    end
    
    function out = get.encoding(obj)
      out = obj.hdrfile.encoding;
    end
    
    function set.encoding(obj,val)
      obj.hdrfile.encoding = val;
    end
    
    function out = get.spaceorigin(obj)
      out = obj.hdrfile.spaceorigin;
    end
    
    function set.spaceorigin(obj,val)
      obj.hdrfile.spaceorigin = val;
    end
    
    function out = get.kinds(obj)
      out = obj.hdrfile.kinds;
    end
    
    function set.kinds(obj,val)
      obj.hdrfile.kinds = val;
    end
    
    function out = get.spacedirections(obj)
      out = obj.hdrfile.spacedirections;
    end;
    
    function set.spacedirections(obj,val)
      obj.hdrfile.spacedirections = val;
    end
    
    function out = get.spaceunits(obj)
      out = obj.hdrfile.spaceunits;
    end
    
    function set.spaceunits(obj,val)
      obj.hdrfile.spaceunits = val;
    end
    
    function out = get.centerings(obj)
      out = obj.hdrfile.centerings;
    end
    
    function set.centerings(obj,val)
      obj.hdrfile.centerings = val;
    end
    
    function out = get.measurementframe(obj)
      out = obj.hdrfile.measurementframe;
    end
    
    function set.measurementframe(obj,val)
      obj.hdrfiles.measurementframe = val;
    end
    
    function out = get.data_fname(obj)
      out = obj.hdrfile.data_fname;
    end
    
    function set.data_fname(obj,filename)
      obj.hdrfile.data_fname = filename;
      obj.datafile.fname = filename;      
    end;
    
    function out = get.fpath(obj)
      out = obj.hdrfile.fpath;
    end
    
    function set.fpath(obj,val)
      obj.hdrfile.fpath = val;
      obj.datafile.fpath = val;
    end
    
    function out = get.fname(obj)
      out = obj.hdrfile.fname;
    end;
    
    function set.fname(obj,filename)
      [~, name, ext] = fileparts(filename);
      if strcmpi(ext,'.nhdr')
        obj.hdrfile.fname = [name '.nhdr'];
        obj.hdrfile.data_fname = [name '.raw'];
        obj.datafile.fname = [name '.raw'];
      else
        error('Filename must end in .nhdr');
      end;
    end
    
    function out = get.existsOnDisk(obj)
      out = obj.hdrfile.existsOnDisk&obj.datafile.existsOnDisk;
    end;
    
    %%
    %% Overloaded set() and get() methods to redirect requests for data into
    %% the datafile object.
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    function out = get.data(obj)
      out = obj.datafile.data;
    end;
    
    function set.data(obj,value)
      obj.datafile.data = value;
      obj.imgRanges = []; %Force recomputation of image ranges when data changes
    end;
    
    function out = get.hasData(obj)
      out = obj.datafile.hasData;
    end;
    
    function out = get.readOnly(obj)
      out = obj.datafile.readOnly;
    end;
    
    function set.readOnly(obj,value)
      obj.datafile.readOnly = value;
    end;
    
    % Push requests to purge data up the line to the datafile
    function purgeData(obj)
      obj.datafile.purgeData;
    end;
    
    function out = get.dataType(obj)
      out = obj.datafile.dataType;
    end;
    
    function set.dataType(obj,val)
      obj.datafile.dataType = val;
    end;
    
    function out = get.saveData(obj)
      out = obj.datafile.saveData;
    end
    
    function set.saveData(obj,val)
      obj.datafile.saveData = val;
    end
    
    DownSample(nrrdIn,downSampleLevel,method);
    nrrdOut = convertToRGB(nrrdIn,fname,fpath);
    %img = GetSlice(nrrdIn,axis,slice,varargin);
    nrrdView = view(obj,varargin);
    nrrdView = threeview(obj,varargin);
    nrrdView = threeViewWithData(obj,data,overlay);
  end
  
  methods (Access=protected)
    function objOut = copyElement(obj)
      objOut = copyElement@matlab.mixin.Copyable(obj);
      
      objOut.hdrfile = obj.hdrfile.copy;
      objOut.datafile = obj.datafile.copy;
      
    end
    
  end
  
  methods (Static=true)
    function out = loadobj(obj)
      % function out = loadobj(obj)
      %
      %
      if ~isa(obj,'file_NRRD')
        mydisp('Object loaded -- Rebuilding file_NRRD');
        % Easier just to totally rebuild the object
        if isfield(obj,'fname')
          % New NRRD object type
          out = file_NRRD(obj.fname,obj.fpath,obj.datafile.readOnly);
        elseif isfield(obj,'filename')
          out = file_NRRD(obj.filename,obj.filepath,obj.readOnly);
          import(out,obj); % Copd fields from old structure over
        end;
      else
        out = obj;
      end;
    end
    
 
  end
  
%   methods (Access=private)
%     
%     function trySplitNRRD(filename)
%     end
%   end
  
  
  
  
end
