function disp(obj)
      % function disp(obj)
      %
      % Overload of display function
      
      disp(['            fname: ' obj.fname]);
      displaypath = obj.fpath;
      if length(displaypath)>35
        displaypath = ['...' displaypath(end-34:end)];
      end;
      disp(['            fpath: ' displaypath]);
      disp(['       data_fname: ' obj.data_fname]);
          
      if ~strcmpi(obj.content,'???')
        disp(['          content: ' obj.content]);    end;
      
      if ~strcmpi(obj.type,'???')
        disp(['             type: ' obj.type]);       end;
      
      if ~strcmpi(obj.dimension,'???')
        disp(['        dimension: ' num2str(obj.dimension)]); end;
      
      if ~strcmpi(obj.space,'???')
        disp(['            space: ' obj.space]);      end;
      
      if ~strcmpi(obj.sizes,'???')
        disp(['            sizes:  [' num2str(obj.sizes) ']']); end;
      
      if ~strcmpi(obj.endian,'???')
        disp(['           endian: ' obj.endian]); end;
      
      if ~strcmpi(obj.encoding,'???')
        disp(['         encoding: ' obj.encoding]); end;
      
      if ~all(isnan(obj.spaceorigin))
        disp(['      spaceorigin:  [ ' num2str(obj.spaceorigin) ' ]']); end;
      
      if ~all(strcmpi(obj.kinds,'???'))
        Str = ['            kinds:  {' ];
        for i = 1:length(obj.kinds)
          Str = [Str ' ''' obj.kinds{i} ''''];
        end;
        Str = [Str ' }'];
        disp(Str); end;
      
      if ~any(isnan(obj.thicknesses))
        disp(['      thicknesses: ' num2str(obj.thicknesses)]); end;
      
      if ~all(isnan(obj.spacedirections))
        Str = num2str(obj.spacedirections);
        disp(['  spacedirections:  [' Str(1,:) ']']);
        for i = 2:size(Str,1)
          disp(['                    [' Str(i,:) ']']);end;
      end;
      
      if ~all(strcmpi('???',obj.spaceunits))
        disp(['       spaceunits: ' obj.spaceunits]); end;
      
      if ~strcmpi(obj.centerings{1},'???')
        disp(['       centerings: ' obj.centerings]); end;
      
      if ~all(isnan(obj.measurementframe(:)))
        Str = num2str(obj.measurementframe);
        disp([' measurementframe: [' Str(1,:) ']']); 
        for i = 2:size(obj.measurementframe,1)
        disp(['                   [' Str(i,:) ']']);
        end;
      end;
                  
end