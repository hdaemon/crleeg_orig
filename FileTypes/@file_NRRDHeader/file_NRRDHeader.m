classdef file_NRRDHeader < file & matlab.mixin.Copyable
  % classdef file_NRRDHeader < file & matlab.mixin.Copyable
  %
  %
  
  properties    
    % Default NRRD header
    content     = '???';
    type        = '???';
    dimension   = '???';
    space       = '???';
    sizes       = '???';
    endian      = '???';
    encoding    = '???';
    spaceorigin = [NaN NaN NaN];
    
    % tensor has to have headerInfo.kinds = [{'3D-masked-symmetric-matrix'} \
    % {'space'} {'space'} {'space'}];
    kinds = [{'???'} {'???'} {'???'}];
    
    thicknesses = NaN; %: don't care for now
    
    % space directions: tensor data is expected to have 10 values (the first
    % one representing 'none', scalar data is expected to have 9 values
    spacedirections = [NaN NaN NaN; NaN NaN NaN; NaN NaN NaN];
    
    % these fields are optional:
    spaceunits = [{'???'} {'???'} {'???'}];
    centerings = [{'???'} {'cell'} {'cell'} {'cell'}];
    measurementframe = [NaN NaN NaN; NaN NaN NaN; NaN NaN NaN];
    modality 
    bvalue
    gradients

    data_fname
  end
  
 
  methods
            
    function obj = file_NRRDHeader(filename,filepath)
      % function obj = file_NRRDHeader(filename,filepath)
      %
      %
      obj = obj@file;
      
      if nargin>0
      if ~exist('filepath','var'), filepath = ''; end;            
      
      obj.validExts = {'.nhdr'};
      obj.fname = filename;
      obj.fpath = filepath;
            
      if isa(filename,'file_NRRDHeader')
        obj = filename;
        return;
      end;
                  
      if ~strcmpi(obj.fext,'.nhdr')
        error('file_NRRDHeader requires a .NHDR file.')
      end;
      
      if obj.existsOnDisk
        read(obj);
      else
        mydisp('Creating New NHDR Object');
        [~, name, ext] = fileparts(filename);
        obj.data_fname = [name '.raw'];
      end;
      end;
    end

    function set.data_fname(obj,filename)
      %
      [~,name,ext] = fileparts(filename);
      if strcmpi(ext,'.raw')
        obj.data_fname = filename;
      else
        error('Data filenames must end in .raw');
      end;
    end;
         
    disp(obj);
    read(nrrdHeader);
    write(nrrdHeader);
    
  end
  
  methods (Access=private)
    readHeader(nrrdHeader);
    writeheader(nrrdHeader);
  end;
      
end