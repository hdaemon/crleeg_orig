% NHDR header reader
% -------------------------------------------------------------------------
% Description:
% 	Write NHDR header and corresponding data in NRRD file
% 
% Usage: 
%   writeHeader( 'dataTest.nhdr', hdrMRI )
% Input:
%   - nhdrFileName: file name of nrrd header (.nhdr)
%   - nrrdHeader: nrrd structure
% 
% Output:
%
% Comments:
% 
% Notes:
%   Writes NRRD structure to file.
% 
% -------------------------------------------------------------------------

function  write( nrrdHeader )

% writes a nhdr file structure to a nhdr file
% can only write raw data for now
%
% writeNrrdStructure( nhdrFileName, nrrdHeader )
%
% marc@bwh.harvard.edu
% kquintus@bwh.harvard.edu
%
% Rewritten in object oriented form by 
%  Damon Hyde (damon.hyde@childrens.harvard.edu)
%


currentDIR = pwd;
cd(nrrdHeader.fpath);

%nrrdWriteData(nrrdHeader);

% [pathstr,matFileName,ext] = fileparts( nhdrFileName );
% 
% bNoPathExists = strcmp(pathstr,'');
% if bNoPathExists
%     nhdrFileName = [matpath,nhdrFileName];
%     [pathstr,matFileName,ext] = fileparts( nhdrFileName );
% end
% 
% if (~strcmp( upper(ext), '.NHDR' ) )
%     nhdrFileName = sprintf('%s.nhdr',nhdrFileName); 
%     [pathstr,matFileName,ext] = fileparts( nhdrFileName );
% end
% 
% % save data in raw format.
% if bNoPathExists
%     nrrdWriteData( nrrdHeader matFileName, nrrdHeader.data, nrrdHeader.type ) 
% else
%     nrrdWriteData( [pathstr,'\',matFileName], nrrdHeader.data, nrrdHeader.type )     
% end

fid = fopen( nrrdHeader.fname, 'w' );

if (fid == -1) 
  fid_test = fopen(nrrdHeader.fname,'r');
  if (fid_test==-1)
  
  fprintf('ABORT: %s does not exist.\n', nrrdHeader.fname );
  keyboard;
  return;
  else
    warning(['File ' nrrdHeader.fname ' exists but appears to be read-only.' ...
             ' Skipping file write.']);
    return;            
  end;
end


fprintf( fid, 'NRRD0004\n' );
fprintf( fid, '# Complete NRRD file format specification at:\n' );
fprintf( fid, '# http://teem.sourceforge.net/nrrd/format.html\n' );
fprintf( fid, 'content: %s\n', nrrdHeader.content);
fprintf( fid, 'type: %s\n', nrrdHeader.type );
fprintf( fid, 'dimension: %d\n', nrrdHeader.dimension );
fprintf( fid, 'space: %s\n', nrrdHeader.space );
fprintf( fid, 'sizes: ' );

for iI=1:length( nrrdHeader.sizes )
  fprintf( fid, '%d', nrrdHeader.sizes(iI) );
  if ( iI~=length( nrrdHeader.sizes ) )
    fprintf( fid, ' ' );
  end
end
fprintf( fid, '\n' );

fprintf( fid, 'space directions: ' );
% if first kind is 3D-masked-symmetric-matrix, assume tensor data.
if strcmp(nrrdHeader.kinds(1), '3D-masked-symmetric-matrix')||...
   strcmp(nrrdHeader.kinds(1), '3D-symmetric-matrix')||...
   strcmp(nrrdHeader.kinds(1), 'RGB-color')||...
   strcmp(nrrdHeader.kinds(1), 'vector')||...
   strcmp(nrrdHeader.kinds(1),'covariant-vector')
    fprintf( fid, 'none ' );
end

sd = nrrdHeader.spacedirections;
fprintf( fid, '(%f,%f,%f) (%f,%f,%f) (%f,%f,%f)\n', ...
 sd(1,1), sd(2,1), sd(3,1), sd(1,2), sd(2,2), sd(3,2), sd(1,3), sd(2,3), sd(3,3) );

fprintf( fid, 'kinds: ' );
for iI=1:length( nrrdHeader.kinds )
  fprintf( fid, '%s', nrrdHeader.kinds{iI} );
  if ( iI~=length( nrrdHeader.kinds ) )
    fprintf( fid, ' ' );
  end
end
fprintf( fid, '\n' );

if any(strcmp(fieldnames(nrrdHeader),'endian'))
  fprintf( fid, 'endian: %s\n', nrrdHeader.endian );
end

% for now encoding is raw since nrrdSave does encoding in raw
%fprintf( fid, 'encoding: %s\n', nrrdHeader.encoding );
fprintf( fid, 'encoding: raw\n');

so = nrrdHeader.spaceorigin;
fprintf( fid, 'space origin: (%f,%f,%f)\n', so(1), so(2), so(3) );

% check if there is a measurement frame
if any(strcmp(fieldnames(nrrdHeader),'measurementframe'))
  if ~all(isnan(nrrdHeader.measurementframe(:)))
    mf = nrrdHeader.measurementframe;
    fprintf( fid, 'measurement frame: (%f,%f,%f) (%f,%f,%f) (%f,%f,%f)\n', ...
        mf(1,1), mf(2,1), mf(3,1), mf(1,2), mf(2,2), mf(3,2), mf(1,3), mf(2,3), mf(3,3) );
  end
end

% check if field thickness is there
if any(strcmp(fieldnames(nrrdHeader),'thicknesses'))
  if ~any(isnan(nrrdHeader.thicknesses))
    fprintf( fid, 'thicknesses: ' );
    for iI=1:length( nrrdHeader.thicknesses )
        fprintf( fid, '%f', nrrdHeader.thicknesses(iI) );
        if ( iI~=length( nrrdHeader.thicknesses ) )
            fprintf( fid, ' ' );
        end
    end
    fprintf( fid, '\n' );
    %thick = nrrdHeader.thicknesses;
    %fprintf( fid, 'thicknesses: %f %f %f\n', thick(1), thick(2), thick(3) );
  end
end

% check if field centerings is there
if any(strcmp(fieldnames(nrrdHeader),'centerings'))  
  if ~strcmpi(nrrdHeader.centerings{1},'???')
    fprintf( fid, 'centerings: ' );
    for iI=1:length( nrrdHeader.centerings )
        fprintf( fid, '%s', char(nrrdHeader.centerings(iI)) );
        if ( iI~=length( nrrdHeader.centerings ) )
            fprintf( fid, ' ' );
        end
    end
    fprintf( fid, '\n' );
    
    %ce = nrrdHeader.centerings;
    %fprintf( fid, 'centerings: %s %s %s\n', char(ce(1)), char(ce(2)), char(ce(3)) );
  end
end

% check if field spaceunits is there
if any(strcmp(fieldnames(nrrdHeader),'spaceunits'))
  if ~all(strcmpi('???',nrrdHeader.spaceunits))
    fprintf( fid, 'space units: ' );
    for iI=1:length( nrrdHeader.spaceunits )
        fprintf( fid, '\"%s\"', char(nrrdHeader.spaceunits(iI)) );
        if ( iI~=length( nrrdHeader.spaceunits ) )
            fprintf( fid, ' ' );
        end
    end
    fprintf( fid, '\n' );
    
    %su = nrrdHeader.spaceunits;
    %fprintf( fid, 'space units: %s %s %s\n', char(su(1)), char(su(2)), char(su(3)) );
  end
end

fprintf( fid, 'data file: %s\n', nrrdHeader.data_fname);

fclose( fid );

cd(currentDIR);