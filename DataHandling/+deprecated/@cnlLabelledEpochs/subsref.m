function varargout = subsref(obj,s)
switch s(1).type
  case '.'
    varargout = {builtin('subsref',obj,s)};
  case '()'           
    if length(s) == 1
      % Implement obj(indices)
      if numel(obj)==1
        if numel(s.subs)==1
          if iscell(s.subs{1})
            outIdx = getIdxFromStringCell(s.subs{1});
            varargout = {cnlLabelledEpochs(obj.data(:,outIdx),obj.labels(outIdx),...
              'epochstart',obj.epochs_start,'epochend',obj.epochs_end)};
          else
            error('Use 2D Indexing with Numeric Values');
          end;                              
        elseif numel(s.subs)==2
          if strcmpi(s.subs{1},':')            
            varargout = {cnlLabelledEpochs(obj.data(:,s.subs{2}),obj.labels(s.subs{2}),...
              'epochstart',obj.epochs_start,'epochend',obj.epochs_end)};
          else                                
         error('To prevent errors in epoch bounds, arbitrary 2D referencing is not permitted');
          end;
        end
      else
        varargout = {obj(s.subs{:})};
      end
    elseif length(s) == 2 && strcmp(s(2).type,'.')
      % Implement obj(ind).PropertyName
      varargout = {builtin('subsref',obj,s)};
    elseif length(s) == 3 && strcmp(s(2).type,'.') && strcmp(s(3).type,'()')
      % Implement obj(indices).PropertyName(indices)
      varargout = {builtin('subsref',obj,s)};
    else
      % Use built-in for any other expression
      varargout = {builtin('subsref',obj,s)};
    end
  case '{}'
    varargout = {builtin('subsref',obj,s)};
%     if length(s) == 1
%       % Implement obj{indices}
%       % Return a cnlLabelledEpochs object with the appropriate epochs
%       % selected
%       if numel(s.subs)==1
%         data = obj.data_epoched;
%         data = data(s.subs{:});
%         varargout = {cnlLabelledEpochs(data,obj.labels)};
%       else
%         error('Not a valid indexing expression');
%       end;
%       
%     elseif length(s) == 2 && strcmp(s(2).type,'.')
%     % Implement obj{indices}.PropertyName
%     varargout = {builtin('subsref',obj,s)};
%     else
%     % Use built-in for any other expression
%     varargout = {builtin('subsref',obj,s)};
%     end
  otherwise
    error('Not a valid indexing expression')
end

  function outIdx = getIdxFromStringCell(cellIn)
    outIdx = zeros(1,numel(cellIn));
    for idx = 1:numel(outIdx)
      tmp = find(strcmp(cellIn{idx},obj.labels));
      assert(numel(tmp)==1,'Invalid Index Labels');
      outIdx(idx) = tmp;
    end
  end

end