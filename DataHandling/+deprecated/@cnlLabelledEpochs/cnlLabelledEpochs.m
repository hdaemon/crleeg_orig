classdef cnlLabelledEpochs < cnlLabelledData
  %  Adds epoch handling functionality to cnlLabeledData
  %
  % classdef cnlLabelledEpochs < cnlLabelledData
  %
  % Written By: Damon Hyde
  % Last Edited: March 2, 2016
  % Part of the cnlEEG Project
  %
  
  properties (GetAccess=public,SetAccess=protected)    
    epochs_start
    epochs_end
  end
  
  properties (Dependent = true)
    nEpochs
    data_epoched
  end
    
  methods
    
    function obj = cnlLabelledEpochs(data,varargin)
      error('This object is deprecated, and functionality rolled into cnlLabelledData');
      obj = obj@cnlLabelledData;
      if nargin>0
        
        % Input Parsing
        p = inputParser;
        p.addRequired('data',@(x) obj.validateData(x)||obj.validateCellData(x));
        p.addOptional('labels',[],@(x) iscellstr(x));
        p.addParamValue('epochstart',[]);
        p.addParamValue('epochend',[]);
        p.addParamValue('epochlen',[]);
        p.parse(data,varargin{:});
        
        % Set Data and Labels
        obj.data = data;
        obj.labels = p.Results.labels;
        
        % Configure Epochs
        start  = p.Results.epochstart;
        finish = p.Results.epochend;
        len = p.Results.epochlen;
        
        if obj.validateData(data)
         [start,finish] = obj.parseEpochs(p.Results.epochstart,p.Results.epochend,p.Results.epochlen);        
         obj.epochs_start = start;
         obj.epochs_end  = finish;
        elseif ~isempty(start)||~isempty(finish)||~isempty(len)
          warning('Provided both cell-epoched data and epoch definitions. Rejecting start/end/len values');
        end;
        
      end
    end
         
    function out = get.data_epoched(obj)
      % function out = dataEpochs(obj)
      %
      % Returns a cell array with each cell containing the data from a
      % single epoch.  If no epochs are defined for the dataset, just
      % returns a single epoch with all of the data in it.
      %
      
        % If epochs haven't been defined, just return a single cell with
        % all the data
        if obj.nEpochs==0   
          out = {obj.data};          
          return;
        end

        % If we have a bunch of epochs defined, output each one in its own
        % cell.
        assert(numel(obj.epochs_start)==numel(obj.epochs_end),...
          'Epochs poorly defined. Number of starting points must match number of ending points');
        out = cell(1,obj.nEpochs);
        tmp = obj.data;
        for idx = 1:obj.nEpochs
          out{idx} = tmp(obj.epochs_start(idx):obj.epochs_end(idx),:);
        end;

    end
        
    function out = get.nEpochs(obj)
      out = numel(obj.epochs_start);
    end;
        
  end
  
  methods (Access=protected)
    function [obj, newdata] = parseData(obj,val)
                                
      assert(obj.validateData(val)||obj.validateCellData(val),...
        ['Input data must either be a numeric matrix, or a cell array '...
         'of numeric matrices with equal second dimensions']);
      
      if obj.validateData(val)
        % Just a Data Matrix
        catepoch = val;    
        obj.epochs_start = 1;
        obj.epochs_end = size(catepoch,1);
      else
        % Epoched Data
        epochCell = val;        
        catepoch = cat(1,epochCell{:});
        % Get the lengths and start/end times for each epoch.        
        epochlengths = reshape(cellfun(@(val)(size(val,1)),epochCell),1,[]);
        starttimes = [1 cumsum(epochlengths(1:end-1))+1];
        endtimes   = cumsum(epochlengths);
        obj.epochs_start = starttimes;
        obj.epochs_end = endtimes;
      end;
      
      % Make sure it fits the criteria for a cnlLabelledData object
      [obj, newdata] = obj.parseData@cnlLabelledData(catepoch);
    end
  end;
  
  methods (Access=private,Static=true)
    
    function isValid = validateCellData(val)
      isValid = iscell(val)&&...
                all(cellfun(@(x) isnumeric(x)&&ismatrix(x), val))&&...
                all(cellfun(@(x) size(x,2), val)==size(val{1},2));
    end
    
    
    function [start finish] = parseEpochs(start,finish,len)
      
      if ~exist('start','var'), start = []; end;
      if ~exist('finish','var'), finish = []; end;
      if ~exist('len','var'), len = []; end;
      
      assert(isempty(finish)||isempty(len),...
        'Can assigned either epoch ends or epoch lengths, but not both');
                        
      assert(isempty(finish)||(numel(start)==numel(finish)),...
        'Number of epoch endpoints must match number of starting points');
      
      assert(isempty(len)||(numel(len)==1)||(numel(len)==numel(start)),...
        'Must provide a single length, or one length per epoch start');
      
      start = start(:)';
      finish = finish(:)';
      
      if ~isempty(len)
        if numel(len)==1, len = len*ones(1,numel(start)); end;
        len = len(:)';
        finish = start+len;      
      end
      
    end
    
    
  end
end
