classdef cnlLinearDecompData < cnlLabelledData
  % Adds a decomposition to cnlLabelledData
  %
  % Written By: Damon Hyde
  % Last Edited: May 25, 2016
  % Part of the cnlEEG Project
  %
  
  properties
    dcMatrices
    dcType
    dcInputs
  end;
  
  properties (Access=private)
    VALID_DECOMPOSITION_TYPES = {'linear'};
  end
  
  methods
    
    function obj = cnlLinearDecompData(data,varargin)
                 
      p = inputParser;
      p.KeepUnmatched = true;
      p.addOptional('data',[],@(x) cnlDecomposedData.validateData(data));
      p.addOptional('labels',[],@(x) isempty(x)||iscellstr(x));
      p.addParamValue('dcType',[]);
      p.addParamValue('dcInputs',[]);
      p.addParamValue('dcMatrices',[]);
      parse(p,data,varargin{:});
      
      obj = obj@cnlLabelledData(p.Results.data,p.Results.labels,p.Unmatched);
      
    end
    
  end
  
end
  