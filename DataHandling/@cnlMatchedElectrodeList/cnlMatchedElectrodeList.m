classdef cnlMatchedElectrodeList
  % classdef cnlMatchedElectrodeList
  %
  % Object class matching two lists of electrode names to one another.
  %
  % function obj = cnlMatchedElectrodeList(Input,Match,varargin)
  %
  % Inputs:
  %    Input: Cell Array of String Labels
  %    Match: Cell Array to Match To
  %    Exclude: List of Labels to Skip
  %
  % Outputs:
  %  
  %
  %
  % Written By: Damon Hyde
  % Last Edited: Mar 4, 2016
  % Part of the cnlEEG Project
  %
    
  properties
    Input
    Match
    Exclude
  end;
  
  properties (Dependent = true)
    Output
    idxInputToOutput
    idxMatchToOutput
  end  
  
  methods
    
    function obj = cnlMatchedElectrodeList(Input,Match,varargin)
      
      if nargin>0
        if isa(Input,'cnlMatchedElectrodeList')
          obj.Input   = Input.Input;
          obj.Match   = Input.Match;
          obj.Exclude = Input.Exclude;
        end;
        
        p = inputParser;
        p.addRequired('Input',@(x) obj.validateList(x));
        p.addRequired('Match',@(x) obj.validateList(x));
        p.addOptional('Exclude',{},@(x) obj.validateList(x));
        
        p.parse(Input,Match,varargin{:});
        
        obj.Input   = p.Results.Input;
        obj.Match   = p.Results.Match;
        obj.Exclude = p.Results.Exclude;
      end;
    end;     
    
    function out = get.Output(obj)
      
      % Return an empty set if the input labels aren't set
      if isempty(obj.Input), out = [];        return; end;
      
      % If the Match labels aren't set, return the same list as the input
      if isempty(obj.Match), out = obj.Input; return; end;
      
      % Otherwise, return all input labels that are in the match set, but not
      % in the exclude set
      out = setdiff(intersect(obj.Input,obj.Match),obj.Exclude);
      
    end
    
    function out = get.idxInputToOutput(obj)
      for idx = 1:numel(obj.Output)
        tmp = find(strcmp(obj.Output{idx},obj.Input));
        assert(numel(tmp)==1,'ERROR - Multiple Matches in List');
        out(idx) = tmp;
      end
    end;
    
    function out = get.idxMatchToOutput(obj)
      if ~isempty(obj.Match)
        for idx = 1:numel(obj.Output)
          tmp = find(strcmp(obj.Output{idx},obj.Match));
          assert(numel(tmp)==1,'ERROR - Multiple Matches in List');
          out(idx) = tmp;
        end;
      else
        out = 1:numel(obj.Output);
      end;
    end
    
  end
  
  methods (Static=true,Access=private)
    function isValid = validateList(val)
      isValid = isempty(val)||iscellstr(val)||...
        isa(val,'function_handle')&&iscellstr(feval(val));
    end
  end
  
end