function out = horzcat(varargin)
% Concatenate cnlLabelledData objects along the electrode dimension
%
% function out = horzcat(varargin)
%
% A few notes on this function:
%  1) There must be no repeated electrode labels across data being
%  concatenated
%  2) All epoch labels are retained, including the full data epoch
%  3) All inputs must have the same number of time samples
%
% Written By: Damon Hyde
% Last Edited: May 31, 2016
% Part of the cnlEEG Project
%


for i = 1:nargin
  assert(isa(varargin{i},'cnlLabelledData'),...
    'Cannot concatenated cnlLabelledData with other object types');
end;

nData = size(varargin{1}.data,1);
outLabels = cell(0);
outData = [];
eStart = [];
eEnd = [];
for i = 1:nargin
  assert(~any(ismember(varargin{i}.labels,outLabels)),...
    'When horizontally concatenating, all electrode labels must be unique');
  assert(size(varargin{i}.data,1)==nData,...
    'All data must have the same time length');
  outLabels = [outLabels varargin{i}.labels];
  outData = [outData varargin{i}.data];
  eStart = [eStart varargin{i}.epochs_start];
  eEnd   = [eEnd   varargin{i}.epochs_end];
end

epochs = [eStart' eEnd'];
epochs = unique(epochs,'rows');

out = cnlLabelledData(outData,outLabels,'epochstart',epochs(:,1),'epochend',epochs(:,2));

keyboard;

end