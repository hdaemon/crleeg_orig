function varargout = subsref(obj,s)
switch s(1).type
  case '.'
    if strcmp(s(1).subs,'plot')
      builtin('subsref',obj,s);
      varargout = {};
    elseif strcmp(s(1).subs,'labels')      
      varargout = {builtin('subsref',obj,s)};
    else
      varargout = {builtin('subsref',obj,s)};
    end;
  case '()'
    if length(s) == 1
      % Implement obj(indices)
      if numel(obj)==1
        % Accessing as obj(a)
        if numel(s.subs)==1
          % If the subsref is a cell array, we're indexing by
          % electrode labels.
          %
          if iscellstr(s.subs{1})|ischar(s.subs{1})
            % Provided a cell array of strings
            outIdx = getIdxFromStringCell(s.subs{1});
            varargout = {cnlLabelledData(obj.data(outIdx,:),obj.labels(outIdx),...
              'epochstart',obj.epochs_start, 'epochend',obj.epochs_end)};
          %elseif ischar(s.subs{1})
          %  % Provided a single character string
          %  outIdx = getIdxFromStringCell({s.subs{1}});
          %  varargout = {cnlLabelledData(obj.data(outIdx,:),obj.labels(outIdx),...
          %    'epochstart',obj.epochs_start, 'epochend',obj.epochs_end)};
          else
            % Poorly defined indexing. Don't allow it.
            error('Use 2D Indexing with Numeric Values');
          end;
        elseif numel(s.subs)==2
          % Accessing as obj(a,b)
          
          % Get Output Electrode Indexing
          if isnumeric(s.subs{1})
            outIdx = s.subs{1};
          elseif isequal(s.subs{1},':')
            outIdx = s.subs{1};
          else
            outIdx = getIdxFromStringCell(s.subs{1});
          end;
          
          % Only allow numeric second dimension indexing
          assert(isnumeric(s.subs{2}),'Second dimension indexing must be numeric');
          
          if ~strcmpi(s.subs{2},':')
            % Discard epoch data if we're not keeping all timepoints
            warning('Arbitrary 2D referencing of cnlLabelledData object. Epoch information is lost');
            varargout = {cnlLabelledData(obj.data(s.subs{:}),...
              obj.labels(s.subs{1}))};
          else            
            % Retain epoch data when keeping all timepoints.
            varargout = {cnlLabelledData(obj.data(outIdx,s.subs{2}),...
              obj.labels(outIdx),'epochstart',obj.epochs_start,...
              'epochend',obj.epochs_end)};
          end;
        elseif numel(s.subs)==3
          if isequal(lower(s.subs{3}),'byepoch')
            
                      % Get Output Electrode Indexing
          if isnumeric(s.subs{1})
            outIdx = s.subs{1};
          elseif isequal(s.subs{1},':')
            outIdx = s.subs{1};
          else
            outIdx = getIdxFromStringCell(s.subs{1});
          end;
          
            epochs = obj.epochs;
            
            for i = 1:numel(epochs)
              epochs{i} = epochs{i}(outIdx,s.subs{2});
            end
            
            varargout = {cnlLabelledData(epochs,obj.labels(outIdx))};
            
            
          else
            error('Invalid indexing expression');
          end
        else
          error('Invalid indexing expression');
        end;
      else
        varargout = {obj(s.subs{:})};
      end;
    elseif length(s) == 2 && strcmp(s(2).type,'.')
      % Implement obj(ind).PropertyName
      if numel(s(1).subs)==1
        tmp = obj.subsref(s(1));
        varargout = {builtin('subsref',tmp,s(2))};
      else
        varargout = {builtin('subsref',obj,s)};
      end;
    elseif length(s) == 3 && strcmp(s(2).type,'.') && strcmp(s(3).type,'()')
      % Implement obj(indices).PropertyName(indices)
      varargout = {builtin('subsref',obj,s)};
    else
      % Use built-in for any other expression
      varargout = {builtin('subsref',obj,s)};
    end
  case '{}'
    
    varargout = {builtin('subsref',obj,s)};
    
  otherwise
    error('Not a valid indexing expression')
end

  function outIdx = getIdxFromStringCell(cellIn)
    assert(iscellstr(cellIn)||ischar(cellIn),'FOOERR');
    if ischar(cellIn), cellIn = {cellIn}; end;
    outIdx = zeros(1,numel(cellIn));
    for idx = 1:numel(outIdx)
      tmp = find(strcmp(cellIn{idx},obj.labels));
      assert(numel(tmp)==1,'Invalid Index Labels');
      outIdx(idx) = tmp;
    end
  end

end


