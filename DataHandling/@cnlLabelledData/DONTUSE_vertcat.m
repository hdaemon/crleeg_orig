function out = vertcat(varargin)
% Concatenate cnlLabelledData objects along the time dimension
%
% function out = vertcat(varargin)
%
% Written By: Damon Hyde
% Last Edited: May 31, 2016
% Part of the cnlEEG Project
%

for i = 1:nargin
  assert(isa(varargin{i},'cnlLabelledData'),...
    'Cannot concatenated cnlLabelledData with other object types');
end;

labels = varargin{1}.labels;

outEpochs = cell(0);
for i = 1:nargin
  assert(isequal(labels,varargin{i}.labels),...
      ['When vertically concatenating cnlLabelledData objects, all ' ...
       'sets of labels must be equal']);
  outEpochs = [outEpochs varargin{i}.data_epoched];
  
end

out = cnlLabelledData(outEpochs,varargin{1}.labels);


end