classdef cnlLabelledData
  % Class for associating labels with columns of a data matrix
  %
  % classdef cnlLabelledData
  %
  % obj = cnlLabelledData(data,labels(optional),varargin);
  %
  % Properties
  %   data          : Ne x Nt Data Matrix
  %   data_epoched  : (Dependant) Ne x 1 cell array of individual epoch data matrices
  %   labels        : Labels for each of the Ne electrodes
  %   epochs_start  : Start, end, and lengths for each assigned epoch
  %   epochs_end
  %   epochs_len
  %   nEpochs       : (Dependant) Number of epochs in the data set
  %   nChan         : (Dependent) Number of channels in the data set.
  %
  % Use obj.setEpochs to set epoch start and end times, as they must be set
  % simultaneously.
  %
  % Written By: Damon Hyde
  % Last Edited: Mar 4, 2016
  % Part of the cnlEEG Project
  %
  
  properties
    description
    data
    labels
    sampleRate
  end
  
  properties (SetAccess=protected,GetAccess=public)
    epochs_start
    epochs_end
  end;
  
  properties (Dependent = true)
    epochs_len
    epochs
    nEpochs        
    nChan
    nSampTotal
    timings
  end
  
  properties (Dependent = true, Hidden)
    nElec;
    data_epoched;
  end;
  
  methods
    function obj = cnlLabelledData(data,varargin)
      % function obj = cnlLabelledData(data,varargin)
      %
      %
      if nargin>0
        
        % Input Parsing
        p = inputParser;
        p.addRequired('data',@(x) obj.validateData(x));
        p.addOptional('labels',[],@(x) isempty(x)||iscellstr(x));
        p.addParamValue('epochstart',[]);
        p.addParamValue('epochend',[]);
        p.addParamValue('epochlen',[]);
        p.addParamValue('desc',[]);
        p.addParamValue('sampleRate',1);
        p.parse(data,varargin{:});
        
        % If we're passed an object, copy the fields and return a new
        % object
        if isa(data,'cnlLabelledData')
          obj.description  = data.description;
          obj.data         = data.data;
          obj.labels       = data.labels;
          obj.epochs_start = data.epochs_start;
          obj.epochs_end   = data.epochs_end;
          obj.sampleRate   = data.sampleRate;
          return;
        end;
        
        % Set the data values. Parsing and error checking done as part of
        % obj.set.data()
        obj.data = data;
        
        %         % Set default labelling
        %         labels = p.Results.labels;
        %         if isempty(labels)&&(size(data,2)>0),
        %           warning('Using default electrode labels');
        %           for i = 1:size(data,2)
        %             labels{i} = ['E' num2str(i)];
        %           end
        %         end;
        
        obj.labels      = p.Results.labels;
        obj.description = p.Results.desc;
        obj.sampleRate  = p.Results.sampleRate;
        
        if obj.validateMatrixData(data)
          % If matrix data was provided, parse the provided epoch times
          obj = obj.setEpochs('start', p.Results.epochstart,...
            'finish',p.Results.epochend, ...
            'length',p.Results.epochlen);
        else
          assert(isempty(p.Results.epochstart)&&...
                 isempty(p.Results.epochend)&&...
                 isempty(p.Results.epochlen),...
                 'Cannot provide both cell-epoched data and epoch definitions');          
        end;
        
      end
    end
    
    function out = get.timings(obj)   
      % Return the time from 
      deltaT = 1/obj.sampleRate;      
      out = deltaT*((1:obj.nSampTotal)-1);      
    end
    
    function obj = set.data(obj,val)
      % function obj = set.data(obj,val)
      %
      [obj, newdata] = obj.parseData(val);
      obj.data = newdata;
    end
    
    function out = get.labels(obj)
      if (~isempty(obj.labels))
        out = obj.labels;
        return;
      elseif (isempty(obj.labels)&&size(obj.data,1)>0)
        for i = 1:size(obj.data,1)
          out{i} = ['E' num2str(i)];
        end;
        return;
      else
        out = [];
      end
    end
    
    function obj = set.labels(obj,val)
      % function obj = set.labels(obj,val)
      %
      % Ensure that labels are a cell array of strings, and that the number
      % of labels matches the number of columns in the data matrix
      assert(isempty(val)||iscellstr(val),'Labels must be a cell array of strings');
      if ~isempty(obj.data)&&~isempty(val)
        assert(numel(val)==size(obj.data,1),...
          'Number of labels must match size of first dimension of data');
      end;
      if ~isempty(val)
        obj.labels = obj.parse_Labels(val);
      else
        obj.labels = [];
      end;
    end
    
    function out = get.epochs(obj)
      % function out = dataEpochs(obj)
      %
      % Returns a cell array with each cell containing the data from a
      % single epoch.  If no epochs are defined for the dataset, just
      % returns a single epoch with all of the data in it.
      %
      
      % If epochs haven't been defined, just return a single cell with
      % all the data
      if obj.nEpochs==0
        out = {obj.data};
        return;
      end
      
      % If we have a bunch of epochs defined, output each one in its own
      % cell.
      assert(numel(obj.epochs_start)==numel(obj.epochs_end),...
        'Epochs poorly defined. Number of starting points must match number of ending points');
      out = cell(1,obj.nEpochs);
      tmp = obj.data;
      for idx = 1:obj.nEpochs
        out{idx} = tmp(:,obj.epochs_start(idx):obj.epochs_end(idx));
      end;
      
    end
    
    function out = get.data_epoched(obj)
      warning('data_epoched is deprecated. Use obj.epochs instead');
      out = obj.epochs;
    end;
    
    function varargout = plot(obj)
      p = uitools.plots.dataexplorer(obj.data',obj.labels);
      p.units = 'normalized';
      if nargout>0
        varargout{1} = p;
      end;
    end
    
    function out = get.epochs_len(obj)
      out = obj.epochs_end-obj.epochs_start+1;
    end;
        
    function out = get.nEpochs(obj)
      % function out = get.nEpochs(obj)
      out = numel(obj.epochs_start);
    end;
    
    function out = get.nSampTotal(obj)
      out = size(obj.data,2);
    end;
    
%     function outIdx = getIdxFromStringCell(obj,cellIn)
%       % Give an input cell string array, output the numeric indices of the
%       % corresponding electrodes
%       
%      outIdx = zeros(1,numel(cellIn));
%      for idx = 1:numel(outIdx)
%        tmp = find(strcmp(cellIn{idx},obj.labels));
%        assert(numel(tmp)==1,'Invalid Index Labels');
%        outIdx(idx) = tmp;
%      end
%    end
%     

      
    function out = get.nChan(obj)
      out = size(obj.data,1);
    end;
    
    function out = get.nElec(obj)
      warning('nElec is deprecated. Use nChan instead');
      out = obj.nChan;
    end;
    
    function obj = setEpochs(obj,varargin)
      % Set epoch boundaries
      %
      % obj = SETEPOCHS(obj,varargin);
      %
      % Param-Value Inputs:
      %  'start'  :  Start time of each epoch
      %  'finish' :  End time of each epoch
      %  'length' :  Length of each epoch
      %
      % Define either 'finish' or 'length', but not both.
      %
      
      p = inputParser;
      p.addParamValue('start',[]);
      p.addParamValue('finish',[]);
      p.addParamValue('length',[]);
      p.parse(varargin{:});
      
      [start finish] = obj.validateEpochs(p.Results.start,p.Results.finish,...
        p.Results.length);
      
      startInRange  = ~any((start<0)|(start>obj.nSampTotal));
      finishInRange = ~any((finish<0)|(finish>obj.nSampTotal));
      
      assert(startInRange&&finishInRange,...
        'Epoch start and/or finish outside data range');
      
      if ~isempty(start)
        obj.epochs_start = round(start);
        obj.epochs_end = round(finish);
      else
        % Default single epoch data.
        obj.epochs_start = 1;
        obj.epochs_end = obj.nSampTotal;
      end;
      
    end
    
    
  end
  
  methods (Access=protected)
    
    function [obj, newdata] = parseData(obj,val)
      % function [obj, newdata] = parseData(obj,val)
      %
      % Ensure that data is a numeric matrix, with the same number of
      % columns as there are electrode labels.
      assert(obj.validateData(val),...
        ['Input data must either be a numeric matrix, or a cell array '...
        'of numeric matrices with equal second dimensions']);
      
      if obj.validateMatrixData(val)
        % Matrix Data Input
        % Set a single initial epoch
        catepoch = val;
        obj.epochs_start = 1;
        obj.epochs_end = size(val,2);
      elseif obj.validateCellData(val)
        % Cell Data Input - Epoched Data
        epochCell = val;
        catepoch = cat(2,epochCell{:});
        % Get the lengths and start/end times for each epoch.
        epochlengths = reshape(cellfun(@(val)(size(val,2)),epochCell),1,[]);        
        starttimes = [1 cumsum(epochlengths(1:end-1))+1];
        endtimes   = cumsum(epochlengths);
        obj.epochs_start = starttimes;
        obj.epochs_end = endtimes;
      else
        error('Should never get here');
      end;
      
      if ~isempty(obj.labels)
        assert(size(catepoch,1)==numel(obj.labels),...
          'First dimension of data matrix does not match number of electrodes');
      end
      newdata = catepoch;
    end
    
  end
  
  methods (Access=protected,Static=true)
    function isValid = validateData(val)
      % function isValid = validateData(val)
      %
      isValid = cnlLabelledData.validateMatrixData(val)|| ...
        cnlLabelledData.validateCellData(val);
    end
    
    function isValid = validateMatrixData(val)
      % function isValid = validateMatrixData(val)
      %
      isValid = isnumeric(val)&&ismatrix(val);
    end;
    
    function isValid = validateCellData(val)
      % function isValid = validateCellData(val)
      %
      isValid = iscell(val)&&...
        all(cellfun(@(x) isnumeric(x)&&ismatrix(x), val))&&...
        all(cellfun(@(x) size(x,1), val)==size(val{1},1));
    end
    
    function [start, finish] = validateEpochs(start,finish,len)
      % function [start, finish] = validateEpochs(start,finish,len)
      %
      
      if ~exist('start','var'), start = []; end;
      if ~exist('finish','var'), finish = []; end;
      if ~exist('len','var'), len = []; end;
      
      assert(isempty(finish)||isempty(len),...
        'Can assigned either epoch ends or epoch lengths, but not both');
      
      assert(isempty(finish)||(numel(start)==numel(finish)),...
        'Number of epoch endpoints must match number of starting points');
      
      assert(isempty(len)||(numel(len)==1)||(numel(len)==numel(start)),...
        'Must provide a single length, or one length per epoch start');
      
      % Assumes Starts and Finishes were provided
      start = start(:)';
      finish = finish(:)';
      
      % If Lengths were provided, replace the currently empty finish values
      % with them.
      if ~isempty(len)
        if numel(len)==1, len = len*ones(1,numel(start)); end;
        len = len(:)';
        finish = start+len-1;
      end
      
    end
    
  end
  
  methods (Static = true, Access=protected);
    
    function labelsOut = parse_Labels(labelsIn)
      %  function labelsOut = parse_Labels(labelsIn)
      %
      % Give a cell array of strings containing electrode names, parses them
      % appropriately so that:
      %
      %   For a 128 lead EGI system,
      
      %% Input Error Checking
      assert(isvector(labelsIn),'Input labels must be a vector');
      assert(iscellstr(labelsIn),'Input labels must be a cell array of strings');
      
      %% Parse Labels
      labelsOut = cell(1,length(labelsIn));
      for idxLabel = 1:length(labelsIn)
        currLabel = labelsIn{idxLabel};
        
        % Strip off leading info
        if length(currLabel)>=4
          if strcmp(currLabel(1:4),'EEG ')
            currLabel = currLabel(5:end);
          elseif strcmp(currLabel(1:2),'E ')
            currLabel = currLabel(3:end);
          elseif strcmp(currLabel(1:2),'P ')
            currLabel = currLabel(3:end);
          elseif strcmp(currLabel(1:4),'POL ')
            currLabel = currLabel(5:end);
          end;
        end;
        
        % Strip tailing info
        if length(currLabel)>=4
          if strcmp(currLabel(end-3:end),'-Ref')
            currLabel = currLabel(1:end-4);
          elseif strcmp(currLabel(end-3:end),'-COM')
            currLabel = currLabel(1:end-4);
          elseif strcmp(currLabel(end-2:end),'-Cz')
            currLabel = currLabel(1:end-3);
          end;
        end;
        
        labelsOut{idxLabel} = currLabel;
        
      end;
    end
    
  end
  
end

