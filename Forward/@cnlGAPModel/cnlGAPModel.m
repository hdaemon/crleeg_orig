classdef cnlCEModel < cnlElectrodeModel
  
  methods
    
    function out = cnlCEModel()
    end
  end
  
  methods (Static)
    
    function nrrdObj = modifyConductivity(Electrodes,nrrdObj)
      % The Complete Electrode Model removes electrode voxels from the
      % computational domain and replaces them with boundary conditions
      %
      assert(isa(Electrodes,'cnlElectrodes'),...
        'Input electrode array must be a cnlElectrodes object');
            
      for idxE = 1:numel(Electrodes)
        nrrdObj.data(Electrodes(idxE).Voxels) = 0;
      end;                  
    end
    
    function matOut = modifyFDMatrix(Electrodes,matIn)
      % The complete electrode model adds a row and column to the FD matrix
      % for each individual electrode.
      %
      matOut = matIn;
      return;
      [row,col,val] = find(matIn);
      
      nNodes = size(matIn,1);
      nElec = Electrodes.nElec;
      
      nodes = Electrodes.Nodes;
      
      for idxE = 1:nElec        
        eNodes = nodes{idxE};
        
        next = nNodes + idxE;                
        
        newVals = -1./(Electrodes(idxE).Impedance*ones(numel(eNodes),1));
        
        row = [row ; next*ones(numel(eNodes),1)];
        col = [col ; eNodes(:)];
        val = [val ; newVals];
        
        row = [row ; eNodes(:)];
        col = [col ; next*ones(numel(eNodes),1)];
        val = [val ; newVals];        
        
        row = [row ; next];
        col = [col ; next];
        val = [val ; -sum(newVals)];
        
      end
      
      matOut = sparse(row,col,val,nNodes+nElec,nNodes+nElec);
    end
    
    function currents = getCurrents(Electrodes,ModelSize,AnodeIdx,CathodeIdx)
      % In the Complete Electrode Model, currents are applied at the
      % auxilliary nodes added by modifyFDMatrix
      %
      currents = zeros(prod(ModelSize),1);
      
      nodesAnode = Electrodes.Nodes{AnodeIdx};
      nodesCathode = Electrodes.Nodes{CathodeIdx};
      
      currents(nodesAnode) = 1/numel(nodesAnode);
      currents(nodesCathode) = 1/numel(nodesCathode);
                  
    end
    
    
  end
  
end