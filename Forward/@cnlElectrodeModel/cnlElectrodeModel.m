classdef (Abstract) cnlElectrodeModel
  % Abstract class for electrode models
  % 
  % This is an abstract class meant to act as the parent for incorporating
  % various types of electrode models into the FD forward model. Two
  % abstract functions need to be defined:
  %
  % modifyConductivity : Modify the conductivity NRRD to incorporate the
  %                       electrodes
  % modifyFDMatrix : Add columns/rows to the FD Matrix to 
  %
  % NOTE: Not sure if an abstract class like this is absolutely necessary
  %
  % Written By: Damon Hyde
  % Last Edited: Aug 1, 2016
  % Part of the cnlEEG Project
  %
  
  methods
    
    function obj = cnlElectrodeModel()
      % Empty constructor
    end
    
  end
  
  methods (Abstract,Static)
    nrrdOut = modifyConductivity(Electrodes,nrrdIn);
    matOut  = modifyFDMatrix(Electrodes,matIn);
    currents = getCurrents(Electrodes,ModelSize,Anode,Cathode);
  end
  
end
    
    
    
    
    
  
  