classdef cnlPEModel < cnlElectrodeModel
  
  methods
    
    function out = cnlPEModel()
    end;
    
  end
  
  methods (Static)
    function nrrdOut = modifyConductivity(~,nrrdIn)
      % Point Electrode Models don't need to modify the conductivity
      nrrdOut = nrrdIn;
    end
    
    function matOut = modifyFDMatrix(~,matIn)
      % Point Electrode Models don't need to modify the FD Matrix
      matOut = matIn;
    end
    
    function currents = getCurrents(Electrodes,ModelSize,AnodeIdx,CathodeIdx)
      % Given a list of electrodes in the model, output the appropriate
      % current input pattern for solving the 
      
      assert(isa(Electrodes,'cnlElectrodes'),...
        'Input electrode array must be a cnlElectrodes object');
      
      currents = zeros(prod(ModelSize),1);
      
      if AnodeIdx~=CathodeIdx
        currents(Electrodes(AnodeIdx).Nodes)   = 1;
        currents(Electrodes(CathodeIdx).Nodes) = -1;
      end;
             
    end
  end
  
end
