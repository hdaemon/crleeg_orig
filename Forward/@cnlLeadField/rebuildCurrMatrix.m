function LeadField = rebuildCurrMatrix(LeadField)
% function LeadField = rebuildCurrentMatrix(LeadField)
%
% Based on the options set in LeadField.isConstrained,
% LeadField.useElectrodes, and LeadField.matWeighting, rebuilds
% Leadfield.currMatrix from scratch.
%
% If LeadField.disableRebuild is set to true, this assigns an empty matrix
% to LeadField.currMatrix, which should be sufficient to trigger an error
% if you later attempt to use it before rerunning the rebuild.
%
% Written By: Damon Hyde
% Last Edited: Feb 3, 2016
% Part of the cnlEEG Project
%

if (~LeadField.disableRebuild)
  mydisp('STARTING Rebuild of Current Leadfield Matrix');
end;

% Use cnlMatrixOnSpace to deal with moving to the current solution space.
LeadField = LeadField.rebuildCurrMatrix@cnlMatrixOnSpace;

% Check if build needs to be terminated
if isempty(LeadField.currMatrix), 
  %mydisp('cnlMatrixOnSpace returned an empty matrix. Terminating rebuild.');
  return; 
end; 

% Set the list of electrodes to use
if ~isempty(LeadField.useElectrodes)
  mydisp('Retaining only usable electrodes');
  LeadField.currMatrix = LeadField.currMatrix(LeadField.useElectrodes,:);
else
  mydisp('Using all available electrodes');
end;

% Renormalize columns if necessary
%LeadField.matWeighting = LeadField.rebuildWeightingMatrix;
if LeadField.isWeighted
  mydisp('Weighting Leadfield');
  LeadField.currMatrix = LeadField.currMatrix*LeadField.matWeighting;
else
  mydisp('Using Unweighted LeadField');
end;

% Apply average reference if the appropriate flag is set
if LeadField.useAvgReference
  A = LeadField.currMatrix;
  LeadField.currMatrix = A - repmat(mean(A,1),size(A,1),1);
end

mydisp('COMPLETED Rebuild of Current Leadfield Matrix');

end