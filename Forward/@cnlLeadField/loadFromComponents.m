function LeadField = loadFromComponents(LeadField,varargin)
  
  
  p = inputParser;
  p.addRequired('origMatrix',@(x) ismatrix(x));
  p.addRequired('origSolSpace',@(x) isa(x,'cnlSolutionSpace'));
  p.addOptional('currSolSpace',[],@(x) isa(x,'cnlSolutionSpace'));
  p.addParamValue('isTransposed',false, @(x) islogical(x));
  p.addParamValue('fname',[],@(x) ischar(x));
  p.addParamValue('fpath',[],@(x) ischar(x));
  p.addParamValue('Electrodes',[],@(x) isa(x,'cnlElectrodes'));
  p.addParamValue('gndElectrode',[],@(x) isscalar(x)||ischar(x));
  p.addParamValue('useAvgReference',false,@(x) islogical(x));
  p.addParamValue('isWeighted',false,@(x) isweighted(x));
  p.addParamValue('matWeighting',[],@(x) ismatrix(x));
  p.addParamValue('colPerVox',3,@(x) isnumeric(x)&&isscalar(x));
  p.addParamValue('useElectrodes',[]);
  
  parse(p,varargin{:});
  
  LeadField.isTransposed = p.Results.isTransposed;
  LeadField.fname        = p.Results.fname;
  LeadField.fpath        = p.Results.fpath;
  LeadField.Electrodes   = p.Results.Electrodes;
  LeadField.gndElectrode = p.Results.gndElectrode;
  LeadField.useAvgReference = p.Results.useAvgReference;
  LeadField.isWeighted    = p.Results.isWeighted;
  LeadField.matWeighting  = p.Results.matWeighting;
  LeadField.colPerVox     = p.Results.colPerVox;
  LeadField.useElectrodes = p.Results.useElectrodes;  
  
  LeadField.origMatrix   = p.Results.origMatrix;
  LeadField.origSolutionSpace = p.Results.origSolSpace;
  if ~isempty(p.Results.currSolSpace)
  LeadField.currSolutionSpace = p.Results.currSolSpace;
  else
    LeadField.currSolutionSpace = p.Results.origSolSpace;
  end

  
  
  
end