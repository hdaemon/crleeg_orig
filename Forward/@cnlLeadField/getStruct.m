function S = getStruct(LeadField)
  % Convert a cnlLeadFIeld object to a struct
  %
  % Usage: LeadField.writeAsStruct
  %
  % Converts the properties of the leadfield to a structure 
  %
  % Written By: Damon Hyde
  % Last Edited: Aug 22, 2016
  % Part of the cnlEEG Project
  %
  
  S = getStruct@cnlMatrixOnSpace(obj);
  S.isWeighted      = LeadField.isWeighted;
  S.useElectrodes   = LeadField.useElectrodes;
  S.fname           = LeadField.fname;
  S.fpath           = LeadField.fpath;
  S.Electrodes      = LeadField.Electrodes;
  S.gndElectrode    = LeadField.gndElectrode;
  S.useAvgReference = LeadField.useAvgReference;
end