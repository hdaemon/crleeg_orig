function weightMatrix = rebuildWeightingMatrix(LeadField)
% Rebuild weighting matrix to normalized leadfield columns
%
% function weightMatrix = REBUILDWEIGHTINGMATRIX(LeadField)
%
% Rebuild the weight matrix using LeadField.currMatrix.  The
% resulting weight matrix is constructed to normalize the columns of
% the leadfield.  If the leadfield is uses unconstrained dipole
% orientations, it weights the voxels by the total norm of all three
% directions, rather than each direction individually.
%
% Written By: Damon Hyde
% Last Edited: Feb 3, 2016
% Part of the cnlEEG Project
%

mydisp('Starting rebuild of Leadfield weighting matrix');

% Get column norms
Omega = sum(LeadField.currMatrix.^2,1);

% Sum across columns if we're collapse
if ~LeadField.isCollapsed
  nCols = LeadField.colPerVox;
  Omega = reshape(Omega,[nCols length(Omega)/nCols]);
  Omega = sum(Omega,1);
end;

Omega = sqrt(Omega);

% Get Omega Inverse
OmegaInv = 1./Omega;
OmegaInv(Omega==0) = 0;

weightMatrix = sparse(1:length(Omega),1:length(Omega),OmegaInv);

if ~LeadField.isCollapsed  
  weightMatrix = kron(weightMatrix,speye(LeadField.colPerVox));
end;

mydisp('Completed rebuild of Leadfield weighting matrix');

end