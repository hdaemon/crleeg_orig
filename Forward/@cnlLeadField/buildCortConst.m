function matCortConst = buildCortConst(obj)
  % Build cortical constraint matrix
  %
  % function matCortConst = BUILDCORTCONST(obj)
  %
  % There are several assumptions that this function is making:
  %   1) It assumes that vecNRRD and obj.origSolutionSpace are defined on
  %   the same grid. If this isn't the case, things might complete, but are
  %   going to be very wrong. 
  %   2) ???
  %
  % 
  %
  % Written By: Damon Hyde
  % Last Edited: Feb 3, 2016
  % Part of the cnlEEG Project
  %

  % Make sure vecNRRD is defined.
  if isempty(obj.vecNRRD), matCortConst = []; return; end;
  
  gridObj = cnlGridSpace(obj.origSolutionSpace);
  gridNRRD = obj.vecNRRD.gridSpace;
  
  assert(gridObj==gridNRRD,'Gridspaces of matrix and vecNRRD do not match');
   
  voxels = obj.origSolutionSpace.Voxels;
  totPoints = length(voxels);
  rowidx = repmat(3*( (1:totPoints) -1)',1,3)' + repmat([1 2 3],totPoints,1)';
  colidx = repmat((1:totPoints)',1,3)';
  data   = obj.vecNRRD.data(:,voxels);
  
  matCortConst = sparse(rowidx(:),colidx(:),data(:));
end

% Updates to be added:
%
% Incorporate interpolation between the solutionspace of the vecNRRD and
% the solutionspace used by the leadfield. 
%
% Add error checking to only allow computation for voxelized solution
% spaces (ie: Don't let this run if it's a parcellation solution space)
%