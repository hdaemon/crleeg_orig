classdef cnlLeadField < cnlMatrixOnSpace
  % Object class for EEG Leadfields
  %
  % classdef cnlLeadField < cnlMatrixOnSpace < cnlAdaptableMatrix
  %
  % There are three ways to call the cnlLeadField constructor:
  %   1) Provide a structure with all the necessary options, and a
  %   leadfield will be computed using cnlFDModel.compute_LeadField
  %       USAGE: LField = cnlLeadField(opts);
  %   2) Provide a file name and path to a leadfield previously saved as a
  %   struct.
  %       USAGE: LField = cnlLeadField(fname,fpath);
  %   3) Provide the components individually.
  %
  % Properties:
  %
  % Inherited:
  % origMatrix = [];          cnlMatrixOnSpace > cnlAdaptableMatrix
  % currMatrix = [];          cnlMatrixOnSpace > cnlAdaptableMatrix
  % isTransposed = false;     cnlMatrixOnSpace > cnlAdaptableMatrix
  % origSolutionSpace;        cnlMatrixOnSpace
  % currSolutionSpace;        cnlMatrixOnSpace
  % colPerVox = 1;            cnlMatrixOnSpace
  %
  % New:
  % isWeighted
  % matWeighting
  % useElectrodes
  % useAvgReference
  % vecNRRD
  % fname
  % fpath
  % Electrodes
  % gndElectrode
  %
  %
  % Written By: Damon Hyde
  % Last Edited: Aug 22, 2016
  % Part of the cnlEEG Project
  %
  
  properties
    % Inherited:
    % origMatrix = [];          cnlMatrixOnSpace > cnlAdaptableMatrix
    % currMatrix = [];          cnlMatrixOnSpace > cnlAdaptableMatrix
    % isTransposed = false;     cnlMatrixOnSpace > cnlAdaptableMatrix
    % origSolutionSpace;        cnlMatrixOnSpace
    % currSolutionSpace;        cnlMatrixOnSpace
    % colPerVox = 1;            cnlMatrixOnSpace
    
    % Options for Building currMatrix
    isWeighted    = false;
    matWeighting  = [];
    useElectrodes = [];
    useAvgReference = false;
    vecNRRD
    
    % Location of the stored leadfield matrix
    fname = 'LeadField.mat';
    fpath = [];
  end
  
  properties (GetAccess=public,SetAccess=protected)
    % Electrode Information
    %  - Protected because these should only be set through a call to the
    %  constructor.
    
    Electrodes = [];
    measElectrode;
    gndElectrode = 80;
  end
  
  properties (Dependent=true)
    gndNode
  end;
  
  methods
    % LeadField Object Constructor
    function LeadField = cnlLeadField(varargin)
      %
      % There are two ways of calling this constructor.  The first is to
      % give it a FDModel, template NRRD, and a list of electrodes and
      % modelOptions.  The leadfield will then be built.
      %
      % The second is to give it
      %
      
      
      % Create an empty cnlMatrixOnSpace object
      LeadField = LeadField@cnlMatrixOnSpace;
      
      
      if nargin==1
        % Given a set of input options, compute the leadfield. Note that
        % this ALWAYS computes things. If you want to load from a file,
        % that needs to be specified in the call to the constructor. It
        % should not be the responsibility of a cnlLeadField object to keep
        % track of this. That's what cnlModel is for.
        warning('This method of constructing a LeadField is supported only for backwards compatibility. It will be deprecated in the near future.');
        LeadField = loadFromOptions(LeadField,varargin{1});                
      elseif nargin>=2
        if ( (nargin==2) && (ischar(varargin{1})) && (ischar(varargin{2})) )
          % Construct was provided with a filename and file path. Try
          % loading that file as an object and then constructing the
          % leadfield.
          load([varargin{1} '/' varargin{2}]);
          LeadField = LeadField.loadFromStruct(LFieldStruct);
        else
          % The components of the leadfield are being provided
          % individually. Parse them and construct the object.
          LeadField = LeadField.loadFromComponents(varargin{:});
        end;
        
      end
      
      % NOTE: We used to compute the dipole orientation constraints here.  This has been changed
      %  so that now it must be called separately from the cnlLeadField
      %  constructor
      %           % Get Dipole Orientation Constraints
      %           if modelOptions.useConstrainedCortex
      %             % Probably not a bad idea to just compute this as a matter of course...
      %             LeadField.matCortConst = build_CorticalConstraintMatrix(NRRDS,dirs,modelOptions,LeadField.origSolutionSpace);
      %           end;
      
    end;
    
    
    % Change the current solution space and rebuild the leadfield
    
    %% Overloaded Functions
    
    %     % Get gndElectrode and gndNode from LeadField.Electrodes
    %     function out = get.gndElectrode(LeadField)
    %       if isfield(LeadField.Electrodes,'GndIdx')
    %         out = LeadField.Electrodes.GndIdx;
    %       else
    %         out = [];
    %       end;
    %     end
    
    function out = get.gndNode(LeadField)
      if ~isempty(LeadField.Electrodes)
        out = LeadField.Electrodes.Nodes(LeadField.gndElectrode);
      else
        out = [];
      end;
    end;
    
    function obj = set.useElectrodes(obj,idxToUse)
      if ~isequal(obj.useElectrodes,idxToUse)
        obj.useElectrodes = idxToUse;
        obj = obj.rebuildCurrMatrix;
      end;
    end;
    
    function obj = set.isWeighted(obj,isWeighted)
      if ( obj.isWeighted~=isWeighted)
        obj.isWeighted = isWeighted;
        obj = obj.rebuildCurrMatrix;
      end;
    end;
    
    function obj = set.useAvgReference(obj,flag)
      if ( obj.useAvgReference ~= flag )
        obj.useAvgReference = flag;
        obj = obj.rebuildCurrMatrix;
      end;
    end
    
    function obj = set.vecNRRD(obj,val)
      if ~isa(val,'file_NRRD'),
        error('obj.vecNRRD must be a file_NRRD');
      end
      obj.vecNRRD = val;
      obj.matCollapse = obj.buildCortConst;
    end;
    
    %% Methods with their own m-files
    obj = rebaseOnCurrent(obj);
    LeadField    = build(LeadField,FDModel,nrrdSeg,parallelComp,forceRebuild);
    weightMatrix = rebuildWeightingMatrix(LeadField);
    LeadField    = rebuildCurrMatrix(LeadField);
    matCortConst = buildCortConst(obj);
    weightMatrix = getWeightingMatrix(obj,wantInverse);
    
    %% Methods for Saving/Reloading
    function S = saveobj(obj)
      S = obj.getStruct;
    end
    
    function S = getStruct(obj)
      S = getStruct@cnlMatrixOnSpace(obj);
      S.isWeighted = obj.isWeighted;
      S.useElectrodes = obj.useElectrodes;
      S.fname = obj.fname;
      S.fpath = obj.fpath;
      S.Electrodes = obj.Electrodes;
      S.gndElectrode = obj.gndElectrode;
      S.useAvgReference = obj.useAvgReference;
    end;
    
    function S  = saveToFile(obj)
    end
    
    function obj = loadFromStruct(obj,S)
      obj = loadFromStruct@cnlMatrixOnSpace(obj,S);
      obj.isWeighted = S.isWeighted;
      obj.useElectrodes = S.useElectrodes;
      obj.fName = S.fname;
      obj.fpath = S.fpath;
      obj.Electrodes = S.Electrodes;
      obj.gndElectrode = S.gndElectrode;
      obj.useAvgReference = S.useAvgReference;
    end
    
  end
  
  %% Static Methods
  methods (Static=true)
    function obj = loadobj(S)
      obj = cnlLeadField;
      obj.disableRebuild = true;
      obj = loadFromStruct(obj,S);
      obj.disableRebuild = false;
    end
  end
end