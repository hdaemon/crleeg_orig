
function weightMatrix = getWeightingMatrix(obj,wantInverse)
  % Get the column normalizing matrix, or its inverse
  %
  % function weightMatrix = GETWEIGHTINGMATRIX(obj,wantInverse)
  %
  % Written By: Damon Hyde
  % Last Edited: Feb 3, 2016
  % Part of the cnlEEG Project
  %
  
  obj.matWeighting = rebuildWeightingMatrix(obj);
  
  if ~exist('wantInverse','var'), wantInverse = false; end;
  
  if wantInverse
    weightMatrix = obj.matWeighting;
  else
    [i,j,s] = find(obj.matWeighting);
    [m,n] = size(obj.matWeighting);
    weightMatrix = sparse(i,j,1./s,m,n);
  end;

end