function [base, orig, final] = getSolutionSpaces(LField,nrrdTemplate,pre,post)
% function [base, orig, final] = getSolutionSpaces(LField)
%
% Given a fully configured cnlLeadfield, computes the base solution space,
% the original solution space that the leadfield is converted to before
% being saved to disk, and the final solution space, which the leadfield is
% the value returned by the build() method.

% Get the space the FDModel is solved on
%solVox = find(nrrdTemplate.data>0); % Hopefully this is a segmentation we've been sent
mydisp('Computing base solution space of FDModel');
base = cnlSolutionSpace(nrrdTemplate,1:prod(nrrdTemplate.sizes),'All voxels in space');

% This is the solution space the leadfield is converted to before
% being saved out as LeadField.mat in the appropriate directory.
mydisp('Computing solution space for saved leadfield');
tmp = clone(nrrdTemplate);
tmp.downSample(pre,'segmentation');
orig = cnlSolutionSpace(tmp,find(tmp.data>0),'All voxels in head at downsampled resolution');

% This is the solution space that the final leadfield is output as.
mydisp('Computing final leadfield solution space');
tmp2 = clone(tmp);
tmp2.downSample(post,'segmentation');
final = cnlSolutionSpace(tmp2,find(tmp2.data>0),'All voxels in head at final resolution');


end