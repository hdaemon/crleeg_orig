
function obj = rebaseOnCurrent(obj)
  % Convert leadfield origSolutionSpace to be currSolutionSPace
  %
  % function obj = REBASEONCURRENT(obj)
  %
  % Take the current obj, rebuild the weighting matrix according
  % to the current solution space, and then make the obj.currSolSpace
  % the original solution space.
  %
  % NOTE: This is not currently the most stable function. The process of
  % rebasing is implicitly built on fully voxelized spaces, and will return
  % strange results (if not fail entirely) if you try it with something
  % else. There is currently little error checking to ensure that
  % everything is working properly.
  %
  % Written By: Damon Hyde
  % Last Edited: Feb 3, 2016
  % Part of the cnlEEG Project
  %
  
  mydisp('Starting Rebasing of Leadfield on Current Matrix and Solution Space');
  
  % Disable Matrix Rebuilding, turn off weighting and dipole constraints,
  % then rebuild the matrix. This saves a couple of rebuilds.
  obj.disableRebuild = true;   
  obj.isCollapsed = false;
  obj.isWeighted = false;    
  obj.disableRebuild = false;
 
  % Make current matrix the original.
  obj.origMatrix        = obj.currMatrix;
  obj.origSolutionSpace = obj.currSolutionSpace;
  obj.matCollapse       = obj.buildCortConst;
  
  mydisp('Completed Rebasing of Leadfield');
end