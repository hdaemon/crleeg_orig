function LeadField = loadFromOptions(LeadField,opts)
  % Given an options structure, construct a leadfield
  
  % Set output info
  LeadField.fname = opts.fName_LeadField;
  LeadField.fpath = opts.dir_Model;
  
  % Set electrode info
  LeadField.Electrodes   = opts.Electrodes;
  LeadField.gndElectrode = opts.gndElectrode;
  LeadField.measElectrode = opts.measElectrode;
  % LeadField.gndNode = LeadField.Electrodes.Nodes(LeadField.gndElectrode);
  
  %LeadField.stillLoading = false;
  % Build LeadField Matrix
  [baseSolSpace, origSolSpace,finalSolSpace] = ...
    getSolutionSpaces(LeadField,opts.nrrdSeg,...
    opts.dwnSmpLvl_LField_PreSave,opts.dwnSmpLvl_LField_PostSave);
  
  
  if exist([LeadField.fpath LeadField.fname],'file')
    mydisp('Loading Precomputed LeadField');
    load([LeadField.fpath LeadField.fname]);
  else
    origMatrix = opts.FDModel.compute_LeadField(LeadField.measElectrode,LeadField.gndElectrode,...
      baseSolSpace, origSolSpace,opts.parallelComputation);
    save(fullfile(LeadField.fpath,LeadField.fname),'origMatrix','-v7.3');
  end;
  
  LeadField.colPerVox = 3;
  LeadField.origMatrix = origMatrix';
  LeadField.origSolutionSpace = origSolSpace;
  LeadField.disableRebuild = true;
  LeadField.currSolutionSpace = finalSolSpace;
  LeadField = LeadField.rebaseOnCurrent;
end