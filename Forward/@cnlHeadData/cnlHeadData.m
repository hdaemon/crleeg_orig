classdef cnlHeadData < handle
  % classdef cnlHeadData
  %
  % The container class for a full set of data files from which a cnlModel
  % can be constructed.
  %
  % Options and Defaults:
  % nrrdT1       : file_NRRD Object to T1 Image
  % nrrdT2       : file_NRRD Object to T2 Image
  % nrrdDTI      : file_NRRD Object to Single Tensor Image
  % nrrdSkin     : NRRD file for skin segmentation
  % nrrdSkull    : NRRD file for skull segmentation
  % nrrdICC      : NRRD file for ICC segmentation
  % nrrdBrain    : NRRD file for brain segmentation
  % nrrdNMM      :
  % nrrdNVM
  % nrrdIBSR
  % nrrdCSFFractions
  % nrrdSurfNorm
  % nrrdPVSeg
  % isFrozen = false;
  %
  % Written By: Damon Hyde
  % Last Edited: Nov 23, 2015
  % Part of the cnlEEG Project
  %

  properties
    % Flag to prevent files from being changed.
    isFrozen = false;
    
    % Raw MRI Data
    nrrdT1   = []; 
    nrrdT2   = [];
    nrrdDTI  = [];
        
    % Stuff for building the model segmentation
    %  These can technically be computed from the above, but we store them
    %  separately since they're processed outside of matlab with the main CRL pipeline.
    nrrdSkin  = [];
    nrrdSkull = [];
    nrrdICC   = [];
    nrrdBrain = [];
    
    % Parcellations
    nrrdNMM
    nrrdNVM
    nrrdIBSR

    %
    nrrdIEEG

    % Other useful head data    
    nrrdCSFFractions  = [];
    nrrdSurfNorm      = [];
    nrrdPVSeg         = [];
  end
  
  properties (Dependent = true)
    modelName
  end
  
  methods
    function obj = cnlHeadData(varargin)            
      if nargin>0
        
        %% If the first input was itself a cnlHeadData object, just copy the
        %% values over and return the result
        if isa(varargin{1},'cnlHeadData')
                   mydisp('Just copying values over');
          tmp = properties('cnlHeadData');
          for i = 1:length(tmp)
            obj.(tmp{i}) = p.Results.headData.(tmp{i});
          end
          mydisp('END CONSTRUCTOR');
          return;
        end
                
        %% Parse Inputs
        p = inputParser;
        p.KeepUnmatched = true;        
        addOptional(p,'dirSearch',[],@(x) isa(x,'char'));
        
        % Add all nrrd* Properties as Parameter-Value Pairs
        nrrdList = cnlHeadData.nrrdList;
        for i = 1:numel(nrrdList)
          addParamValue(p,nrrdList{i},[],@(x) isa(x,'file_NRRD')|isempty(x));
        end;        
        parse(p,varargin{:});                               
                
        %% Assign values
        if isempty(p.Results.dirSearch)
        mydisp('Setting object properties');               
        for i = 1:length(nrrdList)
          obj.(nrrdList{i}) = p.Results.(nrrdList{i});
        end
        else
          obj.scanForDefaults(p.Results.dirSearch);
        end
       
      end;
      
      mydisp('END CONSTRUCTOR');
    end;
    
    
    function out = isempty(obj)
      % function out = isempty(obj)
      % 
      % Overloaded isempty() method for cnlHeadData objects.  Returns true
      % if all obj.nrrd* fields are empty.
      nrrdList = obj.nrrdList;
      out = true;
      for idx = 1:numel(nrrdList)
        if ~isempty(obj.(nrrdList{idx}))
          out = false;
        end;
      end
    end
    
    function out = get.modelName(obj)
      out = obj.genModelName;
    end;
    
    out = genModelName(obj);
    scanForDefaults(obj,dir);

    
    function obj = purgeAll(obj)
      % function obj = purgeAll(obj)
      %
      % Purge data from all nrrds associated with the cnlHeadData object
      tmp = properties(obj);
      for i = 1:length(tmp)
        if strcmp(tmp{i}(1:4),'nrrd')
          if ~isempty(obj.(tmp{i}))
            obj.(tmp{i}).purgeData;
          end
        end
      end
    end
    
    function set.nrrdSkin(obj,val)
      if checkNRRD(obj,'nrrdSkin',val)
        obj.nrrdSkin = val;
      end
    end
    
    function set.nrrdSkull(obj,val)
      if checkNRRD(obj,'nrrdSkull',val)        
        obj.nrrdSkull = val;
      end
    end
          
    function set.nrrdBrain(obj,val)
      if checkNRRD(obj,'nrrdBrain',val)        
        obj.nrrdBrain = val;
      end
    end
    
    function set.nrrdICC(obj,val)
      if checkNRRD(obj,'nrrdICC',val)
        obj.nrrdICC = val;
      end
    end
    
    function set.nrrdT1(obj,val)
      if checkNRRD(obj,'nrrdT1',val)
        obj.nrrdT1 = val;
      end;
    end
    
    function set.nrrdT2(obj,val)
      if checkNRRD(obj,'nrrdT2',val)
        obj.nrrdT2 = val;
      end
    end
    
    function set.nrrdDTI(obj,val)      
      %if checkNRRD(obj,'nrrdDiffTensors',val)        
        obj.nrrdDTI = val;
     % end
    end
    
    function set.nrrdCSFFractions(obj,val)
      if checkNRRD(obj,'nrrdCSFFractions',val)           
        obj.nrrdCSFFractions = val;
      end
    end
    
    function set.nrrdSurfNorm(obj,val)
      if checkNRRD(obj,'nrrdSurfNorm',val)       
        obj.nrrdSurfNorm = val;
      end
    end
    
    function set.nrrdPVSeg(obj,val)
      if checkNRRD(obj,'nrrdPVSeg',val)       
        obj.nrrdPVSeg = val;
      end
    end
        
  end
  
  methods (Static=true)
    
    function nrrdList = nrrdList
       % function nrrdList = nrrdList;
       %
       % Just a quick function to return the list of all cnlHeadData
       % properties that begin with 'nrrd'.  Makes iterating across all
       % NRRDs easier elsewhere.
       nrrdList = properties('cnlHeadData');
       keep = false(size(nrrdList));
       for i = 1:length(nrrdList)
          if (length(nrrdList{i})>4)&&(strcmpi(nrrdList{i}(1:4),'nrrd'))
            keep(i) = true;            
          end;
       end;
       nrrdList = nrrdList(keep);
    end
    
  end
  
  methods (Access=private)
           
    function valid = checkNRRD(obj,field,val)
      % function setNRRD(obj,field,val)
      %
      % Helper function to do validity checking when setting NRRD fields.
      if ~obj.isFrozen
        if isa(val,'file_NRRD');
         if val.existsOnDisk
          mydisp([' Setting ' field ' to ' val.fname]);
          valid = true;
         else
             error(['NRRD file ' val.fname ' does not seem to exist on disk']);
         end;
        elseif isempty(val)
          mydisp(['Clearing ' field ]);
          valid = true;
        else
          error('Invalid NRRD Object');
        end;
      else
        valid = false;
        warning(['Can''t change obj.' field '.  The options have been frozen']);
      end;
    end
  end
  
end