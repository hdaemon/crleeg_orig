function out = genModelName(obj)
% function out = genModelName(obj)
%
% Given a particular set of headdata, generate the model name that should
% be associated with it.
%
% Note that this is currently a total hack, and should be modified so that
% each filename is appropriately parsed for some type of unique identifier.

if isempty(obj)
  out = 'EMPTYMODEL';
  return;
end

if ~isempty(obj.nrrdSkin)
  skinString = 'skinCRL';
else
  error('Must have a skin segmentation to build a model');  
end

if ~isempty(obj.nrrdSkull)
  skullString = 'skinCRL';
else
  skullString = 'skullNONE';
end

if ~isempty(obj.nrrdBrain)
  if ~isempty(findstr(obj.nrrdBrain.fname,'brain_crl'));
   brainString = 'segCRL';
  elseif ~isempty(findstr(obj.nrrdBrain.fname,'nvm_crl'));
    brainString = 'segNVM';
  elseif ~isempty(findstr(obj.nrrdBrain.fname,'brain_nvm'));    
    brainString = 'segNVM';
  elseif ~isempty(findstr(obj.nrrdBrain.fname,'nmm_crl'));
    brainString = 'segNMM';
  elseif ~isempty(findstr(obj.nrrdBrain.fname,'ibsr_crl'));
    brainString = 'segIBSR';
  end;  
else
  brainString = 'segNONE';
end;

if ~isempty(obj.nrrdDTI)
  wmString = 'wmCRL';
else
  wmString = 'wmNONE';
end

out = [skinString '_' skullString '_' brainString '_' wmString];

end