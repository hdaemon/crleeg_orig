    function scanForDefaults(obj,searchDir)
      returnDir = pwd;
      cd(searchDir);
      
      if exist('MRI_T1.nrrd','file'),
        obj.nrrdT1 = file_NRRD('MRI_T1.nrrd',searchDir,true); end;
      
      if exist('MRI_T2.nrrd','file'),
        obj.nrrdT2 = file_NRRD('MRI_T2.nrrd',searchDir,true); end;
      
      if exist('tensors_crl.nrrd','file')
        obj.nrrdDTI = file_NRRD('tensors_crl.nrrd',searchDir,true); end;
      
      if exist('seg_brain_crl.nrrd','file'),
        obj.nrrdBrain = file_NRRD('seg_brain_crl.nrrd',searchDir,true); end;
      
      if exist('seg_skin_crl.nrrd','file'),
        obj.nrrdSkin = file_NRRD('seg_skin_crl.nrrd',searchDir,true); end;
      
      if exist('seg_skull_crl.nrrd','file'),
        obj.nrrdSkull = file_NRRD('seg_skull_crl.nrrd',searchDir,true); end;
      
      if exist('seg_icc_crl.nrrd','file'),
        obj.nrrdICC = file_NRRD('seg_icc_crl.nrrd',searchDir,true); end;
      
      if exist('parcel_nmm_crl.nrrd','file'),
        obj.nrrdNMM = cnlParcellation('parcel_nmm_crl.nrrd',searchDir,'NMM',true); end;
        
      if exist('parcel_nvm_crl.nrrd','file'),
        obj.nrrdNVM = cnlParcellation('parcel_nvm_crl.nrrd',searchDir,'NVM',true); end;
      
      if exist('parcel_ibsr_crl.nrrd','file'),
        obj.nrrdIBSR = cnlParcellation('parcel_ibsr_crl.nrrd',searchDir,'IBSR',true); end;
      
      if exist('vec_CortOrient_crl.nrrd','file'),
        obj.nrrdSurfNorm = file_NRRD('vec_CortOrient_crl.nrrd',searchDir,true); end;       
                                          
      if exist('iEEG_Electrodes','dir'),
        cd iEEG_Electrodes
        d = dir;
        tmpIdx = 0;
        for i = 1:numel(d)
          [~,fname,ext] = fileparts(d(i).name);
          if strcmp(ext,'.nrrd')
            tmpIdx = tmpIdx+1;
            if tmpIdx==1
              obj.nrrdIEEG = cnliEEGElectrodeMap(d(i).name,'./','grid'); obj.nrrdIEEG.namePrefix = fname;
            else
              obj.nrrdIEEG(tmpIdx) = cnliEEGElectrodeMap(d(i).name,'./','grid');  obj.nrrdIEEG(tmpIdx).namePrefix = fname;            
            end;
          end
        end;
      end


      cd(returnDir); 
    end
