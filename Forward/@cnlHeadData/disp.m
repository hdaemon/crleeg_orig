function disp(headData)
% DISP Overloaded display method for cnlHeadData objects
%
% function disp(headData)
%
% Written By: Damon Hyde
% Last Edited: April 2015
% Part of the cnlEEG Project
%

nrrdList = cnlHeadData.nrrdList;

disp(['Raw MRI Images']);
nrrdList = displayNRRD(headData.nrrdT1,'nrrdT1',nrrdList);
nrrdList = displayNRRD(headData.nrrdT2,'nrrdT2',nrrdList);
nrrdList = displayNRRD(headData.nrrdDTI,'nrrdDTI',nrrdList);

disp(['Segmentation Images']);
nrrdList = displayNRRD(headData.nrrdSkin,'nrrdSkin',nrrdList);
nrrdList = displayNRRD(headData.nrrdSkull,'nrrdSkull',nrrdList);
nrrdList = displayNRRD(headData.nrrdICC,'nrrdICC',nrrdList);
nrrdList = displayNRRD(headData.nrrdBrain,'nrrdBrain',nrrdList);

disp(['Parcellations']);
nrrdList = displayNRRD(headData.nrrdNMM,'nrrdNMM',nrrdList);
nrrdList = displayNRRD(headData.nrrdNVM,'nrrdNVM',nrrdList);
nrrdList = displayNRRD(headData.nrrdIBSR,'nrrdIBSR',nrrdList);

disp(['Other images']);

for idx = 1:length(nrrdList)
  [~] = displayNRRD(headData.(nrrdList{idx}),nrrdList{idx},[]);
end;

end

function listOut = displayNRRD(nrrdIn,name,listIn)


dispName = blanks(20);
dispName(end-length(name)+1:end) = name;

if ~isempty(nrrdIn)
  disp([dispName ': ' nrrdIn(1).fname]);
else
  disp([dispName ': EMPTY']);
end;

isOnList = ismember(listIn,name);
listOut = listIn(~isOnList);
  
dispName = blanks(20);
for idx = 2:numel(nrrdIn)
  if isa(nrrdIn(idx),'file_NRRD')
  disp([dispName ': ' nrrdIn(idx).fname])
  isOnList = ismember(listIn,name);
  listOut = listIn(~isOnList);
  end;
end

end
