function matOut = build_InfiniteSpaceMatrix(elecPts,dipolePts,sigma)

nElec = size(elecPts,1);
nPts = size(dipolePts,1);

matOut = zeros(3*nPts,nElec);

for idxElec = 1:nElec
  disp(['Computing electrode number: ' num2str(idxElec)]);
  currPt = elecPts(idxElec,:);
  elecLoc = repmat(currPt,nPts,1);
  
  distVec = dipolePts - elecLoc;
  
  nrms = sqrt(sum(distVec.^2,2));
    
  invNrms = 1./nrms;
  invNrms(isinf(nrms)|isnan(nrms)) = 0;
  
  unitVec = spdiags(invNrms,0,nPts,nPts)*distVec;
  
  nrms(nrms<3) = 3;
  
  weight = 1./(4*pi*sigma*nrms.^2);
  
  lFieldRow = spdiags(weight,0,nPts,nPts)*unitVec;
  lFieldRow = lFieldRow';
  lFieldRow = lFieldRow(:);
  
  matOut(:,idxElec) = lFieldRow;
 
end



end