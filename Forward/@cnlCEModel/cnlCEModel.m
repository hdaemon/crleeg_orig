classdef cnlCEModel < cnlElectrodeModel
  
  methods
    
    function out = cnlCEModel()
    end
  end
  
  methods (Static)
    
    function nrrdObj = modifyConductivity(Electrodes,nrrdObj)
      % The Complete Electrode Model removes electrode voxels from the
      % computational domain and replaces them with boundary conditions
      %
      assert(isa(Electrodes,'cnlElectrodes'),...
        'Input electrode array must be a cnlElectrodes object');
            
      assert(nrrdObj.sizes(1)==6,'Input should be a NRRD of tensors');
      
      for idxE = 1:Electrodes.nElec
        nrrdObj.data(:,Electrodes.Voxels{idxE}) = 0;
      end;      
    end
    
    function matOut = modifyFDMatrix(Electrodes,matIn)
      % The complete electrode model adds a row and column to the FD matrix
      % for each individual electrode.
      %
      [row,col,val] = find(matIn);
      
      nNodes = size(matIn,1);
      nElec = Electrodes.nElec;
      
      nodes = Electrodes.Nodes;
      
      emptyRows = find(~full(any(matIn,2)));      
      
      disp('Modifying FD Matrix for Complete Electrode Model');
      for idxE = 1:nElec 
       disp(['Adding Electrode Number ' num2str(idxE)]);
        eNodes = nodes{idxE};
        eNodes = setdiff(eNodes,emptyRows); %Drop unconnected nodes
        
        next = nNodes + idxE;                
        
        newVals = 1./(Electrodes(idxE).Impedance*ones(numel(eNodes),1));
        
        row = [row ; next*ones(numel(eNodes),1)];
        col = [col ; eNodes(:)];
        val = [val ; newVals];
        
        row = [row ; eNodes(:)];
        col = [col ; next*ones(numel(eNodes),1)];
        val = [val ; newVals];        
        
        row = [row ; next];
        col = [col ; next];
        val = [val ; -sum(newVals)];
        
        idxRows = find(row==col);
        idxNodes = idxRows(find(ismember(row(idxRows),eNodes)));
        if numel(idxNodes)~=numel(eNodes), disp('Something went wrong'); keyboard; end;

        val(idxNodes) = val(idxNodes) - newVals;

        %for idxN = 1:numel(eNodes)
        %  nodeIdx = find((row==eNodes(idxN))&(col==eNodes(idxN)));
        %  if numel(nodeIdx)~=1, disp('Something went wrong'); keyboard; end;
        %  val(nodeIdx) = val(nodeIdx) + 1/Electrodes(idxE).Impedance;
        %end

      end
      
      matOut = sparse(row,col,val,nNodes+nElec,nNodes+nElec);
    end
    
    function currents = getCurrents(Electrodes,ModelSize,AnodeIdx,CathodeIdx)
      % In the Complete Electrode Model, currents are applied at the
      % auxilliary nodes added by modifyFDMatrix
      %
      currents = zeros(prod(ModelSize)+Electrodes.nElec,1);
      
      if AnodeIdx~=CathodeIdx
        currents(prod(ModelSize)+AnodeIdx)   =  1;
        currents(prod(ModelSize)+CathodeIdx) = -1;
      end;
    end
    
    
  end
  
end