function clear_subDirs(modelObj)
  % function clear_subDirs(modelObj)
  %
  % Clear all directories EXCEPT for modelObj.dir_Root
  %
  
  modelObj.dir_AllModels = [];
modelObj.dir_Model = [];
modelObj.dir_SubModel = [];
modelObj.dir_Data = [];
modelObj.dir_PhotoGram = [];

end