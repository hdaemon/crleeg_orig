function clear_fNames(modelObj)
  % function clear_fNames(modelObj)
  %
  % Clear all filename settings
  %
  
  modelObj.fName_Model = [];
modelObj.fName_SegNRRD = [];
modelObj.fName_IsoNRRD = [];
modelObj.fName_AnisoNRRD = [];
modelObj.fName_PhotoGram = [];
modelObj.fName_LeadField = [];
end