
function nrrdCond = getDTIConductivities(nrrdCond,nrrdSeg,nrrdDiffTensors,whitelabel,DSLevel)
% GETDTICONDUCTIVITIES
%
% function getDTIConductivities(nrrdCond,nrrdSeg,nrrdDiffTensors,whitelabel,DSLevel)
%
% Inputs:
%    nrrdCond : file_NRRD of anisotropic conductivity image
%    nrrdSeg  : file_NRRD of segmentation image
%    nrrdDiffTensors : file_NRRD of diffusion tensors.
%
% Optional Inputs:
%    whitelabel : Labels of voxels to assign anisotropic conductivities
%    DSLevel : This is deprecated. Function now requires that the input
%                diffusion images be of the same size as the segmentation. 
%
% Outputs:
%    nrrdCond : updated file_NRRD with anisotropic conductivities 
%
% Written By: Damon Hyde
% Last Edited: Feb 4, 2016
% Part of the cnlEEG Project
%

mydisp('Using Anisotropic Conductivities From Diffusion Imaging');

%% Input Parsing
p = inputParser;
p.addRequired('nrrdCond',@(x) isa(x,'file_NRRD')&&strcmpi(x.kinds{1},'3D-symmetric-matrix'));
p.addRequired('nrrdSeg' ,@(x) isa(x,'file_NRRD')&&all(x.sizes==nrrdCond.sizes(2:end)));
p.addRequired('nrrdDiffTensors',@(x) ...
                        (isa(x,'file_NRRD') && validateSingleTensor(x,nrrdCond))||...
                        (         iscell(x) &&  validateMultiTensor(x,nrrdCond))     );
p.addOptional('whitelabel',7,   @(x) isnumeric(x));
p.addOptional('DSLevel',[1 1 1],@(x) isnumeric(x)&&(numel(x)==numel(nrrdSeg.sizes)));

parse(p,nrrdCond,nrrdSeg,nrrdDiffTensors,whitelabel,DSLevel);

% Are we using a multi-Tensor image
multiTensor = iscell(nrrdDiffTensors);

% Read/Reshape Tensor Data for easier access
if multiTensor
  mydisp('Using Multi-Tensor Model');
  nTensor = numel(nrrdDiffTensors);
  
  % Get the weighting for each of the compartments
  tensorWeights = nrrdDiffTensors{1}.data;
  tensorWeights = reshape(tensorWeights,nTensor,[]);
  
  % Get Tensor Data for Each Compartment
  for idx = 2:nTensor
    tensorData{idx-1} = reshape(nrrdDiffTensors{idx}.data,6,[]);
  end
  
  % Build a set of tensors for the isotropic component of the DCI image.
  % This assumes that the isotropic conductivity is equivalent to CSF.
  % Should probably be made selectable.
  tmpTensor = 0.00238*[1 0 0 1 0 1]'; 
  tensorData{end+1} = repmat(tmpTensor,1,prod(nrrdDiffTensors{idx}.sizes(2:end)));
  
else
  
  mydisp('Using Single Tensor Model');
  nTensor = 1;
  tensorWeights = ones(1,prod(nrrdDiffTensors.sizes(2:end)));
  tensorData{1} = reshape(nrrdDiffTensors.data,[6 prod(nrrdDiffTensors.sizes(2:end))]);
  nrrdDiffTensors = {nrrdDiffTensors};
end;  

%% Computation
voxels_WhiteMatter = find(ismember(nrrdSeg.data,whitelabel));

mydisp('Computing Conductivity Tensors');

[refX, refY, refZ] = ind2sub(nrrdSeg.sizes,voxels_WhiteMatter);

% If we've downsampled, index into Tensor image isn't the same
refX2 = ceil(refX/DSLevel(1));
refY2 = ceil(refY/DSLevel(2));
refZ2 = ceil(refZ/DSLevel(3));

idxTensor = sub2ind(nrrdDiffTensors{1}.sizes(2:4),refX2,refY2,refZ2);

dataOut = nrrdCond.data;

for i = 1:length(voxels_WhiteMatter)
  % Periodic output
  if mod(i,100000)==0
    mydisp(['Completed ' num2str(i) ' of ' num2str(length(voxels_WhiteMatter)) ' tensors.']);
  end
  
  meanTensor = zeros(6,1);
  for idx = 1:nTensor
    tmpTensor = cnlModel.convert_DiffTensorToCondTensor(tensorData{idx}(:,idxTensor(i)));
    meanTensor = meanTensor + tensorWeights(idx,idxTensor(i))*tmpTensor;
  end;   
  
  % If the tensor is non-zero, insert it into the image.
  if any(meanTensor)
    dataOut(:,refX(i),refY(i),refZ(i)) = meanTensor;
  end;
end

nrrdCond.data = dataOut;

end

function isValid = validateSingleTensor(nrrdIn,nrrdCond)
  % Does it claim to be a tensor?
  isValid = strcmpi(nrrdIn.kinds{1},'3D-symmetric-matrix');
  % Does the size match the conductivity image?
  isValid = isValid&&all(nrrdIn.sizes==nrrdCond.sizes);
end

function isValid = validateMultiTensor(nrrdIn,nrrdCond)
  % Are the number of weights equal to the number of tensors
  isValid = nrrdIn{1}.sizes(1)==(numel(nrrdIn));
  % Does the size match the conductivity image
  isValid = isValid && all(nrrdIn{1}.sizes(2:end)==nrrdCond.sizes(2:end));
  % Are all the other images tensors
  for idx = 2:numel(nrrdIn)
  isValid = isValid && strcmpi(nrrdIn{idx}.kinds{1},'3D-symmetric-matrix');
  end
  
end

%nrrdCond.data(:,refX(i),refY(i),refZ(i))
