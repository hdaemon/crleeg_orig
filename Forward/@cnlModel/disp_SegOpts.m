function disp_SegOpts(obj)


disp(['           SEGMENTATION OPTIONS']);
disp(['%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%']);
disp(['          fName_SegNRRD: ' obj.fName_SegNRRD]);
if isa(obj.nrrdModelSeg,'file_NRRD')
  disp(['                nrrdSeg: ' obj.nrrdModelSeg.fname]);
else
  disp(['                nrrdSeg: EMPTY']);
end;

  disp(['        Component NRRDS:']);
if ~isempty(obj.headData.nrrdSkin)
  disp(['               nrrdSkin: ' obj.headData.nrrdSkin.fname]);
else
  disp(['               nrrdSkin: EMPTY']);
end;

if ~isempty(obj.headData.nrrdSkull)
  disp(['              nrrdSkull: ' obj.headData.nrrdSkull.fname]);
else
  disp(['              nrrdSkull: DILATED ICC']);
  disp(['         skullThickness: ' num2str(obj.skullThickness)]);  
end;

if ~isempty(obj.headData.nrrdICC)
  disp(['                nrrdICC: ' obj.headData.nrrdICC.fname]);
else
  disp(['                nrrdICC: EMPTY']);
end;

if ~isempty(obj.headData.nrrdBrain)
  disp(['              nrrdBrain: ' obj.headData.nrrdBrain.fname]);
else
  disp(['              nrrdBrain: EMPTY']);
end;

disp(['          dwnSmpLvl_Seg: [' num2str(obj.dwnSmpLvl_Seg) ']']);
disp(['       force_SegRebuild: ' BoolToString(obj.force_SegRebuild)]);
disp([' ']);

end