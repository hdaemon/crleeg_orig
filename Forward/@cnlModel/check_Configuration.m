function check_Configuration(modelObj)
% CHECK_CONFIGURATION Check configuration of cnlModel object
%
% function CHECK_CONFIGURATION(modelObj)
%
% Configure the cnlModel object for build.
%
% Written By: Damon Hyde
% Last Edited: April 2015
% Part of the cnlEEG Project
%

% Make sure type has been set
if isempty(modelObj.type)
  error('cnlModel:typeUnset',...
    'modelObj.type must be set before proceeding');
end

% Check that the directory structure is set up properly.
modelObj.check_Directories;
modelObj.check_FileNames;

% Read in headdata if it hasn't been set already
if isempty(modelObj.headData)
  disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%');
  disp('%%  Attempting to Automatically Read in Head Data                %%');
  disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%');
  modelObj.headData = cnlHeadData(modelObj.dir_Data);
end;

% Make sure we've computed the necessary electrodes already.
if ~exist([modelObj.dir_Root 'sourceanalysis/photogrammetry/guiElectrodes.mat'],'file');
  [ Electrodes, Fiducials ] = guiNewElectrodes(modelObj.headData.nrrdT1, modelObj.headData.nrrdSkin);
  save([modelObj.dir_Root 'sourceanalysis/photogrammetry/guiElectrodes.mat'],'Electrodes');
  save([modelObj.dir_Root 'sourceanalysis/photogrammetry/fiducials.mat'],'Fiducials');
end

% Do something to report out the current build configuration here?

end