function [Electrodes] = getElectrodeLocations(modelObj)
% function [ElectrodesOut] = getElectrodeLocations(modelObj)
%
% Get the (X,Y,Z) locations of each electrode in the MRI space.
%
% Depending on the modelObj.type field, this is done in one of three ways:
%
% hdeeg_128 : Locations are computed using photogrammetry
% clinical  : Locations are computed using guiNewElectrodes
% ieeg      : Locations are identified from iEEG electrode segmentations
%
% Written By: Damon Hyde
% Last Edited: April 2015
% Part of the cnlEEG Project
%

  %% Tests
  electrodeFile = [modelObj.dir_Model modelObj.dir_SubModel 'ElectrodeNodes.mat'];
  fileExists = exist(electrodeFile,'file');
 
  %% Check if the electrodes file already exists
  if fileExists && ( ~modelObj.force_PhotoG_recomp ) 
    mydisp('Using Existing Electrode File');
    load(electrodeFile);    
    return;
  end;

%% Do Photogrammetry
switch lower(modelObj.type)
  case 'hdeeg128'
    mydisp('Obtaining electrode locations through photogrammetry');
    
    % Set up a cnlPhotoGram object and run the registration to the T1 MRI.
    if exist([modelObj.dir_PhotoGram 'mat-sessions/FinalPG.mat']);
      mydisp('Found already computed photogrammetry file');1
      load([modelObj.dir_PhotoGram 'mat-sessions/FinalPG.mat']);
    else
      mydisp('Running photogrammetry');
      photoObj = cnlPhotoGram( ...
        'fName_MRI' , modelObj.headData.nrrdT1.fname   , ...
        'fName_Skin' , modelObj.headData.nrrdSkin.fname , ...
        'fName_SFP' , modelObj.fName_PhotoGram , ...
        'dir_MRI' , modelObj.headData.nrrdT1.fpath , ...
        'datapath' , [modelObj.dir_PhotoGram ], ...
        'respath' , [modelObj.dir_PhotoGram 'res-sessions/'] , ...
        'matpath' , [modelObj.dir_PhotoGram 'mat-sessions/'] );
      
      photoObj = photoObj.registerEEG;
      save([modelObj.dir_PhotoGram 'mat-sessions/FinalPG.mat'],'photoObj');      
    end;
    
    % Build the cnlElectrodes object
    mydisp('Outputting final electrode object');
    Electrodes = photoObj.getElectrodeObj('final');
    
    if modelObj.useAniso
      Electrodes = Electrodes.mapToNodes(modelObj.nrrdModelSeg,'aniso',true);
    else
      Electrodes = Electrodes.mapToNodes(modelObj.nrrdModelSeg,'iso',true);
    end;
    
  case 'clinical'
    mydisp('Obtaining electrode locations from guiElectrodes.mat');
    
    if exist([modelObj.dir_PhotoGram '/guiElectrodes.mat'],'file');
      mydisp('Found existing guiElectrodes.mat');
      load([modelObj.dir_PhotoGram '/guiElectrodes.mat']);
    else
      mydisp('Opening guiNewElectrodes to identify fiducials');
      [Electrodes Fiducials] = guiNewElectrodes(modelObj.headData.nrrdT1,modelObj.headData.nrrdSkin);
      save([modelObj.dir_PhotoGram '/guiElectrodes.mat'],'Electrodes');
    end;
    
    %Electrodes.nodeMRI = modelObj.nrrdModelSeg;

    if modelObj.useAniso
      Electrodes = Electrodes.mapToNodes(modelObj.nrrdModelSeg,'aniso',true);
    else
      Electrodes = Electrodes.mapToNodes(modelObj.nrrdModelSeg,'iso',true);
    end;
    
  case 'ieeg'
    mydisp('Obtaining electrode locations from iEEG');
    pos = [];
    lab = {};
    vox = {};
    nodes = {};
    figure(1); hold on;
    nrrdEEG = modelObj.headData.nrrdIEEG;
    for i = 1:numel(nrrdEEG)
      mydisp(['Processing NRRD # ' num2str(i)]);
      tmp = nrrdEEG(i).elecLoc;
     % figure(1);
     % plot3(tmp(:,1),tmp(:,2),tmp(:,3),'x','markersize',30);
      pos = [pos ; nrrdEEG(i).elecLoc ];
      lab = {lab{:} nrrdEEG(i).elecLabels{:}};   
      vox = {vox{:} nrrdEEG(i).elecVoxels{:}};
      nodes = {nodes{:} nrrdEEG(i).elecNodes{:}};
    end;
    
    mydisp('Building cnlElectrodes object');
    Electrodes = cnlElectrodes;
    Electrodes.nodeMRI = modelObj.nrrdModelSeg;
    Electrodes.Positions = pos;
    Electrodes.Labels    = lab;
    Electrodes.Voxels = vox;

    switch class(modelObj.elecModel)
      case 'cnlPEModel'
        if modelObj.useAniso
          Electrodes = Electrodes.mapToNodes(modelObj.nrrdModelSeg,'aniso',false);
        else
          Electrodes = Electrodes.mapToNodes(modelObj.nrrdModelSeg,'iso',false);
        end;        
      case 'cnlCEModel'
        Electrodes.Nodes = nodes;        
      otherwise
        error('Unknown Electrode Model Type');
    end
    
  otherwise
    error('getElectrodeLocations:unknownType','Unknown model type');
    
end;

Electrodes.plot3D('surfMRI',modelObj.headData.nrrdSkin);
mydisp('Check alignment after shifting to scalp surface');

  %% Save out the ElectrodeNodes structure
  cd([modelObj.dir_Model modelObj.dir_SubModel]);
  save ElectrodeNodes Electrodes;
return;