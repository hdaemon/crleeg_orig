classdef cnlModel < handle
  % CNLMODEL  
  %
  % classdef cnlModel < handle
  %
  % Extends the cnlHeadData class to include everything needed to build a
  % finite difference model and leadfield.
  %
  % Written By: Damon Hyde
  % Last Edited: April 2015
  % Part of the cnlEEG Project
  %
  
  properties

    type = []; % Empty by default to force the user to assign it
    
    % Directories to locate things in;
    dir_Root      = [];  % Root directory of the patient
    dir_AllModels = [];  % Directory all models are stored in.
    dir_Model     = [];  % Directory to save model specific files in
    dir_SubModel  = [];  % Directory to save Leadfield and Electrodes to
    dir_Data      = [];  % Directory to find input data in
    dir_PhotoGram = [];  % Directory to photogrammetry data
    
    % Filenames to save things out to.
    fName_Model     = []; % Model filename (is this unused?)
    fName_SegNRRD   = []; % Segmentation filename
    fName_IsoNRRD   = []; % Isotropic conductivity filename
    fName_AnisoNRRD = []; % Anisotropic conductivity filename
    fName_PhotoGram = []; % Photogrammetry file to use
    fName_LeadField = []; % Leadfield filename
    
    % Various flags -> Generally, these don't actually need to be set.  The
    % code will assign them automatically.  But if you want to override the
    % default options, you can do that.
    useDTIData = [];           % Get white matter conductivities from DTI
    usePVSulci = [];           % Use partial volume sulci
    usePhotogrammetry = [];    % Use photogrammetry
    useAniso = true;           % This is deprecated
    useConstrainedCortex = []; % Assign 
    
    % Flags to force rebuilt of various model components
    force_SegRebuild       = false;
    force_CondRebuild      = false;
    force_PhotoG_recomp    = false;
    force_LeadFieldRebuild = false;
    
    %       
    headData = cnlHeadData;
    
    % Segmentation Construction Options    
    skullThickness = 4;
    dwnSmpLvl_Seg = [ 1 1 1 ];    
    
    % Conductivity Options    
    CSFFraction = 0;
    % Map Corresponds to:     [ Outside SoftTissue  Bone    CSF  GreyMatter CSF   ????  White Matter];
    map_labeltoconductivity = [ 0 0.33 0.012 1.79 0.33 1.79 0.33 0.33 1e6 1e-6];
        
    % Photogrammetry and Electrode Options
    Electrodes            
    photoCostFunction = 'hcf';
    photoModelTr      = 'quasiaffine';
    photoOptimization = 'powell';
    
    %
    labelWhite = 7;
    

    % FD Model Options
    elecModel = cnlPEModel;
        
    % LeadField Options   
    gndElectrode    = 80;    
    measElectrode = [];
    dwnSmpLvl_LField_PreSave  = [ 1 1 1 ];
    dwnSmpLvl_LField_PostSave = [ 1 1 1 ];    
    LeadField;
    
    % Other Options
    OutputFileName = [];
    parallelComputation = true;
    
    FDModel;
    
  end
  
  properties (Constant,GetAccess=private)
    VALID_TYPES = {'hdeeg128' 'clinical' 'ieeg'};
    subDIR_MODELS    = 'sourceanalysis/models/';
    subDIR_PHOTOGRAM = 'sourceanalysis/photogrammetry/';
    subDIR_DATA      = 'sourceanalysis/data_for_sourceanalysis/';
    FNAME_MODEL      = 'Model_EEG.mat';
    FNAME_ISONRRD    = 'IsotropicConductivity.nrrd';
    FNAME_ANISONRRD  = 'AnisotropicConductivity.nrrd';
    FNAME_SEGNRRD    = 'FullSegmentation.nrrd';
    FNAME_LEADFIELD  = 'LeadField.mat';
  end

  properties (GetAccess=public)
    nrrdModelSeg;
    nrrdFinalSeg;
    nrrdConductivity;
  end
  
  properties (Dependent=true)    
    opts_Seg
    opts_PhotoG
    opts_Cond
    opts_FDModel
    opts_LeadField

    totalDownSample
    totalLFieldDownSample
  end
  
  methods
    
    function obj = cnlModel(rootDir,headData,type)
      % function obj = cnlModel(rootDir,headData)
      %
      %
            
      if (nargin==1)
          if isa(rootDir,'cnlModel')   
            % If the first argument is a model, just pass the same model out.        
            obj = rootDir;
            return;        
          elseif exist(rootDir,'dir')
            % If the first argument is a directory that exists, try
            % building things from there.
            
            if rootDir(end)~=('/')
              rootDir = [rootDir '/'];
            end;
            
            obj.dir_Root = rootDir;        
            obj.check_Directories;
          end;
      end;

      if nargin>=2
        % Set the root directory, and ensure that all the other directory
        % structures are properly in place.
        obj.dir_Root = rootDir;
                                  
        % Set headData
        if ~isa(headData,'cnlHeadData')
          obj.type = headData;
        else
          obj.headData = headData;
        end;          
        
        if exist('type','var')&&~isempty(type)
          obj.type = type;
        end;
        
        obj.check_Compatibility;
      end;
    end;
    
    
    %% Overloaded get methods for common option flags.  Each has a default mode
    %% that gets invoked if the property has not been set.
    function out = get.useDTIData(obj)
      if isempty(obj.useDTIData)
        out = ~isempty(obj.headData.nrrdDTI);
      else
        out = obj.useDTIData;
      end;
    end;

    function out = get.usePVSulci(obj)
      if isempty(obj.usePVSulci)
        out = ~isempty(obj.headData.nrrdCSFFractions);        
      else
        out = obj.usePVSulci;
      end;
    end;

    function out = get.useConstrainedCortex(obj)
      if isempty(obj.useConstrainedCortex)
        out = ~isempty(obj.headData.nrrdSurfNorm);
      else
        out = obj.useConstrainedCortex;
      end
    end;
    
    function out = get.usePhotogrammetry(obj)
      if isempty(obj.usePhotogrammetry)
        if ~isempty(obj.type)
        switch obj.type
          case 'clinical', out = false;
          case 'hdeeg128',    out = true;
          case 'ieeg',     out = false;
        end
        else
          out = false;
        end;
      else
        out = obj.usePhotogrammetry;
      end;
    end
    
    
    %% Get methods for the various directory structures.  If not set, use a default location.
    function out = get.dir_Data(modelObj)
      if isempty(modelObj.dir_Data)
        out = [modelObj.dir_Root modelObj.subDIR_DATA];
      else
        out = modelObj.dir_Data;
      end;
    end;

    function out = get.dir_AllModels(modelObj)
      if isempty(modelObj.dir_AllModels)
        out = [modelObj.dir_Root modelObj.subDIR_MODELS];
      else
        out = modelObj.dir_AllModels;
      end;
    end;

    function out = get.dir_Model(modelObj)
      if isempty(modelObj.dir_Model)
        out = [modelObj.dir_AllModels modelObj.headData.modelName '/'];
      else
        out = modelObj.dir_Model;
      end;
    end

    function out = get.dir_PhotoGram(modelObj)
      if isempty(modelObj.dir_PhotoGram)
        out = [modelObj.dir_Root modelObj.subDIR_PHOTOGRAM];
      else
        out = modelObj.dir_PhotoGram;
      end
    end
    
    function out = get.dir_SubModel(modelObj)
      % function out = get.dir_SubModel(modelObj)
      % 
      % Returns the subdirectory in /sourceanalysis/<ModelName>/ that the
      % electrode locations and leadfield will be stored in.
      %

      if ~isempty(modelObj.type)
      switch modelObj.type
        case 'hdeeg128', out = 'hdEEG_128/';    
        case 'clinical', out = 'clinical/';
        case 'ieeg',     out = 'ieeg/';        
        otherwise error('Unknown Model Type');
      end
      else
        out = '';
      end;
    end
    
    function set.type(modelObj,val)
      valid = validatestring(val,modelObj.VALID_TYPES);
      modelObj.type = valid;
    end
    
    function out = get.type(modelObj)
      if isempty(modelObj.type)
        warning('cnlModel:typeUnset','modelObj.type must be set before proceeding');
      end
      out = modelObj.type;
    end;
    
    function optionsSeg = get.opts_Seg(modelObj)
      % function optionsSeg = get.opts_Seg(modelObj)
      %
      % Returns a structure containing all model options related to the
      % construction of the full segmentation.
      %
      % 
      optionsSeg.skullThickness   = modelObj.skullThickness;
      optionsSeg.dir_Model        = modelObj.dir_Model;
      optionsSeg.fName_SegNRRD    = modelObj.fName_SegNRRD;
      optionsSeg.dwnSmpLvl_Seg    = modelObj.dwnSmpLvl_Seg;
      optionsSeg.force_SegRebuild = modelObj.force_SegRebuild;
      optionsSeg.type             = modelObj.type;
    end
    
    function optionsCond = get.opts_Cond(modelObj)
      % function optionsCond = get.opts_Cond(modelObj)
      % 
      % Returns a structure containing all model options related to the
      % construction of the conductivity image

      optionsCond.useDTIData = modelObj.useDTIData;
      optionsCond.usePVSulci = modelObj.usePVSulci;
      optionsCond.useAniso = true; %modelObj.useAniso;
      optionsCond.dir_Model = modelObj.dir_Model;
      optionsCond.fName_IsoNRRD = modelObj.fName_IsoNRRD;
      optionsCond.fName_AnisoNRRD = modelObj.fName_AnisoNRRD;
      optionsCond.force_CondRebuild = modelObj.force_CondRebuild;
      optionsCond.segDwnSample = modelObj.dwnSmpLvl_Seg;
      optionsCond.map_labeltoconductivity = modelObj.map_labeltoconductivity;
      optionsCond.whiteLabel = modelObj.labelWhite;
    end
    
    function optionsElec = get.opts_PhotoG(modelObj)
      % function optionsElec = get.opts_PhotoG(modelObj)
      %
      % Returns a structure containing all model options related to
      % obtaining a solution to the photogrammetry problem.
      optionsElec.usePhotogrammetry = modelObj.usePhotogrammetry;
      optionsElec.fName_PhotoGram   = modelObj.fName_PhotoGram;
      optionsElec.photoCostFunction = modelObj.photoCostFunction;
      optionsElec.photoModelTr = modelObj.photoModelTr;
      optionsElec.photoOptimization = modelObj.photoOptimization;
      optionsElec.force_PhotoG_recomp = modelObj.force_PhotoG_recomp;
    end
    
    function optionsFDModel = get.opts_FDModel(modelObj)
      % function optionsFDModel = get.opts_FDModel(modelObj)
      % 
      % Returns a structure containing all model options related to
      % construction of the finite difference model.
      %
      optionsFDModel.useAniso = modelObj.useAniso;
    end
    
    function optionsLField = get.opts_LeadField(modelObj)
      % function opts = get.opts_LeadField(modelObj)
      %
      % Returns a structure containing all model options related to
      % construction of the leadfield
      %
      optionsLField.fName_LeadField = modelObj.fName_LeadField;
      optionsLField.dir_Model = [modelObj.dir_Model modelObj.dir_SubModel];
      optionsLField.Electrodes = modelObj.Electrodes;
      optionsLField.gndElectrode = modelObj.gndElectrode;
      optionsLField.nrrdSeg = modelObj.nrrdModelSeg;
      optionsLField.FDModel = modelObj.FDModel;
      optionsLField.useConstrainedCortex = modelObj.useConstrainedCortex;
      optionsLField.dwnSmpLvl_LField_PreSave = modelObj.dwnSmpLvl_LField_PreSave;
      optionsLField.dwnSmpLvl_LField_PostSave = modelObj.dwnSmpLvl_LField_PostSave;
      optionsLField.force_LeadFieldRebuild = modelObj.force_LeadFieldRebuild;
      optionsLField.parallelComputation = modelObj.parallelComputation;
    end;

    function out = get.nrrdFinalSeg(obj)
      % function out = get.nrrdFinalSeg(obj)
      % 
      % Returns a downsampled version of the segmentation used to construct
      % the model. If this has not yet been computed, the function clones
      % obj.nrrdModelSeg and downsamples it by
      % obj.dwnSmpLvl_LField_PreSave.*obj.dwnSmpLvl_Lfield_PostSave
      %
      if ~isempty(obj.nrrdFinalSeg)&&isa(obj.nrrdFinalSeg,'file_NRRD')
        out = obj.nrrdFinalSeg;
        return;
      else
        % If it hasn't been computed yet, get it from obj.nrrdModelSeg
        if isa(obj.nrrdModelSeg,'file_NRRD')
          obj.nrrdFinalSeg = clone(obj.nrrdModelSeg,...
            'nrrdFullSegDwnSmp.nhdr',obj.dir_Model);
          obj.nrrdFinalSeg.DownSample(obj.totalLFieldDownSample,'segmentation');
          obj.nrrdFinalSeg.save;
          out = obj.nrrdFinalSeg;
        else
          warning('cnlModel:finalSegNotSet',['Attempted to access ' ...
                  'obj.nrrdFinalSeg without obj.nrrdModelSeg set first']);
          out = [];
          return;
        end    
      end;      
    end
      
    build(obj);
    
    disp(obj);
    
    
    function out = get.useAniso(obj)
      % function out = get.useAniso(obj)
      % Adds a warning and pause to access cnlModel.useAniso, so code gets
      % changed appropriately before it is ultimately removed.
      warning('cnlModel.useAniso is deprecated.  Please discontinue use');
      out = obj.useAniso;
    end;

    function out = get.totalDownSample(obj)
      out = obj.dwnSmpLvl_Seg;
      out = out.*obj.totalLFieldDownSample;
    end
    
    function out = get.totalLFieldDownSample(obj)
      out = obj.dwnSmpLvl_LField_PreSave;
      out = out.*obj.dwnSmpLvl_LField_PostSave;
    end
    
  end;
  methods (Static=true)
    FullSegmentation = build_Segmentation(headData,varargin);
    CondImg = build_Conductivity(nrrdSeg,varargin);
    nrrdCond = getDTIConductivities(nrrdCond,nrrdSeg,whiteLabel,DSLevel,nrrdDiffTensors);
    [condObj] = getPVSulci(condObj);
    [SymOut] = convert_DiffTensorToCondTensor(diffTensorVals,varargin)
  end
  
end