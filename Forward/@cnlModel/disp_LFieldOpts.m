function disp_LFieldOpts(obj)
  
  disp(['              LEADFIELD OPTIONS']);
disp(['%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%']);
disp(['           fName_LeadField: ' obj.fName_LeadField]);
disp(['              gndElectrode: ' num2str(obj.gndElectrode)]);
disp(['      useConstrainedCortex: ' BoolToString(obj.useConstrainedCortex)]);
disp(['  dwnSmpLvl_LField_PreSave: [' num2str(obj.dwnSmpLvl_LField_PreSave) ']']);
disp([' dwnSmpLvl_LField_PostSave: [' num2str(obj.dwnSmpLvl_LField_PostSave) ']']);
disp(['    force_LeadFieldRebuild: ' BoolToString(obj.force_LeadFieldRebuild)]);
disp([' ']);

end