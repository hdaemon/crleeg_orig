function [FullSegmentation] = build_Segmentation(headData,varargin) %options,modelDIR)
% function [fullSegmentation] =
%                  BUILD_SEGMENTATION(dirs,files,options)
%
% Function to combine multiple segmentations (Skin, ICC, Brain) into a
% single fully segmented head.  Currently uses a dilated intercranial
% cavity in place of an actual skull segmentation. All files are assumed to
% match in number of elements and spatial directions.
%
% Written By: Damon Hyde
% Created: Sept 15, 2008
% MAJOR Changes: March 2011
% More MAJOR Changes: Feb 2014
% Part of the cnlEEG Project
%

mydisp('Building Combined Segmentation from cnlHeadData object.');

% Input Parsing
p = inputParser;
addRequired(p,'headData',@(x) isa(x,'cnlHeadData'));
addParamValue(p,'skullThickness',4,@(x) isnumeric(x));
addParamValue(p,'dir_Model','./');
addParamValue(p,'fName_SegNRRD','nrrdFullSegmentation.nrrd');
addParamValue(p,'dwnSmpLvl_Seg',[1 1 1]);
addParamValue(p,'force_SegRebuild',[0]);
addParamValue(p,'type','hdeeg128');
parse(p,headData,varargin{:});

% Extract Options from Object for Simplicity
skullThickness = p.Results.skullThickness;
dir_Model      = p.Results.dir_Model;
fName_SegNRRD  = p.Results.fName_SegNRRD;
dwnSmpLvl_Seg  = p.Results.dwnSmpLvl_Seg;
forceRebuild   = p.Results.force_SegRebuild;

if ~forceRebuild&&(exist([dir_Model fName_SegNRRD],'file'))
  mydisp('Loading Existing Full Head Segmentation Image');
  FullSegmentation = file_NRRD(fName_SegNRRD,dir_Model);
else
  
  
  %% Initialize with the Skin Segmentation
  mydisp(['Cloning Skin Segmentation to Begin']);
  mydisp(['Skin Segmentation: ' headData.nrrdSkin.fname]);
  FullSegmentation = clone(headData.nrrdSkin,fName_SegNRRD,dir_Model);
  FullSegmentation.readOnly = false;
  headData.nrrdSkin.data = [];
  
  %% Determine Skull Segmentation
  if isempty(headData.nrrdSkull)&&~isempty(headData.nrrdICC)&&(skullThickness>0)
    mydisp('Dilating ICC to obtain skull region');
    mydisp(['ICC File: ' headData.nrrdICC.fname]);
    mydisp(['Skull Thickness: ' num2str(skullThickness)]);
    
    dilatedicc = dilate_ICC(headData.nrrdICC,skullThickness);
    Q = (dilatedicc>0);
    FullSegmentation.data(Q) = 2;
    
  elseif ~isempty(headData.nrrdSkull)
    mydisp('Skull Segmentation Present');
    mydisp(['SkullSeg File : ' headData.nrrdSkull.fname]);
    
    % Build the skull
    if length(unique(headData.nrrdSkull.data(:)))==1
      mydisp(['Skull segmentation has only a single compartment']);
      % One compartment skull model
      Q = (headData.nrrdSkull.data>0);
      FullSegmentation.data(Q) = 2;
    elseif length(unique(headData.nrrdSkull.data(:)))==2
      mydisp(['Skull segmentation has two compartments']);
      % Two compartment skull model
      Q = (headData.nrrdSkull.data==1);
      FullSegmentation.data(Q) = 2; % Hard Bone
      Q = (headData.nrrdSkull.data==2);
      FullSegmentation.data(Q) = 3; % Soft Bone
    else
      error('Not sure what to do with this skull segmentation. Too many segments in it');
    end
    
  else
    warning('No Skull Segmentation Present');
  end;
  
  %% Take the ICC and fill it with CSF
  if ~isempty(headData.nrrdICC)
    mydisp(['ICC Present: Filling with CSF']);
    mydisp(['ICC File: ' headData.nrrdICC.fname]);
    Q = headData.nrrdICC.data>0;
    FullSegmentation.data(Q) = 5;
    headData.nrrdICC.data = [] ; % No need to keep the image around
  end
  
  
  %% Determine Internal Brain Segmentation
  if ~isempty(headData.nrrdBrain)
    mydisp('Incorporating Internal Brain Structure');
    Q = (headData.nrrdBrain.data>0);
    FullSegmentation.data(Q) = headData.nrrdBrain.data(Q);
    headData.nrrdBrain.data = [];
  else
    warning('Brain segmentation not found.  Something is probably going wrong.');
  end;
  
%   %% Add iEEG Electrodes
%   if strcmpi(p.Results.type,'ieeg')
%     mydisp('Adding iEEG Electrodes into Segmentation');
%     %FullSegmentation.fname = 'nrrdFullSegmentation_iEEG.nhdr';
%     for i = 1:numel(headData.nrrdIEEG)
%       tmp = headData.nrrdIEEG(i).buildCondMap;
%       Q = find(tmp);
%       FullSegmentation.data(Q) = tmp(Q);
%     end;
%   end
  
  %% Downsample Segmentation if Necessary
  if any(dwnSmpLvl_Seg>1)
    mydisp('Downsampling Segmentation');
    FullSegmentation.DownSample(dwnSmpLvl_Seg,'segmentation');% = nrrdDownsample(nrrdFullSegmentation,options.modelDownSampleLevel);
  end;
  FullSegmentation.write;
  
end;


mydisp('%%%% Completed Building Combined Segmentation');
return;


function [dilatedicc] = dilate_ICC(icc,skullThickness)
tic
mydisp('Computing Dilation of ICC Segmentation');
aspect = icc.aspect;
filterSize = ceil(skullThickness./aspect);
filterSizeFull = 2*filterSize +1;

dilationFilter = zeros(filterSizeFull(1),filterSizeFull(2),filterSizeFull(3));

fX = (-filterSize(1):filterSize(1))*aspect(1);
fY = (-filterSize(2):filterSize(2))*aspect(2);
fZ = (-filterSize(3):filterSize(3))*aspect(3);
[fX fY fZ] = ndgrid(fX,fY,fZ);

dilationFilter = strel(sqrt(fX.^2 + fY.^2 + fZ.^2)<skullThickness);

dilatedicc = imdilate(icc.data,dilationFilter);
mydisp(['Completed Image Dilation in ' num2str(toc) ' seconds']);

return;
