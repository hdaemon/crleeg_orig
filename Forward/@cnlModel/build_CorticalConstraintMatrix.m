function [varargout] = build_CorticalConstraintMatrix(modelObj)
  % BUILD_CORTICALCONSTRAINTMATRIX 
  %
  % function [matCortConst, nrrdCortexConstraints] = BUILD_CORTICALCONSTRAINTMATRIX(dirs,files,options,finalPts)
  %
  % Build matrix to constrain solutions to the cortical surface normals,
  % usually obtained from the sulci extraction procedure
  %
  % Required Variables:
  %
  % Written By: Damon Hyde
  % Last Edited: 12/21/2015
  % Part of the cnlEEG Project
  %
  
  if isempty(modelObj.headData.nrrdSurfNorm)
    error('You must define modelObj.nrrdSurfNorm before calling build_CorticalConstraintMatrix');
  end;
  
  nrrdSurfNorm = modelObj.headData.nrrdSurfNorm;
  
  mydisp('Building Cortical Constraint Matrix');
  
  % Convert StreamLine Vectors to Cortical Tangents
  mydisp('Copy surface normal NRRD and downsampling to model resolution');
  nrrdCortConst = clone(nrrdSurfNorm, ...
    'cortconst_orig.nrrd',modelObj.dir_Model);
  
  if nrrdCortConst.existsOnDisk
    mydisp('Re-reading from existing cortconst_orig.nrrd');
    nrrdCortConst = ...
      file_NRRD('cortconst_orig.nrrd',modelObj.dir_Model);
  else
    mydisp('Building cortical constraint NRRD');
    nrrdCortConst.data = -nrrdCortConst.data;
    nrrdCortConst.downSample(modelObj.dwnSmpLvl_Seg);
    nrrdCortConst.normalizeVectors;
    
    % Find points to compute at
    done = false;
    toCheck = find(ismember(modelObj.nrrdModelSeg.data,[4 5 7]));
    
    % Preemptively remove those voxels which already have vectors.
    mydisp('Excluding points with vectors already computed');
    Vectors = nrrdCortConst.data(:,toCheck);
    Vectors = reshape(Vectors,[3,numel(toCheck)]);
    Vectors = sum(Vectors,1);
    toCheck(find(Vectors)) = [];
    
    mydisp(['Computing Approximate Surface Normals for Voxels ' ...
      ' Outside Extracted Grey Matter Region']);
    
    Vectors = nrrdCortConst.data;
    
    tic
    nIt = 0;
    while ~done
      nIt = nIt+1;
      mydisp([num2str(length(toCheck)) ' Voxels remain to be checked']);
      
      % Loop across voxels and update vector approximations
      for i = 1:length(toCheck)
        index = toCheck(i);
        % Get all vectors in the neighboring regions
        [x y z] = ind2sub(modelObj.nrrdModelSeg.sizes,index);
        sizes = modelObj.nrrdModelSeg.sizes;
        xRange = x-1:x+1; xRange(xRange<1) = []; xRange(xRange>sizes(1)) = [];
        yRange = y-1:y+1; yRange(yRange<1) = []; yRange(yRange>sizes(2)) = []; 
        zRange = z-1:z+1; zRange(zRange<1) = []; zRange(zRange>sizes(3)) = [];
        
        tmpVec = Vectors(:,xRange,yRange,zRange);
        tmpVec = reshape(tmpVec,[3 numel(tmpVec)/3]);
        if any(tmpVec(:))
          % We have some neighbors with vectors
          [u s v] = svd(tmpVec);
          Vectors(:,x,y,z) = u(:,1); % Take the first singular vector as the vector at this point
          toCheck(i) = -1;
        end;
        
        if mod(numel(toCheck),100000)==0
          mydisp(['Completed ' num2str(i) ' nodes in ' num2str(toc) ' seconds']);
        end;        
      end;
                  
      % Remove completed voxels from the list
      Q = find(toCheck==-1);
      toCheck(Q) = [];
               
      % Check if we're done
      if (numel(toCheck)==0)||(numel(Q)==0)
        % We're finished if we have done every voxel, or have completed a
        % loop without doing anything.
        done = true;
      end;
      disp(['Completed iteration for a total of ' num2str(toc) ' seconds']);
    end;
    
    % Update the data in the nrrdCortConst object and save
    nrrdCortConst.data = Vectors;
    nrrdCortConst.normalizeVectors;
    nrrdCortConst.readOnly = false;
    nrrdCortConst.write;
    
  end;
  
  % Build the Downsampled Version
  dwnSmp = modelObj.dwnSmpLvl_Seg.*modelObj.dwnSmpLvl_LField_PreSave.*modelObj.dwnSmpLvl_LField_PostSave;
  tailString = [num2str(dwnSmp(1)) '_' num2str(dwnSmp(2)) '_' num2str(dwnSmp(3))];
  
  dwnSmpFName = ['cortconst_dwnsmp_' tailString '.nrrd'];
  
  nrrdCortConstDownsampled = clone(nrrdCortConst, ...
    dwnSmpFName,modelObj.dir_Model);
  if nrrdCortConstDownsampled.existsOnDisk
    % Re-read the existing copy
    mydisp(['Reading previously constructed ' ... '
      dwnSmpFName ' file']);
    nrrdCortConstDownsampled = ...
      file_NRRD(dwnSmpFName,modelObj.dir_Model);
  else
    mydisp(['Constructing downsampled cortical constraints at final ' ...
      'leadfield resolution']);
    nrrdCortConstDownsampled.DownSample(dwnSmp);
    nrrdCortConstDownsampled.normalizeVectors;
    nrrdCortConstDownsampled.write;
  end;
      
  modelObj.LeadField.vecNRRD = nrrdCortConstDownsampled;
  modelObj.LeadField.matCollapse = modelObj.LeadField.buildCortConst;

  
  if nargout ==1
    varargout{1} = modelObj.LeadField.matCollapse;
  end;

  mydisp('Completed Cortical Constraint Matrix');
  return
  
  
