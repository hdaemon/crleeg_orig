
function  build(modelObj)
% function  BUILD(modelObj)
%
% Primary build method for cnlModel objects.  Taking a cnlHeadData object
% as input, this takes the various input MRI images and executes the
% following sequence of actions:
%
% 1) Constructs a full head segmentation, incorporating skin surface
% (required), skull, and four tissue brain segmentation
%
% 2) Identifies the nodes in segmentation that correspond to electrode
% locations
%
% 3) Builds isotropic and anisotropic conductivity images from the full
% head segmentation.  The isotropic images aren't really necessary at this
% point, since we only ever use the anistropic image.  Should remove that
% functionality at some point.
%
% 4) Constructs a finite difference model using the appropriate
% conductivity image (ie - the anisotropic one)
%
% 5) Builds a leadfield matrix using the finite difference model and the
% electrode locations
%
% 6) If information about cortical surface normals is available, constructs
% the cortical orientation constraint matrix.

%% Check configuration
modelObj.check_Configuration;

%% Combine Individual Segmentations and Build a Conductivity Image
disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%');
disp('%%   Constructing Full Segmentation                              %%');
disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%');
modelObj.nrrdModelSeg = cnlModel.build_Segmentation(modelObj.headData,modelObj.opts_Seg);

%% Get Electrode Locations and Corresponding Nodes
disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%');
disp('%%   Computing Electrode Locations                               %%');
disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%');
modelObj.Electrodes = getElectrodeLocations(modelObj);

if isempty(modelObj.measElectrode)
  modelObj.measElectrode = 1:modelObj.Electrodes.nElec;
end;

%% Check if we've already built the conductivity maps
disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%');
disp('%%   Constructing The Conductivity Image                         %%');
disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%');
nrrdSeg = modelObj.nrrdModelSeg;
nrrdDTI = modelObj.headData.nrrdDTI;
opts    = modelObj.opts_Cond;
modelObj.nrrdConductivity = cnlModel.build_Conductivity(nrrdSeg,nrrdDTI,opts);   
clear nrrdSeg nrrdDTI opts

%% Construct Finite Difference Model
disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%');
disp('%%   Building Finite Difference Model                            %%');
disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%');
modelObj.FDModel = cnlFDModel(modelObj.nrrdConductivity);
modelObj.FDModel.electrodes = modelObj.Electrodes;
modelObj.FDModel.fpath = modelObj.dir_Model;
if strcmpi(modelObj.type,'ieeg')
  % Change FD Model output filename if we're doing an iEEG model.
  modelObj.FDModel.fname = 'FDModel_iEEG.mat';
end;

% Set the filenames and paths appropriately based on the chosen electrode
% model
modelObj.FDModel.elecModel = modelObj.elecModel;
switch class(modelObj.elecModel)
  case 'cnlPEModel'
    % Point electrode model puts the FD Model in the main model directory
    modelObj.FDModel.fpath = modelObj.dir_Model;    
  case 'cnlCEModel'
    % Complete Electrode Model is electrode specific, and puts the file in
    % the submodel directory.
    tmp = modelObj.FDModel.fname;
    modelObj.FDModel.fname = [tmp(1:(end-4)) '_CEM.mat'];
    modelObj.FDModel.fpath = [modelObj.dir_Model modelObj.dir_SubModel];
end

modelObj.FDModel = modelObj.FDModel.build;

%% Build the LeadField matrix
disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%');
disp('%%   Building Leadfield Matrix                                   %%');
disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%');
if isempty(modelObj.measElectrode)
  modelObj.measElectrode = 1:modelObj.Electrodes.nElec;
end;

opts = modelObj.opts_LeadField;
opts.nrrdSeg = modelObj.nrrdModelSeg;
opts.FDModel = modelObj.FDModel;
opts.Electrodes = modelObj.Electrodes;
opts.measElectrode = modelObj.measElectrode;
opts.gndElectrode = modelObj.gndElectrode;
modelObj.LeadField = cnlLeadField(opts);

%% Build Cortical Constraint Matrix
if ~isempty(modelObj.headData.nrrdSurfNorm)
disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%');
disp('%%   Building Cortical Orientation Constraint Matrix             %%');
disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%');  
  modelObj.LeadField.matCollapse = modelObj.build_CorticalConstraintMatrix;
end


%% Save The Results of Building the Model
disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%');
disp('%%   Completed Model Construction                                %%');
disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%');

% cd([dirs.ProcessedRootDIR dirs.SaveMATFiles]);
% fname = 'Model_';
% 
% if modelOptions.useAniso,             fname = [fname 'Aniso'];
% else,                                 fname = [fname 'Iso'];          end;
% 
% if modelOptions.usePhotogrammetry,    fname = [fname '_128Lead'];
% else,                                 fname = [fname '_10-20'];       end;
% 
% if modelOptions.useDTIData,           fname = [fname '_DTI'];         end;
% if modelOptions.useMNEPatches,        fname = [fname '_MNEPatch'];    end;
% if modelOptions.useConstrainedCortex, fname = [fname '_ConstCortex']; end;
% 
% modelOptions.OutputFileName = [fname '_' date ':' timestring];
% 
% clear fname;

% modelOut.NRRDS = modelObj;
% modelOut.FDModel = FDModel;
% modelOut.LeadField = LeadField;

%save(modelOptions.OutputFileName);

return;

