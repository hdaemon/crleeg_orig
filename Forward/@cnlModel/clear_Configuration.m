function clear_Configuration(modelObj)
  % function clear_Configuration(modelObj)
  %
  % Clear all filenames and directories, leaving only modelObj.dir_Root
  % intact.
  %
 
  modelObj.clear_SubDirs;
  modelObj.clear_fNames;
    
end