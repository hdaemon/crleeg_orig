function [nrrdAnisoCond] = build_Conductivity(nrrdSeg,varargin)
% function [nrrdIsoCond nrrdAnisoCond] = BUILD_CONDUCTIVITY(nrrdSeg,condObj)
%
% Given a segmentation image and a set of conductivity options, return
% isotropic and anisotropic conductivity images
%

% Input Parsing
p = inputParser;
addRequired(p,'nrrdSeg',   @(x) isa(x,'file_NRRD'));
addOptional(p,'nrrdDTI',[],@(x) isa(x,'file_NRRD')||iscell(x)||isempty(x));
addParamValue(p,'useDTIData',false, @islogical);
addParamValue(p,'usePVSulci',false, @islogical);
addParamValue(p,'useAniso',true, @islogical);
addParamValue(p,'dir_Model','./', @(x) exist(x,'dir'));
addParamValue(p,'fName_IsoNRRD',    'IsotropicConductivity.nhdr');
addParamValue(p,'fName_AnisoNRRD','AnisotropicConductivity.nhdr');
addParamValue(p,'map_labeltoconductivity',[]);
addParamValue(p,'force_CondRebuild',false);
addParamValue(p,'segDwnSample',[]);
addParamValue(p,'whitelabel',7);

parse(p,nrrdSeg,varargin{:});

nrrdSeg         = p.Results.nrrdSeg;
nrrdDTI         = p.Results.nrrdDTI;
useDTIData      = p.Results.useDTIData;
usePVSulci      = p.Results.usePVSulci;
useAniso        = p.Results.useAniso;
dir_Model       = p.Results.dir_Model;
fName_IsoNRRD   = p.Results.fName_IsoNRRD;
fName_AnisoNRRD = p.Results.fName_AnisoNRRD;
force_Rebuild   = p.Results.force_CondRebuild;
segDwnSample    = p.Results.segDwnSample;

map_labeltoconductivity = p.Results.map_labeltoconductivity;

%% Get the Conductivity Image
if ~force_Rebuild&&exist([dir_Model fName_AnisoNRRD],'file')
  %% If it's already been writtin out, just load it.
  mydisp('Loading Existing Anisotropic Conductivity');
  nrrdAnisoCond = file_NRRD(fName_AnisoNRRD,dir_Model);
else      
  %% Construct Isotropic Conductivity Tensors from the Segmentation
  mydisp('Building Anisotropic Conductivity')
  nrrdAnisoCond               = clone(nrrdSeg,fName_AnisoNRRD,dir_Model);
  nrrdAnisoCond.content       = 'AnisotropicConductivity';
  nrrdAnisoCond.type          = 'float';
  nrrdAnisoCond.kinds         = { '3D-symmetric-matrix' nrrdAnisoCond.kinds{:} };
  nrrdAnisoCond.sizes         = [6 nrrdSeg.sizes];
  nrrdAnisoCond.dimension     = 4;
  nrrdAnisoCond.data          = zeros([6 nrrdSeg.sizes]);
  nrrdAnisoCond.data(1,:,:,:) = map_labeltoconductivity(nrrdSeg.data+1);
  nrrdAnisoCond.data(4,:,:,:) = map_labeltoconductivity(nrrdSeg.data+1);
  nrrdAnisoCond.data(6,:,:,:) = map_labeltoconductivity(nrrdSeg.data+1);  
      
  %% Replace White Matter Conductivities w/ Those Computed from DTI
  if useDTIData
    if isempty(nrrdDTI)
      error('DTI NRRD Not supplied');
    end;
    nrrdAnisoCond = cnlModel.getDTIConductivities(nrrdAnisoCond,nrrdSeg,nrrdDTI,p.Results.whitelabel,segDwnSample);
  end;
  
  %% Partial Volume Fractions Within Sulci Regions
  if usePVSulci
    error('Partial Volume Sulci not yet fully implemented');
    condObj = cnlModel.getPVSulci(condObj);
  end;
  
  %% Write Out The Anisotropic Conductivity Image
  nrrdAnisoCond.write;
end;

nrrdAnisoCond.data = [];

%% Select the correct conductivity image for the model being constructed
% (The isotropic model is all but totally extinct now)
if useAniso
  nrrdCond = nrrdAnisoCond;
else
  nrrdCond = nrrdIsoCond;
end;

end
