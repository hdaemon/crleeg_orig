function clear_AllDirs(modelObj)
  % function clear_AllDirs(modelObj)
  %
  % Clear all directories, including modelObj.dir_Root
  %
  modelObj.dir_Root = [];
  modelObj.clear_SubDirs;
    
end