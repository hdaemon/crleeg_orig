function out = check_Directories(modelObj)
  % function checkDirectories(modelObj)
  %
  % Check that the directory structure of the model object is properly
  % constructed.  This checks that:
  %
  % 1) modelObj.dir_Root is set and that the directory exists
  % 2) modelObj.dir_AllModels exists
  % 3) modelObj.dir_Model exists.  Creates directory if necessary.
  % 4)
  %
  % Returns true if the current structure passes all tests.  Returns false
  % if one or more tests failed.
  %
  % Written By: Damon Hyde
  % Last Edited: April 2015
  % Part of the cnlEEG Project
  %
    
  out = true;
  
  % Check that the root directory has been set
  if isempty(modelObj.dir_Root)
    warning('cnlModel:rootDirNotSet','Root directory has not been set');
    out = false;
  end;
  
  % Check that the root directory exists
  if ~exist(modelObj.dir_Root,'dir')
    warning('cnlModel:rootDirNonexistant',...
      ['Root directory: ' modelObj.dir_Root ' does not exist']);
    out = false;
  end;
  
  % If head data hasn't been provided, check that dir_Data exists
  if isempty(modelObj.headData)
  if ~exist(modelObj.dir_Data,'dir')
    warning('cnlModel:dataDirNonexistant',...
      ['Data directory: ' modelObj.dir_Data ' does not exist']);
    out = false;
  end
  end;
  
  %
  if ~exist(modelObj.dir_AllModels,'dir')
    warning('cnlModel:noModelDirectory',...
      ['Model directory: ' modelObj.dir_AllModels ' does not exist']);
    out = false;
  end
  
  if ~isempty(modelObj.headData)
  if ~exist(modelObj.dir_Model,'dir')
    mydisp(['Creating ' modelObj.dir_Model ' to store all model files']);
    mkdir(modelObj.dir_Model);   
  end
  end;
  
  if ~exist(modelObj.dir_PhotoGram,'dir')
   warning('cnlModel:photoGramDirNonexistant',...
     ['Photogrammetry Directory: ' modelObj.dir_PhotoGram ...
       ' does not exist']);
   out = false;
  end
  
  if ~exist(fullfile(modelObj.dir_Model,modelObj.dir_SubModel),'dir')
    mkdir(fullfile(modelObj.dir_Model,modelObj.dir_SubModel));
  end;
      
end