function [ElectrodesOut] = getElectrodeNodes(modelObj)
% GETELECTRODENODES cnlModel method to identify electrode locations
%
% function [ElectrodesOut] = getElectrodeNodes(modelObj)
%
% Written By: Damon Hyde
% Last Edited: April 2015
% Part of the cnlEEG Project
%

% Move into the correct directory
% Is this superfluous now?
if ~exist([modelObj.dir_Model modelObj.dir_SubModel],'dir')    
    mkdir([modelObj.dir_Model modelObj.dir_SubModel])
end;
cd([modelObj.dir_Model modelObj.dir_SubModel]);

% Check if we've already built the ElectrodeNodes.mat file
if (~modelObj.force_PhotoG_recomp)&&(exist('ElectrodeNodes.mat','file'))
  mydisp('Using Existing Electrode Nodes');
  load ElectrodeNodes.mat;

else
  
  mydisp('Electrodes not yet solved for.');
  
  % Check if we're able to display things.
  if usejava('jvm') && ~feature('ShowFigureWindows')
    error('Swap to a matlab session with graphical display');
  end
   
  %% Get Electrode Locations from Photogrammetry
  [ Electrodes] = getElectrodeLocations(modelObj);
    
  %% Visualize things
%   mydisp('Check final electrode alignment');
%   Electrodes.plot3D('surfMRI',modelObj.headData.nrrdSkin);
%   keyboard;  
  
  % Determine whether electrodes should be moved to the scalp surface
  if strcmpi(modelObj.type,'ieeg'),    
          pushToSurf = false;
  else    pushToSurf = true;  end;
  
  %% Convert (x,y,z) Electrode Locations to Node Indices
  if modelObj.useAniso
    Electrodes = Electrodes.mapToNodes(modelObj.nrrdModelSeg,'aniso',pushToSurf);
  else
    Electrodes = Electrodes.mapToNodes(modelObj.nrrdModelSeg,'iso',pushToSurf);
  end
  
  Electrodes.plot3D('surfMRI',modelObj.headData.nrrdSkin);
  mydisp('Check alignment after shifting to scalp surface');
  keyboard;
      
  %% Save out the ElectrodeNodes structure
  cd([modelObj.dir_Model modelObj.dir_SubModel]);
  save ElectrodeNodes Electrodes;
          
end;

ElectrodesOut = Electrodes;

end
