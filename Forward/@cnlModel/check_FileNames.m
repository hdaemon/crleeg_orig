function out = check_FileNames(obj)
  % function out = check_FileNames(obj)
  %
  % Check that all filenames have been appropriately set, and set defaults
  % if necessary
  
  if isempty(obj.fName_Model)
    mydisp('Using default model filename');
    obj.fName_Model = addIEEG(obj.FNAME_MODEL);
  end
  mydisp(['Model FileName: ' obj.fName_Model]);
  
  if isempty(obj.fName_SegNRRD)
    mydisp('Using default segmentation filename');    
    obj.fName_SegNRRD = addIEEG(obj.FNAME_SEGNRRD);
  end
  mydisp(['Segmentation Filename: ' obj.fName_SegNRRD]);
  
  if isempty(obj.fName_IsoNRRD)
    mydisp('Using default isotropic conductivity filename');
    obj.fName_IsoNRRD = addIEEG(obj.FNAME_ISONRRD);
  end
  mydisp(['Isotropic Conductivity Filename: ' obj.fName_IsoNRRD]);
  
  if isempty(obj.fName_AnisoNRRD)
    mydisp('Using default anistropic conductivity filename');
    obj.fName_AnisoNRRD = addIEEG(obj.FNAME_ANISONRRD);
  end
  mydisp(['Anisotropic Conductivity Filename: ' obj.fName_AnisoNRRD]);
  
  if isempty(obj.fName_PhotoGram)&&obj.usePhotogrammetry
    error('cnlModel:noPhotogrammetry',...
      'cnlModel.fName_PhotoGram must be set to use photogrammetry');
  end
  
  if isempty(obj.fName_LeadField)
    mydisp('Using default leadfield filename');
    mydisp('By default, LeadFields now add the ground electrode index ');
    mydisp('to the filename.  To account for this with older leadfields');
    mydisp('either change the filename of LeadField.mat, or set ');
    mydisp('model.fName_LeadField to ''LeadField.mat''');
    tmp = obj.FNAME_LEADFIELD;
    [~,name,ext] = fileparts(tmp);
    name = [name '_GND' num2str(obj.gndElectrode)];
    obj.fName_LeadField = addIEEG([name ext]);
  end;

  function out = addIEEG(fname)  
    switch obj.type
      case 'clinical'
        out = fname;
      case 'hdeeg128'
        out = fname;
      case 'ieeg'
        [~,name,ext] = fileparts(fname);  
        out = [name '_iEEG' ext];
    end    
  end
  
  
end


