

function [AnisoTensor] = getAnisoTensor(CSFFraction)
InPlane = CSFFraction*1.76 + (1-CSFFraction)*0.33
Transverse = 1/(CSFFraction/1.76 + (1-CSFFraction)/0.33);
AnisoTensor = [ Transverse 0 0 ; 0 InPlane 0 ; 0 0 InPlane];
end