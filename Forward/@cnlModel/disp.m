function disp(obj)
% DISP Overloaded display method for cnlModel objects
%
% function DISP(obj)
%
% Overloaded display function for cnlModel objects.
%
% Written By: Damon Hyde
% Last Edited: April 2015
% Part of the cnlEEG Project
%


disp(['                    DIRECTORIES']);
disp(['%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%']);
disp(['         Root Directory: ' trimpath(obj.dir_Root)]);
disp(['     AllModel Directory: ' trimpath(obj.dir_AllModels)]);
disp(['        Model Directory: ' trimpath(obj.dir_Model)]);
disp(['     SubModel Directory: ' trimpath(obj.dir_SubModel)]);
disp(['         Data Directory: ' trimpath(obj.dir_Data)]);
disp(['    Photogram Directory: ' trimpath(obj.dir_PhotoGram)]);
disp([' ']);

obj.disp_SegOpts;
obj.disp_CondOpts;
obj.disp_PhotogOpts;

disp(['      FINITE DIFFERENCE OPTIONS']);
disp(['%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%']);
disp(['               useAniso: ' BoolToString(true)]);
disp([' ']);

obj.disp_LFieldOpts;

disp(['                  OTHER OPTIONS']);
disp(['%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%']);
disp(['            OutputFileName: ' obj.OutputFileName]);
disp(['       parallelComputation: ' BoolToString(obj.parallelComputation)]);




end

function p = trimpath(p)
% trimpath Trim pathnames to 40 characters
%
% function p = trimpath(p)
%

if length(p)>40
  p = ['...' p(end-40:end)];
end;

end