function disp_CondOpts(obj)

disp(['           CONDUCTIVITY OPTIONS']);
disp(['%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%']);
disp(['          fName_IsoNRRD: ' obj.fName_IsoNRRD]);
disp(['        fName_AnisoNRRD: ' obj.fName_AnisoNRRD]);

if isa(obj.nrrdConductivity,'file_NRRD')
disp(['       nrrdConductivity: ' obj.nrrdConductivity.fname]);
else
disp(['       nrrdConductivity: EMPTY']);
end;

disp(['             useDTIData: ' BoolToString(obj.useDTIData)]);
if obj.useDTIData
  if isa(obj.headData.nrrdDTI,'file_NRRD')
  disp(['              nrrdDTI: ' obj.headData.nrrdDTI.fname]);
  end;
end


disp(['             usePVSulci: ' BoolToString(obj.usePVSulci)]);
if obj.usePVSulci
  disp(['            nrrdPVSulci: ' obj.headData.nrrdPVSulci.fname]);
end;


disp(['            CSFFraction: ' num2str(obj.CSFFraction)]);
disp(['map_labeltoconductivity: ']);
disp(['      force_CondRebuild: ' BoolToString(obj.force_CondRebuild)]);
disp([' ']);

end