function [condObj] = getPVSulci(condObj) 

  mydisp('Using Anisotropic Conductivities in Extracted Sulci Regions');
  
  Q = find((condObj.PVSeg.data==10)|(condObj.nrrdPVSeg.data==12));
  
  if ~isempty(condObj.CSFFraction)
    % Using a single fixed fraction
    AnisoTensor = getAnisoTensor(condObj.CSFFraction);
    useFile = false;
  else
    % Using a file of individual fractions
    PVFractions = condObj.nrrdCSFFractions.data;
    useFile = true;
  end;
  
  ConstraintVectors = condObj.nrrdCortConst.data;
  for idxVox = 1:length(Q)
    %% Get Neighboring Tangents
    [a b c] = ind2sub(nrrdSeg.sizes,Q(idxVox));
    tmpVec = ConstraintVectors(:,(a-1):(a+1),(b-1):(b+1),(c-1):(c+1));
    tmpVec = reshape(tmpVec,[3 27]);
    
    %% Get Starting Tensor
    if useFile
      AnisoTensor = getAnisoTensor(PVFractions(a,b,c));
    end;
    
    %% Rotate Into Place
    [ u s d] = svd(tmpVec);
    if any(diag(s))
      tensor = u*AnisoTensor*u';
    elseif condObj.CSFFraction==1
      tensor = [1.79 0 0 ; 0 1.79 0 ; 0 0 1.79];
    elseif condObj.CSFFraction==0;
      tensor = [0.33 0 0 ; 0 0.33 0 ; 0 0 0.33];
    else
      tensor = [1.79 0 0 ; 0 1.79 0 ; 0 0 1.79];
    end;
    tensor = tensor([1 2 3 5 6 9]);
    
    tmpCondData(:,a,b,c) = tensor(:);
  end;
  
end