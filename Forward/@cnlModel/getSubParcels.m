function parcelsOut = getSubParcels(modelObj,varargin)
% GETSUBPARCELS Get subparcellations to build cnlParcelSolutionSpace objects
%
% function parcelsOut = getSubParcels(modelObj,varargin)
%
% Generates a subparcellation of the cortical space to be used in the
% construction of a cnlParcelSolutionSpace object.
%
% Executes Several Functions:
%   1) Maps the desired parcellation onto the model segmentation
%       NOTE: This assumes that segmentation label 4 is cortex. This will 
%               be fixed soon
%   2) Applies a mask, if provided
%   3) Computes the desired subparcellation
%
% This function will, by default, look in modelObj.dir_Model to see if a
% file with the desired output name already exists. If it does, and
% forcerebuild has not been set to false, it will just load this
% subparcellation from the file
%
% NOTE: This documentation is out of date.
%
% Inputs:
%   modelObj        : cnlModel Object
%
% Optional Argument-Value Pair Inputs:
%   mask      : Mask image to use. Must be a file_NRRD object.
%   ntarget   : Target number of parcels in the final
%                       subparcellation.
%                     DEFAULT: 1000
%   useparcel : Which parcellation to use as the basis for
%                       subparcellation.
%                     DEFAULT: newModel.headData.nrrdIBSR
%   type      : What type of parcellation algorithm to use.
%                     DEFAULT: hybrid
%   fname     : Base name for output of subparcellations
%                     DEFAULT: subParcel
%   forcerebuild : Flag to force recomputation of the subparcellation even
%                     when a file with the appropriate name already exists.
%
% Outputs:
%   parcelsOut : A cell array of cnlParcellation objects
%
% Written By: Damon Hyde
% Last Edited: Jan 21, 2016
% Part of the cnlEEG Project
%

%% Input Parsing
p = inputParser;
p.addParamValue('mask',[],@(x) isa(x,'file_NRRD'));
p.addParamValue('ntarget',1000,@(x) isnumeric(x));
p.addParamValue('useparcel',[]);
p.addParamValue('useseg',modelObj.nrrdModelSeg,@(x) isa(x,'file_NRRD'));
p.addParamValue('type','hybrid');
p.addParamValue('fname','subParcel');
p.addParamValue('forcerebuild',false);
parse(p,varargin{:});

nrrdMask        = p.Results.mask;
nParcelTarget   = p.Results.ntarget;
useParcellation = p.Results.useparcel;
type  = p.Results.type;
fname_orig = p.Results.fname;
forcerebuild = p.Results.forcerebuild;
segmentationToUse = p.Results.useseg;


if numel(forcerebuild)==1
  forcerebuild = logical(forcerebuild*ones(1,numel(nParcelTarget)));
end;

%% Initialize Output and Check to See if the Files Have Already been Computed
subParcel = cell(1,numel(nParcelTarget));

for idxP = 1:numel(nParcelTarget)
  fname = [fname_orig num2str(nParcelTarget(idxP)) '.nrrd'];
  if (~forcerebuild(idxP))&&(exist([modelObj.dir_Model fname],'file'))
    mydisp([fname ' located. Using existing file']);
    subParcel{idxP} = cnlParcellation(fname,modelObj.dir_Model);
  else
    mydisp([fname ' will be computed']);
  end
end
   
% If we found all the files, we're all done.
if ~any(cellfun(@isempty,subParcel)), 
  for i = 1:length(nParcelTarget)
    parcelsOut{i} = subParcel{i}.ensureConnectedParcels(...
      'fname',subParcel{i}.fname,'fpath',subParcel{i}.fpath);
  end;
  return; 
end;

%% Prep the Mapped Parcellation

% Get the parcellation to start with
parcelToMap = getParcelToMap(modelObj,useParcellation);
mydisp(['Using parcellation: ' parcelToMap.fname ' as initial subdivision of cortex']);

parcelToMap.downSample(modelObj.totalLFieldDownSample,'segmentation');

% Map onto the segmentation
mydisp(['Mapping parcellation onto: ' segmentationToUse.fname ]);
nrrdMappedParcellation = parcelToMap.mapToSegmentation(...
  segmentationToUse,'fname','mappedParcel.nrrd','fpath',modelObj.dir_Model);

% If provided, apply the mask to the mapped parcellation
if ~isempty(nrrdMask)
  mydisp(['Applying mask from ' nrrdMask.fname ]);
nrrdMappedParcellation.data = nrrdMappedParcellation.data.*nrrdMask.data;
end;

% Set noncortical voxels to zero
nrrdMappedParcellation.removeWhiteMatter;
nrrdMappedParcellation.removeSubCorticalGray;
nrrdMappedParcellation.removeCSF;
     
% Get the orientation constraints from the leadfield.
nrrdCortConst = modelObj.LeadField.vecNRRD;

%% Compute the Subparcellation
for idxP = 1:length(nParcelTarget)
  fname = [fname_orig num2str(nParcelTarget(idxP)) '.nrrd'];
  if isempty(subParcel{idxP})
    % Didn't load it, so now let's build it.
    mydisp('Computing new subparcellation');
    subParcel{idxP} = nrrdMappedParcellation.subparcellateByLeadfield(...
          nrrdCortConst,type,modelObj.LeadField,nParcelTarget(idxP),fname);
    %    keyboard;
    subParcel{idxP} = subParcel{idxP}.ensureConnectedParcels(...
                 'fname',subParcel{idxP}.fname, 'fpath', subParcel{idxP}.fpath);
    subParcel{idxP}.readOnly = false;
    subParcel{idxP}.encoding = 'gzip';
    subParcel{idxP}.write;
  end;
  
end;

parcelsOut = reshape(subParcel,size(nParcelTarget));

end

function selectedParcel = getParcelToMap(modelObj,select)
% function selectedParcel = getParcelToMap(modelObj,select)
%
% Select the appropriate parcellation to use as the basis for the
% subparcellation
%

if ~exist('select','var'), select = []; end;

if isempty(select)
  % Scan for defaults
  if ~isempty(modelObj.headData.nrrdNVM)
    selectedParcel = modelObj.headData.nrrdNVM;
    %nrrdMappedParcellation = modelObj.headData.nrrdNVM.mapToSegmentation(...
    %     modelObj.nrrdModelSeg,'mappedNVM.nrrd',modelObj.dir_Model);  
  elseif ~isempty(modelObj.headData.nrrdIBSR)
    selectedParcel = modelObj.headData.nrrdIBSR;
    %nrrdMappedParcellation = modelObj.headData.nrrdIBSR.mapToSegmentation(...
    %     modelObj.nrrdModelSeg,'mappedIBSR.nrrd',modelObj.dir_Model);
  elseif ~isempty(modelObj.headData.nrrdNMM)
    selectedParcel = modelObj.headData.nrrdNMM;
    %nrrdMappedParcellation = modelObj.headData.nrrdNMM.mapToSegmentation(...
    %     modelObj.nrrdModelSeg,'mappedNMM.nrrd',modelObj.dir_Model);
  else
    error('Subparcellations require that either headData.IBSR or headData.NMM be set');
  end
else
  assert(ismember(upper(select),{'IBSR' 'NVM' 'NMM'}),...
    'Requested parcellation must be IBSR, NVM, or NMM');
  selectedParcel = modelObj.headData.(['nrrd' upper(select)]);
  assert(isa(selectedParcel,'cnlParcellation'),...
    'Requested parcellation does not exist');
end

end
