function check_Compatibility(modelObj)
  % function check_Compatibility(modelObj)
  % 
  % Compatibility check to make sure older scripts work properly with the
  % new model setup approach.
  %
  % Written By: Damon Hyde
  % March 18, 2015
  % Part of the cnlEEG Project
  %
   
  if ~isempty(modelObj.dir_Root)&&exist(modelObj.dir_Root,'dir')
 
    % Check the directory structure, assuming that dir_Root is in fact the
    % root directory.  The way cnlModel used to be called, this would
    % actually be the model directory.
    test = modelObj.check_Directories;
    
    if ~test
      mydisp('Trying compatibility mode for older scripts');
      
      % Assume the provided directory is the model directory
      tmp = modelObj.dir_Root;
      cd(modelObj.dir_Root)
      cd('../../../');
      modelObj.dir_Root = [pwd '/'];
      modelObj.dir_Model = tmp;
      test = modelObj.check_Directories;
      if ~test
        warning('Compatibility mode failed');
        modelObj.dir_Root = tmp;
      end;
      
    end
  

  end
    
end