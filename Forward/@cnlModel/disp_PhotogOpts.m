function disp_PhotogOpts(obj)
  
 disp(['         PHOTOGRAMMETRY OPTIONS']);
disp(['%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%']);
disp(['      usePhotogrammetry: ' BoolToString(obj.usePhotogrammetry)]);
if obj.usePhotogrammetry
disp(['        fName_PhotoGram: ' obj.fName_PhotoGram]);
disp(['      photoCostFunction: ' obj.photoCostFunction]);
disp(['           photoModelTr: ' obj.photoModelTr]);
disp(['      photoOptimization: ' obj.photoOptimization]);
disp(['    force_PhotoG_recomp: ' BoolToString(obj.force_PhotoG_recomp)]);
end;
disp([' ']); 
  
end