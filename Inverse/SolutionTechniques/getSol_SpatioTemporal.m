function [SolutionOut, varargout] = getSol_SpatioTemporal(LeadField,Data,reconOptions)
%
% function [SolutionOut] = getSol_SpatioTemporal(LeadField,Data,options)
%
% Obtain a solution using our SpatioTemporal approach solved with conjugate
% gradient on the normal equations.
%

nElectrodes = size(Data,1);
nTimePoints = size(Data,2);

%% Get Spatial Regularizer
SpaceReg = get_SpatialRegularizer(LeadField,Data,reconOptions);

%% Get Temporal Regularizer
TempReg = get_TemporalRegularizer(LeadField,Data,reconOptions);

%% Build Full Regularizer
FullRegularizer = cnlReg_LinearSpatioTemporal(SpaceReg,TempReg);

%% Spatially weighted min norm - normalizes the columns of the weight matrix
if reconOptions.WeightLeadField&&LeadField.isWeighted
  mydisp('Weighting Leadfield');
  wantOmegaInverse = true;
  OmegaInv = LeadField.getWeightingMatrix(wantOmegaInverse);
else
  mydisp('Leaving Leadfield Unweighted');
  OmegaInv = speye(size(LeadField,2));
end;

wantOmegaInverse = false;
Omega = LeadField.getWeightingMatrix(wantOmegaInverse);


%% Solve Problem Using CG on the Normals for Each Lambda Value
mydisp(['Using ' num2str(length(reconOptions.lambda_t)) ' temporal regularization values']);
mydisp(['Using ' num2str(length(reconOptions.lambdas)) ' spatial regularization values']);

% Loop Across Temporal Lambdas (The temporal regularization ends up being
% lambda_t(j)*lambdas(i)

% LeadField2 = LeadField;
% LeadField2 = LeadField2.setWeighting(false);

%keyboard;

solIndex = 1;

if reconOptions.useDependantLambdaT
  timeIndex = length(reconOptions.tempFactor)
else
  timeIndex = length(reconOptions.lambda_t);
end;

mydisp('Starting Matlab Pool');
cnlStartMatlabPool;

mydisp('Starting Inverse Computations');
pause(0.1);

tmpLField = LeadField.currMatrix;

% Loop Across Temporal Regularization
for j = 1:timeIndex
  % Loop Across Spatial regularization parameters

 %tmpLCurve   = zeros(length(reconOptions.lambdas),2);
 %tmpLCurve_t = zeros(length(reconOptions.lambdas),2,size(Data,2));
 clear tmpSol tmpLCurve tmpLCurve_t

  nLam = length(reconOptions.lambdas);
  parfor i = 1:nLam
    solIndex = nLam*(j-1) + i;
 
    mydisp(['tIdx: ' num2str(j) ' sIdx: ' num2str(i)]);
    pause(0.1);
    
    tmpReg = FullRegularizer;
        
     [tmpSol{i} tmp1 tmp2] = ...
       doSolve(tmpLField,Data,FullRegularizer,reconOptions,solIndex);
    
    tmpLCurve{i} = tmp1';
    tmpLCurve_t{i} = tmp2;
              
%     % Terminate loop early if the residual norm gets too big.
%     if tmp2>0.25
%       break;
%     end
    
  end;

  Solution(:,j) = tmpSol;  
  
  LCurve(:,j,:) = cell2mat(tmpLCurve)';
  for idxI = 1:length(tmpLCurve_t)
  LCurve_t(idxI,j,:,:) = tmpLCurve_t{idxI};
  end;
end;


% Compute true errors (if possible)
if ~isempty(reconOptions.xTrue)
  for i = 1:size(Solution,1)
    for j = 1:size(Solution,2)
      trueErrors(i,j) = norm(Solution{i,j}-reconOptions.xTrue,'fro');
    end;    
  end;
else
  trueErrors = [];
end;

% Construct output structure
SolutionOut.solutions   = Solution;
SolutionOut.LCurve      = LCurve;
SolutionOut.LCurve_t    = LCurve_t;
SolutionOut.options     = reconOptions;
SolutionOut.Regularizer = FullRegularizer;
SolutionOut.trueErrors  = trueErrors;

return;



function solStruct = SaveSolution(solStruct)
try
done = false;
idx = 1;
fnamebase = get_SolutionName(solStruct.options);
while ~done
  fname = ['SpatTempSol_' fnamebase '_' num2str(idx) '.mat'];
  if ~exist(fname)
    done = true;
  else
    idx = idx+1;
  end;
end;
solStruct.filename = fname;
save(fname,'solStruct');
catch
  keyboard;
end;
return;

function [Solution LCurve LCurve_t ] = doSolve(LeadField,Data,FullRegularizer,reconOptions,solIndex)
        
    tmpRegularizer = FullRegularizer;
    % Set Regularization Parameters for this Inversion
    tmpRegularizer.lam_time = reconOptions.allLambda_t(solIndex);
    tmpRegularizer.lam_space   = reconOptions.allLambdas(solIndex);
    
    % Get Solution using conjugate gradient on the normal equations
    mydisp(['Solving with lambda = ' num2str(reconOptions.allLambdas(solIndex)) ' and temporal factor: ' num2str(reconOptions.allLambda_t(solIndex))]);
    Solution = Solver_CG_Normals(LeadField,tmpRegularizer,Data,reconOptions.initialGuess,reconOptions.niters,reconOptions.tol);
     
    % Compute LCurve Components
    tmpRegularizer.lam_space   = 1;
    tmpRegularizer.lam_time = 1;
    [LCurve LCurve_t] = get_LCurves(Solution,Data,LeadField,tmpRegularizer);   
               
    mydisp(['  Residual Norm: '  num2str(LCurve(1)) '  Regularization Norm:' num2str(LCurve(2)) ]);

return

