function [Solution varargout] = getSol_LORETA(LeadField,Data,SpaceSize,SolutionPoints,dist,lambdas)

disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%');
disp('Obtaining a solution using LORETA');

if ~exist('lambdas'), lambdas = logspace(-6,1,100); end;

% not the right thing to do, but for now, since the voxels are close to
% cubic.
dist = mean(dist);

%% Build Laplacian Matrix:
disp('Building Laplacian Matrix');
disp(['Solution Space is of Size: ' num2str(SpaceSize(1)) ' x ' num2str(SpaceSize(2)) ' x ' num2str(SpaceSize(3))]);

% Get Indices to All Voxels
[idxX idxY idxZ] = ndgrid(1:SpaceSize(1),1:SpaceSize(2),1:SpaceSize(3));
idxX2 = kron(idxX(:),ones(6,1));
idxY2 = kron(idxY(:),ones(6,1));
idxZ2 = kron(idxZ(:),ones(6,1));

% Convert to Indices of Neighbors
idxX2 = idxX2 + kron(ones(numel(idxX),1),[ 1 -1  0  0  0  0 ]');
idxY2 = idxY2 + kron(ones(numel(idxY),1),[ 0  0  1 -1  0  0 ]');
idxZ2 = idxZ2 + kron(ones(numel(idxZ),1),[ 0  0  0  0  1 -1 ]');

% Trim Those that Fall Outside the Image Volume
Q = find( ( idxX2<1 ) | ( idxX2>SpaceSize(1) ) | ( idxY2<1 ) | ( idxY2>SpaceSize(2) ) | ( idxZ2<1 ) | ( idxZ2>SpaceSize(3) ) );       
idxX2(Q)  = []; idxY2(Q)  = []; idxZ2(Q)  = [];

% Column and Row Indexes Into Sparse matrix
colIdx    = sub2ind(SpaceSize,idxX2,idxY2,idxZ2);

rowIdx = 1:prod(SpaceSize);
rowIdx = kron(rowIdx(:),ones(6,1));
rowIdx(Q) = [];

% Trim To Include Only those points we are solving for
%   This might make the previous step superfluous.
Q = find(~ismember(colIdx,SolutionPoints)|~ismember(rowIdx,SolutionPoints));
colIdx(Q) = [];
rowIdx(Q) = [];

% Build the Sparse Matrix and Cut it Down to Size
% Again - this step might be the only necessary one, as these are currently
% just the off-diagonal components.
A1 = sparse(rowIdx,colIdx,(1/6)*ones(size(colIdx)),prod(SpaceSize),prod(SpaceSize));
A1 = A1(SolutionPoints,SolutionPoints);       

scale = sparse(1:size(A1,1),1:size(A1,1),1./(A1*ones(size(A1,2),1)));

A0 = 0.5*(speye(size(A1,2))+scale)*A1;

A = kron(A0,speye(3));

B = (6/dist^2)*(A-(1.1)*speye(size(A,1)));

%keyboard;

disp(['Solving for ' num2str(length(SolutionPoints)) ' out of a possible ' num2str(prod(SpaceSize)) ' nodes']);

disp('Computing Variances');
numNodes = length(SolutionPoints); %prod(SpaceSize);
Omega = zeros(numNodes,1);
for (idxOmega = 1:numNodes)
  idxStart = (idxOmega-1)*3 +1;
  Omega(idxOmega) = sqrt(LeadField(:,idxStart)'*LeadField(:,idxStart) + ...
                          LeadField(:,idxStart+1)'*LeadField(:,idxStart+1) + ...
                          LeadField(:,idxStart+2)'*LeadField(:,idxStart+2) );                          
end;


Omega = sparse(1:length(Omega),1:length(Omega),Omega);
Omega = kron(Omega,eye(3));

ONE = ones(size(LeadField,1),1);
H = eye(size(LeadField,1)) - ONE*ONE'/(ONE'*ONE);

disp('Building Image Covariance Matrix');
SigmaX = Omega*B'*B*Omega;

disp('Noise Covariance is the Identity');
SigmaN = eye(length(Data)); %-(1/(length(Data))*ones(length(Data)));

disp('Obtaining Solutions Using getSol_StatisticalMinNorm');
Solution = getSol_StatisticalMinNorm(LeadField,Data,zeros(size(LeadField,2),1),SigmaX,SigmaN,lambdas);

% keyboard;
% if nargout==2
%   for i = 1:length(Solution)
%   varargout{1}.residual(i) = norm(LeadField*Solution{i}(:)-Data(:))^2;
%   varargout{2}.xnorm(i) = Solution{i}(:)'*(SigmaX\Solution{i}(:));
%   end;
% end;

return;
