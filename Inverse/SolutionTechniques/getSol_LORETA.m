function [SolutionOut BOut] = getSol_LORETA(LeadField,Data,reconOptions)

mydisp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%');
mydisp('Obtaining a solution using LORETA');

lambdas = reconOptions.lambdas;

SpaceSize      = LeadField.currSolutionSpace.sizes;
SolutionPoints = LeadField.currSolutionSpace.Voxels;
dist           = LeadField.currSolutionSpace.voxSize;

if ~exist('lambdas'), lambdas = logspace(-4,4,100); end;

% not the right thing to do, but for now, since the voxels are close to
% cubic.
dist = mean(dist);

% Build Laplacian Matrix:
useOldWeighting = true;
B = build_HelmholtzMatrix(LeadField.currSolutionSpace,0,reconOptions.isVectorSolution,useOldWeighting);

mydisp(['Solving for ' num2str(length(SolutionPoints)) ' out of a possible ' num2str(prod(SpaceSize)) ' nodes']);

% Get Weighting Matrix
wantOmegaInverse = false;
Omega = LeadField.getWeightingMatrix(wantOmegaInverse);% WeightLeadField(LeadField,reconOptions.isVectorSolution,wantOmegaInverse);
  
% Centering matrix... not really used any more.
ONE = ones(size(LeadField,1),1);
H = eye(size(LeadField,1)) - ONE*ONE'/(ONE'*ONE);

% Weight the Regularizer if we want.
try
  if reconOptions.weightRegularizer
    mydisp('Using Weighted Regularizer');
    SigmaX = Omega*B'*B*Omega;
    BOut = B*Omega;
  else
    % SigmaX = Omega*Omega;
    mydisp('Using Unweighted Regularizer');
    SigmaX = B'*B;
    BOut = B;
  end;
catch
  keyboard
end;

% Add a tiny bit of the identity matrix for numerical stability
SigmaX = SigmaX + speye(size(SigmaX,1))*1e-6*min(abs(sum(SigmaX,1)));

map = LeadField.currSolutionSpace.matGridToSolSpace(:,LeadField.currSolutionSpace.Voxels);

if reconOptions.isVectorSolution
  map = kron(map,eye(3));
end;

SigmaX = map*SigmaX*map';
BOut = B*map';

mydisp('Noise Covariance is the Identity');
SigmaN = eye(size(Data,1));

mydisp('Obtaining Solutions Using getSol_StatisticalMinNorm');
Solution = getSol_StatisticalMinNorm(LeadField,Data,[],SigmaX,SigmaN,lambdas);

%Compute LCurve Values
for i = 1:length(Solution)
  [L Lt] = get_LCurves(Solution{i},Data,LeadField,BOut);
  
  LCurve(i,1,:) = L;
  LCurve_t(i,1,:,:) = Lt;
end;


% Build the Output Structure
SolutionOut.solutions = Solution';
SolutionOut.LCurve = LCurve;
SolutionOut.LCurve_t = LCurve_t;
SolutionOut.options = reconOptions;
SolutionOut.Regularizer = BOut;
SolutionOut.trueErrors = [];


return;


%% Old non-function based approach
% mydisp('Building Laplacian Matrix');
% mydisp(['Solution Space is of Size: ' num2str(SpaceSize(1)) ' x ' num2str(SpaceSize(2)) ' x ' num2str(SpaceSize(3))]);
% 
% % Get Indices to All Voxels
% [idxX idxY idxZ] = ndgrid(1:SpaceSize(1),1:SpaceSize(2),1:SpaceSize(3));
% idxX2 = kron(idxX(:),ones(6,1));
% idxY2 = kron(idxY(:),ones(6,1));
% idxZ2 = kron(idxZ(:),ones(6,1));
% 
% % Convert to Indices of Neighbors
% idxX2 = idxX2 + kron(ones(numel(idxX),1),[ 1 -1  0  0  0  0 ]');
% idxY2 = idxY2 + kron(ones(numel(idxY),1),[ 0  0  1 -1  0  0 ]');
% idxZ2 = idxZ2 + kron(ones(numel(idxZ),1),[ 0  0  0  0  1 -1 ]');
% 
% % Trim Those that Fall Outside the Image Volume
% Q = find( ( idxX2<1 ) | ( idxX2>SpaceSize(1) ) | ( idxY2<1 ) | ( idxY2>SpaceSize(2) ) | ( idxZ2<1 ) | ( idxZ2>SpaceSize(3) ) );       
% idxX2(Q)  = []; idxY2(Q)  = []; idxZ2(Q)  = [];
% 
% % Column and Row Indexes Into Sparse matrix
% colIdx    = sub2ind(SpaceSize,idxX2,idxY2,idxZ2);
% 
% rowIdx = 1:prod(SpaceSize);
% rowIdx = kron(rowIdx(:),ones(6,1));
% rowIdx(Q) = [];
% 
% % Trim To Include Only those points we are solving for
% %   This might make the previous step superfluous.
% Q = find(~ismember(colIdx,SolutionPoints)|~ismember(rowIdx,SolutionPoints));
% colIdx(Q) = [];
% rowIdx(Q) = [];
% 
% % Build the Sparse Matrix and Cut it Down to Size
% % Again - this step might be the only necessary one, as these are currently
% % just the off-diagonal components.
% A1 = sparse(rowIdx,colIdx,(1/6)*ones(size(colIdx)),prod(SpaceSize),prod(SpaceSize));
% A1 = A1(SolutionPoints,SolutionPoints);       
% 
% scale = sparse(1:size(A1,1),1:size(A1,1),1./(A1*ones(size(A1,2),1)));
% 
% A0 = 0.5*(speye(size(A1,2))+scale)*A1;
% 
% A = kron(A0,speye(3));
% 
% B = (6/dist^2)*(A-speye(size(A,1)));



% mydisp('Computing Variances');
% numNodes = length(SolutionPoints); %prod(SpaceSize);
% Omega = zeros(numNodes,1);
% try
% for (idxOmega = 1:numNodes)
%   idxStart = (idxOmega-1)*3 +1;
%   Omega(idxOmega) = sqrt(LeadField(:,idxStart)'*LeadField(:,idxStart) + ...
%                           LeadField(:,idxStart+1)'*LeadField(:,idxStart+1) + ...
%                           LeadField(:,idxStart+2)'*LeadField(:,idxStart+2) );                          
% end;
% catch
%   keyboard
% end;
% 
% Omega = sparse(1:length(Omega),1:length(Omega),Omega);
% Omega = kron(Omega,eye(3));
