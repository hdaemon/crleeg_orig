function [Solution] = getSol_MinimumCurrent(LeadField,Data,lambdas)

n = size(LeadField,2);

%for i = 1:length(lambdas);
cvx_begin
  variable x(n)
  minimize norm(x,1)
  subject to
     LeadField*x == Data;
%     norm(LeadField*x-Data,2)<=lambdas(i)
cvx_end
Solution = x;

%end;

return;