function [Solutions] = getSol_MinimumNorm(LeadField,Data,lambdas,flagCentering)
%
%  function [Solutions] = getSol_MinimumNorm(LeadField,Data,lambdas)
%
%  Obtains a solution to the underdetermined EEG problem:
%
%        Wx = b
%
%  Using the Moore-Penrose matrix pseudoinverse:
%
%       \hat{x} = W'*(W*W'+lambda^2*I)^{-1}b
%
%  for a range of regularization parameters as defined in lambdas.  Default
%  range is lospace(-3,3,100);
%
% Written By: Damon Hyde
% Created:     Sept 19, 2008
% Last Edited: Sept 19, 2008


MtM = LeadField*LeadField';

if ~exist('lambdas')||isempty(lambdas)
 lambdas = logspace(-3,3,100);
end;


nElectrodes = size(LeadField,1);
if exist('flagCentering')&&(flagCentering==1)
  H = eye(nElectrodes) - (ones(nElectrodes,1)*ones(1,nElectrodes))/nElectrodes;
else
  H = eye(nElectrodes);
end;

for i = 1:length(lambdas)
  
  [U S V] = svd(H*MtM*H + lambdas(i)^2*H);
  S = diag(1./diag(S));
  S(120:end,120:end) = 0;
  Solutions{i} = LeadField'*H*( V*S*U'*Data(:) );
  try
  disp(['getSol_MinimumNorm:: Completed Lambda ' num2str(i) ' with residual norm ' num2str(norm(Data(:)-LeadField*Solutions{i}))]);
  catch
  keyboard
  end
  
end;


return