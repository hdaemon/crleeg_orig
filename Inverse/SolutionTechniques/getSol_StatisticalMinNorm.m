function [Solution] = getSol_StatisticalMinNorm(LeadField,Data, muX, SigmaX,SigmaN,lambdas)
%
%  function [Solutions] = getSol_StatisticalMinNorm(LeadField,Data,lambdas)
%
%  Obtains a solution to the underdetermined EEG problem:
%
%        Wx = b + n
%
%  Given prior models for the image and noise covariance as:
%
%    x ~ N(muX,inv(SigmaX))   n ~ N(0,SigmaN)
%
%  Using the Moore-Penrose matrix pseudoinverse:
%
%       \hat{x} = W'*(W*W'+lambda^2*I)^{-1}b
%
%  for a range of regularization parameters as defined in lambdas.  Default
%  range is lospace(-3,3,100);  
%
%  IMPORTANT!!! Note that the input SigmaN is the covariance matrix for the
%  measurement noise, while the input SigmaX is actually the _INVERSE_ of
%  the covariance matrix for the image.
%
% Written By: Damon Hyde
% Created:     Sept 19, 2008
% Last Edited: Aug 5, 2015
% Part of the cnlEEG Project
%
debug = 1;

mydisp('  Using statistical underdetermined least squares approach with direct inversion');

mydisp(['  Leadfield is of size: ' num2str(size(LeadField,1)) ' X ' num2str(size(LeadField,2))]);

if ~exist('lambdas')
  mydisp(['  Using Default Regularization Parameters']);
 lambdas = logspace(-3,3,100);
end;

if size(Data,1)~=size(LeadField,1)
  mydisp(['  Transposing Data']);
  Data = Data';
end;

if isempty(muX)
  mydisp(['  Defaulting to Zero Mean']);
  muX = zeros(size(LeadField,2),size(Data,2));
end;


DataMinusMean = Data - LeadField*muX;

if isa(LeadField,'cnlLeadField');
  mat = LeadField.currMatrix;
else
  mat = LeadField;
end;

Q = find(sum(mat,1));

tmp = SigmaX(Q,Q);
if ~any(any(tmp)), keyboard; end;

SigmaX = SigmaX(Q,Q);
mat = mat(:,Q);

% Computation of SigmaXL can be numerically unstable if SigmaX is poorly
% conditioned.  The best thing to do is to provide a SigmaX which is well
% conditioned, however, if SigmaX is dimensionally small enough, this will
% try to use the pseudoinverse in place of \ when SigmaX is poorly
% conditioned.
%
tmp = condest(SigmaX);
if tmp<1e8
   SigmaXL = (SigmaX)\(mat');
else
  if max(size(SigmaX))<5000
    mydisp('Using pinv() to compute SigmaXL');
    SigmaXL = pinv(full(SigmaX))*mat';
  else
%     done = false;
%     s = logspace(-8,0,20);
%     i = 1;
%     while ~done
%       tmp = condest(SigmaX + s(i)*speye(size(SigmaX)));
%       mydisp(['Trying to condition SigmaX: ' num2str(tmp)]);
%       if tmp<1e8
%         done = true;
%         SigmaX = SigmaX + s(i)*speye(size(SigmaX));
%       end;
%       i = i+1;
%     end
%     SigmaXL = (SigmaX)\(mat');          
    error(['Covariance is poorly conditioned (' num2str(tmp) '), and too large to use the pseudoinverse']);
  end
end

LtL = mat*SigmaXL;

muX = muX(Q,:);

mydisp('Obtaining a Solution for each regularization level');
for i = 1:length(lambdas)  
  Solution{i} = zeros(size(LeadField,2),size(Data,2));
  Solution{i}(Q,:) = muX + (SigmaXL*( (LtL + lambdas(i)^2*inv(SigmaN)) \ DataMinusMean ));
  if debug
    mydisp(['Completed Lambda #' num2str(i) ' with Lambda = ' num2str(lambdas(i))]);
    mydisp(['Error Norm is :' num2str(norm(LeadField*Solution{i}-Data,'fro')/norm(Data,'fro'))]);
  end;
end;

% Check for NaN, INF, and imaginary solutions
if any(any(isnan(Solution{i}(Q,:))|isinf(Solution{i}(Q,:))|imag(Solution{i}(Q,:))))
  keyboard;
end;

%keyboard;
return
