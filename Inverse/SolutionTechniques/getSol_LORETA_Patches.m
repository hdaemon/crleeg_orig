function [Solution] = getSol_LORETA_Patches(LeadField,Data,PatchInfo,lambdas)

disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%');
disp('Obtaining a solution using LORETA');

if ~exist('lambdas'), lambdas = logspace(-6,1,100); end;


Faces   = PatchInfo.Faces;
Nearest = PatchInfo.Nearest;

%% Build Laplacian Matrix:
disp('Building Laplacian Matrix');
%disp(['Solution Space is of Size: ' num2str(SpaceSize(1)) ' x ' num2str(SpaceSize(2)) ' x ' num2str(SpaceSize(3))]);

connections = [Faces(:,1) Faces(:,2) ; ...
               Faces(:,2) Faces(:,1) ; ...
               Faces(:,1) Faces(:,3) ; ...
               Faces(:,3) Faces(:,1) ; ...
               Faces(:,2) Faces(:,3) ; ...
               Faces(:,3) Faces(:,2) ];
connections = double(Nearest(connections));
connections = unique(connections,'rows');
             
connectivity = sparse(connections(:,1),connections(:,2),ones(size(connections(:,1))));

B = connectivity - speye(size(connectivity));

numNbrs = sum(B,2) + 0.01;

B = -B + sparse(1:length(numNbrs),1:length(numNbrs),numNbrs);

%disp(['Solving for ' num2str(length(SolutionPoints)) ' out of a possible ' num2str(prod(SpaceSize)) ' nodes']);

disp('Computing Variances');
numNodes = size(LeadField,2); %prod(SpaceSize);
Omega = zeros(numNodes,1);
for (idxOmega = 1:numNodes)
  Omega(idxOmega) = sqrt(LeadField(:,idxOmega)'*LeadField(:,idxOmega) );                          
end;


Omega = sparse(1:length(Omega),1:length(Omega),Omega);
%Omega = kron(Omega,eye(3));

ONE = ones(size(LeadField,1),1);
H = eye(size(LeadField,1)) - ONE*ONE'/(ONE'*ONE);

disp('Building Image Covariance Matrix');

keyboard;

SigmaX = Omega*B'*B*Omega;

disp('Noise Covariance is the Identity');
SigmaN = eye(length(Data));

Centering = eye(length(Data)) - (1/length(Data))*ones(length(Data));

%Data = Data - mean(Data);

disp('Obtaining Solutions Using getSol_StatisticalMinNorm');
Solution = getSol_StatisticalMinNorm(LeadField,Data(:),zeros(size(LeadField,2),1),SigmaX,SigmaN,lambdas);

% for i = 1:length(Solution)
%   Solution{i} = connectivity*Solution{i};
% end;

return;
