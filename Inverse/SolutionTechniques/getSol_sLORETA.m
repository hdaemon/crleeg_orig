function [Solution] = getSol_sLORETA(LeadField,Data,lambdas)

disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%');
disp('Obtaining a solution using sLORETA');

if ~exist('lambdas'), lambdas = logspace(-6,6,100); end;

disp('Obtaining Solutions Using getSol_MinimumNorm');
SolutionMinNorm = getSol_MinimumNorm(LeadField,Data,lambdas,true);

disp('For each Solution, computing the Standardized Estimate');
nElectrodes = size(LeadField,1);
H = eye(nElectrodes) - (ones(nElectrodes,1)*ones(1,nElectrodes))/nElectrodes;

LeadField = H*LeadField;

for idxSol = 1:length(SolutionMinNorm)
  currSol = SolutionMinNorm{idxSol};
  currLambda = lambdas(idxSol);
  Solution{idxSol} = zeros(size(currSol));
  [U S V] = svd(LeadField*LeadField' + currLambda^2*H);
  S = diag(1./diag(S));
  S(end-10:end,end-10:end) = 0;
  CenterMat = V*S*U';
  for idxVox = 1:length(currSol)/3
    colIdx = ((idxVox-1)*3+1):((idxVox-1)*3+3);
    covarBlock = LeadField(:,colIdx)'*CenterMat*LeadField(:,colIdx);
    foo = inv(sqrtm(covarBlock));
    if any(imag(foo(:))), keyboard; end;
    Solution{idxSol}(colIdx) = currSol(colIdx)'*foo*currSol(colIdx);    
    if any(isnan(Solution{idxSol}))
      keyboard;
    end;
  end;
end;

return;
