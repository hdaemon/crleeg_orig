function solutionOut = getSol_ParcelBeamFormer(LeadField,Data,reconOptions)
% Parcellation Based Beamformer Inverse Solver
%
% function solOut = GETSOL_PARCELBEAMFORMER(LeadField,Data,reconOptions)
%
% Inputs: 
%   LeadField    : cnlLeadField object with obj.currSolutionSpace set to 
%                   be a cnlParcelSolutionSpace object
%   Data         : Input EEG data of size (nElectrode by nTime)
%   reconOptions : Reconstruction options structure
%
% Outputs:
%   solOut : Solution, of size (nParcel X nTime)
%
% Written By: Damon Hyde
% Last Edited: Aug 17, 2015
% Part of the cnlEEG Project
%


assert(isa(LeadField.currSolutionSpace,'cnlParcelSolutionSpace'), ...
  ['Leadfield.currSolutionSpace must be a cnlParcelSolutionSpace ' ...
  'to use this solver']);

nParcels = LeadField.currSolutionSpace.nParcel;
nVec     = LeadField.currSolutionSpace.nVecs;

% Get Data Covariance
dataCov    = cov(Data');
dataCovInv = inv(dataCov);


% Initialize Solution
solOut = zeros(nParcels*nVec,size(Data,2));

%
% dNrm = sqrt(sum(Data.^2,1));
% dNrm = repmat(dNrm,size(Data,1),1);
% Data = Data./dNrm;

% Loop Across Parcels
tic
for idxP = 1:nParcels
  %disp(['Doing parcel ' num2str(idxP)]);
  iVLo = 1 + (idxP-1)*nVec;
  iVHi = idxP*nVec;
  
  currMat = LeadField.currMatrix(:,iVLo:iVHi);
%   currMat = currMat./norm(currMat);
  
  F = (currMat'*(dataCov\currMat))\(currMat'/dataCov);  
  %F = (currMat'*dataCovInv*currMat)\(currMat'*dataCovInv);
  %F = (currMat'*currMat)\currMat';    
  
  solOut(iVLo:iVHi,:) = F'*Data;        
  
end
toc

solutionOut.solutions{1} = solOut;
solutionOut.options = reconOptions;

end