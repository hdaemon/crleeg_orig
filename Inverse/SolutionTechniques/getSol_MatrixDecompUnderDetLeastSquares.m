function SolutionOut = getSol_MatrixDecompUnderDetLeastSquares(LeadField,leftMatrix,rightMatrix,reconOptions)
% A modification of Damon Hyde's UnderDetLeastSquares function for
% regularized least squares solutions on matrix approximations by
% decomposition
% Author: Burak Erem

Data=leftMatrix*rightMatrix;

SpaceReg = get_SpatialRegularizer(LeadField,Data,reconOptions);
SpaceReg.currSolutionSpace = reconOptions.solutionSpace;
SpaceReg.rebuildCurrMatrix;

LtL = SpaceReg.currMatrix'*SpaceReg.currMatrix;

SigmaN = eye(size(Data,1));

lambdas = reconOptions.lambdas;
leftSolution = getSol_StatisticalMinNorm(LeadField,leftMatrix,[],LtL,SigmaN,lambdas);

%Compute Residual Values
RRt=rightMatrix*rightMatrix.';
for i = 1:numel(leftSolution)
  leftResidMatrix = LeadField*leftSolution{i} - leftMatrix;
  
  relativeError(i) = trace(leftResidMatrix*RRt*leftResidMatrix.')/trace(leftMatrix*RRt*leftMatrix.');
end

LCurve = zeros(numel(lambdas),1,2);
LCurve(:,1,1) = relativeError;

% Build the Output Structure
SolutionOut.solutions = leftSolution';
SolutionOut.rightmatrix = rightMatrix;
SolutionOut.relativeError = relativeError;
SolutionOut.LCurve = LCurve;
SolutionOut.options = reconOptions;
SolutionOut.Regularizer = SpaceReg;
SolutionOut.trueErrors = [];

end