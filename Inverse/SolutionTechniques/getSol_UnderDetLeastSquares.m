function SolutionOut = getSol_UnderDetLeastSquares(LeadField,Data,reconOptions)
% cnlEEG wrapper for getSol_StatisticalMinNorm
%
% function SolutionOut = getSol_UnderDetLeastSquares(LeadField,Data,reconOptions)
%
% Written By: Damon Hyde
% Last Edited: Aug 5, 2015
% Part of the cnlEEG Project
%

% Get the spatial regularizer
SpaceReg = get_SpatialRegularizer(LeadField,Data,reconOptions);
SpaceReg.currSolutionSpace = reconOptions.solutionSpace;
SpaceReg.rebuildCurrMatrix;

% Compute L'*L
LtL = SpaceReg.currMatrix'*SpaceReg.currMatrix;

% Assume diagonal noise prior
SigmaN = eye(size(Data,1));

% Get regularization values
lambdas = reconOptions.lambdas;

% Compute solutions
Solution = getSol_StatisticalMinNorm(LeadField,Data,[],LtL,SigmaN,lambdas);

% Compute LCurve Values
for i = 1:length(Solution)
  [L Lt] = get_LCurves(Solution{i},Data,LeadField,SpaceReg.currMatrix);
  
  LCurve(i,1,:) = L;
  LCurve_t(i,1,:,:) = Lt;
end;

% Build the Output Structure
SolutionOut.solutions = Solution';
SolutionOut.LCurve = LCurve;
SolutionOut.LCurve_t = LCurve_t;
SolutionOut.options = reconOptions;
SolutionOut.Regularizer = SpaceReg;
SolutionOut.trueErrors = [];

end