function [Solution Solution2 Omega] = getSol_WMinNorm(LeadField,Data,reconOptions)

disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%');
disp('Obtaining a solution using Weighted Minimum Norm');

lambdas = reconOptions.lambdas;
%SolutionPoints = reconOptions.solutionSpace.Voxels;

if ~exist('lambdas'), lambdas = logspace(-6,1,100); end;

%disp(['Solving for ' num2str(length(SolutionPoints)) ' out of a possible ' num2str(prod(SpaceSize)) ' nodes']);

disp('Computing Variances');
% numNodes = length(SolutionPoints); %prod(SpaceSize);
% Omega = zeros(numNodes,1);
% for (idxOmega = 1:numNodes)
%   idxStart = (idxOmega-1)*3 +1;
%   Omega(idxOmega) = sqrt(LeadField(:,idxStart)'*LeadField(:,idxStart) + ...
%                           LeadField(:,idxStart+1)'*LeadField(:,idxStart+1) + ...
%                           LeadField(:,idxStart+2)'*LeadField(:,idxStart+2) );                          
% end;
% 
% 
% Omega = sparse(1:length(Omega),1:length(Omega),Omega);
% Omega = kron(Omega,eye(3));
wantOmegaInverse = false;
Omega = LeadField.getWeightingMatrix(wantOmegaInverse);% WeightLeadField(LeadField,reconOptions.isVectorSolution,wantOmegaInverse);

disp('Building Image Covariance Matrix');
SigmaX = Omega; %Omega*Omega;
%SigmaX = B'*B;

% map = LeadField.currSolutionSpace.matGridToSolSpace(:,LeadField.currSolutionSpace.Voxels);
% 
% Omega = diag(sqrt(diag(Omega)))*map';
% SigmaX = map*SigmaX*map';

disp('Noise Covariance is the Identity');
SigmaN = eye(length(Data)); %-(1/(length(Data))*ones(length(Data)));

disp('Obtaining Solutions Using getSol_StatisticalMinNorm');
tmpSol = getSol_StatisticalMinNorm(LeadField,Data,zeros(size(LeadField,2),size(Data,2)),SigmaX,SigmaN,lambdas);
 
%Compute LCurve Values
for i = 1:length(tmpSol)
  [L Lt] = get_LCurves(tmpSol{i},Data,LeadField,Omega);
  
  LCurve(i,1,:) = L;
  LCurve_t(i,1,:,:) = Lt;
end;


% Build the Output Structure
Solution.solutions = tmpSol';
Solution.LCurve = LCurve;
Solution.LCurve_t = LCurve_t;
Solution.options = reconOptions;
Solution.Regularizer = Omega;
Solution.trueErrors = [];
return;
