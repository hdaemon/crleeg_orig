function [SolutionOut multiLevelSols] = getSol_MultiResolution(LeadField,Data,NRRDS,reconOptions)
%
% function [SolutionOut] = getSol_MultiResolution(LeadField,Data,NRRDS,options)
%
% Obtain a solution using our SpatioTemporal approach solved with conjugate
% gradient on the normal equations.
%

nElectrodes = size(Data,1);
nTimePoints = size(Data,2);

%% Get Parcellation Solution Spaces
NMM = ParcellationMapping('nmm');
IBSR = ParcellationMapping('ibsr');

useValsNMM = NMM.cortexLabels;
useValsIBSR = IBSR.cortexLabels;

nmmSolutionSpace = SolutionSpace(NRRDS.NMM,useValsNMM,'parcel','NMM Parcellation');
ibsrSolutionSpace = SolutionSpace(NRRDS.IBSR,useValsIBSR,'parcel','IBSR Parcellation');

currSolutionSpace = forceCompatibility(nmmSolutionSpace,LeadField.currSolutionSpace);
currSolutionSpace = forceCompatibility(ibsrSolutionSpace,currSolutionSpace);
nmmSolutionSpace = forceCompatibility(currSolutionSpace,nmmSolutionSpace);
ibsrSolutionSpace = forceCompatibility(currSolutionSpace,ibsrSolutionSpace);

LeadField.currSolutionSpace = currSolutionSpace;

reconOptions.solutionSpace = currSolutionSpace;

%% Get Spatial Regularizer at Highest Resolution
mydisp(['Computing Spatial Regularizer']);
switch lower(reconOptions.SpaceRegType)
  case 'loreta'
    SpaceReg = LORETAReg(reconOptions, LeadField);
  case 'helmholtz'
    SpaceReg = HelmholtzReg(reconOptions, LeadField); 
  case 'minnorm'
    SpaceReg = MinNormReg(reconOptions,LeadField);
  case 'weighted_minnorm';
    SpaceReg = Weighted_MinNorm(LeadField);
  case 'armodel'
    SpaceReg = ARModelReg(reconOptions,LeadField,Data);
  otherwise
    SpaceReg = [];
end;

%% Get Temporal Regularizer
mydisp(['Computing Temporal Regularizer']);
switch lower(reconOptions.TempRegType)
  case 'temporalhh'
    TempReg = TemporalHHReg(reconOptions.TempHHFact, Data);
  case 'datacorr'
    TempReg = reg_DataCorr(Data);
  otherwise
    TempReg = [];
end

%% Get Map from currSolutionSpace to Parcellations
if reconOptions.isVectorSolution
  nmmMap  = getVectorMapping(nmmSolutionSpace,currSolutionSpace);
  nmmMapBack = getVectorMapping(currSolutionSpace,nmmSolutionSpace);
  ibsrMap = getVectorMapping(ibsrSolutionSpace,currSolutionSpace);
  ibsrMapBack = getVectorMapping(currSolutionSpace,ibsrSolutionSpace);
else
  nmmMap     = getMapping(nmmSolutionSpace,currSolutionSpace);
  nmmMapBack = getMapping(currSolutionSpace,nmmSolutionSpace);
  ibsrMap     = getMapping(ibsrSolutionSpace,currSolutionSpace);
  ibsrMapBack = getMapping(currSolutionSpace,ibsrSolutionSpace);
end;

startingSolSpace = LeadField.currSolutionSpace;

nmmSpaceReg = SpaceReg;
nmmSpaceReg.mat = nmmMap*nmmSpaceReg.mat*nmmMap';
nmmSpaceReg.solSpace = nmmSolutionSpace;

ibsrSpaceReg = SpaceReg;
ibsrSpaceReg.mat = ibsrMap*ibsrSpaceReg.mat*ibsrMap';
ibsrSpaceReg.solSpace = ibsrSolutionSpace;

%% Build Full Regularizer for each Resolution
FullReg_Full = SpatioTemporalRegularizer(SpaceReg,TempReg);
FullReg_nmm  = SpatioTemporalRegularizer(nmmSpaceReg,TempReg);
FullReg_ibsr = SpatioTemporalRegularizer(ibsrSpaceReg,TempReg);

nV = size(Data,1); if reconOptions.isVectorSolution, nV = 3*nV; end;
nT = size(Data,2);

%% Build Temporal Smoothing Matrix
vecSmooth = [0.006 0.061 0.242 0.383 0.242 0.061 0.006];
vecSmooth = repmat(vecSmooth,nT,1);
matTSmooth = spdiags(vecSmooth,[-3 -2 -1 0 1 2 3],nT,nT);
tmp = sum(matTSmooth,2);
matTSmooth = matTSmooth*diag(1./tmp);


%%  Get Solution at NMM resolution, map to IBSR resolution
try
  LeadField.currSolutionSpace = nmmSolutionSpace;
  fullNMMSpaceReg = kron(SpaceReg.mat*nmmMap',speye(nT));
  
  fullNMMTempReg  = kron(nmmMap',FullReg_nmm.Temp.mat);
  fullNMMTempReg  = 1e3*fullNMMTempReg;
  fullReg = fullNMMSpaceReg + fullNMMTempReg; fullReg = fullReg'*fullReg;
  clear fullNMMSpaceReg fullNMMTempReg
  leadFieldMatrix = kron(speye(nT),LeadField.currMatrix);
  
  lambdas = [1e-10 1e-9 1e-8 1e-7 1e-6 1e-5 1e-4 1e-3 1e-2 1e-1 ];
  solutionNMM = getSol_StatisticalMinNorm(leadFieldMatrix,Data(:),[],fullReg,speye(numel(Data)),lambdas);
  
  for i = 1:length(lambdas)
    err(i) = norm(Data - LeadField*reshape(solutionNMM{i},[length(solutionNMM{i})/nT nT]),'fro')/norm(Data(:));
    regnorm(i) = norm(fullReg*solutionNMM{i},'fro');
  end;
  
  solutionNMMout = solutionNMM{find(err==min(err))};
  solutionNMMout = reshape(solutionNMMout,[length(solutionNMMout)/nT nT]);
  
  solutionNMMoutSmooth = solutionNMMout*matTSmooth;
  
 Q = find(err==min(err));
 LCurve(:,1) = err(Q);
 LCurve(:,2) = regnorm(Q);
  
 Solution{1} = nmmMapBack*solutionNMMout;
 Solution{2} = nmmMapBack*solutionNMMoutSmooth;
  
  muXibsr = ibsrMapBack'*(nmmMapBack*solutionNMMoutSmooth); muXibsr = zeros(size(muXibsr));
  sigmaXibsr = 1./abs(muXibsr); sigmaXibsr = ones(size(sigmaXibsr));
  
  multiLevelSols{1}.solution = solutionNMM;
  multiLevelSols{1}.LCurve   = LCurve;
  multiLevelSols{1}.Regularizer = fullReg;
  multiLevelSols{1}.trueErrors = [];
  multiLevelSols{1}.options = reconOptions;
  
  clear LCurve Solution
  
catch
  mydisp('Error while computing NMM Parcel SOlution');
  keyboard;
end;

%% Get Solution at IBSR Resolution, Map to Full Resolution
try
  LeadField.currSolutionSpace = ibsrSolutionSpace;
  fullIBSRSpaceReg = kron(SpaceReg.mat*ibsrMap',speye(nT));
  fullIBSRSpaceReg = fullIBSRSpaceReg*spdiags(sigmaXibsr(:),0,numel(sigmaXibsr),numel(sigmaXibsr));
  
  fullIBSRTempReg  = kron(ibsrMap',FullReg_ibsr.Temp.mat);
  fullIBSRTempReg  = 1e5*fullIBSRTempReg;
  
  fullReg = fullIBSRSpaceReg + fullIBSRTempReg; fullReg = fullReg'*fullReg;
  clear fullIBSRSpaceReg fullIBSRTempReg
  leadFieldMatrix = kron(speye(nT),LeadField.currMatrix);
  lambdas =[1e-10 1e-9 1e-8 1e-7 1e-6 1e-5 1e-4 1e-3 1e-2 1e-1 ];
  
  muX = zeros(size(muXibsr));
  solutionIBSR = getSol_StatisticalMinNorm(leadFieldMatrix,Data(:),muX(:),fullReg,speye(numel(Data)),lambdas);
  
  for i = 1:length(lambdas)
    err(i) = norm(Data - LeadField*reshape(solutionIBSR{i},[length(solutionIBSR{i})/nT nT])*matTSmooth*matTSmooth,'fro')/norm(Data(:));
    regnorm(i) = norm(fullReg*solutionIBSR{i},'fro');
  end;
  
  solutionIBSRout = solutionIBSR{find(err==min(err))};
  solutionIBSRout = reshape(solutionIBSRout,[length(solutionIBSRout)/nT nT]);
  
  solutionIBSRoutSmooth = solutionIBSRout*matTSmooth*matTSmooth;
  
  Q = find(err==min(err));
  LCurve(:,1) = err(Q);
  LCurve(:,2) = regnorm(Q);
  
  Solution{1} = ibsrMapBack*[1:length(solutionIBSRout)]';
  Solution{2} = ibsrMapBack*solutionIBSRoutSmooth;
  
  muX = ibsrMapBack*solutionIBSRoutSmooth;
  sigmaX = 1./(mean(abs(muX),2));
  
  FullReg_Full.Space.mat = FullReg_Full.Space.mat*spdiags(sigmaX(:),0,numel(sigmaX),numel(sigmaX));
  
  multiLevelSols{2}.solution = solutionIBSR;
  multiLevelSols{2}.LCurve   = LCurve;
  multiLevelSols{2}.Regularizer = fullReg;
  multiLevelSols{2}.trueErrors = [];
  multiLevelSols{2}.options = reconOptions;
  
catch
  mydisp('Error while obtaining IBSR SOlution');
  keyboard;
end;
% %keyboard;
% 
% %% Solve Problem Using CG on the Normals for Each Lambda Value
% mydisp(['Using ' num2str(length(reconOptions.lambda_t)) ' temporal regularization values']);
% mydisp(['Using ' num2str(length(reconOptions.lambdas)) ' spatial regularization values']);
% 
% % Loop Across Temporal Lambdas (The temporal regularization ends up being
% % lambda_t(j)*lambdas(i)
% 
% LeadField.currSolutionSpace = currSolutionSpace;
% solIndex = 1;
% for j = 1:length(reconOptions.lambda_t)
%   % Loop across normal regularization parameters
%   for i = 1:length(reconOptions.lambdas)
%     % Set Regularization Parameters for this Inversion
%     FullReg_Full.lambda_t = reconOptions.allLambda_t(solIndex);
%     FullReg_Full.lambda   = reconOptions.allLambdas(solIndex);
%   
%     % Get Solution using conjugate gradient on the normal equations
%     mydisp(['Solving with lambda = ' num2str(reconOptions.allLambdas(solIndex)) ' and temporal factor: ' num2str(reconOptions.allLambda_t(solIndex))]);
%     Solution{i,j} = Solver_CG_Normals(LeadField,FullReg_Full,Data,muX,reconOptions.niters,reconOptions.tol);
%     
%     % Compute Error Norm
%     simDat1 = LeadField*Solution{i,j};
%     LCurve(i,1,j) = norm(simDat1(:)-Data(:))/norm(Data(:));
%     
%     % Compute Regularization Norm
%     FullReg_Full.lambda = 1;
%     FullReg_Full.lambda_t = 1;
%     regvec = FullReg_Full*Solution{i,j};
%     LCurve(i,2,j) = norm(regvec(:));
%            
%     mydisp(['  Residual Norm: '  num2str(LCurve(i,1,j)) '  Regularization Norm:' num2str(LCurve(i,2,j)) ]);
%     solIndex = solIndex + 1;
%   end;
% end;

% Compute true errors (if possible)
if ~isempty(reconOptions.xTrue)
  for i = 1:size(Solution,1)
    for j = 1:size(Solution,2)
      trueErrors(i,j) = norm(Solution{i,j}-reconOptions.xTrue,'fro');
    end;    
  end;
else
  trueErrors = [];
end;

% Construct output structure
SolutionOut.solutions   = Solution;
SolutionOut.multiRes    = multiLevelSols;
SolutionOut.LCurve      = LCurve;
SolutionOut.options     = reconOptions;
SolutionOut.Regularizer = FullReg_Full;
SolutionOut.trueErrors  = trueErrors;
SolutionOut = SaveSolution(SolutionOut);

return;



function solStruct = SaveSolution(solStruct)
done = false;
idx = 1;
fnamebase = get_SolutionName(solStruct.options);
while ~done
  fname = ['SpatTempSol_' fnamebase '_' num2str(idx) '.mat'];
  if ~exist(fname)
    done = true;
  else
    idx = idx+1;
  end;
end;
solStruct.filename = fname;
save(fname,'solStruct');
return;

