classdef cnlEEGData_Clone < handle
  % classdef cnlEEGData < handle
  %
  % Handle class for dealing with input EEG data.
  %
  % Inherited Properties: (From cnlMatchedData)
  %   datafile      : Object or structure with data in the .data field
  %   elecLabels    : List of labels for electrodes in model
  %
  % Inherited Dependent Properties: (From cnlMatchedData)
  %   currLabels    : List of labels associated with data_parsed
  %   data_parsed   : Input data with columns reordered to match elecLabels
  %
  % Properties:
  %   datafile      : Object or structure with data in the .data field
  %   badfile       : Object or structure with labels of bad electrodes
  %                      in the .badlabel field.
  %   elecLabels    : List of labels for electrodes in model
  %   gndElectrode  : Index of electrode to use as a reference
  %   useTimes      : List of timepoints to use
  %   nEpochs       : Number of epochs in the data
  %   epochs_start  : Start time of each epoch
  %   epochs_end    : End time of each epoch
  %
  % Dependant Properties:
  %   currLabels    : Labels associated with current data_* fields
  %   dataLabels    : Labels associated with input data file
  %   badLabels     : List of bad labels associated with bad electrodes
  %   useElec       : List of electrodes currently used for data_* fields
  %   data_parsed   : Data parsed according
  %   data_reref    :
  %   data_avgref   :
  %
  % Part of the cnlEEG project, 2014
  % Written by: Damon Hyde
  
  properties 
    inputOBJ    
    pipeline
  end
  
  properties (Dependent = true)
    datafile
    elecLabels
    currLabels

    dataLabels

    sampleRate
    badfile
    gndElectrode
    useTimes
    removeTemporalMean
    removeTemporalMeanPerEpoch
    outputType
    nEpochs
    epochs_start
    epochs_end
    badLabels
    badElec
    gndElec
    useElec
    useData
    data
    data_epoched

  end
    
  properties (Dependent=true,Hidden=true)
    idxIntoFile
    idxIntoElec
    data_parsed
    data_reref
    data_avgref
  end
  
  
  methods

    function obj = cnlEEGData_Clone(file,varargin)
     
      if ~exist('file','var'), file = [];  end;    
      if isa(file,'cnlEEGData_Clone')
        obj.inputOBJ = file.inputOBJ;
        obj.pipeline = file.pipeline;
        return;
      end;
      
      
      %% Set Data
  
      obj.datafile = file;            
      
      %% Set Default Pipeline
      % Electrode Label Matching
      obj.pipeline{1} = cnlPLMod.EEG.elec.matchToList(obj.inputOBJ);
      obj.pipeline{1}.toMatch = obj.inputOBJ.labels;
      % Bad Electrode Removal
      obj.pipeline{2} = cnlPLMod.EEG.elec.remove(obj.pipeline{1});
      % Signal Rereferencing
      obj.pipeline{3} = cnlPLMod.EEG.elec.rereference(obj.pipeline{2});
      % Temporal Mean 
      obj.pipeline{4} = cnlPLMod.EEG.signal.temporalmean(obj.pipeline{3});       
      % Temporal Mean Removal - Disabled by Default
      obj.pipeline{5} = cnlPLMod.EEG.signal.difference(obj.pipeline{3},obj.pipeline{4});      
      obj.pipeline{5}.disabled = true;
      % Lowpass Filter - Default to 80Hz
      obj.pipeline{6} = cnlPLMod.EEG.filter.lowpass(obj.pipeline{5});      
      obj.pipeline{6}.PassbandFrequency = 80;      
      obj.pipeline{6}.disabled = true;
      % Highpass Filter - Default to 0.1Hz
      obj.pipeline{7} = cnlPLMod.EEG.filter.highpass(obj.pipeline{6});
      obj.pipeline{7}.PassbandFrequency = 0.1;
      obj.pipeline{7}.disabled = true;
      % Notch Filter - Default to 60Hz w/ 2Hz Half Power Bandwidth
      obj.pipeline{8} = cnlPLMod.EEG.filter.notch(obj.pipeline{7});
      obj.pipeline{8}.HalfPowerFrequency1 = 58;
      obj.pipeline{8}.HalfPowerFrequency2 = 62;
      obj.pipeline{8}.disabled = true;
     
      % Trim epochs by interval - Disabled by Default
      obj.pipeline{9} = cnlPLMod.EEG.epochs.cropToInterval(obj.pipeline{8});
      obj.pipeline{9}.disabled = true;
            
      obj.pipeline{end+1} = cnlPLMod.EEG.signal.buffer(obj.pipeline{end});
      
      %% Input Parsing
      if nargin>0
        p = inputParser;

        addParamValue(p,'elec',[]);
        addParamValue(p,'model',[]);
        addParamValue(p,'badfile',[]);
        addParamValue(p,'eleclabels',[]);
        addParamValue(p,'outputtype','recorded');
        addParamValue(p,'usetimes',[]);
        addParamValue(p,'gndElec','Cz');

        parse(p,varargin{:});
                
        obj.badfile = p.Results.badfile;
        
        %% Get the list of electrode labels to use for the output
        if ~isempty(p.Results.model)
          if ~isempty(p.Results.eleclabels)
            error('You can set either ''model'' or ''eleclabels'', but not both');
          end

          obj.setModel(p.Results.model);                    
        else
          matchElec = p.Results.eleclabels;
          gndElec    = p.Results.gndElec;
          
          % If not list provided, output all inputs.
          if isempty(matchElec), matchElec = obj.inputOBJ.labels; end;          
          obj.elecLabels = matchElec;
          obj.gndElectrode = gndElec;                               
        end;
        
        % Set Pipeline Options                
        obj.badfile = p.Results.badfile;
        obj.outputType = p.Results.outputtype;
        
      end
    end

    function out = get.sampleRate(obj)
      out = obj.pipeline{6}.SampleRate;
    end;
    
    function set.sampleRate(obj,val)
      assert(isnumeric(val)&isscalar(val),'Sample rate must be a numeric scalar value');
      if ~isempty(obj.pipeline)
      obj.pipeline{6}.SampleRate = val;
      obj.pipeline{7}.SampleRate = val;
      obj.pipeline{8}.SampleRate = val;
      end;
    end
    
    function setModel(obj,model)
      assert(isa(model,'cnlModel'),'Input must be a cnlModel object');
      obj.elecLabels = model.Electrodes.Labels;
      obj.gndElectrode = model.gndElectrode;
    end
    
    %% Epoch information comes straight from the input object.
    function out = get.nEpochs(obj)
      out = obj.inputOBJ.nEpochs;
    end
    
    function out = get.epochs_start(obj)
      out = obj.inputOBJ.epochs_start;
    end
    
    function out = get.epochs_end(obj)
      out = obj.inputOBJ.epochs_end;
    end    
    
    %% Redirect gndElectrode to pipeline object parameters
    function set.gndElectrode(obj,val)
      if isempty(val), obj.pipeline{3}.refElec = []; return; end;
     
      % If numeric, use that index from the matching electrode list.                  
      if isnumeric(val), val = obj.pipeline{1}.toMatch{val}; end;
      
      obj.pipeline{3}.refElec = val;
      
    end
                    
    function out = get.gndElectrode(obj)
      out = obj.pipeline{3}.refElec;
    end
    
    %% Redirect currLabels
    function out = get.currLabels(obj)
      tmp = obj.pipeline{end}.output;
      out = tmp.labels;
    end;
    
    %% Redirect elecLabels
    function set.elecLabels(obj,val)
      obj.pipeline{1}.toMatch = val;
    end
    
    function out = get.elecLabels(obj)
      out = obj.pipeline{1}.toMatch;
    end    
    
    %% Redirect outputtype to the pipeline object parameters
    function set.outputType(obj,val)
      obj.pipeline{3}.refType = val;
    end
    
    function out = get.outputType(obj)
      out = obj.pipeline{3}.refType;
    end
        
    %% Redirect data_parsed to pipeline output          
    function out = get.data_parsed(obj)
      warning('obj.data_parsed is deprecated. Please fix usage');
      tmp = obj.pipeline{1}.output;
      out = tmp.data;
    end
    
    function set.data_parsed(obj,val)
      error('obj.data_parsed is deprecated. Please fix usage');
    end
    
    %% Indices 
    
    function out = get.dataLabels(obj)
      out = obj.inputOBJ.labels;
    end;
    
    function out = get.idxIntoFile(obj)
      % NOTE: this indexing is done immediately post matching, prior to any
      % bad electrode elimination or rereferencing.
      warning('obj.idxIntoFile is deprecated. Please fix usage');
      labels = obj.pipeline{1}.output.labels;
      out = cellfun(@(x) find(strcmpi(x,obj.dataLabels)), labels);
    end
    
    function out = get.idxIntoElec(obj)         
      % NOTE: this indexing is done immediately post matching, prior to any
      % bad electrode elimination or rereferencing.
      warning('obj.idxIntoElec is deprecated. Please fix usage');
      labels = obj.pipeline{1}.output.labels;
      out = cellfun(@(x) find(strcmpi(x,obj.elecLabels)),labels);
    end;
    
    %% Redirect removeTemporalMean
    function set.removeTemporalMean(obj,val)
      assert(islogical(val),'Input must be logical');
      obj.pipeline{4}.disabled = val;
      if val
      obj.pipeline{4}.byEpoch = false;
      end;
    end;
    
    function out = get.removeTemporalMean(obj)
      out = ~(obj.pipeline{4}.disabled || obj.pipeline{4}.perEpoch);
    end;
    
    %% Redirect removeTemporalMeanPerEpoch
    function set.removeTemporalMeanPerEpoch(obj,val)
      assert(islogical(val),'Input must be logical');
      obj.pipeline{4}.disabled = val;
      obj.pipeline{4}.byEpoch = true;
    end;
    
    function out = get.removeTemporalMeanPerEpoch(obj)
      out = obj.pipeline{4}.disabled&&obj.pipeline{4}.perEpoch;
    end;
    
    %% Redirect useTimes
    function set.useTimes(obj,val)
      % NOTE: This only permits cropping to continuous intervals.
      % Subsampling is thus prohibited. This will be changed in a future
      % revision.
      if ~isempty(val)
        if numel(val)==2
          obj.pipeline{9}.interval = val;
          obj.pipeline{9}.disabled = false;
        else
          obj.pipeline{9}.interval = [min(val) max(val)];
          obj.pipeline{9}.disabled = false;
        end;          
      else
        % Clear interval and disable pipeline module.
        obj.pipeline{9}.interval = [];
        obj.pipeline{9}.disabled = true;
      end;
    end
    
    function out = get.useTimes(obj)
      out = obj.pipeline{9}.interval;
      if isempty(out)
        out = 1:size(obj.pipeline{end}.output.data,2);
      end
    end
    
    %% Redirect datafile
    function set.datafile(obj,val)
      % function set.datafile(obj,val)
      %
      % Set method for cnlMatchedData.datafile.
      %

      if isa(val,'cnlLabelledData'), obj.inputOBJ = val; return; end;
      
      assert( isempty(val) || isa(val,'file_EDF') || isfield(val,'data'),...
        'Input invalid');
                  
      % Set Default Labels if they're missing.
      if ~isempty(val)
        
        if iscell(val.data)
          toEval = val.data{1};
        else
          toEval = val.data;
        end
        
        if ~isfield(val,'labels')&&~isprop(val,'labels')
          warning('Using default electrode labels!!!');
          for i = 1:size(toEval,1)
            val.labels{i} = ['E' num2str(i)];
          end
        end
        
        % Permit input data to be oriented either way.
        if size(toEval,2)==numel(val.labels)
          if iscell(val.data)
            for i = 1:numel(val.data)
              dataIn{i} = val.data{i}';
            end
          else
            dataIn = val.data';
          end;
        else
          dataIn = val.data;
        end;
        
        try
          % Try getting a samplerate from the input field.
          sRate = val.sampleRate;
        catch
          sRate = 1;
        end
        
        obj.inputOBJ = cnlLabelledData(dataIn,val.labels,'sampleRate',sRate);
      else
        obj.inputOBJ = cnlLabelledData;
      end;      
    end

    function out = get.datafile(obj)
      out.data = obj.inputOBJ.data;
      out.labels = obj.inputOBJ.labels;
    end;
    
    function reinit(obj)
      error('obj.reinit is Deprecated');      
    end

    function varargout = plot(obj)
      %uitools.cnlDataPlot(obj.data,obj.currLabels(obj.useData));
      tmp = uitools.plots.dataexplorer(obj.data,obj.currLabels(obj.useData));
      tmp.units = 'normalized';
      if nargout>0, varargout{1} = tmp; end;
    end

    %% Methods for Parsed, Rereferenced, and Average Referenced Dependant Properties

    function out = get.data(obj)
      tmp = obj.pipeline{end}.output;
      out = tmp.data';           
    end

    function out = get.data_epoched(obj)
      tmp = obj.pipeline{end}.output;
      out = tmp.epochs;
      for i = 1:numel(out)
        out{i} = out{i}';
      end;
    end;
    
    function out = get.data_reref(obj)
      error('obj.data_reref is deprecated');      
    end

    function out = get.data_avgref(obj)
      error('obj.data_avgref is deprecated');
    end

    %% Redirect badfile
    function set.badfile(obj,val)
      % function set.badfile(obj,val)
      %
      %
      if isempty(val), obj.pipeline{2}.toRemove = []; return; end;
      
      if (isfield(val,'badLabels')||isprop(val,'badLabels'))
        obj.pipeline{2}.toRemove = val.badLabels;      
      elseif iscellstr(val)
        obj.pipeline{2}.toRemove = val;
      end;
    end;

    %% GET/SET For obj.badLabels
    function set.badLabels(obj,val)
      % function set.badLabels
      %
      if iscellstr(val)
      obj.pipeline{2}.toRemove = val;
      end;
    end;
    
    function out = get.badLabels(obj)
      %  function out = get.badLabels(obj)
      %
      %  If obj.badfile is defined, get the list of bad labels from it.
      %  Otherwise return an empty set.
      out = obj.pipeline{2}.toRemove;      
    end;

    function out = get.badElec(obj)
      out = [];
      if ~isempty(obj.badLabels)
      badLabels  = obj.badLabels;
      currLabels = obj.currLabels;

      idx = 0;
      for idxElec = 1:length(badLabels)
        tmp = find(strcmpi(badLabels{idxElec},currLabels));
        if ~isempty(tmp)
          idx = idx+1;
          out(idx) = tmp;
        end
      end
      end;
    end

    function out = get.gndElec(obj)
      % function out = get.gndElec(obj)
      %
      % Find the index into obj.data_parsed associated with the current
      % ground electrode label
      out = [];
      if ~isempty(obj.gndElectrode)
        if isnumeric(obj.gndElectrode)
         out = find(strcmpi(obj.elecLabels{obj.gndElectrode},obj.currLabels));
        else
          out = find(strcmpi(obj.gndElectrode,obj.currLabels));
        end
      end
    end

    
    
    function out = get.useElec(obj)
      % function out = get.useElec(obj)
      %
      % This is a subset of idxIntoElec that corresponds to the indices
      % into the matching electrode labels of the data at the output.
      %
      labels = obj.currLabels;
      out = cellfun(@(x) find(strcmpi(x,obj.elecLabels)), labels);     
    end

    function out = get.useData(obj)
      % function out = get.useData
      % 
      % Get the appropriate indices into obj.data_parsed 
      currLabels = obj.currLabels;
      useElec = obj.useElec;
      out = find(cellfun(@(x) ismember(x,obj.elecLabels(useElec)),currLabels));     
    end

    function set.data_epoched(obj,epochCell)      
      obj.inputOBJ.data = epochCell;            
    end
    
    function out = HFO_Epochs(obj)
        epochLengths=obj.epochs_end-obj.epochs_start+1;
        
        % All frequency values are in Hz.
        Fs = obj.datafile.header.SampleRate;  % Sampling Frequency

        Fstop = 70;        % Stopband Frequency
        Fpass = 80;        % Passband Frequency
        Astop = 60;          % Stopband Attenuation (dB)
        Apass  = 1;           % Passband Ripple (dB)

        if(exist('fdesign'))
            h  = fdesign.highpass('Fst,Fp,Ast,Ap', Fstop, Fpass, Astop, Apass, Fs);
            Hd = design(h, 'equiripple');

            out=cell(obj.nEpochs,1);
            for i=1:obj.nEpochs
                if(numel(Hd.Numerator)*3 > epochLengths(i)) % if the existing filter is too long, design a new filter
                    if(floor(epochLengths(i)/3)<3) % the filter order has to be >=3, so if that's not possible, just pass the data back as-is
                        out{i}=obj.data_epoched{i};
                    else
                        htemp=fdesign.highpass('N,Fst,Fp',floor(epochLengths(i)/3),Fstop, Fpass, Fs);
                        Hdtemp=design(htemp, 'equiripple');

                        out{i}=filtfilt(Hdtemp.Numerator,1,obj.data_epoched{i});
                    end
                else
                    out{i}=filtfilt(Hd.Numerator,1,obj.data_epoched{i});
                end
            end
        else % use alternative functions for filter design and filtering
            h=firls(200,[0 Fstop Fpass Fs/2]./(Fs/2),[0 0 1 1]);

            out=cell(obj.nEpochs,1);
            for i=1:obj.nEpochs
                if(numel(h)*3 > epochLengths(i)) % if the existing filter is too long, design a new filter
                    if(floor(epochLengths(i)/3)<3) % the filter order has to be >=3, so if that's not possible, just pass the data back as-is
                        out{i}=obj.data_epoched{i};
                    else
                        htemp=firls(floor(epochLengths(i)/3),[0 Fstop Fpass Fs/2]./(Fs/2),[0 0 1 1]);

                        out{i}=filttlif(htemp,1,obj.data_epoched{i});
                    end
                else
                    out{i}=filttlif(h,1,obj.data_epoched{i});
                end
            end
        end
        
    end

  end

  methods (Access = protected)

    function parseData(obj,val)
      % function parseData(obj,val)
      %
      % parseData(obj,val) is called by cnlMatchedData.set.datafile
      % whenever the datafile in the object is changed.  This subsequently
      % sets the nEpochs, epochs_start, and epochs_end fields to the values
      % output by
      disp('Calling cnlEEGData.setdata');
      %obj.parseData@cnlMatchedData(val);
      if isnumeric(obj.datafile.data) 
        % Data is provided in the data object as a matrix.  See if there
        % are epochs defined on it
        if isfield(obj.datafile,'nEpochs')|isprop(obj.datafile,'nEpochs'),
          obj.nEpochs = obj.datafile.nEpochs; end;
        if isfield(obj.datafile,'epochs_start')|isprop(obj.datafile,'epochs_start'),
          obj.epochs_start = obj.datafile.epochs_start; end;
        if isfield(obj.datafile,'epochs_end')|isprop(obj.datafile,'epochs_end'),
          obj.epochs_end = obj.datafile.epochs_end; end;

      elseif iscell(obj.datafile.data)
        % Data is provided in the data object as a cell array.
        % Automatically extract epoch information.
        disp('Processing cell array input');
        obj.nEpochs = length(obj.datafile.data);
        epochMat = cat(1,obj.datafile.data{:});
        
        epochlengths = cellfun(@(val)(size(val,1)),obj.datafile.data);
        epochlengths = reshape(epochlengths,1,[]);
        
        obj.epochs_start = [1 cumsum(epochlengths(1:end-1))+1];
        obj.epochs_end   = cumsum(epochlengths);
        
        obj.datafile.data = epochMat;

      end
    end

  end
  
 
end
