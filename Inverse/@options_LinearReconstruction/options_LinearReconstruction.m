classdef options_LinearReconstruction
  
  % This class contains every option for every type of regularizer.  It's
  % going to be a bit of a catch all, and each regularizer object should
  % have its own method to check the options and ensure their validity.
  
  properties
    % Regularizer Oriented Properties
    solutionTechnique = 'spatiotemporal';
    TempRegType  = 'none';  % Define the type of temporal regularization
    SpaceRegType = 'none';  % Define the type of spatial  regularization    
    weightRegularizer    = true; % Flag to Include Weighting in Regularization
    oldWeighting         = false; % Flag for old/new weighting of regularizer
    HHFact = 0;             % Helmholtz Factor for Spatial Regularization
    TempHHFact = 0;         % Helmholtz Factor for Temporal Regularization
    
    % Inversion Oriented Properties
    
    solutionSpace = [];           % Space to obtain a solution on
    useElec = [];                 % List of electrodes to use
    niters = 600;                 % Number of iterations to run (if applicable)
    lambdas =  logspace(-2,2,20); % Spatial Regularization parameter values
    lambda_t = [];                % Temporal Regularization Parameter Values    
    tempFactor = 1;               % Factor for dependant temporal regularization
    tol = 1e-6;                   % Convergence tolerances
        
    initialGuess = [];
    
    % Flags
    WeightLeadField = false;      % Flag to enable direct weighting of leadfield
    useDependantLambdaT = false;  % Flag to enable dependant temporal regularization
    isVectorSolution = true;      % Flag to enable/disable vector solutions
    corrHelmholtz = false;
    
    % Derived properties (Set when calling obj.check)
    totalInversions = 0;
    allLambdas  = [];
    allLambda_t = [];
    
    %
    useAverageReference = false;
    modelName = []; % What model was used for these reconstructions
    modelDir = []; %Directory the model is in
    
    xTrue = [];
    
    solutionName;
    
  end
  
  methods
    function obj = options_LinearReconstruction()
      options.TempRegType = 'none';
    end;
    
    
    
    % Check that spatial and temporal regularization are consistent and
    % compute regularization parameter pairs for all 
    function obj = check(obj)
      if ~strcmpi(obj.TempRegType,'none')
        if obj.useDependantLambdaT
          
          if ~isempty(obj.lambda_t)&&(obj.lambda_t~=obj.tempFactor)
            error('Lambda_t must be empty if using dependant temporal regularization');
          else
            if obj.tempFactor==0
              error('You''re using temporal regularizer, but have set the temporal factor to zero');
            elseif length(obj.tempFactor)==1
              obj.totalInversions = length(obj.lambdas);
              obj.lambda_t    = obj.tempFactor;
              obj.allLambdas  = obj.lambdas;
              try
              obj.allLambda_t = obj.lambdas*obj.tempFactor;
              catch
                keyboard
              end;
            else
              obj.totalInversions = length(obj.lambdas)*length(obj.tempFactor);
              obj.allLambdas = repmat(obj.lambdas(:),1,length(obj.tempFactor));
              
              obj.allLambda_t = obj.allLambdas;
              obj.allLambda_t = obj.allLambda_t*diag(obj.tempFactor);
              
              obj.allLambdas = obj.allLambdas(:);
              obj.allLambda_t = obj.allLambda_t(:);
            end;
          end;
          
        else
          
          if isempty(obj.lambda_t)
            error('Lambda_t must NOT be empty if using independent temporal regularization');
          else
            obj.totalInversions = length(obj.lambdas)*length(obj.lambda_t);
            obj.allLambdas = repmat(obj.lambdas(:),1,length(obj.lambda_t));
            obj.allLambdas = obj.allLambdas(:);
            obj.allLambda_t = repmat(obj.lambda_t(:)',length(obj.lambdas),1);
            obj.allLambda_t = obj.allLambda_t(:);
          end;
        end;
      else
        obj.lambda_t = 0;
        obj.allLambdas = obj.lambdas(:);
        obj.allLambda_t = zeros(size(obj.allLambdas));
      end;
    end;        
   
  
  function disp(obj)
  
  disp(['solutionTechnique: ' obj.solutionTechnique]);
  disp(['solutionName: ' obj.solutionName]);
  disp(['modelName: ' obj.modelName]);
  disp(['modelDir: ' obj.modelDir]);
  disp([' ']);
  disp(['isVectorSolution: ' num2str(obj.isVectorSolution)]);
  disp(['weightRegularizer: ' num2str(obj.weightRegularizer)]);
  disp(['WeightLeadField: ' num2str(obj.WeightLeadField)]);
  disp(['oldWeighting: ' num2str(obj.oldWeighting)]);
  disp(['useElec: ' num2str(obj.useElec)]);
  disp(['niters: ' num2str(obj.niters)]);
  disp(['lambdas: ' num2str(obj.lambdas)]);
  disp(['lambda_t: ' num2str(obj.lambda_t)]);
  disp(['tol: ' num2str(obj.tol)]);
  disp(['useAverageReference: ' num2str(obj.useAverageReference)]);
  disp(['xTrue: ' num2str(obj.xTrue)]);
  
  if any(obj.initialGuess)
    disp(['Initial Guess:  INITIALIZED']);
  else
    disp(['Initial Guess:  NONE']);
  end;        
%  disp(['solutionSpace: ' obj.solutionSpace]);
  disp(['SpaceRegType: ' obj.SpaceRegType]);
  disp(['HHFact: ' obj.HHFact]);
  if obj.corrHelmholtz
    disp(['corrHelmholtz: TRUE']);
  else
    disp(['corrHelmholtz: FALSE']);
  end
  disp(['TempRegType: ' obj.TempRegType]);
  disp(['TempHHFact: ' obj.TempHHFact]);
  disp(['tempFactor: ' num2str(obj.tempFactor)]);
  disp(['useDependantLambdaT: ' num2str(obj.useDependantLambdaT)]);
             
  end
  
  end
  
end
