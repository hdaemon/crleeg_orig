function [name] = get_SolutionName(reconOptions)

if ~isempty(reconOptions)
  
  switch lower(reconOptions.solutionTechnique)
    case 'multiresolution'
      name = 'MultiResolution';
    
    case 'underdetls'
      
      if reconOptions.weightRegularizer&&reconOptions.WeightLeadField
        weightstring = 'RegLFWtng';
      elseif reconOptions.weightRegularizer
        weightstring = 'RegWtng';
      elseif reconOptions.WeightLeadField;
        weightstring = 'LFWtng';
      else
        weightstring = 'NoWtng';
      end;
      
      if reconOptions.oldWeighting
        weightstring = [weightstring 'OLD'];
      end;
      
      if reconOptions.isVectorSolution
        vecstring = 'Vec';
      else
        vecstring = 'Const';
      end;
      
      switch lower(reconOptions.SpaceRegType)
        case 'loreta'
          spacestring = 'LORETA';
        case 'helmholtz'
          if reconOptions.corrHelmholtz
            spacestring = ['CHH' num2str(reconOptions.HHFact)];
          else
            spacestring = ['HH' num2str(reconOptions.HHFact)];
          end;
        case 'minnorm'
          spacestring = 'MinNorm';
        case 'armodel'
          spacestring = 'ARModel';
        otherwise 
          spacestring = '';
      end
      
      switch lower(reconOptions.TempRegType)
        case 'temporalhh'
          tempstring = ['TempHH' num2str(reconOptions.TempHHFact) '_TFact' num2str(reconOptions.tempFactor)];
        case 'datacorr'
          tempstring = ['TempDataCorr' ];
        otherwise
          tempstring = '';
      end
      
      name = ['ST_' spacestring '_' tempstring '_' weightstring '_' vecstring];
    case 'loreta'
      
      if reconOptions.weightRegularizer&&reconOptions.WeightLeadField
        weightstring = 'RegLFWtng';
      elseif reconOptions.weightRegularizer
        weightstring = 'RegWtng';
      elseif reconOptions.WeightLeadField;
        weightstring = 'LFWtng';
      else
        weightstring = 'NoWtng';
      end;
      
       if reconOptions.isVectorSolution
        vecstring = 'Vec';
      else
        vecstring = 'Const';
      end;
      
      name = ['LORETA' weightstring '_' vecstring];
    otherwise
      name = 'FLABLARGH';
      %error('Unknown solution technique');
  end
  
else
  disp('Don''t know how to name this!');
  name = '';
end;


end