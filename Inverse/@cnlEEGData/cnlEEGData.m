classdef cnlEEGData < cnlMatchedData
  % classdef cnlEEGData < handle
  %
  % Handle class for dealing with input EEG data.
  %
  % Inherited Properties: (From cnlMatchedData)
  %   datafile      : Object or structure with data in the .data field
  %   elecLabels    : List of labels for electrodes in model
  %
  % Inherited Dependent Properties: (From cnlMatchedData)
  %   currLabels    : List of labels associated with data_parsed
  %   data_parsed   : Input data with columns reordered to match elecLabels
  %
  % Properties:
  %   datafile      : Object or structure with data in the .data field
  %   badfile       : Object or structure with labels of bad electrodes
  %                      in the .badlabel field.
  %   elecLabels    : List of labels for electrodes in model
  %   gndElectrode  : Index of electrode to use as a reference
  %   useTimes      : List of timepoints to use
  %   nEpochs       : Number of epochs in the data
  %   epochs_start  : Start time of each epoch
  %   epochs_end    : End time of each epoch
  %
  % Dependant Properties:
  %   currLabels    : Labels associated with current data_* fields
  %   dataLabels    : Labels associated with input data file
  %   badLabels     : List of bad labels associated with bad electrodes
  %   useElec       : List of electrodes currently used for data_* fields
  %   data_parsed   : Data parsed according
  %   data_reref    :
  %   data_avgref   :
  %
  % Part of the cnlEEG project, 2014
  % Written by: Damon Hyde

  properties
    % Input files/objects
    badfile
    gndElectrode = [];
    useTimes

    removeTemporalMean = false;
    removeTemporalMeanPerEpoch = false;
    
    outputType = 'avgref';

    % Properties to deal with epoched data
    nEpochs
    epochs_start
    epochs_end

  end

  properties (Dependent=true)
    %% Label Properties
    badLabels   % Labels of bad electrodes
    badElec
    gndElec
    %%
    useElec
    useData    

    data_epoched
  end;

  properties (Hidden = true)
    VALID_OUTPUT_TYPES =  {'recorded' 'reref' };
  end


  properties (Dependent = true, Hidden = true)
    data_reref   % Data rereferenced to obj.gndElectrode
    data_avgref  % Average referenced data
  end;


  methods

    function obj = cnlEEGData(file,varargin)
      if ~exist('file','var'), file = [];  end;
      obj = obj@cnlMatchedData(file);
      
      if nargin>0
        p = inputParser;

        addParamValue(p,'elec',[]);
        addParamValue(p,'model',[]);
        addParamValue(p,'badfile',[]);
        addParamValue(p,'eleclabels',[]);
        addParamValue(p,'outputtype','reref');
        addParamValue(p,'usetimes',[]);
        addParamValue(p,'gndElec',[]);

        parse(p,varargin{:});
                
%
%         if isfield(obj.datafile,'nEpochs')
%           obj.nEpochs = obj.datafile.nEpochs;
%           obj.lenEpochs = obj.datafile.epochs_end - obj.datafile.epochs_start;
%         end;

        obj.badfile = p.Results.badfile;
        if ~isempty(p.Results.model)
          if ~isempty(p.Results.eleclabels)
            error('You can set either ''model'' or ''eleclabels'', but not both');
          end

          obj.elecLabels = p.Results.model.Electrodes.Labels;
          obj.gndElectrode = p.Results.model.gndElectrode;
        else
          obj.elecLabels = p.Results.eleclabels;
          obj.gndElectrode    = p.Results.gndElec;
        end;


      end
    end


    function reinit(obj)
      if isfield(obj.datafile,'nEpochs')
        obj.nEpochs = obj.datafile.nEpochs;
        obj.epochs_start = obj.datafile.epochs_start;
        obj.epochs_end = obj.datafile.epochs_end;
      else
        obj.nEpochs = 1;
        obj.epochs_start = 1;
        obj.epochs_end = size(obj.data,1);
      end;
    end

    function plot(obj)
      %uitools.cnlDataPlot(obj.data,obj.currLabels(obj.useData));
      uitools.plots.dataexplorer(obj.data,'labels',obj.currLabels(obj.useData));
    end

    %% Methods for Parsed, Rereferenced, and Average Referenced Dependant Properties

    function out = data(obj)
      switch obj.outputType
        case 'reref'
          tmp = obj.data_reref;
          if ~isempty(tmp)
            out = tmp(:,obj.useData);
          else
            out = [];
          end;
        case 'recorded'
          out = obj.data_parsed(obj.useTimes,obj.useData);
        case 'avgref'
          out = obj.data_avgref;
      end
      
      if obj.removeTemporalMean
        out = out - repmat(mean(out,1),size(out,1),1);
        %for i = 1:size(out,2);
        %  out(:,i) = out(:,i) - mean(out(:,i));
        %end
      end
      
    end

    function out = get.data_reref(obj)
      % function out = get.data_reref(obj)
      %
      %

      % If the ground electrode isn't set, return a warning and an empty set.
      if isempty(obj.gndElec),         
        warning('cnlEEGData:noGround',...
          'Trying to get data_reref without a ground assigned. Returning an empty array');
          out = []; 
          return; 
      end;
      out = obj.data_parsed(obj.useTimes,:) - repmat(obj.data_parsed(obj.useTimes,obj.gndElec),...
        [1 size(obj.data_parsed,2)]);
      %out(:,obj.gndElec) = [];
    end

    function out = get.data_avgref(obj)
      % function out = get.data_avgref(obj)
      %
      %

      avgref = mean(obj.data_parsed(obj.useTimes,obj.useData),2);
      out = obj.data_parsed(obj.useTimes,obj.useData) - repmat(avgref,[1 size(obj.data_parsed(:,obj.useData),2)]);

    end

    %% Getting and setting the timepoints to use.
    function out = get.useTimes(obj)
      % function out = get.useTimes(obj)
      %
      % If obj.useTimes_Stored hasn't been set, just return the full length of the
      % input data block.
      out = obj.useTimes;
      if isempty(out)&&~isempty(obj.datafile), 
        out = 1:size(obj.datafile.data,1); 
      end
    end


    function set.badfile(obj,val)
      % function set.badfile(obj,val)
      %
      %
      if isempty(val)||isa(val,'file_BAD')||isfield(val,'badLabels'),
        obj.badfile = val;
      else error('Incorrect file type');
      end;
    end;

    function set.badLabels(obj,val)
      % function set.badLabels
      %
      % Throws an error to describe itself.
      error('cnlEEGData:cantSetBadLabels',...
        ['cnlEEGData.badLabels is set by assigning cnlEEGData.badfile ' ...
        'with the .badLabels field']);
    end;
    

    %% Get methods for obj.dataLabels, obj.elecLabels, obj.badLabels,
    %% and obj.currLabels
    function out = get.badLabels(obj)
      %  function out = get.badLabels(obj)
      %
      %  If obj.badfile is defined, get the list of bad labels from it.
      %  Otherwise return an empty set.
      out = [];
      if ~isempty(obj.badfile),
        out = obj.badfile.badLabels;
      end;
    end;

    function out = get.badElec(obj)
      out = [];
      if ~isempty(obj.badLabels)
      badLabels  = obj.badLabels;
      currLabels = obj.currLabels;

      idx = 0;
      for idxElec = 1:length(badLabels)
        tmp = find(strcmpi(badLabels{idxElec},currLabels));
        if ~isempty(tmp)
          idx = idx+1;
          out(idx) = tmp;
        end
      end
      end;
    end

    function out = get.gndElec(obj)
      % function out = get.gndElec(obj)
      %
      % Find the index into obj.data_parsed associated with the current
      % ground electrode label
      out = [];
      if ~isempty(obj.gndElectrode)
        if isnumeric(obj.gndElectrode)
         out = find(strcmpi(obj.elecLabels{obj.gndElectrode},obj.currLabels));
        else
          out = find(strcmpi(obj.gndElectrode,obj.currLabels));
        end
      end
    end

    function out = get.useElec(obj)
      % function out = get.useElec(obj)
      %
      % Given the current list of
      out = obj.idxIntoElec;
      switch obj.outputType
        case 'reref'
          out([obj.badElec obj.gndElec]) = []; %setdiff(out,obj.gndElectrode,'stable');
        case 'recorded'
          out(obj.badElec) = [];
        case 'avgref'
          out(obj.badElec) = [];
      end
    end

    function out = get.useData(obj)
      % function out = get.useData
      % 
      % Get the appropriate indices into obj.data_parsed 
       useElec = obj.useElec;
       eLabels = obj.elecLabels(useElec);
       dLabels = obj.currLabels;     
       out = [];
       for idx = 1:numel(dLabels)
         if ismember(dLabels{idx},eLabels)
            out = [out  idx];
          end;
       end
    end

    function out = get.data_epoched(obj)
      % function out = dataEpochs(obj)
      %
      % Returns a cell array with each cell containing the data from a
      % single epoch.  If no epochs are defined for the dataset, just
      % returns a single epoch with all of the data in it.
      %
      
        % If epochs haven't been defined, just return a single cell with
        % all the data
        if isempty(obj.nEpochs)   
          out = {obj.data};          
          return;
        end

        % If we have a bunch of epochs defined, output each one in its own
        % cell.
        out = cell(1,obj.nEpochs);
        tmp = obj.data;
        for idx = 1:obj.nEpochs
          out{idx} = tmp(obj.epochs_start(idx):obj.epochs_end(idx),:);
          if obj.removeTemporalMeanPerEpoch
            out{idx} = out{idx} - repmat(mean(out{idx},1),size(out{idx},1),1);
          end
        end;

    end

    function set.data_epoched(obj,epochCell)
      % function set.data_epoched(obj,epochCell)
      %
      % 
        
        % Concatenate the epoched data
        catEpoch = cat(1,epochCell{:});
        
        % Get number of electrodes provided
        nElecIn = size(catEpoch,2); 
        
        % Modify behavior depending on the number of electrode provided
        % There are two options: provide data for EVERY electrode in the
        % currLabels list, or provide data just for the ones in obj.useData
        switch nElecIn
          case numel(obj.datafile.labels)
            tmpData = catEpoch;
          case numel(obj.currLabels)
            tmpData = catEpoch;
          case numel(obj.useData)
            tmpData = zeros(size(catEpoch,1),numel(obj.currLabels));
            tmpData(:,obj.useData) = catEpoch;
          otherwise
            error('cnlEEGData:sizeMismatch',...
              'New epoched data has the incorrect number of electrodes');
        end
                        
        % Construct a new temporary data file structure
        tempdatafile.labels = obj.currLabels(obj.useData);        
        %tempdatafile.labels = obj.datafile.labels;
        tempdatafile.data = tmpData;       

        % Get the lengths and start/end times for each epoch.        
        epochlengths = reshape(cellfun(@(val)(size(val,1)),epochCell),1,[]);
        starttimes=[1 cumsum(epochlengths(1:end-1))+1];
        endtimes=cumsum(epochlengths);


        tempdatafile.nEpochs = numel(epochlengths);
        tempdatafile.epochs_start=starttimes;
        tempdatafile.epochs_end=endtimes;
        
        obj.datafile = tempdatafile;
        obj.nEpochs = numel(epochlengths);
        obj.epochs_start = starttimes;
        obj.epochs_end=endtimes;
        
    end
    
    function out = HFO_Epochs(obj)
        epochLengths=obj.epochs_end-obj.epochs_start+1;
        
        % All frequency values are in Hz.
        Fs = obj.datafile.header.SampleRate;  % Sampling Frequency

        Fstop = 70;        % Stopband Frequency
        Fpass = 80;        % Passband Frequency
        Astop = 60;          % Stopband Attenuation (dB)
        Apass  = 1;           % Passband Ripple (dB)

        if(exist('fdesign'))
            h  = fdesign.highpass('Fst,Fp,Ast,Ap', Fstop, Fpass, Astop, Apass, Fs);
            Hd = design(h, 'equiripple');

            out=cell(obj.nEpochs,1);
            for i=1:obj.nEpochs
                if(numel(Hd.Numerator)*3 > epochLengths(i)) % if the existing filter is too long, design a new filter
                    if(floor(epochLengths(i)/3)<3) % the filter order has to be >=3, so if that's not possible, just pass the data back as-is
                        out{i}=obj.data_epoched{i};
                    else
                        htemp=fdesign.highpass('N,Fst,Fp',floor(epochLengths(i)/3),Fstop, Fpass, Fs);
                        Hdtemp=design(htemp, 'equiripple');

                        out{i}=filtfilt(Hdtemp.Numerator,1,obj.data_epoched{i});
                    end
                else
                    out{i}=filtfilt(Hd.Numerator,1,obj.data_epoched{i});
                end
            end
        else % use alternative functions for filter design and filtering
            h=firls(200,[0 Fstop Fpass Fs/2]./(Fs/2),[0 0 1 1]);

            out=cell(obj.nEpochs,1);
            for i=1:obj.nEpochs
                if(numel(h)*3 > epochLengths(i)) % if the existing filter is too long, design a new filter
                    if(floor(epochLengths(i)/3)<3) % the filter order has to be >=3, so if that's not possible, just pass the data back as-is
                        out{i}=obj.data_epoched{i};
                    else
                        htemp=firls(floor(epochLengths(i)/3),[0 Fstop Fpass Fs/2]./(Fs/2),[0 0 1 1]);

                        out{i}=filttlif(htemp,1,obj.data_epoched{i});
                    end
                else
                    out{i}=filttlif(h,1,obj.data_epoched{i});
                end
            end
        end
        
    end

  end

  methods (Access = protected)

    function parseData(obj,val)
      % function parseData(obj,val)
      %
      % parseData(obj,val) is called by cnlMatchedData.set.datafile
      % whenever the datafile in the object is changed.  This subsequently
      % sets the nEpochs, epochs_start, and epochs_end fields to the values
      % output by
      disp('Calling cnlEEGData.setdata');
      %obj.parseData@cnlMatchedData(val);
      if isnumeric(obj.datafile.data) 
        % Data is provided in the data object as a matrix.  See if there
        % are epochs defined on it
        if isfield(obj.datafile,'nEpochs')|isprop(obj.datafile,'nEpochs'),
          obj.nEpochs = obj.datafile.nEpochs; end;
        if isfield(obj.datafile,'epochs_start')|isprop(obj.datafile,'epochs_start'),
          obj.epochs_start = obj.datafile.epochs_start; end;
        if isfield(obj.datafile,'epochs_end')|isprop(obj.datafile,'epochs_end'),
          obj.epochs_end = obj.datafile.epochs_end; end;

      elseif iscell(obj.datafile.data)
        % Data is provided in the data object as a cell array.
        % Automatically extract epoch information.
        disp('Processing cell array input');
        obj.nEpochs = length(obj.datafile.data);
        epochMat = cat(1,obj.datafile.data{:});
        
        epochlengths = cellfun(@(val)(size(val,1)),obj.datafile.data);
        epochlengths = reshape(epochlengths,1,[]);
        
        obj.epochs_start = [1 cumsum(epochlengths(1:end-1))+1];
        obj.epochs_end   = cumsum(epochlengths);
        
        obj.datafile.data = epochMat;

      end
    end

  end

end
