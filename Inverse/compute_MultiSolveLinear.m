function [spikesOut] = compute_MultiSolveLinear(EEG,LeadField,reconOptions)
% function [spikesOut] = get_MultiEEGSolve(EEG,LeadField,reconOptions)
%
% You should make sure you are in the appropriate
% /sourcelocalization/solutions directory before calling this.

rOptSize = size(reconOptions);
reconOptions = reconOptions(:);

Solution = cell(rOptSize);
for m = 1:numel(EEG) 
  mydisp(['Computing solution for data set #' num2str(m)]);
  for i = 1:numel(reconOptions)
    if isa(reconOptions{i},'options_LinearReconstruction')
    Solution{i} = compute_LinearSolutions(LeadField,EEG(m),reconOptions{i});
    end;
  end;
  
  Solution = reshape(Solution,rOptSize);
  
  spikesOut{m}.EEG   = EEG(m);
  spikesOut{m}.Recons = Solution;    
  
end

spikesOut = reshape(spikesOut,size(EEG));

end