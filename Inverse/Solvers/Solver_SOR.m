function [xOpt] = Solver_SOR(MatIn,Data,niters,tol,x)

omega = 1.95;
r = Data;
x = zeros(size(MatIn,1),1);
for idxIter = 1:niters
  for i = 1:size(MatIn,1)
    tic
    xTmp = x;
    xTmp(i) = 0;
    sigma = MatIn(i,:)*xTmp(:);
    x(i) = (1-omega)*x(i) + (omega/MatIn(i,i))*(Data(i)-sigma);
    toc
  end;
  rold = r;
  r = Data- MatIn*x;
  disp(['Residual: ' num2str(norm(r))]);
end;

return;