%function [x] = Solver_CG(MatIn,Data,niters,tol,x)
%
%  A conjugate gradient solver that uses the Jacobi preconditoner to
%  accelerate the convergence.
% 
%  Inputs:  MatIn:  A positive symmetric definite matrix to be inverted
%           Data:   Data to solve for
%           niters: Maximum number of iterations (optional, default 200)
%           tol:    Target solution tolerance (optional, default 1e-4)
%           x:      Initial value for x (optional, default all zeros)
%

function [xOut] = Solver_CG_preconditioned(MatIn,Data,niters,tol,x)

disp('Using Conjugate Gradient WIth A Jacobi Preconditioner');

xOut = zeros(size(MatIn,2),1);
solPoints = find(any(MatIn,2));
MatIn = MatIn(solPoints,solPoints);
Data = Data(solPoints);
if exist('x')
x = x(solPoints);
end;

%% Get Preconditioner
tmp = diag(MatIn);
idx = 1:length(tmp);
Q = find(tmp~=0);
len = length(tmp);

tmp = 1./tmp(Q);
idx = idx(Q);
M = sparse(idx,idx,tmp,len,len);
clear tmp;


%% Initial Guess, if it doesn't already exist.
if (nargin<5)||(isempty(x)), x = zeros(size(MatIn,2),1); end;
if (nargin<4)||(isempty(tol)), tol = 1e-4; end;
if (nargin<3)||(isempty(niters)), niters = 200; end;


%% Initialize other variables
r = Data - MatIn*x;
d = M*r;
deltanew = r'*d;
deltazero = deltanew;


%% Loop
CGwaitbar = waitbar(0,'Using CG to Solve System');
t = 0;
ropt = 1e100;
for idxLoop = 1:niters
  try
    tic
    q = MatIn*d;
    alpha = deltanew/(d'*q);
    
    change = norm(alpha*d)/norm(x);
    meanchange = mean(abs(alpha*d./x));
    x = x + alpha*d;

    rold = r;
    if mod(idxLoop,50)
      r = Data - MatIn*x;
    else
      r = r - alpha*q;   
    end;   
    
    % Check For Convergence
    if norm(r)/norm(Data)<tol,
      disp(['Reached Convergence in ' num2str(idxLoop) ' iterations']);
      break;
    end

    % Get Next Direction
    s = M*r;
    deltaold = deltanew;
    deltanew = r'*s;
    
    beta = deltanew/deltaold;

    dold = d;
    d = s + beta*d;

    disp(['Iter:' num2str(idxLoop) ' Residual: ' num2str(norm(r)/norm(Data),10)  ' Time:' num2str(toc,3) ' Change:' num2str(change) ' MeanChange:' num2str(meanchange)]);   
    
    t2 = toc;
    t = t+t2;
    if mod(idxLoop,100)==0
      save(['Potentials' num2str(idxLoop)],'x');
    end;
    
  catch
    keyboard;
  end;

  waitbar(idxLoop/niters,CGwaitbar);
end;

xOut(solPoints) = x;


close(CGwaitbar);
disp(['Final Residual of ' num2str(norm(Data - MatIn*x)/norm(Data))]);

return;