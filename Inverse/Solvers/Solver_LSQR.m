%function [x] = Solver_LSQR(MatIn,Data,niters,tol,x)
%
%  An LSQR solver
%
%  Inputs:  MatIn:  A positive symmetric definite matrix to be inverted
%           Data:   Data to solve for
%           niters: Maximum number of iterations (optional, default 200)
%           tol:    Target solution tolerance (optional, default 1e-4)
%           x:      Initial value for x (optional, default all zeros)
%

function [x] = Solver_LSQR(MatIn,Data,niters,tol,display_On,x)


%% Initial Guess, if it doesn't already exist.
if (nargin<6)||(isempty(x)), x = zeros(size(MatIn,2),1); end;
if (nargin<5)||(isempty(tol)), tol = 1e-4; end;
if (nargin<4)||(isempty(niters)), niters = 200; end;

%% general initialization
beta = norm(Data); u = Data/beta;

tmp  = MatIn'*u;
alpha = norm(tmp); v = tmp/alpha;

w = v;

phi_bar  = beta;
rho_bar  = alpha;

%%%%%% START MAIN LOOP
if display_On
  h = waitbar(0,'Computing');
end;

for i=1:niters
  % bidiagonalization step  
  % Update U
  tmp  = (MatIn)*v-alpha*u;
  beta = norm(tmp);  u = tmp/beta;
  
  % Update V
  tmp   = (MatIn).'*u - beta*v;
  alpha = norm(tmp);  v = tmp/alpha;
    
  % next rotate to upper bidiag
  rho     = sqrt(rho_bar^2+beta^2);
  c       = rho_bar/rho;  s=beta/rho;
  theta   =  s*alpha;
  rho_bar = -c*alpha;
  phi     = c*phi_bar;
  phi_bar = s*phi_bar;
  
  if any(isnan(x(:)));
    keyboard; end;
  
  x = x + (phi/rho)*w;
  w = v - (theta/rho)*w;
  
  
  err = Data - MatIn*x;
  
  
  mydisp(['Iteration: ' num2str(i) ' Err: ' num2str(norm(err(:))/norm(Data(:)))]);
  waitbar(i/niters,h,['Completed ' num2str(i) ' of ' num2str(niters) ' iterations']);
end % loop on i

close(h);

%keyboard;
%disp(['Final Residual of ' num2str(norm(Data - MatIn*xOpt)/norm(Data))]);

return;