%function [x] = Solver_CG_Normals(MatIn,Data,niters,tol,x)
%
%  A conjugate gradient solver applied to the normal equations.  
% 
%  Solves:  argmin_x ||Ax-b||^2 + \lambda^2*||L(x-mu)||^2
%
%  A few assumptions are made:
%
%   MatIn is the leadfield matrix for a SINGLE timepoint.  
%   Data  is an (NxM) matrix where:
%     N is the number of electrodes
%     M is the number of timepoints
%   RegMat is an object which has an overloaded multiplication 
%

function [x] = Solver_CG_Normals(MatIn,RegMat,Data,RegMean,niters,tol,x)

tStart = clock;

%% Set Initial Guess, Convergence Tolerance, and Number of Iterations
if (nargin<7)||(isempty(x)), x = zeros(size(MatIn,2),size(Data,2)); end;
if (nargin<6)||(isempty(tol)), tol = 1e-6; end;
if (nargin<5)||(isempty(niters)), niters = 200; end;
if isempty(RegMean), RegMean = zeros(size(MatIn,2),size(Data,2)); end;

%% Build Right Hand Side
DataInitial = double(MatIn'*Data);
if ~isempty(RegMat)
  RegInitial = RegMat'*(RegMat*RegMean);  
  RHS = DataInitial + RegInitial;
else
  RHS = DataInitial;
end;


%% Initialize The Residual
r = RHS - MatIn'*(MatIn*x);
p = r;

%% Loop
t = 0;
tic
for idxLoop = 1:niters
    tic
    % Compute New Solution Estimate
    if ~isempty(RegMat)
      Ap = MatIn'*(MatIn*p) + RegMat'*(RegMat*p);
    else
      Ap = MatIn'*(MatIn*p);
    end;
    alpha = (r(:)'*r(:))/(p(:)'*(Ap(:)));   %% Standard CG Approach
   % alpha = (r(:)'*Ap(:))/(Ap(:)'*Ap(:));   %% My Bizarro Method
    
    x = x + alpha*p;
    
    if any(isnan(x(:))); keyboard; end;
    
    % Compute New Residual
    rold = r;
    r = r - alpha*Ap;
    
    % Compute New Direction
    beta = (r(:)'*r(:))/(rold(:)'*rold(:));
    pold = p;
    p = r + beta*p;
   % toc
    % Check Convergence
    res = norm(r(:))/norm(DataInitial(:));
    if res<tol,
      %mydisp(['Reached Convergence in ' num2str(idxLoop) ' iterations']);
      break;
    end
    
   
   % mydisp(['Completed iteration ' num2str(idxLoop) ' in ' num2str(toc) ' seconds. Residual is ' num2str(res)]);

end;

%mydisp(['Final Residual of ' num2str(norm(r(:))/norm(DataInitial(:)))]);
%mydisp(['Computed solution with Solver_CG_Normals in ' num2str(etime(clock,tStart)) ' seconds']);
    
return;