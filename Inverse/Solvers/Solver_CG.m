
%function [x] = Solver_CG(MatIn,Data,niters,tol,x)
%
%  A conjugate gradient solver that uses the Jacobi preconditoner to
%  accelerate the convergence.
%
%  Inputs:  MatIn:  A positive symmetric definite matrix to be inverted
%           Data:   Data to solve for
%           niters: Maximum number of iterations (optional, default 200)
%           tol:    Target solution tolerance (optional, default 1e-4)
%           x:      Initial value for x (optional, default all zeros)
%

function [xOut] = Solver_CG(MatIn,Data,niters,tol,display_on,x)

try
  
  if ~exist('display_on')||isempty(display_on)||(display_on==0)
    %mydisp('Display is OFF');
    display_on = 0;
  else
    mydisp('Display is ON');
  end;
  
  %% Initial Guess, if it doesn't already exist.
  if (nargin<6)||(isempty(x)), x = zeros(size(MatIn,2),size(Data,2)); end;
  if (nargin<4)||(isempty(tol)), tol = 1e-4; end;
  if (nargin<3)||(isempty(niters)), niters = 200; end;
       
  %% Initialize other variables  
  r = Data - MatIn*x; %(:);
  d = r;
  rsold = r(:)'*r(:);
  
  %% Initialize Waitbar
  if display_on
    CGwaitbar = waitbar(0,'Using CG to Solve System');
  end;
      
  %% Loop
  for idxLoop = 1:niters
      tic
      Ad = MatIn*d;
                 
      % Get Distance            
      alpha = (rsold)/(d(:)'*Ad(:));
      
      if alpha ==0, mydisp('Alpha is ZERO'); end;      
      change     = norm(alpha*d(:))/norm(x(:));
      meanchange = mean(abs(alpha*d(:)./x(:)));
      
      rold = r;
      x = x + alpha*d;  % Update Solution                 
      r = r - alpha*Ad; % Update Residual
      
      rsnew = r(:)'*r(:);     
      beta = rsnew/(rsold);      
      d = r + beta*d;
      
     % if rsnew>rsold, keyboard; end;
      
      rsold = rsnew;
      
%        if mod(idxLoop,10) ==0
%          r = Data - MatIn*x;
%          d = r;
%        end;
      
      %Check for convergenve
      if norm(r)/norm(Data)<tol,
        mydisp(['Reached Convergence in ' num2str(idxLoop) ' iterations']);
        break;
      end
      
      if display_on
        residual = Data - MatIn*x;        
        mydisp(['Iter:' num2str(idxLoop) ' ResidualA: ' num2str(norm(residual),10) ' Residual B: ' num2str(norm(r(:)),10) ' Time:' num2str(toc,3) ' Change:' num2str(change) ' MeanChange:' num2str(meanchange)]);
        pause(0.01);
      end;
          
    if display_on
      waitbar(idxLoop/niters,CGwaitbar);
    end;
    
  end;
  
  xOut = x;
  
  if display_on
    close(CGwaitbar);
    mydisp(['Final Residual of ' num2str(norm(Data - MatIn*xOut)/norm(Data))]);
  end;
  
catch
  keyboard;
end

return;
