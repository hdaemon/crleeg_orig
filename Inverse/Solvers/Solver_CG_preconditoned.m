%function [x] = Solver_CG(MatIn,Data,niters,tol,x)
%
%  A conjugate gradient solver that uses the Jacobi preconditoner to
%  accelerate the convergence.
% 
%  Inputs:  MatIn:  A positive symmetric definite matrix to be inverted
%           Data:   Data to solve for
%           niters: Maximum number of iterations (optional, default 200)
%           tol:    Target solution tolerance (optional, default 1e-4)
%           x:      Initial value for x (optional, default all zeros)
%

function [xOpt] = Solver_CG_preconditioned(MatIn,Data,niters,tol,x)

%% Initial Guess, if it doesn't already exist.
if (nargin<5)||(isempty(x)), x = zeros(size(MatIn,2),1); end;
if (nargin<4)||(isempty(tol)), tol = 1e-4; end;
if (nargin<3)||(isempty(niters)), niters = 200; end;

%% Get Preconditioner
tmp = diag(MatIn);
idx = 1:length(tmp);
Q = find(tmp~=0);
len = length(tmp);

tmp = 1./tmp(Q);
idx = idx(Q);
M = sparse(idx,idx,tmp,len,len);
clear tmp;

%% Initialize other variables
r = Data - MatIn*x;
z = M*r;
p = z;

%% Loop
CGwaitbar = waitbar(0,'Using CG to Solve System');
t = 0;
ropt = 1e10;
for idxLoop = 1:niters
  try
    %tic
    Ap = MatIn*p;
    alpha = (r'*z)/(p'*(Ap));
    x = x + alpha*p;

    if any(isnan(x(:))); keyboard; end;

    rold = r;
    r = r - alpha*Ap;

    if norm(r)/norm(Data)<tol,
      disp(['Reached Convergence in ' num2str(idxLoop) ' iterations']);
      break;
    end

    zold = z;
    z = M*r;
    beta = (r'*z)/(rold'*zold);

    pold = p;
    p = z + beta*p;

    if norm(r)<norm(ropt)
      idxOpt = idxLoop;
      ropt = r;
      xOpt = x;
    end;
    % t2 = toc;
    % t = t+t2;
  catch
    keyboard;
  end;
  % norm(rold)<norm(r)
  % disp(['Completed Iteration: ' num2str(idxLoop) ' with residual error: ' num2str(norm(r)/norm(Data)) ' in ' num2str(t2) ' seconds,  Total time: ' num2str(t) ...
  %         ' Current Residual:' num2str(norm(r)) ' Old Residual:' num2str(norm(rold))]);
  waitbar(idxLoop/niters,CGwaitbar);
end;

if (idxOpt~=idxLoop)
  disp('##################################################################################');
end;

close(CGwaitbar);
disp(['Final Residual of ' num2str(norm(Data - MatIn*xOpt)/norm(Data))]);

return;