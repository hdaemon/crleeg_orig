function [x] = Solver_GaussSeidel(L,U,data,x,niters,debug)

if ~exist('debug','var'), debug = 0; end;

data = data(:);
x = x(:);
try
rold = norm(data-L*x-U*x);
for idxIter = 1:niters 
  tic
   xold = x;
   x = L\(data - U*x);
   r = norm(data-L*x-U*x);
   if (rold<r),
     disp(['Breaking from Gauss Seidel after ' num2str(idxIter) ' iterations']);
     x = Solver_CG(L+U,data,10,1e-6,0,xold);     
     break;
   end;
   rold = r;
   if debug
    disp(['Completed iteration:' num2str(idxIter) ' in ' num2str(toc) ' seconds with relative residual ' num2str(norm(data-(L+U)*x)/norm(data))]);
   end;
end;
catch
  keyboard;
end;
%disp(['Completed iteration:' num2str(idxIter) ' in ' num2str(toc) ' seconds with relative residual ' num2str(norm(data-Mat*x)/norm(data))]);

return;