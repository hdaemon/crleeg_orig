%function [x] = Solver_CG(MatIn,Data,niters,tol,x)
%
%  A conjugate gradient solver that uses the Jacobi preconditoner to
%  accelerate the convergence.
%
%  Inputs:  MatIn:  A positive symmetric definite matrix to be inverted
%           Data:   Data to solve for
%           niters: Maximum number of iterations (optional, default 200)
%           tol:    Target solution tolerance (optional, default 1e-4)
%           x:      Initial value for x (optional, default all zeros)
%

function [xOut] = Solver_CG(MatIn,Data,niters,tol,x)

%% Initial Guess, if it doesn't already exist.
if (nargin<5)||(isempty(x)), x = zeros(size(MatIn,2),1); end;
if (nargin<4)||(isempty(tol)), tol = 1e-4; end;
if (nargin<3)||(isempty(niters)), niters = 200; end;

%% Get Preconditioner
% tmp = diag(MatIn);
% idx = 1:length(tmp);
% Q = find(tmp~=0);
% len = length(tmp);
%
% tmp = 1./tmp(Q);
% idx = idx(Q);
% L = sparse(idx,idx,tmp,len,len);
% clear tmp;
%
% %% Apply Preconditioner
% MatIn = L*MatIn;
% Data  = L*Data;

Data  = MatIn*Data;
MatIn = MatIn'*MatIn;

disp('Eliminating Columns of Zeros');
Q = find(any(MatIn)|any(MatIn'));
nPoints = length(Data);
try
MatIn = MatIn(Q,Q);
Data = Data(Q);
x = x(Q);
catch
  keyboard;
end;
%Mat = MatIn'*MatIn;

%% Initialize other variables
%r = MatIn'*Data - Mat*x;
r = Data - MatIn*x;
d = r;
deltaNew = r'*r;
delta0 = deltaNew;

%keyboard;

%% Loop
CGwaitbar = waitbar(0,'Using CG to Solve System');
disp('Starting Iteration');

idxLoop = 1;
while (idxLoop<=niters)&&(deltaNew>tol*delta0)
  try
      q = MatIn*d;
      alpha = deltaNew/(d'*q);
      x = x+ alpha*d;
      if (mod(idxLoop,50)==0)
        r = Data - MatIn*x;
      else
        r = r - alpha*q;
      end;
      deltaOld = deltaNew;
      deltaNew = r'*r;
      beta = deltaNew/deltaOld;
      dold = d;
      d = r + beta*d;
      disp(['Iter:' num2str(idxLoop) ' Orthog: ' num2str(acosd(d'*MatIn*dold/(norm(d)*norm(MatIn*dold)))) ' Residual: ' num2str(norm(r)) ' Time:' num2str(toc)]);
      idxLoop = idxLoop+1;           
  catch
    keyboard;
  end;
  % norm(rold)<norm(r)
  %disp(['Completed Iteration: ' num2str(idxLoop) ' with residual error: ' num2str(norm(r)/norm(Data)) ' or ' num2str(norm(Data-MatIn*x)/norm(Data)) ' in ' num2str(t2) ' seconds,  Total time: ' num2str(t) ...
  %  ' Current Residual:' num2str(norm(r)) ' Old Residual:'
  %  num2str(norm(rold))]);
  waitbar(idxLoop/niters,CGwaitbar);
end;

xOpt = x;
xOut = zeros(nPoints,1);
xOut(Q) = xOpt;

% %% Finalize Output
% 
% if (idxOpt~=idxLoop)
%   disp('##################################################################################');
% end;

close(CGwaitbar);
disp(['Final Residual of ' num2str(norm(Data - MatIn*xOpt)/norm(Data))]);

return;