function [x,xnorm,R,regc2,Err]=lsqr_hybrid(A,b,lams,maxits,xex)
%% INPUT: A is a structure.  If A.ismatrix = 1, then multiplies are 
%%   done with A.mat.  If A.ismatrix = 0, it is assumed that A.fun contains
%%   a string with the name of the function that does a matrix-vector product, using
%%   only the structure A and the vector as inputs to the routine (plus an optional
%%   string called 'transp') and A.size gives
%%   the dimension of the problem.  If 'transp' is passed as the fourth input to A.fun,
%%   then A.fun should return the product of the matrix-transpose with a vector.
%%
%%   Works only on *REAL* input matrices.
%% 
%%        b is a vector
%%        lams is a vector of Tikhonov values, usually chosen as lams=fliplr(logspace(b,a,numlams)); 
%%              where 10^{b} is the estimate of the largest singular value of the matrix is
%%              10^{a} is several orders of magnitude less than 10^{b}, and can be negative ( a<b),
%%              and numlams is the number of values to try.
%%        maxits is the max number of LSQR iterations
%%        xex is the exact solution (optional).  If xex is provided, the
%%         relative error is returned in the vector Err. 
%% OUTPUT:  
%%    x is actually a matrix with the solutions at all the different regularization params for the
%%      last step (i=maxits).  
%%    xnorm and R are matrices with the solution norm and residual norm as a function of both
%%      the iteration (column) and lambda value (row).  Thus, a log-log plot xnorm(:,k), R(:,k) for
%%      fixed k should give an L-curve is k is big enough.  
%%    The vector regc2 contains the value of lambda corresponding to the corner of each such curve.  
%%    
%%    Err (only defined if xex is input), is a matrix with relative error as a function of both
%%      the iteration (column) and lambda value (row).  
%%    Ideally, looking at a plot of the vector regc should show it becomes flat after a while, meaning
%%      the iterative LSQR method has reconstructed the best it can.  Ideally, then, we should be able
%%      to use this 'flattness' to tell us it's okay to stop iterating.  
%%    
%%    My suggestion is to choose x(:,regc2(maxits)) as the solution, assuming you've gone enough iterations.  
%%    To see if you've gone enough iterations, you can look at the error if you have it.  Otherwise, run this
%%    code once with lams=[0].   Use the l-curve to find an optimal parameter for the lambda=0 case.  
%%    You will most likely need to go at least this many iterations to find the optimal solution for
%%    the case lambda is not equal to zero.   
%%
%% Requires REGTOOLS functions: l_corner.m and dependencies (i.e. the spline toolbox).     

if A.ismatrix
 [n,m]=size(A.mat);
else
 tmp=A.size;
 m=tmp(1); n=tmp(2);
end

lamsize=length(lams);
eflag=0;

if nargin==5
  nrmx=norm(xex);
  eflag=1;
end

%% general initialization
bet=norm(b); u=b/bet; 
if A.ismatrix
 tmp=(A.mat).'*u;  
else 
 tmp = feval(A.fun,u,A.FMT,1,'transp');
end

%Q = find(tmp<0); tmp(Q) = 0; %%%%%%% HUGE MODIFICATION HERE
alph=norm(tmp); v=tmp/alph; 

for k=1:lamsize
 x(:,k)=zeros(m,1);
 w(:,k)=v;
 phi_bar(k)=bet; 
 ro_bar(k)=alph;
end

%V(:,1)=v; U(:,1)=u;

%% initialization for estimate of norm x

res2=zeros(lamsize,1);
xxnorm=zeros(lamsize,1);
c2=-1*ones(lamsize,1); s2=zeros(lamsize,1); z=zeros(lamsize,1);


bet1=bet;

%%%%%% START MAIN LOOP
 h = waitbar(0,'Computing');
for i=1:maxits
 %% bidiagonalization step - same for all lambda, so no loop over lambda here
 
 if A.ismatrix
  tmp=(A.mat)*v-alph*u;  bet=norm(tmp); u=tmp/bet;
  tmp=(A.mat).'*u - bet*v; alph=norm(tmp); v=tmp/alph;
 else
  tmp1=feval(A.fun,v,A.FMT,1); 
  if size(tmp1)~=size(u); keyboard; end;
  tmp=tmp1-alph*u; bet=norm(tmp); u=tmp/bet;
  tmp1=feval(A.fun,u,A.FMT,1,'transp'); tmp=tmp1-bet*v; 
 % Q = find(tmp<0); tmp(Q) = 0;  %% ANOTHER HUGE MODIFICATION  
  alph=norm(tmp); v=tmp/alph;
 end
  
 
 %% construct and apply rotations for each lambda
 warning('OFF','MATLAB:dividebyzero');
 for k=1:lamsize
  lam=lams(k);

  %% first rotate lambda out
  ro_tilde(k) = sqrt(ro_bar(k)^2 + lam^2);
  c_hat=ro_bar(k)/ro_tilde(k); 
  s_hat=lam/ro_tilde(k);
  phi_tilde(k)=c_hat*phi_bar(k);
  psi(k)=s_hat*phi_bar(k);
 
  %% next rotate to upper bidiag 
  ro(k) = sqrt(ro_tilde(k)^2+bet^2);
  c = ro_tilde(k)/ro(k);  s=bet/ro(k);
  theta(k) = s*alph;
  ro_bar(k)= - c*alph;
  phi(k) = c*phi_tilde(k);
  phi_bar(k) = s*phi_tilde(k);

  %% update x(lam) and w(lam)
  xold = x;
  if isnan(phi(k)); phi(k) = 0; ro(k) = 1; end;
  if ro(k)==0; ro(k) = 1; phi(k) = 0; end;
  x(:,k) = x(:,k) + (phi(k)/ro(k))*w(:,k);
  w(:,k) = v - (theta(k)/ro(k))*w(:,k);
  
  if any(isnan(x(:))); 
      keyboard; end;

  %size(V(:,1:i)*vu)  
  %csd1=conj(fft(x(:,k)));
  %csd2=(fft(b-A*x(:,k)));
  %[mymax]=max(abs([csd1.*csd2]));
  %csdtmp=csd(x(:,k),b-A*x(:,k));
  %csdh(k,i)=mymax;
  
  %% estimate the norm of x(:,k)

  delta=s2(k)*ro(k); gambar=-c2(k)*ro(k); rhs=phi(k) - delta*z(k);
  zbar=rhs/gambar;  
  xnorm(k,i)=sqrt(xxnorm(k) + zbar^2);
  gamma=sqrt(gambar^2+theta(k)^2);
  c2(k)=gambar/gamma; s2(k)=theta(k)/gamma; z(k)=rhs/gamma; 
  xxnorm(k)=xxnorm(k)+z(k)^2;

  %% R has columns as a function of lambda, rows as function of i

  res1=phi_bar(k)^2; res2(k)=res2(k)+psi(k)^2;
  R2=sqrt(res1+res2(k));
  R(k,i)=sqrt(R2^2 - lam^2*xnorm(k,i)^2);  

  if eflag; Err(k,i)= norm(xex-x(:,k))/nrmx; end
  
  %if (lamsize > 5)
  %    resvector(:,k)=b-A.mat*x(:,k); 
  %end
      
 end %loop on lambda

 warning('ON','MATLAB:dividebyzero');
  %% now create an l-curve, choose a corner, save the corner
  %% This is the Tikhonov curve for the projected problem.
 if (lamsize > 5)
   %[indx,info]=corner(xnorm(:,i),R(:,i));
   [tmp1,tmp2]=l_corner(R(:,i),xnorm(:,i),lams);
   regc2(i)=find(tmp1==lams);
   %regc(i)=indx;
   %[kcp,diff]=cumper2D_v3(resvector,1);
   %[dummy,icump_diff]=min(diff);  
   %icd(i)=icump_diff;
   %f=find(kcp); 
   %if (~isempty(f))
   %  icump=f(1);
   %else
  %   icump=0;
  % end
 
  % ic2(i)=icump;
 end

 %disp(['Completed ' num2str(i) ' of ' num2str(maxits) ' iterations']);
 waitbar(i/maxits,h,['Completed ' num2str(i) ' of ' num2str(maxits) ' iterations']); 
end % loop on i

close(h);

%keyboard;
   
return;
