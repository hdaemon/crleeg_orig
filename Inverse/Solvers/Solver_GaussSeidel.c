

#include <math.h> /* Needed for the ceil() prototype */
#include "mex.h"

/* If you are using a compiler that equates NaN to be zero, you must
 * compile this example using the flag  -DNAN_EQUALS_ZERO. For example:
 *
 *     mex -DNAN_EQUALS_ZERO fulltosparse.c
 *
 * This will correctly define the IsNonZero macro for your C compiler.
 */

#if defined(NAN_EQUALS_ZERO)
#define IsNonZero(d) ((d)!=0.0 || mxIsNaN(d))
#else
#define IsNonZero(d) ((d)!=0.0)
#endif

void mexFunction(
int nlhs,       mxArray *plhs[],
int nrhs, const mxArray *prhs[]
)
{
    /* Declare variable */
  mwSize m,n, mX, nX;
  mwIndex *irs,*jcs,j,k;
  int cmplx,isfull;
  double *pr,*sr,*Xr, *OutVec,*Br;
  
  /* Gauss Seidel Code Begins Here */

    /*Read Sparse Matrix*/
  m   = mxGetM(prhs[0]);
  n   = mxGetN(prhs[0]);
  sr  = mxGetPr(prhs[0]);
  irs = mxGetIr(prhs[0]);
  jcs = mxGetJc(prhs[0]);
  
  /*Read Input Right Hand Side*/
  mX = mxGetM(prhs[1]);
  nX = mxGetM(prhs[1]);
  Xr = mxGetPr(prhs[1]);
  
  /*Read Input Left Hand Size*/
  Br = mxGetPr(prhs[2]);
  
  /*Initialize Output*/
  plhs[0] = mxCreateDoubleMatrix(1,m,mxREAL);
  OutVec = mxGetPr(plhs[0]);
  
  int nIters = mxGetScalar(prhs[3]);
  int nnz = jcs[n];  
  int rowIdx = -1;
  int colIdx;
  double newX = 0;
  double fact = 0;
  
  int idxIter, idxElement;
    /* Loop Across Iterations*/
  for (idxIter==0; idxIter<nIters ; idxIter++){
    /* Loop Across Nonzero Elements of Sparse Matrix */
    for (idxElement==0; idxElement<nnz ; idxElement++){      
      /*If we're starting a new row, initialize the output value*/
      if (idxElement==jcs[rowIdx+1]){      
        rowIdx++;
        fact = 0;
        Xr[rowIdx] = 0;
      }
      colIdx = irs[idxElement];
      if (colIdx==rowIdx)
        fact = sr[idxElement];
      else
        Xr[rowIdx] += sr[idxElement]*Xr[colIdx];
    }
    if (fact!=0)
      Xr[rowIdx] = (Br[rowIdx]-Xr[rowIdx])/fact;
  }
  
  for (idxElement==0 ; idxElement<m ; idxElement++){
    OutVec[idxElement] = Xr[idxElement];
  }
  
  
}
 
