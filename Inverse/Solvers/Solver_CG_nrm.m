%function [x] = Solver_CG(MatIn,Data,niters,tol,x)
%
%  A conjugate gradient solver that uses the Jacobi preconditoner to
%  accelerate the convergence.
%
%  Inputs:  MatIn:  A positive symmetric definite matrix to be inverted
%           Data:   Data to solve for
%           niters: Maximum number of iterations (optional, default 200)
%           tol:    Target solution tolerance (optional, default 1e-4)
%           x:      Initial value for x (optional, default all zeros)
%

function [xOut] = Solver_CG(MatIn,Data,niters,tol,x)

%% Initial Guess, if it doesn't already exist.
if (nargin<5)||(isempty(x)), x = zeros(size(MatIn,2),1); end;
if (nargin<4)||(isempty(tol)), tol = 1e-4; end;
if (nargin<3)||(isempty(niters)), niters = 200; end;

%% Get Preconditioner
% tmp = diag(MatIn);
% idx = 1:length(tmp);
% Q = find(tmp~=0);
% len = length(tmp);
%
% tmp = 1./tmp(Q);
% idx = idx(Q);
% L = sparse(idx,idx,tmp,len,len);
% clear tmp;
%
% %% Apply Preconditioner
% MatIn = L*MatIn;
% Data  = L*Data;

Data_Orig = Data;


Data  = MatIn'*Data;
Mat   = MatIn'*MatIn;


%% Initialize other variables
r = Data - Mat*x;
d = r;

%keyboard;

%% Loop
CGwaitbar = waitbar(0,'Using CG to Solve System');
t = 0;
ropt = 1e100;
dirs = zeros(size(Mat,2),20);
disp('Starting Iteration');

%keyboard;

for idxLoop = 1:niters
  try
    tic
 %   dirs(:,idxLoop) = d(:);
    Ad = Mat*d;

    % Get Distance
    %alpha = (r'*r)/(d'*(Ad));
    %alpha = (d'*r)/(d'*(Ad));
    alpha = (r'*Ad)/(Ad'*Ad);

    % Update Residual
    rold = r;
    r = r - alpha*Ad;

    % Update Solution
    x = x + alpha*d;
    
    % This is a check for if things go off the rails
    if any(isnan(x(:)));
      if (ropt<1.5*tol)
        break;
      else
        keyboard;
      end;
    end;

%     % Update optimal solution
%     if norm(r)<ropt
%       %disp(['Found a new optimal solution at loop iteration ' num2str(idxLoop)]);
%       idxOpt = idxLoop;
%       ropt = norm(r);
%       xOpt = x;
%     end;

    %% Update Direction
    beta = (r'*r)/(rold'*rold);
    %beta = (r'*r)/(rold'*d);
    dold = d;
    d = r + beta*d;

    if mod(idxLoop,50) ==0
      %perturb = randn(size(x));
      %x = x + 0.01*norm(x)*(perturb/norm(perturb));
      r = Data - Mat*x;
      d = r;
    end;    
    
    %Check for convergenve
    if norm(r)/norm(Data)<tol,
      disp(['Reached Convergence in ' num2str(idxLoop) ' iterations']);
      break;
    end

    if mod(idxLoop,50)==0
       
    end;
    
    disp(['Iter:' num2str(idxLoop) ' Orthog: ' num2str(acosd(d'*Mat*dold/(norm(d)*norm(Mat*dold)))) ' Residual: ' num2str(norm(r)/norm(Data)) ' Res2: ' num2str(norm(Data_Orig-MatIn*x)/norm(Data_Orig)) ' Time:' num2str(toc)]);

  catch
    keyboard;
  end;
  % norm(rold)<norm(r)
  %disp(['Completed Iteration: ' num2str(idxLoop) ' with residual error: ' num2str(norm(r)/norm(Data)) ' or ' num2str(norm(Data-MatIn*x)/norm(Data)) ' in ' num2str(t2) ' seconds,  Total time: ' num2str(t) ...
  %  ' Current Residual:' num2str(norm(r)) ' Old Residual:' num2str(norm(rold))]);
  waitbar(idxLoop/niters,CGwaitbar);
end;


%xOpt = x;
%xOut = zeros(nPoints,1);
%xOut(Q) = xOpt;
xOut = x;

% %% Finalize Output
% 
% if (idxOpt~=idxLoop)
%   disp('##################################################################################');
% end;

close(CGwaitbar);
disp(['Final Residual of ' num2str(norm(Data - MatIn*xOut)/norm(Data))]);

return;