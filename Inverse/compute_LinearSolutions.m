function Solutions = compute_LinearSolutions(LeadField,EEG,reconOptions)
% function Solutions = compute_LinearSolutions(LeadField,EEG,reconOptions)
%
%
% Part of the cnlEEG Project
% Damon Hyde, 2014

mydisp('START: Computing linear solutions');

if ~isa(reconOptions,'options_LinearReconstruction')
  error('reconOptions must be an options_LinearReconstruction object')
end

% Get Appropriate Data Subset and Leadfield Matrix Rows
if reconOptions.useAverageReference
  EEG.outputType = 'avgref';  
end;

if size(EEG.data,2)==numel(EEG(1).useElec)
  %cnlEEGData_Clone object
 Data = EEG.data';
end;

if isempty(Data), error('Empty data'); end;

%% Configure the LeadField
LeadField.disableRebuild = true;

if strcmpi(EEG.outputType,'avgref')
  LeadField.useAvgReference = true;
end;

% Get the Appropriate leadfield Columns
LeadField.currSolutionSpace = reconOptions.solutionSpace;

%LeadField = LeadField.setElectrodeList(reconOptions.useElec);
LeadField.useElectrodes = EEG(1).useElec;

% Convert to a Vector Solution if Necessary
LeadField.isCollapsed = ~reconOptions.isVectorSolution;

% Set whether it's a weighted or unweighted leadfield
LeadField.isWeighted = reconOptions.WeightLeadField;

%
LeadField.disableRebuild = false;

% Check recostruction options
reconOptions = reconOptions.check;

if false %isempty(reconOptions.initialGuess)
  reconOptions.initialGuess = zeros(size(LeadField,2),size(Data,2));
end;

%% Reconstruct Using Appropriate Technique
switch lower(reconOptions.solutionTechnique)
  case 'multiresolution'
    error('Why the hell are you using this?');
    Solutions = getSol_MultiResolution(LeadField,Data,NRRDS,reconOptions);    
  case 'spatiotemporal'
    Solutions = getSol_SpatioTemporal(LeadField,Data,reconOptions);    
  case 'underdetls'
    Solutions = getSol_UnderDetLeastSquares(LeadField,Data,reconOptions);
  case 'matrixdecompunderdetls'
    if(~isa(EEG,'cnlEEGDataMatrixDecomp')), error('This solution technique requires that the EEG object is of cnlEEGDataMatrixDecomp type.'); end
    Solutions = getSol_MatrixDecompUnderDetLeastSquares(LeadField,EEG.leftMatrix,EEG.rightMatrix,reconOptions);
  case 'parcelbeam'
    Solutions = getSol_ParcelBeamFormer(LeadField,Data,reconOptions);
end

for i = 1:numel(Solutions.solutions)
 Solutions.DataApprox{i,1} = LeadField*Solutions.solutions{i};
end;
Solutions.Data = Data;

end