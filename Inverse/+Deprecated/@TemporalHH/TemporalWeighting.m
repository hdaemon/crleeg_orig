function [output] = TemporalWeighting(TempHH,Data)

if ~isa(TempHH,'TemporalHH')
  error('Why are we managing to get here?');
end;

npoints = TempHH.npoints;

if size(Data,1)==npoints
  output = diag(TempHH.TemporalWeights)*Data;
elseif size(Data,2)==npoints
  output = Data*diag(TempHH.TemporalWeights);
else
  error('Data is not of the correct size');
end;

return;