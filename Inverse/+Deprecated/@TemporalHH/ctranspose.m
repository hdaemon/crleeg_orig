function [out] = ctranspose(in);

if isa(in,'TemporalHH')
  out = in;
  out.mat = out.mat';
end;

return;