classdef TemporalHHReg
  
  properties
    mat
    npoints = 0;
    HHfact = 0;
    isWeighted = false;
  end
  
  methods
    function obj = TemporalHHReg(npoints,hhfact,isWeighted,Data)
      obj.npoints = npoints;
      obj.HHfact = hhfact;
      % Build Regularization Matrix
      for i = 1:npoints
        if i>1
          output.mat(i,i-1) = 1;
          output.mat(i,i) = output.mat(i,i) - 1;
        end;
        if i<npoints
          output.mat(i,i+1) = 1;
          output.mat(i,i) = output.mat(i,i) - 1;
        end;
        output.mat(i,i) = output.mat(i,i) - hhfact;
      end;
      
      if exist('isWeighted')&&isWeighted
        % Set Temporal Weighting
        if size(Data,1)==npoints
          output.TemporalWeights = 1./sqrt(sum(Data.^2,2));
        elseif size(Data,2)==npoints
          output.TemporalWeights = 1./sqrt(sum(Data.^2,1));
        else
          error('Incorrect Data Size for Weighting');
        end;
        output.TemporalWeights = output.TemporalWeights./max(output.TemporalWeights);
        output.mat = output.mat*diag(output.TemporalWeights);
      end;
      
    end
    
    function objOut = ctranspose(objIn)
      objOut = objIn;
      objOut.mat = objOut.mat';
    end;
    
    function out = mtimes(a,b)
      if isa(a,'TemporalHHReg')
        C1 = a.mat;
      else
        C1 = a;
      end;
      if isa(b,'TemporalHHReg')
        C2 = b.mat;
      else
        C2 = b;
      end
            
      out = C1*C2;      
    end;
    
  end
end

%   
% function [ output ] = TemporalHH( npoints , hhfact , Data )
% %TEMPORALHH Summary of this function goes here
% %   Detailed explanation goes here
% 
% if nargin == 0;
%   %Generate a default temporal helmholtz regularizer
%   output.mat = [];
%   output.npoints = 0;
%   output.hhfact = 0;
%   output = class(output,'TemporalHH');
%   
% elseif isa(npoints,'TemporalHH')  
%   %If it's already a TemporalHH, just return it
%   output = npoints;
%   
% elseif isnumeric(npoints)&&(prod(size(npoints))==1)
%   %Use the provided input to generate the appropriate matrix
%   output.mat = zeros(npoints,npoints);
%   output.npoints = npoints;
%   output.hhfact = hhfact;
%   
%   % Build Regularization Matrix
%   for i = 1:npoints
%     if i>1
%       output.mat(i,i-1) = 1;
%       output.mat(i,i) = output.mat(i,i) - 1;
%     end;
%     if i<npoints
%       output.mat(i,i+1) = 1;
%       output.mat(i,i) = output.mat(i,i) - 1;
%     end;
%     output.mat(i,i) = output.mat(i,i) - hhfact;
%   end;
%   
%   % Set Temporal Weighting
%   if size(Data,1)==npoints
%     output.TemporalWeights = 1./sqrt(sum(Data.^2,2));
%     output.TemporalWeights = output.TemporalWeights./max(output.TemporalWeights);
%     output.mat = output.mat*diag(output.TemporalWeights);
%   elseif size(Data,2)==npoints
%     output.TemporalWeights = 1./sqrt(sum(Data.^2,1));
%     output.TemporalWeights = output.TemporalWeights./max(output.TemporalWeights);    
%     output.mat = output.mat*diag(output.TemporalWeights);
%   else 
%     error('Incorrect Data Size for Weighting');
%   end;  
%   output = class(output,'TemporalHH');
%   
% end;
% 
% return;