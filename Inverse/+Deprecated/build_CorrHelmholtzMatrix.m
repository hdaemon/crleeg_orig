function [B] = build_HelmholtzMatrix(LeadField,HHFact,vectorSolution)

if ~isa(LeadField,'EEG_LeadField')
  error('Must input a SolutionSpace object')
end;

solSpace = LeadField.currSolutionSpace;

SpaceSize      = solSpace.SpaceSize;
SolutionPoints = solSpace.Voxels;
dist           = solSpace.dist;

dist = sqrt(sum(dist.^2)); % A bit of a hack for the moment.

%% Build Laplacian Matrix:
mydisp('Building Helmholtz Matrix with Additional Correlation');
mydisp(['Solution Space is of Size: ' num2str(SpaceSize(1)) ' x ' num2str(SpaceSize(2)) ' x ' num2str(SpaceSize(3))]);

% Get Indices to All Voxels
[idxX idxY idxZ] = ndgrid(1:SpaceSize(1),1:SpaceSize(2),1:SpaceSize(3));
idxX2 = kron(idxX(:),ones(6,1));
idxY2 = kron(idxY(:),ones(6,1));
idxZ2 = kron(idxZ(:),ones(6,1));

% Convert to Indices of Neighbors
offsetX = kron(ones(numel(idxX),1),[ 1 -1  0  0  0  0 ]');
offsetY = kron(ones(numel(idxY),1),[ 0  0  1 -1  0  0 ]');
offsetZ = kron(ones(numel(idxZ),1),[ 0  0  0  0  1 -1 ]');

idxX2 = idxX2 + offsetX;
idxY2 = idxY2 + offsetY;
idxZ2 = idxZ2 + offsetZ;

% Trim Those that Fall Outside the Image Volume
Q = find( ( idxX2<1 ) | ( idxX2>SpaceSize(1) ) | ( idxY2<1 ) | ( idxY2>SpaceSize(2) ) | ( idxZ2<1 ) | ( idxZ2>SpaceSize(3) ) );       
idxX2(Q)  = []; idxY2(Q)  = []; idxZ2(Q)  = [];

offsetX(Q) = []; offsetY(Q) = []; offsetZ(Q) = [];
offsetVec = [offsetX offsetY offsetZ];
vNorm = sqrt(sum(offsetVec.^2,2));
vNorm = repmat(vNorm,[1 3]);
offsetVec = offsetVec./vNorm;

% Column and Row Indexes Into Sparse matrix
colIdx    = sub2ind(SpaceSize,idxX2,idxY2,idxZ2);

  rowIdx = 1:prod(SpaceSize);
  rowIdx = kron(rowIdx(:),ones(6,1));
rowIdx(Q) = [];

% Trim To Include Only those points we are solving for
%   This might make the previous step superfluous.
Q = find(~ismember(colIdx,SolutionPoints)|~ismember(rowIdx,SolutionPoints));
colIdx(Q) = [];
rowIdx(Q) = [];
offsetVec(Q,:) = [];


%% Comute Correlation Weights Based on Surface Normals and Physical Proximity

%Convert the cortical constraint matrix into a 3xN matrix of normals
tmp = LeadField.matCortConst;
[row col val] = find(tmp);
row2 = repmat([1 2 3],[length(row)/3 1]); row2 = row2'; row2 = row2(:);
tmp2 = sparse(row2,LeadField.origSolutionSpace.Voxels(col),val,3,prod(SpaceSize));
tmp2 = tmp2';


WVa = tmp2(colIdx,:);
WVb = tmp2(rowIdx,:);
Cos_AngleBetweenNormals = (sum(WVa.*WVb,2));

Cos_AngleBetweenColNormalAndVoxelDiff = abs(sum(WVa.*offsetVec,2));
Cos_AngleBetweenRowNormalAndVoxelDiff = abs(sum(WVb.*offsetVec,2));

Cos_Avg = cosd(0.5*(acosd(Cos_AngleBetweenColNormalAndVoxelDiff)+acosd(Cos_AngleBetweenRowNormalAndVoxelDiff)));

AngleBetweenNormals = acosd(Cos_AngleBetweenNormals);
AngleAvg = acosd(Cos_Avg);
AngleColNorm = acosd(Cos_AngleBetweenColNormalAndVoxelDiff);
AngleRowNorm = acosd(Cos_AngleBetweenRowNormalAndVoxelDiff);

WeightVec1 = Cos_AngleBetweenNormals;
areAligned= (WeightVec1>0); WeightVec1 = WeightVec1 + 1; WeightVec1(~areAligned) = 0; 

WeightVec2 = Cos_Avg;
WeightVec2 = WeightVec2 + 1;

WeightVec = WeightVec1.*WeightVec2;

Q1 = (AngleBetweenNormals<45);
Q2 = (AngleAvg<45);

WeightVec = ones(size(WeightVec));
WeightVec(Q1&Q2) = 3;
WeightVec(~areAligned) = 0;

%WeightVec = WeightVec1.*WeightVec2;

%keyboard;

% Build the Sparse Matrix and Cut it Down to Size
% Again - this step might be the only necessary one, as these are currently
% just the off-diagonal components.
oldmethod = false;
if oldmethod
  A1 = sparse(rowIdx,colIdx,(1/6)*ones(size(colIdx)),prod(SpaceSize),prod(SpaceSize));  
  A1 = A1(SolutionPoints,SolutionPoints);
  
  % Get the Row Sums
  scale = sparse(1:size(A1,1),1:size(A1,1),1./(A1*ones(size(A1,2),1)));
  
  A0 = 0.5*(speye(size(A1,2))+scale)*A1;
  
  if vectorSolution
    A = kron(A0,speye(3));  
  else
    A = A0;
  end;
  
  B = (6/dist^2)*(A-(1+HHFact^2)*speye(size(A,1)));
    
else  
  A1 = sparse(rowIdx,colIdx,WeightVec,prod(SpaceSize),prod(SpaceSize));
  A1 = A1(SolutionPoints,SolutionPoints);
  
  % Get the Row Sums  
  tmp = A1*ones(size(A1,2),1);
  %tmp = tmp + 0.1*abs(tmp-6);
  scale = sparse(1:size(A1,1),1:size(A1,1),tmp);
    
  A0 = A1; 
  
  if vectorSolution
    A = kron(A0,speye(3));
    scale = kron(scale,speye(3));
  else
    A = A0;
  end;
  
  B = A - scale - HHFact^2*speye(size(A,1));
end;

return;
