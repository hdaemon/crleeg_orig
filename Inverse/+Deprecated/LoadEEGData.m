

%% Read the EDF File
cd([dirs.ProcessedRootDIR dirs.EEGData]);
[dataEEG headEEG] = sload(files.EEGData);

% Trim Data Set
if (isfield(options,'useData')&&~isempty(options.useData));
  dataEEG = dataEEG(options.useData,:);
end;

cd([dirs.ProcessedRootDIR dirs.SaveMATFiles]);

if options.usePhotogrammetry
  dataInverseInput = dataEEG(20,1:128);
  if isfield(options,'gndElectrode')
    refVals = dataInverseInput(:,options.gndElectrode);
    dataInverseInput = dataInverseInput - repmat(refVals,[1 128]);
    dataInverseInput(:,options.gndElectrode) = [];
  else
    error('option.gndElectrode is undefined! Probably shouldn'' be getting here');    
  end;
else
  Labels = upper(headEEG.Label);
  
  % Get the ground electrode label
  if (isfield(options,'gndElectrodeLabel')&&(~isempty(options.gndElectrodeLabel)))
    gndLabel = upper(options.gndElectrodeLabel);
  else
    gndLabel = 'CZ';
  end;
  
  % Get the Components of Each Data Signal
  for i = 1:length(Electrodes.Labels)
    found = strfind(Labels,[' ' upper(Electrodes.Labels{i})]);
    for j = 1:length(found)
      if ~isempty(found{j})
        FirstPos(j,i) = 1;
        if strcmp(upper(Electrodes.Labels{i}),gndLabel)
          gndElectrodeFirst = j;
        end;
      else
        FirstPos(j,i) = 0;
      end;
    end;
    found = strfind(Labels,['_' upper(Electrodes.Labels{i})]);
    for j = 1:length(found)
      if ~isempty(found{j})
        SecondPos(j,i) = 1;
        if strcmp(upper(Electrodes.Labels{i}),gndLabel)
          gndElectrodeScnd = j;
        end;
      else
        SecondPos(j,i) = 0;
      end;
    end;
  end;
  
  
  if ~any(SecondPos(:))
      % Get the Rows of the LeadField Matrix that will be used
      UseLeadFieldRows = find(any(FirstPos(:,1:end-1)));

      % Trim the data to correspond to matching electrodes
      UseElectrodes = find(any(FirstPos'));
      dataEEG = dataEEG(:,UseElectrodes);
      
      % Construct the output data structure
      dataOut = zeros(size(dataEEG));
      for i = 1:length(UseLeadFieldRows)
        idxElectrode = find(FirstPos(:,UseLeadFieldRows(i)));
        dataOut(:,i) = dataEEG(:,idxElectrode)-dataEEG(:,gndElectrodeFirst);
      end;
  else
    error('No Current Support For Bipolar Data Files ');
  end;
  
end;

%dataInverseInputZeroed = dataInverseInput - mean(dataInverseInput);