function isCompatible = checkCompatibility(spaceIn,spaceOut)
% function isCompatible = checkCompatibility(spaceIn,spaceOut)
%
% Checks to see if spaceIn can be transformed to spaceOut.  In practice,
% this means that the voxels in spaceOut are a subset of those in spaceIn.
%
% D. Hyde, Feb 2013

if isa(spaceIn,'SolutionSpace')&&(isa(spaceOut,'SolutionSpace'))
%   if strcmpi(spaceIn.type,'grid')&strcmpi(spaceOut.type,'grid')
%     isCompatible = SolutionSpace.checkCompList(spaceIn.Voxels,spaceOut.Voxels);
%   else
    if all(spaceIn.SpaceSize==spaceOut.SpaceSize)
      % Built on same size grid
      isCompatible = SolutionSpace.checkCompList(spaceIn.Voxels,spaceOut.Voxels);
    else
      % Built on different grids
      matGridToGrid = SolutionSpace.getMapGridToGrid(spaceIn.SpaceSize,spaceOut.SpaceSize);
      spaceInVoxInspaceOut = find(sum(matGridToGrid(spaceIn.Voxels,:),1));
      isCompatible = SolutionSpace.checkCompList(spaceInVoxInspaceOut,spaceOut.Voxels);
    end;
%     if ~isCompatible
%       keyboard;
%     end;
%   end;
else
  error('Both inputs must be solution spaces');
end;
end