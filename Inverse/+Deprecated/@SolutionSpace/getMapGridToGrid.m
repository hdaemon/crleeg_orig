function mapOut = getMapGridToGrid(gridIn,gridOut)
% function mapOut = getMapGridToGrid(gridIn,gridOut)
%
% Given grid sizes gridIn and gridOut, generates either a restriction or
% interpolation matrix as appropriate to map gridIn to gridOut


% Check Inputs
if all(size(gridIn)==[1 3]) & all(size(gridOut)==[1 3])
  nCols = prod(gridIn);
  nRows = prod(gridOut);
  
  if all(gridIn==gridOut)
    % They're the same grid. No Mapping Needed.
    mapOut = 1;
  else
    if nCols>nRows
      % We're downsampling to a lower resolution grid
      scale = floor(gridIn./gridOut);
      % Row References
      rowRef = kron([1:nRows]',ones(prod(scale),1));
      
      % Offsets
      [xOff yOff zOff] = ndgrid(1:scale(1),1:scale(2),1:scale(3));
      offIndex = sub2ind(gridIn,xOff,yOff,zOff);
      offsets = repmat(offIndex(:),[nRows 1]) - 1;
      
      % Starting Points
      [x y z] = ind2sub(gridOut,rowRef);
      x = 1 + (x-1)*scale(1);
      y = 1 + (y-1)*scale(2);
      z = 1 + (z-1)*scale(3);
      start = sub2ind(gridIn,x,y,z);
      
      colRef = start + offsets;
      Vals = ones(size(colRef));
      
    else
      % We're upsampling to a higher resolution grid.
      scale = floor(gridOut./gridIn);
      
      colRef = kron([1:nCols]',ones(prod(scale),1));
      
      [xOff yOff zOff] = ndgrid(1:scale(1),1:scale(2),1:scale(3));
      offIndex = sub2ind(gridOut,xOff,yOff,zOff);
      offsets = repmat(offIndex(:),[nCols 1]) -1;
      
      [x y z] = ind2sub(gridIn,colRef);
      x = 1 + (x-1)*scale(1);
      y = 1 + (y-1)*scale(2);
      z = 1 + (z-1)*scale(3);
      start = sub2ind(gridOut,x,y,z);
      
      rowRef = start + offsets;
      Vals = (1/prod(scale))*ones(size(colRef));
      
    end;
    
    mapOut = sparse(rowRef,colRef,Vals,nRows,nCols)';
    
  end;
  
else
  error('Grid Sizes Must be Three Dimensional');
end;
end