function matOut = getMapping(spaceIn,spaceOut)
% function matOut = getMapping(spaceOut,spaceIn)
%
% Given spaceOut and spaceIn as solutionSpaces, returns a matrix mapping
% spaceIn to spaceOut by right multiplication
%
try
  if isa(spaceIn,'SolutionSpace')&isa(spaceOut,'SolutionSpace')
    if checkCompatibility(spaceIn,spaceOut)            
      % Map the Input Space to Its Fundamental Grid (Left Multiplier)
      
      mydisp('Obtaining map from Input Space to Common Grid');
      if strcmpi(spaceIn.type,'grid')
        inGrid = speye(prod(spaceIn.SpaceSize));            
        inGrid = inGrid(spaceIn.Voxels,:);
      elseif strcmpi(spaceIn.type,'parcel')
        inGrid = spaceIn.matGridToParcel';
      else
        error('Shouldn''t be getting here');
      end;
      
      % Map the Output Space to Its Fundamental Grid (Right Multiplier)
      mydisp('Obtaining map from Common Grid to Output Space');
      if strcmpi(spaceOut.type,'grid')
        outGrid = speye(prod(spaceOut.SpaceSize));
        outGrid = outGrid(:,spaceOut.Voxels);
      elseif strcmpi(spaceOut.type,'parcel')
        outGrid = spaceOut.matGridToParcel;
      else
        error('Shouldn''t be getting here');
      end;
      
      % Get Map Betweens Grids (this is just 1 if they're identical)
      gridToGrid = SolutionSpace.getMapGridToGrid(spaceIn.SpaceSize,spaceOut.SpaceSize);      
      
      % Get the total mapping matrix
      matOut = inGrid*gridToGrid*outGrid;
            
%       if strcmpi(spaceOut.type,'grid')&strcmpi(spaceIn.type,'grid')
%         if all(spaceOut.SpaceSize==spaceIn.SpaceSize)
%           
%           %Q = ismember(spaceIn.Voxels,spaceOut.Voxels);
%           %matOut = speye(length(spaceIn.Voxels));
%           %matOut = matOut(:,Q);
%           
%           %I Think I can redefine it like this and have more consistency,
%           %if perhaps a slight hit to speed
%           matOut = speye(prod(spaceIn.SpaceSize));
%           matOut = matOut(spaceIn.Voxels,spaceOut.Voxels);
%           
%         else %Both grids, but different ones
%           error('Entering on incomplete code');
%           
%           matOut = getMapGridToGrid(spaceIn,SpaceOut);
%           matOut = matOut(spaceIn.Voxels,spaceOut.Voxels);
%         end;
%       else %One or the other is a parcellation
%       end;
    else
      error('Solution Spaces are Incompatible');
    end;
  else
    error('Both inputs to SolutionSpace.getMapping must be solutionSpace objects');
  end;
catch
  disp('Error Caught in Call to SolutionSpace.getMapping');
  keyboard
end;
end

