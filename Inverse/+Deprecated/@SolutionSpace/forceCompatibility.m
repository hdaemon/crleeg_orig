function spaceOut = forceCompatibility(spaceIn,spaceTarget)
% function spaceOut = forceCompatibility(spaceIn,spaceTarget)
%
% Modify spaceTarget such that checkCompatibility(spaceIn,spaceTarget) will
% return true. This is done by eliminating voxels in spaceTarget that lie
% outside the scope of spaceTarget
%
% D.Hyde Feb 2013

try

if checkCompatibility(spaceIn,spaceTarget)
  spaceOut = spaceTarget; % They're already compatible
else
  matGridToGrid = SolutionSpace.getMapGridToGrid(spaceIn.SpaceSize,spaceTarget.SpaceSize);
  if numel(matGridToGrid)>1
    spaceInVoxInSpaceOut = find(sum(matGridToGrid(spaceIn.Voxels,:),1));
  else    
    spaceInVoxInSpaceOut = spaceIn.Voxels;
  end;
  voxCompatible = ismember(spaceTarget.Voxels,spaceInVoxInSpaceOut);

  spaceOut = spaceTarget;
  spaceOut.Voxels = spaceOut.Voxels(voxCompatible);
  
  if ~isempty(spaceOut.matGridToParcel)
    allVox = 1:size(spaceOut.matGridToParcel,1);
    toZero = ~ismember(allVox,spaceOut.Voxels);
    spaceOut.matGridToParcel(toZero,:) = 0;
  end;
end

if ~checkCompatibility(spaceIn,spaceOut)
  mydisp('Couldn''t force compatibility');
  keyboard;
end;

catch
  mydisp('Error in forceCompatibility()');
  keyboard;
end;

end