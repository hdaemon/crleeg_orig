classdef SolutionSpace < cnlSolutionSpace
  % function obj = SolutionSpace(spsize,voxels,dist,desc)
  %
  % Two ways of constructing a SolutionSpace object:
  %
  %  obj = SolutionSpace(spsize,voxels,dist,desc)
  %    spsize : 1x3 matrix with spatial dimensions in voxels
  %    voxels : List of indices into space corresponding to voxels being
  %               used
  %    dist   : 1x3 matrix defining x-y-z size of voxels
  %    desc   : Description of solution space
  %
  %  obj = SolutionSpace(nrrd,useVals,type,desc)
  %    nrrd   : NRRD file containing parcellation
  %    useVals: a) A threshold value on the labelmap in nrrd
  %             b) A specific list of labels to use
  %                 NOTE: Using a single label not currently supported
  %    desc   : Description of the solution space.
  %
  % Created by: Damon Hyde, Feb 2013.
  
  properties
    SpaceSize = [0 0 0];
    Voxels = [];
    Points = [];
    type = 'grid';
    matGridToParcel = [];
    matParcelToGrid = [];
    description = ' ';
    dist = [1 1 1];
  end;
  
  methods
    function obj = SolutionSpace(spsize,voxels,dist,desc)
      % function obj = SolutionSpace(spsize,voxels,dist,desc)
      %
      % Two ways of calling this:
      %
      %  obj = SolutionSpace(spsize,voxels,dist,desc)
      %
      %  obj = SolutionSpace(nrrd,useVals,type,desc)
      %
      % Created by: Damon Hyde, Feb 2013.
      
      if isa(spsize,'SolutionSpace')
        obj = spsize;
      else
        if isa(spsize,'file_NRRD')
          try
            nrrd = spsize;
            useVals = voxels;
          catch
            error('One or more input arguments missing');
          end;
          
          if numel(nrrd.sizes)~=3
            error('Must use a 3D nrrd');
          end;
          
          obj.SpaceSize = nrrd.sizes;
          obj.dist = sqrt(sum(nrrd.spacedirections.^2,1));
          
          % If useVals is a single number, it is a threshold
          if numel(useVals) == 1
            allVals = unique(nrrd.data);
            useVals = allVals(find(allVals>=useVals));
          end;
          
          nCols = length(useVals);
          nRows = prod(nrrd.sizes);
          
          colRef = [];
          rowRef = [];
          for i = 1:length(useVals)
            Q = find(nrrd.data==useVals(i));
            colRef = [colRef ; i*ones(length(Q),1)];
            rowRef = [rowRef ; Q(:) ];
          end;
          
          Vals = ones(size(colRef));
          
          uniqueVox = unique(rowRef);
          
          % Mapping from grid to parcel
          GridToParcel = sparse(rowRef,colRef,Vals,nRows,nCols);
          
          % The inverse mapping
          Vals2 = sum(GridToParcel,1);
          ParcelToGrid = GridToParcel*spdiags(1./Vals2',0,length(Vals2),length(Vals2));
          
          obj.Voxels = uniqueVox;
          obj.matGridToParcel = GridToParcel;
          obj.matParcelToGrid = ParcelToGrid;
          obj.type = 'parcel';
          
          
        else % Using an original grid-type solutionspace
          %% Set Space Size
          if all(size(spsize)==[1 3])
            obj.SpaceSize = spsize;
          else
            keyboard;
            error('Space must be three dimensional');
          end;
          
          %% Set Points (Index into Leadfield) to Solve At
          try
            if exist('voxels')
              if (max(voxels)>prod(obj.SpaceSize))||(min(voxels)<1)
                error('Trying to use solution points outside the volume');
              else
                [voxels order] = sort(voxels);
                if any((order(1:end-1)-order(2:end))>0)
                  error('Voxel list must be sorted in ascending order');
                else
                  obj.Voxels = voxels;
                end;
              end
            end
            
            if exist('dist')
              if all(size(dist)==[1 3])
                obj.dist = dist;
              else
                error('Dist must be a 1x3 matrix');
              end;
            end;
            
            % Set a description for the solution field
            if exist('desc')
              obj.description = desc;
            end;
          catch
            keyboard;
          end
          
        end;
        
      end
    end
    
%     function matOut = getVectorMapping(spaceIn,spaceOut)
%       % function matOut = getVectorMapping(spaceIn,spaceOut)
%       %
%       % Given two solutionSpaces, spaceIn and spaceOut, returns a matrix to
%       % map a vector solution from spaceIn to spaceOut by right
%       % multiplication.
%       try
%         if checkCompatibility(spaceIn,spaceOut)
%           matOut = getMapping(spaceIn,spaceOut);
%           matOut = kron(matOut,speye(3));
%         else
%           error('Solution Spaces are Incompatible');
%         end;
%       catch
%         disp('Caught error in getVectorMapping(spaceIn,SpaceOut)');
%         keyboard;
%       end;
%     end
    
%     function gridOut = getSolutionGrid(spaceIn)
%       x = 0:spaceIn.dist(1):((spaceIn.SpaceSize(1)-1)*spaceIn.dist(1));
%       y = 0:spaceIn.dist(2):((spaceIn.SpaceSize(2)-1)*spaceIn.dist(2));
%       z = 0:spaceIn.dist(3):((spaceIn.SpaceSize(3)-1)*spaceIn.dist(3));
%       [X Y Z] = ndgrid(x,y,z);
%       X = X(spaceIn.Voxels);
%       Y = Y(spaceIn.Voxels);
%       Z = Z(spaceIn.Voxels);
%       gridOut = [X(:) Y(:) Z(:)];
%     end;
    
  end
  
  
  methods(Static = true)
    
    function isCompatible = checkCompList(VoxA,VoxB)
      % function isCompatible = checkCompList(VoxA,VoxB)
      %
      % Determines if voxB is a subset of voxA
      if all(ismember(VoxB,VoxA))
        isCompatible = true;
      else
        isCompatible = false;
      end;
    end;
    
    % Define a placeholder.  Code in separate file now
    mapOut = getMapGridToGrid(gridA,gridB)
    
  end
end
