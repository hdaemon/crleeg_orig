function [ matOut ] = SpatioTemporal( SpaceReg,TempReg )
%SPATIOTEMPORAL Summary of this function goes here
%  Constructor function for the spatiotemporal regularizer class.

if nargin == 0
  matOut.Space  = [];
  matOut.Temp   = [];
  matOut.lambda  = 1;
  matOut.lambda_t = 1;
  matOut.uselambda_t = false;
  matOut.tempfact = 1;
  matOut = class(matOut,'SpatioTemporal');
elseif isa(SpaceReg,'SpatioTemporal')
  matOut = SpaceReg;
else
  if exist('SpaceReg')
    matOut.Space = SpaceReg;
  else
    matOut.Space = [];
  end;
  if exist('TempReg');
    matOut.Temp   = TempReg;
  else
    matOut.Temp = [];
  end;
  matOut.lambda = 1;
  matOut.lambda_t = 1;
  matOut.uselambda_t = false;
  matOut.tempfact = 1;
  matOut = class(matOut,'SpatioTemporal');
end

return;