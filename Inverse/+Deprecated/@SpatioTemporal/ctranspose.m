function [a] = ctranspose(a)

if ~isa(a,'SpatioTemporal')
  error('This is used only for SpatioTemporal regularizers');
end;

try
  a.Space = a.Space';
catch
  error('Error Transposing Spatial Regularizer');
end;

try
  a.Temp = a.Temp';
catch
 error('Error Transposing Temporal Regularizer');
end;

return;