
function a = set(a,varargin)

% SET Set asset properties and return the updated object
propertyArgIn = varargin;
while length(propertyArgIn) >= 2,
   prop = propertyArgIn{1};
   val = propertyArgIn{2};
   propertyArgIn = propertyArgIn(3:end);
   switch upper(prop)
   case 'LAMBDA'
      a.lambda = val;
   case 'LAMBDAT'
       a.lambda_t = val;
   case 'USELAMBDAT'
       a.uselambda_t = val;
   case 'TEMPFACT'
      a.tempfact = val;
   case 'SPACEREG'
      a.SpaceReg = val;
   case 'TEMPREG'
      a.TempReg = val;
     case 'TEMPWEIGHTS'
       a.useTempWeights = val;
   otherwise
      error('Regularization Properties: LAMBDA, TEMPFACT, SPACEREG, TEMPREG')
   end
end

return;