
function [OmegaOut] = GetOmegaMatrix(LeadField,isVectorized, wantInverse)
  try
  if isVectorized
    numNodes = size(LeadField,2)/3; %prod(SpaceSize);
    Omega = zeros(numNodes,1);

    for (idxOmega = 1:numNodes)
      idxStart = (idxOmega-1)*3 +1;
      Omega(idxOmega) = sqrt(LeadField(:,idxStart)'*LeadField(:,idxStart) + ...
        LeadField(:,idxStart+1)'*LeadField(:,idxStart+1) + ...
        LeadField(:,idxStart+2)'*LeadField(:,idxStart+2) );
    end;
  else
    numNodes = size(LeadField,2);
    Omega = zeros(numNodes,1);
    for (idxOmega = 1:numNodes)
      Omega(idxOmega) = norm(LeadField(:,idxOmega));
    end;
  end;
  
  OmegaInv = 1./Omega;
  OmegaInv(Omega==0) = 0;
  
  if wantInverse
    OmegaOut = sparse(1:length(Omega),1:length(Omega),OmegaInv);
  else
    OmegaOut = sparse(1:length(Omega),1:length(Omega),Omega);
  end;

  if isVectorized
    OmegaOut = kron(OmegaOut,eye(3));
  end;  
  
  catch
    disp('Error in Generating LeadField Matrix Weights');
    keyboard
  end;

return;