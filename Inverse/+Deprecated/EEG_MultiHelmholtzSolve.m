
function [SolutionsOut reconOptionsOut] = EEG_MultiHelmholtzSolve(reconOptions,LeadField,DATAIN,HHFacts,tHHFacts,tempFacts,doCompute)

% function [SolutionsOut] = EEG_MultiHelmholtzSolve(reconOptions,LeadField,DATAIN,HHFacts,tHHFacts);
%
% A Decent Default Set of Reconstruction Options:
% reconOptions{1,1,1} = optionsLinearReconstruction;
% reconOptions{1,1,1}.WeightLeadField = false;
% reconOptions{1,1,1}.SpaceRegType = 'Helmholtz';
% reconOptions{1,1,1}.weightRegularizer    = true;
% reconOptions{1,1,1}.HHFact = 0.25;
% reconOptions{1,1,1}.TempRegType = 'TemporalHH';
% reconOptions{1,1,1}.tempFactor = 1;
% reconOptions{1,1,1}.useDependantLambdaT = true;
% reconOptions{1,1,1}.TempHHFact = 0.05;
% reconOptions{1,1,1}.solutionSpace = solutionRegions{1};
% reconOptions{1,1,1}.niters = 100;
% reconOptions{1,1,1}.lambdas =  logspace(-2,2,10);
% reconOptions{1,1,1}.useElec = Electrodes.useElec;
% reconOptions{1,1,1}.isVectorSolution = false;
%
% % Use Leadfield Weighting Instead (ie - Generate SPMs)
% reconOptions{2,1,1} = reconOptions{1,1,1};
% reconOptions{2,1,1}.WeightLeadField = true;
% reconOptions{2,1,1}.weightRegularizer = false;
% reconOptions{2,1,1}.oldWeighting = true;
% reconOptions{2,1,1}.lambdas = logspace(-2,2,20);

% Compute Helmholtz Solutions
reconOptions = reconOptions(:);

if ~exist('doCompute'), doCompute = ones(length(HHFacts),length(tHHFacts),length(tempFacts)); end;

if length(size(doCompute))==3
  sTest = size(doCompute);
elseif length(size(doCompute))==2
  sTest = [size(doCompute) 1];
end;

if ~all(sTest==[length(HHFacts) length(tHHFacts) length(tempFacts)])
  error('doCompute must be of size [length(HHFacts) length(tHHFacts) length(tempFacts)]');
end;


reconOptionsOut = repmat(reconOptions,[1 length(HHFacts) length(tHHFacts) length(tempFacts)]);
for idxOpt = 1:length(reconOptions)
for idx_HH = 1:length(HHFacts)
        for idx_tHH = 1:length(tHHFacts)
          for idx_TempFact = 1:length(tempFacts)
          if doCompute(idx_HH,idx_tHH,idx_TempFact)           
            reconOptionsOut{idxOpt,idx_HH,idx_tHH,idx_TempFact}.HHFact     = HHFacts(idx_HH);
            reconOptionsOut{idxOpt,idx_HH,idx_tHH,idx_TempFact}.TempHHFact = tHHFacts(idx_tHH);
            reconOptionsOut{idxOpt,idx_HH,idx_tHH,idx_TempFact}.tempFactor = tempFacts(idx_TempFact);
            
            %Solution{idx_HH,idx_tHH,idx_TempFact} = EEG_Compute_Solutions(LeadField,DATAIN,reconOptionsTmp);
          end;
          end;
        end;
      end
end;

Solution = get_MultiDataSolve(DATAIN,LeadField,reconOptionsOut);

      % Save out the reconstructions
      idx = 1;
      done = false;
      while ~done
        fname = ['MultiHHSol_Constrained_' num2str(idx) '.mat'];
        if ~exist(fname)
          done = true;
        else
          idx = idx+1;
        end;
      end;
      save(fname,'SolutionsOut','DATAIN');

end