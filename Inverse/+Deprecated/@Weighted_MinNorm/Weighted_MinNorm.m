function [ output ] = Weighted_MinNorm(LeadField)
%LORETA Summary of this function goes here
%   Detailed explanation goes here

numNodes = size(LeadField,2)/3; %prod(SpaceSize);
Omega = zeros(numNodes,1);
for (idxOmega = 1:numNodes)
  idxStart = (idxOmega-1)*3 +1;
  Omega(idxOmega) = sqrt(LeadField(:,idxStart)'*LeadField(:,idxStart) + ...
    LeadField(:,idxStart+1)'*LeadField(:,idxStart+1) + ...
    LeadField(:,idxStart+2)'*LeadField(:,idxStart+2) );
end;

Omega = sparse(1:length(Omega),1:length(Omega),Omega);
Omega = kron(Omega,eye(3));

output.mat = Omega;
output = class(output,'Weighted_MinNorm');

return;