function [Solutions] = EEG_Compute_Solutions(LeadField,EEG,reconOptions)



if isa(reconOptions,'options_LinearReconstruction')
  %% Linear Solution
  
  % Get Appropriate Data Subset and Leadfield Matrix Rows
  if reconOptions.useAverageReference
    mydisp('Using Averaged Reference');
    Data = EEG.data_avgref';
  else
    mydisp('Using Absolute Signal wrt Ground Electrode');
    Data = EEG.data_reref';
  end;
  
  if isempty(Data)
    error('Empty data');
  end;
    
  %Data = Data(EEG.useElec,:);  
  
  % Get the Appropriate leadfield Columns 
  LeadField.currSolutionSpace = reconOptions.solutionSpace;
    
  %LeadField = LeadField.setElectrodeList(reconOptions.useElec);    
  LeadField.useElectrodes = EEG.useElec;
  
  % Convert to a Vector Solution if Necessary
  %LeadField = LeadField.setConstrained(~reconOptions.isVectorSolution);
  LeadField.isCollapsed = ~reconOptions.isVectorSolution;
  
  % Set whether it's a weighted or unweighted leadfield
  LeadField.isWeighted = reconOptions.WeightLeadField;
 
  % Check reconstruction options
  reconOptions = reconOptions.check;  
  
  if isempty(reconOptions.initialGuess)
    reconOptions.initialGuess = zeros(size(LeadField,2),size(Data,2));
  end;
  
  % Reconstruct Using Appropriate Technique
  switch lower(reconOptions.solutionTechnique)
    case 'multiresolution'
      Solutions = getSol_MultiResolution(LeadField,Data,NRRDS,reconOptions);
    
    case 'spatiotemporal'
      Solutions = getSol_SpatioTemporal(LeadField,Data,reconOptions);
      
    case 'loreta'
      Solutions = getSol_LORETA(LeadField,Data,reconOptions);
      
    case 'minnorm'
      Solutions = getSol_WMinNorm(LeadField,Data,reconOptions);
  end
  
  Solutions.Data = Data;
  
else
  %% Nonlinear Solution
  warning('Inversion:NotImplemented', 'WARNING: Nonlinear inversion techniques are not currently supported');
  keyboard;
end


mydisp('Completed Call to EEG_Compute_Solutions');
end


% 
% brainVoxels  = find(nrrds.DownSampledSegmentation.data>=4);   
% brainPoints = find(ismember(solutionPointsSmall,brainVoxels));
%  
% solutionRegions{1}.Voxels = brainVoxels;
% solutionRegions{1}.Points = brainPoints;
% solutionRegions{1}.Index  = [3*(brainPoints-1)+1 3*(brainPoints-1)+2 3*(brainPoints-1)+3]';
% solutionRegions{1}.Desc   = 'All points within intracranial cavity';
% 
% %% Make sure data directories and files are set appropriately.
% dirs.EEGData = 'eeg/';
% files.EEGData = [];
% files.EEGData{1} = 'CZ61-128export.edf';
% 
% %% Identify Voxels within Various Brain Regions, and the associated rows of the leadfield matrix
% idxVoxel.AllSolutionPoints = solutionPointsSmall;
% 
% % Things inside ICC
% idxVoxel.brain          = find(nrrdDownSampledSegmentation.data>=4);   
% solutionPoints.brain    = find(ismember(solutionPointsSmall,brainVoxels));
% 
% % BrainStem Region
% cd([dirs.ProcessedRootDIR dirs.MRI]);
% if exist('StemAndCerebellum.nhdr');
%   nrrdBrainStem = maReadNrrdHeader('StemAndCerebellum.nhdr');
%   cd([dirs.ProcessedRootDIR dirs.SaveMATFiles]);
%   nrrdBrainStem = nrrdDownsample(nrrdBrainStem,options.modelDownSampleLevel * options.inverseDownSampleLevel);
%   idxVoxel.brainstem = find(nrrdBrainStem.data==6); if isempty(stemVoxels), keyboard; end;
% else
%   idxVoxel.brainstem = [];
% end;
% solutionPoints.brainstem = find(ismember(solutionPointsSmall,idxVoxel.brainstem));
% 
% % Identify CSF and White Matter voxels
% idxVoxel.cortex       = find(nrrdDownSampledSegmentation.data==4);
% solutionPoints.cortex = find(ismember(solutionPointsSmall,idxVoxel.cortex));
% 
% idxVoxel.CSF       = find(nrrdDownSampledSegmentation.data==5);
% solutionPoints.CSF = find(ismember(solutionPointsSmall,idxVoxel.CSF));
% 
% idxVoxel.WM       = find(nrrdDownSampledSegmentation.data==7);
% solutionPoints.WM = find(ismember(solutionPointsSmall,idxVoxel.WM));
% 
% 
% %% Build Solution Regions
% 
% solutionRegions{1}.Voxels = idxVoxel.brain;
% solutionRegions{1}.Points = solutionPoints.brain;
% solutionRegions{1}.Desc   = 'All points within intracranial cavity';
% solutionRegions{1}.SpaceSize = sizeImgSmall;
% solutionRegions{1}.dist   = getNRRDAspect(nrrdDownSampledSegmentation);
% 
% %% Set Up Regularizers.
% regularizers{1}.WeightLeadField = false;
% regularizers{1}.SpaceRegType = 'Helmholtz';
% regularizers{1}.useWeighting    = true;
% regularizers{1}.HHfact = 0.025;
% regularizers{1}.TempRegType = 'TemporalHH';
% regularizers{1}.tempfact = 1;
% regularizers{1}.uselambda_t = false;
% regularizers{1}.TempHHFact = 0.05;
% 
% %% Load EEG Data
% 
% 
% % This should really be done by parsing the .BAD file, but I'm lazy right
% % now
% Electrodes.badElec =  read_BadElectrodes('CZ61-128export.bad',dirs);
% Electrodes.useElec = 1:127;
% Electrodes.useElec = setdiff(Electrodes.useElec,Electrodes.badElec);
% 
% if isfield(options,'gndElectrode')
%  Electrodes.useElec = setdiff(Electrodes.useElec,options.gndElectrode);
% end
% 
% 
% cd([dirs.ProcessedRootDIR dirs.EEGData]);
% % Read Data File(s)
% for i = 1:length(files.EEGData);
%   [EEG(i).data EEG(i).header] = sload(files.EEGData{i});
%   EEG(i).data = EEG(i).data'; % Transpose so it's the right orientation
%   if isfield(options,'gndElectrode')
%     refVals = EEG(i).data(options.gndElectrode,:);
%     EEG(i).rereferenced = EEG(i).data - repmat(refVals,[size(EEG(i).data,1) 1]);
%   end;
% end;
% 
% 
% %%
% %[sol_HelmH ] = getSol_Helmholtz( double(matLeadFieldSmall(useElec,:)) ,EEG(1).data(useElec),sizeImgSmall,corticalPts,getNRRDAspect(nrrdDownSampledSegmentation),logspace(-8,3,100));
% %[sol_LORETA ]= getSol_LORETA( double(matLeadFieldSmall(useElec,corticalIndex))*matCortConst ,EEG(1).data(25,useElec),sizeImgSmall,solutionPointsSmall(corticalPts),getNRRDAspect(nrrdDownSampledSegmentation),logspace(-4,7,100));
% 
% 
% %%
% reconOptions.solutionSpace = solutionRegions{1};
% reconOptions.regularizerOptions = regularizers{1};
% reconOptions.niters = 100;
% reconOptions.lambdas =  logspace(-2,2,20);
% reconOptions.useElec = Electrodes.useElec;
% reconOptions.vectorSolution = true;
% reconOptions.constraintMat = [];
% 
% [sol_SpatTemp SpatReg TempReg]= getSol_SpatioTemporal(matLeadFieldSmall,EEG(1).rereferenced(:,1:50),reconOptions);
% 
% %%
% saveSolutionAsNrrd(nrrdDownSampledSegmentation,sol_HelmH{50},finalPts2,'Helmholtz50_Test');
