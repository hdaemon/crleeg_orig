function [out] = ctranspose(in);

if isa(in,'Helmholtz')
  out = in;
  out.mat = out.mat';
end;

return;