classdef Helmholtz
  %
  % function [ output ] = Helmholtz( options, LeadField )
  %
  % Constructor function for a Helmholtz class spatial regularizer object.
  %
  % Required Inputs:
  %
  %  options.solutionSpace.SpaceSize          Size of solution space
  %                       .dist               Size of solution voxels
  %                       .Voxels             Which voxels you're solving at
  %         .regularizerOptions.HHFact        Helmholtz Factor
  %                            .useWeighting  Flag to include weighting of
  %                                             Helmholtz matrix by the norms
  %                                             of the Leadfield columns
  %         .vectorSolution                   Flat indicating if the solution
  %                                             a vector or a scalar at each
  %                                             solution point
  %  LeadField                                The LeadField Matrix
  %
  %  Defined Methods:
  %    ctranspose
  %    mtimes
  %
  %  Written By: Damon Hyde (Last Edited: 11/05/2013)
  %
  %  11/05/2013 - Rewritten for new object oriented coding approach.
  
  properties
    mat = [];
    sizes = [];
    solutionpoints = [];
    differential = 0;
    HHfact = 0;
  end
  
  methods
    function [ output ] = Helmholtz( options, LeadField )
                  
      if nargin == 0;
        output.mat = [];
        output.sizes = [];
        output.solutionpoints = [];
        output.differential = 0;
        output.HHfact = 0;
        output = class(output,'Helmholtz');
      elseif isa(options,'Helmholtz')
        output = options;
      else
        disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%');
        disp('Obtaining a solution using Helmholtz');
        
        % Parse Options
        SpaceSize      = options.solutionSpace.SpaceSize;
        dist           = options.solutionSpace.dist;
        SolutionPoints = options.solutionSpace.Voxels;
        HHFact         = options.regularizerOptions.HHfact;
        useWeighting   = options.regularizerOptions.useWeighting;
        
        % not the right thing to do, but for now, since the voxels are close to
        % cubic.
        dist = mean(dist);
        
        %% Build Laplacian Matrix:
        try
          B = build_HelmholtzMatrix(SpaceSize,SolutionPoints,dist,HHFact,options.vectorSolution);
          
          disp(['Solving for ' num2str(length(SolutionPoints)) ' out of a possible ' num2str(prod(SpaceSize)) ' nodes']);
          
          disp('Computing Variances');
          if size(LeadField,1)==size(B,2)
            LeadField = LeadField';
          end;
          
          if useWeighting
            disp('Using Helmholtz Regularizer WITH Spatial Weighting');            
            OmegaInv = LeadField.matWeighting;
            Omega = 1./OmegaInv; 
            output.mat = B*Omega;
            % wantOmegaInverse = false;
            % WeightLeadField(LeadField,options.vectorSolution,wantOmegaInverse);
          else
            disp('Using Helmholtz Regularizer WITHOUT Spatial Weighting');
            output.mat = B;
          end;
        catch
          keyboard;
        end;
        
        % Construct Class
        output.sizes = SpaceSize;
        output.solutionpoints = SolutionPoints;
        output.differential = 0;
        output.HHfact = HHFact;
      end; % end ELSE
    end % end Function
  end % end Methods
  
end % end ClassDef
