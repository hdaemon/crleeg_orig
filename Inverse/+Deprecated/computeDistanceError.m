function [meanErr, totErr,numVox] = computeDistanceError(recon,reconPts,origsize,distanceMap,multiThresh)

reconPower = sqrt(sum(recon.^2,2));

reconPowerImg = zeros(origsize);
reconPowerImg(reconPts) = reconPower;


%distanceMap = getDistanceMap(orig);

for idxThresh = 1:length(multiThresh)
  try
  thresh = multiThresh(idxThresh);
  
  Qrecon = find(reconPowerImg>thresh*max(reconPowerImg(:)));
  
  meanErr(idxThresh) = mean(distanceMap(Qrecon)); %mean(abs(recon(Qrecon)).*distanceMap(Qrecon));
  totErr(idxThresh) = sum(distanceMap(Qrecon)); %sum(abs(recon(Qrecon)).*distanceMap(Qrecon));
  
%   if meanErr<0.1
%      keyboard;
%   end;
  numVox(idxThresh) = length(Qrecon);
  catch
    keyboard
  end;
end;

meanErr = meanErr(:)';
totErr = totErr(:)';
numVox = numVox(:)';

end