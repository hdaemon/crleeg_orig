function [B] = build_SmoothingMatrix(solSpace,HHFact,vectorSolution)

if ~isa(solSpace,'SolutionSpace')
  error('Must input a SolutionSpace object')
end;

SpaceSize      = solSpace.SpaceSize;
SolutionPoints = solSpace.Voxels;
dist           = solSpace.dist;

dist = sqrt(sum(dist.^2)); % A bit of a hack for the moment.

%% Build Laplacian Matrix:
mydisp('Building Laplacian Matrix');
mydisp(['Solution Space is of Size: ' num2str(SpaceSize(1)) ' x ' num2str(SpaceSize(2)) ' x ' num2str(SpaceSize(3))]);

% Get Indices to All Voxels
[idxX idxY idxZ] = ndgrid(1:SpaceSize(1),1:SpaceSize(2),1:SpaceSize(3));
idxX2 = kron(idxX(:),ones(6,1));
idxY2 = kron(idxY(:),ones(6,1));
idxZ2 = kron(idxZ(:),ones(6,1));

% Convert to Indices of Neighbors
idxX2 = idxX2 + kron(ones(numel(idxX),1),[ 1 -1  0  0  0  0 ]');
idxY2 = idxY2 + kron(ones(numel(idxY),1),[ 0  0  1 -1  0  0 ]');
idxZ2 = idxZ2 + kron(ones(numel(idxZ),1),[ 0  0  0  0  1 -1 ]');

% Trim Those that Fall Outside the Image Volume
Q = find( ( idxX2<1 ) | ( idxX2>SpaceSize(1) ) | ( idxY2<1 ) | ( idxY2>SpaceSize(2) ) | ( idxZ2<1 ) | ( idxZ2>SpaceSize(3) ) );       
idxX2(Q)  = []; idxY2(Q)  = []; idxZ2(Q)  = [];

% Column and Row Indexes Into Sparse matrix
colIdx    = sub2ind(SpaceSize,idxX2,idxY2,idxZ2);

rowIdx = 1:prod(SpaceSize);
rowIdx = kron(rowIdx(:),ones(6,1));
rowIdx(Q) = [];

% Trim To Include Only those points we are solving for
%   This might make the previous step superfluous.
Q = find(~ismember(colIdx,SolutionPoints)|~ismember(rowIdx,SolutionPoints));
colIdx(Q) = [];
rowIdx(Q) = [];

% Build the Sparse Matrix and Cut it Down to Size
% Again - this step might be the only necessary one, as these are currently
% just the off-diagonal components.
 
  A1 = sparse(rowIdx,colIdx,0.25*ones(size(colIdx)),prod(SpaceSize),prod(SpaceSize));
  A1 = A1(SolutionPoints,SolutionPoints);
      
  A0 = A1;
  
  if vectorSolution
    A = kron(A0,speye(3));
    scale = kron(scale,speye(3));
  else
    A = A0;
  end;
  
  B = A + speye(size(A,1));
  
  % Get the Row Sums  
  tmp = B*ones(size(B,2),1);
  %tmp = tmp + 0.1*abs(tmp-6);
  scale = sparse(1:size(B,1),1:size(B,1),1./tmp);
  
  B = scale*B;
  
  
    


return;
