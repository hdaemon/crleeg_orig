function [B] = build_HelmholtzMatrix(solSpace,HHFact,vectorSolution,oldmethod)

if ~exist('oldmethod')
  oldmethod = true;
end;

if ~isa(solSpace,'cnlSolutionSpace')
  error('Must input a cnlSolutionSpace object')
end;

SpaceSize      = solSpace.sizes;
SolutionPoints = solSpace.Voxels;
dist           = solSpace.voxSize;

dist = sqrt(sum(dist.^2)); % A bit of a hack for the moment.

%% Build Laplacian Matrix:
mydisp('Building Laplacian Matrix');
mydisp(['Solution Space is of Size: ' num2str(SpaceSize(1)) ' x ' num2str(SpaceSize(2)) ' x ' num2str(SpaceSize(3))]);

imgGrid = cnlGrid(SpaceSize);
[rowIdx, colIdx] = imgGrid.getGridConnectivity(6);

% Trim To Include Only those points we are solving for
Q = find(~ismember(colIdx,SolutionPoints)|~ismember(rowIdx,SolutionPoints));
colIdx(Q) = [];
rowIdx(Q) = [];

% Build the Sparse Matrix and Cut it Down to Size
% Again - this step might be the only necessary one, as these are currently
% just the off-diagonal components.
if oldmethod
  mydisp('Building Helmholtz Matrix Using Old Method');
  A1 = sparse(rowIdx,colIdx,(1/6)*ones(size(colIdx)),prod(SpaceSize),prod(SpaceSize));  
  A1 = A1(SolutionPoints,SolutionPoints);
  
  % Get the Row Sums
  scale = sparse(1:size(A1,1),1:size(A1,1),1./(A1*ones(size(A1,2),1)));
    
  A0 = 0.5*(speye(size(A1,2))+scale)*A1;
  
  if vectorSolution
    A = kron(A0,speye(3));  
  else
    A = A0;
  end;
  
  B = (6/dist^2)*(A-(1+HHFact^2)*speye(size(A,1)));
    
else  
  mydisp('Building Helmholtz Matrix Using New Method');
  A1 = sparse(rowIdx,colIdx,ones(size(colIdx)),prod(SpaceSize),prod(SpaceSize));
  A1 = A1(SolutionPoints,SolutionPoints);
  
  % Get the Row Sums  
  tmp = A1*ones(size(A1,2),1);
  %tmp = tmp + 0.1*abs(tmp-6);
  scale = sparse(1:size(A1,1),1:size(A1,1),tmp);
    
  A0 = A1;
  
  if vectorSolution
    A = kron(A0,speye(3));
    scale = kron(scale,speye(3));
  else
    A = A0;
  end;
  
  B = A - scale - HHFact^2*speye(size(A,1));
end;

return;
