  

% finalDownSample = [4 4 4];
% NRRDS.DownSampledSegmentation = NRRDS.DownSampledSegmentation.DownSample(finalDownSample);
% 
% final
% 
% LeadField.currSolutionSpace = 

NRRDS.DownSampledSegmentation.DownSample(modelOptions.leadFieldDownSampleLevel);

%% Get Usable Electrode list and Read Data
Electrodes = get_ElectrodeList(dirs,files,Electrodes,modelOptions);
EEG        = read_EEGData(dirs,files,modelOptions);

%% Identify Voxels within Various Brain Regions, and the associated rows of the leadfield matrix
[NRRDS voxelIndices]= get_VoxelIndices(dirs,NRRDS,LeadField);

%% Build Solution Regions
clear solutionRegions
solutionRegions{1} = SolutionSpace(LeadField.currSolutionSpace.SpaceSize,voxelIndices.Cortex,LeadField.currSolutionSpace.dist,'All points in originally segmented cortex');
solutionRegions{2} = SolutionSpace(LeadField.currSolutionSpace.SpaceSize,voxelIndices.connectedCortex2,LeadField.currSolutionSpace.dist,'All sufficiently connected cortical points');
solutionRegions{3} = SolutionSpace(LeadField.currSolutionSpace.SpaceSize,voxelIndices.ICC,LeadField.currSolutionSpace.dist,'All voxels inside the ICC');
solutionRegions{4} = SolutionSpace(LeadField.currSolutionSpace.SpaceSize,voxelIndices.dual,LeadField.currSolutionSpace.dist,'All CSF and Cortical Voxels');

files.NMM = 'ParcellationNMM.nrrd';
if ~isempty(files.NMM)
  NMM = file_NRRD(files.NMM,[dirs.ProcessedRootDIR 'common-processed/modules/parcellationNMM']);
  NRRDS.NMM = parcelateOriginalSegmentation(NRRDS.FullSegmentation,NMM,'nmm','NMMParcel.nhdr');
  NRRDS.NMM.DownSample(modelOptions.inverseDownSampleLevel);
  clear NMM;
end;

files.IBSR = 'ParcellationIBSR.nrrd';
if ~isempty(files.IBSR)
  IBSR = file_NRRD(files.IBSR,[dirs.ProcessedRootDIR 'common-processed/modules/parcellationIBSR']);
  NRRDS.IBSR = parcelateOriginalSegmentation(NRRDS.FullSegmentation,IBSR,'ibsr','IBSRParcel.nhdr');
  NRRDS.IBSR.DownSample(modelOptions.inverseDownSampleLevel);
  clear IBSR;
end;

gridToGrid = SolutionSpace.getMapGridToGrid(NRRDS.NMM.sizes,NRRDS.DownSampledSegmentation.sizes);
Q = find(NRRDS.NMM.data);
tmp = zeros(numel(NRRDS.NMM.data),1); tmp(Q) =1 ;
foo = gridToGrid'*tmp;

solutionRegions{5} = SolutionSpace(LeadField.currSolutionSpace.SpaceSize,intersect(find(foo),voxelIndices.connectedCortex2),LeadField.currSolutionSpace.dist,'All Cortical Voxels From Parcellation');

clear gridToGrid Q tmp foo;