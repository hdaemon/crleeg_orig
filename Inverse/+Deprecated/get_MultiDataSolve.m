function [spikesOut] = get_MultiDataSolve(DATAIN,LeadField,reconOptions)
%
% function [spikesOut] = get_MultiDataSolve(DATAIN,LeadField,reconOptions)
%
% You should make sure you are in the appropriate
% /sourcelocalization/solutions directory before calling this.

rOptSize = size(reconOptions);
reconOptions = reconOptions(:);

for m = 1:length(DATAIN)
  %  [path name ext] = fileparts(files.EEGData{m});
  
%   % And a separate directory for each data set
%   name = DATAIN(m).dirName;
%   
%   if ~exist(name,'dir')
%     mkdir(name);
%   end;
%   cd(name);

 
  for i = 1:numel(reconOptions)
    Solution{i} = EEG_Compute_Solutions(LeadField,DATAIN(m),reconOptions{i});
  end;
  
  Solution = reshape(Solution,rOptSize);
  
  spikesOut{m}.Data   = DATAIN(m);
  spikesOut{m}.Recons = Solution;    
  
%  cd ..
end

end