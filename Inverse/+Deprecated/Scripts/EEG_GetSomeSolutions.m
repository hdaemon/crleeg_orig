%% Set Up Regularizers.
cd([dirs.ProcessedRootDIR 'sourceanalysis/solutions']);
clear reconOptions;
reconOptions{1} = options_LinearReconstruction;
reconOptions{1}.isVectorSolution = true;
reconOptions{1}.solutionTechnique = 'LORETA';
reconOptions{1}.SpaceRegType = 'LORETA';
reconOptions{1}.niters = 600;
reconOptions{1}.useElec = Electrodes.useElec;
reconOptions{1}.oldWeighting = true;
reconOptions{1}.TempRegType = 'none';
reconOptions{1}.lambdas = logspace(-3,3,100);
reconOptions{1}.useAverageReference = false;
reconOptions{1}.solutionSpace = solutionRegions{5};
reconOptions{1}.weightRegularizer =  false;
reconOptions{1}.isVectorSolution = false; 

reconOptions{2} = options_LinearReconstruction;
reconOptions{2}.solutionTechnique = 'spatiotemporal';
reconOptions{2}.WeightLeadField = false; % Weight the LeadField?
reconOptions{2}.weightRegularizer = true; % Weight the Regularizer?
reconOptions{2}.isVectorSolution = false; % Obtain a vector solution?
reconOptions{2}.useElec = Electrodes.useElec; % Which Electrodes to Use
reconOptions{2}.solutionSpace = solutionRegions{5};
reconOptions{2}.niters = 600;
reconOptions{2}.lambdas =  logspace(-2,2,20); 
reconOptions{2}.tol = 1e-4;
reconOptions{2}.useAverageReference = false;
reconOptions{2}.SpaceRegType = 'helmholtz';
reconOptions{2}.HHFact = 0;
reconOptions{2}.TempHHFact = 0.25;
reconOptions{2}.corrHelmholtz = false;
reconOptions{2}.oldWeighting = true;
reconOptions{2}.TempRegType = 'temporalhh';
reconOptions{2}.tempFactor = 50;
reconOptions{2}.useDependantLambdaT = true;

 reconOptions{3} = reconOptions{1};
reconOptions{3}.weightRegularizer = true;
% 
% reconOptions{4} = reconOptions{2};
% reconOptions{4}.HHFact = 0.1;
% reconOptions{4}.tempFactor = 500;
% 
% reconOptions{5} = reconOptions{2};
% reconOptions{5}.HHFact = 0.1;
% reconOptions{5}.weightRegularizer = false;
% reconOptions{5}.tempFactor = 50;
% 
% reconOptions{6} = reconOptions{2};
% reconOptions{6}.HHFact = 0.1;
% reconOptions{6}.weightRegularizer = false;
% reconOptions{6}.tempFactor = 100; 
% 
% reconOptions{7} = reconOptions{2};
% reconOptions{7}.HHFact = 0.1;
% reconOptions{7}.weightRegularizer = false;
% reconOptions{7}.tempFactor = 500;


% % We have a separate directory for each model
% if ~exist(modelOptions.OutputFileName,'dir');
%   mkdir(modelOptions.OutputFileName);
% end;
% cd(modelOptions.OutputFileName);

%   tmp = reconOptions([1:5]); %[2 3 4 7 8 9]);
%   clear reconOptions;
%   reconOptions = tmp;

DATAIN = EEG(1:5);
 
doRecon = 2; 

tmp = get_MultiDataSolve(DATAIN,LeadField,reconOptions(doRecon),NRRDS);
for i = 1:length(tmp)
 Spikes{i}.Solution(doRecon) = tmp{i}.Solution;
end;
%%
saveOptions{1} = options_Save;
saveOptions{1}.doLambda = 0;% 1:2:20;
saveOptions{1}.doTimePoints = [1:2:256]; %[14 16 18 20];
saveOptions{1}.doSolution = 2;
saveOptions{1}.fname = 'SpatTemp';
saveOptions{1}.errTarget = 0.01;
saveOptions{1}.isRGB = true;

saveOptions{2} = saveOptions{1};
saveOptions{2}.doTimePoints = [1:2:11];


save_MultiDataSolutions(DATAIN,NRRDS.DownSampledSegmentation,Spikes,saveOptions);

