


%% Set Up Regularizers.
%cd([dirs.ProcessedRootDIR 'sourceanalysis/solutions']);
clear reconOptions;
reconOptions{1,1,1} = options_LinearReconstruction;
reconOptions{1,1,1}.WeightLeadField = false;
reconOptions{1,1,1}.SpaceRegType = 'helmholtz';
reconOptions{1,1,1}.weightRegularizer    = true;
reconOptions{1,1,1}.HHFact = 0.25;
reconOptions{1,1,1}.TempRegType = 'datacorr';
reconOptions{1,1,1}.tempFactor = 0.1;
reconOptions{1,1,1}.useDependantLambdaT = true;
reconOptions{1,1,1}.TempHHFact = 0.05;
reconOptions{1,1,1}.solutionSpace = solutionRegions{2};
reconOptions{1,1,1}.niters = 600;
reconOptions{1,1,1}.lambdas =  logspace(-2,2,20); 
reconOptions{1,1,1}.useElec = Electrodes.useElec;
reconOptions{1,1,1}.isVectorSolution = false;
reconOptions{1,1,1}.corrHelmholtz = false;
reconOptions{1,1,1}.oldWeighting = true;
reconOptions{1,1,1}.tol = 1e-3;
reconOptions{1,1,1}.modelName = modelOptions.OutputFileName;
reconOptions{1,1,1}.useAverageReference = true;

HHFacts   = linspace(0,1,5); %[0 logspace(-2,0,3)];
tHHFacts  = 0; % linspace(0,1,5); %[0 logspace(-2,0,3)];
TempFacts = [100 500 1000 ];
 

%reconOptions{1,1,1}.lambdas = reconOptions{1,1,1}.lambdas(8:12);
%reconOptions{2,1,1}.lambdas = reconOptions{2,1,1}.lambdas(8:12);

doCompute = ones(length(HHFacts),length(tHHFacts),length(TempFacts));

% doCompute(1,1,1) = 1;
% doCompute(3,3,1) = 1;

% % We have a separate directory for each model
% if ~exist(modelOptions.OutputFileName,'dir');
%   mkdir(modelOptions.OutputFileName);
% end;
% cd(modelOptions.OutputFileName);

clear Spikes;

clear solutionMultiHH solutionLORETA reconOptionsLORETA

for m = 1:length(DATAIN)
  %  [path name ext] = fileparts(files.EEGData{m});
  
  % And a separate directory for each data set
  name = DATAIN(m).dirName;
  
%   if ~exist(name,'dir')
%     mkdir(name);
%   end;
%   cd(name);
    
  solutionMultiHH = EEG_MultiHelmholtzSolve(reconOptions,LeadField,DATAIN(m),HHFacts,tHHFacts,TempFacts,doCompute);
   
  reconOptionsLORETA{1} = reconOptions{1,1,1};
  reconOptionsLORETA{1}.oldWeighting = true;
  reconOptionsLORETA{1}.TempRegType = '';
  reconOptionsLORETA{1}.SpaceRegType = 'LORETA';
  reconOptionsLORETA{1}.TempRegType = 'none';
    
  solutionLORETA{1} = EEG_Compute_Solutions(LeadField,DATAIN(m),reconOptionsLORETA{1});
  
 %  reconOptionsLORETA{2} = reconOptionsLORETA{1}; 
 %  reconOptionsLORETA{2}.solutionTechnique = 'LORETA';
 
 %  solutionLORETA{2} = EEG_Compute_Solutions(LeadField,DATAIN(m),reconOptionsLORETA{2});
    
done = false;
idx = 1;
fnamebase = getSolutionName(solutionLORETA{1}.options);
while ~done
  fname = ['LORETASol_' fnamebase '_' num2str(idx) '.mat'];
  if ~exist(fname)
    done = true;
  else
    idx = idx+1;
  end;
end;
solutionLORETA{1}.filename = fname;
save(fname,'solutionLORETA');

  
  Spikes{m}.solutionMultiHH    = solutionMultiHH;  
  Spikes{m}.Solutions_LORETA         = solutionLORETA;
  
cd ..
end