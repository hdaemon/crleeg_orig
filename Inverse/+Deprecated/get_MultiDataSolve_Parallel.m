function [spikesOut] = get_MultiDataSolve_Parallel(DATAIN,LeadField,reconOptions,NRRDS)
%
% function [spikesOut] = get_MultiDataSolve(DATAIN,LeadField,reconOptions)
%
% You should make sure you are in the appropriate
% /sourcelocalization/solutions directory before calling this.

rOptSize = size(reconOptions);
reconOptions = reconOptions(:);

for m = 1:length(DATAIN)
  %  [path name ext] = fileparts(files.EEGData{m});
  
  % And a separate directory for each data set
  name = DATAIN(m).dirName;
  
  if ~exist(name,'dir')
    mkdir(name);
  end;
  cd(name);
    
   if matlabpool('size')==0
      
      [foo systemname] = system('hostname');
                 
      if ismember(systemname,{'io' 'ganymede' 'europa'})
        matlabpool open local 8
      else
       matlabpool open local 8;
      end;
  end;
  
 parfor i = 1:length(reconOptions)
    Solution{i} = EEG_Compute_Solutions(LeadField,DATAIN(m),reconOptions{i},NRRDS);
 end;
  
  Solution = reshape(Solution,rOptSize);
  
  spikesOut{m}.Data     = DATAIN(m);
  spikesOut{m}.Solution = Solution;    
  
  cd ..
end

end