function [distMap,centers] = getDistanceMap(orig)

Regions = unique(orig(:));
Regions(1) = []; % Eliminate  background

x = 1:size(orig,1);
y = 1:size(orig,2);
z = 1:size(orig,3);

[X Y Z] = ndgrid(x,y,z);

%% Get Info About Each Region
centerX = zeros(length(Regions),1);
centerY = zeros(length(Regions),1);
centerZ = zeros(length(Regions),1);

for idxRegion = 1:length(Regions)
  % Get Indices
  Q{idxRegion} = find(orig==Regions(idxRegion));
  
  % Get Center
  centerX(idxRegion) = mean(X(Q{idxRegion}));
  centerY(idxRegion) = mean(Y(Q{idxRegion}));
  centerZ(idxRegion) = mean(Z(Q{idxRegion}));
  
  % Get Locations
  regionX{idxRegion} = X(Q{idxRegion});
  regionY{idxRegion} = Y(Q{idxRegion});
  regionZ{idxRegion} = Z(Q{idxRegion});
  
end;

% %% Get distances to each region
% errors = zeros([length(Regions) size(Regions(1)));
% for idxRegion = 1:length(Regions);
%   imgMax = max(orig(:)):
% end;


%% Do it the brute force way
distMap = zeros([length(Regions) size(orig)]);

for idxX = 1:size(orig,1)
  for idxY = 1:size(orig,2)
    for idxZ = 1:size(orig,3)
     % if orig(idxX,idxY,idxZ)>0
      for idxRegion = 1:length(Regions)
         xDist = x(idxX) - regionX{idxRegion};
         yDist = y(idxY) - regionY{idxRegion};
         zDist = z(idxZ) - regionZ{idxRegion};
         
         Dist = sqrt(xDist.^2 + yDist.^2 + zDist.^2);
         Dist = min(Dist);
         distMap(idxRegion,idxX,idxY,idxZ) = Dist;
      end;
    %  end;
    end
  end
end


distMap = squeeze(min(distMap,[],1));

centers = [centerX(:) centerY(:) centerZ(:)];

end