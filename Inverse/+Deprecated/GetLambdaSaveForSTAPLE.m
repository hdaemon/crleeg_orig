clear all;
%%

SolNum = 2;
S1 = load(['MultiHHSol_Constrained_' num2str(SolNum)]);
S2 = load(['MultiHHSol_ConstrainedSPM_' num2str(SolNum)]);

fname = 'ConstSol';
timepoint = 24;

load([S1.modelUsed.dir S1.modelUsed.file]);

dirname = ['TryStaple_' num2str(SolNum)];
if ~exist(dirname,'dir')
  mkdir(dirname);
end
cd(dirname);

%%
for i = 1:numel(S1.Solutions_Constrained)
 CurrError = S1.Solutions_Constrained{i}.LCurve(:,1);
 delta = abs(CurrError-0.1);
 
 Q = find(delta==min(delta));
 UseLambda(i) = Q;

end;

saveSolutionsForSTAPLE(S1.Solutions_Constrained,fname,NRRDS.DownSampledSegmentation,timepoint,UseLambda);
useLambdaConstrained = UseLambda;

%%
for i = 1:numel(S2.Solutions_ConstrainedSPM)
 CurrError = S2.Solutions_ConstrainedSPM{i}.LCurve(:,1);
 delta = abs(CurrError-0.1);
 
 Q = find(delta==min(delta));
 UseLambda(i) = Q;

end;

saveSolutionsForSTAPLE(S2.Solutions_ConstrainedSPM,[fname '_SPM'],NRRDS.DownSampledSegmentation,timepoint,UseLambda);
useLambdaConstrainedSPM = UseLambda;





