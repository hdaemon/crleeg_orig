function [ output ] = LORETA( options, LeadField )
%LORETA Summary of this function goes here
%   Detailed explanation goes here

if nargin == 0;
  output.mat = [];
  output.sizes = [];
  output.solutionpoints = [];
  output.differential = 0;
  output = class(output,'LORETA');
elseif isa(options,'LORETA')
  output = options;
else
  mydisp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%');
  mydisp('Using LORETA for Spatial Regularization');
  
  mydisp(['Solving for ' num2str(length(SolutionPoints)) ' out of a possible ' num2str(prod(SpaceSize)) ' nodes']);
  
  SpaceSize      = options.solutionSpace.SpaceSize;
  dist           = options.solutionSpace.dist;
  SolutionPoints = options.solutionSpace.Voxels;
  
  % not the right thing to do, but for now, since the voxels are close to
  % cubic.
  dist = mean(dist);

  %% Build Laplacian Matrix:  
  B = build_HelmholtzMatrix(SpaceSize,SolutionPoints,dist,0);

  mydisp('Computing Variances');
  if size(LeadField,1)==size(B,2)
    LeadField = LeadField';
  end;
  wantOmegaInverse = false;
  Omega = WeightLeadField(LeadField,options.vectorSolution,wantOmegaInverse);
  
  % Build output structure.
  output.mat = B*Omega;
  output.sizes = SpaceSize;
  output.solutionpoints = SolutionPoints;
  output.differential = 0;
  
  output = class(output,'LORETA');

end;

return;



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5
%%%%  OLD CODE BELOW
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%   
%   mydisp('Building Laplacian Matrix');
%   mydisp(['Solution Space is of Size: ' num2str(SpaceSize(1)) ' x ' num2str(SpaceSize(2)) ' x ' num2str(SpaceSize(3))]);
% 
%   
%   % Get Indices to All Voxels
%   [idxX idxY idxZ] = ndgrid(1:SpaceSize(1),1:SpaceSize(2),1:SpaceSize(3));
%   idxX2 = kron(idxX(:),ones(6,1));
%   idxY2 = kron(idxY(:),ones(6,1));
%   idxZ2 = kron(idxZ(:),ones(6,1));
% 
%   % Convert to Indices of Neighbors
%   idxX2 = idxX2 + kron(ones(numel(idxX),1),[ 1 -1  0  0  0  0 ]');
%   idxY2 = idxY2 + kron(ones(numel(idxY),1),[ 0  0  1 -1  0  0 ]');
%   idxZ2 = idxZ2 + kron(ones(numel(idxZ),1),[ 0  0  0  0  1 -1 ]');
% 
%   % Trim Those that Fall Outside the Image Volume
%   Q = find( ( idxX2<1 ) | ( idxX2>SpaceSize(1) ) | ( idxY2<1 ) | ( idxY2>SpaceSize(2) ) | ( idxZ2<1 ) | ( idxZ2>SpaceSize(3) ) );       
%   idxX2(Q)  = []; idxY2(Q)  = []; idxZ2(Q)  = [];
% 
%   % Column and Row Indexes Into Sparse matrix
%   colIdx    = sub2ind(SpaceSize,idxX2,idxY2,idxZ2);
% 
%   rowIdx = 1:prod(SpaceSize);
%   rowIdx = kron(rowIdx(:),ones(6,1));
%   rowIdx(Q) = [];
% 
%   % Trim To Include Only those points we are solving for
%   %   This might make the previous step superfluous.
%   Q = find(~ismember(colIdx,SolutionPoints)|~ismember(rowIdx,SolutionPoints));
%   colIdx(Q) = [];
%   rowIdx(Q) = [];
% 
%   % Build the Sparse Matrix and Cut it Down to Size
%   % Again - this step might be the only necessary one, as these are currently
%   % just the off-diagonal components.
%   A1 = sparse(rowIdx,colIdx,(1/6)*ones(size(colIdx)),prod(SpaceSize),prod(SpaceSize));
%   A1 = A1(SolutionPoints,SolutionPoints);       
% 
%   scale = sparse(1:size(A1,1),1:size(A1,1),1./(A1*ones(size(A1,2),1)));
% 
%   A0 = 0.5*(speye(size(A1,2))+scale)*A1;
% 
%   if options.vectorSolution
%    A = kron(A0,speye(3));
%   else
%     A = A0;
%   end;
%
%  B = (6/dist^2)*(A-speye(size(A,1)));


 
%     if options.vectorSolution
%     numNodes = size(LeadField,2)/3; %prod(SpaceSize);
%     Omega = zeros(numNodes,1);
%     for (idxOmega = 1:numNodes)
%       idxStart = (idxOmega-1)*3 +1;
%       Omega(idxOmega) = sqrt(LeadField(:,idxStart)'*LeadField(:,idxStart) + ...
%         LeadField(:,idxStart+1)'*LeadField(:,idxStart+1) + ...
%         LeadField(:,idxStart+2)'*LeadField(:,idxStart+2) );
%     end;
%   else
%     numNodes = size(LeadField,2);
%     Omega = zeros(numNodes,1);
%     for (idxOmega = 1:numNodes)
%       Omega(idxOmega) = norm(LeadField(:,idxOmega));
%     end;
%   end;
% 
%   Omega = sparse(1:length(Omega),1:length(Omega),Omega);
%   if options.vectorSolution
%   Omega = kron(Omega,eye(3)); 
%   end;
