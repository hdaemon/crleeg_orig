function [out] = ctranspose(in);

if isa(in,'LORETA')
  out = in;
  out.mat = out.mat';
end;

return;