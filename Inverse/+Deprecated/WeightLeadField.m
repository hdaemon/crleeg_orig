function [OmegaOut] = WeightLeadField(LeadField,isVectorized, wantInverse)
%
% function [OmegaOut] = WeightLeadField(LeadField,isVectorized, wantInverse)
%
% Obtains a diagonal weighting matrix based on either the 2-norm of the
% columns of the leadfield, or the inverse of that matrix.
%
%

error('Shouldn''t use WeightLeadField.  Get the weighting matrix directly from the leadfield instead');

try
  
  if isVectorized
    % If we're doing a vector solution, obtain a single norm for each voxel
    numNodes = size(LeadField,2)/3; %prod(SpaceSize);
    Omega = zeros(numNodes,1);
    
    for (idxOmega = 1:numNodes)
      idxStart = (idxOmega-1)*3 +1;
      Omega(idxOmega) = sqrt(LeadField(:,idxStart)'*LeadField(:,idxStart) + ...
        LeadField(:,idxStart+1)'*LeadField(:,idxStart+1) + ...
        LeadField(:,idxStart+2)'*LeadField(:,idxStart+2) );
    end;
  
    Omega = kron(Omega,ones(3,1));
  else
    
    numNodes = size(LeadField,2);
    Omega = zeros(numNodes,1);
    for (idxOmega = 1:numNodes)
      Omega(idxOmega) = norm(LeadField(:,idxOmega));
    end;
  end;
  
  OmegaInv = 1./Omega;
  OmegaInv(Omega==0) = 0;
  
  if wantInverse
    OmegaOut = sparse(1:length(Omega),1:length(Omega),OmegaInv);
  else
    OmegaOut = sparse(1:length(Omega),1:length(Omega),Omega);
  end;
  
    
catch
  disp('Error in Generating LeadField Matrix Weights');
  keyboard
end;

return;