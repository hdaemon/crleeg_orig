function [a] = ctranspose(a)


try
  a.Space = a.Space';
catch
  error('Error Transposing Spatial Regularizer');
end;

try
  a.Temp = a.Temp';
catch
 error('Error Transposing Temporal Regularizer');
end;

return;