classdef SpatioTemporalRegularizer
  % function obj = SpatioTemporalRegularizer(SpaceReg,TempReg)
  %
  %
  
  properties
    Space
    Temp
    lambda = 0;
    lambda_t = 0;
  end
  
  methods
    
    function obj = SpatioTemporalRegularizer(SpaceReg,TempReg)
      if exist('SpaceReg')&&isa(SpaceReg,'SpatioTemporalRegularizer')
        obj = SpaceReg;
      else
        if exist('SpaceReg')
          obj.Space = SpaceReg;
        end
        if exist('TempReg')
          obj.Temp = TempReg;
        end
      end
    end;
    
    function [valOut] = mtimes(a,b)
      
      if ~isa(a,'SpatioTemporalRegularizer')
        error('Only Right Multiplication Currently Supported for General Spatiotemporal regularization');
      end;
      
      if ~isnumeric(b)
        error('Right hand side must be numeric');
      end;
      
      % Multiply by spatial regularizer
      try
        if ~isempty(a.Space)
          SpaceOut = a.Space*b;
          SpaceOut = a.lambda*SpaceOut;
        else
          SpaceOut = [];
        end;
      catch
        warning('Something is going wrong with multiplication by the spatial regularization matrix');
        keyboard;
        SpaceOut = [];
      end;
      
      % Multiply by temporal regularizer
      try
        if ~isempty(a.Temp)
          TempOut = (a.Temp*b')';
          TempOut = a.lambda_t*TempOut;
        else
          TempOut = [];
        end;
      catch
        warning('Something is going wrong with multiplication by the temporal regularization matrix');
        keyboard;
        TempOut = [];
      end;
      
      map = a.Space.solSpace.matGridToSolSpace(:,a.Space.solSpace.Voxels);
      
      % This is a bit of a hack at the moment, put in here to deal with
      % whether things are transposed or not.  This will need to be updated
      % soon.
      if size(map,1)==size(SpaceOut,1)
        TempOut = map*TempOut;
      else              
      TempOut = map'*TempOut;    
      end;
      
      % Get total product
      if ~isempty(SpaceOut)
        valOut = SpaceOut;
        if ~isempty(TempOut)
          valOut = valOut + TempOut;
        end;
      else
        valOut = TempOut;
      end;
      
      valOut = double(valOut);
      
      if isempty(valOut)
        keyboard;
        error('Returning empty set from nonempty multiplication!!');
      end;
      
    end;
       
    function [a] = ctranspose(a)
      
      
      try
        a.Space = a.Space';
      catch
        error('Error Transposing Spatial Regularizer');
      end;
      
      try
        a.Temp = a.Temp';
      catch
        error('Error Transposing Temporal Regularizer');
      end;
      
    end;
    
  end  %% End Methods Section
  
  
  
end


