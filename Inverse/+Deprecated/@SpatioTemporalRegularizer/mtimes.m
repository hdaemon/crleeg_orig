function [valOut] = mtimes(a,b)

if ~isa(a,'SpatioTemporal')
  error('Only Right Multiplication Currently Supported for General Spatiotemporal regularization');
end;

if ~isnumeric(b)
  error('Right hand side must be numeric');
end;

% Multiply by spatial regularizer
try
  if ~isempty(a.Space)
    SpaceOut = a.Space*b;
    SpaceOut = a.lambda*SpaceOut;
  else
    SpaceOut = [];
  end;
catch
  SpaceOut = [];
end;

% Multiply by temporal regularizer
try
  if ~isempty(a.Temp)
    TempOut = a.Temp*b;
    TempOut = a.lambda_t*TempOut;
  else
    TempOut = [];
  end;
catch
  TempOut = [];
end;

% Get total product
if ~isempty(SpaceOut)
  valOut = SpaceOut;
  if ~isempty(TempOut)
    valOut = valOut + TempOut;
  end;
else
  valOut = TempOut;
end;

valOut = double(valOut);

if isempty(valOut)
  keyboard;
  error('Returning empty set from nonempty multiplication!!');
end;

return;