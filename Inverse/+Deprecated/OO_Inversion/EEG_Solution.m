classdef EEG_Solution
  
  properties
    Data
    Model
    Method
  end;
  
  methods
    function obj = EEG_Solution(c)
      if ~exist('c'), c = []; end;
      if ~isa(c,'EEG_Solution')
        obj.LeadFieldHandle = [];
        obj.DataHandle      = [];
        obj.SpaceSubset     = [];
        obj.DataSubset      = [];
        obj.SolutionMethod  = [];
      else
        obj.SpaceSubset    = c.SpaceSubset;
        obj.DataSubset     = c.SpaceSubset;
        obj.SolutionMethod = c.SolutionMethod;
      end;
    end
    
    function out = test(obj,A)
      out = obj.SpaceSubset+A;
    end
    
  end
  
end