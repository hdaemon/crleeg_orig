function [ valOut ] = mtimes(a,b)
%MTIMES Summary of this function goes here
%   Detailed explanation goes here

if ~isa(a,'MinNorm')
  error('Only right multiplication supported for LORETA regularizers')
end;

if ~isnumeric(b)
  error('right multiplication object must be numeric');
end;

try
 valOut = (double(b));
catch
  keyboard;
end;

return