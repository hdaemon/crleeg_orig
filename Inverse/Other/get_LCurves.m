function [LCurve, LCurve_t] = get_LCurves(Solution,Data,LeadField,Regularizer)
% function [LCurve LCurve_t] = get_LCurves(Solution,Data,LeadField,Regularizer)
%
% Compute LCurves for all timepoint simultaneously as well as each
% individual timepoint.

%mydisp('Computing L-Curves');

  % Compute Error Norm
  simData = LeadField*Solution;
  dataError = simData - Data;
  
  % Compute Regularization Norm
  regTerm = Solution'*((Regularizer'*Regularizer)*Solution);
  regNorm = trace(regTerm);
  
  % Build the Unified LCurve
  LCurve(1) = norm(dataError(:))/norm(Data(:));
  LCurve(2) = regNorm;
  
   % Build the Individual TimePoint LCurve
  %LCurve_t(1,:) = sqrt(sum(dataError.^2,1)./sum(Data.^2,1));
  %LCurve_t(2,:) = sqrt(sum(regTerm.^2,1));
  LCurve_t = 0;
  
end