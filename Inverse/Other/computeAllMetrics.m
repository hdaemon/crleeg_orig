 function Norms = computeAllMetrics(currImgRecon,currImgOrig,LeadField)
% function Norms = computeAllMetrics(currImgRecon,currImgOrig)
%
% currImgRecon should be the output of running get_MultiDataSolve
%
% currImgOrig should be a structure, with two fields:
%    currImgOrig.fullImage -> The noisy image used to generate data
%    currImgOrig.trueImage -> The true image used to generate data

% Thresholds to use for Dice coefficient/etc
allThresh = 0:0.05:1;

%% Initial Processing of true images

% Put the image into the full grid
simImg{1} = zeros(currImgOrig.solSpace.sizes);
simImg{1}(currImgOrig.solSpace.Voxels) = currImgOrig.fullImage;
simImg{2} = zeros(currImgOrig.solSpace.sizes);
simImg{2}(currImgOrig.solSpace.Voxels) = currImgOrig.trueImage;

% Set Leadfield solution space, rebase, and get matCollapse
origTransMat = LeadField.matCollapse;
LeadField.currSolutionSpace = currImgOrig.solSpace;
LeadField = LeadField.rebaseOnCurrent;
transMat = LeadField.matCollapse;

% Their vector counterparts
simImgVec{1} = zeros([3 currImgOrig.solSpace.sizes]);
simImgVec{1}(:,currImgOrig.solSpace.Voxels) = ...
  reshape(transMat*currImgOrig.fullImage,[3 currImgOrig.solSpace.nVoxels]);
simImgVec{2} = zeros([3 currImgOrig.solSpace.sizes]);
simImgVec{2}(:,currImgOrig.solSpace.Voxels) = ...
  reshape(transMat*currImgOrig.trueImage,[3 currImgOrig.solSpace.nVoxels]);

%% Compute metrics against each solution image

% Loop across Reconstruction Types
for idxSol = 1:numel(currImgRecon.Recons)
  
   solSpace = currImgRecon.Recons{idxSol}.options.solutionSpace;   
   gridMap = getMapGridToGrid(solSpace,currImgOrig.solSpace);
   gridMap = gridMap'*solSpace.matGridToSolSpace';
   voxInSolImg = find(sum(gridMap,2));
   
  % Loop across Regularization Values
  for idxLam = 1:size(currImgRecon.Recons{idxSol}.solutions,1)
    % Loop across Temporal Regularization Scales
    for idxLamT = 1:size(currImgRecon.Recons{idxSol}.solutions,2);
      
      % Get the image and shift it to the same grid as the image to be
      % compared against
      Img = (currImgRecon.Recons{idxSol}.solutions{idxLam,idxLamT});                 
      Img = gridMap*Img(:);                  
      
      % Get joint set of voxels to compare across.
      finalVoxSet = union(currImgOrig.solSpace.Voxels,voxInSolImg);
      
      % Get the constrained and unconstrained versions of the image
      if currImgRecon.Recons{idxSol}.options.isVectorSolution
        ConstImg = reshape(Img,[3 length(Img)/3])';
        ConstImg = sqrt(sum(ConstImg.^2,2));
        VecImg = Img;
      else
        ConstImg = Img;
        VecImg = zeros([3 currImgOrig.solSpace.sizes]);
        tmp = transMat*ConstImg(currImgOrig.solSpace.Voxels,:);        
        VecImg(:,currImgOrig.solSpace.Voxels) = ...
          reshape(tmp,[3 currImgOrig.solSpace.nVoxels]);
      end;      

      for isTrueImageNoisy = 0:1
        for doVectorComparison = 0:1
          
          % Choose the appropriate solution image and comparison
          % image
          if doVectorComparison, tmpImg = simImgVec;  solImg = VecImg;
          else                   tmpImg = simImg;     solImg = ConstImg;
          end
          
          % Select the appropriate "true" image (With or without image
          % noise)
          if isTrueImageNoisy,  trueImg = tmpImg{1};
          else                  trueImg = tmpImg{2};
          end
          
          
          Norms{idxSol,idxLam,idxLamT,isTrueImageNoisy+1,doVectorComparison+1}...
            = computeNorms(trueImg(finalVoxSet),solImg(finalVoxSet),allThresh);
          
        end %vecOrNot
      end %noisyOrNot
    end %idxLam
  end % idxLamT
end %idxSol

 end

 
