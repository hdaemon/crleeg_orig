function normsOut = computeNorms(img1,img2,allThresh)
% function normsOut = computeNorms(img1,img2,allThresh)
%
% Given two images, img1 and img2, compute the 2-norm difference, the RDM
% and MAG, and the Dice, Jaccard, and Tversky Indexes for all threshold
% levels in allThresh.


% Get the dice coefficient, jaccard index, and two versions of tversky
% There's only one version of this, and it's the same for
% both vector and constrained images
for idxThresh = 1:length(allThresh)
  thresh = allThresh(idxThresh);
  Q1 = find(abs(img2)>=thresh*max(abs(img2(:))));
  Q2 = find(abs(img1)>=thresh*max(abs(img1(:))));
  
  img1 = img1(union(Q1,Q2));
  img2 = img2(union(Q1,Q2));
  
  
  Corr(idxThresh) = corr(img1(:),img2(:));
  absCorr(idxThresh) = corr(abs(img1(:)),abs(img2(:)));
  
  TwoNorm(idxThresh) = norm(img1-img2)/norm(img2);
  
  RDM(idxThresh) = metrics.RDM(img1,img2);
  absRDM(idxThresh) = metrics.RDM(abs(img1),abs(img2));
  MAG(idxThresh) = metrics.MAG(img1,img2);  
  
  Dice(idxThresh) = metrics.dice(Q1,Q2);
  Jaccard(idxThresh) = metrics.jaccard(Q1,Q2);
  TverskyA(idxThresh) = metrics.tversky(Q1,Q2,1,0);
  TverskyB(idxThresh) = metrics.tversky(Q1,Q2,0,1);
   
end;

normsOut.TwoNorm = TwoNorm;
normsOut.RDM = RDM;
normsOut.MAG = MAG;
normsOut.Corr = Corr;
normsOut.Dice = Dice;
normsOut.Jaccard = Jaccard;
normsOut.TverskyA = TverskyA;
normsOut.TverskyB = TverskyB;
normsOut.absCorr = absCorr;
normsOut.absRDM  = absRDM;


end