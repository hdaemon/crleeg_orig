function plotLCurve(LCurve,fignum)

%  function plotLCurve(LCurve,fignum)
%
%  Given an Nx2 matrix LCurve, plots loglog(LCurve(:,1),LCurve(:,2)), and
%  indicates the end and midpoints with red X's. 
%
%  If fignum is defined, clears that figure and displays the plot there.

if exist('fignum')
  figure(fignum);
  clf;
else
  figure;
end;

if length(size(LCurve))==2
  disp('This curve');
loglog(LCurve(:,1),LCurve(:,2),'x');
hold on;
loglog(LCurve(end,1),LCurve(end,2),'rx');

l = size(LCurve,1);
halfway = round(l/2);
loglog(LCurve(halfway,1),LCurve(halfway,2),'rx');
hold off;

else
  disp('That Curve');
  for i = 1:size(LCurve,2)
  loglog(LCurve(:,i,1),LCurve(:,i,2),'x');
  %loglog(LCurve(:,i,1),LCurve(:,i,2));
hold on;
loglog(LCurve(end,i,1),LCurve(end,i,2),'rx');

l = size(LCurve,1);
halfway = round(l/2);
loglog(LCurve(halfway,i,1),LCurve(halfway,i,2),'rx');

  
  end
hold off;
end;

% subplot(1,2,2);
% title('Set Two');
% loglog(LCurve(:,3),LCurve(:,4),'x');
% hold on;
% loglog(LCurve(end,3),LCurve(end,4),'rx');
% 
% l = size(LCurve,1);
% halfway = round(l/2);
% loglog(LCurve(halfway,3),LCurve(halfway,4),'rx');
% hold off;

end