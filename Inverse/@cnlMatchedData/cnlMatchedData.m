classdef cnlMatchedData < handle & matlab.mixin.Copyable
  % classdef cnlMatchedData < handle
  %
  % Object class for taking raw input data with associated electrode
  % labels, and matching it to a different set of electrode labels.  This
  % simply has the effect of appropriately reordering the indicies of the
  % electrodes so that they match the provided label list.  Data associated
  % with labels which are present only in the input file are ignored, as
  % are output labels which do not match any input data label.
  %
  % obj = cnlMatchedData(datafile,elecLabels);
  %
  % Properties:
  %   datafile      : Object or structure with data in the .data field and
  %                     (optionally) electrode labels in the .labels field.
  %   elecLabels    : List of labels to match the labels from obj.datafile
  %                     to. If not provided, defaults to all electrodes in
  %                     the datafile.
  %
  % Dependent Properties:
  %   currLabels    : List of labels associated with obj.data_parsed
  %   data_parsed   : Input data with columns reordered to match elecLabels
  %
  % Written By: Damon Hyde
  % Last Modified: March 22, 2015
  % Part of the cnlEEG project.
  %
  
  properties
    datafile
    elecLabels
  end
  
  properties (Dependent = true)
    currLabels
    data_parsed
  end
  
  properties (Dependent = true, Hidden = true)
    dataLabels  % Labels associated with obj.datafile.
    idxIntoFile % Index from obj.data_* into obj.datafile.data
    idxIntoElec % Index from obj.data_* into obj.elecLabels
  end
  
  methods
    
    function obj = cnlMatchedData(datafile,elecLabels)
      % function obj = cnlMatchedData(datafile,elecLabels)
      %
      % Object constructor
      if nargin>0
        % Set the datafile
        obj.datafile = datafile;
        
        % If electrode labels are provided, use them.  Otherwise just use
        % all labels available in the data.
        if exist('elecLabels','var'), obj.elecLabels = elecLabels;
        else                          obj.elecLabels = obj.dataLabels;
        end;
      end
    end
    
    %% Method for setting datafile
    function set.datafile(obj,val)
      % function set.datafile(obj,val)
      %
      % Set method for cnlMatchedData.datafile.
      %

      if isempty(val)||(isa(val,'file_EDF'))||...
          (isfield(val,'data'))
        % Can pass in a file_EDF, a structure with .data and .label fields,
        % or an empty array.
        obj.datafile = val;
      else
        obj.datafile.data = val;
      end;
      
      % Set Default Labels if they're missing.
      if ~isempty(obj.datafile)
      if ~isfield(obj.datafile,'labels')&&~isprop(obj.datafile,'labels')
        warning('Using default electrode labels!!!');
        for i = 1:size(obj.datafile.data,2)
          obj.datafile.labels{i} = ['E' num2str(i)];
        end
      end      
      
      obj.parseData(val);
      end;
    end
    
    function out = get.dataLabels(obj)
      % function out = get.dataLabels(obj)
      %
      % If obj.datafile is defined, return the list of electrode labels
      % from it.  Otherwise return an empty set.
      out = [];
      if ~isempty(obj.datafile),
        out = cnlMatchedData.parse_Labels(obj.datafile.labels);
      end;
    end
    
    function set.elecLabels(obj,val)
      % function set.elecLabels(obj,val)
      %
      % Set the 
      disp('Setting Electrode Labels');
      if ~isempty(val)
        if isa(val,'cnlElectrodes')
          obj.elecLabels = cnlMatchedData.parse_Labels(val.Labels);
        else
          obj.elecLabels = cnlMatchedData.parse_Labels(val);
        end;
      end
    end;
    
    function out = get.currLabels(obj)
      % function out = get.currLabels(obj)
      %
      %
      
      % If we don't have data labels, just return an empty set.
      if isempty(obj.dataLabels), out = []; return; end;
      
      % If the electrodes object isn't defined, just return the labels
      % associated with the original data
      if isempty(obj.elecLabels),
        out = obj.dataLabels;
        if length(out)>size(obj.datafile.data,2)
          out = out(1:size(obj.datafile.data,2));
        end;
        return;
      end;
      dLab   = obj.dataLabels;
      eLab   = obj.elecLabels;
      
      % Current list should be all electrode labels that also show up in
      % the data.
      out = intersect(eLab,dLab,'stable');
    end
    
    %% Methods to return the index into the datafile and the electrode list from the model.
    function out = get.idxIntoFile(obj)
      % function out = get.idxIntoFile(obj)
      %
      % Returns the numeric index into obj.datafile.data for each of the
      % labels currently in obj.currLabels.
      dataLabels = obj.dataLabels;
      currLabels = obj.currLabels;
      out = zeros(1,length(currLabels));
      for idxElec = 1:length(currLabels)
        % Find index into original labels that matches the model electrode
        idxData = find(strcmp(currLabels{idxElec},dataLabels));
        if ~numel(idxData)==1, error('WTF'); end; %Random error checking
        out(idxElec) = idxData;
      end
    end
    
    function out = get.idxIntoElec(obj)
      % function out = get.idxIntoElec(obj)
      %
      % For each electrode in the data file, find the index of that
      % matching entry in obj.elecLabels (which comes from the model)
      %
      
      elecLabels = obj.elecLabels; %#ok<PROP>
      currLabels = obj.currLabels;
      out = zeros(1,length(currLabels));
      if ~isempty(elecLabels)
        for idxElec = 1:length(currLabels)
          % Find index into original labels that matches the model electrode
          idxData = find(strcmp(currLabels{idxElec},elecLabels));
          %if ~(numel(idxData)<=1), error('WTF'); end; %Random error checking
          if ~isempty(idxData)
            out(idxElec) = idxData;
          end;
        end
      else
        out = 1:length(currLabels);
      end;
    end
    
    
    %% Set and get methods for data_parsed
    function set.data_parsed(obj,val)
      % function set.data_parsed(obj,val)
      %
      % Manually set cnlMatchedData.data_parsed.  The second dimension
      % needs to be the same size as obj.currLabels
      
      % Check that number of electrodes in new data matches current labels
      if size(val,2)~=numel(obj.currLabels)
        error('cnlMatchedData:sizeMismatch',...
          'Second dimension of new data_parsed must match size of obj.currLabels');
      end;
      
      tmpDataFile.labels = obj.currLabels;
      tmpDataFile.data = val;
      
      obj.datafile = tmpDataFile;
    end
    
    function out = get.data_parsed(obj)
      % function out = get.data_parsed(obj)
      %
      % If obj.datafile.data is an array, this returns
      % out = obj.datafile.data(:,obj.idxIntoFile).
      %
      % If obj.datafile.data is a cell array, this returns
      % out{idx} = obj.datafile.data{idx}(:,obj.idxIntoFile)
      if ~isempty(obj.datafile)
        if ~iscell(obj.datafile.data)
          out = obj.datafile.data(:,obj.idxIntoFile);
        else
          out = cell(size(obj.datafile.data));
          for idx = 1:numel(obj.datafile.data)
            out{idx} = obj.datafile.data{idx}(:,obj.idxIntoFile);
          end
        end
      else
        warning('cnlMatchedData:emptyData','obj.datafile is empty');
        out = [];
      end;
    end
    
  end
  
  %% Protected Methods
  methods (Access=protected)
    function parseData(obj,val)
      % function setdata(obj,val)
      %
      % This is just a dummy function so that obj.set.datafile has
      % something to call
      
      if ~isnumeric(obj.datafile.data)
        error('cnlMatchedData Requires that the data object be numeric');
      end;
      
    end
  end
  
  %% Static Methods
  methods (Static=true)
    out = parse_Labels(labelsIn);
  end
  
end