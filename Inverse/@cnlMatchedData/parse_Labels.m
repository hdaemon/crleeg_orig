function labelsOut = parse_Labels(labelsIn)
%  function labelsOut = parse_Labels(labelsIn)
%
% Give a cell array of strings containing electrode names, parses them
% appropriately so that:
%
%   For a 128 lead EGI system, 

for idxLabel = 1:length(labelsIn)
  currLabel = labelsIn{idxLabel};
  
  % Strip off leading info
  if length(currLabel)>=4
    if strcmp(currLabel(1:4),'EEG ')
      currLabel = currLabel(5:end);
    elseif strcmp(currLabel(1:2),'E ')
      currLabel = currLabel(3:end);
    elseif strcmp(currLabel(1:2),'P ')
      currLabel = currLabel(3:end);
    elseif strcmp(currLabel(1:4),'POL ')
      currLabel = currLabel(5:end);
    end;
  end;
  
  % Strip tailing info
  if length(currLabel)>=4
    if strcmp(currLabel(end-3:end),'-Ref')
      currLabel = currLabel(1:end-4);
    elseif strcmp(currLabel(end-3:end),'-COM');
      currLabel = currLabel(1:end-4);
    elseif strcmp(currLabel(end-2:end),'-Cz');
      currLabel = currLabel(1:end-3);
    end;
  end;
  
  labelsOut{idxLabel} = currLabel;
  
end;

end