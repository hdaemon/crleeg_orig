classdef cnlEEGDataMatrixDecomp < cnlEEGData_Clone
%
%
% Requires these properties of cnlEEGData
%    data_epoched
%    epochs_start
%    epochs_end
%
    properties
        leftMatrix
        rightMatrix
        otherDecompVars

        matrixDecomp='ARLTI';
        decompInputs={'delaysamples',40, ...
                      'systemrank',50, ...
                      'randomsamples',1000};
    end
    
    properties (Dependent=true)
      targetlength
    end;

    methods
        function obj = cnlEEGDataMatrixDecomp(file,varargin)
            if(nargin > 0)
                arglist{1}=file;
                arglist(2:(numel(varargin)+1))=varargin(:);
            else
                arglist=cell(0,0);
            end
            obj = obj@cnlEEGData_Clone(arglist{:});
%             tmp = obj.pipeline(4:end);            
%             obj.pipeline{4} = ...
%               cnlPLMod.EEG.epochs.splitIntoEvenly(obj.pipeline{3},50,'divide');                        
%             obj.pipeline(5:end+1) = tmp;
%             obj.pipeline{5}.inputobj = obj.pipeline{4};
%             obj.pipeline{6}.inputobj = obj.pipeline{4};
        end

        function set.targetlength(obj,val)
          obj.pipeline{4}.targetLength = val;
        end;
        
        function out = get.targetlength(obj)
          out = obj.pipeline{4}.targetLength;
        end;
        
        function runMatrixDecomp(obj)
            switch(obj.matrixDecomp)
                case 'ARLTI'
                [obj.otherDecompVars.stateEvolutionMatrix, ...
                 obj.leftMatrix, ...
                 obj.otherDecompVars.initialStates, ...
                 obj.otherDecompVars.fullstatematrices]= ...
                        identifyARLTISystemFromSegments( ...
                        cellfun(@transpose,obj.data_epoched,'un',0), ...
                        obj.decompInputs{:});
                obj.rightMatrix = cat(2,obj.otherDecompVars.fullstatematrices{:});
            end
        end

        function out = dataApprox(obj)
            out = (obj.leftMatrix*obj.rightMatrix).';
        end

        function out = dataEpochApprox(obj)
            out = mat2cell(obj.dataApprox,obj.epochs_end-obj.epochs_start+1,size(obj.data,2));
        end

    end

end
