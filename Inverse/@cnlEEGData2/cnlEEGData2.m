classdef cnlEEGData2 < handle & matlab.mixin.Copyable
  
  properties
    % Input Data
    dataSource
    
    % Requested Output
    requestedElecList       
    outputType = 'avgref';
    outputRef  = 'avgref';                    
    outputBadElecs
  end
  
  properties (Dependent = true)            
    %% Data and labels after processing and possiblerereferencing
    data
    outputData
    useElec % List of electrodes in the 
  end
  
  properties (SetAccess=private, Hidden = true)
    inputData; % Input data parsed into a cnlLabelledData structure
  end
  
  properties (Hidden = true, Dependent = true)
    outputList
  end
  
  properties (Hidden = true,Constant=true)      
    VALID_OUTPUT_TYPES = {'recorded' 'reref' 'avgref'};
  end
  
  methods
    
    function obj = cnlEEGData2(data,varargin)
      
      if nargin>0
      p = inputParser;
      p.addRequired('data',@(x) obj.validateData(x));
      p.addOptional('labels',[],@(x) iscellstr(x));
      p.addParamValue('elec',[]);
      p.addParamValue('model',[],@(x) isa(x,'cnlModel'));
      p.addParamValue('badlabels',[],@(x) iscellstr(x));
      p.addParamValue('eleclabels',[],@(x) iscellstr(x));
      p.addParamValue('usetimes',[]);
      p.addParamValue('gndElec','avgref');
      
      parse(p,data,varargin{:});
           
      obj.inputData = cnlLabelledData(p.Results.data,p.Results.labels);      
      
      obj.outputBadElecs = p.Results.badlabels;
      assert(isempty(p.Results.model)||isempty(p.Results.eleclabels),...
        'You can set either ''model'' or ''eleclabels'', but not both');
      if ~isempty(p.Results.model)
        obj.requestedElecList = p.Results.model.Electrodes.Labels;
        obj.outputRef      = p.Results.model.gndElectrode;
      else
        obj.requestedElecList   = p.Results.eleclabels;
        obj.outputRef = p.Results.gndElec;
      end            
      
      end;
    end        
    
    function out = get.outputList(obj)
      out = cnlMatchedElectrodeList(obj.inputData.labels,obj.requestedElecList,...
                                      [obj.outputBadElecs obj.outputRef]);
    end;
    
    
    
    %% requestedElecList
    function set.requestedElecList(obj,val)
      assert(isempty(val)||iscellstr(val),'Electrode list needs to be a cell array of strings');
      obj.requestedElecList = val;
    end;
    
    function out = get.requestedElecList(obj)
      % function out = get.requestedElecList(obj)
      %
      % If requestedElecList hasn't been set, return the list of input
      % labels.
      if isempty(obj.requestedElecList)
        if isfield(obj.inputData,'labels')||isprop(obj.inputData,'labels');
          out = obj.inputData.labels;
        else
          out = [];
        end;
      else
        out = obj.requestedElecList;
      end
    end;

    %% outputRef
    function set.outputRef(obj,val)
      % function set.outputRef(obj,val)
      %
      % Output reference needs to be referred to with a string.
      if ischar(val)
        obj.outputRef = val;
      else
        assert(false,'Output reference needs to be a string');
      end;
    end
    
    function out = get.referenceData(obj)
      % function out = get.referenceData(obj)
      %
      % Return a time varying reference signal for all electrodes to use.
      switch(obj.outputType)
        case 'avgref'
          out = mean(obj.matchedData.data,2);
        case 'recorded'
          out = zeros(size(obj.matchedData.data,1),1);
        case 'reref'
          tmp = obj.matchedData(obj.outputRef);
          out = tmp.data;          
      end
    end
    

            
%     %% Methods for Setting input Data and Labels
%     function set.inputData(obj,val)
%       assert(isfield(val,'data')||isprop(val,'data'),...
%         'inputData must have a field or property called ''outputData'' ');
%       
%       obj.inputData = val;
%                   
%       % Set the input outputLabels in obj.matchedList, after checking that it's
%       % been properly configured
%       assert(isa(obj.matchedList,'cnlMatchedElectrodeList'),...
%           'Setting outputData before object has been properly configured');
%       if isfield(val,'labels')||isprop(val,'labels')        
%         obj.matchedList.Input = val.labels;
%       else
%         warning('Using default electrode outputLabels');
%         elecList = cell(1,size(val.data,2));
%         for i = 1:size(val.data,2)
%           elecList{i} = ['E' num2str(i)];
%         end
%         obj.matchedList.Input = elecList;
%       end;
%             
%     end
%         
%     
%     %% Methods to get the matched 
%     function out = get.matchedData(obj)
%       out = obj.inputData(:,obj.matchedList.idxInputToOutput);
%     end
%     
%     function out = get.matchedLabels(obj)
%       out = obj.matchedList.Output;
%     end
%     
%     function out = get.elecLabels(obj)
%       out = obj.matchedList.Match;
%     end;
%     
%     function set.elecLabels(obj,val)
%       assert(isa(obj.matchedList,'cnlMatchedElectrodeList'),...
%         'obj.matchedList has not been properly configured yet');
%       obj.matchedList.Match = val;
%     end
%     
%     function out = get.badLabels(obj)
%       out = obj.matchedList.Exclude;
%     end
%     
%     function set.badLabels(obj,val)
%       assert(isa(obj.matchedList,'cnlMatchedElectrodeList'),...
%         'obj.matchedList has not been properly configured yet');      
%       obj.matchedList.Exclude = val;
%     end;
%     
%     function out = get.idxInputToMatched(obj)
%       out = obj.matchedList.idxInputToOutput;
%     end;
%     
%     function out = get.idxMatchToMatched(obj)
%       out = obj.matchedList.idxMatchToOutput;
%     end;
%     
%     function out = get.idxRefElecInOutput(obj)
%       out = [];
%       if ~isempty(obj.refElectrode)
%         if isnumeric(obj.refElectrode)
%           out = find(strcmpi(obj.elecLabels{obj.refElectrode},obj.outputLabels));
%         else
%           out = find(strcmpi(obj.refElectrode,obj.outputLabels));
%         end
%       end
%     end
    
  end
  
  methods (Static=true)
    function isValid = validateData(val)
      warning('Not actually validating anything here');
      isValid = true;
    end
  end
  
end
