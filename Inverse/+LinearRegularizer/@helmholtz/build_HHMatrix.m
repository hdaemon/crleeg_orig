function [matOut] = build_HHMatrix(solSpace,HHFact)
% function [matOut] = build(obj)
%
% Matrix build function for the cnlReg_Helmholtz regularizer class
%
% Constructs a 
%
% Written By: Damon Hyde
% Last Edited: Nov 16, 2015
% Part of the cnlEEG Project
%

SpaceSize      = solSpace.sizes;

%% Build Laplacian Matrix:
mydisp('Building Laplacian Matrix');
mydisp(['Solution Space is of Size: ' num2str(SpaceSize(1)) ' x ' num2str(SpaceSize(2)) ' x ' num2str(SpaceSize(3))]);

imgGrid = cnlGrid(SpaceSize);
[rowIdx, colIdx] = imgGrid.getGridConnectivity(6);

A1 = sparse(rowIdx,colIdx,ones(size(colIdx)),prod(SpaceSize),prod(SpaceSize));

%Eliminate connections to voxels outside the current solution space.
allVox    = 1:size(A1,1);
voxInSol  = solSpace.Voxels;
voxOutSol = setdiff(allVox,voxInSol);

A1(voxOutSol,:) = 0;
A1(:,voxOutSol) = 0;

% Get row sums
tmp = A1*ones(size(A1,2),1);
tmp(tmp==0) = -1;
scale = sparse(1:size(A1,1),1:size(A1,1),tmp);

% Make a vector matrix, if necessary
%A = kron(A1,speye(colPerVox));
A = A1;
%scale = kron(scale,speye(colPerVox));

% Build Helmholtz Matrix
%matOut = A - scale - HHFact^2*speye(size(A,1)) - A;
matOut = (scale - A) + HHFact^2*speye(size(A,1)) ;

% Project matrix into starting solution space.
tmpMat = solSpace.matGridToSolSpace;
%tmpMat = kron(tmpMat,speye(colPerVox));
matOut = tmpMat*(matOut*tmpMat');

end

