classdef helmholtz < cnlMatrixOnSpace
  % Helmholtz Function Based Spatial Regularization 
  %
  % classdef cnlLinReg_Helmholtz < cnlReg_LinearSpace
  %
  % Implements a Helmholtz-type spatial regularizer based on the
  % cnlReg_LinearSpace class.
  %
  % Written By: Damon Hyde
  % Last Edited: Jan 21, 2016
  % Part of the cnlEEG Project
  %
  
  properties
    HHFactor = 0;
  end
  
  methods
    
    function obj = helmholtz(solSpace,varargin)
      
      obj = obj@cnlMatrixOnSpace;
      
      if nargin>0
        if isa(solSpace,'cnlReg_Helmholtz')
          obj = solSpace;
          return;
        end
        
        assert(isa(solSpace,'cnlSolutionSpace'),...
          'Must define a cnlSolutionSpace to contruct a helmholtz regularizer');
        
        mydisp('Constructing a linear Helmholtz regularizer');
        p = inputParser;
        p.addParamValue('HHFact',0,@(x) isscalar(x));
        p.addParamValue('isVec',false,@(x) islogical(x));
        p.parse(varargin{:});
        
        obj.HHFactor = p.Results.HHFact;
                        
        if p.Results.isVec, nCol = 3; else nCol = 1; end;
        obj.colPerVox = nCol;
        
        % Build the original matrix on the underlying regular grid.
        solGrid = cnlGridSpace(solSpace);
        origSolSpace = cnlSolutionSpace(solGrid,solSpace.Voxels);
        
        obj.origSolutionSpace = origSolSpace;
        obj.origMatrix = obj.build_HHMatrix(origSolSpace,obj.HHFactor);
        if obj.colPerVox>1
          obj.origMatrix = kron(obj.origMatrix,speye(obj.colPerVox));
        end;
                
        % Project to desired Solution Space
        obj.currSolutionSpace = solSpace;                                
      end
    end
    
  end
  
  methods (Static=true)  
    matOut = build_HHMatrix(solSpace,HHFact);    
  end
  
end