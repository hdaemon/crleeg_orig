classdef cnlReg_Helmholtz < cnlReg_LinearSpace
  
  % classdef cnlLinReg_Helmholtz < cnlReg_LinearSpace
  %
  % Implements a Helmholtz-type spatial regularizer based on the
  % cnlReg_LinearSpace class.
  %
  % Written By: Damon Hyde
  % Last Edited: Jan 21, 2016
  % Part of the cnlEEG Project
  %
  
  properties
    HHFactor = 0;
  end
  
  methods
    
    function obj = cnlReg_Helmholtz(options,leadField)
      
      obj = obj@cnlReg_LinearSpace;
      
      if nargin>0
        if isa(options,'cnlReg_Helmholtz')
          obj = options;
          return;
        end
        
        if ~isfield(options,'weightRegularizer');
          options.weightRegularizer = false;
        end;
        
        
        
        mydisp('Constructing a linear Helmholtz regularizer');
        obj = obj.setup(options,leadField);
        
      end
    end
    
  end
  
  methods (Access=protected)
    function obj = setup(obj,options,leadField)
      obj.HHFactor = options.HHFact;
      
      % Set spatial weighting if desired
      
      if options.weightRegularizer;
        obj.matRightWeight = leadField.getWeightingMatrix(false);
        obj.isRightWeighted = true;
      end;
      
      % Build Helmholtz matrix
      if options.isVectorSolution, obj.colPerVox = 3; end;
      solGrid = cnlGridSpace(leadField.currSolutionSpace);
      solSpace = cnlSolutionSpace(solGrid,leadField.currSolutionSpace.Voxels);      
      obj.origSolutionSpace = solSpace;
      obj.origMatrix = obj.build;
      
      % Set current solution space and rebuild matrix
      obj.currSolutionSpace = leadField.currSolutionSpace;
    end
    
    matOut = build(obj);
    
  end
  
end