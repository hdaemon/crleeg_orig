function [matOut] = build(obj)
% function [matOut] = build(obj)
%
% Matrix build function for the cnlReg_Helmholtz regularizer class
%
% Constructs a 
%
% Written By: Damon Hyde
% Last Edited: Nov 16, 2015
% Part of the cnlEEG Project
%

SpaceSize      = obj.origSolutionSpace.sizes;

%% Build Laplacian Matrix:
mydisp('Building Laplacian Matrix');
mydisp(['Solution Space is of Size: ' num2str(SpaceSize(1)) ' x ' num2str(SpaceSize(2)) ' x ' num2str(SpaceSize(3))]);

imgGrid = cnlGrid(SpaceSize);
[rowIdx, colIdx] = imgGrid.getGridConnectivity(6);

A1 = sparse(rowIdx,colIdx,ones(size(colIdx)),prod(SpaceSize),prod(SpaceSize));

%Eliminate connections to voxels outside the current solution space.
allVox    = 1:size(A1,1);
voxInSol  = obj.origSolutionSpace.Voxels;
voxOutSol = setdiff(allVox,voxInSol);

A1(voxOutSol,:) = 0;
A1(:,voxOutSol) = 0;

% Get row sums
tmp = A1*ones(size(A1,2),1);
%tmp(tmp==0) = -1;
scale = sparse(1:size(A1,1),1:size(A1,1),tmp);

% Build Helmholtz Matrix
%matOut = A - scale - obj.HHFactor^2*speye(size(A,1)) - A;

matOut = scale - A1;
tmp2 = 1./tmp; tmp2(isnan(tmp2)) = 0; tmp2(isinf(tmp2)) = 0;
scale2 = sparse(1:size(matOut,1),1:size(matOut,1),tmp2);

matOut = scale2*matOut;

matOut = matOut + obj.HHFactor^2*speye(size(matOut,1));

%matOut = scale*matOut;

%matOut = (scale - A) + obj.HHFactor^2*speye(size(A,1)) ;

% Project matrix into starting solution space.
tmpMat = obj.origSolutionSpace.matGridToSolSpace;
%tmpMat = kron(tmpMat,speye(obj.colPerVox));
%matOut = tmpMat*(matOut*tmpMat');
matOut = matOut*tmpMat';

% Make a vector matrix, if necessary
matOut = kron(matOut,speye(obj.colPerVox));


end

