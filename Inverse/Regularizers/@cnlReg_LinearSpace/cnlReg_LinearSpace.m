classdef cnlReg_LinearSpace < cnlMatrixOnSpace

% classDef cnlReg_LinearSpace
%
% Properties:
%   Inherited:  origMatrix     < cnlMatrixOnSpace < cnlAdaptableMatrix
%               currMatrix     < cnlMatrixOnSpace < cnlAdaptableMatrix
%               isTransposed   < cnlMatrixOnSpace < cnlAdaptableMatrix
%               needsRebuild   < cnlMatrixOnSpace < cnlAdaptableMatrix
%               origSolutionSpace < cnlMatrixOnSpace
%               currSolutionSpace < cnlMatrixOnSpace
%               colPerVox         < cnlMatrixOnSpace
%               isCollapsed       < cnlMatrixOnSpace
%               matCollapse       < cnlMatrixOnSpace
%   New:  isRightWeighted
%         isLeftWeighted
%         matLeftWeight
%         matRightWeight
  
  properties                    
    isRightWeighted = false;
    isLeftWeighted  = false;
    matLeftWeight  = [];
    matRightWeight = [];
  end
  
  properties (Hidden = true)
    AtAPrecompute
  end
  
  methods
    function obj = cnlReg_LinearSpace(matrix,solSpace)
      obj = obj@cnlMatrixOnSpace;
      if nargin>0
        obj.origMatrix = matrix;
        obj.origSolutionSpace = solSpace;
        obj.currSolutionSpace = solSpace;
      end
    end;
            
    function out = multByAtA(obj,a)
      % function out = multByAtA(obj,a)
      %
      % Because the regularization matrix will often be constructed in the
      % full MRI image space, and then clustered to a lower dimensional
      % space, here we precompute the matrix inner product to vastly
      % accelerate computations later on.
      %
      
      if isempty(obj.AtAPrecompute)
        out = obj.AtAPrecompute * a;
      else
        out = obj.currMatrix'*(obj.currMatrix*a);
      end
    end
        
    function obj = set.isRightWeighted(obj,val)
      obj.isRightWeighted = val;
      obj = obj.rebuildCurrMatrix;
    end;
    
    function obj = set.isLeftWeighted(obj,val)
      obj.isLeftWeighted = val;
      obj = obj.rebuildCurrMatrix;
    end;
    
    function obj = set.matLeftWeight(obj,val)
      obj.matLeftWeight = val;
      obj = obj.rebuildCurrMatrix;
    end;
    
    function obj = set.matRightWeight(obj,val)
      obj.matRightWeight = val;
      obj = obj.rebuildCurrMatrix;
    end;
        
    function out = canRebuild(obj)      
      tmp1 = canRebuild@cnlMatrixOnSpace(obj);
      tmp2 = obj.isRightWeighted && isempty(obj.matRightWeight);
      tmp3 = obj.isLeftWeighted &&  isempty(obj.matLeftWeight);
      
      if ~tmp1||tmp2||tmp3
        out = false;
        return
      end
      
      out = true;      
    end
    
    function obj = rebuildCurrMatrix(obj)
      % function obj = rebuildCurrMatrix(obj)
      %
      % Rebuild the current linear regularization matrix.  First rebuilds
      % the matrix using the rebuildCurrMatrix@cnlMatrixOnSpace, and then
      % applies left and right weighting matrices (if defined).  Finally,
      % precomputes AtA if it will save space.
      
      mydisp('Starting Rebuild');
               
      obj = obj.rebuildCurrMatrix@cnlMatrixOnSpace;
      
      if ~isempty(obj.currMatrix)
      if obj.isRightWeighted
        obj.currMatrix = obj.currMatrix*obj.matRightWeight;
      end
      
      if obj.isLeftWeighted
        obj.currMatrix = obj.matLeftWeight*obj.currMatrix;
      end;
            
%       if size(obj.currMatrix,2)<0.5*size(obj.currMatrix,1)
%         mydisp('Precomputing AtA for speed');
%         obj.AtAPrecompute = obj.currMatrix'*obj.currMatrix;
%       else
%         obj.AtAPrecompute = [];
%       end
      end;
    end        
    
    function S = saveobj(obj)
      S = saveobj@cnlMatrixOnSpace(obj);
      S.isRightWeighted = obj.isRightWeighted;
      S.isLeftWeighted  = obj.isLeftWeighted;
      S.matLeftWeight  = obj.matLeftWeight;
      S.matRightWeight = obj.matRightWeight;
    end
    
    function obj = loadFromStruct(obj,S)
      obj = loadFromStruct@cnlMatrixOnSpace(obj,S);
      if isfield(S,'isRightWeighted')
      obj.isRightWeighted = S.isRightWeighted; end;
      if isfield(S,'isLeftWeighted')
      obj.isLeftWeighted = S.isLeftWeighted; end;
    if isfield(S,'matLeftWeight')
      
      obj.matLeftWeight = S.matLeftWeight; end
    if isfield(S,'matRightWeight')
      obj.matRightWeight = S.matRightWeight;      end;
      
    end
  
  end
  
  methods (Static=true)
    
    function obj = loadobj(S)
      obj = cnlReg_LinearSpace;
      obj.disableRebuild = true;
      obj = loadFromStruct(obj,S);
      obj.disableRebuild = false;
      obj = obj.rebuildCurrMatrix;
    end
    
  end
  
end