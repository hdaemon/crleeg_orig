classdef cnlReg_LORETA < cnlReg_Helmholtz
  
% classdef cnlReg_LORETA < cnlReg_Helmholtz
%
% function obj = cnlReg_LORETA(options,leadField);
%
% Inputs:
%   options   : 
%   leadField : cnlLeadField object
%
% Outputs:
%   obj : cnlReg_LORETA object
%
% Implements a LORETA spatial regularizer based on the
% cnlReg_Helmholtz class.  
%
% Written By: Damon Hyde
% Last Edited: Jan 21, 2016
% Part of the cnlEEG Project
%
      
  methods
    
    function obj = cnlReg_LORETA(options,leadField)
   
      obj = obj@cnlReg_Helmholtz;
    
      if nargin>0
        if isa(options,'cnlReg_LORETA')
          obj = options;
          return;
        end
        
        mydisp('Constructing a linear LORETA regularizer');        
        options.HHFact = 0;
        
        obj = obj.setup(options,leadField);

      end
    end        
  end        
  
  methods (Access=protected)
    function obj = setup(obj,options,leadfield)
      obj = obj.setup@cnlReg_Helmholtz(options,leadfield);
      
      %%obj.origMatrix = obj.origMatrix + 0.01*speye(size(obj.origMatrix));
      
    end
  end
  
end