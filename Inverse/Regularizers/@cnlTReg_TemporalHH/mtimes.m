function [ valOut ] = mtimes( HH, b )
%MTIME Summary of this function goes here
%   Detailed explanation goes here

if ~isa(HH,'TemporalHH')
  error('Only right multiplication supported for temporal helmholtz operators')
end;

if ~isnumeric(b)
  error('right multiplication object must be numeric');
end;

valOut = (HH.mat*b')';

return