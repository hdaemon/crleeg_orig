function TempReg = get_TemporalRegularizer(LeadField,Data,ReconOpts)


mydisp(['Computing Temporal Regularizer']);
switch lower(ReconOpts.TempRegType)
  case 'temporalhh'
    TempReg = cnlTReg_TemporalHH(ReconOpts.TempHHFact, Data,true);
  case 'datacorr'
    TempReg = cnlTReg_DataCorr(Data);
  otherwise
    TempReg = [];
end


%Set norm of the matrix to 1
RegNorm = svds(TempReg.mat,1);
TempReg.mat = TempReg.mat./RegNorm;

end