classdef cnlReg_CorticalGraph < cnlReg_LinearSpace
  % CNLREG_CORTICALGRAPH Spatially varying smoothness regularizer
  %
  % classdef cnlReg_CorticalGraph < cnlReg_LinearSpace
  %
  % Written By: Damon Hyde
  % Last Edited: Dec 30, 2015
  % Part of the cnlEEG project
  %
  
  properties
    type = 'hybrid';
  end
  
  methods
    
    function obj = cnlReg_CorticalGraph(options,leadfield)
      obj = obj@cnlReg_LinearSpace;
      
      if nargin>0
        if isa(options,'cnlReg_CorticalGraph')
          obj = options;
          return;
        end
        
        obj = obj.setup(options,leadfield);
      end
            
    end
    
    function obj = setup(obj,options,leadField)
      % SETUP - Construct cortical graph regularizer
      %
      % function obj = setup(obj,options,leadField)
      %
      %
            
      if options.isVectorSolution, obj.colPerVox = 3; end;            
      
      nrrdVec     = leadField.vecNRRD;
      outSolSpace = leadField.currSolutionSpace;
      voxLField   = leadField.currSolutionSpace.Voxels;
      tmpSolSpace = cnlSolutionSpace(nrrdVec.gridSpace,voxLField);
      leadField.disableRebuild = true;
      leadField.isCollapsed = true;     
      leadField.currSolutionSpace = tmpSolSpace;
      leadField.disableRebuild = false;
      leadField = leadField.rebuildCurrMatrix;
            
      graph = cnlImageGraph(nrrdVec.data,3,'voxList',voxLField,...
                            'distType',obj.type,'nbrType','none',...
                            'Leadfield',leadField.currMatrix, ...
                            'connectivity',6);
                                            
      solGrid = cnlGridSpace(leadField.currSolutionSpace);
      solSpace = cnlSolutionSpace(solGrid,voxLField);      
      obj.origSolutionSpace = solSpace;
      
      % Regularize using the graph Laplacian, 
      %lap = graph.Laplacian;
      %deg = diag(lap);
      %lap = lap-diag(deg);
      
      %deg(deg==0) = max(deg);
      %deg = graph.degree;
      deg = sum(abs(graph.Cmatrix),2);
      deg(deg==0) = max(deg);
      
      deg = sparse(diag(deg));
                              
      obj.origMatrix = deg - graph.Cmatrix;
    %  obj.origMatrix = obj.origMatrix - 0.01*speye(size(obj.origMatrix,1));
      
      obj.currSolutionSpace = outSolSpace;
    end
        
  end
  
end