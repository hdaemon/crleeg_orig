classdef SpatialBlockReg
  
  properties
   Nv
   Nt
   blockRefs
   LeftBlocks
   RightBlocks
  end
  
  methods
    
    function obj = SpatialBlockReg(Nv,Nt,blockRefs,LeftBlocks,RightBlocks)
      if isa(blockRefs,'SpatialBlockReg')
        obj = blockRefs;
      else
%         if length(blockRefs)~=length(Blocks)
%           error('Must have same number of blocks and references');
%         elseif length(unique(blockRefs))~=length(blockRefs)
%           keyboard;
%           error('All block references must be unique');
%         elseif (min(blockRefs)<(-Nt+1))||(max(blockRefs)>(Nt-1))
%           error('Block references outside available range');
%         end;
%         
%         checkSize = size(Blocks{1});
%         for i = 2:length(Blocks)
%           if ~all(size(Blocks{i})==checkSize)
%             error('All blocks must be the same size');
%           end;
%         end;
               
        obj.Nv = Nv;
        obj.Nt = Nt;
        obj.blockRefs = blockRefs;
        obj.LeftBlocks = LeftBlocks;
        obj.RightBlocks = RightBlocks;
        
      end;
      
      
    end
    
    function [out] = ctranspose(in)
      out = in;
      out.blockRefs = -out.blockRefs;
      for i = 1:length(out.LeftBlocks)
        out.LeftBlocks{i} = out.LeftBlocks{i}';
        out.RightBlocks{i} = out.RightBlocks{i}';
      end;
    end;
    
    function out = mtimes(a,b)      
      % Right multiply
      if isa(a,'SpatialBlockReg')
        if isa(b,'SpatialBlockReg')
          error('Can''t multiply two Spatial Block Regularizers together')
        else
          if ~all(size(b)==[a.Nv a.Nt])
            error('Size of right multiplier is incorrect');
          else
            out = zeros(size(b));
            for i = 1:length(a.blockRefs)
              d = a.blockRefs(i);
              out = out + a.LeftBlocks{i}*b*a.RightBlocks{i}; %diag(ones(a.Nt-abs(d),1),d);
            end;
          end;
        end;
      % Left Multiply
      elseif isa(b,'SpatialBlockReg')
        if ~all(size(a)==[b.Nt b.Nv])
          error('Size of left multiplier is incorrect')
        else
          out = zeros(b.Nv,b.Nt);
          for i = 1:length(b.blockRefs)
            d = b.blockRefs(i);
            out = out + b.Blocks{i}'*a'*b.RightBlocks{i}; %diag(ones(b.Nt-abs(d),1),-d);
          end;
          out = out';
        end;
      end;
    end;
    
  end
end