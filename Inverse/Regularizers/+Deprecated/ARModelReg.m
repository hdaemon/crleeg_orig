classdef ARModelReg
 
%
% function [ output ] = ARModelReg( options, LeadField )
%
% Constructor function for a Helmholtz class spatial regularizer object.
%
% Required Inputs:
%
%  options.solutionSpace.SpaceSize          Size of solution space
%                       .dist               Size of solution voxels
%                       .Voxels             Which voxels you're solving at
%         .regularizerOptions.HHFact        Helmholtz Factor
%                            .useWeighting  Flag to include weighting of
%                                             Helmholtz matrix by the norms
%                                             of the Leadfield columns
%         .vectorSolution                   Flat indicating if the solution
%                                             a vector or a scalar at each
%                                             solution point
%  LeadField                                The LeadField Matrix
%
%  Defined Methods:
%    ctranspose
%    mtimes
%
%  Written By: Damon Hyde (Last Edited: 03/02/2011)
%
%  Change Notes:  This code uses the old style of Matlab object oriented
%  code.  It's probably not a bad idea to go back and update it for the
%  next approach, circa R2008a.
%  
  
  properties
    mat
    solSpace
    isWeighted = false;
    oldWeighting = false;
    HHFactor = 0;
    isVectorSol = false;
  end
  
  methods
    function obj = ARModelReg(regOptions,LeadField,Data)
      
      if isa(regOptions,'ARModelReg')
        obj = regOptions;
      else
        
        mydisp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%');
        mydisp('Obtaining a solution using ARModel Regularizer');
        
        solutionSpace = LeadField.currSolutionSpace;
        
        obj.solSpace = solutionSpace;
        
        obj.HHFactor   = regOptions.HHFact;
        obj.isWeighted = regOptions.weightRegularizer;
        obj.oldWeighting = regOptions.oldWeighting;
        obj.isVectorSol  = regOptions.isVectorSolution;
        
        % not the right thing to do, but for now, since the voxels are close to
        % cubic.
        dist = mean(solutionSpace.dist);
        
        %% Build Laplacian Matrix:
        try
          if regOptions.corrHelmholtz
            mydisp('Using Correlated Helmholtz Matrix');
            B = build_CorrHelmholtzMatrix(LeadField,regOptions.HHFact,regOptions.isVectorSolution,false);
          else
            mydisp('Using Original Helmholtz Matrix');
            B = build_HelmholtzMatrix(solutionSpace,regOptions.HHFact,regOptions.isVectorSolution,false);            
          end;
          B1 = B;
                    
          mydisp(['Solving for ' num2str(length(solutionSpace.Voxels)) ' out of a possible ' num2str(prod(solutionSpace.SpaceSize)) ' nodes']);
          
          % Remove Diagonal from B (this is the matrix on the -1st
          % diagonal)
          %
          % This is currently just a super basic AR model.  It should get
          % updated in some way to make it all 
 
          B = B - diag(diag(B));
          rowsums = sum(B,2);
          B = B +  0.25*diag(rowsums);
          
          rowsums = sum(B,2);                    
          B = spdiags(1./rowsums(:),0,length(rowsums),length(rowsums))*B;
          B = B./0.5;
  
          B1 = spdiags(1./diag(B1),0,length(diag(B1)),length(diag(B1)))*B1;
          B1 = 0.5*B1;
         
          if regOptions.isVectorSolution
            LeadField = LeadField.setConstrained(false);
          else
            LeadField = LeadField.setConstrained(true);
          end;
          
          if obj.isWeighted
            mydisp('Using ARModel Regularizer WITH Spatial Weighting');
            wantOmegaInverse = false;
           
            Omega = LeadField.getWeightingMatrix(wantOmegaInverse);
            if obj.oldWeighting
              B = B*Omega;
            else
              B = Omega*B;
            end;
          else
            mydisp('Using ARModel Regularizer WITHOUT Spatial Weighting');
            obj.mat = B;
          end;              
          
          B0 = -spdiags(ones(size(rowsums)),0,size(B,1),size(B,1));                    
          
          LeftBlocks = {B,B0,B};
          blockRefs = [-1 0 1];
          
          RightBlocks{1} = diag(ones(size(Data,2)-1,1),-1);
          RightBlocks{2} = diag(ones(size(Data,2),1),0);         
          RightBlocks{3} = diag(size(size(Data,2)-1,1),1);
          
          
          LeftBlocks = LeftBlocks(1:2);
          RightBlocks = RightBlocks(1:2);
          blockRefs = [-1 0];
          
          obj.mat = SpatialBlockReg(length(solutionSpace.Voxels),size(Data,2),blockRefs,LeftBlocks,RightBlocks);
          
        catch
          keyboard;
        end;
        
      end;
      
    end;
    
    function [out] = ctranspose(in)     
        out = in;
        out.mat = out.mat';           
    end;
    
    function out = mtimes(a,b)    
      if isa(a,'ARModelReg')
        C1 = a.mat;
      else
        C1 = a;
      end;
      if isa(b,'ARModelReg')
        C2 = b.mat;
      else
        C2 = b;
      end
            
      out = C1*C2;
    end
    
  end;

end


