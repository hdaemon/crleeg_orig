classdef SpatioTemporalRegularizer

  % This is a meta-class, which consists of a spatial regularizer and a
  % temporal regularizer, and regularization coefficients for each of them.
  % The mtimes method only implements right multiplication at this time,
  % although temporal regularizers used must have left multiplication
  % implemented.
    
  properties
    Space
    Temp
    lambda   = 0;
    lambda_t = 0;
  end
  
  methods
    
    function obj = SpatioTemporalRegularizer(SpaceReg,TempReg)
      if exist('SpaceReg')&&isa(SpaceReg,'SpatioTemporalRegularizer')
        obj = SpaceReg;
      else
        if exist('SpaceReg')
          obj.Space = SpaceReg;
        end
        if exist('TempReg')
          obj.Temp = TempReg;
        end
      end
    end;
    
    function [valOut] = mtimes(a,b)
      
      if ~isa(a,'SpatioTemporalRegularizer')
        error('Only Right Multiplication Currently Supported for General Spatiotemporal regularization');
      end;
      
      if ~isnumeric(b)
        error('Right hand side must be numeric');
      end;
      
      % Multiply by spatial regularizer
      try
        if ~isempty(a.Space)
          SpaceOut = a.Space*b;
          SpaceOut = a.lambda*SpaceOut;
        else
          SpaceOut = [];
        end;
      catch
        SpaceOut = [];
      end;
      
      % Multiply by temporal regularizer
      try
        if ~isempty(a.Temp)
          TempOut = b*a.Temp;
          TempOut = a.lambda_t*TempOut;
        else
          TempOut = [];
        end;
      catch
        TempOut = [];
      end;
      
      % Get total product
      if ~isempty(SpaceOut)
        valOut = SpaceOut;
        if ~isempty(TempOut)
          valOut = valOut + TempOut;
        end;
      else
        valOut = TempOut;
      end;
      
      valOut = double(valOut);
      
      if isempty(valOut)
        keyboard;
        error('Returning empty set from nonempty multiplication!!');
      end;
      
    end;
       
    function [a] = ctranspose(a)            
      try
        a.Space = a.Space';
      catch
        keyboard;
        error('Error Transposing Spatial Regularizer');
      end;
      
      try
        a.Temp = a.Temp';
      catch
        error('Error Transposing Temporal Regularizer');
      end;
      
    end;
    
  end  %% End Methods Section
  
  
  
end


