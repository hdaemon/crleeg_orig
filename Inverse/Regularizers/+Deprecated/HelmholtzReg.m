classdef HelmholtzReg
  
  properties
    mat
    solSpace
    isWeighted = false;
    oldWeighting = false;
    HHFactor = 0;
    isVectorSol = false;
  end
  
  methods
    function obj = HelmholtzReg(regOptions,LeadField)
      
      if isa(regOptions,'HelmholtzReg')
        obj = regOptions;
      else
        
        mydisp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%');
        mydisp('Obtaining a solution using Helmholtz Regularizer');
        
        solutionSpace = LeadField.currSolutionSpace;
        
        obj.solSpace = solutionSpace;
        
        obj.HHFactor   = regOptions.HHFact;
        obj.isWeighted = regOptions.weightRegularizer;
        obj.oldWeighting = regOptions.oldWeighting;
        obj.isVectorSol  = regOptions.isVectorSolution;
        
        % not the right thing to do, but for now, since the voxels are close to
        % cubic.
        dist = mean(solutionSpace.dist);
        
        %% Build Laplacian Matrix:
        try
          if regOptions.corrHelmholtz
            mydisp('Using Correlated Helmholtz Matrix');
            B = build_CorrHelmholtzMatrix(LeadField,regOptions.HHFact,regOptions.isVectorSolution,false);
          else
            mydisp('Using Original Helmholtz Matrix');
            B = build_HelmholtzMatrix(solutionSpace,regOptions.HHFact,regOptions.isVectorSolution,false);
          end;
          
          mydisp(['Solving for ' num2str(length(solutionSpace.Voxels)) ' out of a possible ' num2str(prod(solutionSpace.SpaceSize)) ' nodes']);
          
          if obj.isWeighted
            mydisp('Using Helmholtz Regularizer WITH Spatial Weighting');
            wantOmegaInverse = false;
            Omega = LeadField.getWeightingMatrix(wantOmegaInverse);

            if obj.oldWeighting
              obj.mat = B*Omega;
            else
              obj.mat = Omega*B;
            end;
          else
            mydisp('Using Helmholtz Regularizer WITHOUT Spatial Weighting');
            obj.mat = B;
          end;
        catch
          keyboard;
        end;
        
      end;
      
    end;
    
    function [out] = ctranspose(in)     
        out = in;
        out.mat = out.mat';           
    end;
    
    function out = mtimes(a,b)    
      if isa(a,'HelmholtzReg')
        C1 = a.mat;
      else
        C1 = a;
      end;
      if isa(b,'HelmholtzReg')
        C2 = b.mat;
      else
        C2 = b;
      end
            
      out = C1*C2;
    end
    
  end;

end

%
% function [ output ] = Helmholtz( options, LeadField )
%
% Constructor function for a Helmholtz class spatial regularizer object.
%
% Required Inputs:
%
%  options.solutionSpace.SpaceSize          Size of solution space
%                       .dist               Size of solution voxels
%                       .Voxels             Which voxels you're solving at
%         .regularizerOptions.HHFact        Helmholtz Factor
%                            .useWeighting  Flag to include weighting of
%                                             Helmholtz matrix by the norms
%                                             of the Leadfield columns
%         .vectorSolution                   Flat indicating if the solution
%                                             a vector or a scalar at each
%                                             solution point
%  LeadField                                The LeadField Matrix
%
%  Defined Methods:
%    ctranspose
%    mtimes
%
%  Written By: Damon Hyde (Last Edited: 03/02/2011)
%
%  Change Notes:  This code uses the old style of Matlab object oriented
%  code.  It's probably not a bad idea to go back and update it for the
%  next approach, circa R2008a.
%
