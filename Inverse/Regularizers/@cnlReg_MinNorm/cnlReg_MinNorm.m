classdef cnlReg_MinNorm < cnlReg_LinearSpace
  
  
  methods
    
    function obj = cnlReg_MinNorm(options,LeadField)
                  
      obj.origSolutionSpace = LeadField.currSolutionSpace;            
      obj.origMatrix = speye(size(LeadField.currSolutionSpace.matGridToSolSpace,1));
      
      if options.isVectorSolution, 
        obj.colPerVox = 3; 
        obj.origMatrix = kron(obj.origMatrix,eye(obj.colPerVox));
      end;
      
      if options.weightRegularizer
        obj.matRightWeight = LeadField.getWeightingMatrix(false);
        
        if max(diag(obj.matRightWeight))/min(diag(obj.matRightWeight)) > 1e8
          scale = 1e-8*max(diag(obj.matRightWeight));
          obj.matRightWeight = obj.matRightWeight + scale*speye(size(obj.matRightWeight));
        end
        
        obj.isRightWeighted = true;
      end;
                  
      obj.currSolutionSpace = LeadField.currSolutionSpace;
    end;        
    
  end
  
end
