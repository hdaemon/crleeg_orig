classdef cnlReg_LinearSpatioTemporal
  
  
  properties
    Space
    Temp
    spatialTransform
    lam_space = 0;
    lam_time = 0;
  end
  
  methods
    
    function obj = cnlReg_LinearSpatioTemporal(SpaceReg,TempReg)
      % function obj = cnlReg_LinearSpatioTemporal(SpaceReg,TempReg)
      %
      % 
      if nargin>0        
        if isa(SpaceReg,'cnlReg_LinearSpatioTemporal')
          obj = SpaceReg;
          return;
        end
                 
        obj.Space = SpaceReg;      
        if exist('TempReg'), obj.Temp = TempReg; end;
        
%         solGrid = cnlGridSpace(obj.Space.currSolutionSpace);
%         solSpace = cnlSolutionSpace(solGrid,obj.Space.currSolutionSpace.Voxels);
%         matTransform = getMapping(solSpace,obj.Space.currSolutionSpace);
%         obj.spatialTransform = matTransform;
        
      end;
    end;
    
    function [valOut] = mtimes(a,b)
      
      if ~isa(a,'cnlReg_LinearSpatioTemporal')
        error('Only Right Multiplication Currently Supported for General Spatiotemporal regularization');
      end;
      
      if ~isnumeric(b)
        error('Right hand side must be numeric');
      end;
      
      % Multiply by spatial regularizer

        if ~isempty(a.Space)
          SpaceOut = a.Space*b;
          SpaceOut = a.lam_space*SpaceOut;
        else
          SpaceOut = [];
        end;
      
        % Multiply by temporal regularizer
        if ~isempty(a.Temp)
                    
          TempOut = (a.Temp*b')';
%          TempOut = a.spatialTransform*TempOut;
          TempOut = a.lam_time*TempOut;
          
%           vox = a.Space.currSolutionSpace.Voxels;
%           map = a.Space.currSolutionSpace.matGridToSolSpace(:,vox);
%           
%           % This is a bit of a hack at the moment, put in here to deal with
%           % whether things are transposed or not.  This will need to be updated
%           % soon.
%           if size(map,1)==size(SpaceOut,1)
%             TempOut = map*TempOut;
%           else
%             TempOut = map'*TempOut;
%           end;
        else
          TempOut = [];
        end;
      

      
      % Get total product
      if ~isempty(SpaceOut)
       % disp(['Size of SpaceOut = ' num2str(size(SpaceOut))]);
        valOut = SpaceOut;
        if ~isempty(TempOut)
       %   disp(['Size of TempOut = ' num2str(size(TempOut))]);
          valOut = valOut + TempOut;
        end;
      else
        valOut = TempOut;
      end;
      
      valOut = double(valOut);
      
      if isempty(valOut)
        keyboard;
        error('Returning empty set from nonempty multiplication!!');
      end;
      
    end;
       
    function [a] = ctranspose(a)                
      a.Space = a.Space';           
      a.Temp = a.Temp';     
      a.spatialTransform = a.spatialTransform';
    end;
    
  end  %% End Methods Section
  
  
  
end


