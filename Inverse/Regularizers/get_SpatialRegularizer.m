function SpaceReg = get_SpatialRegularizer(LeadField,Data,ReconOpts)
% GET_SPATIALREGULARIZER Compute spatial regularizer
%
% function SpaceReg = get_SpatialRegularizer(LeadField,Data,ReconOpts)
%
% Given a leadfield, input data, and linear reconstruction options, compute
% the spatial regularization object needed to obtain an inverse solution.
%
% Written By: Damon Hyde
% Last Edited: Dec 21, 2015
% Part of the cnlEEG Project
%

mydisp(['Computing Spatial Regularizer']);
switch lower(ReconOpts.SpaceRegType)
  case 'loreta'
    SpaceReg = cnlReg_LORETA(ReconOpts, LeadField);
  case 'helmholtz'
    SpaceReg = cnlReg_Helmholtz(ReconOpts, LeadField); 
  case 'helmholtz_static'
    SpaceReg = cnlReg_Helmholtz_Static(ReconOpts,LeadField);
  case 'corticalgraph'
    SpaceReg = cnlReg_CorticalGraph(ReconOpts,LeadField);
  case 'minnorm'
    SpaceReg = cnlReg_MinNorm(ReconOpts,LeadField);
  otherwise
    SpaceReg = [];
end;

%Set norm of the matrix to 1
% RegNorm = svds(SpaceReg.currMatrix,1);
% SpaceReg.matLeftWeight = (1./RegNorm)*speye(size(SpaceReg,1));
% SpaceReg.isLeftWeighted = true;

end