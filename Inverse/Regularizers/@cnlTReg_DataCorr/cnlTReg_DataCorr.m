classdef cnlTReg_DataCorr
  
  
  properties
    mat
    npoints = 0;
  end
  
  methods
    function obj = cnlTReg_DataCorr(DataIn)

        obj.npoints = size(DataIn,2);
        
        % Normalize Rows
        %DataIn = DataIn*diag(1./sqrt(sum(DataIn.^2,1)));
        
        % Get Sample Covariance
        DataCorr = DataIn'*DataIn;
        
        obj.mat = pinv(DataCorr);
        %[U S V] = svd(DataCorr);
        %Sinv = diag(1./sqrt(diag(S)));
        %corrInv = V*Sinv*U';
        
%         [U S V] = svd(DataIn);
%                 
%         %corrInv = V*diag(1./sqrt(diag(S)))*V';
%         %obj.mat = corrInv;                
%         
%         w = [0:(size(DataIn,2)-1)]';
%         alpha = 1000;
%         fval = (pi/alpha)*exp(-(w*pi).^2/alpha);
%         fval = 1e-10*ones(size(fval));
%         fval(1:3) = 1;
%         
%         filtered = diag(S).*fval;
%         obj.mat = V*diag(1./filtered)*V';
%         
%         figure;
%         subplot(1,3,1);
%         plot(log10(diag(S)./max(diag(S)))); title('Original Spectrum');
%         subplot(1,3,2); 
%         plot(log10(fval./max(fval)));title('Filter Spectrum');
%         subplot(1,3,3); 
%         plot(log10(filtered./max(filtered)));title('Final Spectrum');
%         keyboard;
%         
% %        obj.mat = sqrtm(pinv(DataCorr));
%         obj.mat = obj.mat./norm(obj.mat,'fro');

    end
    
    function objOut = ctranspose(objIn)
      objOut = objIn;
      objOut.mat = objOut.mat';
    end;
    
    function out = mtimes(a,b)
      if isa(a,'cnlTReg_DataCorr')
        C1 = a.mat;
      else
        C1 = a;
      end;
      if isa(b,'cnlTReg_DataCorr')
        C2 = b.mat;
      else
        C2 = b;
      end
            
      out = C1*C2;      
    end;    
    
  end
  
end