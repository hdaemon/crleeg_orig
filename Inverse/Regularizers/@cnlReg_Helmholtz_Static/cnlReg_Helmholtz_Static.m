classdef cnlReg_Helmholtz_Static < cnlReg_LinearSpace
  
  % classdef cnlLinReg_Helmholtz < cnlReg_LinearSpace
  %
  % Implements a Helmholtz-type spatial regularizer based on the
  % cnlReg_LinearSpace class.
  
  properties
    HHFactor = 0;
  end
  
  methods
    
    function obj = cnlReg_Helmholtz_Static(options,leadField)
      
      obj = obj@cnlReg_LinearSpace;
      
      if nargin>0
        if isa(options,'cnlReg_Helmholtz')
          obj = options;
          return;
        end
        
        mydisp('Constructing a linear Helmholtz regularizer');
        obj = obj.setup(options,leadField);
        
      end
    end
    
    function obj = rebuildCurrMatrix(obj)
      % function obj = rebuildCurrMatrix(obj)
      %
      % This completely skips the use of rebuildCurrMatrix@cnlMatrixOnSpace
      %      
      
      if obj.stillLoading, obj.currMatrix = []; return; end;
      
      if ~obj.canRebuild
        warning('Failed rebuild');
        obj.currMatrix = [];
        return;
      end            
      
      mydisp('Rebuilding Static Helmholtz object');
      matTransform = getMapping(obj.origSolutionSpace,obj.currSolutionSpace);
      
      if ~obj.isCollapsed,
        matTransform = kron(matTransform,speye(obj.colPerVox));
        obj.currMatrix = obj.origMatrix;
      else obj.currMatrix = obj.origMatrix*obj.matCollapse;
      end;
      
      obj.currMatrix = matTransform'*obj.currMatrix*matTransform;
      
    end
    
    function obj = setup(obj,options,leadField)
      obj.HHFactor = options.HHFact;
      
      % Set spatial weighting if desired
      
      if options.weightRegularizer;
        obj.matRightWeight = leadField.getWeightingMatrix(false);
        obj.isRightWeighted = true;
      end;
      
      % Build Helmholtz matrix
      if options.isVectorSolution, obj.colPerVox = 3; end;
      solGrid = cnlGridSpace(leadField.currSolutionSpace);
      solSpace = cnlSolutionSpace(solGrid,leadField.currSolutionSpace.Voxels);      
      obj.origSolutionSpace = solSpace;
      obj.origMatrix = obj.build;
      
      % Set current solution space and rebuild matrix
      obj.currSolutionSpace = leadField.currSolutionSpace;
    end
    
    matOut = build(obj);
    
    function S = saveobj(obj)
      S = saveobj@cnlReg_LinearSpace(obj);
      S.HHFactor = obj.HHFactor;
    end;
    
    function obj = reload(obj,S)
      obj = reload@cnlReg_LinearSpace(obj,S);
      obj.HHFactor = S.HHFactor;
    end
      
    
  end
  
  methods (Static = true)
    
    function obj = loadobj(S)
      obj = cnlReg_Helmholtz_Static; % Initialize empty object
      obj.stillLoading = true;       % Turn off rebuilding
      obj = reload(obj,S);           % Set properties
      obj.stillLoading = false;      % Turn rebuilding back on
      obj = obj.rebuildCurrMatrix;   % Rebuild
    end
    
  end
  
end