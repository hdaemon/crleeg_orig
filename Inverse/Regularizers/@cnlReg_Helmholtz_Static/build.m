function [matOut] = build(obj)
% function [matOut] = build(obj)
%
% Matrix build function for the cnlReg_Helmholtz regularizer class
%

SpaceSize      = obj.origSolutionSpace.sizes;

%% Build Laplacian Matrix:
mydisp('Building Laplacian Matrix');
mydisp(['Solution Space is of Size: ' num2str(SpaceSize(1)) ' x ' num2str(SpaceSize(2)) ' x ' num2str(SpaceSize(3))]);

imgGrid = cnlGrid(SpaceSize);
[rowIdx, colIdx] = imgGrid.getGridConnectivity(6);

A1 = sparse(rowIdx,colIdx,ones(size(colIdx)),prod(SpaceSize),prod(SpaceSize));

%Eliminate connections to voxels outside the current solution space.
allVox = 1:size(A1,1);
voxInSol = obj.origSolutionSpace.Voxels;
voxOutSol = setdiff(allVox,voxInSol);

A1(voxOutSol,:) = 0;
A1(:,voxOutSol) = 0;

% Get row sums
tmp = A1*ones(size(A1,2),1);
tmp(tmp==0) = -1;
scale = sparse(1:size(A1,1),1:size(A1,1),tmp);

% Make a vector matrix, if necessary
A = kron(A1,speye(obj.colPerVox));
scale = kron(scale,speye(obj.colPerVox));



% Build Helmholtz Matrix
matOut = A - scale - obj.HHFactor^2*speye(size(A,1));

% Project matrix into starting solution space.
tmpMat = obj.origSolutionSpace.matGridToSolSpace;
matOut = tmpMat*(matOut*tmpMat');

end

