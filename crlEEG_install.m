%% Script to install cnlEEG and add appropriate directories to the path
tmp = mfilename('fullpath');
[dir_Root,~,~] = fileparts(tmp);

%% Install cnlEEG Components
path_BasicObjects = genpath([dir_Root '/BasicObjects']);
path_FileTypes    = genpath([dir_Root '/FileTypes']);
path_Forward      = genpath([dir_Root '/Forward']);
path_Inverse      = genpath([dir_Root '/Inverse']);
path_NRRDTypes    = genpath([dir_Root '/NRRDTypes']);
path_Options      = genpath([dir_Root '/Options']);
path_Other        = genpath([dir_Root '/Other']);
path_Packages     = genpath([dir_Root '/Packages']);
path_Pipelines    = genpath([dir_Root '/Pipelines']);
path_DataHandling = genpath([dir_Root '/DataHandling']);


%% Try to install biosig from cnlEEG/External/biosig
%  For EDF reader functionality, biosig must either be manually installed
%  in the path, or linked to in /External/biosig
%try
%disp('Trying to Install BioSig');
% cd([dir_Root '/External/biosig']);
%  BIOSIG_HOME = pwd;
%  if exist('install.m','file')
%    install;
%  elseif exist('biosig_installer.m','file')
%    biosig_installer;
%  else
%    error('cnlEEG:NoBioSig','BioSig directory in place, but no installer found');
%  end;
%  
%  %% Remove the BioSig NaN Directory.  It just messes things up royally.
%  rmpath([BIOSIG_HOME,'/NaN']);
%  %% support both types of directory structure
%  if exist([BIOSIG_HOME,'/NaN/inst'],'dir')
%    rmpath([BIOSIG_HOME,'/NaN/inst']);
%  end;
%  if exist([BIOSIG_HOME,'/NaN/src'],'dir')
%    rmpath([BIOSIG_HOME,'/NaN/src']);
%  end
%  
%  
%  disp('Successfully Installed BioSig Path');
%catch
%  warning('cnlEEG:NoBioSig','BioSig Not Found. EDF Reader Functionality will not work');
%end;
%
%%% Try to install cvx from cnlEEG/External/cvx
%try
%  disp('Trying to Install CVX');
%  cd([dir_Root '/External/cvx']);
%  cvx_setup;
%  disp('Successfully Installed CVX');
%catch
%  warning('cnlEEG:NoCVX','CVX Not Found. Convex Optimization functionality will not work');
%end
%
%%% Install fastica
%if exist([dir_Root '/External/fastica'],'dir')
%path(path,[dir_Root '/External/fastica']);
%else
%  warning('cnlEEG:NoFastICA','FastICA Package Not Found');
%end;

% Move to the root directory to finish up
cd(dir_Root);

% Add cnlEEG Directories to Path
path(path_Other,path);
path(path_Options,path);
path(path_NRRDTypes,path);
path(path_Inverse,path);
path(path_Forward,path);
path(path_FileTypes,path);
path(path_BasicObjects,path);
path(path_Packages,path);
path(path_Pipelines,path);
path(path_DataHandling,path);
path(dir_Root,path);

clear dir_Root BIOSIG_HOME path_*






