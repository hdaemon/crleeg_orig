function cnlStartMatlabPool
% CNLSTARTMATLABPOOL Start parallel processing pool
%
% function CNLSTARTMATLABPOOL()
%
% Checks if there is a parallel processing pool currently running, and if
% not, creates a new temporary storage location and starts the pool running
% there. Functionality determining the number of workers to start is
% specific to CRL computers, and should be modified when running this
% elsewhere.
%
% Written By: Damon Hyde
% Last Edited: Oct 22, 2015
% Part of the cnlEEG Project
%

global nWorkers

%% Generalized function to start the matlabpool
%
mydisp('Using Parallel Computational Approach');

% mydisp('Forcing creation of parallel pool at execution time');
% return;
pool = gcp('nocreate');

if isempty(pool)
  
  [foo, systemname] = system('hostname');
  systemname = systemname(systemname ~=10);
  
  towerWorkstations = {'io' 'ganymede' 'europa' 'callisto'};
  r620s = {'zephyr' 'eurus' 'boreas' 'auster'};
  
  if isempty(nWorkers)
  if ismember(systemname,towerWorkstations)
    nWorkers = 12;
  elseif ismember(systemname,r620s)
    nWorkers = 12;
  else
    nWorkers = 12;
  end;
  end;
  
  c = parcluster('local');
  t = tempname();
  mkdir(t);
  c.JobStorageLocation = t;
  if exist('parpool')
    % >= 2013b
    localPool = parpool(c,nWorkers);
  else
    % <2013b
    matlabpool(c,nWorkers);
  end;
  
elseif isa(pool,'parallel.Pool')
  mydisp(['Matlabpool already running with ' num2str(pool.NumWorkers) ' workers.']);
else
    error('Global variable localPool should either be empty or an object of type parallel.Pool');
end;

mydisp('Parallel Pool Successfully Started');

end