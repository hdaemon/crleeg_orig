function [connectedCortex] = get_ConnectedCortex(voxels,spaceSize,connectionLevel)
% function [connectedCortex] = get_ConnectedCortex(voxels,spaceSize,connectionLevel)
%
% Given a list of voxels (voxels) in a space of size (spaceSize), return a
% list of voxels with a number of neighbors greater than or equal to the
% connectionLevel
%
%  Inputs:   voxels          : List of voxels in the space
%            spaceSize       : Size of the 3D space in which voxels exist
%            connectionLevel : Minimum number of connections to be retained
%
%  Outputs:  connectedCortex : List of voxels with sufficient connection level
%
%  Calls:
%
%  Called By: get_VoxelIndices
%
% Last Modified: Damon Hyde, 04/26/2012

% Get Binary Image of Segmented Cortex
tmp = zeros(spaceSize);
tmp(voxels) = 1;

% Initialize
done = false;
connectedCortex = find(tmp);

while ~done
  
  % Get the indices of every neighboring voxel
  [idxX idxY idxZ] = ind2sub(size(tmp),voxels);
  idxX2 = kron(idxX(:),ones(6,1));
  idxY2 = kron(idxY(:),ones(6,1));
  idxZ2 = kron(idxZ(:),ones(6,1));
  
  % Convert to Indices of Neighbors
  offsetX = kron(ones(numel(idxX),1),[ 1 -1  0  0  0  0 ]');
  offsetY = kron(ones(numel(idxY),1),[ 0  0  1 -1  0  0 ]');
  offsetZ = kron(ones(numel(idxZ),1),[ 0  0  0  0  1 -1 ]');
  
  idxX2 = idxX2 + offsetX;
  idxY2 = idxY2 + offsetY;
  idxZ2 = idxZ2 + offsetZ;
  
  % Trim Those that Fall Outside the Image Volume
  Q = find( ( idxX2<1 ) | ( idxX2>size(tmp,1) ) | ( idxY2<1 ) | ( idxY2>size(tmp,2) ) | ( idxZ2<1 ) | ( idxZ2>size(tmp,3) ) );
  idxX2(Q)  = []; idxY2(Q)  = []; idxZ2(Q)  = [];
  
  % Column and Row Indexes Into Sparse matrix
  colIdx    = sub2ind(size(tmp),idxX2,idxY2,idxZ2);
  
  rowIdx = voxels;
  rowIdx = kron(rowIdx(:),ones(6,1));
  
  validNeighbor = (tmp(colIdx)==1);
  rowIdx(~validNeighbor) = [];
  colIdx(~validNeighbor) = [];
  
  howConnected = zeros(size(tmp));
  for i = 1:length(rowIdx)
    howConnected(rowIdx(i)) = howConnected(rowIdx(i)) + 1;
  end;
  
  oldConnectedCortex = connectedCortex;
  connectedCortex = find(howConnected>=connectionLevel);
   
  if length(oldConnectedCortex)==length(connectedCortex)
  %  disp(['Final number of voxels: ' num2str(length(connectedCortex))]);
    done = true;
  else
    tmp = zeros(spaceSize);
    tmp(connectedCortex) = 1;
  %  disp(['Number of voxels: ' num2str(length(connectedCortex))]);
  end;
  
end;


end