classdef options_Save

  properties
    doSolution    = [];
    doByTimePoint = 0;  % flag to determine whether lambdas are selected per solution or per timepoint
    doLambda      = [];
    doLambdaT     = 1;
    doTimePoints  = [];
    isRGB         = false;
    convertVecToScalar = false;
    errTarget     = 0.1;    
    fname         = '';
  end;
  
  methods
    
  function obj = option_Save()
  end;
  
  end;
  
end
