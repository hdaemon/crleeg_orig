function [SimSetOut] = createSyntheticImage(nrrdParcel,LeadField,Parcels,...
  Intensities,Noise,fnamebase,savedir)
%function [SimSetOut] = createSyntheticImage(nrrdParcel,LeadField,Parcels,Intensities,Noise,fnamebase,savedir)
%
%
% Inputs:
%

    % Extract parcellation data
    parcelData = nrrdParcel.data(LeadField.currSolutionSpace.Voxels);  

    % Get the noise image
    noiseImage = zeros(size(parcelData));
    Q = find(parcelData>0);
    if Noise~=0
      noiseImage(Q) = Noise*randn(size(Q));        
    end;
    
    % Assign intensities within each activated parcel
    sigImage = zeros(size(parcelData));
    for i = 1:length(Parcels)
     Q = find(ismember(parcelData,Parcels(i)));         
     sigImage(Q) = Intensities(i);    
    end;

    % Get the full imageh
    fullImage = noiseImage + sigImage;
    
    % Generate Simulated Data
    noiseData = LeadField*noiseImage(:);
    sigData   = LeadField*sigImage(:);
    fullData  = noiseData+sigData;
                
    % Save the Image out at a NRRD    
    nrrdFullImage = clone(nrrdParcel,[fnamebase '_Noisy.nhdr'],savedir);
    nrrdFullImage.type = 'float';    
    dataOut = zeros(LeadField.currSolutionSpace.sizes);
    dataOut(LeadField.currSolutionSpace.Voxels) = fullImage;
    nrrdFullImage.data    = dataOut;
                      
    nrrdNoiseImage = clone(nrrdParcel,[fnamebase '_Noise.nhdr'],savedir);
    nrrdNoiseImage.type = 'float';    
    dataOut = zeros(LeadField.currSolutionSpace.sizes);
    dataOut(LeadField.currSolutionSpace.Voxels) = noiseImage;
    nrrdNoiseImage.data    = dataOut;
    
    nrrdTrueImage = clone(nrrdParcel,[fnamebase '_NoiseFree.nhdr'],savedir);
    nrrdTrueImage.type = 'float';    
    dataOut = zeros(LeadField.currSolutionSpace.sizes);
    dataOut(LeadField.currSolutionSpace.Voxels) = sigImage;
    nrrdTrueImage.data    = dataOut;
   
    SimSetOut.nrrdFullImage = nrrdFullImage;
    SimSetOut.nrrdTrueImage = nrrdTrueImage;
    SimSetOut.noiseData     = noiseData;
    SimSetOut.sigData       = sigData;
    SimSetOut.fullData      = fullData;
    
end