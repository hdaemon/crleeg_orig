function [DataOut] = ...
  createSyntheticTemporalData(IBSR,Parcels,TimeSeries,Noise,...
  nrrdTemplate,LeadField,savedir,casenumstr,imagenumstr)

% function [DataOut] = ...
%  createSyntheticTemporalData(IBSR,Parcels,TimeSeries,Noise,...
%  nrrdTemplate,LeadField,savedir,casenumstr,imagenumstr)
%
%

nT = size(TimeSeries,1);

DataOut.fullData = zeros(size(LeadField,1),nT);

for idxT = 1:nT
    [SimSet] = createSyntheticImage(IBSR,LeadField,Parcels,TimeSeries(idxT,:),...
      Noise,['Case' casenumstr 'SimData' imagenumstr '_T' num2str(idxT)],savedir);
    
   % SimSet.nrrdFullImage.save;
   % SimSet.nrrdTrueImage.save;
    
    DataOut.fullImage(:,idxT) = SimSet.nrrdFullImage.data(:);
    DataOut.trueImage(:,idxT) = SimSet.nrrdTrueImage.data(:);
    DataOut.fullData(:,idxT) = SimSet.fullData;
end;

end