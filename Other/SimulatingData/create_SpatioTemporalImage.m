function imgOut = create_SpatioTemporalImage(nrrdParcel,useParcels,timeSeries)
% CREATE_SPATIOTEMPORALIMAGE
%
% function imgOut = create_SpatioTemporalImage(nrrdParcel,useParcels,timeSeries)
%
% Creates a simulated spatiotemporally varying image using an input
% parcellation, a list of parcels for activity to be centered on, and a set
% of times series to define the 
%
% Currently, this just creates image patterns based on a sinc function.
% Future functionality will make this an externally available option.
%
% Written By: Damon Hyde
% Last Edited: Dec 21, 2015
% Part of the cnlEEG project
%


if length(useParcels)~=size(timeSeries,1)
  error(['Number of parcels to be used must match number of rows ' ...
          'in time series']);
end

nT = size(timeSeries,2);

allGridPoints = nrrdParcel.gridSpace.getGridPoints;

imgOut = zeros(prod(nrrdParcel.sizes),size(timeSeries,2));

voxInBrain = find(nrrdParcel.data>0);
ptsInBrain = allGridPoints(voxInBrain,:);

mydisp(['Building spatiotemporal image from ' num2str(numel(useParcels)) ' parcels']);
for i = 1:numel(useParcels)
  voxInParcel = find(nrrdParcel.data==useParcels(i));
  voxLocations = allGridPoints(voxInParcel,:);
  
  centroid = mean(voxLocations,1);
  
  tmpImg = create_SpatialDistribution(nrrdParcel,centroid,...
                  'sigma',timeSeries(i,:),'type','sinc');
        
  imgOut = imgOut + tmpImg;
end;

end