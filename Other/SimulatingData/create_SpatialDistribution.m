function imgOut = create_SpatialDistribution(nrrdTemplate, varargin)
% CREATE_SPATIALDISTRIBUTION
%
% function imgOut = create_SpatialDistribution(nrrdTemplate, varargin)
%
% Using an input file_NRRD as a template for the image size and geometry,
% generate a 3D spatial distribution 
%
% Required Inputs:
%  nrrdTemplate : file_NRRD Object to base geometry on
%  
% Optional Inputs:
%  'type'       : Type of Image Pattern to Use
%                   'gauss' : Gaussian distribution with standard deviation
%                               = sigma
%                   'const' :
%                   'dist'  :
%                   'sinc'  :
%                   Default: 'gauss'
%  'cutoffdist' : Distance from centroid to threshold image to zero
%                   Default: 60 (mm)
%  'sigma'      : Size/shape parameter for Images
%                   Default: []
%  'normalized' : If true, image will be normalized to max(imgOut(:))==1
%                   Default: FALSE
%
% Written By: Damon Hyde
% Last Edited: Jan 12, 2016
% Part of the cnlEEG Project
%

%% Input Parsing
p = inputParser;
p.addRequired('centroid',@(x) size(x,2)==3);
p.addParamValue('type','gauss');
p.addParamValue('cutoffdist',60);
p.addParamValue('sigma',[]);
p.addParamValue('normalized',false);
parse(p,varargin{:});

centroid = p.Results.centroid;
sigma = p.Results.sigma;
type = p.Results.type;

nCenter = size(centroid,1);
nSigma = numel(sigma);
if nSigma==0, nSigma=1; end;

assert((nCenter==nSigma)||(nCenter==1)||(nSigma==1),...
  ['Must provide either a single sigma value, or one per centroid location ']);
     
%% Initialize Output
nOut = max(nCenter,nSigma);
imgOut = zeros(prod(nrrdTemplate.sizes),nOut);

%% Get Brain Locations
allGridPoints = nrrdTemplate.gridSpace.getGridPoints;
voxInBrain = find(nrrdTemplate.data>0);
ptsInBrain = allGridPoints(voxInBrain,:);

for idxC = 1:nCenter
  currcentroid = centroid(idxC,:);
  
  % Get Distance to Centroid
  delta = ptsInBrain - repmat(currcentroid,numel(voxInBrain),1);
  dist = sqrt(sum(delta.^2,2));
  useVox = dist<p.Results.cutoffdist;
       
  if (nCenter==nSigma)&&~isempty(sigma)    
    currsigma = sigma(idxC);    
  else
    currsigma = sigma;
  end;
  
  switch type
    case 'const'
      vals = ones(size(dist));
    case 'dist'
      vals = dist;
    case 'gauss'
      if isempty(currsigma), currsigma = median(dist); end;
      for i = 1:numel(currsigma)
        vals(:,i) = exp(-0.5*dist/currsigma(i));
      end
    case 'sinc'
      if isempty(currsigma), currsigma = 3 + 4*rand(1); end;
      for i = 1:numel(currsigma)
        vals(:,i) = sin(dist/currsigma(i))./(dist/currsigma(i));
      end;
    otherwise
      error('Unknown image generator function type');
  end
  
  if nCenter==nSigma    
    imgOut(voxInBrain(useVox),idxC) = vals(useVox);
  else
    imgOut(voxInBrain(useVox),:) = vals(useVox,:);
  end
  
end;

% Normalize images, if desired.
if p.Results.normalized
  for i = 1:size(imgOut,2)
    imgOut(i,:) = imgOut(i,:)./max(abs(imgOut(i,:)));
  end
end

end

