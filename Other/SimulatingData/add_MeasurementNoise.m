function sigOut = add_MeasurementNoise(sigIn,SNR)
% function sigOut = add_MeasurementNoise(sigIn,SNR)
%
% Adds Gaussian white noise to sigIn to make SNR equal to provided value.

Scale = @(SNR,sig,noise) norm(sig(:))/norm(noise(:))*(1/10^(SNR/10));
noise = randn(size(sigIn));
noise = noise*Scale(SNR,sigIn,noise);

sigOut = sigIn + noise;
end