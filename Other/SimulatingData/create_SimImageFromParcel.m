function imgOut = create_SimImageFromParcel(nrrdParcel,...
                  useParcels,timeSeries,sigma)
% CREATE_SIMIMAGEFROMPARCEL
%
% function imgOut = create_SimImageFromParcel(nrrdParcel,...
%                  useParcels,timeSeries);                
%
% Using an input parcellation, constructed a simulated cortical activation
% pattern
%
% Inputs:
%   nrrdParcel : cnlParcellation object
%   useParcels : Index of parcels in nrrdParcel to use
%   timeSeries : nParcels X nTime matrix of time series activations
%
% Written By: Damon Hyde
% Last Edited: Dec 21, 2015
% Part of the cnlEEG Project
%


p = inputParser;
p.addRequired('useParcels');
%p.addParamValue('type','gauss');
%p.addParamValue('cutoffdist


if ~exist('sigma','var')||isempty(sigma), sigma = 10; end;

if length(useParcels)~=size(timeSeries,1)
  error(['Number of parcels to be used must match number of rows ' ...
          'in time series']);
end

nT = size(timeSeries,2);

allGridPoints = nrrdParcel.gridSpace.getGridPoints;

imgOut = zeros(prod(nrrdParcel.sizes),size(timeSeries,2));

voxInBrain = find(nrrdParcel.data>0);
%ptsInBrain = allGridPoints(voxInBrain,:);

for i = 1:numel(useParcels)
  voxInParcel = find(nrrdParcel.data==useParcels(i));
  
  %% Create image based on parcel centroid
  voxLocations = allGridPoints(voxInParcel,:); 
  centroid = mean(voxLocations,1);    
  img = create_SpatialDistribution(nrrdParcel,centroid,'type','gauss','sigma',sigma);
  
%   img = zeros(prod(nrrdParcel.sizes,1));
%   img(voxInParcel) = 1;
%   
  imgOut = imgOut + img(:)*timeSeries(i,:);        

end;
  
% fullParcel = zeros(nrrdParcel.nParcel,size(timeSeries,2));
% fullParcel(useParcels,:) = timeSeries;
% 
% imgOut = nrrdParcel.get_MappingMatrix*fullParcel;

end