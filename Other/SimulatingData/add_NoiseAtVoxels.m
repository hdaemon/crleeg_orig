function imgOut = add_NoiseAtVoxels(imgIn,voxelList,SNR)
% function imgOut = add_NoiseAtVoxels(imgIn,voxels,SNR)
%
% Take an image, and add Gaussian white noise at all voxels in voxelList,
% so the resulting SNR is 


imgOut = imgIn;
imgOut(voxelList,:) = add_MeasurementNoise(imgIn(voxelList,:),SNR);

end