function [TimeSeries] = create_SimTimeSequence(Basis,Intensities)
% function [TimeSeries] = createSyntheticTemporalSequence(Basis,Intensities)
%
% Given a set of basis functions, and a list of region intensities,
% generates a time course for each region as a random mixing of the
% provided basis functions.
%

nT = size(Basis,2); % Get the number of timepoints
nRegions = length(Intensities);

RandomGen = randn(size(Basis,1),nRegions);
RandomGen = RandomGen*diag(1./sqrt(sum(RandomGen.^2,1)));

TimeSeries = Basis'*RandomGen;
TimeSeries = TimeSeries*diag(1./max(TimeSeries,[],1));
TimeSeries = TimeSeries*diag(Intensities);
TimeSeries = TimeSeries';

end