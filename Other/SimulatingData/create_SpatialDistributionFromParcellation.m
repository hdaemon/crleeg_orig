function imgOut = create_SpatialDistribution(nrrdTemplate,...
                                    varargin)

% CREATE_SPATIALDISTRIBUTIONFROMPARCELLATION
%
% Written By: Damon Hyde
% Last Edited: Oct 13, 2015
% Part of the cnlEEG Project
%

p = inputParser;
p.addRequired('centroid',@(x) numel(x)==3);
p.addParamValue('type','gauss');
p.addParamValue('cutoffdist',30);
p.addParamValue('sigma',[]);
parse(p,varargin{:});

assert(numel(parcel)==1,'Must select a single parcel');

if ~exist('type','var'), type = 'sinc'; end;

allGridPoints = nrrdTemplate.gridSpace.getGridPoints;

imgOut = zeros(prod(nrrdTemplate.sizes));

voxInBrain = find(nrrdTemplate.data>0);
ptsInBrain = allGridPoints(voxInBrain,:);

% voxInParcel = find(nrrdTemplate.data==useParcels);
% voxLocations = allGridPoints(voxInParcel,:);
% 
% centroid = mean(voxLocations,1);

delta = ptsInBrain - repmat(centroid,numel(voxInBrain),1);
dist = sqrt(sum(delta.^2,2));
useVox = dist<p.Results.cutoffdist;

sigma = p.Results.sigma;
switch type
  case 'gauss'    
    if isempty(sigma), sigma = median(dist); end;
    vals = exp(-0.5*dist/sigma);    
  case 'sinc'
    if isempty(sigma), sigma = 3 + 4*rand(1); end;
    vals = sin(dist/sigma)./(dist/sigma);
  otherwise
    error('Unknown image generator function type');
end

imgOut(voxInBrain(useVox)) = vals(Q);

end
                                  
                                