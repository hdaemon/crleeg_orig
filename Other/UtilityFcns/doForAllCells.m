function output = doForAllCells(fcnHandle,inputCells)
% function output = doForAllCells(fcnHandle,inputCells)
%
% A convenient utility function to apply a function handle to all cells in
% a cell array.  All this does is:
%
% for i = 1:length(inputCells)
%  output{i} = fcnHandle(inputCells{i});
% end
%
% Potentially saves a couple of lines of code, and makes things a little
% more elegant.  If this is used as the base function to expand on, it
% should be straightforward to create functions that operate on all
% solutions, all reconstruction types, all datasets, all patients, etc.  As
% long as they're all stored in cell arrays.
%
% Written by: D. Hyde
% January, 2014
%
  
for i = 1:length(inputCells)
  output{i} = fcnHandle(inputCells{i});
end;
   
end