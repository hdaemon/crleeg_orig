function strOut = log2str(val)
  
  if val
    strOut = 'TRUE';
  else
    strOut = 'FALSE';
  end
  
end