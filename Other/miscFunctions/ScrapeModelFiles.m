%%
idxCases = getIdxCases
startingDir = pwd;

for idx = 1:length(idxCases)
  cd('/common/projects2/epilepsysurgicalplanning/processed/');
  
  
  % Get list of model directories
  try
  cd(['Case' idxCases{idx} '/sourceanalysis/models']);
  mydisp(['Changing to Case' idxCases{idx} '/sourceanalysis/models']);
  directories = dir;
  
  % Loop across directories
  for i = 1:length(directories)
    
    currDIR = directories(i);
    
    % Make sure it's a subdirectory
    if ~(strcmp(currDIR.name,'.')||strcmp(currDIR.name,'..'))
      if currDIR.isdir
        
        % Move to subdir
        cd(currDIR.name)
        disp([' Checking ' currDIR.name]);

        if exist('hdEEG_ModelReady.mat','file')
        if exist('./hdEEG_128/LeadField.mat','file')
          disp('   Located finished hdEEG Leadfield');
            Patients{idx}.(currDIR.name).hdEEG = [pwd 'hdEEG_ModelReady'];
        else
          disp('     HDEEG MODEL READY BUT NOT YET BUILT');
        end;
        end
        
        if exist('clinical_ModelReady.mat','file')
          if exist('./clinical/LeadField.mat','file')
            disp('   Located finished clinical Leadfield');
            Patients{idx}.(currDIR.name).clinical = [pwd 'clinical_ModelReady'];          
        else
          disp('  CLINICAL MODEL READY BUT NOT YET BUILT');
          end;
        end
           
        cd ..
      end; % END IF currDIR.isdir
    end; % END IF ~(strcmp(currDIR.name,'.')||strcmp(currDIR.name,'..'))Pati
  end; % END for i = 1:length(directories)
  
  catch
  end;
  
end; % for idx = 1:length(idxCases)

cd(startingDir);