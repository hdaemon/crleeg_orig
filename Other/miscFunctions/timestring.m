function [stringOut] = timestring

t = fix(clock);

if t(4)>12
  t(4) = t(4)-12;
  ampm = 'pm';
else
  ampm = 'am';
end;

stringOut = [num2str(t(4)) ':' num2str(t(5)) ':' num2str(t(6)) ampm ];

return;