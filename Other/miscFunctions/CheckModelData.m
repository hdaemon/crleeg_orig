%%
idxCases = getIdxCases

for idx = 1:length(idxCases)
  cd('/external/crl-rdynaspro5/damon/epilepsysurgicalplanning');    
    cd(['Case' idxCases{idx} ]);
    mydisp(['Changing to Case' idxCases{idx} ]);
    directories = dir;
    
    % Check For Data 
    t1w = false; t2w = false; flair = false; tensors = false;        
    if exist('data_for_analysis','dir')
      cd data_for_analysis
      files = dir;
      pulledData = true;
      for i = 1:length(files)
        if ~isempty(strfind(lower(files(i).name),'t1w'))
          t1w = true; end;
        if ~isempty(strfind(lower(files(i).name),'t2w'))
          t2w = true; end;
        if ~isempty(strfind(lower(files(i).name),'tensors'))
          tensors = true; end;
        if ~isempty(strfind(lower(files(i).name),'flair'))
          flair = true; end;
      end;
      cd ..
    else
      pulledData = false;
    end;
    
    % Check for a 3-compartment segmentation and ICC
    segmentation = false; icc = false; brainseg = false;
    if exist('brainseg','dir')
      cd brainseg
      brainseg = true;
      if exist('segmentation.nrrd','file')
        segmentation = true; end;
      if exist('icc.nrrd','file')
        icc = true; end;              
      cd ..
    else 
      brainseg = false;
    end;
    
    % Check if DTI has been coregistered and resampled yet
    dti = false;
    if exist('dti','dir')
      cd dti
      if exist('clean-warped-tensors.nhdr')
        dti = true; end;
      cd ..
    end;
    
    %check for NMM and IBSR parcellations
    NMM = false; IBSR = false;
    if exist('common-processed/modules','dir')
      cd common-processed/modules;
      pipeline = true;
      if exist('parcellationNMM/ParcellationNMM.nrrd','file');
        NMM = true; end;
      if exist('parcellationIBSR/ParcellationIBSR.nrrd','file');
        IBSR = true; end;
      cd ../..
    else
      pipeline = false;
    end;
    
    % Check that sulci identification has been done
    sulci = false;
   if exist('sulcidetection','dir')
     cd sulcidetection
     if exist('FinalSeg.nrrd','file')
       sulci = true; end;
     cd ..   
   end;
      
   Patient{idx}.caseID = idxCases{idx};
   Patient{idx}.pulledData = pulledData;
   Patient{idx}.t1w = t1w;
   Patient{idx}.t2w = t2w;
   Patient{idx}.tensors = tensors;
   Patient{idx}.flair = flair;
   Patient{idx}.segmentation = segmentation;
   Patient{idx}.icc = icc;
   Patient{idx}.brainseg = brainseg;
   Patient{idx}.dti = dti;
   Patient{idx}.pipeline = pipeline;
   Patient{idx}.NMM = NMM;
   Patient{idx}.IBSR = IBSR;
   Patient{idx}.sulci = sulci;
end;