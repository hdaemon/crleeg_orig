function nrrdOut = sufficientlyConnected(nrrdIn,connectedness)
% Remove insufficiently connected voxels from a binary image
%
% function nrrdOut = sufficientlyConnected(nrrdIn,connectedness)
%
%

Q = unique(nrrdIn.data);
if numel(Q)~=2, error('This filter is designed for binary images'); end;

SpaceSize = nrrdIn.sizes;
imgGrid = cnlGrid(SpaceSize);
[rowIdx, colIdx] = imgGrid.getGridConnectivity(6);

A1 = sparse(rowIdx,colIdx,ones(size(colIdx)),prod(SpaceSize),prod(SpaceSize));

allVox = 1:size(A1,1);
voxInImg = find(nrrdIn.data(:));
voxOutside = setdiff(allVox,voxInImg);

A1(voxOutside,:) = 0;
A1(:,voxOutside) = 0;

toRemove = [];

disp(['Removing voxels that are not at least : ' num2str(connectedness) ' connected']);

nBackground = numel(find(sum(A1,2)==0));

done= false;
numRemoved = 0;
nIter = 0;
while ~done  
  numConnections = sum(A1,2);  
  Q = find(numConnections<connectedness);
  if numel(Q)==numRemoved
    done = true;
  else    
    numRemoved = numel(Q);
    A1(Q,:) = 0;
    A1(:,Q) = 0;
    nIter = nIter+1;
    disp(['Removed: ' num2str(numel(Q)-nBackground) ' voxels after iteration ' num2str(nIter)]);
  end;
  
end

disp(['Found ' num2str(numel(Q)-nBackground) ' voxels in ' num2str(nIter) ' iterations']);  

nrrdOut = clone(nrrdIn);
nrrdOut.data(Q) = 0;

end