classdef cnlElectrodes
  % classdef cnlElectrodes
  %
  %
  % Properties:
  %   Positions     : Location of electrodes in X-Y-Z space
  %   Labels        : String labels for each electrode
  %
  %   FIDPositions  : Location of Fiducials in X-Y-Z space
  %   FIDLabels     : String labels for each fiducial
  %
  %   Nodes         : Index into nodeMRI associated with each electrode location
  %   nodeType      : "Iso" or "Aniso"
  %   nodeMRI       : NRRD file the nodes are identified in
  %
  %   fNameOut      : Filename to save the electrode data in
  %   dir_Model     : Directory to save the electrode data in
  %
  % Dependent Properties:
  %   nElec         : number of electrodes
  %
  % Part of the cnlEEG Project
  % Damon Hyde, 2014.
  
  properties   
    % Basic properties
    Positions % X-Y-Z Position of Electrode Centers
    Labels
    Nodes
    Voxels
    Impedance = 100

    % Fiducial Data to Orient the Electrode Mesh
    FIDPositions 
    FIDLabels

    % Mapping from electrode Positions to voxels/nodes in a particular MRI.
    nodeMRI
    nodeType = 'aniso';
                      
    % Where the computed data is stored
    fNameOut = 'Electrodes.mat';
    dir_Model;
    
  end
  
  properties (Dependent=true)
    nElec
  end 
  
  methods
    
    function obj = cnlElectrodes(Positions,varargin)
      % function obj = cnlElectrodes(Positions,varargin)
      %
      %
      
      if nargin>0
      p = inputParser;
      addRequired(p,'Positions',@(x) size(x,2)==3);
      addParamValue(p,'nodes',[],@(x) isvector(x)||iscell(x));
      addParamValue(p,'labels',[],@(x) iscell(x));
      addParamValue(p,'voxels',[],@(x) iscell(x));
      addParamValue(p,'impedance',[],@(x) iscell(x));
      addParamValue(p,'nrrdMRI',[],@(x) isa(x,'file_NRRD'));
      addParamValue(p,'nodeType','aniso');
      addParamValue(p,'dir_Model','./');
      addParamValue(p,'fNameOut','Electrodes.mat');
      
      parse(p,Positions,varargin{:});
      
      obj.Positions = p.Results.Positions;
      obj.Labels = p.Results.labels;
      obj.Nodes  = p.Results.nodes;
      end;
                                   
    end
    
    function obj = set.Positions(obj,val)
      % function obj = set.Positions(obj,val)
      %
      %
      if numel(val)~=3*size(val,1)
        error('Positions must be X-Y-Z coordinates');
      else
        obj.Positions = val;
      end;
    end
    
    function obj = set.Nodes(obj,val)
      % function obj = set.Nodes(obj,val)
      %
      %
      
      if isempty(val), obj.Nodes = val; return; end;
      test = (isvector(val) || iscell(val));
      assert(test,'Nodes values must be a vector or cell array');
      
      assert(numel(val)==obj.nElec,...
        'Number of nodes must equal number of electrode positions');
      
      obj.Nodes = val;
    end
    
    function obj = set.Labels(obj,val)
      % function obj = set.Labels(obj,val)
      %
      % 
      assert(iscell(val),'Electrode labels must be a cell array');
      assert(numel(val)==obj.nElec,'Number of labels must equal number of electrodes');
            
      obj.Labels = val(:)';      
    end
        
    function nElec = get.nElec(elecObj)
      % function nElec = get.nElec(elecObj)
      %
      % Return number of electrodes as size of first dimension of
      % elecObj.Positions.
      nElec = size(elecObj.Positions,1);
    end
    
  end
  
  
end