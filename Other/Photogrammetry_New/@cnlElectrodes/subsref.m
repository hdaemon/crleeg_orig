function varargout = subsref(obj,s)

switch s(1).type
      case '.'           
         if strcmpi(s(1).subs,'plot3D')
           builtin('subsref',obj,s);
         else
        varargout = {builtin('subsref',obj,s)};
         end;
      case '()'
         if length(s) == 1
            % Implement obj(indices)
            if numel(obj)==1
              % Accessing as obj(a)
              if numel(s.subs)==1
                % If the subsref is a cell array, we're indexing by
                % electrode labels.
                %
                if iscellstr(s.subs{1})||ischar(s.subs{1})
                  % Provided a string or cell array of strings
                  
                  inIdx = s.subs{1};
                  if ~iscell(inIdx), inIdx = {inIdx}; end;
                  
                  outIdx = getIdxFromStringCell(idx);
                                    
                  tmpObj = cnlElectrodes;
                  tmpObj.Positions    = obj.Positions(outIdx,:);
                  tmpObj.Labels       = obj.Labels(outIdx);
                  tmpObj.Nodes        = obj.Nodes(outIdx);
                  tmpObj.nodeType     = obj.nodeType;
                  tmpObj.nodeMRI      = obj.nodeMRI;
                  tmpObj.FIDPositions = obj.FIDPositions;
                  tmpObj.FIDLabels    = obj.FIDLabels;      
                  if numel(obj.Impedance)==1,
                    tmpObj.Impedance = obj.Impedance;
                  else
                    tmpObj.Impedance = obj.Impedance(outIdx);
                  end;
                  
                  
                  varargout = {tmpObj};
%                 elseif ischar(s.subs{1})
%                   % Provided a single character string
%                   outIdx = getIdxFromStringCell({s.subs{1}});
%                    tmpObj = cnlElectrodes;
%                   tmpObj.Positions    = obj.Positions(outIdx,:);
%                   tmpObj.Labels       = obj.Labels(outIdx);
%                   tmpObj.Nodes        = obj.Nodes(outIdx);
%                   tmpObj.nodeType     = obj.nodeType;
%                   tmpObj.nodeMRI      = obj.nodeMRI;
%                   tmpObj.FIDPositions = obj.FIDPositions;
%                   tmpObj.FIDLabels    = obj.FIDLabels;  
%                   if numel(obj.Impedance)==1,
%                     tmpObj.Impedance = obj.Impedance;
%                   else
%                     tmpObj.Impedance = obj.Impedance(outIdx);
%                   end;
%                   varargout = {tmpObj};
                else
                  % Poorly defined indexing. Don't allow it.
                  outIdx = s.subs{1};
                  tmpObj = cnlElectrodes;
                  tmpObj.Positions    = obj.Positions(s.subs{:},:);
                  tmpObj.Labels       = obj.Labels(s.subs{:});
                  tmpObj.Nodes        = obj.Nodes(s.subs{:});
                  tmpObj.nodeType     = obj.nodeType;
                  tmpObj.nodeMRI      = obj.nodeMRI;
                  tmpObj.FIDPositions = obj.FIDPositions;
                  tmpObj.FIDLabels    = obj.FIDLabels;                                    
                  if numel(obj.Impedance)==1,
                    tmpObj.Impedance = obj.Impedance;
                  else
                    tmpObj.Impedance = obj.Impedance(outIdx);
                  end;
                  varargout = {tmpObj};                                                      
                end;
              elseif numel(s.subs)==2     
                % Accessing as obj(a,b)
                if strcmpi(s.subs{1},':')
                varargout = {cnlLabelledData(obj.data(s.subs{:}),...
                  obj.labels(s.subs{2}),'epochstart',obj.epochs_start,...
                  'epochend',obj.epochs_end)};
                else
                  error('To prevent errors in epoch bounds, arbitrary 2D referencing is not permitted');
                end;
              else
                error('Invalid indexing expression');
              end;
            else
              varargout = {obj(s.subs{:})};
            end;              
         elseif length(s) == 2 && strcmp(s(2).type,'.')
            % Implement obj(ind).PropertyName
            if numel(s(1).subs)==1
              tmp = obj.subsref(s(1));
              varargout = {builtin('subsref',tmp,s(2))};
            else
              varargout = {builtin('subsref',obj,s)};
            end;
         elseif length(s) == 3 && strcmp(s(2).type,'.') && strcmp(s(3).type,'()')
            % Implement obj(indices).PropertyName(indices)
            varargout = {builtin('subsref',obj,s)};
         else
            % Use built-in for any other expression
            varargout = {builtin('subsref',obj,s)};
         end
      case '{}'
             
          varargout = {builtin('subsref',obj,s)};         

      otherwise
         error('Not a valid indexing expression')
   end

   function outIdx = getIdxFromStringCell(cellIn)
     outIdx = zeros(1,numel(cellIn));
     for idx = 1:numel(outIdx)
       tmp = find(strcmp(cellIn{idx},obj.Labels));
       assert(numel(tmp)==1,'Invalid Index Labels');
       outIdx(idx) = tmp;
     end
   end
   
end


