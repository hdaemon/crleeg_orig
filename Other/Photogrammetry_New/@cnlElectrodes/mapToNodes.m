function [elecOut] = mapToNodes(elecIn,nrrdIn,mapType,pushToSurf)
% function [elecOut] = mapToNodes(elecIn,nrrdIn,mapType)
%
% Inputs:
%   elecIn    : cnlElectrodes object
%   nrrdIn    : file_NRRD object
%  mapType    : either 'iso' or 'aniso'
%  pushToSurf : Flag to determine whether electrodes should be moved to the
%                 scalp surface.
%
% Takes the values in elecIn.Locations and identifies the corresponding
% nodes in nrrdIn, and stores them in elecOut.Nodes.
%


% Default to anisotropic node mapping
if ~exist('mapType','var'), 
  mydisp('Defaulting to anisotropic node mapping');
  mapType = 'aniso'; 
end;

% By default, push electrodes to surface
if ~exist('pushToSurf','var'), 
  mydisp('Defaulting to push-to-surface mode');
  pushToSurf = true; 
end;

% Get the grid that we're mapping onto and the list of nodes that are
% inside and outside the volume.
%
switch lower(mapType)
  case 'aniso'
    FDGrid = getAlternateGrid(nrrdIn.gridSpace);
    NodesInside = nrrdIn.gridSpace.getNodesFromCells(nrrdIn.nonZeroVoxels);
    NodesOutside = nrrdIn.gridSpace.getNodesFromCells(nrrdIn.zeroVoxels);
    
    % Make sure all nodes at the boundary of the space are in the Outside
    % list
    altGrid = nrrdIn.gridSpace.getAlternateGrid;
    tmp = ones(altGrid.sizes);
    tmp(2:end-1,2:end-1,2:end-1) = 0;
    AlsoOutside = find(tmp);
    
    NodesOutside = [NodesOutside(:); AlsoOutside(:)];
    NodesOutside = unique(NodesOutside);        
  case 'iso'
    %error('You shouldn''t be using an isotropic model.');
    FDGrid = nrrdIn.gridSpace;
    NodesInside = nrrdIn.nonZeroVoxels;
    NodesOutside = nrrdIn.zeroVoxels;
  otherwise
    error('Unknown mapping type.  Should be either iso or aniso');
end

if pushToSurf  
  % Move Electrodes inside the head to the surface;
  elecNodes = getNearestNodes(FDGrid,NodesOutside,elecIn.Positions);
  
  % Get New Locations for Everything.
  Pts = FDGrid.getGridPoints;
  Pts = Pts(elecNodes,:);
  
  % Move Electrodes outside the head to the surface
  elecNodes = getNearestNodes(FDGrid,NodesInside,Pts);
  
else
  elecNodes = getNearestNodes(FDGrid,NodesInside,elecIn.Positions);
end;

% % Get new physical locations
% [x,y,z] = ind2sub(FDGrid.sizes,elecNodes);
% XYZ = [x(:) y(:) z(:)]';
% newLoc = (FDGrid.directions*XYZ)' + repmat(FDGrid.origin,size(XYZ,2),1);

% Why not just do this?
newLoc = FDGrid.getGridPoints(elecNodes);

elecOut = elecIn;
elecOut.Nodes = elecNodes;
elecOut.Positions = newLoc;
elecOut.nodeMRI = nrrdIn;

end
