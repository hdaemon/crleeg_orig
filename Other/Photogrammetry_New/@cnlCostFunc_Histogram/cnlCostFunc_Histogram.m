classdef cnlCostFunc_Histogram 
  
  properties
    mode = 'global';
    radius = 10;
    step = 1;
    interpolation = 'mexpartvol';
    binning = 32;
    divtype = 'kldiv';
  end
  
  properties (Dependent=true)
    useGlobal
    useLocalSym
    useLocalNet
    useBkgCon
  end
  
  properties (Access=private)
    data
    sampling
    X
    Y
    Z
    indout
    otsu_thresh
    sph
    xbins
    Href
    Psym
    Pnet
    Wsym
    Wnet
  end
  
  methods
    
    function obj = cnlCostFunc_Histogram(varargin)
      % function obj = CostFunc_Histogram(varargin)
      %
      %
      if nargin>0
        p = inputParser;
        addParamValue(p,'mode',[]);
        addParamValue(p,'radius',[]);
        addParamValue(p,'step',[]);
        addParamValue(p,'interpolation',[]);
        addParamValue(p,'binning',[]);
        addParamValue(p,'divtype',[]);
        addParamValue(p,'Psym',[]);
        addParamValue(p,'Pnet',[]);
        addParamValue(p,'Wsym',[]);
        addParamValue(p,'Wnet',[]);
        addParamValue(p,'otsu_thresh',[]);
        addParamValue(p,'sampling',[]);
        parse(p,varargin{:});
        
        fldNames = fieldnames(p.Results);
        for i = 1:length(fldNames)
          if ~ismember(fldNames{i},p.UsingDefaults)
            obj.(fldNames{i}) = p.Results.(fldNames{i});
          end
        end
        
      end
    end
    
    function out = get.useGlobal(obj)
      out = ~isempty(strfind(obj.mode,'global'));
    end;
    
    function out = get.useLocalSym(obj)
      out = ~isempty(strfind(obj.mode,'localsym'));
    end;
    
    function out = get.useLocalNet(obj)
      out = ~isempty(strfind(obj.mode,'localnet'));
    end;
    
    function out = get.useBkgCon(obj)
      out = ~isempty(strfind(obj.mode,'bkgcon'));
    end;
    
    
  end
  
end