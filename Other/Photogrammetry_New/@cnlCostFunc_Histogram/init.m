function [objHCF] = init(objHCF,nrrdMRI)
% function [objHCF] = init(objHCF)
%
%

if ~exist('nrrdMRI','var')||~isa(nrrdMRI,'file_NRRD')
  error('Error: second input needs to be a file_NRRD');
end

r       = objHCF.radius;
dr      = objHCF.step;
intp    = objHCF.interpolation;
binning = objHCF.binning;
data    = nrrdMRI.data;

objHCF.data = data;
[objHCF.X, objHCF.Y, objHCF.Z] = cnlPhotoGram.getXYZFromMRI(nrrdMRI);

% Check interpolation type
validInterp = {'cubic' 'linear' 'partvol' 'mexpartvol'};
try
  validatestring(intp,validInterp);
catch
  warning(['Unknown interpolation type' ' "' intp '" ' ...
    'Using linear interpolation instead']);
  intp = 'linear';
end

switch intp
  case {'nearest','linear','cubic'},
    disp('We''re getting here. Is this changing things?');
    [ X Y Z ] = meshgrid( objHCF.Y, objHCF.X, objHCF.Z);
    objHCF.X = X; objHCF.Y = Y; objHCF.Z = Z;
    clear X Y Z
    % case 'partvol', maPartVolInterp( 'init', X, Y, Z, data ); % init partial volume interpolation class
  case 'mexpartvol'
  otherwise error(['Error: "',intp,'" not recognized as a valid interpolation method!'])
end

% compute equidistant grid in the shape of a sphere
% rs = -r : dr : r;
nsmp = ceil(2*r/dr);
if ceil(nsmp/2 - floor(nsmp/2))==0, nsmp = nsmp + 1; end
rs = linspace(-r, r, nsmp);
rs2 = rs.^2;
r2 = r^2;
sph = zeros(length(rs)^3,3);
i = 1;
for x = 1 : length(rs)
  for y = 1 : length(rs)
    for z = 1 : length(rs)
      if r2>=(rs2(x)+rs2(y)+rs2(z))
        sph(i,:) = [rs(x),rs(y),rs(z)];
        i = i + 1;
      end
    end
  end
end

objHCF.sph = sph(1:i-1,:); % save only points that the sphere contains
objHCF.sph = unique(sph,'rows'); % take out center point, if there are more
objHCF.xbins = linspace(min(data(:)), max(data(:)), binning); % get bin spacing
objHCF.Href = histc( data(:), objHCF.xbins );

% get mode of operation
%mode = objHCF.hcfopt.mode;

if objHCF.useGlobal || objHCF.useBkgCon,
  % get global histogramm
  if ~isempty(objHCF.otsu_thresh)
    indout = find(objHCF.xbins<=objHCF.otsu_thresh);
    valout = sum(objHCF.Href(indout));
    valin = sum(objHCF.Href(find(objHCF.xbins>objHCF.otsu_thresh)));
    quot = valin / valout;
    objHCF.Href(indout) = quot * objHCF.Href(indout);
    objHCF.indout = indout;
    % a = objHCF.otsu_thresh - objHCF.xbins(indout);
    % b = objHCF.xbins(indout+1) - objHCF.otsu_thresh;
  else
    objHCF.Href(1) = sum(objHCF.Href(2:end)); % normalize background
  end
  objHCF.Href = objHCF.Href + 1; % global image histogram
  objHCF.Href = objHCF.Href ./ sum(objHCF.Href); % normalize histogram sums to 1
  objHCF.Href = reshape(objHCF.Href,1,numel(objHCF.xbins));
end

if objHCF.useLocalSym,
  % get point pairs
  if isempty(objHCF.Psym), error('Error: missing "Psym" data!'), end
  if isempty(objHCF.Wsym), error('Error: missing "Wsym" data!'), end
  % stretch & normalize weights
  if isempty(objHCF.Wsym), objHCF.Wsym = ones(size(objHCF.Psym,1),1); end
  % objHCF.Wsym = exp(-objHCF.Wsym);
  objHCF.Wsym = -objHCF.Wsym + max(objHCF.Wsym);
  objHCF.Wsym = objHCF.Wsym / sum(objHCF.Wsym);
end

if objHCF.useLocalNet,
  % get point pairs
  if isempty(objHCF.Psym), error('Error: missing "Pnet" data!'), end
  if isempty(objHCF.Wsym), error('Error: missing "Wnet" data!'), end
  % stretch & normalize weights
  if isempty(objHCF.Wnet), objHCF.Wnet = ones(size(objHCF.Pnet,1),1); end
  % objHCF.Wnet = exp(-objHCF.Wnet);
  objHCF.Wnet = -objHCF.Wnet + max(objHCF.Wnet);
  objHCF.Wnet = objHCF.Wnet / sum(objHCF.Wnet);
end


end