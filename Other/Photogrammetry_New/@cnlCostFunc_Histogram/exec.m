function KLsum = exec(objHCF,points)
% function H = exec(objHCF)
%
%

if nargin < 2, error('Error: missing input points!'); end

% get type of KL measure
kltype = objHCF.divtype;

% compute & normalize histograms
bplot = false; % dont plot by default

% check input arguments
if ~exist('points','var'), error('Error: Missing input points!'); end

intp = objHCF.interpolation;
[npoints,~] = size(points); % get number of points
H = zeros(npoints,length(objHCF.xbins)); % get memory for histograms
Hpoints = zeros(size(objHCF.sph)); % get memory for point locations

switch intp
  % case 'partvol', % partial volume interpolation (maPartVolInterp)
  %    % for i = 1 : npoints
  %    for i = drange(1 : npoints)
  %        Hpoints(:,1) = objHCF.sph(:,1) + points(i,1);
  %        Hpoints(:,2) = objHCF.sph(:,2) + points(i,2);
  %        Hpoints(:,3) = objHCF.sph(:,3) + points(i,3);
  %        H(i,:) = maPartVolInterp( 'exec', Hpoints, objHCF.xbins ); % get histogram
  %        if bplot > 0, plotHistogramm( H(i,:), objHCF.xbins, i ); end
  %    end
  case 'mexpartvol', % partial volume interpolation (mexPartVolInterp)
    for i = 1 : npoints
      %   for i = drange( 1 , npoints)
      Hpoints(:,1) = objHCF.sph(:,1) + points(i,1);
      Hpoints(:,2) = objHCF.sph(:,2) + points(i,2);
      Hpoints(:,3) = objHCF.sph(:,3) + points(i,3);
      H(i,:) = cnlPhotoGram.mexPartVolInterp( Hpoints, objHCF.data, objHCF.sampling, objHCF.X, objHCF.Y, objHCF.Z, objHCF.xbins ); % get histogram
      if bplot > 0, plotHistogramm( H(i,:), objHCF.xbins, i ); end
    end
  case {'nearest','cubic'}, % nearest, linear or cubic interpolation (interp3)
    for i = 1 : npoints
      %for i = drange( 1 , npoints)
      Hpoints(:,1) = objHCF.sph(:,1) + points(i,1);
      Hpoints(:,2) = objHCF.sph(:,2) + points(i,2);
      Hpoints(:,3) = objHCF.sph(:,3) + points(i,3);
      Hvals = interp3(objHCF.X,objHCF.Y,objHCF.Z,objHCF.data,Hpoints(:,2),Hpoints(:,1),Hpoints(:,3),intp);
      H(i,:) = histc( Hvals, objHCF.xbins ); % get histogram
      if bplot > 0, plotHistogramm( H(i,:), objHCF.xbins, i ); end
    end
  case 'linear',
    for i = 1 : npoints
      %for i = drange( 1 , npoints)
      Hpoints(:,1) = objHCF.sph(:,1) + points(i,1);
      Hpoints(:,2) = objHCF.sph(:,2) + points(i,2);
      Hpoints(:,3) = objHCF.sph(:,3) + points(i,3);
      Hvals = trilinear([Hpoints(:,2),Hpoints(:,1),Hpoints(:,3)],objHCF.X,objHCF.Y,objHCF.Z);
      H(i,:) = histc( Hvals, objHCF.xbins ); % get histogram
      if bplot > 0, plotHistogramm( H(i,:), objHCF.xbins, i ); end
    end
  otherwise, error(['Error: "',intp,'" not recognized as a valid interpolation method.'])
end

H = H + 1e-2 * ones(size(H));
for j = 1 : size(points,1)
  H(j,:) = H(j,:) ./ sum(H(j,:)); % normalize sum to 1 (!!!)
end

% init Kullback-Leibner distance
KLsum = 0;

% distance to the global reference histogramm
if objHCF.useGlobal,
  for j = 1 : size(points,1)
    KLsum = KLsum + cnlPhotoGram.kldiv(objHCF.xbins,H(j,:),objHCF.Href,kltype);
  end
  KLsum = KLsum / size(points,1); % normalization
end

% sum of KLdivs between single L-R histogramms
if objHCF.useLocalSym,
  for j = 1 : size(objHCF.Psym,1)
    Hleft = H(objHCF.Psym(j,1),:);
    Hright = H(objHCF.Psym(j,2),:);
    KLsum = KLsum + objHCF.Wsym(j) * cnlPhotoGram.kldiv(objHCF.xbins,Hleft,Hright,kltype);
  end
end

% sum of KLdivs between neighbouring histogramms
if objHCF.useLocalNet,
  for j = 1 : size(objHCF.Pnet,1)
    Hleft = H(objHCF.Pnet(j,1),:);
    Hright = H(objHCF.Pnet(j,2),:);
    KLsum = KLsum + objHCF.Wnet(j) * cnlPhotoGram.kldiv(objHCF.xbins,Hleft,Hright,kltype);
  end
end

% add background constraint
if objHCF.useBkgCon,
  %             if isfield(objHCF,'indout'),
  %                 partsum = sum(objHCF.Href(objHCF.indout));
  %                 for j = 1 : size(points,1)
  %                     KLsum = KLsum + kldiv(objHCF.xbins(objHCF.indout),...
  %                                             H(j,objHCF.indout)/sum(H(j,objHCF.indout)),...
  %                                             objHCF.Href(objHCF.indout)/partsum,...
  %                                             kltype);
  %                 end
  %                 KLsum = KLsum / size(points,1); % normalization
  %             else
  lambda = log(2)/0.1; % penalty on 10% difference is 1/3 of the measure
  KLsum = KLsum + (sum(exp(lambda * abs(H(:,1)-objHCF.Href(1))))-size(H,1))/size(H,1);
  %             end
end

if isnan(KLsum)
  keyboard
end;

end