

function [ValsOut] = get_CirclePoints(CircleDef,Percentages)

Angles = CircleDef.TotalAngle*Percentages;

for i = 1:length(Angles)
  theta = Angles(i);
  mat = [cosd(theta) -sind(theta) ; sind(theta) cosd(theta)];
  val = CircleDef.Radius*mat*CircleDef.Vec;
  ValsTmp(:,i) = (val+CircleDef.Center);  
end;
ValsOut = CircleDef.ShiftInv*[ValsTmp ; CircleDef.Offset*ones(1,size(ValsTmp,2))];

return;