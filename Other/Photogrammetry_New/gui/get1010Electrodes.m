%    162   132    92
%     12   130    92
%     88   230    80
%     88    23   130
%     88   192   221

function Electrodes = get1010Electrodes(Nas,Inion,Cz,Pa_Right, Pa_Left);

Electrodes.Labels = {'Nz' 'Iz' 'Cz' 'T10' 'T9'};
Electrodes.Pos    = [Nas(:)' ; Inion(:)' ; Cz(:)' ; Pa_Right(:)' ; Pa_Left(:)'];

%% Compute basis for Nasion-Inion Circle


for i = 1:10
Electrodes = get_CirclePoints({'Nz' 'Cz' 'Iz'} , Electrodes, {'Cz' },[ 0.5 ]);

Electrodes = get_CirclePoints({'Nz' 'T9' 'Iz'} , Electrodes, {'T9' },[ 0.5 ]);

Electrodes = get_CirclePoints({'Nz' 'T10' 'Iz'}, Electrodes, {'T10'},[ 0.5 ]);

%Electrodes = get_CirclePoints({'T9' 'Cz' 'T10'}, Electrodes, {'T9' 'Cz' '10'},[ -90 0 90 ],'angles');

end;

%Electrodes.Labels = {'Nz' 'Iz' 'Cz' 'T9' 'T10'};
%Electrodes.Pos    = [Nas(:)' ; Inion(:)' ; Cz(:)' ; Pa_Right(:)' ; Pa_Left(:)'];

%% Compute basis for Nasion-Inion Circle
Electrodes = get_CirclePoints({'Nz' 'Cz' 'Iz'},Electrodes, ...
              {'Fpz' 'AFz' 'Fz' 'FCz' 'CPz' 'Pz' 'POz' 'Oz'},...
              [ 0.1   0.2   0.3  0.4  0.6   0.7  0.8   0.9]);

Electrodes = get_CirclePoints({'T9' 'Cz' 'T10'},Electrodes,...
              {'T7' 'C5' 'C3' 'C1' 'C2' 'C4' 'C6' 'T8'},...
              [ 0.1  0.2 0.3  0.4  0.6  0.7  0.8  0.9]);
            
Electrodes = get_CirclePoints({'Fpz' 'T7' 'Oz'},Electrodes,...
             {'Fp1' 'AF7' 'F7' 'FT7' 'TP7' 'P7' 'PO7' 'O1'},...
             [ 0.1  0.2   0.3   0.4   0.6  0.7   0.8  0.9]);
            
Electrodes = get_CirclePoints({'Fpz' 'T8' 'Oz'},Electrodes,...
             {'Fp2' 'AF8' 'F8' 'FT8' 'TP8' 'P8' 'PO8' 'O2'},....
             [ 0.1   0.2   0.3  0.4   0.6   0.7  0.8   0.9]);
           
Electrodes = get_CirclePoints({'Fp1' 'C3' 'O1'},Electrodes,...
             {'AF3' 'F3' 'FC3' 'CP3' 'P3' 'PO3'},....
             [0.125 0.25 0.375 0.625 0.75 0.875]);
           
Electrodes = get_CirclePoints({'Fp1' 'C5' 'O1'},Electrodes,...
             {'F5' 'FC5' 'CP5' 'P5' },....
             [0.25 0.375 0.625 0.75 ]);           

Electrodes = get_CirclePoints({'Fp2' 'C4' 'O2'},Electrodes,...
             {'AF4' 'F4' 'FC4' 'CP4' 'P4' 'PO2'},....
             [0.125 0.25 0.375 0.625 0.75 0.875]);
           
Electrodes = get_CirclePoints({'Fp2' 'C6' 'O2'},Electrodes,...
             { 'F6' 'FC6' 'CP6' 'P6' },....
             [ 0.25 0.375 0.625 0.75 ]);
           
Electrodes = get_CirclePoints({'Fpz' 'C1' 'Oz'},Electrodes,...
             {'F1' 'FC1' 'CP1' 'P1'},...
             [0.25 0.375 0.625 0.75]);
           
Electrodes = get_CirclePoints({'Fpz' 'C2' 'Oz'},Electrodes,...
             {'F2' 'FC2' 'CP2' 'P2'},...
             [0.25 0.375 0.625 0.75]);           
           
Electrodes = get_CirclePoints({'Nz' 'T9' 'Iz'},Electrodes,...
             {'F9' 'FT9' 'TP9' 'P9'},...
             [0.3   0.4   0.6   0.7]);
           
Electrodes = get_CirclePoints({'Nz' 'T10' 'Iz'},Electrodes,...
             {'F10' 'FT10' 'TP10' 'P10'},...
             [0.3   0.4   0.6   0.7]);           
           
           
           
           



return;



function [StructOut] = get_DefiningCircle(Point1,Point2,Point3)
  
  % Make it a column
  Point1 = Point1(:);
  Point2 = Point2(:);
  Point3 = Point3(:);
  
  % get vectors
  try
  Vec1 = Point2 - Point1; L1 = norm(Vec1); Vec1 = Vec1/norm(Vec1);
  Vec2 = Point3 - Point1; L2 = norm(Vec2); Vec2 = Vec2/norm(Vec2);
  Perp = cross(Vec1,Vec2); Perp = Perp/norm(Perp);
  catch
    keyboard;
  end;
  
  % Get Midline Points
  MidPt1 = Point1 + 0.5*L1*Vec1;
  MidPt2 = Point1 + 0.5*L2*Vec2;
  
  % Get Normals to MidPoint
  Nrm1 = Vec2 - (Vec2'*Vec1)*Vec1; Nrm1 = Nrm1/norm(Nrm1);
  Nrm2 = Vec1 - (Vec2'*Vec1)*Vec2; Nrm2 = Nrm2/norm(Nrm2);
  
 
  
  
  % Get locations of points in planar coordinate system
  matShift = [Vec1' ; Nrm1' ; Perp' ];
  Pt1 = matShift*Point1; Pt1 = Pt1(1:2);
  Pt2 = matShift*Point2; Pt2 = Pt2(1:2);
  Pt3 = matShift*Point3; Pt3 = Pt3(1:2);
  Mp1 = matShift*MidPt1; Mp1 = Mp1(1:2);
  Mp2 = matShift*MidPt2; Mp2 = Mp2(1:2);
  N1 = matShift*Nrm1; N1 = N1(1:2);
  N2 = matShift*Nrm2; N2 = N2(1:2);
   
  % Get Center
  mat = [-N1 N2];
  tmp = Mp1 - Mp2;
  tmp2 = inv(mat)*tmp;
  Center = Mp1 + N1*tmp2(1);
  
  Pts = [Pt1 Pt2 Pt3 Mp1 Mp2];
  Nrms = [N1 N2];
  
%   figure;
%   plot(Pts(1,:),Pts(2,:),'rx'); hold on;
%   plot(Pt1(1),Pt1(2),'gx');
%   plot(Pt2(1),Pt2(2),'cx');
%   plot([Pts(1,1:3) Pts(1,1)],[Pts(2,1:3) Pts(2,1)]);
%   quiver([Mp1(1) Mp2(1)],[Mp1(2) Mp2(2)],[N1(1) N2(1)],[N1(2) N2(2)]);
%   %quiver([Pts(1,1) Pts(1,1)],[Pts(2,1) Pts(2,1)],[V1(1) V2(1)],[V1(2) V2(2)])
%   plot(Center(1),Center(2),'gx'); 
%   axis equal
  
  Vec1 = Pt1 - Center;  
  Vec2 = Pt2 - Center; 
  Vec3 = Pt3 - Center;
  
  ALen1 = acosd((Vec1'*Vec2)/(norm(Vec1)*norm(Vec2)));
  ALen2 = acosd((Vec2'*Vec3)/(norm(Vec2)*norm(Vec3)));
   
  StructOut.ShiftInv = inv(matShift);
  StructOut.Center = Center;
  StructOut.Radius = norm(Vec1);
  StructOut.Vec = Vec1/norm(Vec1);
  StructOut.TotalAngle = ALen1 + ALen2;  
  StructOut.Offset = matShift(3,:)*Point1;
  
return

function [Electrodes] = get_CirclePoints(CircleDef,Electrodes,Labels,Values,ValueType)

if ~exist('ValueType'), ValueType = 'percentages'; end;

if length(Labels)~=length(Values)
  error('Need a label for every point to be solved for!!');
end;

Pt1 = find(strcmp(CircleDef{1},Electrodes.Labels));
Pt2 = find(strcmp(CircleDef{2},Electrodes.Labels));
Pt3 = find(strcmp(CircleDef{3},Electrodes.Labels));

if (length(Pt1)~=1)|(length(Pt2)~=1)|(length(Pt3)~=1)
  keyboard;
end;

Circle = get_DefiningCircle(Electrodes.Pos(Pt1,:),Electrodes.Pos(Pt2,:),Electrodes.Pos(Pt3,:));

if strcmpi(ValueType,'percentages')
  Angles = Circle.TotalAngle*Values;
elseif strcmpi(ValueType,'angles')
  Angles = Circle.TotalAngle/2 + Values;
end;

for i = 1:length(Angles)
  theta = Angles(i);
  mat = [cosd(theta) -sind(theta) ; sind(theta) cosd(theta)];
  val = Circle.Radius*mat*Circle.Vec;
  ValsTmp(:,i) = (val+Circle.Center);  
end;
ValsOut = Circle.ShiftInv*[ValsTmp ; Circle.Offset*ones(1,size(ValsTmp,2))];
ValsOut = ValsOut';

for i = 1:length(Labels)
  idx = find(strcmp(Labels{i},Electrodes.Labels));
  if length(idx)==1
    Electrodes.Pos(idx,:) = ValsOut(i,:);
  elseif isempty(idx)   
    Electrodes.Labels{end+1} = Labels{i};
    Electrodes.Pos(end+1,:) = ValsOut(i,:);
  else
    disp('Shouldn''t be getting here');
    keyboard;    
  end;
end;

return;