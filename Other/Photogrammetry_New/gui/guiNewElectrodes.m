function [ElectrodesOut FIDPos] = guiNewElectrodes(nrrdMRI,nrrdSkin)

hFig = figure;

% Set up MRI Image Axes
nrrdViewObj = nrrdMRI.view('parent',hFig);
set(nrrdViewObj.axes,'ButtonDownFcn',@pickMRIPoint)
set(nrrdViewObj.axes,'Units','Pixels');
%set(nrrdViewObj.imageLabel,'Units','Pixels');
nrrdViewObj.units = 'pixels';

elecPanel = uipanel('Title','Electrode Identification','Parent',hFig, ...
                        'Units','pixels','Position',[550 5 200 625]);

fidSel = uicontrol('Style','popupmenu','Parent',elecPanel, ...
    'String', {'Nas', 'Cz', 'Inion', 'RPA', 'LPA'},...
    'Value',1,'Position', [5 550 190 30]);
fidSelLabel = uicontrol('Style','text','Parent',elecPanel,...
    'String','Set Fiducial:','Position',[5 580 190 20]);

fidPosTxt{1} = uicontrol('Style','text','Parent',elecPanel,...
    'String', ['Nas:   [ ? ? ? ]'],'Position',[5 500 190 35]);
fidPosTxt{2} = uicontrol('Style','text','Parent',elecPanel,...
    'String', ['Cz:    [ ? ? ? ]'],'Position',[5 460 190 35]);
fidPosTxt{3} = uicontrol('Style','text','Parent',elecPanel,...
    'String', ['Inion: [ ? ? ? ]'],'Position',[5 420 190 35]);
fidPosTxt{4} = uicontrol('Style','text','Parent',elecPanel,...
    'String', ['RPA:   [ ? ? ? ]'],'Position',[5 380 190 35]);
fidPosTxt{5} = uicontrol('Style','text','Parent',elecPanel,...
    'String', ['LPA:   [ ? ? ? ]'],'Position',[5 340 190 35]);

ElectrodesOut = cnlElectrodes;
ElectrodesOut.FIDLabels = {'Nas' 'Cz' 'Inion' 'RPA' 'LPA'};
ElectrodesOut.FIDPositions = zeros(5,3);
updateFidPosTxt;

FIDPos.Positions = [];
FIDPos.Labels = ElectrodesOut.FIDLabels;

updateElecButton = uicontrol('Style','pushbutton','Parent',elecPanel,...
    'String','Update Electrodes','Position',[5 150 190 190],...
    'CallBack',@updateElectrodes);

exitButton = uicontrol('Style','pushbutton','Parent',elecPanel,...
    'String','Save and Exit','Position',[5 5 190 140],...
    'CallBack',@closeFigure);


%% Set up surface rendering panel
surfPanel = uipanel('Title','Skin Surface Rendering','Parent',hFig, ...
                        'Units','pixels', 'Position', [755 5 625 625]);                
                    
currPos = get(hFig,'Position');  
set(hFig,'Position',[currPos(1:2) 1400 650]);

width = 550;
  surfAxes = axes('Parent',surfPanel,'Units','Pixels',...
                        'Position',[312.5-width/2 312.5-width/2 width width]);
  set(surfAxes,'Units','normalized');
                    
%% Set Up Skin Surface 
if exist('nrrdSkin','var')&isa(nrrdSkin,'file_NRRD')     
  skinSurf = ExtractIsosurface(nrrdSkin);  
else
  skinSurf = []; 
end
updateSurfAxes;

    
    function closeFigure(varargin)        
        delete(hFig);
    end

    function updateElectrodes(varargin)
      if any(sum(ElectrodesOut.FIDPositions,2)==0)
          warning('Finish setting fiducials first');
          return;
      end
      
      foo = ElectrodesOut.FIDPositions;
      tmp = get1010Electrodes(foo(1,:),foo(3,:),foo(2,:),foo(4,:),foo(5,:));
      
      ElectrodesOut.Positions = tmp.Pos;
      ElectrodesOut.Labels = tmp.Labels;
           
      updateSurfAxes;
        
    end


    function updateSurfAxes
        % function updateSurfAxes
        %
        % Clear surface axes and redisplay.
        set(hFig,'CurrentAxes',surfAxes);
        cla;
        if ~isempty(skinSurf)
            ViewSurface(hFig,skinSurf,true);
        end;
        ElectrodesOut.plot3D('figRef',hFig);        
    end

    function updateFidPosTxt
        % function updateFidPosTxt
        %
        % Update text using fiducial locations from
        % ElectrodeOut.FIDPositions
        for idx = 1:5
        preText = get(fidPosTxt{idx},'String');
        preText = preText(1:7);
        posString = ['[ ' num2str(ElectrodesOut.FIDPositions(idx,:),4) ' ]'];            
            set(fidPosTxt{idx},'String',[preText posString]);        
        end;
    end

    function pickMRIPoint(h,varargin)
        % function pickMRIPoint(h,varargin)
        %
        % Callback function for MRI viewing axis.  Determine the location
        % of the point clicked on in X-Y-Z space and assign that value to
        % the location of the currently selected fiducial.
        mydisp('Calling pickMRIpoint');
        %axisSelect = get(nrrdViewObj.axSel,'Value');
        %axisSelect = 
        %axisSelect = [axisSelect{1} axisSelect{2} axisSelect{3}];
        currAxis = nrrdViewObj.volume.axis;
        if numel(currAxis)>1, keyboard; end;
                        
        pos = get(get(h,'Parent'),'CurrentPoint');
        a = pos(1,1);
        b = pos(1,2);        
        %slice = round(get(nrrdViewObj.slice.currSlice,'Value'));
        slice = nrrdViewObj.volume.slice;
        
        switch currAxis
            case 1
                x = slice;
                y = a; 
                z = b; %nrrdSkin.sizes(3)-b;
            case 2
                x = a;
                y = slice;
                z = b; %nrrdSkin.sizes(3)-b;
            case 3
                x = a;
                y = nrrdSkin.sizes(2)-b;
                z = slice;
        end
        
        mydisp(['Electrode at: X:' num2str(x) ' Y:' num2str(y) ' Z:' num2str(z)]);
        
        %nrrdMRI.spacedirections
        %nrrdMRI.spaceorigin
        
        loc = (nrrdMRI.spacedirections*[x y z]')' + nrrdMRI.spaceorigin;

        currFID = get(fidSel,'Value');        
        FIDPos.Positions(currFID,:) = [x y z];
        ElectrodesOut.FIDPositions(currFID,:) = loc;
        updateFidPosTxt;
        updateSurfAxes;
    end
  
  
  waitfor(hFig);

end