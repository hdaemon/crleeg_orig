function varargout = guiPlaceElectrodes(varargin)
% GUIPLACEELECTRODES M-file for guiPlaceElectrodes.fig
%      GUIPLACEELECTRODES, by itself, creates a new GUIPLACEELECTRODES or raises the existing
%      singleton*.
%
%      H = GUIPLACEELECTRODES returns the handle to a new GUIPLACEELECTRODES or the handle to
%      the existing singleton*.
%
%      GUIPLACEELECTRODES('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in GUIPLACEELECTRODES.M with the given input arguments.
%
%      GUIPLACEELECTRODES('Property','Value',...) creates a new GUIPLACEELECTRODES or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before guiPlaceElectrodes_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to guiPlaceElectrodes_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help guiPlaceElectrodes

% Last Modified by GUIDE v2.5 30-Dec-2008 13:25:01

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @guiPlaceElectrodes_OpeningFcn, ...
                   'gui_OutputFcn',  @guiPlaceElectrodes_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

end

% --- Executes just before guiPlaceElectrodes is made visible.
function guiPlaceElectrodes_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to guiPlaceElectrodes (see VARARGIN)

% Choose default command line output for guiPlaceElectrodes
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes guiPlaceElectrodes wait for user response (see UIRESUME)
% uiwait(handles.figure1);

end

% --- Outputs from this function are returned to the command line.
function varargout = guiPlaceElectrodes_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

end


% --- Executes on slider movement.
function zSlider_Callback(hObject, eventdata, handles)
% hObject    handle to zSlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

zindex = round(get(handles.zSlider,'Value'));
set(handles.txtzSlider,'String',['Z Slice #: ',num2str(zindex)]);
handles.usrdata.sliderpos(3) = zindex;
handles.usrdata.sliderchg = 'Z';

UpdateMRIAxes( handles );

% save handles structure
guidata(hObject,handles);

end

% --- Executes during object creation, after setting all properties.
function zSlider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to zSlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

end

% --- Executes on slider movement.
function ySlider_Callback(hObject, eventdata, handles)
% hObject    handle to ySlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

yindex = round(get(handles.ySlider,'Value'));
set(handles.txtySlider,'String',['Y Slice #: ',num2str(yindex)]);
handles.usrdata.sliderpos(2) = yindex;
handles.usrdata.sliderchg = 'Y';

UpdateMRIAxes( handles );

% save handles structure
guidata(hObject,handles);

end

% --- Executes during object creation, after setting all properties.
function ySlider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ySlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

end


% --- Executes on slider movement.
function xSlider_Callback(hObject, eventdata, handles)
% hObject    handle to xSlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

xindex = round(get(handles.xSlider,'Value'));
set(handles.txtxSlider,'String',['X Slice #: ',num2str(xindex)]);
handles.usrdata.sliderpos(1) = xindex;
handles.usrdata.sliderchg = 'X';

UpdateMRIAxes( handles );

% save handles structure
guidata(hObject,handles);

end

% --- Executes during object creation, after setting all properties.
function xSlider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to xSlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

end

% --- Executes during object creation, after setting all properties.
function MRIAxes_CreateFcn(hObject, eventdata, handles)
% hObject    handle to MRIAxes (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: place code in OpeningFcn to populate MRIAxes

end

function MRIFileNameInput_Callback(hObject, eventdata, handles)
% hObject    handle to MRIFileNameInput (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of MRIFileNameInput as text
%        str2double(get(hObject,'String')) returns contents of MRIFileNameInput as a double

end

% --- Executes during object creation, after setting all properties.
function MRIFileNameInput_CreateFcn(hObject, eventdata, handles)
% hObject    handle to MRIFileNameInput (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

end

function FilenameSkinSurface_Callback(hObject, eventdata, handles)
% hObject    handle to FilenameSkinSurface (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of FilenameSkinSurface as text
%        str2double(get(hObject,'String')) returns contents of FilenameSkinSurface as a double

end

% --- Executes during object creation, after setting all properties.
function FilenameSkinSurface_CreateFcn(hObject, eventdata, handles)
% hObject    handle to FilenameSkinSurface (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

end

% --- Executes on button press in LoadSkinSurfaceButton.
function LoadSkinSurfaceButton_Callback(hObject, eventdata, handles)
% hObject    handle to LoadSkinSurfaceButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

fname = get(handles.FilenameSkinSurface,'String');

%keyboard;
headSurf  = ExtractIsosurface(file_NRRD(fname,'./'));
handles.usrdata.headSurf = headSurf;

UpdateElectrodeDisplay(handles);

% save handles structure
guidata(hObject,handles);


end

% --- Executes on button press in LoadMRIFileButton.
function LoadMRIFileButton_Callback(hObject, eventdata, handles)
% hObject    handle to LoadMRIFileButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

fname = get(handles.MRIFileNameInput,'String');
nrrdMRI = file_NRRD(fname);
handles.usrdata.nrrdMRI = nrrdMRI;

%disp('Successfully Read MRI Header');

handles.usrdata.volume = nrrdMRI.data;
handles.usrdata.resample = 2;


% set slider properties: Min
set(handles.xSlider,'Min',1);
set(handles.ySlider,'Min',1);
set(handles.zSlider,'Min',1);

% set slider properties: Max
set(handles.xSlider,'Max',nrrdMRI.sizes(1));
set(handles.ySlider,'Max',nrrdMRI.sizes(2));
set(handles.zSlider,'Max',nrrdMRI.sizes(3));

% set slider properties: Value
set(handles.xSlider,'Value',1);
set(handles.ySlider,'Value',1);
set(handles.zSlider,'Value',1);
handles.usrdata.sliderpos = ones(1,3);
handles.usrdata.sliderchg = 'Z';

% set slider text properties
set(handles.txtxSlider,'String',['X Slice #: ',num2str(1)]);
set(handles.txtySlider,'String',['Y Slice #: ',num2str(1)]);
set(handles.txtzSlider,'String',['Z Slice #: ',num2str(1)]);

%disp('Set Properties');
aspect = zeros(1,3); % aspect ratio
aspect(1) = sqrt(sum(nrrdMRI.spacedirections(:,1).^2)); % x-sampling
aspect(2) = sqrt(sum(nrrdMRI.spacedirections(:,2).^2)); % y-sampling
aspect(3) = sqrt(sum(nrrdMRI.spacedirections(:,3).^2)); % z-sampling

[xs ys zs] = size(nrrdMRI.data);
handles.usrdata.X =  aspect(1) * [ (-xs/2+1) : xs/2 ]; % x-sampling grid
handles.usrdata.Y =  aspect(2) * [ (-ys/2+1) : ys/2 ]; % y-sampling grid
handles.usrdata.Z =  aspect(3) * [ (-zs/2+1) : zs/2 ]; % z-sampling grid

% make preview
UpdateMRIAxes(handles);

% save handles structure
guidata(hObject,handles);

end

% --- Generates preview on the axes
function UpdateMRIAxes( handles )

% cla(handles.axesPreview);
 axes(handles.MRIAxes);


   % preview in 2D
    switch handles.usrdata.sliderchg
        case 'X'
            index = handles.usrdata.sliderpos(1);
            if ~index, index = 1; end
            img = squeeze(handles.usrdata.volume(index,:,:));
            imghnd = image( squeeze(handles.usrdata.volume(index,:,:)) );
            img95 = prctile(img(:),95);
            title('Displaying Y-Z plane')            
        case 'Y'
            index = handles.usrdata.sliderpos(2);
            if ~index, index = 1; end
            img = squeeze(handles.usrdata.volume(:,index,:));
            imghnd = image( squeeze(handles.usrdata.volume(:,index,:)) );
            img95 = prctile(img(:),95);
            title('Displaying X-Z plane')
        case 'Z'  
            index = handles.usrdata.sliderpos(3);
            if ~index, index = 1; end
            img = handles.usrdata.volume(:,:,index);
            imghnd = image( handles.usrdata.volume(:,:,index) );
            img95 = prctile(img(:),95);
            title('Displaying X-Y plane')
        otherwise
            return;
    end
    axis xy   

    handles.usrdata
    
    if isfield(handles.usrdata,'point_coords')&&~isempty(handles.usrdata.point_coords)
        mypoints = [];
        mylabels = {};
    
        switch handles.usrdata.sliderchg
            case 'X', x = handles.usrdata.sliderpos(1); % z = c1; y = c2;
                      ind = find(handles.usrdata.point_index(:,1)==x);
                      if ~isempty(ind), 
                          mypoints = handles.usrdata.point_index(ind,[3 2]); 
                          mylabels = { handles.usrdata.point_labels{ind} };                          
                      end
            case 'Y', y = handles.usrdata.sliderpos(2); % z = c2; y = c1;
                      ind = find(handles.usrdata.point_index(:,2)==y);
                      if ~isempty(ind), 
                          mypoints = handles.usrdata.point_index(ind,[3 1]); 
                          mylabels = { handles.usrdata.point_labels{ind} };                          
                      end
            case 'Z', z = handles.usrdata.sliderpos(3); % x = c2; y = c1;
                      ind = find(handles.usrdata.point_index(:,3)==z);
                      if ~isempty(ind), 
                        mypoints = handles.usrdata.point_index(ind,[2 1]); 
                        mylabels = { handles.usrdata.point_labels{ind} };
                      end
            otherwise, return;
        end   

        if ~isempty(mypoints) && ~isempty(mylabels)
            axes(handles.MRIAxes), hold on
            for i = 1 : size(mypoints)
                plot(handles.MRIAxes,mypoints(i,1),mypoints(i,2),'+r')
                text(mypoints(i,1),mypoints(i,2),[' ',mylabels{i}],'Color','red')
            end
            axes(handles.MRIAxes), hold off
        end
    end
    
img95 = round(img95);
    colormap(bone(img95));
% colormap(bone(256))
%colormap(jet(256))

set(imghnd,'ButtonDownFcn',@MRIAxes_ButtonDownFcn);

end


% --- Executes on selection change in AssignedElectrodes.
function AssignedElectrodes_Callback(hObject, eventdata, handles)
% hObject    handle to AssignedElectrodes (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns AssignedElectrodes contents as cell array
%        contents{get(hObject,'Value')} returns selected item from AssignedElectrodes

end

% --- Executes during object creation, after setting all properties.
function AssignedElectrodes_CreateFcn(hObject, eventdata, handles)
% hObject    handle to AssignedElectrodes (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

end

% --- Executes on selection change in ElectrodeLabels.
function ElectrodeLabels_Callback(hObject, eventdata, handles)
% hObject    handle to ElectrodeLabels (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns ElectrodeLabels contents as cell array
%        contents{get(hObject,'Value')} returns selected item from ElectrodeLabels

end

% --- Executes during object creation, after setting all properties.
function ElectrodeLabels_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ElectrodeLabels (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

end


% --- Executes on mouse press over axes background.
function MRIAxes_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to MRIAxes (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

handles = guidata(hObject);

contents = get(handles.ElectrodeLabels,'String');
handles.usrdata.curelectrode = contents{get(handles.ElectrodeLabels,'Value')};

pos = get(handles.MRIAxes,'CurrentPoint');
c1 = pos(1,1);
c2 = pos(1,2);

axes(handles.MRIAxes), hold on
plot(handles.MRIAxes,c1,c2,'+r')
text(c1,c2,[' ',handles.usrdata.curelectrode],'Color','red')
axes(handles.MRIAxes), hold off

c1 = round(c1);
c2 = round(c2);

switch handles.usrdata.sliderchg
    case 'X', x = handles.usrdata.sliderpos(1); z = c1; y = c2;
    case 'Y', y = handles.usrdata.sliderpos(2); x = c2; z = c1;
    case 'Z', z = handles.usrdata.sliderpos(3); x = c2; y = c1;
    otherwise, return;
end

if ~isfield(handles.usrdata,'point_labels'), handles.usrdata.point_labels = {}; end
if ~isfield(handles.usrdata,'point_coords'), handles.usrdata.point_coords = []; end

refidx = length(handles.usrdata.point_labels) + 1;
for i = 1:length(handles.usrdata.point_labels)
  if strcmp(handles.usrdata.curelectrode,handles.usrdata.point_labels{i})
    refidx = i;
  end;
end;

% Get index associated with current fiducial marker
switch lower(handles.usrdata.curelectrode)
    case 'rpa'
        refidx = 5;
    case 'lpa'
        refidx = 4;
    case 'manual_cz'
        refidx = 3;
    case 'inion'
        refidx = 2;
    case 'nasion'
        refidx = 1;
end;
  
% X = handles.usrdata.X;
% Y = handles.usrdata.Y;
% Z = handles.usrdata.Z;
% 
% xLoc = X(min([max([round(x) 1]) length(X)]));
% yLoc = Y(min([max([round(y) 1]) length(Y)]));
% zLoc = Z(min([max([round(z) 1]) length(Z)]));

disp([x y z]);
disp([xLoc yLoc zLoc]);

tmp = handles.usrdata.nrrdMRI.spacedirections*[x y z]' + handles.usrdata.nrrdMRI.spaceorigin;

xLoc = tmp(1); yLoc = tmp(2); zLoc = tmp(3);

handles.usrdata.point_labels{refidx} = handles.usrdata.curelectrode;
handles.usrdata.point_index(refidx,:)= [x y z];
handles.usrdata.point_coords(refidx,:) = [xLoc yLoc zLoc];

str = [handles.usrdata.curelectrode,': ',num2str(x),' ',num2str(y),' ',num2str(z)];
list_entries = get(handles.AssignedElectrodes,'String');
list_entries{refidx} = str; 

set(handles.AssignedElectrodes,'String',list_entries,'Value',size(list_entries,1));

if isfield(handles.usrdata,'headSurf');
UpdateElectrodeDisplay(handles);
end;

% save handles structure
guidata(hObject,handles);

end

%% Control of Electrode Configurations

function InputNumElectrodes_Callback(hObject, eventdata, handles)
% hObject    handle to InputNumElectrodes (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of InputNumElectrodes as text
%        str2double(get(hObject,'String')) returns contents of InputNumElectrodes as a double

end

% --- Executes during object creation, after setting all properties.
function InputNumElectrodes_CreateFcn(hObject, eventdata, handles)
% hObject    handle to InputNumElectrodes (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

end

% --- Executes on button press in SetNumElectrodes.
function SetNumElectrodes_Callback(hObject, eventdata, handles)
% hObject    handle to SetNumElectrodes (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

numElectrodes = str2num(get(handles.InputNumElectrodes,'String'));

labels = set_ElectrodeLabels(numElectrodes);

set(handles.ElectrodeLabels,'String',labels);

end

function [labels] = set_ElectrodeLabels(numElectrodes)

labels{1} = 'Nasion';
labels{2} = 'Inion';
labels{3} = 'Manual_Cz';
labels{4} = 'RPA';
labels{5} = 'LPA';

for i = 6:(numElectrodes+5)
  labels{i} = ['Electrode ' num2str(i-5) ];
end;
end

% --- Executes on button press in ClearElectrodeButton.
function ClearElectrodeButton_Callback(hObject, eventdata, handles)
% hObject    handle to ClearElectrodeButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)\

handles.usrdata.point_coords = [];
handles.usrdata.point_labels = {};
handles.usrdata.point_index = [];

UpdateElectrodeDisplay(handles);

guidata(hObject,handles);
end


% --- Executes on selection change in ElectrodeSelection.
function ElectrodeSelection_Callback(hObject, eventdata, handles)
% hObject    handle to ElectrodeSelection (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns ElectrodeSelection contents as cell array
%        contents{get(hObject,'Value')} returns selected item from ElectrodeSelection
end

% --- Executes during object creation, after setting all properties.
function ElectrodeSelection_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ElectrodeSelection (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

labels{1} = '10-20';
labels{2} = '10-10';
labels{3} = '10-5';
labels{4} = 'Custom';

set(hObject,'String',labels);

end

% --- Executes on button press in SetConfigurationButton.
function SetConfigurationButton_Callback(hObject, eventdata, handles)
% hObject    handle to SetConfigurationButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

Configurations = get(handles.ElectrodeSelection,'String');
config = Configurations{get(handles.ElectrodeSelection,'Value')};

switch lower(config)
  case '10-20'
    labels = set_ElectrodeLabels(21);
    set(handles.ElectrodeLabels,'String',labels);
    set(handles.InputNumElectrodes,'String','26');
    handles.usrdata.point_labels = [];
    handles.usrdata.point_coords = [];
    handles.usrdata.point_labels = labels;
  case '10-10'
    labels = set_ElectrodeLabels(73);
    set(handles.ElectrodeLabels,'String',labels);
    set(handles.InputNumElectrodes,'String','45');
    handles.usrdata.point_labels = [];
    handles.usrdata.point_coords = [];
    handles.usrdata.point_labels = labels;    
  case '10-5'
    labels = set_ElectrodeLabels(40);
    set(handles.ElectrodeLabels,'String',labels);
    set(handles.InputNumElectrodes,'String','45');        
  case 'custom'
    numElectrodes = str2num(get(handles.InputNumElectrodes,'String'));
    labels = set_ElectrodeLabels(numElectrodes);
    set(handles.ElectrodeLabels,'String',labels);    
  otherwise
end

guidata(hObject,handles);


end


%% Display Functions

% --- Executes on button press in RedrawMRI.
function RedrawMRI_Callback(hObject, eventdata, handles)
% hObject    handle to RedrawMRI (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

UpdateMRIAxes(handles);

end

function DisplaySurf( hfig, ViewAngle, surfStruct, Ds )

if nargin<3, Ds = []; end

axes(hfig); cla;

if isfield(surfStruct,'FaceColor')
  FColor = surfStruct.FaceColor;
else
  FColor = [1,0.75,0.65];
  %disp('Using Default Face Color');
end;

if isfield(surfStruct,'FAlpha');
  FAlpha = surfStruct.FAlpha;  
else
  FAlpha = 0.75;
  %disp('Using Default Alpha Value');
end;

if isfield(surfStruct,'EdgeColor')
  EColor = surfStruct.EdgeColor;
else
  EColor = [0.7 0.7 0.7];
  %disp('Using Default Edge Color');
end;

headSurf.faces = surfStruct.faces;
headSurf.vertices = surfStruct.vert;

headSurf = reducepatch(headSurf, 0.05);


hiso = patch( 'Faces',headSurf.faces,...
              'Vertices',headSurf.vertices,...
              'FaceColor',FColor,...
              'FaceAlpha',FAlpha,...
              'EdgeAlpha',0,...
              'EdgeColor',EColor,...
              'FaceLighting','phong'); % draw surface              
          

xlabel('x'); ylabel('y'); zlabel('z');            
            
% hiso = patch( 'Faces',faces,...
%               'Vertices',vert,...          
%               'FaceColor',[1,.75,.65],...
%               'EdgeColor','none'); % draw surface
view(ViewAngle);      
axis equal % define the view
%daspect(1./surfStruct.aspect) % set aspect ratio
% % daspect(aspect) % set aspect ratio
% 
% if isempty(Ds), return, end
% lightangle(45,30); % add lighting
% set(hfig,'Renderer','zbuffer'); lighting phong

% clear  faces vert aspect
% load headsurf.mat Ds
% isonormals(Ds,hiso)
% set(hiso,'SpecularColorReflectance',0,'SpecularExponent',50)
% 
% alpha(.5)
end

function DisplayElectrodes( hfig, handles )

if isfield(handles.usrdata,'point_coords');
  posNew = handles.usrdata.point_coords;
  labels = handles.usrdata.point_labels;
else
  posNew = [];
end;

cmap = [];
if isempty(posNew), 
  %disp('No Point Coordinates to Display');
  return; 
end % if no data points, return

axes(hfig); % select figure window                            
hold on; % lock figure data

[sX sY sZ] = sphere(30); % create sphere        
npoints = (size(posNew,1)); % number of points
% sC = zeros(size(sZ)); sC(:) = 1;

 sX = sX * 3;
 sY = sY * 3;
 sZ = sZ * 3;

if ~exist('resid'), resid=[]; end;
if ~isempty(resid)
    [n,xout] = hist( resid );            
    cmap = colormap(jet(length(xout)+2));                               
end

if ~exist('edgecolor'),edgecolor = []; end;
if ~isempty(edgecolor)
    maxcolors = 5;
    cmap = colormap(jet(maxcolors));
    if edgecolor>maxcolors, edgecolor = maxcolors; end
    cmap = cmap(edgecolor,:);
end

if ~exist('posNear'); posNear = []; end;

for i = 1 : npoints               

    x = posNew(i,1);
    y = posNew(i,2);
    z = posNew(i,3);
    
    if ~isempty(resid)
        if resid(i)<xout(1), ind = 1;                  
        elseif resid(i)>xout(end), ind = length(xout)+2;
        else ind = floor((resid(i)-xout(1))/abs(diff(xout(1:2)))) + 2; end
        surf( gca, sX+x, sY+y, sZ+z, 'EdgeColor', cmap(ind,:) ); % plot sphere 
    elseif ~isempty(edgecolor)
        surf( gca, sX+x, sY+y, sZ+z, edgecolor*ones(size(sZ)), 'EdgeColor', cmap ); % plot sphere, edge color as in cmap        
        colormap(cmap)
    else
        surf( gca, sX+x, sY+y, sZ+z ); % plot sphere, edge color default (black)
    end        

    text(x,y,z,labels{i},'Color','red')
    
    % surf( gca, sX+x, sY+y, sZ+z, 'CData', sC, 'EdgeColor', sE(1,:) ); % plot sphere                        
    if(isempty(posNear)) continue; end

    [minval,x2] = min(abs(X - posNear(i,1))); % get x-dir index
    [minval,y2] = min(abs(Y - posNear(i,2))); % get y-dir index
    [minval,z2] = min(abs(Z - posNear(i,3))); % get z-dir index                                          
    plot3([x2 x], [y2 y], [z2 z],'-r','LineWidth',2);
end      

hold off; % unlock figure data  
end

function UpdateElectrodeDisplay(handles)

ViewAngle(1) = get(handles.AngleSlider,'Value');
ViewAngle(2) = get(handles.ElevationSlider,'Value');
DisplaySurf(handles.RenderedAxes,ViewAngle,handles.usrdata.headSurf);
DisplayElectrodes(handles.RenderedAxes,handles);

end




%%  Control of View Angle and Elevation for RenderedAxes
% --- Executes on slider movement.
function AngleSlider_Callback(hObject, eventdata, handles)
% hObject    handle to AngleSlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
el = get(handles.ElevationSlider,'Value');
angle = get(handles.AngleSlider,'Value');

axes(handles.RenderedAxes);

view([angle,el]);
end

% --- Executes during object creation, after setting all properties.
function AngleSlider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to AngleSlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

set(hObject,'Min',0);
set(hObject,'Max',360);
set(hObject,'Value',40);

guidata(hObject,handles);

end

% --- Executes on slider movement.
function ElevationSlider_Callback(hObject, eventdata, handles)
% hObject    handle to ElevationSlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

el = get(handles.ElevationSlider,'Value');
angle = get(handles.AngleSlider,'Value');

axes(handles.RenderedAxes);

view([angle,el]);

end

% --- Executes during object creation, after setting all properties.
function ElevationSlider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ElevationSlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

set(hObject,'Min',-90);
set(hObject,'Max',90);
set(hObject,'Value',30);

guidata(hObject,handles);


end

%%

% --- Executes during object creation, after setting all properties.
function RenderedAxes_CreateFcn(hObject, eventdata, handles)
% hObject    handle to RenderedAxes (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: place code in OpeningFcn to populate RenderedAxes
end


% --- Executes on button press in ElectrodesAutoAssign.
function ElectrodesAutoAssign_Callback(hObject, eventdata, handles)
% hObject    handle to ElectrodesAutoAssign (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

Configurations = get(handles.ElectrodeSelection,'String');
config = Configurations{get(handles.ElectrodeSelection,'Value')};

switch config
  case '10-20'
    if isfield(handles.usrdata,'point_coords');
      posNew = handles.usrdata.point_coords;
    else
      error('Must define characteristic points first');
    end;
    Electrodes = get1020Electrodes(posNew(1,:),posNew(2,:),posNew(3,:),posNew(4,:),posNew(5,:));

    for i = 1:length(Electrodes.Labels)
      handles.usrdata.point_labels{5+i}   = Electrodes.Labels{i};
      handles.usrdata.point_coords(5+i,:) = Electrodes.Pos(i,:);
    end;
  case '10-10'
    if isfield(handles.usrdata,'point_coords');
      posNew = handles.usrdata.point_coords;
    else
      error('Must define characteristic points first');
    end;    

    mydisp('About to compute 1010 electrodes');
    keyboard;
    
    tmp = get1010Electrodes(posNew(1,:),posNew(2,:),posNew(3,:),posNew(4,:),posNew(5,:));
    
    Electrodes = cnlElectrodes;
    Electrodes.Positions = tmp.Pos;
    Electrodes.Labels = tmp.Labels;
%    Electrodes.
        
    for i = 1:length(Electrodes.Labels)
      handles.usrdata.point_labels{5+i}   = Electrodes.Labels{i};
      handles.usrdata.point_coords(5+i,:) = Electrodes.Pos(i,:);
    end;    
end;

global guiDataOut
guiDataOut.Electrodes = Electrodes;

if isfield(handles.usrdata,'headSurf')
 UpdateElectrodeDisplay(handles);
end;
guidata(hObject,handles);

end

% --- If Enable == 'on', executes on mouse press in 5 pixel border.
% --- Otherwise, executes on mouse press in 5 pixel border or over SetConfigurationButton.
function SetConfigurationButton_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to SetConfigurationButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
end

