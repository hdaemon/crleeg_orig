
%% Set Directories and Filenames
% ProcessedRootDIR = '/common/data/processed/epilepsysurgicalplanning/Case030/';
% dirMRI = 'damon_eeg/';
% dirPhotogrammetry = 'photogrammetry/';
% fileMRI = 'o_t1w.nhdr';
% fileSkinSeg = 'o_t1w_skinseg.nhdr';
% filePhotogrammetry = 'Michael-set3-ziga-exp.sfp';

function [ Electrodes ] = EEG_RegisterPhotogrammetryData(dirs,files,modelOptions);


mydisp('EEG_RegisterPhotogrammetryData >> BEGIN');
%% Get original directory
OrigDir = pwd;


% global casepath datapath respath matpath mripath curcase
% curcase = ' ';
% mripath  =  dirs.MRIFull;
% casepath =  dirs.PhotogrammetryFull;
% datapath =  dirs.PhotogrammetryFull;
% respath  =  [dirs.PhotogrammetryFull 'Res-Sessions/'];
% 



%% Set PhotogrammetryOptions
photoObj = cnlPhotoGram(...
    'costFuncType', modelOptions.photoCostFunction,...
    'transfType'  , modelOptions.photoModelTr,...
    'optimType'   , modelOptions.photoOptimization,...
    'MRIFname'    , files.MRI, ...
    'SkinFname'   , files.SkinSeg, ...
    'EEGFname'    , files.Photogrammetry, ...
    'MRIPath'     , dirs.MRIFull, ...    
    'datapath'    , dirs.PhotogrammetryFull, ...
    'respath'     , [dirs.PhotogrammetryFull 'Res-Sessions/'], ...
    'matpath'     , [dirs.PhotogrammetryFull 'Mat-Sessions/']);
     
%[finalTr, time, photoObj] = maRegistration(photoObj);

%% Get Heaed surface for display
% All this is doing is reading the MRI data from the skin surface file and
% generating the surface data.  Should probably just skip it.
% DisplayRegRes(photoObj,'init');



mydisp('EEG_RegisterPHotogrammetryData >> END');
return;