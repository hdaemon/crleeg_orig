% Principal axes computation and point subset extraction
% -------------------------------------------------------------------------
% Description:
%
% Usage:
%	[G1, G2, N, V] = maPrincipalAxes( points, 'indices' )
%        [G1, G2] = maPrincipalAxes( points, 'indices' )
%
% Input:
%   - points: EEG point coordinates
%   - export: 'indices' or 'coordinates'
%   - selsplit*: 'auto-equal' or 'principal-axes'
%
% Output:
%   - G1, G2: point indices or coordinates
%   - N: computed surface normal
%   - V*: principal axis vector
%   - EigVect*: export all eigenvectors
%   - EigVal*: export all eigenvalues
%
% Comments:
%
% Notes:
%
% -------------------------------------------------------------------------

function [G1, G2, varargout] = PrincipalAxes( points, export, selsplit )


mydisp('cnlPhotoGram.PrincipalAxes >> BEGIN');

if nargin<3, selsplit = 'auto-equal'; end

%% load data & centerize points
mc = sum(points,1) ./ size(points,1); % Get geometric center
points = points - repmat(mc,[size(points,1) 1]);

%% compute inertia moments & build inertia matrix
mxx = sum(points(:,2).^2 + points(:,3).^2);
myy = sum(points(:,1).^2 + points(:,3).^2);
mzz = sum(points(:,1).^2 + points(:,2).^2);
mxy = sum(points(:,1) .* points(:,2));
mxz = sum(points(:,1) .* points(:,3));
myz = sum(points(:,2) .* points(:,3));

% inertia matrix as appeared in Kozinska (2001)
M = [ mxx, mxy, mxz; ...
      mxy, myy, myz; ...
      mxz, myz, mzz];

[V,D] = eig(M); % compute principal axes

% Indices Minimum and Maximum Eigenvalues
D = diag(D);
minEig = find(D==min(D)); % get index of smallest eigenvalue
maxEig = find(D==max(D)); % get index of largest eigenvalue


switch lower(selsplit)
  case 'principal-axes'    
    % This is the method that's pretty much always used
    finalNormal = cross(V(:,minEig),V(:,maxEig)); % Get normal to Normal defined by largest and smallest eigenvectors
    
  case 'auto-equal'
    mydisp('cnlPhotoGram.PrincipalAxes >> going off the expected path');
    keyboard;
    startNormal = [1,0,0];
    cf = @(Normal) SplitProjSum(Normal, points);
    simopt = optimset( 'LargeScale','off',...
      'GradObj','off',...
      'Display','iter',...
      'MaxFunEvals',1000,...
      'MaxIter',8,...
      'TolFun',1e-3,...
      'TolX',1e-3 ); % setup    
    finalNormal = fmincon( cf, startNormal, [], [], V(:,minEig)',  0,[],[],[]     , simopt ); % run
    
  otherwise
    error('Selsplit must be either ''principal-axes'' or ''auto-equal'' ');
end

finalNormal = finalNormal ./ norm(finalNormal);
[G1, G2] = GetGroups( finalNormal, points, export );

if nargout>2, varargout{1} = finalNormal; end
if nargout>3, varargout{2} = V(:,minEig); end
if nargout>4, varargout{3} = V; end
if nargout>5, varargout{4} = D; end

if false
  figure;
  plot3(points(:,1),points(:,2),points(:,3),'.');
  hold on;
  quiver3(zeros(1,3),zeros(1,3),zeros(1,3),2*V(1,:),4*V(2,:),6*V(3,:),'linewidth',3);
  quiver3(0,0,0,6*finalNormal(1),6*finalNormal(2),6*finalNormal(3),'r','linewidth',3);
end


mydisp('cnlPhotoGram.PrincipalAxes >> END');
end

%% split the points in two groups by a Normal
% a*(x - xc)+b*(y - yc)+c*(z - zc)=0
function [G1, G2] = GetGroups( Normal, points, export )
mydisp('cnlPhotoGram.PrincipalAxes.GetGroups >> BEGIN');
try
  Normal = reshape(Normal,3,1);
  Normal = Normal ./ norm(Normal); % norm the Normal normal
  Sprod = points * Normal; % compute dot product
catch
  keyboard;
end;

switch export
  case 'coordinates'
    % export point coordinates
    G1 = points(find(Sprod<0),:);
    G2 = points(find(Sprod>=0),:);
    % case 'indices'
  otherwise
    % export only indices
    G1 = find(Sprod<0);
    G2 = find(Sprod>=0);
end

mydisp('cnlPhotoGram.PrincipalAxes.GetGroups >> END');
end

%% split the point space by Normal
function cval = SplitProjSum( Normal, points )
mydisp('cnlPhotoGram.PrincipalAxes.SplitProjSum >> BEGIN');
Normal = reshape(Normal,3,1); % reshape parameter matrix
Normal = Normal ./ norm(Normal); % norm the Normal normal
Sprod = points * Normal; % compute dot product

cval = exp(abs(length(Sprod(find(Sprod<0)))-length(Sprod(find(Sprod>=0))))); % split with equal powers
cval = cval + max(abs(sum(Sprod(find(Sprod<0)))),sum(Sprod(find(Sprod>=0)))); % split with minimal distance
mydisp('cnlPhotoGram.PrincipalAxes.SplitProjSum >> END');
end




