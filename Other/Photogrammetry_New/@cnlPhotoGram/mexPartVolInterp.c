#include <math.h>
#include "mex.h"
#include "math.h" 

/*
 *  Partial volume interpolation
 *      (mexPartVolInterp.c)
 *
 * Usage: 
 *   H = mexPartVolInterp( points, data, smp, X, Y, Z, xbins )
 * 
 * Parameters:
 *   - points: points for evaluation
 *   - data: volume data
 *   - smp: sampling in x, y, z space directions
 *   - X, Y, Z: x,y,z sampling grid
 *   - xbins: bin intervals
 */


#define PARAM_POINTS 0
#define PARAM_DATA 1
#define PARAM_SMP 2        
#define PARAM_X 3    
#define PARAM_Y 4    
#define PARAM_Z 5    
#define PARAM_BINS 6

void partvolinterp( 
double *data, double *dhistout, double* dbins, int ibincount, /* data, histogram and bins */
double *smp, double *dxs, double *dys, double *dzs, /* x,y,z sampling */
int ixs, int iys, int izs, /* datasizes */
double *dpxc, double *dpyc, double *dpzc, int ipcount) /* x,y,z point coordinates & point count */
{
    int i, ix, iy, iz, ix2, iy2, iz2, idatap, ixys;
    double dx, dy, dz, dx2, dy2, dz2;
    double fdbin = fabs(dbins[1]-dbins[0]);
    
    ixys = ixs * iys;
    
    /* loop through all points (x,y,z) */
    for(i=0; i<ipcount; i++)
    {            
        if( dpxc[i]<dxs[0] || dpxc[i]>=dxs[ixs-1]) { dhistout[0]+= 1.0; continue; }
        if( dpyc[i]<dys[0] || dpyc[i]>=dys[iys-1]) { dhistout[0]+= 1.0; continue; }
        if( dpzc[i]<dzs[0] || dpzc[i]>=dzs[izs-1]) { dhistout[0]+= 1.0; continue; }

        ix = (int)((dpxc[i]-dxs[0])/smp[0]); ix2 = ix + 1;               
        iy = (int)((dpyc[i]-dys[0])/smp[1]); iy2 = iy + 1;        
        iz = (int)((dpzc[i]-dzs[0])/smp[2]); iz2 = iz + 1;        
        
        dx = fabs(dxs[ix] - dpxc[i]) / smp[0];
        dx2 = fabs(dxs[ix2] - dpxc[i]) / smp[0];

        dy = fabs(dys[iy] - dpyc[i]) / smp[1];
        dy2 = fabs(dys[iy2] - dpyc[i]) / smp[1];

        dz = fabs(dzs[iz] - dpzc[i]) / smp[2];
        dz2 = fabs(dzs[iz2] - dpzc[i]) / smp[2];        

        /* update histogram by adding partial voxel volumes                                                     
           point right-anterior-inferior */
        idatap = ix + iy * ixs + iz * ixys;
        idatap = (int)(data[idatap]/fdbin);
        dhistout[idatap] += dx2*dy2*dz2;

        /* point left-anterior-inferior */
        idatap = ix2 + iy * ixs + iz * ixys;
        idatap = (int)(data[idatap]/fdbin);
        dhistout[idatap] += dx*dy2*dz2;

        /* point right-posterior-inferior */               
        idatap = ix + iy2 * ixs + iz * ixys;
        idatap = (int)(data[idatap]/fdbin);
        dhistout[idatap] += dx2*dy*dz2;

        /* point right-anterior-superior */
        idatap = ix + iy * ixs + iz2 * ixys;
        idatap = (int)(data[idatap]/fdbin);
        dhistout[idatap] += dx2*dy2*dz;

        /* point left-posterior-inferior */               
        idatap = ix2 + iy2 * ixs + iz * ixys;
        idatap = (int)(data[idatap]/fdbin);
        dhistout[idatap] += dx*dy*dz2;
      
        /* point right-posterior-superior */
        idatap = ix + iy2 * ixs + iz2 * ixys;
        idatap = (int)(data[idatap]/fdbin);
        dhistout[idatap] += dx2*dy*dz;

        /* point left-anterior-superior */               
        idatap = ix2 + iy * ixs + iz2 * ixys;
        idatap = (int)(data[idatap]/fdbin);
        dhistout[idatap] += dx*dy2*dz;

        /* point left-posterior-superior */               
        idatap = ix2 + iy2 * ixs + iz2 * ixys;
        idatap = (int)(data[idatap]/fdbin);
        dhistout[idatap] += dx*dy*dz;            
    }
}

void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[] )
{
    double *dhistout, *dbins, *smp, *dxs, *dys, *dzs, *dpxc, *dpyc, *dpzc;
    int i, ipcount, ixs, iys, izs, ibincount, ncols;
 
    /* Check for proper number of arguments. */
    if(nrhs<7)  mexErrMsgTxt("Missing parameter: xbins.");    
    else if(nrhs<6)  mexErrMsgTxt("Missing parameter: Z.");      
    else if(nrhs<5)  mexErrMsgTxt("Missing parameter: Y.");      
    else if(nrhs<4)  mexErrMsgTxt("Missing parameter: X.");      
    else if(nrhs<3)  mexErrMsgTxt("Missing parameter: smp.");    
    else if(nrhs<2)  mexErrMsgTxt("Missing parameter: data.");   
    else if(nrhs<1)  mexErrMsgTxt("Missing parameter: points.");      

    if(nlhs>1) {
        mexErrMsgTxt("Too many output arguments");
    }
    else if(nlhs==0) return;  
    
    /* Get input parameters.
       PARAM_POINTS */
    ipcount = mxGetM(prhs[PARAM_POINTS]);
    ncols = mxGetN(prhs[PARAM_POINTS]);
    
    if(ncols!=3) mexErrMsgTxt("PARAM_POINTS: Missing point coordinates!");
    dpxc = (double*)mxGetData(prhs[PARAM_POINTS]);    
    dpyc = dpxc + ipcount;
    dpzc = dpyc + ipcount;         

    /* PARAM_SMP */  
    ncols = mxGetNumberOfElements(prhs[PARAM_SMP]);
    if(ncols!=3) mexErrMsgTxt("PARAM_SMP: Missing sampling data!");    
    smp = (double*)mxGetData(prhs[PARAM_SMP]);
    
    /* PARAM_X */
    ixs = mxGetNumberOfElements(prhs[PARAM_X]);
    dxs = (double*)mxGetData(prhs[PARAM_X]);    
    
    /* PARAM_Y */
    iys = mxGetNumberOfElements(prhs[PARAM_Y]);
    dys = (double*)mxGetData(prhs[PARAM_Y]);    
    
    /* PARAM_Z */
    izs = mxGetNumberOfElements(prhs[PARAM_Z]);
    dzs = (double*)mxGetData(prhs[PARAM_Z]);        

    /* PARAM_BINS */
    ibincount = mxGetNumberOfElements(prhs[PARAM_BINS]);
    dbins = (double*)mxGetData(prhs[PARAM_BINS]);        
    
    /* Create matrix for the return argument. */
    plhs[0] = mxCreateDoubleMatrix(ibincount,1, mxREAL);
    dhistout = mxGetData(plhs[0]);   
    for(i=0; i<ibincount; i++) dhistout[i] = 0.0;
                  
    partvolinterp((double*)mxGetData(prhs[PARAM_DATA]),dhistout,dbins,ibincount,smp,dxs,dys,dzs,ixs,iys,izs,dpxc,dpyc,dpzc,ipcount);    
}

