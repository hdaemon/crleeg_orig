function photoObj = DataPreprocessing(photoObj)
% function photoObj = DataPreprocessing(photoObj)
%
%
%  Calls:
%   cnlPhotoGram.PrincipalAxes
%   cnlPhotoGram.PointPairMatching
%   cnlPhotoGram.PointGrouping
%   cnlPhotoGram.MRISegmentation
%   cnlPhotoGram.MRISymmetryPlane

mydisp('photoObj.DataPreprocessing >> BEGIN');

%% Preprocess Data from SFP File

% Compute Principle Axes of EEG Cloud
mydisp('photogObj.DataPreprocessing >>  Computing EEG points cloud principal axes...')
%[G1, G2, photoObj.Np, photoObj.V, photoObj.EVCTp, photoObj.EVALp] = ...
[G1, G2, photoObj.Np, ~ , ~, ~] = ...
  cnlPhotoGram.PrincipalAxes( photoObj.posEEG, 'indices', 'principal-axes' );

% Total Hack right now
% photoObj.Np = [ 1 0 0 ];

% Do point pair matching
mydisp('photogObj.DataPreprocessing >>  Searching for maximal cardinality bipartite graph...')
[photoObj.Psym, photoObj.Wsym, photoObj.MCp] = ...
  cnlPhotoGram.PointPairMatching( photoObj.posEEG, G1, G2, photoObj.Np, true );

photoObj.MCp = reshape(photoObj.MCp,1,3);
photoObj.Np  = reshape(photoObj.Np,1,3);

mydisp('photogObj.DataPreprocessing >>  Searching for neighbouring point pairs...')
[photoObj.Pnet,photoObj.Wnet] = cnlPhotoGram.PointGrouping( photoObj.posEEG );

keyboard;

%% Preprocess MRI Data
% Compute Otsu Segmentation
% all that's changed is photoObj.volume and potentially
% photoObj.otsu_thresh
mydisp('photogObj.DataPreprocessing >>  Performing Otsu segmentation...')
photoObj = photoObj.MRISegmentation('otsu');

mydisp('cnlPhotoGram.Datapreprocessing >> Searching for MRI Symmetry plane');
if ~isempty(photoObj.otsu_thresh), thresh = photoObj.otsu_thresh; else thresh = 10; end
%[photoObj.Nmri, photoObj.MCmri, photoObj.EVCTmri, photoObj.EVALmri] = ...
[photoObj.Nmri, photoObj.MCmri, ~, ~] = ...
  cnlPhotoGram.MRISymmetryPlane( photoObj.nrrdMRI, 'T', thresh );

photoObj.MCmri = reshape(photoObj.MCmri,1,3);
photoObj.Nmri  = reshape(photoObj.Nmri,1,3);

% sampling distances x-y-z
photoObj.sampling = photoObj.nrrdMRI.getAspect;

% apply parameter space scaling (for rotation parameters)
% anglescale = asin((sampling*sqrt(sum(size(DT).^2))/4)^(-1));
cubediag = 0.25*sqrt(sum((photoObj.nrrdMRI.sizes .* photoObj.sampling).^2));
photoObj.anglescale = asin(inv(cubediag));
photoObj.shiftscale = 1;

% x-y-z sampling grid
%
% Why are we insisting on sampling on a zero-centered grid.  Is there
% any real reason?  Why couldn't we just sample on the original
% spatial grid of the NRRD (In which case, we can just use
warning('Should probably be using a cnlGridSpace here');
mydisp('cnlPhotoGram.Datapreprocessing >> computing sampling grid');
nrrdSizes = photoObj.nrrdMRI.sizes;
photoObj.X = photoObj.sampling(1) .* [ (-nrrdSizes(1)/2+1) : nrrdSizes(1)/2 ];
photoObj.Y = photoObj.sampling(2) .* [ (-nrrdSizes(2)/2+1) : nrrdSizes(2)/2 ];
photoObj.Z = photoObj.sampling(3) .* [ (-nrrdSizes(3)/2+1) : nrrdSizes(3)/2 ];

% Save Initialization Data
cd(photoObj.matpath)
save InitData.mat photoObj

mydisp('photogObj.DataPreprocessing >> END');
% if isfield(regs.data,'empty'), rmfield(regs.data,'empty'); end
end