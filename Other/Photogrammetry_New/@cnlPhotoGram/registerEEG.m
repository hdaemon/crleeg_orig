function [photoObj] = registerEEG(photoObj)
% function [photoObj] = registerEEG(photoObj)
%
% This is the primary method for a cnlPhotoGram object.  Given an 
% appropriately configured object, registerEEG() does the necessary data
% preprocessing, computes the initial registration, if desired, initializes
% the cost function, and then optimizes it.
  
% Move to the right Directory
OrigDir = pwd;
cd(photoObj.matpath);

%%  Preprocess Data
photoObj = photoObj.preprocessElectrodes;
photoObj = photoObj.preprocessMRI;
mydisp('Data Preprocessing Complete');

%%  Do initial rigid transformation
if ~photoObj.skipinitreg
  tic; mydisp('Computing initial closed-form rigid transformation..');
 photoObj = photoObj.InitialRegistration;
 time.initreg = toc;
 mydisp('Initial Rigid Transform Computed');
else
  photoObj.posStart = photoObj.posEEG;
  photoObj.posFIDStart = photoObj.posFID;
  mydisp('Skipping Initial Rigid Registration');
end;

mydisp('Check initial registration and ensure it makes sense');
tmpElec = photoObj.getElectrodeObj('start');
tmpElec.plot3D(photoObj.nrrdSkin);
keyboard;


%%  Initialize Cost Function
photoObj = photoObj.InitializeCostFunction;
mydisp('Cost Function Initialized');

%% Cost Function Initialization
if ~isempty(photoObj.initTr) & strcmp(photoObj.transfType,'rigid'), 
    [T,s] = getTransformMatrix( photoObj.initTr, 'rigid', 'shiftscale', 1 , 'anglescale', 1);
    photoObj.initTr = T;                
end

%% Cost Function Optimization
tic, mydisp('   Optimizing cost function with respect to transformation... ');
photoObj = photoObj.CostFunctionOptimization;
time.costopt = toc; %fprintf(['Execution time: ',num2str(time.multistart),' s\n']);

%% compute total time
time.total = 0;

if isfield(time,'preproc'),    time.total = time.total + time.preproc;    end
if isfield(time,'initcf'),     time.total = time.total + time.initcf;     end
if isfield(time,'initreg'),    time.total = time.total + time.initreg;    end
if isfield(time,'multistart'), time.total = time.total + time.multistart; end
if isfield(time,'costopt'),    time.total = time.total + time.costopt;    end
if isfield(time,'evaluation'), time.total = time.total + time.evaluation; end

mydisp(['Finished in ',num2str(time.total),' s.'])            
            
%% clear handles photoObj structure
photoObj.cf = []; 

finalTr = photoObj.optimTr;

cd(OrigDir);  
  
end