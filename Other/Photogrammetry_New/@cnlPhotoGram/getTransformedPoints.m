%% transform points with T, scale with s
function posNew = getTransformedPoints( posOld, T, s )
  % loop through all points
  posNew = zeros(size(posOld));
  npoints = max(size(posOld)); % get number of points
  for i = 1 : npoints
    p = T * [s .* posOld(i,:),1]';
    posNew(i,:) = p(1:3);
  end
  
end