% HCF Options 
% -------------------------------------------------------------------------
% Description:
% 	Get HCF (histogram criterion function) options structure 
% 
% Usage: 
%	hcfopt = maGetHCFOptions( ); % default structure
%   hcfopt = maGetHCFOptions( 'radius', 3, 'step', 0.5, 'interpolation','linear','binning',32)
%       
% Input:
%   - mode*: 'global' or 'localsym' or 'localnet' or 'bkgcon'
%   - radius*: sphere radius for histogramm calculation (default: 3)
%   - step*: sampling of the 3D space (default: 0.5)
%   - interpolation*: 'nearest' or 'linear' or 'cubic' or 'partvol' (default: linear)
%   - binning*: number of bins in histogramm
%   - divtype*: 'kldiv' or 'sym' or 'js'
%   
% Output:
%   - hcfopt: structure with initialized settings
%
% Comments:
%   varargin: 'fieldname' followed by 'fieldvalue'
%   modes of operation:
%   - global: similarity of local histograms to the global histogram
%   - localsym: local similarity of symmetrical points
%   - localnet: local similarity of neighbouring net point
%   - bkgcon: background constraint 
% 
% Notes:
%   Modes of operation can be concatenated, ie. 'global-localnet-bkgcon'.
% 
% -------------------------------------------------------------------------

function hcfopt = GetHCFOptions( varargin )

% init default structure
hcfopt.mode = 'global'; % 'global' or 'localsym' or 'localnet' or 'bkgcon'
hcfopt.radius = 10; % default radius
hcfopt.step = 1; % default radii sampling
hcfopt.interpolation = 'mexpartvol'; % default linear interpolation
hcfopt.binning = 32; % default binning
hcfopt.divtype = 'kldiv'; % Kullback-Leibler divergence

if nargin > 1
    nvars = 1;
    while nvars < nargin 
        if ~ischar(varargin{nvars}), error('Error: field name should be a character array!'); end         
        if nvars+1 <= nargin, hcfopt = setfield(hcfopt,lower(varargin{nvars}),varargin{nvars+1}); end
        nvars = nvars + 2; 
    end
end

mode.global   = ~isempty(strfind(hcfopt.mode,'global'));
mode.localsym = ~isempty(strfind(hcfopt.mode,'localsym'));
mode.localnet = ~isempty(strfind(hcfopt.mode,'localnet'));
mode.bkgcon   = ~isempty(strfind(hcfopt.mode,'bkgcon'));
hcfopt.mode   = mode;

end









