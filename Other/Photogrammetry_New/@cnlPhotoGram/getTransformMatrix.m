function [T,s] = getTransformMatrix(Tr, type, varargin)
%
% function [T,s] = getTransformMatrix(Tr, type, varargin)
%
% Condensation of three different transform functions.  This should REALLY
% have some better input parsing to make sure that everything is good.
%
% Rewritten By: D. Hyde, 01/19/2014
% Originally Written by Ziga Spiclin, 2009
% Part of the cnlEEG project

p = inputParser;
addRequired(p,'Tr');
validTransforms = {'affine' 'rigid' 'quasiaffine'};
addRequired(p,'type', @(x) any(validatestring(x,validTransforms)));
addParamValue(p,'shiftscale',1);
addParamValue(p,'anglescale',1);
validModelTr = {'translation' 'rotation'};
addParamValue(p,'modeltr','',@(x) any(validatestring(x,validModelTr)));

parse(p,Tr,type,varargin{:});

switch lower(type)
  case 'rigid'
    % make it a row vector
    Tr = Tr(:)';
    
    switch p.Results.modeltr
      case 'translation', Tr = [Tr 0 0 0];
      case 'rotation',    Tr = [0 0 0 Tr];
      otherwise
    end
    
    if numel(Tr) == 6, Tr = [10 Tr]; end
    
    Tr(2) = Tr(2) * p.Results.shiftscale;
    Tr(3) = Tr(3) * p.Results.shiftscale;
    Tr(4) = Tr(4) * p.Results.shiftscale;
    
    Tr(5) = Tr(5) * p.Results.anglescale;
    Tr(6) = Tr(6) * p.Results.anglescale;
    Tr(7) = Tr(7) * p.Results.anglescale;
    
    s = Tr(1); % scale factor
    
    At = [ 0,0,0,Tr(2); ...
           0,0,0,Tr(3); ...
           0,0,0,Tr(4); ...
           0,0,0,0 ]; % x,y,z translation
    
    Ax = [ 1,          0,           0, 0; ...
           0, cos(Tr(5)), -sin(Tr(5)), 0; ...
           0, sin(Tr(5)),  cos(Tr(5)), 0; ...
           0,          0,           0, 1 ]; % x-rotation
    
    Ay = [ cos(Tr(6)), 0, sin(Tr(6)), 0; ...
                    0, 1,          0, 0; ...
          -sin(Tr(6)), 0, cos(Tr(6)), 0; ...
                    0, 0,          0, 1 ]; % y-rotation
    
    Az = [ cos(Tr(7)), -sin(Tr(7)), 0, 0; ...
      sin(Tr(7)),  cos(Tr(7)), 0, 0; ...
      0,           0, 1, 0; ...
      0,           0, 0, 1 ]; % z-rotation
    
    T = Az * Ay * Ax + At; % compute general transform matrix
  case 'affine'
    s = 1;
    T = [reshape(Tr,3,4); 0 0 0 1];
  case 'quasiaffine'
    Tr(4) = Tr(4) * p.Results.shiftscale;
    Tr(5) = Tr(5) * p.Results.shiftscale;
    Tr(6) = Tr(6) * p.Results.shiftscale;
    
    Tr(7) = Tr(7) * p.Results.anglescale;
    Tr(8) = Tr(8) * p.Results.anglescale;
    Tr(9) = Tr(9) * p.Results.anglescale;
    
    s = 1; % fix scale factor
    As = [ Tr(1),0,0,0; ...
      0,Tr(2),0,0; ...
      0,0,Tr(3),0; ...
      0,0,0,1 ]; % scale factor
    
    At = [ 0,0,0,Tr(4); ...
      0,0,0,Tr(5); ...
      0,0,0,Tr(6); ...
      0,0,0,0 ]; % x,y,z translation
    
    Ax = [ 1,0,0,0; ...
      0,cos(Tr(7)),-sin(Tr(7)),0; ...
      0,sin(Tr(7)),cos(Tr(7)),0; ...
      0,0,0,1 ]; % x-rotation
    
    Ay = [ cos(Tr(8)),0,sin(Tr(8)),0; ...
      0,1,0,0; ...
      -sin(Tr(8)),0,cos(Tr(8)),0; ...
      0,0,0,1 ]; % y-rotation
    
    Az = [ cos(Tr(9)),-sin(Tr(9)),0,0; ...
      sin(Tr(9)),cos(Tr(9)),0,0; ...
      0,0,1,0; ...
      0,0,0,1 ]; % z-rotation
    
    % T = (Az * Ay * Ax + At)*As; % compute general transform matrix
    T = Az * Ay * Ax * As + At; % compute general transform matrix
end


end