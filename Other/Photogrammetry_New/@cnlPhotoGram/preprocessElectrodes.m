function [photoObj] = preprocessElectrodes(photoObj)
% function [photoObj] = preprocessSFPFile(photoObj)
%
%  What is the ultimate purpose of doing all this preprocessing?  What do
%  we need later on?

%% Preprocess Data from SFP File
mydisp('Preprocessing Electrode Locations');

% Compute Principle Axes of EEG Cloud
mydisp('Computing Principal Axes of Electrode Point Cloud')
%[G1, G2, photoObj.Np, photoObj.V, photoObj.EVCTp, photoObj.EVALp] = ...
[G1, G2, Np, ~ , ~, ~] = ...
  cnlPhotoGram.PrincipalAxes( photoObj.posEEG, 'indices', 'principal-axes' );

% Total Hack right now
% photoObj.Np = [ 1 0 0 ];

% Do point pair matching
mydisp('Searching for maximal cardinality bipartite graph...')
[photoObj.Psym, photoObj.Wsym, photoObj.MCp] = ...
  cnlPhotoGram.PointPairMatching( photoObj.posEEG, G1, G2, Np, true );

photoObj.MCp = reshape(photoObj.MCp,1,3);
%photoObj.Np  = reshape(photoObj.Np,1,3);

mydisp('Searching for neighbouring point pairs...')
[photoObj.Pnet,photoObj.Wnet] = cnlPhotoGram.PointGrouping( photoObj.posEEG );

mydisp('Finished Preprocessing Electrode Locations');


end