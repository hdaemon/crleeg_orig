function photoObj = preprocessAll(photoObj)
% function photoObj = DataPreprocessing(photoObj)
%
%
%  Calls:
%   cnlPhotoGram.PrincipalAxes
%   cnlPhotoGram.PointPairMatching
%   cnlPhotoGram.PointGrouping
%   cnlPhotoGram.MRISegmentation
%   cnlPhotoGram.MRISymmetryPlane

mydisp('photoObj.DataPreprocessing >> BEGIN');

photoObj = preprocessElectrodes(photoObj);

photoObj = preprocessMRI(photoObj);

% Save Initialization Data
cd(photoObj.matpath)
save InitData.mat photoObj

mydisp('photogObj.DataPreprocessing >> END');
% if isfield(regs.data,'empty'), rmfield(regs.data,'empty'); end
end