% compute rigid transformation between two corresponding point sets
function [T] = regsvd(pts1, pts2)

if size(pts1,1) ~= size(pts2,1), warning('Number of points in both sets must be equal!'), return, end

mu1 = mean(pts1);
mu2 = mean(pts2);

pts1 = pts1 - repmat(mu1,size(pts1,1),1);
pts2 = pts2 - repmat(mu2,size(pts1,1),1); 

H = zeros(3,3);
for i=1:size(pts1,1)
    H = H + pts1(i,:)'*pts2(i,:);
end

[su,sd,sv] = svd(H);
R = sv*su';
if det(R)<0, 
    R = [sv(:,1),sv(:,2),-sv(:,3)] * su'; 
end

D = mu2' - R * mu1';

T = [R, D; 0 0 0 1];

end