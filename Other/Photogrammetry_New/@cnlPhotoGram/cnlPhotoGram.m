classdef cnlPhotoGram 
  
  properties
    
    % Photogrammetry Options
    
    % Optimization options
    transfType   = 'quasiaffine';           % Transform Type
    optimType    = 'powell';          % Optimization Algorithm
    costFuncType = 'hcf';             % Cost Function
    
    datapath     = '';
    respath      = '';
    matpath      = '';
    
    % NRRD Files from MRI
    dir_MRI       = '';
    fName_MRI     = '';                % data file to use
    nrrdMRI
    
    fName_Skin    = '';                % label file to use (skin segmentation)
    nrrdSkin
    
    fName_SFP      = '';                % eeg file to use
    sfpFile
    
    
    edtfile      = '';                % eeg file to use
    reiniteeg    = true;              % reinit all eeg-related data
    bplot        = true;              % draw results by default
    skipinitreg  = false;             % dont skip initial registration
    redopreproc  = true;             % dont redo data preprocessing
    initialization = 'fiducials';     % initialize by matching fiducials
    fidfile      = 'fiducials.mat'; % fiducials file to use
    evaloutput   = false;             % dont export evaluation results
    evalfile     = 'real-evaluation'; % eval file to write to
    
    % For intermediate computations
    posEEG
    labEEG
    
    % Final output values
    posEEG_final

    
    posFID
    labFID
    
    posFIDmri
    
    posFID_final

    
    posStart
    posFIDStart
    
    % Obtained from calling MRISegmentation
    otsu_thresh
    volume
    nrrdOtsuSeg
    
    % Obtained from calling PrincipalAxes
    G1
    G2
  %  Np
  %  V
  %  EVCTp
  %  EVALp
    
    Psym = [];
    Wsym = [];
    MCp
    
    Pnet = [];    
    Wnet = [];
    
    Nmri
    MCmri
   % EVCTmri
   % EVALmri
    
    sampling
    anglescale
    shiftscale
    X
    Y
    Z
    
    D
    DT
    T
    mxyz
    
    imghdr
    
    % Cost Function Options and Function Handle
    hcfopt
    icpopt
    cf
    
    Href
    sph
    xbins
    
    indout
    
    initTr
    optimTr
    optimTrParams
    
    CostFuncObj
    
  end
  
  methods
    
    function obj = cnlPhotoGram(varargin)
      % function obj = cnlPhotoGram(varargin)
      %
      %
      mydisp('BEGIN');
      
      if nargin>0
      % Parse Inputs
      if length(varargin) > 1
        nvars = 1;
        while nvars < length(varargin)
          if ~ischar(varargin{nvars}),
            error('Error: field name should be a character array!');
          end
          if nvars+1 <= length(varargin),
            obj.(varargin{nvars}) = varargin{nvars+1};
          end
          nvars = nvars + 2;
        end
      end
      
      % Input checking
      if isempty(obj.fName_MRI),  error('You must supply an MRI file'); end;
      if isempty(obj.fName_Skin), error('You must supply a Skin Segmentation'); end;
      if isempty(obj.fName_SFP),  error('You must supply an SFP File'); end;
                  
      % Check for the existence of necessary directories
      if ~isdir(obj.respath)
        mkdir(obj.respath);
      end;
      
      if ~isdir(obj.matpath)
        mkdir(obj.matpath);
      end;
      
      % Read the NRRDs in.      
      mydisp('Reading MRI File');
      obj.nrrdMRI  = file_NRRD(obj.fName_MRI,obj.dir_MRI);      
      mydisp('Reading Skin Segmentation File');
      obj.nrrdSkin = file_NRRD(obj.fName_Skin,obj.dir_MRI);
      
      % Read in the SFP File      
      mydisp('Reading SFP File');
      obj.sfpFile = file_SFP(obj.fName_SFP,obj.datapath);
      obj.posEEG = obj.sfpFile.ElecLoc*10;
      obj.posFID = obj.sfpFile.FidLoc*10;
      obj.labEEG = obj.sfpFile.ElecLabel;
      obj.labFID = obj.sfpFile.FidLabel;
      
      mydisp('END');
      end;
    end
    
    % The main function to compute a photogrammetry solution
    [photoObj] = registerEEG(photoObj);
    
    % The HCF Cost Function
    [ varargout ] = HCF( photoObj,fcn, varargin );
    
    % Function to output a cnlElectrodes object
    ElecOut = getElectrodeObj(photoObj,type);
  end;
  
  methods (Access=private);
    % These methods are all called by cnlPhotoGram.registerEEG.  They are
    % called in the order presented below.
    
    [photoObj] = DataPreprocessing(photoObj)
        
    [photoObj] = InitialRegistration( photoObj );
    
    [photoObj] = InitializeCostFunction( photoObj )
    
    [ photoObj ] = CostFunctionOptimization( photoObj )
    
    % MRI segmentation for Photogrammetry
    %  This is called in cnlPhotoGram.dataPreProcessing.
    [photoObj] = MRISegmentation(photoObj,method);
  end
  
  methods (Static=true)
    [X, Y, Z] = getXYZFromMRI(nrrdMRI);
    H = mexPartVolInterp(Hpoints,data,sampling,X,Y,Z,Xbins);
    KL = kldiv(varValue,pVect1,pVect2,type);
  end
  
  methods (Static=true,Access=private)
    [Iseg,sep,thr] = otsu(I,n);
    [G1, G2, varargout] = PrincipalAxes( points, export, selsplit );
    [P, varargout] = PointPairMatching( points, G1, G2, N, bplot );
    [P, varargout] = PointGrouping( points, distthr, bplot );
    [finalABC, mc, varargout] = MRISymmetryPlane( photoObj, path,optimization, T, xdim, q );
    [T] = regsvd(pts1, pts2);
    posNew = getTransformedPoints( posOld, T, s );
    [T,s] = getTransformMatrix(Tr,type,varargin);
    hcfopt = GetHCFOptions( varargin );
  end
end