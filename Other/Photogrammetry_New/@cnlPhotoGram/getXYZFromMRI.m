function [X Y Z] = getXYZFromMRI(nrrdMRI)
% function [X Y Z] = getXYZFromMRI(nrrdMRI)
%
%

sampling = nrrdMRI.getAspect;
nrrdSizes = nrrdMRI.sizes;

X = sampling(1) .* [(-nrrdSizes(1)/2+1) : nrrdSizes(1)/2];
Y = sampling(2) .* [(-nrrdSizes(2)/2+1) : nrrdSizes(2)/2];
Z = sampling(3) .* [(-nrrdSizes(3)/2+1) : nrrdSizes(3)/2];

end