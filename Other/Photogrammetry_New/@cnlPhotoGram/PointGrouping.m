% Point grouping
% -------------------------------------------------------------------------
% Description:
% 
% Usage: 
%	P = maPointGrouping( points )
%	[P,W] = maPointGrouping( points )
% 
% Input:
%   - points: EEG point coordinates
%   - distthr*: threshold on point-pair distance
%   - bplot*: plot intermediate steps
% 
% Output:
%   - P: indexes of point pairs
%   - W*: corresponding weights (Euclidean distances)
%
% Comments:
% 
% Notes:
% 
% -------------------------------------------------------------------------

function [P, varargout] = PointGrouping( points, distthr, bplot )

if nargin < 3, bplot = false; end
if nargin < 2, distthr = 3; end

%% compute Euclidean distances between each point pair
count = size(points,1);
dist = zeros(count*(count-1)/2,1);
P = zeros(count*(count-1)/2,2);
ind = 1;
for i = 1 : count-1
    for j = i+1 : count
        if ind>numel(dist), break; end
        P(ind,:) = [i,j];
        dist(ind) = norm(points(i,:)-points(j,:));        
%         dist(ind) = sqrt(sum((points(i,:)-points(j,:)).^2));
        ind = ind + 1;
    end
end

%% histogram of Euclidean distances
% hist(dist,floor(sqrt(numel(dist))));

%% plot point cloud and connections
% distthr = 3;
% bplot = true;

count = sum(dist<distthr); % number of connections to plot
count = 0;

% sort edges according to ascending distance
[dist,IX] = sort(dist);
Psort = P(IX,:);

% take only first #count of edges
E1 = points(Psort(1:count,1),:);
E2 = points(Psort(1:count,2),:);
% hist(sqrt(sum((E1-E2).^2,2)))

if bplot
%     figure(gcf)
    % plot points
    plot3(points(:,1),points(:,2),points(:,3),'+b'), hold on
    % plot connection between closest points
    for i = 1 : count
        plot3([E1(i,1);E2(i,1)],[E1(i,2);E2(i,2)],[E1(i,3);E2(i,3)],'-m','LineWidth',2)
    end
    hold off    
end

P = Psort(1:count,:);
W = dist(1:count);


if nargout>1, varargout{1} = W; end

end






