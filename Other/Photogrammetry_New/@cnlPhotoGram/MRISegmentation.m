% MRI Segmentation Methods
% -------------------------------------------------------------------------
% Description:
% 	MRI segmentation
%
% Usage:
%	[segImage] = maMRISegmentation('grow3D', 'bestt1w-us.nhdr')
%   [segImage,thr] = maMRISegmentation('otsu', 'bestt1w-us.nhdr')
%
% Input:
%   - method: 'otsu' or 'grow3D'
%   - datafile: select datafile
%   - casestr*: case to initialize
%
% Output:
%   - segImage*: segmented image
%   - thr*: computed threshold for Otsu method
%
% Comments:
%
% Notes:
%
% -------------------------------------------------------------------------
%% Segmentation
%   - Otsu grey-level thresholding
%   - 3D region growing

function [photoObj] = MRISegmentation(photoObj,method)

mydisp('cnlPhotoGram.MRISegmentation >> BEGIN');

matpath = photoObj.matpath;
%mripath = photoObj.mripath;

% This should probably be moved elsewhere, btu we'll leave it here for now
%photoObj.MRInrrd = file_NRRD(photoObj.mrifile,photoObj.mripath);

switch method
  case 'skinseg'
    mydisp('cnlPhotoGram.MRISegmentation >> Using predefined skin segmentation');
    photoObj.volume = obj.nrrdSkin.data;
  case 'otsu'
    mydisp('cnlPhotoGram.MRISegmentation >> Using Otsu segmentation of MRI');
    %% Otsu thresholding
    
    %photoObj.MRInrrd = file_NRRD(photoObj.mrifile,photoObj.mripath);
    
    [volume,sep,thr] = cnlPhotoGram.otsu(photoObj.nrrdMRI.data,2);
    
    photoObj.volume        = volume; % Segmentated volume
    photoObj.otsu_thresh   = thr;    % Threshold for segmentation
    photoObj.nrrdOtsuSeg = clone(photoObj.nrrdMRI,'OtsuSeg.nhdr',matpath);
  case 'grow3D'
    error('Region growing approach not currently supported');
    %         %% 3d region growing
    %         seed = [5 5 5];
    %         maxdist = 18.5;
    %
    %         % Wtf is going on here?
    %         volume = mexrg3d(volume,seed,maxdist);
    %         photoObj.volume = volume;
  otherwise
end

if ~isempty(matpath), save([matpath,'segmentation-',method,'.mat'],'volume'); end

mydisp('cnlPhotoGram.MRISegmentation >> END');
end