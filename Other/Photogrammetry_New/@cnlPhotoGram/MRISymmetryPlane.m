% Find symmetry plane in MRI
% -------------------------------------------------------------------------
% Description:
% 	[finalPlane, mc] = maMRISymmetryPlane( 'bestt1w-us.nhdr', 'simplex', 10, 32, 20 )
%
% Usage:
%   maDisplayRegRes( 'init' )
%   maDisplayRegRes( 'draw' )
%   maDisplayRegRes( 'draw', posNew, posNear )
%   maDisplayRegRes( 'drawpoints', posNew )
%   maDisplayRegRes( 'kill' )
%
% Input:
%   - filename: MRI dataset
%   - optimization: 'simplex' or 'steepdesc' or ...
%   - T: condition points by intensity threshold( T )
%   - xdim: new x dimension (integer, can also be a vector for multistage)
%   - q: space quantization for initial parameters estimation
%
% Output:
%   - finalPlane: parameters of mid-sagittal plane
%   - mc: center of points
%   - v*: eigenvectors of downsampled volume
%   - e*: eigenvalues of downsampled volume
%
% Comments:
%
% Notes:
%   Implementation from Ardekani et al. (1997) - Automatic detection
%   of the mid-sagittal plane in 3D brain images. Transactions on
%   medical imaging, vol.16, no.6.
%
% -------------------------------------------------------------------------

function [finalPlane, mc, varargout] = MRISymmetryPlane( nrrdMRI, varargin)
  
  p = inputParser;
  addRequired(p,'nrrdMRI',@(x) isa(x,'file_NRRD'));
  addParamValue(p,'optimization','simplex')
  addParamValue(p,'T',50);
  addParamValue(p,'xdim',32);
  addParamValue(p,'q',20);
  parse(p,nrrdMRI,varargin{:});
  
  optimization = p.Results.optimization;
  T    = p.Results.T;
  xdim = p.Results.xdim;
  q    = p.Results.q;
  
  %datafile = ['maMRI-resampled-xdim',num2str(xdim(1)),'.mat'];
  
  % Load MRI Datafile, either from the original NRRD or from the MAT file.
  img = maResampleImage(nrrdMRI,xdim(1));
     
  % Get the center of mass estimate.  We don't use the principle axes, just
  % the center of mass
  mydisp('Finding initial estimation of plane parameters...')
  [ img.mxyz, evects, eigval ] = getCOMandPrincpAxes( img, T );
  mc = img.mxyz;
  
  % Get the starting symmetry plane by brute force
  mydisp('Getting the starting plane');
  img.T = T;
  [startPlane] = getStartingPlane(img, q);
  
  %% setup & run optimization
  cf = @(Plane) symmetryCF(Plane,img); % criterion function handle
  mydisp('Parameter optimization... stage 1.')
  switch optimization
    case 'simplex'
      simopt = optimset( 'Display','final',...
        'MaxFunEvals',1000,...
        'MaxIter',200,...
        'TolFun',1e-3,...
        'TolX',1e-3 ); % setup
      
      finalPlane = fminsearch( cf, startPlane, simopt ); % run
      
    case {'bfgs','dfp','steepdesc'}
      simopt = optimset( 'LargeScale','off',...
        'GradObj','off',...
        'HessUpdate', optimization,...
        'InitialHessType','identity',...
        'Display','final',...
        'MaxFunEvals',1000,...
        'MaxIter',100,...
        'TolFun',1e-3,...
        'TolX',1e-3 ); % setup
      
      finalPlane = fminunc( cf, startPlane, simopt ); % run
      
    otherwise
      return;
  end
  
  finalPlane = finalPlane ./ norm(finalPlane);
  if length(xdim)>1
    for i = 2 : length(xdim)
      % check if resampled volume exists
     % datafile = ['maMRI-resampled-xdim',num2str(xdim(i)),'.mat'];
      
      if exist(datafile,'file')
        load(datafile,'photoObj');
        photoObj.D(find(isnan(photoObj.D))) = 0;
      else
        mydisp(['Downsampling volume... stage ',num2str(i),'.'])
        if ~exist('hdrMRI','var'), hdrMRI = maReadNrrdHeader( filename ); end % load dataset
        photoObj = maResampleImage( hdrMRI, xdim(i) ); % downsample image
        save([matpath,datafile],'photoObj');
      end
      
      [ mc, evects, eigval] = getCOMandPrincpAxes( img, T ); % recompute center of mass
      img.mxyz = mc;
      %photoObj.T = T; % set intensity threshold value
      
      mydisp(['Parameter optimization... stage ',num2str(i),'.'])
      cf = @(Plane) maSymmetryCF(Plane,photoObj); % criterion function handle
      switch optimization
        case 'simplex', finalPlane = fminsearch( cf, finalPlane, simopt ); % run
        case {'bfgs','dfp','steepdesc'}, finalPlane = fminunc( cf, finalPlane, simopt ); % run
      end
      finalPlane = finalPlane ./ norm(finalPlane);
    end
  end
  
  % output other data
  if nargout>2, varargout{1} = evects; end
  if nargout>3, varargout{2} = eigval; end
  
end

%% compute object center
function [ mc, evects, eigval] = getCOMandPrincpAxes( img, thr )
  mydisp('cnlPhotoGram.MRISymmetryPlane.getCOMandPrincpAxes >> BEGIN');
  warning('Not really sure what this is doing.  Should probably check on it');
  % get center of mass from MRI image
  % M = sum(img.D(:));
  % mxyz = [0,0,0];
  % mxyz(1) = sum(img.D(:) .* img.Y(:)) / M; % x-dim
  % mxyz(2) = sum(img.D(:) .* img.X(:)) / M; % y-dim
  % mxyz(3) = sum(img.D(:) .* img.Z(:)) / M; % z-dim
  % img.mxyz = mxyz;
  % mc = mxyz;
  
  bUseWeighted = false;
  if bUseWeighted
    % Compute the center of mass
    ind = find(img.D>thr);
    avgX = mean(img.D(ind) .* img.Y(ind));
    avgY = mean(img.D(ind) .* img.X(ind));
    avgZ = mean(img.D(ind) .* img.Z(ind));
    avgW = mean(img.D(ind));
    mc = [avgX avgY avgZ] ./ avgW;
    img.mxyz = mc;
    
    % compute covariance matrix
    covmat = zeros(3,3);
    for i=1:numel(ind)
      pos = [img.Y(ind(i)),img.X(ind(i)),img.Z(ind(i))];
      covmat = covmat + img.D(ind(i)) * [pos - mc]'*[pos - mc];
    end
    covmat = covmat ./ numel(ind);
    
    % get its eigen -vectors and -values
    [evects,eigval] = eig(covmat);
  else
    % THis is never actually reached, because of the hardcoding above.
    ind = find(img.D>thr);
    avgX = mean(img.Y(ind));
    avgY = mean(img.X(ind));
    avgZ = mean(img.Z(ind));
    mc = [avgX avgY avgZ];
    img.mxyz = mc;
    % compute covariance matrix
    covmat = zeros(3,3);
    for i=1:numel(ind)
      pos = [img.Y(ind(i)),img.X(ind(i)),img.Z(ind(i))];
      covmat = covmat + [pos - mc]'*[pos - mc];
    end
    covmat = covmat ./ numel(ind);
    %    keyboard;
    % get its eigen -vectors and -values
    [evects,eigval] = eig(covmat);
  end
  
  mydisp('cnlPhotoGram.MRISymmetryPlane.getCOMandPrincpAxes >> END');
end

%% compute initial plane parameters
function [ startPlane ] = getStartingPlane( img, q)
  % function [ startPlane ] = getStartingPlane( img, q, mc )
  %
  % It looks like this just tries a whole bunch of symmetry planes and brute
  % force computes the symmetry cost function for each of them, selecting the
  % best one at the end.
  
  mydisp('BEGIN');
  maxval = 0;
  startPlane = [1,0,0];
     
  % Starting orientations
  dphi = pi / (2*q);
  
  % Loop across orientations
  for i = 0 : q
    phi = i*dphi;                        % Compute Current Angle
    nj = ceil(q*sin(phi)+1e-3);          % get number of theta samples
    alpha = 2*pi*rand();                 % get alpha offset
    theta = linspace(alpha,alpha+pi,nj); % get theta starting positions
    
    
    for j = 1 : nj
      % Get Plane Parameters and Compute Cost
      testPlane = [sin(phi)*cos(theta(j)),sin(phi)*sin(theta(j)),cos(phi)];
      cval = symmetryCF( testPlane, img );
      
      % compute symmetry measure, find max & save its parameters
      if cval<maxval % CF has (-) sign
        maxval = cval;
        startPlane = testPlane;
      end
            
    end
  end
  
  startPlane = startPlane ./ norm(startPlane); % normalize output
    
  % figure
  % hist(vals(find(vals~=0)))
  
  mydisp('END');
end

%% criterion function for simmetry detection
function cval = symmetryCF( Plane, img )  
  mydisp('BEGIN',5);
  
  % Normalize Input Vector
  Plane = reshape(Plane,3,1); 
  Plane = Plane./norm(Plane);
  
  % Get Plane Offset
  poff = sum(Plane(:) .* img.mxyz(:));
  
  % Find points above threshold
  ind_DgtT = find(img.D(:)>img.T); % find f(i,j,k)>T
  
  % Get Image Values and Locations
  Ft = img.D(ind_DgtT); 
  P = [img.Y(ind_DgtT),img.X(ind_DgtT),img.Z(ind_DgtT)]; 
  
  % Take inner products and subtract offset
  Fp = (P * Plane - poff); % insert points P into plane equation  
  indPgt0 = find(Fp>0); % find F(P)>0
  P  =  P(indPgt0,:); 
  Fp = Fp(indPgt0); 
  Ft = Ft(indPgt0); % remove corresponding point locations and projections
  Pr = P - 2 * Fp * Plane' / (norm(Plane)^2); % find reflection points
  
  % % remove points that fall out of range
  % ind = find(Pr(:,1)>=img.X(1)); Ft = Ft(ind); Pr = Pr(ind,:);
  % ind = find(Pr(:,1)<=img.X(end)); Ft = Ft(ind); Pr = Pr(ind,:);
  % ind = find(Pr(:,2)>=img.Y(1)); Ft = Ft(ind); Pr = Pr(ind,:);
  % ind = find(Pr(:,2)<=img.Y(end)); Ft = Ft(ind); Pr = Pr(ind,:);
  % ind = find(Pr(:,3)>=img.Z(1)); Ft = Ft(ind); Pr = Pr(ind,:);
  % ind = find(Pr(:,3)<=img.Z(end)); Ft = Ft(ind); Pr = Pr(ind,:);
  
  % A much better way to do this:
  insideXBnd = (Pr(:,1)>=img.X(1))&(Pr(:,1)<=img.X(end));
  insideYBnd = (Pr(:,2)>=img.Y(1))&(Pr(:,2)<=img.Y(end));
  insideZBnd = (Pr(:,3)>=img.Z(1))&(Pr(:,3)<=img.Z(end));
  idxInside = find(insideXBnd & insideYBnd & insideZBnd);
  Ft = Ft(idxInside); 
  Pr = Pr(idxInside,:);
  
  % interpolation of values at points Pr
  Fr = interp3(img.X,img.Y,img.Z,img.D,Pr(:,1),Pr(:,2),Pr(:,3),'linear');
  
  % Drop NaN Values
  ind = isnan(Fr); 
  Fr = Fr(~ind); 
  Ft = Ft(~ind);
  
  % compute cross-correlation
  Ft = Ft - mean(Ft);
  Fr = Fr - mean(Fr);
  cval = -(Ft' * Fr) / (norm(Ft) * norm(Fr));
  
  mydisp('END',5);
end


%% image resampling
function photoObj = maResampleImage( hdrMRI, xdim )
  
  warning('cnlPhotoGram.maResampleImage should be integrated into the file_NRRD object');
  
  %% get sampling grid
  sdist = [ sqrt(sum(hdrMRI.spacedirections(1,:).^2)),...
    sqrt(sum(hdrMRI.spacedirections(2,:).^2)),...
    sqrt(sum(hdrMRI.spacedirections(3,:).^2)) ]; % sampling distances x-y-z
  
  sdist = hdrMRI.getAspect;
  
  x = sdist(1)*((-hdrMRI.sizes(1)/2+1) : hdrMRI.sizes(1)/2); % x-sampling grid
  y = sdist(2)*((-hdrMRI.sizes(2)/2+1) : hdrMRI.sizes(2)/2); % y-sampling grid
  z = sdist(3)*((-hdrMRI.sizes(3)/2+1) : hdrMRI.sizes(3)/2); % z-sampling grid
  % [ Xg,Yg,Zg ] = meshgrid( y,x,z ); % generate grid
  
  %% set isotropic sampling and y,z volume dimensions
  sampling = sdist(1) * (hdrMRI.sizes(1) - 1) / xdim;
  ydim = ceil(sdist(2) * (hdrMRI.sizes(2) - 1) / sampling);
  zdim = ceil(sdist(3) * (hdrMRI.sizes(3) - 1) / sampling);
  
  %% setup gaussian filter & filter the image
  Dg = hdrMRI.data;
  clear hdrMRI
  r = 2; % sampling * r == FWHM
  sigma = sqrt((sampling^2-sdist(1)^2) * r^2 / log(256)); % std
  xl = 1.65 * sigma / sdist(1); % estimate kernel size
  ks = 3; if xl > 3, ks = 5; end % set kernel size
  disp(['  Gaussian filtering... kernel: [',num2str([ks ks ks]),'] sigma: ',num2str(sigma)])
  Dg = smooth3(Dg,'gaussian',[ks ks ks],sigma); % smooth volume data
    
  %% resample the image
  [ Xg,Yg,Zg ] = meshgrid( y,x,z ); % generate grid
  disp(['  Volume resampling... x:',num2str(xdim),' y:',num2str(ydim),' z:',num2str(zdim)])
  x = sampling*((-xdim/2+1) : xdim/2); % x-sampling grid
  y = sampling*((-ydim/2+1) : ydim/2); % y-sampling grid
  z = sampling*((-zdim/2+1) : zdim/2); % z-sampling grid
  [ X,Y,Z ] = meshgrid( y,x,z ); % generate grid
  D = interp3(Xg,Yg,Zg,Dg,X,Y,Z,'linear'); % interpolate
  D(find(isnan(D))) = 0;
  clear Xg Yg Zg Dg
  
  %% fill output structure
  photoObj.D = D;
  photoObj.X = X;
  photoObj.Y = Y;
  photoObj.Z = Z;
end




