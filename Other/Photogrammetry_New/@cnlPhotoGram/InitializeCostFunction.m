% Initialize cost function
% -------------------------------------------------------------------------
% Description:
% 	Initialize cost function for iterative registration
%
% Usage:
%	regs = maInitializeCostFunction( regs );
%
% Input:
%   - regs: structure with initialized settings
%
% Output:
%   - regs: structure with cost function handler
%
% Comments:
%
% Notes:
%
% -------------------------------------------------------------------------

function photoObj = InitializeCostFunction( photoObj )
  mydisp('photogObj.InitializeCostFunction >> BEGIN');
  
  %% init similarity measure
  switch photoObj.costFuncType
    case 'icp'
      error('ICP Cost Function Not Currently Implemented');
%       mydisp('photogObj.InitializeCostFunction >> Initializing ICP Cost Function');      
%       mydisp('photogObj.InitializeCostFunction >> Loading Data From DistanceTransform.mat');
%       load('DistanceTransform.mat','DT');
%       photoObj.DT = DT;
%       clear DT;
%       mode = 'rms';
%       
%       if isempty(photoObj.icpopt)
%         photoObj.icpopt = GetICPOptions( 'mode', mode );
%       end
%       
%       % initialize hcf
%       photoObj = photoObj.ICP( 'init');
%       
%       % set function handle of criterion function
%       photoObj.cf = @(points) photoObj.ICP('exec', points);
      
    case 'hcf'
      
      mydisp('Initializing HCF Cost Function');
      
      % Set up the options structure
      opts.mode = 'global-localnet';                 
      opts.step = min(photoObj.sampling);
      opts.Psym = photoObj.Psym;
      opts.Pnet = photoObj.Pnet;
      opts.Wsym = photoObj.Wsym;
      opts.Wnet = photoObj.Wnet;
      opts.otsu_thresh = photoObj.otsu_thresh;
      opts.sampling = photoObj.sampling;
            
      % Call object constructor, and then initialization
      photoObj.CostFuncObj = cnlCostFunc_Histogram(opts);
      photoObj.CostFuncObj = photoObj.CostFuncObj.init(photoObj.nrrdMRI);
     
      
%       if isempty(photoObj.hcfopt)
%         photoObj.hcfopt = cnlPhotoGram.GetHCFOptions( 'mode', mode,...
%           'radius', 10,...
%           'step', min(photoObj.sampling),...
%           'interpolation', 'mexpartvol',...
%           'binning', 32 );
%       end
%       
%       % initialize hcf
%       photoObj = photoObj.HCF( 'init');
      
      % Set function handle of criterion function
      photoObj.cf = @(points) photoObj.CostFuncObj.exec(points);
      
    otherwise
      error('ERROR: cost function not recognized!')
  end
  
  mydisp('END');
end

