function ElecOut = getElectrodeObj(photoObj,type)
% function ElecOut = getElectrodeObj(photoObj,type)
%
% Method for cnlPhotogrammetryObj
%
% Given the current state of the object, returns one of several sets of
% electrode locations as a cnlElectrode object
%
% Options for 'type':
%   mrifid : Returns the fiducials marked on the MRI
%   start  : Returns the electrode locatiosn at the start of the
%              optimization
%   curr   : Returns the current electrode locations.  Mostly useful for
%              debugging of the photogrammetry code
%   final  : Returns the final locations, after running Ziga's optimization
%
% Written By: D. Hyde
% Part of the cnlEEG project
% Originally written at some point.
% Edited March 6 2015
%
 
if ~exist('type','var'), type = 'curr'; end;

switch lower(type)
  case 'mrifid'
    ElecOut = cnlElectrodes;
    ElecOut.Positions    = photoObj.posFIDmri;
    ElecOut.Labels       = photoObj.labFID;
    ElecOut.FIDPositions = photoObj.posFIDmri;
    ElecOut.FIDLabels    = photoObj.labFID;
  case 'start'
    ElecOut = cnlElectrodes;
    ElecOut.Positions    = photoObj.posStart;
    ElecOut.Labels       = photoObj.labEEG;
    ElecOut.FIDPositions = photoObj.posFIDStart;
    ElecOut.FIDLabels    = photoObj.labFID;    
  case 'curr'
    ElecOut = cnlElectrodes;
    ElecOut.Positions    = photoObj.posEEG;
    ElecOut.Labels       = photoObj.labEEG;
    ElecOut.FIDPositions = photoObj.posFID;
    ElecOut.FIDLabels    = photoObj.labFID;
  case 'final'
    ElecOut = cnlElectrodes;
    ElecOut.Positions    = photoObj.posEEG_final;
    ElecOut.Labels       = photoObj.labEEG;
    ElecOut.FIDPositions = photoObj.posFID_final;
    ElecOut.FIDLabels    = photoObj.labFID;
end

for i = 1:size(ElecOut.Positions,1)
  [~,x] = min(abs(ElecOut.Positions(i,1)-photoObj.X));
  [~,y] = min(abs(ElecOut.Positions(i,2)-photoObj.Y));
  [~,z] = min(abs(ElecOut.Positions(i,3)-photoObj.Z));
  ElecOut.Positions(i,:) = [x y z];
end

for i = 1:size(ElecOut.FIDPositions,1)
  [~,x] = min(abs(ElecOut.FIDPositions(i,1)-photoObj.X));
  [~,y] = min(abs(ElecOut.FIDPositions(i,2)-photoObj.Y));
  [~,z] = min(abs(ElecOut.FIDPositions(i,3)-photoObj.Z));
  ElecOut.FIDPositions(i,:) = [x y z];
end

% % In photogrammetry calculations, the MRI grid is centered on [0 0 0].
% % This is not necessarily true for the NRRD itself.  So we want to subtract
% % off the center position so that locations are referenced with respect to
% % the center of the NRRD.
% center = photoObj.nrrdMRI.gridSpace.center;
% ElecOut.Positions = ElecOut.Positions + repmat(center,ElecOut.nElec,1);
% ElecOut.FIDPositions = ElecOut.FIDPositions + repmat(center,size(ElecOut.FIDPositions,1),1);
% 
% % Subtract off the origin so locations are now relative to the NRRD origin
% origin = photoObj.nrrdMRI.gridSpace.origin;
% ElecOut.Positions = ElecOut.Positions - repmat(origin,ElecOut.nElec,1);
% ElecOut.FIDPositions = ElecOut.FIDPositions - repmat(origin,size(ElecOut.FIDPositions,1),1);
% 
% % Get locations in terms of number of voxels
% aspect = photoObj.nrrdMRI.getAspect;
% ElecOut.Positions = ElecOut.Positions*diag(1./aspect);
% ElecOut.FIDPositions = ElecOut.FIDPositions*diag(1./aspect);

% Compute actual XYZ locations in NRRD space.
dirs = photoObj.nrrdMRI.spacedirections;
origin = photoObj.nrrdMRI.gridSpace.origin;
ElecOut.Positions = (dirs*ElecOut.Positions')' + repmat(origin,ElecOut.nElec,1);
ElecOut.FIDPositions = (dirs*ElecOut.FIDPositions')' + repmat(origin,size(ElecOut.FIDPositions,1),1);

end