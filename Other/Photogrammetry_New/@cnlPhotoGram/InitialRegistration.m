% Initial closed-form rigid registration
% -------------------------------------------------------------------------
% Description:
% 	Compute the initial closed-form rigid transformation
% 
% Usage: 
%	photoObj = maInitialRegistration( photoObj ); 
%       
% Input:
%   - photoObj: structure with initialized settings
%   
% Output:
%   - photoObj: structure with initial transformation
%
% Comments:
% 
% Notes:
% 
% -------------------------------------------------------------------------

% Parameters we need:
% matpath
% fidfile
% initialization
% posEEG
% posFID
%
% Parameters Set in this Function:
% photoObj.initTr
% photoObj.posStart
% photoObj.posFIDStart

function photoObj = InitialRegistration( photoObj )
mydisp('photogObj.InitialRegistration >> BEGIN');

if exist([photoObj.datapath photoObj.fidfile],'file') && strcmp(photoObj.initialization,'fiducials')
  mydisp('photogObj.InitialRegistration >> Using fiducials file');
    load([photoObj.datapath photoObj.fidfile])
    coords = Fiducials.Positions;
    labels = Fiducials.Labels;
        
    photoObj.posFIDmri = coords;
    
    EEGpts = zeros(4,3);
    MRIpts = zeros(4,3);    
    posEEG = photoObj.posEEG;
    posFID = photoObj.posFID;
    
    % find Nasion
    ind1 = find(strcmp(photoObj.labFID,'FidNz'));
    ind2 = find(strcmp(labels,'Nas'));    
    EEGpts(1,:) = photoObj.posFID(ind1,:);
    MRIpts(1,:) = coords(ind2,:);    
    
    % find Nasion
    ind1 = find(strcmp(photoObj.labFID,'FidT10'));
    ind2 = find(strcmp(labels,'RPA'));    
    EEGpts(2,:) = photoObj.posFID(ind1,:);
    MRIpts(2,:) = coords(ind2,:);  
    
    % find Nasion
    ind1 = find(strcmp(photoObj.labFID,'FidT9'));
    ind2 = find(strcmp(labels,'LPA'));    
    EEGpts(3,:) = photoObj.posFID(ind1,:);
    MRIpts(3,:) = coords(ind2,:);  
    
    % find Nasion
    ind1 = find(strcmp(photoObj.labFID,'Cz'));
    ind2 = find(strcmp(labels,'Cz'));    
    EEGpts(4,:) = photoObj.posFID(ind1,:);
    MRIpts(4,:) = coords(ind2,:);      
 
    EEGpts = EEGpts;       
        
    MRIpts = round(MRIpts);
    MRIpts = [photoObj.X(MRIpts(:,1))', photoObj.Y(MRIpts(:,2))', photoObj.Z(MRIpts(:,3))'];

    T = cnlPhotoGram.regsvd(EEGpts, MRIpts);
    photoObj.initTr = T;
    photoObj.posStart    =  cnlPhotoGram.getTransformedPoints( posEEG, T, 1 );   
    photoObj.posFIDStart =  cnlPhotoGram.getTransformedPoints( posFID, T, 1 );
    photoObj.posFIDmri = MRIpts;

%     bplot = true;
%     if bplot, drawSurfAndAxes(photoObj, T, photoObj.posStart, 'fiducials init'); end                
%  keyboard;  
%     posStart =  getTransformedPoints( photoObj.posEEG, T, 10 );
%     maDisplayRegRes( 'draw', posStart );
else
  error('You need to generate a fiducials file first!');
end

mydisp('photoObj.InitialRegistration >> END');
end    
    
