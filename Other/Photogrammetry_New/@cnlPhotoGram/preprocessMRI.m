function [photoObj] = preprocessMRI(photoObj)
% function [photoObj] = preprocessMRI(photoObj)
%
%

%% Preprocess MRI Data
% Compute Otsu Segmentation
% all that's changed is photoObj.volume and potentially
% photoObj.otsu_thresh
mydisp('Performing Otsu segmentation...')
photoObj = photoObj.MRISegmentation('otsu');

mydisp('Searching for MRI Symmetry plane');
if ~isempty(photoObj.otsu_thresh), thresh = photoObj.otsu_thresh; else thresh = 10; end
%[photoObj.Nmri, photoObj.MCmri, photoObj.EVCTmri, photoObj.EVALmri] = ...
[photoObj.Nmri, photoObj.MCmri, ~, ~] = ...
  cnlPhotoGram.MRISymmetryPlane( photoObj.nrrdMRI, 'T', thresh );

photoObj.MCmri = reshape(photoObj.MCmri,1,3);
photoObj.Nmri  = reshape(photoObj.Nmri,1,3);

% sampling distances x-y-z
photoObj.sampling = photoObj.nrrdMRI.getAspect;

% apply parameter space scaling (for rotation parameters)
% anglescale = asin((sampling*sqrt(sum(size(DT).^2))/4)^(-1));
cubediag = 0.25*sqrt(sum((photoObj.nrrdMRI.sizes .* photoObj.nrrdMRI.getAspect).^2));
photoObj.anglescale = asin(inv(cubediag));
photoObj.shiftscale = 1;

% x-y-z sampling grid
%
% Why are we insisting on sampling on a zero-centered grid.  Is there
% any real reason?  Why couldn't we just sample on the original
% spatial grid of the NRRD (In which case, we can just use
warning('Should probably be using a cnlGridSpace here');
mydisp('Computing sampling grid');
[photoObj.X, photoObj.Y, photoObj.Z] = cnlPhotoGram.getXYZFromMRI(photoObj.nrrdMRI);

end