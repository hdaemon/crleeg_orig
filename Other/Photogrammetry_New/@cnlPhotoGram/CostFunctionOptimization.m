% Optimize cost function
% -------------------------------------------------------------------------
% Description:
% 	Cost function optimization
%
% Usage:
%	photoObj = maCostFunctionOptimization( photoObj );
%
% Input:
%   - photoObj: structure with initialized settings
%
% Output:
%   - photoObj: structure with cost function handler
%
% Comments:
%
% Notes:
%
% -------------------------------------------------------------------------

function photoObj = CostFunctionOptimization( photoObj )

%% Add Initial Transform, if not yet set
% Why is is even a possibility that we don't have an initial transform???
if isempty(photoObj.initTr), photoObj.initTr = eye(4); end

%% Configure Cost Fuction
switch photoObj.transfType
  case {'translation','rotation'},
    mydisp('Obtaining translate and rotation');
    cf = @(Tr) maCriterionScaleRigid( Tr, photoObj );  initTr = zeros(1,3);
  case 'scalerigid',
    mydisp('Obtaining scaled rigid transformation');
    cf = @(Tr) maCriterionScaleRigid( Tr, photoObj );  initTr = zeros(1,7);
  case 'rigid',
    mydisp('Obtaining rigid transformation');
    cf = @(Tr) maCriterionScaleRigid( Tr, photoObj );  initTr = zeros(1,6);
  case 'affine',
    mydisp('Obtaining affince transformation');
    cf = @(Tr) maCriterionAffine( Tr, photoObj );      initTr = reshape(eye(3,4),1,12);
  case 'quasiaffine',
    mydisp('Obtaining quasiaffine transformation');
    cf = @(Tr) maCriterionQuasiAffine( Tr, photoObj ); initTr = [ones(1,3) zeros(1,6)];
  otherwise, error('ERROR: transformation model not recognized!')
end

outfcn = @(x,optimValues,state) maOutputFcn(x,optimValues,state,photoObj);

%%  Run Optimization Procedure
switch photoObj.optimType
  case 'simplex'
    mydisp('Solving using SIMPLEX method');
    simopt = optimset( 'Display','iter',...
      'MaxFunEvals',500,...
      'MaxIter',10,...
      'TolFun',1e-3,...
      'OutputFcn',outfcn,...
      'TolX',1e-6 ); % setup
    %'PlotFcns',@maPlotResult,...
    
    finalTr = fminsearch( cf, initTr, simopt ); % run
    
  case {'bfgs','dfp','steepdesc'}
    mydisp('Solving using steepest descent');
    simopt = optimset( 'LargeScale','off',...
      'GradObj','off',...
      'HessUpdate', photoObj.optimization,...
      'InitialHessType','identity',...
      'Display','iter',...
      'MaxFunEvals',250,...
      'MaxIter',20,...
      'TolFun',1e-3,...
      'OutputFcn',outfcn,...
      'TolX',1e-3 ); % setup
    %                            'InitialHessType','user-supplied',...
    %                            'InitialHessMatrix',0.1,
    
    finalTr = fminunc( cf, initTr, simopt ); % run
    
  case 'anneal'
    mydisp('Solving using annealing');
    % use simulated annealing with default parameters
    [finalTr,FVAL] = anneal( cf, initTr );
    
  case 'levmar'
    mydisp('Solving using levmar');
    % use simulated annealing with default parameters
    [finalTr,FVAL] = levmar( initTr, cf, 1e-3 );
    
  case 'powell'
    mydisp('Solving using Powell');
    finalTr = powell( cf, initTr(:), 1e-4, 50, outfcn, true);
    finalTr = finalTr(:)'; %reshape(finalTr,1,numel(finalTr));
    
  otherwise
    mydisp('Using initial transform as final transform');
    finalTr = initTr; % skip optimization, assign output parameters
    
end

%% save registered point cloud
%[T,s]


switch photoObj.transfType
  case {'scalerigid','rigid','translation','rotation'}
    [T,s] = cnlPhotoGram.getTransformMatrix( finalTr,'rigid','shiftscale',photoObj.shiftscale, ...
      'anglescale',photoObj.anglescale, ...
      'modeltr', photoObj.transfType);
  case 'affine'
    [T,s] = cnlPhotoGram.getTransformMatrix( finalTr,'affine' );
  case 'quasiaffine'
    [T,s] = cnlPhotoGram.getTransformMatrix( finalTr,'quasiaffine', 'shiftscale',photoObj.shiftscale, ...
      'anglescale',photoObj.anglescale);
    finalTr(4:6) = finalTr(4:6) * photoObj.shiftscale;
    finalTr(7:9) = finalTr(7:9) * photoObj.anglescale;
  otherwise
end

% Apply the transform to the electrode locations
mydisp('photogObj.CostFunctionOptimization >> Applying Final Transform to Electrode Locations');
photoObj.posEEG_final =  cnlPhotoGram.getTransformedPoints( photoObj.posEEG, photoObj.initTr * T, s );
photoObj.posFID_final =  cnlPhotoGram.getTransformedPoints( photoObj.posFID,photoObj.initTr * T,s);

photoObj.optimTr = T;
photoObj.optimTrParams = finalTr;

end

% end of function

%% criterion function: rigid transform & scaling
function cval = maCriterionScaleRigid( Tr, photoObj )

[T,s]  = cnlPhotoGram.getTransformMatrix( Tr,'rigid','shiftscale',photoObj.shiftscale,...
  'anglescale',photoObj.anglescale,...
  'modeltr',photoObj.transfType);
posNew =  cnlPhotoGram.getTransformedPoints( photoObj.posEEG, photoObj.initTr * T, s );
cval   = photoObj.cf(posNew);

end

%% criterion function: affine transform
function cval = maCriterionAffine( Tr, photoObj )

[T,s]  = cnlPhotoGram.getTransformMatrix ( Tr, 'affine');
posNew = cnlPhotoGram.getTransformedPoints( photoObj.posEEG, photoObj.initTr * T, s );
cval   = photoObj.cf(posNew);

end

%% criterion function: rigid transform & scaling
function cval = maCriterionQuasiAffine( Tr, photoObj )

[T,s] = cnlPhotoGram.getTransformMatrix(Tr,'quasiaffine','shiftscale',photoObj.shiftscale ,...
  'anglescale', photoObj.anglescale  );
posNew =  cnlPhotoGram.getTransformedPoints( photoObj.posEEG, photoObj.initTr * T, s );
cval = photoObj.cf(posNew);

if isnan(cval)
  keyboard;
end;

end

%% display results at each iteration
function stop = maOutputFcn(x, optimValues, state, photoObj)
stop = false;
bplot = false;

x = reshape(x,1,numel(x));

switch state
  case 'init'
    %         if bplot
    %             maDisplayRegRes('init'); % draw object
    %         end
    
    %         if strcmp(photoObj.transfType)
    ts = clock; % get time stamp
    OutputRegRes( photoObj,'create','optimization_steps.txt',...
      '% ------------------------------------------------------------------------------',...
      ['% Displaying optimization steps [',...
      ' on ',num2str(ts(3),'%.2d'),'/',num2str(ts(2),'%.2d'),'/',num2str(ts(1),'%.4d'),...
      ' at ',num2str(ts(4),'%.2d'),':',num2str(ts(5),'%.2d'),']'],...
      '% ------------------------------------------------------------------------------');
    %         end
    
  case {'iter', 'done'}
    % %         if optimValues.iteration == 0
    % %             maDisplayRegRes('draw'); % draw object
    % %         end
    %
    %         if bplot
    %             [T,s] = getRigidTransformMatrix( x, photoObj ); % obtain transform matrix
    %             posNew = getTransformedPoints( photoObj.posEEG, T, s );
    %     %         maDisplayRegRes('drawpoints',posNew); % draw object
    %             maDisplayRegRes('draw',posNew); % draw object
    %         end
    
    if strcmp(photoObj.transfType,'translation'), 
      finalTr = x .* photoObj.shiftscale;
    elseif strcmp(photoObj.transfType,'rotation'), 
      finalTr = x .* photoObj.anglescale;
    elseif (strcmp(photoObj.transfType,'rigid') && numel(x)==6),
      finalTr = [10 x .* [repmat(photoObj.shiftscale,1,3) repmat(photoObj.anglescale,1,3)]];
    elseif (strcmp(photoObj.transfType,'scalerigid') && numel(x)==7)
      finalTr = x .* [1 repmat(photoObj.shiftscale,1,3) repmat(photoObj.anglescale,1,3)];
    elseif strcmp(photoObj.transfType,'quasiaffine')
      finalTr = x;
      finalTr(4:6) = finalTr(4:6) * photoObj.shiftscale;
      finalTr(7:9) = finalTr(7:9) * photoObj.anglescale;
    end
    
    cf = 0;
    if isfield(optimValues,'fval'), cf = optimValues.fval; end
    OutputRegRes( photoObj,'write','optimization_steps.txt', [finalTr(:)' cf] );
    
    %         if bplot && strcmp(state,'done')
    %             maDisplayRegRes('done'); % clean-up
    %         end
  case 'interrupt'
    
  otherwise
end


end


