% Histogram criterion function 
% -------------------------------------------------------------------------
% Description: 
%
% Usage: 
%   photoObj = maHCF( 'init', photoObj ) 
%   KLsum = maHCF( 'exec', photoObj, points )
%  
% Input:
%   - fcn: 'init' or 'exec' or 'kill'
%   - photoObj: photoObj structure
%   - points: 3-D point coordinates
% 
% Output:
%   - photoObj*: in 'init', the photoObj structure is returned
%   - KLsum*: in 'exec', the cost function value is returned
% 
% Comments:
% 
% Notes:
% 
% -------------------------------------------------------------------------
function [ varargout ] = HCF( photoObj,fcn, varargin )

warning('How the heck are we getting in here?');
keyboard;

switch fcn
    case 'init' % constructor      
        
        % init histograms
        photoObj = photoObj.GetHistograms( 'init');
        
        % get mode of operation
        mode = photoObj.hcfopt.mode;        
        
        if mode.global || mode.bkgcon,
            % get global histogramm    
            if ~isempty(photoObj.otsu_thresh)
                indout = find(photoObj.xbins<=photoObj.otsu_thresh);
                valout = sum(photoObj.Href(indout));
                valin = sum(photoObj.Href(find(photoObj.xbins>photoObj.otsu_thresh)));
                quot = valin / valout;
                photoObj.Href(indout) = quot * photoObj.Href(indout);
                photoObj.indout = indout;
                % a = photoObj.otsu_thresh - photoObj.xbins(indout);
                % b = photoObj.xbins(indout+1) - photoObj.otsu_thresh;                         
            else
                photoObj.Href(1) = sum(photoObj.Href(2:end)); % normalize background                
            end               
            photoObj.Href = photoObj.Href + 1; % global image histogram                                                
            photoObj.Href = photoObj.Href ./ sum(photoObj.Href); % normalize histogram sums to 1                                    
            photoObj.Href = reshape(photoObj.Href,1,numel(photoObj.xbins));
        end
        
        if mode.localsym,
            % get point pairs
            if isempty(photoObj.Psym), error('Error: missing "Psym" data!'), end
            if isempty(photoObj.Wsym), error('Error: missing "Wsym" data!'), end                                  
            % stretch & normalize weights
            if isempty(photoObj.Wsym), photoObj.Wsym = ones(size(photoObj.Psym,1),1); end
            % photoObj.Wsym = exp(-photoObj.Wsym);             
            photoObj.Wsym = -photoObj.Wsym + max(photoObj.Wsym);            
            photoObj.Wsym = photoObj.Wsym / sum(photoObj.Wsym);                
        end
        
        if mode.localnet,
            % get point pairs
            if isempty(photoObj.Psym), error('Error: missing "Pnet" data!'), end
            if isempty(photoObj.Wsym), error('Error: missing "Wnet" data!'), end                                  
            % stretch & normalize weights
            if isempty(photoObj.Wnet), photoObj.Wnet = ones(size(photoObj.Pnet,1),1); end 
            % photoObj.Wnet = exp(-photoObj.Wnet); 
            photoObj.Wnet = -photoObj.Wnet + max(photoObj.Wnet);            
            photoObj.Wnet = photoObj.Wnet / sum(photoObj.Wnet); 
        end
                               
        % check output arguments
        if nargout>0, varargout{1} = photoObj; end  
        
    case 'exec' % execute function 
        if nargin < 3, error('Error: missing input points!'); end        
    
        % read input points
        points = varargin{1};
        
        % get mode of operation
        mode = photoObj.hcfopt.mode;         
        
        % get type of KL measure
        kltype = photoObj.hcfopt.divtype;         
        
        % compute & normalize histograms
        H = photoObj.GetHistograms( 'exec', points );
        H = H + 1e-2 * ones(size(H));
        for j = 1 : size(points,1)
            H(j,:) = H(j,:) ./ sum(H(j,:)); % normalize sum to 1 (!!!)     
        end         
                
        % init Kullback-Leibner distance        
        KLsum = 0;
        
        % distance to the global reference histogramm
        if mode.global,
            for j = 1 : size(points,1)
                KLsum = KLsum + cnlPhotoGram.kldiv(photoObj.xbins,H(j,:),photoObj.Href,kltype);
            end   
            KLsum = KLsum / size(points,1); % normalization
        end
        
        % sum of KLdivs between single L-R histogramms
        if mode.localsym,
            for j = 1 : size(photoObj.Psym,1)
                Hleft = H(photoObj.Psym(j,1),:);
                Hright = H(photoObj.Psym(j,2),:);
                KLsum = KLsum + photoObj.Wsym(j) * kldiv(photoObj.xbins,Hleft,Hright,kltype);
            end
        end
        
        % sum of KLdivs between neighbouring histogramms        
        if mode.localnet,
            for j = 1 : size(photoObj.Pnet,1)
                Hleft = H(photoObj.Pnet(j,1),:);
                Hright = H(photoObj.Pnet(j,2),:);
                KLsum = KLsum + photoObj.Wnet(j) * cnlPhotoGram.kldiv(photoObj.xbins,Hleft,Hright,kltype);
            end
        end        

        % add background constraint
        if mode.bkgcon,
%             if isfield(photoObj,'indout'),
%                 partsum = sum(photoObj.Href(photoObj.indout));
%                 for j = 1 : size(points,1)
%                     KLsum = KLsum + kldiv(photoObj.xbins(photoObj.indout),...
%                                             H(j,photoObj.indout)/sum(H(j,photoObj.indout)),...
%                                             photoObj.Href(photoObj.indout)/partsum,...
%                                             kltype);
%                 end   
%                 KLsum = KLsum / size(points,1); % normalization                               
%             else
                lambda = log(2)/0.1; % penalty on 10% difference is 1/3 of the measure
                KLsum = KLsum + (sum(exp(lambda * abs(H(:,1)-photoObj.Href(1))))-size(H,1))/size(H,1);
%             end
        end                
               
        if nargout>0, varargout{1} = KLsum; end
        
    case 'kill' % destructor function         
        
    otherwise
        error(['Error: class function "',fcn,'" not recognized!'])
end

    

