function out = convertSymToMatlab(in)
  
  out = zeros(in.size);
  out(triu(ones(in.size,in.size))>0) = in.data;
  out = out';
  out(triu(ones(in.size,in.size))>0) = in.data;
  
  
end