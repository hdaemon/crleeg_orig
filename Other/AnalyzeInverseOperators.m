%% Extract the matrix from the Leadfield structure

CortexPoints = find(NRRDS.DownSampledSegmentation.data==4);

solutionSpace = LeadField.currSolutionSpace;
solutionSpace.Voxels = CortexPoints;

LeadField = LeadField.changeSolutionSpace(solutionSpace);

LeadField  = LeadField.setConstrained(true);
LeadField2 = LeadField.setWeighting(true);

LField     = LeadField.currMatrix;
LFieldNorm = LeadField2.currMatrix;

lambdas = [ 0.001 0.001 ; ...
            0.001 0.001 ; ...
            0.001 0.001];

% Get the Tikhonov regularizer inverse operator and take the SVD
clear InvOp
InvOp{1,1}.lambda = lambdas(1,1);
InvOp{1,1}.Lmat = speye(size(LeadField,2));
InvOp{1,1}.name = 'InvOpMinNorm';
InvOp{1,1} = build_InverseOperator(InvOp{1,1},LField);
disp('Done With InvOp1 - Minimum Norm');
pause(1);

% % Get the Helmholtz Regularized Inverse Operator and take the SVD
% InvOp{2,1}.lambda =lambdas(2,1);
% LapMat = build_HelmholtzMatrix(LeadField.currSolutionSpace,0,false);
% B = LeadField.getWeightingMatrix(false);
% InvOp{2,1}.Lmat = LapMat*B;
% InvOp{2,1} = build_InverseOperator(InvOp{2,1},LField);
% InvOp{2,1}.name = 'InvOpLap';
% 
% disp('Done With InvOp2 - Laplacian');
% pause(1);
% 
% % Get the Helmholtz Regularized Inverse Operator and take the SVD
% InvOp{3,1}.lambda = lambdas(3,1);
% HHmat = build_HelmholtzMatrix(LeadField.currSolutionSpace,0.9,false);
% InvOp{3,1}.Lmat = HHmat*B;
% InvOp{3,1}.name = 'InvOpHH';
% InvOp{3,1} = build_InverseOperator(InvOp{3,1},LField);
% disp('Done With InvOp3 - Helmholtz');
% pause(1);

% Get the swLORETA Solution Operator
InvOp{1,2}.lambda = lambdas(1,2);
InvOp{1,2}.Lmat = speye(size(LeadField,2));
InvOp{1,2}.name = 'InvOp_W';
InvOp{1,2} = build_InverseOperator(InvOp{1,2},LFieldNorm);
disp('Done With InvOp4 - MinNorm w/ Weighted Leadfield');
pause(1);

% Get 
% InvOp{2,2}.lambda = lambdas(2,2);
% InvOp{2,2}.Lmat = build_HelmholtzMatrix(LeadField2.currSolutionSpace,0,false);
% InvOp{2,2}.name = 'InvOpLap_W';
% InvOp{2,2} = build_InverseOperator(InvOp{2,2},LFieldNorm);
% 
% disp('Done With InvOp5 - Laplacian w/ Weighted Leadfield');
% pause(1);
% 
% % HH with normalized columns
% InvOp{3,2}.lambda = lambdas(3,2);
% InvOp{3,2}.Lmat = build_HelmholtzMatrix(LeadField2.currSolutionSpace,0.5,false);
% InvOp{3,2}.name = 'InvOpHH_W';
% InvOp{3,2} = build_InverseOperator(InvOp{3,2},LFieldNorm);

disp('Done With InvOp6 - Helmholtz w/ Weighted LeadField ');
pause(1);

% %%
% clear InvOp
fooLam = [1 1];

for idx = [0 0.7 1 1.3 2 3 4 ];
  InvOp{end+1,1}.lambda = fooLam(1);
  HHmat = build_HelmholtzMatrix(LeadField.currSolutionSpace,idx,false);
  InvOp{end,1}.Lmat = HHmat;
  InvOp{end,1}.name = ['InvOpHH' num2str(idx)];
  InvOp{end,1} = build_InverseOperator(InvOp{end,1},LField);
  
  % HH with normalized columns
  InvOp{end,2}.lambda = fooLam(2);
  InvOp{end,2}.Lmat = build_HelmholtzMatrix(LeadField2.currSolutionSpace,idx,false);
  InvOp{end,2}.name = ['InvOpHH' num2str(idx) '_W'];
  InvOp{end,2} = build_InverseOperator(InvOp{end,2},LFieldNorm);
  
end;

% Now to get the normalization values;
for idx = 1:numel(InvOp);
  InvOp{idx}.norm = zeros(size(LField,2),1);
end


  for idxA = 1:size(InvOp,1);
    for idxB = 1:2
      LtL = InvOp{idxA,idxB}.Lmat'*InvOp{idxA,idxB}.Lmat;
      if idxB==1
        ALtL = LField/LtL;
      else
        ALtL = LFieldNorm/LtL;
      end;
      
      for idxVox = 1:size(LField,2)           
        InvOp{idxA,idxB}.norm(idxVox) = sqrt(InvOp{idxA,idxB}.InvOp(idxVox,:)*(ALtL(:,idxVox)));          
      end;
      
    end
  end  


for idxA = 1:size(InvOp,1)
  for idxB = 1:size(InvOp,2)
    InvOp{idxA,idxB}.Standardized = spdiags(1./InvOp{idxA,idxB}.norm,0,length(InvOp{idxA,idxB}.norm),length(InvOp{idxA,idxB}.norm))*InvOp{idxA,idxB}.InvOp;
    [U S V] = svd(InvOp{idxA,idxB}.Standardized,'econ');
    InvOp{idxA,idxB}.U_S = U;
    InvOp{idxA,idxB}.S_S = S;
    InvOp{idxA,idxB}.V_S = V;
  end
end

disp('All Done');

CortexPoints = LeadField.currSolutionSpace.Voxels;% find(NRRDS.DownSampledSegmentation.data(LeadField.currSolutionSpace.Voxels)==4);

nSamples = 2;
CortexSamples = round(length(CortexPoints)*rand(nSamples,1));
CortexSamples = unique(CortexSamples);

LeadFieldPoints = CortexSamples;


for idxA = 1:size(InvOp,1)
  for idxB = 1:size(InvOp,2)
    InvOp{idxA,idxB}.PSF = ( InvOp{idxA,idxB}.InvOp*LeadField(:,LeadFieldPoints));    
    InvOp{idxA,idxB}.PSF = InvOp{idxA,idxB}.PSF./kron(max(abs(InvOp{idxA,idxB}.PSF),[],1),ones(size(InvOp{idxA,idxB}.PSF,1),1));
    saveImagesAsNRRDS(InvOp{idxA,idxB}.PSF, LeadField.currSolutionSpace, InvOp{idxA,idxB}.name, NRRDS.DownSampledSegmentation);
    
    InvOp{idxA,idxB}.PSF_S = (InvOp{idxA,idxB}.Standardized*LeadField(:,LeadFieldPoints));
    InvOp{idxA,idxB}.PSF_S = InvOp{idxA,idxB}.PSF_S./kron(max(abs(InvOp{idxA,idxB}.PSF_S),[],1),ones(size(InvOp{idxA,idxB}.PSF_S,1),1));
    saveImagesAsNRRDS(InvOp{idxA,idxB}.PSF_S, LeadField.currSolutionSpace, [InvOp{idxA,idxB}.name '_S'], NRRDS.DownSampledSegmentation);
  end
end

% write out true images
TrueImages = zeros([nSamples NRRDS.DownSampledSegmentation.sizes]);
[x y z] = ind2sub(NRRDS.DownSampledSegmentation.sizes,CortexPoints(CortexSamples));
[X Y Z] = ndgrid(-1:1,-1:1,-1:1);
F = exp(-sqrt(X.^2+Y.^2+Z.^2));

for idx = 1:nSamples  
  TrueImages(idx,x(idx),y(idx),z(idx))= 1;
  TrueImages(idx,:,:,:) = convn(TrueImages(idx,:,:,:),F,'same');
end;
TrueImages = reshape(TrueImages,[nSamples prod(NRRDS.DownSampledSegmentation.sizes)]);
TrueImages = TrueImages';
TrueImages = TrueImages(LeadField.currSolutionSpace.Voxels,:);
saveImagesAsNRRDS(TrueImages,LeadField.currSolutionSpace,'TrueImage', NRRDS.DownSampledSegmentation);

%% Display Stuff

figure(1);
clf;
semilogy(diag(S)/S(1,1));
hold on;
semilogy(diag(SLap)/SLap(1,1),'r');
semilogy(diag(SHH)/SHH(1,1),'g');
semilogy(diag(S_W)/S_W(1,1),'k');
semilogy(diag(SLap_W)/SLap_W(1,1),'c');
semilogy(diag(SHH_W)/SHH_W(1,1),'m');

figure(2);
clf;
semilogy(diag(S_S)/S_S(1,1));
hold on;
semilogy(diag(SLap_S)/SLap_S(1,1),'r');
semilogy(diag(SHH_S)/SHH_S(1,1),'g');
semilogy(diag(S_SW)/S_SW(1,1),'k');
semilogy(diag(SLap_SW)/SLap_SW(1,1),'c');
semilogy(diag(SHH_SW)/SHH_SW(1,1),'m');


for idx = 1:20
figure(1+idx);
plot(U(:,idx));
hold on; 
plot(ULap(:,idx),'r');
plot(UHH(:,idx),'g');
end