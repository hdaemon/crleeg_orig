function InvOp = build_InverseOperator(InvOp,FMat)

L = InvOp.Lmat;
lambda = InvOp.lambda;

LtL = L'*L;
LtLAt = LtL\FMat';
ALtLAt = FMat*LtLAt;
InvOpMat = LtLAt*inv(ALtLAt + lambda*eye(128));

[U S V] = svd(InvOpMat,'econ');

InvOp.InvOp = InvOpMat;
InvOp.U = U;
InvOp.S = S;
InvOp.V = V;

end