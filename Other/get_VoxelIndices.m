function [voxelIndices] = get_VoxelIndices(nrrdSeg)
%
% function [voxelIndices] = get_VoxelIndices(dirs,NRRDS,solutionSpace)
%
% Get voxel indices for a bunch of different tissue types.  Also adds the
% BrainStem and OutSeg NRRDS.
%
% Inputs: dirs          : EEG_Directory object
%         NRRDS         : EEG_NRRDS Structure
%         LeadField     : Full Solution Space of the Appropriate Leadfield
%
% Outputs: voxelIndices : structure containing indices for a bunch of
%                           different tissue options
% 
% Calls: get_ConnectedCortex
%
% Called By: EEG_InversionPrep
%
% Last Modified: Damon Hyde, 04/26/2012

%voxelIndices.AllSolutionPoints = LeadField.currSolutionSpace.Voxels;
voxelIndices.InICC          = find(nrrdSeg.data>=4);   

% BrainStem Region
% NRRDS.BrainStem = file_NRRD('StemAndCerebellum.nhdr',dirs.MRIFull);
% voxelIndices.brainstem = find(NRRDS.BrainStem.data==6);

% Identify CSF and White Matter voxels
voxelIndices.Cortex   = find(nrrdSeg.data==4);
voxelIndices.CSF      = find(nrrdSeg.data==5);
voxelIndices.WM       = find(nrrdSeg.data==7);
voxelIndices.Brain    = union(voxelIndices.WM,voxelIndices.Cortex);
voxelIndices.ICC      = find(nrrdSeg.data>=4);

% NRRDS.OutSeg = file_NRRD('OutSeg.nhdr',dirs.CortConstFull);
% voxelIndices.ExtractedCortex = find(ismember(NRRDS.OutSeg.data,[4 9]));
% voxelIndices.ExtractedCSF    = find(ismember(NRRDS.OutSeg.data,[5 10 12]));
% voxelIndices.dual   = find((nrrdSeg.data==4)|(nrrdSeg.data==5));


%%  Identify sufficiently connected cortex
voxelIndices.connectedCortex2 = get_ConnectedCortex(voxelIndices.Cortex,nrrdSeg.sizes,2);
voxelIndices.connectedCortex3 = get_ConnectedCortex(voxelIndices.Cortex,nrrdSeg.sizes,3);

end