%%

NVM = cnlParcellation('parcel_nvm_crl.nrrd','./','nvm');
NVM.removeWhiteMatter;
NVM.removeCSF;
NVM.removeSubCorticalGray;
NVM = NVM.fourLabelSeg;
NVM.data = double(NVM.data==4);
NVM.fname = 'mask_nvm_manual.nrrd';
NVM.fpath = './';
NVM.encoding = 'gzip';
NVM.type = 'unsigned int';
NVM.write;