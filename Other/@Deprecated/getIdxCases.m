function idxCases = getIdxCases

idxCases = { '030' ...
             '039' ...
             '045' ...
             '046' ...
             '052/20090810' ...
             '054/20110601' ...
             '057' ...
             '058/20090608' ...
             '065/20100626' ...
             '066' ...
             '067/20090515' ...
             '068/20090914' ...
             '069/20100702' ...
             '073/20100823' ...
             '074/20110513' ...
             '075/20110330' ...
             '076/20100804' ...
             '079' ...
             '083/20110903' ...
             '086/20120505' ...
             '091' ...
             '093/20121214' ...
             '094/20111104' ...
             '106/20120425' ...
             '110/20110814' ...
             '111/20120113' ...
             '115/20111007' ...
             '131' ...
             '135' ...
             '139' ...
             '141' ...
             '145' ...
             '157/20120430' ...
             '158' ...             
             '160' ...
             '161' ...
             '162/20131007' ...
             '164/20140106' ...
             '172/20130102' ...
             '173/20140303' ...
             '174' ...
             '175' ...
             '176/scan01' ...
             '177/scan02' ...
             '178/scan01' ...
             '179/scan01' ...
             '181' ...
             '183' ...
             '184' ...
             '185' ...
             };

end

%'159/20120110' ...  % Never actually got hdEEG study