function [EEG Electrodes] = read_EEGData(dirs,files,Electrodes,modelOptions)
%
% function [EEG] = read_EEGData(dir,files,gndElectrode)
%
% Given a list of EEG files and the directory they are located, read them
% in.  If given a ground electrode, also generates a rereferenced dataset.
%
% Inputs: dir          : directory containing EEG files
%         files        : list of EEG files
%         gndElectrode : ground electrode number
%
% Outputs: EEGData : Structure Containing the EEG Data
%
% Calls : sload
%
% Called By: EEG_InversionPrep
%
% Last Modified: Damon Hyde, 04/26/2012

currentDIR = pwd;
cd([dirs.ProcessedRootDIR dirs.EEGData]);

% Read Data File(s)
for i = 1:length(files.EEGData);
  % Read EEG Data from EDF File
  [EEG(i).original.data EEG(i).original.header] = sload(files.EEGData{i});  
  
  % Grab the filename for unique directory identifiers later
  [path name ext] = fileparts(files.EEGData{i});  
  EEG(i).dirName = name;
  
  % Match EEG electrodes to model electrodes
  EEG(i).original.ElecLabels = parse_Labels(EEG(i).original.header.Label);
  
  Electrodes.badElecLabels = read_BadElectrodes(dirs,files);  
  Electrodes.useElec = [];
  Electrodes.badElec = [];  
  
  data = zeros(size(EEG(i).original.data,1),length(Electrodes.Labels));
  ElecLabels = [];  
  IndexIntoOriginal = [];
  originalLabels = EEG(i).original.ElecLabels;
  
  for idxElec = 1:length(Electrodes.Labels)        
  
    if any(strcmpi(Electrodes.Labels{idxElec},originalLabels))
      % Only do the following for electrodes that are part of the data
      
      % Find index into original labels that matches the model electrode
      idxOrig = find(strcmpi(Electrodes.Labels{idxElec},originalLabels));
      
      % Copy data from the original to the set for reconstruction
      data(:,idxElec) = EEG(i).original.data(:,idxOrig);
      
      % Get the label for the electrode
      ElecLabels{idxElec} = originalLabels{idxOrig};
      
      % Save the index into the original      
      IndexIntoOriginal(idxElec) = idxOrig;
      
      % Check if the electrode is usable (ie - Not bad and not the ground)
      if ~any(strcmpi(Electrodes.Labels{idxElec},Electrodes.badElecLabels)) % and that it's not bad        
        if ~(idxElec==modelOptions.gndElectrode) %skip the ground electrode
          Electrodes.useElec = [Electrodes.useElec idxElec]; % add it to the list
        end;
      else
        Electrodes.badElec = [Electrodes.badElec idxElec];
      end
    end
          
  end
  
  % Use spline interpolation to estimate data values when we have NaN
  % errors
  if any(any(isnan(data)))
    mydisp('USING SPLINE INTERPOLATION TO FILL IN THE GAPS');
  for idxElec = 1:size(data,2);
    test = data(:,idxElec);
    Q = find(~isnan(test));
    dataout = spline(Q,test(Q),1:length(test));
    data(:,idxElec) = dataout;
  end;
  end;
  
  % Drop data points with very low power (don't even know why these are in
  % the data files) NOT SURE WHY THIS WAS HERE - commented out 11/19/2013
  % by D.Hyde
  % This is to eliminate the extra zeros that get appended to the data file
  % for some reason
%   dataPower = sqrt(sum(EEG(i).data.^2,2));
%   Q = dataPower>1;
%   EEG(i).data = EEG(i).data(Q,:); 
  
  % Transpose so the data is in the right orientation later on.
  EEG(i).data = data'; 
  EEG(i).ElecLabels = ElecLabels;
  EEG(i).IndexIntoOriginal = IndexIntoOriginal;
  EEG(i).useElec = Electrodes.useElec;
  EEG(i).badElec = Electrodes.badElec;
  
  % Rereference Using the Ground Electrode
  idxGnd = modelOptions.gndElectrode;
  if modelOptions.gndElectrode ~= 0
    refVals = EEG(i).data(idxGnd,:);
    EEG(i).rereferenced = EEG(i).data - repmat(refVals,[size(EEG(i).data,1) 1]);
  end;
  
  % Remove DC Bias
  EEG(i).rerefbias = mean(EEG(i).rereferenced,1);
  EEG(i).averagereference = EEG(i).rereferenced - repmat(EEG(i).rerefbias,[size(EEG(i).rereferenced,1) 1]);  
   
end;

cd(currentDIR);

end

