function m = temporalmat(a)

if nargin==0
  m.mat = [];
  m = class(p,'temporalmat');
elseif  isa(a,'temporalmat')
  m = a;
else
  m.mat = a;
  m = class(p,'temporalmat');
end;
  
  
return;