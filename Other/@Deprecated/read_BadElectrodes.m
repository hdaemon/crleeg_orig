function [badElec] = read_BadElectrodes(dirs,files)
% function [badElec] = read_BadElectrodes(dirs,files)
%
% Read and parse the bad electrode list
%
% Inputs: files : EEG_files object
%         dirs  : EEG_directory structure
%
% Outputs: badElec : list of bad electrodes
%
% Last Modified: Damon Hyde, 04/26/2012

try
  
  currentDIR = pwd;
  cd([dirs.ProcessedRootDIR dirs.EEGData]);
  
  if ~isempty(files.badElectrodes)
    
    fname = files.badElectrodes;
    fid = fopen(files.badElectrodes,'r');
    
    if fid<0, warning(['File "',fname,'" does not exist!']),
      keyboard, end
    line = fgetl( fid ); % read first line
    
    % loop through file
    labels = {};
    while line~=-1
      cell = textscan(line,'%s');         % convert line data
      if length(cell{1})==1
        labels = {labels{:},cell2mat(cell{1})};
      end;
      line = fgetl(fid);                            % read new line
    end
    fclose(fid); % c
    
    
    badElec = [];
    for i = 1:length(labels)
      if strcmp(labels{i}(end),'*')
        badElec{end+1}  = labels{i}(1:end-1);
      elseif ~isempty(labels{i})        
          badElec{end+1} = labels{i}(1:end);        
      end;
    end;
;
    
  else
    warning('NO BAD ELECTRODE FILE');
    badElec = [];
  end;
  
  cd(currentDIR);
  
catch
  mydisp('Error in read_BadElectrodes');
  keyboard;
end;

return;