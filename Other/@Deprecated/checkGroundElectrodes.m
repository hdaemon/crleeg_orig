clear all;

doTimePoints = {[25] [100] [16 20 22] [15 25 35] [24 35] [12 18 20] [13 14 15] [15 20] [20 25] [ 10 18 27]};

doTimePoints = {[25] [100] [20] [25] [24] [18] [14] [1:2:38] [25] [18]};

doPatients = [ 1 2 3 4 5 6 8 9 10 ];
doPatients = [1 2 3 4 5 6 7 8 9 10];
doPatients = [2 6];
saveTimePoints = { [1] [1] [1] [1] [1] [1] [1] [17] [1] [1] [1] };

doReconstructions = true;

ScrapeModelFiles;
for idxPatient = doPatients
  disp(['Doing Patient ' num2str(idxPatient) ]);
  if isfield(Patients{idxPatient},'PSModelFile')&&(isfield(Patients{idxPatient},'AtlasModelFile'))
    %% Load Models
    PSModel    = load(Patients{idxPatient}.PSModelFile);
    AtlasModel = load(Patients{idxPatient}.AtlasModelFile);
    
    Tests(idxPatient,1,:) = PSModel.Electrodes.Nodes([80 129]);
    Tests(idxPatient,2,:) = AtlasModel.Electrodes.Nodes([80 129]);

  end;
end;



