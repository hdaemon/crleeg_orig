% Point pair matching
% -------------------------------------------------------------------------
% Description:
%
% Usage:
% 	[P, W, mc] = maPointPairMatching( points, G1, G2, N, bplot )
%
% Input:
%   - points: EEG point coordinates
%   - G1, G2: point indices
%   - N: plane normal
%   - bplot: plot intermediate steps
%
% Output:
%   - P: indexes of point pairs
%   - W*: corresponding weights (Euclidean distances)
%   - mc*: arithmetic point center
%
% Comments:
%
% Notes:
%
% -------------------------------------------------------------------------

function [P, varargout] = PointPairMatching( points, G1, G2, N, bplot )

if nargin < 5, bplot = false; end

%% load data & centerize points
mc = sum(points,1) ./ size(points,1); % center points
points = points - repmat(mc,[size(points,1), 1]);

%% match set 1 -> 2
len = max(length(G1),length(G2));
P = zeros(len,2);

N = reshape(N,3,1);

% get indexes of non-zero elements
inx1 = find(G1>0);
p1 = points(G1(inx1),:) - (points(G1(inx1),:) * N) * N';

inx2 = find(G2>0);
p2 = points(G2(inx2),:) - (points(G2(inx2),:) * N ) * N';

if bplot
  % plot in-plane projections of points
  close all
  plot3(p1(:,1),p1(:,2),p1(:,3),'+r'), hold on
  plot3(p2(:,1),p2(:,2),p2(:,3),'+b')
end

% get edge matrix
E12 = getNearest3Vertices( p1, inx1, p2, inx2 );
E21 = getNearest3Vertices( p2, inx2, p1, inx1 );
E = [E12; E21(:,2), E21(:,1)];

E = unique(E,'rows');
E1 = p1(E(:,1),:);
E2 = p2(E(:,2),:);
W = sqrt(sum((E1-E2).^2,2));

P = [G1(E(:,1)),G2(E(:,2))];

E1 = points(P(:,1),:);
E2 = points(P(:,2),:);


if bplot
  figure, hold on
  for i = 1 : length(E(:,1))
    plot3([E1(i,1);E2(i,1)],[E1(i,2);E2(i,2)],[E1(i,3);E2(i,3)],'-g')
  end
  hold off
  keyboard
end


if nargout>1, varargout{1} = W; end
if nargout>2, varargout{2} = mc; end

end


%% get nearest 3 vertices to the given reference vertexes
function E = getNearest3Vertices( p1, inx1, p2, inx2 )

ident = ones(length(inx2),1);
Et = zeros(length(inx1),3);
for i = 1 : length(inx1)
  l1dist = sum(abs(p2(inx2,:) - ident * p1(i,:)),2);
  maxval = max(l1dist);
  % get first minimum
  [val,mininx] = min(l1dist);
  Et(i,1) = mininx;
  l1dist(mininx) = maxval;
  % get second minimum
  [val,mininx] = min(l1dist);
  Et(i,2) = mininx;
  l1dist(mininx) = maxval;
  % get third minimum
  [val,mininx] = min(l1dist);
  Et(i,3) = mininx;
  %         l1dist(mininx) = maxval;
end

% construct edge matrix with point indices
E = [inx1,inx2(Et(:,1));...
  inx1,inx2(Et(:,2));...
  inx1,inx2(Et(:,3))];
end





