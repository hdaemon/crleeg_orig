% Principal axes computation and point subset extraction
% -------------------------------------------------------------------------
% Description:
%
% Usage:
%	[G1, G2, N, V] = maPrincipalAxes( points, 'indices' )
%        [G1, G2] = maPrincipalAxes( points, 'indices' )
%
% Input:
%   - points: EEG point coordinates
%   - export: 'indices' or 'coordinates'
%   - selsplit*: 'auto-equal' or 'principal-axes'
%
% Output:
%   - G1, G2: point indices or coordinates
%   - N: computed surface normal
%   - V*: principal axis vector
%   - EigVect*: export all eigenvectors
%   - EigVal*: export all eigenvalues
%
% Comments:
%
% Notes:
%
% -------------------------------------------------------------------------

function [G1, G2, varargout] = PrincipalAxes( points, export, selsplit )

mydisp('PrincipalAxes >> BEGIN');
if nargin<3, selsplit = 'auto-equal'; end

%% load data & centerize points
mc = sum(points,1) ./ size(points,1); % Get geometric center
points = points - repmat(mc,[size(points,1) 1]);

%% compute inertia moments & build inertia matrix
mxx = sum(points(:,2).^2 + points(:,3).^2);
myy = sum(points(:,1).^2 + points(:,3).^2);
mzz = sum(points(:,1).^2 + points(:,2).^2);
mxy = sum(points(:,1) .* points(:,2));
mxz = sum(points(:,1) .* points(:,3));
myz = sum(points(:,2) .* points(:,3));

% inertia matrix as appeared in Kozinska (2001)
M = [ mxx, mxy, mxz; mxy, myy, myz; mxz, myz, mzz];

[V,D] = eig(M); % compute principal axes

% Indices Minimum and Maximum Eigenvalues
D = diag(D);
minEig = find(D==min(D)); % get index of smallest eigenvalue
maxEig = find(D==max(D)); % get index of largest eigenvalue


switch lower(selsplit)
  case 'principal-axes'    
    finalABC = cross(V(:,minEig),V(:,maxEig)); % Get normal to plane defined by largest and smallest eigenvectors
    
  case 'auto-equal'
    startABC = [1,0,0];
    cf = @(ABC) SplitProjSum(ABC, points);
    simopt = optimset( 'LargeScale','off',...
      'GradObj','off',...
      'Display','iter',...
      'MaxFunEvals',1000,...
      'MaxIter',8,...
      'TolFun',1e-3,...
      'TolX',1e-3 ); % setup    
    finalABC = fmincon( cf, startABC, [], [], V(:,minEig)',  0,[],[],[]     , simopt ); % run
    
  otherwise
    error('Selsplit must be either ''principal-axes'' or ''auto-equal'' ');
end
finalABC = [1 0 0 ];
finalABC = finalABC ./ norm(finalABC);
[G1, G2] = GetGroups( finalABC, points, export );

figure(101);
plot3(points(G1,1),points(G1,2),points(G1,3),'rx'); hold on;
plot3(points(G2,1),points(G2,2),points(G2,3),'bx'); hold off;
keyboard;

if nargout>2, varargout{1} = finalABC; end
if nargout>3, varargout{2} = V(:,minEig); end
if nargout>4, varargout{3} = V; end
if nargout>5, varargout{4} = D; end

mydisp('PrincipalAxes >> END');
end

%% split the points in two groups by a plane
% a*(x - xc)+b*(y - yc)+c*(z - zc)=0
function [G1, G2] = GetGroups( ABC, points, export )

try
  ABC = reshape(ABC,3,1);
  ABC = ABC ./ norm(ABC); % norm the plane normal
  Sprod = points * ABC; % compute dot product
catch
  keyboard;
end;

switch export
  case 'coordinates'
    % export point coordinates
    G1 = points(find(Sprod<0),:);
    G2 = points(find(Sprod>=0),:);
    % case 'indices'
  otherwise
    % export only indices
    G1 = find(Sprod<0);
    G2 = find(Sprod>=0);
end

end

%% split the point space by plane
function cval = SplitProjSum( ABC, points )

ABC = reshape(ABC,3,1); % reshape parameter matrix
ABC = ABC ./ norm(ABC); % norm the plane normal
Sprod = points * ABC; % compute dot product

cval = exp(abs(length(Sprod(find(Sprod<0)))-length(Sprod(find(Sprod>=0))))); % split with equal powers
cval = cval + max(abs(sum(Sprod(find(Sprod<0)))),sum(Sprod(find(Sprod>=0)))); % split with minimal distance

end




