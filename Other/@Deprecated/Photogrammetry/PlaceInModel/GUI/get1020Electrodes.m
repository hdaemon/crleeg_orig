%    162   132    92
%     12   130    92
%     88   230    80
%     88    23   130
%     88   192   221

function Electrodes = get1020Electrodes(Nas,Inion,Cz,Pa_Right, Pa_Left)

Nas = Nas(:);
Inion = Inion(:);
Cz = Cz(:);
Pa_Right = Pa_Right(:);
Pa_Left = Pa_Left(:);

%% Compute basis for Nasion-Inion Circle
NasInion = get_DefiningCircle(Nas,Cz,Inion);
Elec1 = get_CirclePoints(NasInion,[0.1 0.3 0.5 0.7 0.9]);
Tmp1 = Elec1(:,1); 
Fz   = Elec1(:,2);
Cz2  = Elec1(:,3);
Pz   = Elec1(:,4);
Tmp2 = Elec1(:,5);

PaCircle = get_DefiningCircle(Pa_Left,Cz2,Pa_Right);
Elec2 = get_CirclePoints(PaCircle,[0.1 0.3 0.5 0.7 0.9]);
T3  = Elec2(:,5);
C3  = Elec2(:,4);
Cz3 = Elec2(:,3);
C4  = Elec2(:,2);
T4  = Elec2(:,1);

LeftHem = get_DefiningCircle(Tmp1,T3,Tmp2);
Elec3 = get_CirclePoints(LeftHem,[0.1 0.3 0.5 0.7 0.9]);
Fp1 = Elec3(:,1);
F7  = Elec3(:,2);
T32 = Elec3(:,3);
T5  = Elec3(:,4);
O1  = Elec3(:,5);

RightHem = get_DefiningCircle(Tmp1,T4,Tmp2);
Elec4 = get_CirclePoints(RightHem,[0.1 0.3 0.5 0.7 0.9]);
Fp2 = Elec4(:,1);
F8  = Elec4(:,2); 
T42 = Elec4(:,3);
T6  = Elec4(:,4);
O2  = Elec4(:,5);

LeftMid = get_DefiningCircle(Fp1,C3,O1);
Elec5 = get_CirclePoints(LeftMid,[0.25 0.5 0.75]);
F3  = Elec5(:,1);
C32 = Elec5(:,2);
P3  = Elec5(:,3);

RightMid = get_DefiningCircle(Fp2,C4,O2);
Elec6 = get_CirclePoints(RightMid,[0.25 0.5 0.75]);
F4  = Elec6(:,1);
C4  = Elec6(:,2);
P4  = Elec6(:,3);

Pts = [Nas Inion Cz Pa_Right Pa_Left Fz Cz2 Pz Tmp1 Tmp2 T3 T4 Cz3 C3 C4];

% %
% figure;
% plot3(Pts(1,1:5),Pts(2,1:5),Pts(3,1:5),'rx','MarkerSize',8);
% hold on;
% plot3(Pts(1,6:10),Pts(2,6:10),Pts(3,6:10),'x','MarkerSize',8);
% axis equal;
% plot3(Pts(1,11:15),Pts(2,11:15),Pts(3,11:15),'gx','MarkerSize',8)
% plot3(Elec3(1,:),Elec3(2,:),Elec3(3,:),'x','MarkerSize',8);
% plot3(Elec4(1,:),Elec4(2,:),Elec4(3,:),'x','MarkerSize',8);
% plot3(Elec5(1,:),Elec5(2,:),Elec5(3,:),'x','MarkerSize',8);
% plot3(Elec6(1,:),Elec6(2,:),Elec6(3,:),'x','MarkerSize',8);

Electrodes.Labels = {'Fp1' 'Fp2' 'Fz' 'F3' 'F4' 'F7' 'F8' 'Cz' 'C3' 'C4' 'T7' 'T8' 'Pz' 'P3' 'P4' 'P7' 'P8' 'O1' 'O2'};
Electrodes.Pos    = [ Fp1   Fp2   Fz   F3   F4   F7   F8   Cz   C3   C4   T3   T4   Pz   P3   P4   T5   T6   O1   O2]';

return;



% function [StructOut] = get_DefiningCircle(Point1,Point2,Point3)
%   
%   % Make it a column
%   Point1 = Point1(:);
%   Point2 = Point2(:);
%   Point3 = Point3(:);
%   
%   % get vectors
%   Vec1 = Point2 - Point1; L1 = norm(Vec1); Vec1 = Vec1/norm(Vec1);
%   Vec2 = Point3 - Point1; L2 = norm(Vec2); Vec2 = Vec2/norm(Vec2);
%   Perp = cross(Vec1,Vec2); Perp = Perp/norm(Perp);
%   
%   % Get Midline Points
%   MidPt1 = Point1 + 0.5*L1*Vec1;
%   MidPt2 = Point1 + 0.5*L2*Vec2;
%   
%   % Get Normals to MidPoint
%   Nrm1 = Vec2 - (Vec2'*Vec1)*Vec1; Nrm1 = Nrm1/norm(Nrm1);
%   Nrm2 = Vec1 - (Vec2'*Vec1)*Vec2; Nrm2 = Nrm2/norm(Nrm2);
%        
%   % Get locations of points in planar coordinate system
%   matShift = [Vec1' ; Nrm1' ; Perp' ];
%   Pt1 = matShift*Point1; Pt1 = Pt1(1:2);
%   Pt2 = matShift*Point2; Pt2 = Pt2(1:2);
%   Pt3 = matShift*Point3; Pt3 = Pt3(1:2);
%   Mp1 = matShift*MidPt1; Mp1 = Mp1(1:2);
%   Mp2 = matShift*MidPt2; Mp2 = Mp2(1:2);
%   N1 = matShift*Nrm1; N1 = N1(1:2);
%   N2 = matShift*Nrm2; N2 = N2(1:2);
%    
%   % Get Center
%   mat = [-N1 N2];
%   tmp = Mp1 - Mp2;
%   tmp2 = inv(mat)*tmp;
%   Center = Mp1 + N1*tmp2(1);
%   
%   Pts = [Pt1 Pt2 Pt3 Mp1 Mp2];
%   Nrms = [N1 N2];
%   
% %   figure;
% %   plot(Pts(1,:),Pts(2,:),'rx'); hold on;
% %   plot(Pt1(1),Pt1(2),'gx');
% %   plot(Pt2(1),Pt2(2),'cx');
% %   plot([Pts(1,1:3) Pts(1,1)],[Pts(2,1:3) Pts(2,1)]);
% %   quiver([Mp1(1) Mp2(1)],[Mp1(2) Mp2(2)],[N1(1) N2(1)],[N1(2) N2(2)]);
% %   %quiver([Pts(1,1) Pts(1,1)],[Pts(2,1) Pts(2,1)],[V1(1) V2(1)],[V1(2) V2(2)])
% %   plot(Center(1),Center(2),'gx'); 
% %   axis equal
%   
%   Vec1 = Pt1 - Center;  
%   Vec2 = Pt2 - Center; 
%   Vec3 = Pt3 - Center;
%   
%   ALen1 = acosd((Vec1'*Vec2)/(norm(Vec1)*norm(Vec2)));
%   ALen2 = acosd((Vec2'*Vec3)/(norm(Vec2)*norm(Vec3)));
%    
%   StructOut.ShiftInv = inv(matShift);
%   StructOut.Center = Center;
%   StructOut.Radius = norm(Vec1);
%   StructOut.Vec = Vec1/norm(Vec1);
%   StructOut.TotalAngle = ALen1 + ALen2;  
%   StructOut.Offset = matShift(3,:)*Point1;
%   
% return
% 
% function [ValsOut] = get_CirclePoints(CircleDef,Percentages)
% 
% Angles = CircleDef.TotalAngle*Percentages;
% 
% for i = 1:length(Angles)
%   theta = Angles(i);
%   mat = [cosd(theta) -sind(theta) ; sind(theta) cosd(theta)];
%   val = CircleDef.Radius*mat*CircleDef.Vec;
%   ValsTmp(:,i) = (val+CircleDef.Center);  
% end;
% ValsOut = CircleDef.ShiftInv*[ValsTmp ; CircleDef.Offset*ones(1,size(ValsTmp,2))];
% 
% return;