function [StructOut] = get_DefiningCircle(Point1,Point2,Point3)
  
  % Make it a column
  Point1 = Point1(:);
  Point2 = Point2(:);
  Point3 = Point3(:);
  
  % get vectors
  Vec1 = Point2 - Point1; L1 = norm(Vec1); Vec1 = Vec1/norm(Vec1);
  Vec2 = Point3 - Point1; L2 = norm(Vec2); Vec2 = Vec2/norm(Vec2);
  Perp = cross(Vec1,Vec2); Perp = Perp/norm(Perp);
  
  % Get Midline Points
  MidPt1 = Point1 + 0.5*L1*Vec1;
  MidPt2 = Point1 + 0.5*L2*Vec2;
  
  % Get Normals to MidPoint
  Nrm1 = Vec2 - (Vec2'*Vec1)*Vec1; Nrm1 = Nrm1/norm(Nrm1);
  Nrm2 = Vec1 - (Vec2'*Vec1)*Vec2; Nrm2 = Nrm2/norm(Nrm2);
       
  % Get locations of points in planar coordinate system
  matShift = [Vec1' ; Nrm1' ; Perp' ];
  Pt1 = matShift*Point1; Pt1 = Pt1(1:2);
  Pt2 = matShift*Point2; Pt2 = Pt2(1:2);
  Pt3 = matShift*Point3; Pt3 = Pt3(1:2);
  Mp1 = matShift*MidPt1; Mp1 = Mp1(1:2);
  Mp2 = matShift*MidPt2; Mp2 = Mp2(1:2);
  N1 = matShift*Nrm1; N1 = N1(1:2);
  N2 = matShift*Nrm2; N2 = N2(1:2);
   
  % Get Center
  mat = [-N1 N2];
  tmp = Mp1 - Mp2;
  tmp2 = inv(mat)*tmp;
  Center = Mp1 + N1*tmp2(1);
  
  Pts = [Pt1 Pt2 Pt3 Mp1 Mp2];
  Nrms = [N1 N2];
  
%   figure;
%   plot(Pts(1,:),Pts(2,:),'rx'); hold on;
%   plot(Pt1(1),Pt1(2),'gx');
%   plot(Pt2(1),Pt2(2),'cx');
%   plot([Pts(1,1:3) Pts(1,1)],[Pts(2,1:3) Pts(2,1)]);
%   quiver([Mp1(1) Mp2(1)],[Mp1(2) Mp2(2)],[N1(1) N2(1)],[N1(2) N2(2)]);
%   %quiver([Pts(1,1) Pts(1,1)],[Pts(2,1) Pts(2,1)],[V1(1) V2(1)],[V1(2) V2(2)])
%   plot(Center(1),Center(2),'gx'); 
%   axis equal
  
  Vec1 = Pt1 - Center;  
  Vec2 = Pt2 - Center; 
  Vec3 = Pt3 - Center;
  
  ALen1 = acosd((Vec1'*Vec2)/(norm(Vec1)*norm(Vec2)));
  ALen2 = acosd((Vec2'*Vec3)/(norm(Vec2)*norm(Vec3)));
   
  StructOut.ShiftInv = inv(matShift);
  StructOut.Center = Center;
  StructOut.Radius = norm(Vec1);
  StructOut.Vec = Vec1/norm(Vec1);
  StructOut.TotalAngle = ALen1 + ALen2;  
  StructOut.Offset = matShift(3,:)*Point1;
  
return
