function [ElectrodesOut] = EEG_GetElectrodeLocations(dirs,files,options)

%% Do Photogrammetry
if options.usePhotogrammetry

  cd([dirs.ProcessedRootDIR dirs.Photogrammetry]);
  if ~exist('Mat-Sessions','dir')
    mkdir('Mat-Sessions');
  end;
  cd([dirs.ProcessedRootDIR dirs.Photogrammetry 'Mat-Sessions/']);
  if (options.recomputeAll)||(~exist('Electrodes.mat','file'));
    mydisp('Computing Photogrammetry using EEG_RegisterPhotogrammetryData');
    Electrodes = EEG_RegisterPhotogrammetryData(dirs,files,options);
    save([dirs.ProcessedRootDIR dirs.Photogrammetry 'Mat-Sessions/Electrodes.mat'],'Electrodes');
  else
    mydisp('Using existing photogrammetry results from Electrodes.mat');
    load Electrodes.mat
  end;
  
  % Locate closest node for each electrode.
  % Note that this isn't nearly careful enough about directions to operate
  % properly for anisotropic diffusion.
  ElectrodesOut.ElecLoc   = Electrodes.posEEG;  
  
%   % Define the reference electrode.
%   if any(options.refElectrodeLoc) % We define an (X,Y,Z) Reference Location
%     mydisp('Using a prespecified (X,Y,Z) reference electrode location');
% %    ElectrodesOut.ElecLoc(end+1,:) = options.refElectrodeLoc;
%     
%     ElectrodeOut.GndLoc = options.refElectrodeLoc;
%     
%   elseif ~isempty(options.gndElectrode)
%     mydisp(['Using electrode ' num2str(options.gndElectrode) ' as reference electrode']);
%     %mydisp(['Setting ground node to ' num2str(Electrodes.
%     ElectrodesOut.ElecLoc(end+1,:) = ElectrodesOut.ElecLoc(options.gndElectrode,:);
%     %ElectrodesOut.ElecLoc(options.gndElectrode,:) = [];
%     
%     ElectrodesOut.GndLoc = ElectrodesOut.ElecLoc(options.gndElectrode,:);
%     ElectrodesOut.GndIdx = options.gndElectrode;
%   else
%     keyboard;
%     error('I don''t know where to put the ground electrode!!!');
%   end;
  
  ElectrodesOut.ElecLabels = Electrodes.labEEG;
  ElectrodesOut.FIDLoc     = Electrodes.posFID;
  ElectrodesOut.FIDLabels  = Electrodes.labFID;
  
else
  
  warning('Make sure you intend on doing a 10-10 or 10-20 System');
  keyboard;
  
  cd([dirs.ProcessedRootDIR dirs.SaveMATFiles]);
  load guiElectrodes.mat
%  guiPlaceElectrodes;
%  global guiDataOut;
  if isfield(guiDataOut,'useElectrodes')
    use = guiDataOut.useElectrodes;
  else
    use = 1:length(guiDataOut.point_labels);
  end;

  ElectrodesOut.ElecLoc = guiDataOut.point_coords(use,:);
  ElectrodesOut.ElecLabels = guiDataOut.point_labels(use);

%   % Assign the reference electrode
%   if (isfield(options,'gndElectrodeLabel')&&(~isempty(options.gndElectrodeLabel)))
%     mydisp(['Using electrode ' options.gndElectrodeLabel ' as ground electrode']);
%     idx = find(strcmp(options.gndElectrodeLabel,ElectrodesOut.ElecLabels));
%     if isempty(idx)||(length(idx)>1)
%       error('Something is wrong with locating the ground electrode');
%     else
%       ElectrodesOut.ElecLoc(end+1,:) = ElectrodesOut.ElecLoc(idx,:);
%       ElectrodesOut.ElecLoc(idx,:) = [];
%       ElectrodesOut.ElecLabels(end+1) = ElectrodesOut.ElecLabels(idx);
%       ElectrodesOut.ElecLabels(idx) = [];
%     end;
%   else
%      mydisp(['Using Cz as ground electrode']);
%     idx = find(strcmp('Cz',ElectrodesOut.ElecLabels));
%     if isempty(idx)||(length(idx)>1)
%       error('Something is wrong with locating the ground electrode');
%     else
%       ElectrodesOut.ElecLoc(end+1,:) = ElectrodesOut.ElecLoc(idx,:);
%       ElectrodesOut.ElecLoc(idx,:) = [];
%       ElectrodesOut.ElecLabels(end+1) = ElectrodesOut.ElecLabels(idx);
%       ElectrodesOut.ElecLabels(idx) = [];
%     end;
%  end;

end;

return;