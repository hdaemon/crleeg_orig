function [ElectrodesOut] = EEG_GetElectrodeNodes(dirs,files,modelOptions,nrrdConductivity)

cd(modelOptions.modelDIR);

if (~modelOptions.recomputeAll)&&(exist('ElectrodeNodes.mat'))
  mydisp('Using Existing Electrode Nodes');
  load ElectrodeNodes.mat
else
  
  mydisp('Electrodes Not Solved for. Swap to Graphical matlab');
  mydisp('Have you run guiSlices yet to identify the fiducials?');
  mydisp('If not, you should run that now');
  keyboard;
  
  %% Get Electrode Locations from Photogrammetry
  [ Electrodes] = EEG_GetElectrodeLocations(dirs,files,modelOptions);
    
  %% Visualize things
  headSurf  = ExtractIsosurface('filledSkinSeg.nhdr',dirs.MRIFull ,modelOptions.modelDownSampleLevel,0.5);
  
  % Display head surface and electrode locations  
  ViewSurface(figure,headSurf);
  ViewEEG(gcf,Electrodes);
    
  mydisp('Check Quality of Electrode Locations');
  keyboard;  
  
  %% Convert (x,y,z) Electrode Locations to Node Indices
  
  % Why the hell aren't I just usnig the segmentation?
  mydisp('Computing Electrode Locations as Node Indices');
  if modelOptions.useAniso
    %If we're using an anisotropic model, we need to shift the node
    %locations
    [Xaniso Yaniso Zaniso] = convert_NodeLocations_IsoToAniso(headSurf.X,headSurf.Y,headSurf.Z);
    
    % Find the indices corresponding to non-zero conductivities.
    Q = find(squeeze(any(nrrdConductivity.data,1)));
    AnisoIdx = convert_NodesIsoToAniso(Q,nrrdConductivity.sizes(2:end));
    
    % Get a Binary Map of The Interior of the VOlume
    isInside = zeros(nrrdConductivity.sizes(2:end)+[1 1 1]);
    isInside(AnisoIdx) = 1;
    isInside = logical(isInside);
    
    % Push all the electrodes inside the head to the surface
    try
      tmp       = get_NearestNodes(Xaniso,Yaniso,Zaniso,~isInside,Electrodes.ElecLoc);
      [tmpX tmpY tmpZ] = ind2sub([length(Xaniso) length(Yaniso) length(Zaniso)],tmp);
      tmpPos = [Xaniso(tmpX)' Yaniso(tmpY)' Zaniso(tmpZ)'];
    catch
      mydisp('Error Moving Electrodes to Surface of Head');
      keyboard;
    end;
    
    % Move electrodes outside the head to the surface
    ElecNodes = get_NearestNodes(Xaniso,Yaniso,Zaniso,isInside,tmpPos);
    [tmpX tmpY tmpZ] = ind2sub([length(Xaniso) length(Yaniso) length(Zaniso)],ElecNodes);
    ElecLoc = [Xaniso(tmpX)' Yaniso(tmpY)' Zaniso(tmpZ)'];
  
  else
    warning('WHY ARE YOU USING AN ISOTROPIC MODEL');
    keyboard;
    
%     isInside = (nrrdConductivity.data>0);
%     
%     tmp = get_NearestNodes(headSurf.X,headSurf.Y,headSurf.Z,~isInside,ElecLoc);
%     [tmpX tmpY tmpZ] = ind2sub([length(headSurf.X) length(headSurf.Y) length(headSurf.Z)],tmp);
%     tmpPos = [headSurf.X(tmpX)' headSurf.Y(tmpY)' headSurf.Z(tmpZ)'];
%     
%     ElecNodes = get_NearestNodes(headSurf.X,headSurf.Y,headSurf.Z,isInside,tmpPos);
    
  end;
  
  
  %% Define the reference electrode.
  if any(modelOptions.refElectrodeLoc) % We define an (X,Y,Z) Reference Location
    mydisp('Using a prespecified (X,Y,Z) reference electrode location');    
    GndLoc  = modelOptions.refElectrodeLoc;
    GndIdx  = -1;
    GndNode =  0; 
    
  elseif ~isempty(modelOptions.gndElectrode) % We declare a particular electrode to be the reference
    mydisp(['Using electrode ' num2str(modelOptions.gndElectrode) ' as reference electrode']);   
    %ElectrodesOut.ElecLoc(end+1,:) = ElectrodesOut.ElecLoc(modelOptions.gndElectrode,:);    
    %ElectrodesOut.ElecLoc(modelOptions.gndElectrode,:) = [];
    
    GndLoc  = ElecLoc(modelOptions.gndElectrode,:);
    GndIdx  = modelOptions.gndElectrode;
    GndNode = ElecNodes(modelOptions.gndElectrode);
  else
    keyboard;
    error('I don''t know where to put the ground electrode!!!');
  end;
  
  % Save out the ElectrodeNodes structure
  cd([dirs.ProcessedRootDIR dirs.SaveMATFiles]);
  save ElectrodeNodes ElecNodes Electrodes GndLoc GndIdx GndNode ElecLoc;
        
  
end;

% Build Output Object
ElectrodesOut.Nodes     = ElecNodes;
ElectrodesOut.Labels    = Electrodes.ElecLabels;
ElectrodesOut.Locations_Orig = Electrodes.ElecLoc;
if exist('ElecLoc') % This is here for compatibility with older code
  ElectrodesOut.Locations = ElecLoc;
else
  ElectrodesOut.Locations = Electrodes.ElecLoc;
end;
ElectrodesOut.GndLoc    = GndLoc;
ElectrodesOut.GndIdx    = GndIdx;
ElectrodesOut.GndNode   = GndNode;

return
