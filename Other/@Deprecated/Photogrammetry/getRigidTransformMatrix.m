%% generate rigid transform matrix
function [T,s] = getRigidTransformMatrix( Tr, photoObj )

% make it a row vector
Tr = Tr(:)'; 

switch photoObj.modeltr
    case 'translation', Tr = [Tr 0 0 0]; 
    case 'rotation',    Tr = [0 0 0 Tr];
    otherwise
end
                
if numel(Tr) == 6, Tr = [10 Tr]; end

Tr(2) = Tr(2) * photoObj.shiftscale;
Tr(3) = Tr(3) * photoObj.shiftscale;
Tr(4) = Tr(4) * photoObj.shiftscale;

Tr(5) = Tr(5) * photoObj.anglescale;
Tr(6) = Tr(6) * photoObj.anglescale;
Tr(7) = Tr(7) * photoObj.anglescale;

s = Tr(1); % scale factor

At = [ 0,0,0,Tr(2); ...
       0,0,0,Tr(3); ...
       0,0,0,Tr(4); ...
       0,0,0,0 ]; % x,y,z translation
     
Ax = [ 1,          0,           0, 0; ... 
       0, cos(Tr(5)), -sin(Tr(5)), 0; ...
       0, sin(Tr(5)),  cos(Tr(5)), 0; ...
       0,          0,           0, 1 ]; % x-rotation
     
Ay = [ cos(Tr(6)), 0, sin(Tr(6)), 0; ...
                0, 1,          0, 0; ...
      -sin(Tr(6)), 0, cos(Tr(6)), 0; ...
                0, 0,          0, 1 ]; % y-rotation

Az = [ cos(Tr(7)), -sin(Tr(7)), 0, 0; ...
       sin(Tr(7)),  cos(Tr(7)), 0, 0; ...
                0,           0, 1, 0; ...
                0,           0, 0, 1 ]; % z-rotation
              
T = Az * Ay * Ax + At; % compute general transform matrix