% Save results to file
% -------------------------------------------------------------------------
% Description:
%
% Usage: 
%   maOutputRegRes( 'create', 'filename.txt', '% HEADER', '%-------' )
%   maOutputRegRes( 'write', 'filename.txt', [1 2 3 4 5 6] )
% 
% Input:
%   - action: 'create' or 'write' or 'write_noenum'
%   - data: strings or numeric data
% 
% Output:
%
% Comments:
% 
% Notes:
% 
% -------------------------------------------------------------------------

function OutputRegRes( photoObj,action, varargin )


respath = photoObj.respath;
datapath = photoObj.datapath;


switch action
    case 'create'
        if nargin<3, error('Error: missing input arguments!'), end
        
        filename = [respath,varargin{1}];
        fid = fopen(filename,'a+'); 

        if fid<0, error('Error: could not open file "',filename,'".'), end        
                
        optargin = size(varargin,2);
        if optargin>1
            for i = 2 : optargin
                fprintf(fid,'%s\n',varargin{i});        
            end        
        end
        
        fclose(fid); % close file                
                     
    case {'write','write_noenum'}
        if nargin<4, error('Error: missing input arguments!'), end
        
        filename = [respath,varargin{1}];
        fid = fopen(filename,'a+');         
        
        if fid<0, warning('Could not open file "',filename,'".'), return, end               
        
        if strcmp(action,'write_noenum'), linenum = -1;
        else linenum = GetLastLineNumber( fid ); end
        
        optargin = size(varargin,2);      
        if optargin>1       
            for i = 2 : optargin
                data = varargin{i};

                if isnumeric(data)
                    if linenum >= 0,
                        % print line number
                        str = sprintf('%3d',linenum);
                        fprintf(fid,'%s',str);
                        % set new line number
                        linenum = linenum + 1;                    
                    end

                    % print data
                    for i = 1 : numel(data)
                        str = sprintf('\t\t%4.4f',data(i));
                        fprintf(fid,'%s',str);
                    end        

                    % print newline
                    fprintf(fid,'\n');                                                                          
                elseif ischar(data)
                    fprintf(fid,'%s\n',varargin{i});                                        
                end        
            end
        end
        
        % close file
        fclose(fid); 
        
    case 'append'
        if nargin<3, error('Error: missing input arguments!'), end
        
        filename = [datapath,varargin{1}];
        fid = fopen(filename,'a+'); 

        if fid<0, error('Error: could not open file "',filename,'".'), end        
                
        optargin = size(varargin,2);
        if optargin>1
            for i = 2 : optargin
                fprintf(fid,'%s\n',varargin{i});        
            end        
        end
        
        fclose(fid); % close file          
        
    otherwise
        Warning('Uknown file command!')
end
        
%% find last line
function linenum = GetLastLineNumber( fid )

linenum = 0;
if fseek(fid, 0, 'bof') < 0, return, end
line = fgetl( fid ); % read first line 

% loop through file
while line~=-1   
    cell = textscan(line,'%n'); % convert line data            
    if ~isempty(cell{1})  
        linenum = cell{1};
        linenum = linenum(1) + 1;
    else
        linenum = 0;
    end
    line = fgetl(fid); % read new line
end

fseek(fid, 0, 'eof');



