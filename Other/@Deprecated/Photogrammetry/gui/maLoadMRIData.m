% Read and display MRI dataset
% -------------------------------------------------------------------------
% Description:
% 
% Usage: 
%	 hdrMRI = maLoadMRIData('bestt1w.nhdr','bestt1wseg.nhdr','none')
% 
% Input:
%   - nhdrMRI: header file for mri data (.nhdr)
%   - mhdrMASK: header file for mask data (.nhdr)
%   - view: 'quickslices', 'render3D', 'none'
% 
% Output:
%   - hdrMRI: structure with image data & metadata
%
% Comments:
% 
% Notes:
% 
% -------------------------------------------------------------------------

function hdrMRI = maLoadMRIData( nhdrMRI,nhdrMASK,view )

% MRI data: read NRRD header + data
disp(['Loading file: ',nhdrMRI]);
hdrMRI = file_NRRD(nhdrMRI); % maReadNrrdHeader( nhdrMRI );

% segmentation mask: read NRRD header + data
if( ~isempty(nhdrMASK) )   
    disp(['Loading file: ',nhdrMASK]);
    hdrSegMask = maReadNrrdHeader( nhdrMASK );
    hdrMRI.data = hdrMRI.data .* hdrSegMask.data; % mask MRI data
    clear hdrSegMask; % release mask
end

% show preview
if(strcmp(view,'quickslices'))
    viewquick(hdrMRI);
elseif(strcmp(view,'render3D'))
    view3D(hdrMRI);
end

% Quick preview of slices in (y,z)-(x,z)-(x,y) planes
function viewquick(headerInfo)

map = colormap(bone(256));

figure
disp('X-dim preview ...')
for i = 1 : headerInfo.sizes(1)
    img(:,:) = headerInfo.data(i,:,:);
    image(img);
    colormap(map);
    pause(0.1);
end

disp('Y-dim preview ...')
clear img
for i = 1 : headerInfo.sizes(2)
    img(:,:) = headerInfo.data(:,i,:);
    image(img);
    colormap(map);
    pause(0.1);
end

disp('Z-dim preview ...')
for i = 1 : headerInfo.sizes(3)
    image(headerInfo.data(:,:,i));
    colormap(map);
    pause(0.1);
end

clear img
close all

% 3D rendering of MRI data using isosurface
function view3D(headerInfo)

figure
% resample the data by removing every second slice
D = headerInfo.data;
D = D(1:2:end,1:2:end,1:2:end);

% isosurface
disp('Calculating isosurface...')
Ds = smooth3(D);
hiso = patch(isosurface(Ds,10),...
	'FaceColor',[1,.75,.65],...
	'EdgeColor','none');

% hcap = patch(isocaps(D,5),...
% 	'FaceColor','interp',...
% 	'EdgeColor','none');
% colormap(map)

% define the view
view(45,30) 
axis tight 

% compute aspect ratio
xa = sqrt(sum(headerInfo.spacedirections(1,:).^2));
ya = sqrt(sum(headerInfo.spacedirections(2,:).^2));
za = sqrt(sum(headerInfo.spacedirections(3,:).^2));
daspect([xa,ya,za])

% add lighting
disp('Calculating isonormals...')
lightangle(45,30); 
set(gcf,'Renderer','zbuffer'); lighting phong
isonormals(Ds,hiso)
% set(hcap,'AmbientStrength',.6)
set(hiso,'SpecularColorReflectance',0,'SpecularExponent',50)

% contour slice preview
% contourslice(headerInfo.data,[],[],100)
% axis ij
% xlim([1,headerInfo.sizes(2)])
% ylim([1,headerInfo.sizes(1)])
% daspect([1,1,1])
% colormap('default')    
% pause(0.1);    

% contour slice preview with multiple slices
% phandles = contourslice(headerInfo.data,[],[],[10,64,128,192,230],8);
% view(3); axis tight
% set(phandles,'LineWidth',2)

% 3D slice view
% slice(headerInfo.data,10,10,10)

