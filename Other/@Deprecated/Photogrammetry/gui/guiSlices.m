function varargout = guiSlices(varargin)
% GUISLICES M-file for guiSlices.fig
%      GUISLICES, by itself, creates a new GUISLICES or raises the existing
%      singleton*.
%
%      H = GUISLICES returns the handle to a new GUISLICES or the handle to
%      the existing singleton*.
%
%      GUISLICES('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in GUISLICES.M with the given input arguments.
%
%      GUISLICES('Property','Value',...) creates a new GUISLICES or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before guiSlices_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to guiSlices_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help guiSlices

% Last Modified by GUIDE v2.5 22-Aug-2008 13:36:59

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @guiSlices_OpeningFcn, ...
                   'gui_OutputFcn',  @guiSlices_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

% --- Executes just before guiSlices is made visible.
function guiSlices_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to guiSlices (see VARARGIN)

% Choose default command line output for guiSlices
handles.output = hObject;
handles.usrdata.sliderpos = zeros(1,3);

% h = actxcontrol('mwsamp.mwsampctrl.2', [0 0 200 200], ... 
%     gcf, 'sampev')
% h.registerevent({'Click', mycallbacks('myclick')});

% h = actxcontrol('progid','position',[0 0 200 200],...
% 	'parent',gcf,...
% 	'callback',{'Click' 'myClickHandler'});

% 	newa=arrow(gcbo,'down',[X 0]);
% 	set(gcf,'currentaxes',d.axmain);

% Update handles structure
guidata(hObject, handles);

% This sets up the initial plot - only do when we are invisible
% so window can get raised using guiSlices.
% if strcmp(get(hObject,'Visible'),'off')
%     plot(rand(5));
% end


% UIWAIT makes guiSlices wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = guiSlices_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

% --- Executes on button press in btnLoad.
function btnLoad_Callback(hObject, eventdata, handles)
% hObject    handle to btnLoad (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% axes(handles.axesPreview);
% cla;

filename = get(handles.editFilename,'String');
hdrMRI = maLoadMRIData( filename, [], [] );
% hdrMRI = maLoadMRIData( filename, 'bestt1w-uc-skin.nhdr', [] );

if get(handles.chkScaleData,'Value')
    minval = min(hdrMRI.data(:));
    maxval = max(hdrMRI.data(:));
    scale = 256 / (maxval - minval);
    
    hdrMRI.data = hdrMRI.data - minval;
    hdrMRI.data = hdrMRI.data .* scale;
end

handles.usrdata.volume = hdrMRI.data;
handles.usrdata.resample = 2;

% set slider properties: Min
set(handles.sldXSlider,'Min',1);
set(handles.sldYSlider,'Min',1);
set(handles.sldZSlider,'Min',1);

% set slider properties: Max
set(handles.sldXSlider,'Max',hdrMRI.sizes(1));
set(handles.sldYSlider,'Max',hdrMRI.sizes(2));
set(handles.sldZSlider,'Max',hdrMRI.sizes(3));

% set slider properties: Value
set(handles.sldXSlider,'Value',1);
set(handles.sldYSlider,'Value',1);
set(handles.sldZSlider,'Value',1);
handles.usrdata.sliderpos = ones(1,3);
handles.usrdata.sliderchg = 'Z';

% set slider text properties
set(handles.txtXSlider,'String',['X Slice #: ',num2str(1)]);
set(handles.txtYSlider,'String',['Y Slice #: ',num2str(1)]);
set(handles.txtZSlider,'String',['Z Slice #: ',num2str(1)]);

% make preview
makePreview(handles);

% save handles structure
guidata(hObject,handles);

% --------------------------------------------------------------------
function FileMenu_Callback(hObject, eventdata, handles)
% hObject    handle to FileMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function OpenMenuItem_Callback(hObject, eventdata, handles)
% hObject    handle to OpenMenuItem (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
file = uigetfile('*.fig');
if ~isequal(file, 0)
    open(file);
end

% --------------------------------------------------------------------
function PrintMenuItem_Callback(hObject, eventdata, handles)
% hObject    handle to PrintMenuItem (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
printdlg(handles.figure1)

% --------------------------------------------------------------------
function CloseMenuItem_Callback(hObject, eventdata, handles)
% hObject    handle to CloseMenuItem (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
selection = questdlg(['Close ' get(handles.figure1,'Name') '?'],...
                     ['Close ' get(handles.figure1,'Name') '...'],...
                     'Yes','No','Yes');
if strcmp(selection,'No')
    return;
end

delete(handles.figure1)


% --- Executes on selection change in popupmenu1.
function popupmenu1_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns popupmenu1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu1


% --- Executes during object creation, after setting all properties.
function popupmenu1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
     set(hObject,'BackgroundColor','white');
end

set(hObject, 'String', {'plot(rand(5))', 'plot(sin(1:0.01:25))', 'bar(1:.5:10)', 'plot(membrane)', 'surf(peaks)'});


% --- Executes on button press in chkPreview3D.
function chkPreview3D_Callback(hObject, eventdata, handles)
% hObject    handle to chkPreview3D (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of chkPreview3D


% --- Executes on slider movement.
function sldYSlider_Callback(hObject, eventdata, handles)
% hObject    handle to sldYSlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

yindex = round(get(handles.sldYSlider,'Value'));
set(handles.txtYSlider,'String',['Y Slice #: ',num2str(yindex)]);
handles.usrdata.sliderpos(2) = yindex;
handles.usrdata.sliderchg = 'Y';

makePreview( handles );

% save handles structure
guidata(hObject,handles);



% --- Executes during object creation, after setting all properties.
function sldYSlider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sldYSlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function sldZSlider_Callback(hObject, eventdata, handles)
% hObject    handle to sldZSlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

zindex = round(get(handles.sldZSlider,'Value'));
set(handles.txtZSlider,'String',['Z Slice #: ',num2str(zindex)]);
handles.usrdata.sliderpos(3) = zindex;
handles.usrdata.sliderchg = 'Z';

makePreview( handles );

% save handles structure
guidata(hObject,handles);


% --- Executes during object creation, after setting all properties.
function sldZSlider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sldZSlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function sldXSlider_Callback(hObject, eventdata, handles)
% hObject    handle to sldXSlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

xindex = round(get(handles.sldXSlider,'Value'));
set(handles.txtXSlider,'String',['X Slice #: ',num2str(xindex)]);
handles.usrdata.sliderpos(1) = xindex;
handles.usrdata.sliderchg = 'X';

makePreview( handles );

% save handles structure
guidata(hObject,handles);

% --- Executes during object creation, after setting all properties.
function sldXSlider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sldXSlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function editFilename_Callback(hObject, eventdata, handles)
% hObject    handle to editFilename (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editFilename as text
%        str2double(get(hObject,'String')) returns contents of editFilename as a double


% --- Executes during object creation, after setting all properties.
function editFilename_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editFilename (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Generates preview on the axes
function makePreview( handles )

% cla(handles.axesPreview);
% axes(handles.axesPreview);

if get(handles.chkPreview3D,'Value')
   % preview in 3D 
    rs = handles.usrdata.resample;  
    minivol = handles.usrdata.volume(1:rs:end,1:rs:end,1:rs:end);
    [xs,ys,zs] = size(minivol);
    [x,y,z] = meshgrid(1:ys,1:xs,1:zs);
  
    xslice = floor(handles.usrdata.sliderpos(1)/rs); if ~xslice, xslice = 1; end
    yslice = floor(handles.usrdata.sliderpos(2)/rs); if ~yslice, yslice = 1; end
    zslice = floor(handles.usrdata.sliderpos(3)/rs); if ~zslice, zslice = 1; end      
    
    imghnd = slice(x,y,z,minivol,yslice,xslice,zslice)
else
   % preview in 2D
    switch handles.usrdata.sliderchg
        case 'X'
            index = handles.usrdata.sliderpos(1);
            if ~index, index = 1; end
            imghnd = image( squeeze(handles.usrdata.volume(index,:,:)) );
            title('Displaying Y-Z plane')            
        case 'Y'
            index = handles.usrdata.sliderpos(2);
            if ~index, index = 1; end
            imghnd = image( squeeze(handles.usrdata.volume(:,index,:)) );
            title('Displaying X-Z plane')
        case 'Z'  
            index = handles.usrdata.sliderpos(3);
            if ~index, index = 1; end
            imghnd = image( handles.usrdata.volume(:,:,index) );
            title('Displaying X-Y plane')
        otherwise
            return;
    end
    axis xy   
    
    if isfield(handles.usrdata,'point_coords')
        mypoints = [];
        mylabels = {};
        switch handles.usrdata.sliderchg
            case 'X', x = handles.usrdata.sliderpos(1); % z = c1; y = c2;
                      ind = find(handles.usrdata.point_coords(:,1)==x);
                      if ~isempty(ind), 
                          mypoints = handles.usrdata.point_coords(ind,[3 2]); 
                          mylabels = { handles.usrdata.point_labels{ind} };                          
                      end
            case 'Y', y = handles.usrdata.sliderpos(2); % z = c2; y = c1;
                      ind = find(handles.usrdata.point_coords(:,2)==y);
                      if ~isempty(ind), 
                          mypoints = handles.usrdata.point_coords(ind,[3 1]); 
                          mylabels = { handles.usrdata.point_labels{ind} };                          
                      end
            case 'Z', z = handles.usrdata.sliderpos(3); % x = c2; y = c1;
                      ind = find(handles.usrdata.point_coords(:,3)==z);
                      if ~isempty(ind), 
                        mypoints = handles.usrdata.point_coords(ind,[2 1]); 
                        mylabels = { handles.usrdata.point_labels{ind} };
                      end
            otherwise, return;
        end         
        if ~isempty(mypoints) && ~isempty(mylabels)
            axes(handles.axesPreview), hold on
            for i = 1 : size(mypoints)
                plot(handles.axesPreview,mypoints(1),mypoints(2),'+r')
                text(mypoints(1),mypoints(2),[' ',mylabels{i}],'Color','red')
            end
            axes(handles.axesPreview), hold off
        end
    end          
end
% colormap(bone(256))
colormap(jet(256))

set(imghnd,'ButtonDownFcn',@axesPreview_ButtonDownFcn);

% --- Executes on button press in btnLoadMat.
function btnLoadMat_Callback(hObject, eventdata, handles)
% hObject    handle to btnLoadMat (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% axes(handles.axesPreview);
% cla;

filename = get(handles.editMatFilename,'String');
strVol = load(filename);

if get(handles.chkScaleData,'Value')
    minval = min(strVol.volume(:));
    maxval = max(strVol.volume(:));
    scale = 256 / (maxval - minval);
    
    for i = 1 : length(strVol.volume(:))
        strVol.volume(i) = scale * (strVol.volume(i) - minval);        
    end    
end

handles.usrdata.volume = strVol.volume;
handles.usrdata.resample = 1;
[xs,ys,zs] = size(strVol.volume);

% set slider properties: Min
set(handles.sldXSlider,'Min',1);
set(handles.sldYSlider,'Min',1);
set(handles.sldZSlider,'Min',1);

% set slider properties: Max
set(handles.sldXSlider,'Max',xs);
set(handles.sldYSlider,'Max',ys);
set(handles.sldZSlider,'Max',zs);

% set slider properties: Value
set(handles.sldXSlider,'Value',1);
set(handles.sldYSlider,'Value',1);
set(handles.sldZSlider,'Value',1);
handles.usrdata.sliderpos = ones(1,3);
handles.usrdata.sliderchg = 'Z';

% set slider text properties
set(handles.txtXSlider,'String',['X Slice #: ',num2str(1)]);
set(handles.txtYSlider,'String',['Y Slice #: ',num2str(1)]);
set(handles.txtZSlider,'String',['Z Slice #: ',num2str(1)]);

% make preview
makePreview(handles);

% save handles structure
guidata(hObject,handles);



function editMatFilename_Callback(hObject, eventdata, handles)
% hObject    handle to editMatFilename (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editMatFilename as text
%        str2double(get(hObject,'String')) returns contents of editMatFilename as a double


% --- Executes during object creation, after setting all properties.
function editMatFilename_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editMatFilename (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in chkScaleData.
function chkScaleData_Callback(hObject, eventdata, handles)
% hObject    handle to chkScaleData (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of chkScaleData


% --- Executes on button press in pushbutton7.
function pushbutton7_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

list_entries = get(handles.listbox1,'String');
index_selected = get(handles.listbox1,'Value');
currentVal = min(index_selected);
if isempty(index_selected), return; end

ind = [];
for i = 1 : numel(list_entries)    
    if isempty(find(index_selected==i))
        ind = [ind i];
    end
end    

if isempty(ind), list_entries = {}; 
else list_entries = {list_entries{ind}}; end
currentVal = min(currentVal,size(list_entries,1));
set(handles.listbox1,'Value',currentVal,'String',list_entries)

if isempty(ind), rmfield(handles.usrdata,'point_labels'); end
if isempty(ind), rmfield(handles.usrdata,'point_coords'); end
if ~isfield(handles.usrdata,'point_labels'), return, end
if ~isfield(handles.usrdata,'point_coords'), return, end
handles.usrdata.point_labels = {handles.usrdata.point_labels{ind}};
handles.usrdata.point_coords = handles.usrdata.point_coords(ind,:);

makePreview( handles );

% save handles structure
guidata(hObject,handles);
    

% --- Executes on selection change in popupmenu2.
function popupmenu2_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns popupmenu2 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu2

contents = get(hObject,'String');
handles.usrdata.curfiducial = contents{get(hObject,'Value')};

% save handles structure
guidata(hObject,handles);


% --- Executes during object creation, after setting all properties.
function popupmenu2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on mouse press over axes background.
function axesPreview_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to axesPreview (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

handles = guidata(hObject);

contents = get(handles.popupmenu2,'String');
handles.usrdata.curfiducial = contents{get(handles.popupmenu2,'Value')};

pos = get(handles.axesPreview,'CurrentPoint');
c1 = pos(1,1);
c2 = pos(1,2);

axes(handles.axesPreview), hold on
plot(handles.axesPreview,c1,c2,'+r')
text(c1,c2,[' ',handles.usrdata.curfiducial],'Color','red')
axes(handles.axesPreview), hold off

c1 = round(c1);
c2 = round(c2);

switch handles.usrdata.sliderchg
    case 'X', x = handles.usrdata.sliderpos(1); z = c1; y = c2;
    case 'Y', y = handles.usrdata.sliderpos(2); z = c2; y = c1;
    case 'Z', z = handles.usrdata.sliderpos(3); x = c2; y = c1;
    otherwise, return;
end



if ~isfield(handles.usrdata,'point_labels'), handles.usrdata.point_labels = {}; end
if ~isfield(handles.usrdata,'point_coords'), handles.usrdata.point_coords = []; end
handles.usrdata.point_labels = {handles.usrdata.point_labels{:}, handles.usrdata.curfiducial};
handles.usrdata.point_coords = [handles.usrdata.point_coords; [x y z]];

str = [handles.usrdata.curfiducial,': ',num2str(x),' ',num2str(y),' ',num2str(z)];
list_entries = get(handles.listbox1,'String');
if isempty(list_entries), list_entries = {str};
else list_entries = {list_entries{:}, str}; end
set(handles.listbox1,'String',list_entries,'Value',size(list_entries,1));

% save handles structure
guidata(hObject,handles);


% --- Executes on key press with focus on pushbutton7 and none of its controls.
function pushbutton7_KeyPressFcn(hObject, eventdata, handles)
% hObject    handle to pushbutton7 (see GCBO)
% eventdata  structure with the following fields (see UICONTROL)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on mouse press over figure background.
function figure1_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

pos = get(hObject,'CurrentPoint');


% --- Executes on selection change in listbox1.
function listbox1_Callback(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns listbox1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listbox1


% --- Executes during object creation, after setting all properties.
function listbox1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton11.
function pushbutton11_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global matpath

if ~isfield(handles.usrdata,'point_labels'), return, end
if ~isfield(handles.usrdata,'point_coords'), return, end
labels = handles.usrdata.point_labels;
coords = handles.usrdata.point_coords;

i = 1;
filename = ['fiducials-',num2str(i),'.mat'];
while exist(filename)
    i = i + 1;
    filename = ['fiducials-',num2str(i),'.mat'];
end    
save([matpath,filename],'labels','coords');
