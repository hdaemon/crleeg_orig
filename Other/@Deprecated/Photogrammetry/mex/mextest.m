%%
% abspath = 'Z:\Work\Research\Software\MedAna\EEG-MRI registration\Algorithms'; % work: bukev
abspath = 'D:\Work\EEG-MRI registration\Algorithms'; % home: pavilion
addpath(abspath)
%% speed test
hcfopt = maGetHCFOptions( 'mode','global',...
                            'radius', 10,...
                            'step',1,...
                            'interpolation','partvol',...
                            'binning', 32 ); % set measure

% maGetHistograms( 'init', hcfopt )                       
% disp('Testing partvol...')                        
% tic
% for i = 1 : 10
% H1 = maGetHistograms( 'exec', posEEG );          
% end
% toc
% maGetHistograms( 'kill' );


posEEG = maReadEEGData( 'electrode-net-coordinates.txt','' );

hcfopt.interpolation = 'mexpartvol';
maGetHistograms( 'init', hcfopt );



disp('Testing mexpartvol...')    
nevals = 50;
tic
for i = 1 : nevals
H1 = maGetHistograms( 'exec', posEEG*.10 );          
end
time = toc;
disp(['mean time: ',num2str(time/nevals)])
disp(['total time: ',num2str(time)])
              

%% accuracy test
hcfopt = maGetHCFOptions( 'mode','global',...
                            'radius', 12,...
                            'step',2,...
                            'interpolation','partvol',...
                            'binning', 32 ); % set measure

maGetHistograms( 'init', hcfopt )
tic
H1 = maGetHistograms( 'exec', posEEG.*10 );          
toc
maGetHistograms( 'kill' );

hcfopt.interpolation = 'mexpartvol';
maGetHistograms( 'init', hcfopt );
tic
H2 = maGetHistograms( 'exec', posEEG.*10 );          
toc
disp(['max diff: ',num2str(max(max(H1-H2)))])
maGetHistograms( 'kill' );



                        