
#include "avs.h"
#include <string.h>
#include <stdio.h>
#include <time.h>
#include <math.h>
#include "mex.h"
#include "math.h"

//////////////////////////////////////////////////////////////////////////
//default (factory) settings
unsigned char __factoryNumberOfChannels__=1;
float __factoryWlCoeffs__[8]=
{	
	1.767172494e2, 6.038721202e-1, -1.653022887e-5, -2.091938142e-9, 0.0,
};
float __factoryGain__=1.2;
float __factoryOffset__=-130;
unsigned short __factoryStartPixel__=0;
unsigned short __factoryStopPixel__=1615;
///////////////////////////////////////////////////////////////////////////


#define __round__(a)\
	((a)>=0)?((ceil(a)-(a)<=0.5)? ceil(a):floor(a)):((ceil(a)-(a)<0.5)? ceil(a):floor(a))

bool silent_=false;
void _ShowMessage(char *msg){
	if(!silent_){
  	mexPrintf(msg);
  	mexPrintf("\n");
  }
}

Avs *avs1=NULL;
char *passwd=NULL;

const char *CommandNames[]={"open", "close", "list devices", "select device",
	"acquire", "wavelengths", "integration time", "is open", "averaging", "channel", 
	"filter", "pixel selection", "dynamic dark", "set io", "get io", 
	"integration delay", "number of channels", "number of pixels", "gain", 
	"offse", "coefficients", "pixel range", "password", "restore defaults", 
	"trigger", "dll version", "detector type", "firmware version", 
	"device serial number", "help",NULL};

void mavsExit(){
	if (avs1){
		delete avs1;
		avs1=NULL;
	}
	return;
}

void mexFunction(int nlhs, mxArray *plhs[], int nrhs,
                 const mxArray *prhs[])
                 
{
	unsigned int uitmp1=0, uitmp2=0;
	int nchars=0,i=0, itmp1=0;
	char *commandstring;
	unsigned int len=0;
	unsigned short ustmp1=0, ustmp2=0;
	unsigned char uctmp1=0;
	AvsIdentityType *ls;
	double dtmp1=0, *dptr1=NULL, *dptr2=NULL, *dptr3=NULL;
	float ftmp1=0, fa1[NR_FIT_COEF], *fptr1=NULL, *fptr2=NULL;
		
	/*Clean up function*/
	mexAtExit(mavsExit);
		
	/* Check for proper number of arguments. */
  if (nrhs < 1)
    mexErrMsgTxt("At least one input argument required.");

  if(nlhs > 2)
	mexErrMsgTxt("Too many output arguments.");
	
	/*The first input argument must be a string.*/
	if (!mxIsChar(prhs[0]))
  	mexErrMsgTxt("The first input argument must be a string of characters.");
  	
	
	nchars=mxGetNumberOfElements(prhs[0]);
  if(nchars<1)
  	mexErrMsgTxt("The first input argument must be a string of character.");
	commandstring=(char *)mxCalloc(nchars+1,sizeof(char));
  mxGetString(prhs[0],commandstring,nchars+1);
  
  if(!strcmpi(commandstring,"open")){
  	//Open device:
		mxFree(commandstring);
		if(avs1){
			mexWarnMsgTxt("The device is allready opened.");
			plhs[0]=mxCreateDoubleMatrix(1,1,mxREAL);
			*(double *)mxGetPr(plhs[0])=1;
			return;
		}
		if(nlhs>1)
			mexErrMsgTxt("Too many output arguments.");
		if(nrhs>2)
			mexErrMsgTxt("Too many input arguments.");
		//Create the driver object
		itmp1=0;		//default is USB communication port
		if(nrhs==2){
			if(!mxIsChar(prhs[1]))
				mexErrMsgTxt("The second input argument must be a string of characters.");
				
			nchars=mxGetNumberOfElements(prhs[1]);
 		 	if(nchars<1)
  			mexErrMsgTxt("The second input argument must be a string of character.");
			commandstring=(char *)mxCalloc(nchars+1,sizeof(char));
  		mxGetString(prhs[1],commandstring,nchars+1);
  
			if(!strcmpi("USB",commandstring)){
				itmp1=0;
			}else if(!strcmpi("COM1",commandstring)){
				itmp1=1;
			}else if(!strcmpi("COM2",commandstring)){
				itmp1=2;
			}else if(!strcmpi("COM3",commandstring)){
				itmp1=3;
			}else if(!strcmpi("COM4",commandstring)){
				itmp1=4;
			}else if(!strcmpi("AUTO",commandstring)){
				itmp1=-1;
			}else{
				mexWarnMsgTxt("The specified communication port is not available. Using auto-detect.");
			}
			mxFree(commandstring);
		}
		avs1=new Avs(itmp1);

		plhs[0]=mxCreateDoubleMatrix(1,1,mxREAL);
		*(double *)mxGetPr(plhs[0])=1;
		if(!avs1->libraryLoaded || !avs1->functionsLoaded){
			delete avs1;
			avs1=NULL;
			*(double *)mxGetPr(plhs[0])=-1;		
			mexErrMsgTxt("Failed to load the AVS Dll.");
		}
		if(avs1->communicationPort<0 || avs1->communicationPort>4){
			delete avs1;
			avs1=NULL;
			*(double *)mxGetPr(plhs[0])=-1;		
			switch(itmp1){
				case -1:
					mexWarnMsgTxt("No devices found.");
					break;
				case 0:
					mexWarnMsgTxt("No devices found on USB.");
					break;
				case 1:
					mexWarnMsgTxt("No devices found on the COM1 port.");
					break;
				case 2:
					mexWarnMsgTxt("No devices found on the COM2 port.");
					break;
				case 3:
					mexWarnMsgTxt("No devices found on the COM3 port.");
					break;
				case 4:
					mexWarnMsgTxt("No devices found on the COM4 port.");
					break;
				default:
					mexWarnMsgTxt("No devices found.");
					break;
			}
		}
		if(avs1->devHandle==INVALID_AVS_HANDLE_VALUE){
			*(double *)mxGetPr(plhs[0])=-1;		
			delete avs1;
			avs1=NULL;
			mexWarnMsgTxt("No devices found.");
		}
		//return 1 if a device was successfully opened
	}
	else if(!strcmpi(commandstring,"acquire")){
		/*Select a device from the list returned by the "list devices command."*/
		mxFree(commandstring);
		if(!avs1)
			mexErrMsgTxt("You must first open the AVS device.");
		if(nlhs>2)
			mexErrMsgTxt("Too many output arguments.");
		if(nrhs>2)
			mexErrMsgTxt("Too many input arguments.");
		
		itmp1=-1;
		if(nrhs>1){
			if(!mxIsDouble(prhs[1])){
				mexErrMsgTxt("The second input argument should be an integer.");
			}
			itmp1=(int)__round__(*(double *)mxGetPr(prhs[1]));
		}
		if(avs1->Scan(itmp1)){
			mexErrMsgTxt("Failed to complete the acquisition.");
		}
		avs1->GetData(&dptr1,&dptr2,itmp1,avs1->channel);
		//the data array
		plhs[0]=mxCreateDoubleMatrix(itmp1,1,mxREAL);
		dptr3=(double *)mxGetPr(plhs[0]);
		for(i=0;i<itmp1;++i){
			dptr3[i]=dptr1[i];
		}
		if(nlhs>1){
			//the wavelengths array
			plhs[1]=mxCreateDoubleMatrix(itmp1,1,mxREAL);
			dptr3=(double *)mxGetPr(plhs[1]);
			for(i=0;i<itmp1;++i){
				dptr3[i]=dptr2[i];
			}
		}
		}
	else if(!strcmpi(commandstring,"wavelengths")){
		/*Select a device from the list returned by the "list devices command."*/
		mxFree(commandstring);
		if(!avs1)
			mexErrMsgTxt("You must first open the AVS device.");
		if(nlhs>1)
			mexErrMsgTxt("Too many output arguments.");
		if(nrhs!=1)
			mexErrMsgTxt("This function takes exactly one input argument.");
			
		avs1->GetWavelengths(&dptr2,uitmp1);
		plhs[0]=mxCreateDoubleMatrix(uitmp1,1,mxREAL);
		dptr1=(double *)mxGetPr(plhs[0]);
		for(i=0;i<(int)uitmp1;++i){
			dptr1[i]=dptr2[i];
		}
	}
	else if(!strcmpi(commandstring,"list devices")){
		/*Return all serial numbers of the currently attached devices in a cell array.*/
		mxFree(commandstring);
		if(!avs1)
			mexErrMsgTxt("You must first open the AVS device.");
		if(nlhs>1)
			mexErrMsgTxt("Too many output arguments.");
		if(nrhs>1)
			mexErrMsgTxt("Too many input arguments.");
		
		len=0;		
		avs1->GetDeviceList(len,&ls);
		plhs[0]=mxCreateCellMatrix(len,1);
		for(i=0;i<(int)len;++i){
			 mxSetCell(plhs[0],i,mxCreateString(ls[i].SerialNumber));
		}
	}
	else if(!strcmpi(commandstring,"select device")){
		/*Select a device from the list returned by the "list devices command."*/
		mxFree(commandstring);
		if(!avs1)
			mexErrMsgTxt("You must first open the AVS device.");
		if(nlhs>1)
			mexErrMsgTxt("Too many output arguments.");
		if(nrhs!=2)
			mexErrMsgTxt("This function takes exactly two input arguments.");
		
		if(!mxIsDouble(prhs[1])){
			mexErrMsgTxt("The second input argument should be a valid device number (zero based index of the device in the list returned by the \"list devices\" command.");
		}
		if(avs1->SelectDevice((int)__round__(*(double *)mxGetPr(prhs[0])))){
			mexErrMsgTxt("Failed to select the specified device.");
		}
	}
	else if(!strcmpi(commandstring,"close")){
		mxFree(commandstring);
		if(!avs1)
			mexErrMsgTxt("You must first open the AVS device.");
		if(nlhs>1)
			mexErrMsgTxt("Too many output arguments.");
		if(nrhs>1)
			mexErrMsgTxt("Too many input arguments.");
		
		delete avs1;
		avs1=NULL;
		mexPrintf("AVS device closed.\n");
		plhs[0]=mxCreateDoubleMatrix(1,1,mxREAL);
		*(double *)mxGetPr(plhs[0])=1;
	}
	else if(!strcmpi(commandstring,"is open")){
		if(nlhs>1)
			mexErrMsgTxt("Too many output arguments.");
		if(nrhs>1)
			mexErrMsgTxt("Too many input arguments.");
		plhs[0]=mxCreateDoubleMatrix(1,1,mxREAL);
		if(avs1)
			*(double *)mxGetPr(plhs[0])=1;
		else
			*(double *)mxGetPr(plhs[0])=0;
	}
	else if(!strcmpi(commandstring,"integration time")){
		mxFree(commandstring);
		if(!avs1)
			mexErrMsgTxt("You must first open the AVS device.");
		if(nlhs>1)
			mexErrMsgTxt("Too many output arguments.");
		if(nrhs>2)
			mexErrMsgTxt("Too many input arguments.");
		
		plhs[0]=mxCreateDoubleMatrix(1,1,mxREAL);
		*(double *)mxGetPr(plhs[0])=-1;
		if(nrhs>1){
			if(!mxIsDouble(prhs[1])){
				mexErrMsgTxt("The second input argument should be a valid integration time in ms.");
			}
			uitmp1=(unsigned int)__round__(*(double *)mxGetPr(prhs[1]));
			if(avs1->IntegrationTime(uitmp1,true)){
				mexErrMsgTxt("Failed to set the integration time.");
			}
		}
		avs1->IntegrationTime(uitmp1);
	  *(double *)mxGetPr(plhs[0])=(double)uitmp1;
	}
	else if(!strcmpi(commandstring,"averaging")){
		mxFree(commandstring);
		if(!avs1)
			mexErrMsgTxt("You must first open the AVS device.");
		if(nlhs>1)
			mexErrMsgTxt("Too many output arguments.");
		if(nrhs>2)
			mexErrMsgTxt("Too many input arguments.");
		
		plhs[0]=mxCreateDoubleMatrix(1,1,mxREAL);
		*(double *)mxGetPr(plhs[0])=-1;
		if(nrhs>1){
			if(!mxIsDouble(prhs[1])){
				mexErrMsgTxt("The second input argument should be a positive integer.");
			}
			uitmp1=(unsigned int)__round__(*(double *)mxGetPr(prhs[1]));
			if(avs1->AveragingNumber(uitmp1,true)){
				mexErrMsgTxt("Failed to set the specified averaging number.");
			}
		}
		avs1->AveragingNumber(uitmp1);
	  *(double *)mxGetPr(plhs[0])=(double)uitmp1;
	}
	else if(!strcmpi(commandstring,"channel")){
		mxFree(commandstring);
		if(!avs1)
			mexErrMsgTxt("You must first open the AVS device.");
		if(nlhs>1)
			mexErrMsgTxt("Too many output arguments.");
		if(nrhs>2)
			mexErrMsgTxt("Too many input arguments.");
		
		plhs[0]=mxCreateDoubleMatrix(1,1,mxREAL);
		*(double *)mxGetPr(plhs[0])=-1;
		if(nrhs>1){
			if(!mxIsDouble(prhs[1])){
				mexErrMsgTxt("The second input argument should be a positive integer.");
			}
			uitmp1=(unsigned int)__round__(*(double *)mxGetPr(prhs[1]));
			if(avs1->Channel(uitmp1,true)){
				mexErrMsgTxt("Failed to select the specified channel.");
			}
		}
		avs1->Channel(uitmp1);
	  *(double *)mxGetPr(plhs[0])=(double)uitmp1;
	}
	else if(!strcmpi(commandstring,"filter")){
		mxFree(commandstring);
		if(!avs1)
			mexErrMsgTxt("You must first open the AVS device.");
		if(nlhs>1)
			mexErrMsgTxt("Too many output arguments.");
		if(nrhs>2)
			mexErrMsgTxt("Too many input arguments.");
		
		plhs[0]=mxCreateDoubleMatrix(1,1,mxREAL);
		*(double *)mxGetPr(plhs[0])=-1;
		if(nrhs>1){
			if(!mxIsDouble(prhs[1])){
				mexErrMsgTxt("The second input argument should be a positive integer.");
			}
			ustmp1=(unsigned short)__round__(*(double *)mxGetPr(prhs[1]));
			if(avs1->Smoothing(ustmp1,true)){
				mexErrMsgTxt("Failed to set the filter length.");
			}
		}
		avs1->Smoothing(ustmp1);
	  *(double *)mxGetPr(plhs[0])=(double)ustmp1;
	}
	else if(!strcmpi(commandstring,"pixel selection")){
		mxFree(commandstring);
		if(!avs1)
			mexErrMsgTxt("You must first open the AVS device.");
		if(nlhs>1)
			mexErrMsgTxt("Too many output arguments.");
		if(nrhs>2)
			mexErrMsgTxt("Too many input arguments.");
		
		plhs[0]=mxCreateDoubleMatrix(2,1,mxREAL);
		dptr1=(double *)mxGetPr(plhs[0]);
		dptr1[0]=-1; dptr1[1]=-1;
		if(nrhs>1){
			if(!mxIsDouble(prhs[1]))
				mexErrMsgTxt("The second input argument should an integer matrix of two elements.");
			if(mxGetNumberOfElements(prhs[1])!=2)
				mexErrMsgTxt("The second input argument should an integer matrix of two elements.");

			dptr2=(double *)mxGetPr(prhs[1]);
			ustmp1=(unsigned int)__round__(dptr2[0]);
			ustmp2=(unsigned int)__round__(dptr2[1]);
			if(avs1->PixelSelection(ustmp1,ustmp2,true)){
				mexErrMsgTxt("Failed to set the specified pixel range.");
			}
		}
		avs1->PixelSelection(ustmp1,ustmp2);
	  dptr1[0]=(double)ustmp1; dptr1[1]=(double)ustmp2;
	}
	else if(!strcmpi(commandstring,"dynamic dark")){
		mxFree(commandstring);
		if(!avs1)
			mexErrMsgTxt("You must first open the AVS device.");
		if(nlhs>1)
			mexErrMsgTxt("Too many output arguments.");
		if(nrhs>2)
			mexErrMsgTxt("Too many input arguments.");
		
		plhs[0]=mxCreateDoubleMatrix(2,1,mxREAL);
		dptr1=(double *)mxGetPr(plhs[0]);
		dptr1[0]=-1; dptr1[1]=-1;
		if(nrhs>1){
			if(!mxIsDouble(prhs[1]))
				mexErrMsgTxt("The second input argument should an integer matrix of two elements.");
			if(mxGetNumberOfElements(prhs[1])!=2)
				mexErrMsgTxt("The second input argument should an integer matrix of two elements.");
				
			dptr2=(double *)mxGetPr(prhs[1]);
			uctmp1=(unsigned char)__round__(dptr2[0]);
			uitmp1=(unsigned int)__round__(dptr2[1]);
			if(avs1->DynamicDark(uctmp1,uitmp1,true)){
				mexErrMsgTxt("Failed to set the dynamic dark correction parameters.");
			}
		}
		avs1->DynamicDark(uctmp1,uitmp1);
	  dptr1[0]=(double)uctmp1; dptr1[1]=(double)uitmp1;
	}
	else if(!strcmpi(commandstring,"set io")){
		mxFree(commandstring);
		if(!avs1)
			mexErrMsgTxt("You must first open the AVS device.");
		if(nlhs>1)
			mexErrMsgTxt("Too many output arguments.");
		if(nrhs!=2)
			mexErrMsgTxt("This function takes exactly two input arguments.");
		
		plhs[0]=mxCreateDoubleMatrix(2,1,mxREAL);
		dptr1=(double *)mxGetPr(plhs[0]);
		dptr1[0]=-1; dptr1[1]=-1;
		
		if(!mxIsDouble(prhs[1])){
			mexErrMsgTxt("The second input argument should an integer matrix of two elements.");
		}
		dptr2=(double *)mxGetPr(prhs[1]);
		ustmp1=(unsigned short)__round__(dptr2[0]);
		ustmp2=(unsigned short)__round__(dptr2[1]);
		if(avs1->IOoutput(ustmp1,ustmp1)){
			mexErrMsgTxt("Failed to set the IO pins.");
		}
		dptr1[0]=(double)ustmp1; dptr1[1]=(double)ustmp2;
	}
	else if(!strcmpi(commandstring,"get io")){
		mxFree(commandstring);
		if(!avs1)
			mexErrMsgTxt("You must first open the AVS device.");
		if(nlhs>1)
			mexErrMsgTxt("Too many output arguments.");
		if(nrhs!=1)
			mexErrMsgTxt("This function takes exactly one input argument.");
		
		plhs[0]=mxCreateDoubleMatrix(1,1,mxREAL);
		*(double *)mxGetPr(plhs[0])=-1;

		if(avs1->IOinput(uctmp1)){
			mexErrMsgTxt("Failed to acquire the state of the input pin.");
		}
		*(double *)mxGetPr(plhs[0])=uctmp1;
	}
	else if(!strcmpi(commandstring,"integration delay")){
		mxFree(commandstring);
		if(!avs1)
			mexErrMsgTxt("You must first open the AVS device.");
		if(nlhs>1)
			mexErrMsgTxt("Too many output arguments.");
		if(nrhs>2)
			mexErrMsgTxt("Too many input arguments.");
		
		plhs[0]=mxCreateDoubleMatrix(1,1,mxREAL);
		*(double *)mxGetPr(plhs[0])=-1;
		
		if(nrhs>1){
			if(!mxIsDouble(prhs[1])){
				mexErrMsgTxt("The second input argument should be an integer.");
			}
			uitmp1=(unsigned int)__round__(*(double *)mxGetPr(prhs[1]));
			if(avs1->IntegrationDelay(uitmp1,true))
				mexErrMsgTxt("Failed to set the integration delay.");
		}
		avs1->IntegrationDelay(uitmp1);
		*(double *)mxGetPr(plhs[0])=(double)uitmp1;
	}
	else if(!strcmpi(commandstring,"number of channels")){
		mxFree(commandstring);
		if(!avs1)
			mexErrMsgTxt("You must first open the AVS device.");
		if(nlhs>1)
			mexErrMsgTxt("Too many output arguments.");
		if(nrhs>2)
			mexErrMsgTxt("Too many input arguments.");
		
		plhs[0]=mxCreateDoubleMatrix(1,1,mxREAL);
		*(double *)mxGetPr(plhs[0])=-1;
		if(nrhs>1){
			if(!mxIsDouble(prhs[1])){
				mexErrMsgTxt("The second input argument should be an integer.");
			}
			uctmp1=(unsigned char)*(double *)mxGetPr(prhs[1]);
			if(avs1->NumberOfChannels(uctmp1,true))
				mexWarnMsgTxt("Failed to set the number of channels.");
		}
		avs1->NumberOfChannels(uctmp1);
		*(double *)mxGetPr(plhs[0])=(double)uctmp1;
	}
	else if(!strcmpi(commandstring,"number of pixels")){
		mxFree(commandstring);
		if(!avs1)
			mexErrMsgTxt("You must first open the AVS device.");
		if(nlhs>1)
			mexErrMsgTxt("Too many output arguments.");
		if(nrhs!=1)
			mexErrMsgTxt("This function takes exactly one input argument.");
		
		plhs[0]=mxCreateDoubleMatrix(1,1,mxREAL);
		avs1->NumberOfPixels(ustmp1);
		*(double *)mxGetPr(plhs[0])=(double)ustmp1;
	}
	else if(!strcmpi(commandstring,"gain")){
		mxFree(commandstring);
		if(!avs1)
			mexErrMsgTxt("You must first open the AVS device.");
		if(nlhs>1)
			mexErrMsgTxt("Too many output arguments.");
		if(nrhs>2)
			mexErrMsgTxt("Too many input arguments.");
		
		plhs[0]=mxCreateDoubleMatrix(1,1,mxREAL);
		*(double *)mxGetPr(plhs[0])=-1;
		if(nrhs>1){
			if(!mxIsDouble(prhs[1])){
				mexErrMsgTxt("The second input argument should be a double value.");
			}
			ftmp1=(float)*(double *)mxGetPr(prhs[1]);
			if(avs1->Gain(ftmp1,true))
				mexWarnMsgTxt("Failed to set the gain.");
		}
		
		avs1->Gain(ftmp1);
		*(double *)mxGetPr(plhs[0])=(double)ftmp1;
	}
	else if(!strcmpi(commandstring,"offset")){
		mxFree(commandstring);
		if(!avs1)
			mexErrMsgTxt("You must first open the AVS device.");
		if(nlhs>1)
			mexErrMsgTxt("Too many output arguments.");
		if(nrhs>2)
			mexErrMsgTxt("Too many input arguments.");
		
		plhs[0]=mxCreateDoubleMatrix(1,1,mxREAL);
		*(double *)mxGetPr(plhs[0])=0;
		if(nrhs>1){
			if(!mxIsDouble(prhs[1])){
				mexErrMsgTxt("The second input argument should be a double value.");
			}
			ftmp1=(float)*(double *)mxGetPr(prhs[1]);
			if(avs1->Offset(ftmp1,true))
				mexWarnMsgTxt("Failed to set the offset.");
		}
		
		avs1->Offset(ftmp1);
		*(double *)mxGetPr(plhs[0])=(double)ftmp1;
	}
	else if(!strcmpi(commandstring,"coefficients")){
		mxFree(commandstring);
		if(!avs1)
			mexErrMsgTxt("You must first open the AVS device.");
		if(nlhs>1)
			mexErrMsgTxt("Too many output arguments.");
		if(nrhs>2)
			mexErrMsgTxt("Too many input arguments.");
		
		plhs[0]=mxCreateDoubleMatrix(NR_FIT_COEF,1,mxREAL);
		dptr1=(double *)mxGetPr(plhs[0]);
		for(i=0;i<NR_FIT_COEF;++i){
			dptr1[i]=0;
		}
		if(nrhs>1){
			if(!mxIsDouble(prhs[1]))
				mexErrMsgTxt("The second input argument should be a double array of five elements.");
			if(mxGetNumberOfElements(prhs[1])!=5)
				mexErrMsgTxt("The second input argument should be a double array of five elements.");
				
			dptr2=(double *)mxGetPr(prhs[1]);
			for(i=0;i<NR_FIT_COEF;++i){
				fa1[i]=(float)dptr2[i];
			}
			fptr1=fa1;
			if(avs1->WlCoeff(&fptr1,true))
				mexWarnMsgTxt("Failed to set the wavelength coefficients.");
		}
		avs1->WlCoeff(&fptr2);
		for(i=0;i<NR_FIT_COEF;++i){
			dptr1[i]=(double)fptr2[i];
		}
	}
	else if(!strcmpi(commandstring,"pixel range")){
		mxFree(commandstring);
		if(!avs1)
			mexErrMsgTxt("You must first open the AVS device.");
		if(nlhs>1)
			mexErrMsgTxt("Too many output arguments.");
		if(nrhs>2)
			mexErrMsgTxt("Too many input arguments.");
		
		plhs[0]=mxCreateDoubleMatrix(2,1,mxREAL);
		dptr1=(double *)mxGetPr(plhs[0]);
		dptr1[0]=-1; dptr1[1]=-1;
		if(nrhs>1){
			if(!mxIsDouble(prhs[1]))
				mexErrMsgTxt("The second input argument should an integer matrix of two elements.");
			if(mxGetNumberOfElements(prhs[1])!=2)
				mexErrMsgTxt("The second input argument should an integer matrix of two elements.");
				
			dptr2=(double *)mxGetPr(prhs[1]);
			ustmp1=(unsigned short)__round__(dptr2[0]);
			ustmp2=(unsigned short)__round__(dptr2[1]);
			if(avs1->StartStopPixel(ustmp1,ustmp2,true)){
				mexWarnMsgTxt("Failed to set the specified start and stop pixels.");
			}
		}
		avs1->StartStopPixel(ustmp1,ustmp2);
		dptr1[0]=(double)ustmp1; dptr1[1]=(double)ustmp2;
	}
	else if(!strcmpi(commandstring,"password")){
		mxFree(commandstring);
		if(!avs1)
			mexErrMsgTxt("You must first open the AVS device.");
		if(nlhs>1)
			mexErrMsgTxt("Too many output arguments.");
		if(nrhs!=2)
			mexErrMsgTxt("This function takes exactly two input arguments.");
		
		if(!mxIsChar(prhs[1]))
			mexErrMsgTxt("The second input argument must be a string of characters.");
		
		if(passwd)
			mxFree(passwd);

		nchars=mxGetNumberOfElements(prhs[1])+1;
		passwd=(char *)mxCalloc(nchars,sizeof(char));
		mxGetString(prhs[1],passwd,nchars);
		//set the password for one time usage
		avs1->Password(passwd);
		
		mxFree(passwd);
		passwd=NULL;
		
		plhs[0]=mxCreateDoubleMatrix(1,1,mxREAL);
		*(double *)mxGetPr(plhs[0])=1;
	}
	else if(!strcmpi(commandstring,"restore defaults")){
		mxFree(commandstring);
		if(!avs1)
			mexErrMsgTxt("You must first open the AVS device.");
		if(nlhs>1)
			mexErrMsgTxt("Too many output arguments.");
		if(nrhs!=1)
			mexErrMsgTxt("This function takes exactly one input argument.");

		avs1->RestoreFactoryDefaults(__factoryNumberOfChannels__, 
		&__factoryGain__,  &__factoryOffset__, __factoryWlCoeffs__, 
		&__factoryStartPixel__, &__factoryStopPixel__);
		
		plhs[0]=mxCreateDoubleMatrix(1,1,mxREAL);
		*(double *)mxGetPr(plhs[0])=1;
	}
	else if(!strcmpi(commandstring,"trigger")){
		mxFree(commandstring);
		if(!avs1)
			mexErrMsgTxt("You must first open the AVS device.");
		if(nlhs>1)
			mexErrMsgTxt("Too many output arguments.");
		if(nrhs>2)
			mexErrMsgTxt("Too many input arguments.");
		
		plhs[0]=mxCreateDoubleMatrix(1,1,mxREAL);
		*(double *)mxGetPr(plhs[0])=0;
		if(nrhs>1){
			if(!mxIsDouble(prhs[1])){
				mexErrMsgTxt("The second input argument should be an integer.");
			}
			if(*(double *)mxGetPr(prhs[1]))
				uctmp1=TRIGGER_EXTERNAL;
			else
				uctmp1=TRIGGER_INTERNAL;
				
			if(avs1->TriggerMode(uctmp1,true))
				mexWarnMsgTxt("Failed to set the external trigger.");
			else if(uctmp1==TRIGGER_EXTERNAL)
				mexWarnMsgTxt("External trigger on.");
			
		}
		
		avs1->TriggerMode(uctmp1);
		*(double *)mxGetPr(plhs[0])=(double)uctmp1;
	}
	else if(!strcmpi(commandstring,"dll version")){
		mxFree(commandstring);
		if(!avs1)
			mexErrMsgTxt("You must first open the AVS device.");
		if(nlhs>1)
			mexErrMsgTxt("Too many output arguments.");
		if(nrhs!=1)
			mexErrMsgTxt("This function takes exactly one input argument.");

		plhs[0]=mxCreateString(avs1->DllVersion);
	}
	else if(!strcmpi(commandstring,"detector type")){
		mxFree(commandstring);
		if(!avs1)
			mexErrMsgTxt("You must first open the AVS device.");
		if(nlhs>1)
			mexErrMsgTxt("Too many output arguments.");
		if(nrhs!=1)
			mexErrMsgTxt("This function takes exactly one input argument.");

		plhs[0]=mxCreateString(avs1->detectorName);
	}
	else if(!strcmpi(commandstring,"firmware version")){
		mxFree(commandstring);
		if(!avs1)
			mexErrMsgTxt("You must first open the AVS device.");
		if(nlhs>1)
			mexErrMsgTxt("Too many output arguments.");
		if(nrhs!=1)
			mexErrMsgTxt("This function takes exactly one input argument.");

		plhs[0]=mxCreateString(avs1->FirmwareVersion);
	}
	else if(!strcmpi(commandstring,"device serial number")){
		mxFree(commandstring);
		if(!avs1)
			mexErrMsgTxt("You must first open the AVS device.");
		if(nlhs>1)
			mexErrMsgTxt("Too many output arguments.");
		if(nrhs!=1)
			mexErrMsgTxt("This function takes exactly one input argument.");

		plhs[0]=mxCreateString(avs1->deviceSerial);
	}
	else if(!strcmpi(commandstring,"help")){
		/*Return all available commands.*/
		mxFree(commandstring);
		if(nlhs>1)
			mexErrMsgTxt("Too many output arguments.");
		if(nrhs>1)
			mexErrMsgTxt("Too many input arguments.");
		
		len=0;
		while (CommandNames[len]){
			
			 ++len;
		}
		plhs[0]=mxCreateCellMatrix(len,1);
		for(i=0;i<len;++i){
			mxSetCell(plhs[0],i,mxCreateString(CommandNames[i]));
		}
	}

	else{
		mxFree(commandstring);
		mexErrMsgTxt("Illegal command.");
	}
}