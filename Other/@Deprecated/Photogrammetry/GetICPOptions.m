% ICP Options 
% -------------------------------------------------------------------------
% Description:
% 	Get ICP (iterative closest point) options structure 
% 
% Usage: 
%	icpopt = maGetICPOptions( ); % default structure
%   icpopt = maGetICPOptions( 'mode', 'rms' )
%       
% Input:
%   - mode*: 'sum' or 'sad' or 'ssd' or 'rms'
%   
% Output:
%   - icpopt: structure with initialized settings
%
% Comments:
%   varargin: 'fieldname' followed by 'fieldvalue'
% 
% Notes:
% 
% -------------------------------------------------------------------------

function icpopt = GetICPOptions( varargin )

% init default structure
icpopt.mode = 'rms'; % 'rms' by default

if nargin > 1
    nvars = 1;
    while nvars < nargin 
        if ~ischar(varargin{nvars}), error('Error: field name should be a character array!'); end         
        if nvars+1 <= nargin, icpopt = setfield(icpopt,lower(varargin{nvars}),varargin{nvars+1}); end
        nvars = nvars + 2; 
    end
end













