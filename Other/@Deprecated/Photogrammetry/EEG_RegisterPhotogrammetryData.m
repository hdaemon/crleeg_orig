
%% Set Directories and Filenames
% ProcessedRootDIR = '/common/data/processed/epilepsysurgicalplanning/Case030/';
% dirMRI = 'damon_eeg/';
% dirPhotogrammetry = 'photogrammetry/';
% fileMRI = 'o_t1w.nhdr';
% fileSkinSeg = 'o_t1w_skinseg.nhdr';
% filePhotogrammetry = 'Michael-set3-ziga-exp.sfp';

function [ Electrodes ] = EEG_RegisterPhotogrammetryData(dirs,files,modelOptions);


mydisp('EEG_RegisterPhotogrammetryData >> BEGIN');
%% Get original directory
OrigDir = pwd;


% global casepath datapath respath matpath mripath curcase
% curcase = ' ';
% mripath  =  dirs.MRIFull;
% casepath =  dirs.PhotogrammetryFull;
% datapath =  dirs.PhotogrammetryFull;
% respath  =  [dirs.PhotogrammetryFull 'Res-Sessions/'];
% 

%% Check for the existence of necessary directories (Shoudl probably be moved to the validateandload() function
if ~isdir([dirs.PhotogrammetryFull 'Res-Sessions/'])
  mkdir(dirs.PhotogrammetryFull,'Res-Sessions');
end;

if ~isdir([dirs.PhotogrammetryFull 'Mat-Sessions/'])
  mkdir(dirs.PhotogrammetryFull,'Mat-Sessions');
end;

%% Set PhotogrammetryOptions
photoObj = photogrammetryObject(...
    'costfunction', modelOptions.photoCostFunction,...
    'modeltr'     , modelOptions.photoModelTr,...
    'optimization', modelOptions.photoOptimization,...
    'mrifile'     , files.MRI, ...
    'labelfile'   , files.SkinSeg, ...
    'eegfile'     , files.Photogrammetry, ...
    'mripath'     , dirs.MRIFull, ...    
    'datapath'    , dirs.PhotogrammetryFull, ...
    'respath'     , [dirs.PhotogrammetryFull 'Res-Sessions/'], ...
    'matpath'     , [dirs.PhotogrammetryFull 'Mat-Sessions/']);
     
%[finalTr, time, photoObj] = maRegistration(photoObj);

%% Get Heaed surface for display
% All this is doing is reading the MRI data from the skin surface file and
% generating the surface data.  Should probably just skip it.
DisplayRegRes(photoObj,'init');

%%  Preprocess Data
photoObj = photoObj.DataPreprocessing;
mydisp('EEG_RegisterPhotogrammetryData >> Data Preprocessing COmplete');
keyboard;

%%  Initialize Cost Function
photoObj = photoObj.InitializeCostFunction;
mydisp('EEG_RegisterPhotogrammmetryData >> Cost Function Initialized');
keyboard;

%%  Do initial rigid transformation
if ~photoObj.skipinitreg
  tic; mydisp('Computing initial closed-form rigid transformation..');
 photoObj = photoObj.InitialRegistration;
 time.initreg = toc;
end;
mydisp('EEG_RegisterPhotogrammetryData >> Rigid Transform Computed');
keyboard

%% cost function optimization
if ~isempty(photoObj.initTr) & strcmp(photoObj.modeltr,'rigid'), 
    shiftscale = photoObj.shiftscale;
    anglescale = photoObj.anglescale; 

    photoObj.shiftscale = 1;
    photoObj.anglescale = 1; 

    [T,s] = getRigidTransformMatrix( photoObj.initTr, photoObj );
    photoObj.initTr = T;    
        
    % restore scales
    photoObj.shiftscale = shiftscale;
    photoObj.anglescale = anglescale;           
end

tic, mydisp('   Optimizing cost function with respect to transformation... ');
photoObj = photoObj.CostFunctionOptimization;
time.costopt = toc; %fprintf(['Execution time: ',num2str(time.multistart),' s\n']);

%% evaluation of final registration & display of results
%tic, fprintf('   Evaluating final registration... \n');
%photoObj = maRegistrationEvaluation( photoObj );
%time.evaluation = toc; %fprintf(['Execution time: ',num2str(time.evaluation),' s\n']); 

%% compute total time
time.total = 0;

if isfield(time,'preproc'),    time.total = time.total + time.preproc;    end
if isfield(time,'initcf'),     time.total = time.total + time.initcf;     end
if isfield(time,'initreg'),    time.total = time.total + time.initreg;    end
if isfield(time,'multistart'), time.total = time.total + time.multistart; end
if isfield(time,'costopt'),    time.total = time.total + time.costopt;    end
if isfield(time,'evaluation'), time.total = time.total + time.evaluation; end

mydisp(['Finished in ',num2str(time.total),' s.'])            
            
%% clear handles photoObj structure
photoObj.cf = []; 

finalTr = photoObj.optimTr;
if nargout>1, varargout{1} = time; end
if nargout>2, varargout{2} = photoObj; end


Electrodes = photoObj;
cd(OrigDir);

mydisp('EEG_RegisterPHotogrammetryData >> END');
return;