% Display results
% -------------------------------------------------------------------------
% Description:
% 
% Usage: 
%   maDisplayRegRes( 'init' )
%   maDisplayRegRes( 'draw' )
%   maDisplayRegRes( 'draw', posNew, posNear, resid, hfig )
%   maDisplayRegRes( 'drawpoints', posNew, resid, hfig )
%   maDisplayRegRes( 'kill' )
% 
% Input:
%   - action: 'init' or 'draw' of 'done'(optional)
%   - myhfig*: my figure handle
%   - posNew*: positions of electrode points
%   - posNear*: positions of EEG target points on head surface
%   - resid*: residuals of points
% 
% Output:
%   - cmap: computed colormap for residuals
%
% Comments:
% 
% Notes:
%   - 09.07.2008: changed 'init' action; now surface data is computed 
%                 once and saved to 'SurfaceDisplayData.mat'
% -------------------------------------------------------------------------

function cmap = DisplayRegRes(photoObj, action, varargin)

matpath = photoObj.matpath;

%% display registration results
cmap = [];
        
switch( action )
    case 'init'                       
        if isempty(matpath), warning('Please reinitialize workspace! (maInitEnv.m)'), return, end        
        if exist('SurfaceDisplayData.mat','file'), return, end
                
        headSurf = ExtractIsosurface(photoObj.labelfile,photoObj.mripath,2,0.5);
                
        save([matpath,'SurfaceDisplayData.mat'],'headSurf');                       
       
    case 'draw'
        if ~exist([matpath 'SurfaceDisplayData.mat'],'file'), 
            warning('Missing surface data for display!'), return
        end        
        % load surface data
        load([matpath 'SurfaceDisplayData.mat'],'headSurf')
        
        if nargin<6, hfig = [];    else hfig  = varargin{4}; end        
        if nargin<5, resid = [];   else resid = varargin{3}; end
        if nargin<4, posNear = []; else posNear  = varargin{2}; end
        if nargin<3, posNew = [];  else posNew  = varargin{1}; end
               
        if isempty(hfig),             
            hfig = figure; % create new figure
        end
        
        mydisp('Plotting surface');
        figure(hfig); % select figure window    
        ViewSurface(hfig,headSurf);
        xlabel(gca,'X'), ylabel(gca,'Y'), zlabel(gca,'Z') % set axes labels
   
        % plot positions of EEG electrodes
        mydisp('Plotting EEG');
        cmap = ViewEEG( hfig,  posNew, posNear, resid, [] );               
        
        
        mydisp('Done DisplayRegRes');
    case 'drawpoints'
        if ~exist([matpath 'SurfaceDisplayData.mat'],'file'), 
            warning('Missing surface data for display!'), return
        end
        % load surface data
        load([matpath 'SurfaceDisplayData.mat'],'faces','vert','aspect','X','Y','Z')
        
        if nargin<7, hfig = []; else hfig  = varargin{5}; end        
        if nargin<6, edgecolor = []; else edgecolor = varargin{4}; end        
        if nargin<5, resid = []; else resid = varargin{3}; end
        if nargin<4, posNear = []; else posNear  = varargin{2}; end
        if nargin<3, posNew = []; else posNew  = varargin{1}; end
        
        if isempty(hfig),             
            hfig = figure; % create new figure
        end
        
        ViewSurface(hfig,headSurf); % draw surface from MRI
        xlabel(gca,'X'), ylabel(gca,'Y'), zlabel(gca,'Z') % set axes labels        
        
        % plot positions of EEG electrodes
        cmap = viewEEG( hfig, posNew, posNear, resid, edgecolor );              
        view(45,30), axis tight % define the view
        daspect(1./aspect) % set aspect ratio                               
    case 'done'            
%             clear faces vert aspect hfig X Y Z
            
    otherwise            
end
% 
% %% 3D rendering of MRI data
% function view3D( hfig, faces, vert, aspect, Ds )
% 
% if nargin<5, Ds = []; end
% 
% clf(hfig,'reset'); % clear all graphic objects 
% % plot3(vert(:,2),vert(:,1),vert(:,3),'.');
% 
% hiso = patch( 'Faces',faces,...
%               'Vertices',vert,...
%               'FaceColor',[1,.75,.65],...
%               'FaceAlpha',0.5,...
%               'EdgeColor','none'); %[0.7 0.7 0.7]); % draw surface              
%           
% % hiso = patch( 'Faces',faces,...
% %               'Vertices',vert,...          
% %               'FaceColor',[1,.75,.65],...
% %               'EdgeColor','none'); % draw surface
%          
% view(45,30), axis tight % define the view
% daspect(1./aspect) % set aspect ratio
% % % daspect(aspect) % set aspect ratio
% % 
% % if isempty(Ds), return, end
% % lightangle(45,30); % add lighting
% % set(hfig,'Renderer','zbuffer'); lighting phong
% 
% % clear  faces vert aspect
% % load headsurf.mat Ds
% % isonormals(Ds,hiso)
% % set(hiso,'SpecularColorReflectance',0,'SpecularExponent',50)
% % 
% % alpha(.5)
% 
% %% plotting of EEG electrodes
% function cmap = viewEEG( hfig, X, Y, Z, posNew, posNear, resid, edgecolor )
% 
% cmap = [];
% if isempty(posNew), return; end % if no data points, return
% 
% figure(hfig); % select figure window                            
% hold on; % lock figure data
% 
% [sX sY sZ] = sphere(30); % create sphere        
% npoints = max(size(posNew)); % number of points
% % sC = zeros(size(sZ)); sC(:) = 1;
% 
% % sX = sX * 3;
% % sY = sY * 3;
% % sZ = sZ * 3;
% 
% if ~isempty(resid)
%     [n,xout] = hist( resid );            
%     cmap = colormap(jet(length(xout)+2));                               
% end
% 
% 
% if ~isempty(edgecolor)
%     maxcolors = 5;
%     cmap = colormap(jet(maxcolors));
%     if edgecolor>maxcolors, edgecolor = maxcolors; end
%     cmap = cmap(edgecolor,:);
% end
% 
% save posNew posNew
% 
% for i = 1 : npoints
%     [minval,x] = min(abs(X - posNew(i,1))); % get x-dir index
%     [minval,y] = min(abs(Y - posNew(i,2))); % get y-dir index
%     [minval,z] = min(abs(Z - posNew(i,3))); % get z-dir index                   
% 
%     if ~isempty(resid)
%         if resid(i)<xout(1), ind = 1;                  
%         elseif resid(i)>xout(end), ind = length(xout)+2;
%         else ind = floor((resid(i)-xout(1))/abs(diff(xout(1:2)))) + 2; end
%         surf( gca, sX+x, sY+y, sZ+z, 'EdgeColor', cmap(ind,:) ); % plot sphere 
%     elseif ~isempty(edgecolor)
%         surf( gca, sX+x, sY+y, sZ+z, edgecolor*ones(size(sZ)), 'EdgeColor', cmap ); % plot sphere, edge color as in cmap        
%         colormap(cmap)
%     else
%         surf( gca, sX+x, sY+y, sZ+z ); % plot sphere, edge color default (black)
%     end        
% 
%     % surf( gca, sX+x, sY+y, sZ+z, 'CData', sC, 'EdgeColor', sE(1,:) ); % plot sphere                        
%     if(isempty(posNear)) continue; end
% 
%     [minval,x2] = min(abs(X - posNear(i,1))); % get x-dir index
%     [minval,y2] = min(abs(Y - posNear(i,2))); % get y-dir index
%     [minval,z2] = min(abs(Z - posNear(i,3))); % get z-dir index                                          
%     plot3([x2 x], [y2 y], [z2 z],'-r','LineWidth',2);
% end      
% 
% hold off; % unlock figure data  
% end
