% Load EEG electrodes position data
% -------------------------------------------------------------------------
% Description:
% 
% Usage: 
%	posEEG = maReadEEGData( 'electrode-net-coordinates.txt','' )
%   [pos,lab] = maReadEEGData('electrode-net-coordinates.txt','all')
% Input:
%   - filename: filename of EEG data points
%   - pointset*: 'electrodes' or 'fiducials' or 'all'
% 
% Output:
%   - points: n x 3 matrix of EEG point locations
%   - labels*: labels of points 
% 
%
% Comments:
% 
% Notes:
% 
% -------------------------------------------------------------------------

function [points, varargout] = ReadEEGData(filename,datapath, varargin)

warning('This is part of the photogrammetry code.  This should be relocated to the EEG reading code for standardization');

% Input parser
p = inputParser;
addRequired(p,'filename');
addRequired(p,'datapath');
addOptional(p,'pointset','electrodes');

parse(p,filename,datapath,varargin);

% Dump inputs to variables
filename = p.Results.filename;
datapath = p.Results.datapath;
pointset = p.Results.pointset;

% Open The File
mydisp(['Reading EEG Data From: ' [datapath filename] ]);
id = fopen([datapath filename], 'r' ); % open data file
if id<0, 
  warning(['File "',filename,'" does not exist!']);
  return;
end

line = fgetl( id ); % read first line


% loop through file
try
labels = {}; 
points = ones( 0,3 ); % create empty matrix
while line~=-1
    cell = textscan(line,'%s %f %f %f');         % convert line data    
    points = [points;[cell{2},cell{3},cell{4}]]; % convert to matrix form
    labels = {labels{:},cell2mat(cell{1})}; 
    line = fgetl(id);                            % read new line
end

catch
  disp('Caught error in ReadEEGData.m');
  keyboard
  
end;

fclose(id); % close file

% filter data
if strcmp(pointset,'electrodes')
    % Find just those points whose description starts with 'E'
    ind = zeros(size(points,1),1);    
    for i = 1 : numel(ind)
        % check description of data (if E... its the electrode)    
        desc = labels{i}; % convert to string (class: char)        
        if(desc(1) == 'E'), ind(i) = 1; end   
    end
    points = points(find(ind>0),:);
    labels = labels(find(ind>0));
elseif strcmp(pointset,'fiducials')    
    ind = zeros(size(points,1),1);    
    for i = 1 : numel(ind)
        % check description of data (if E... its the electrode)    
        desc = labels{i}; % convert to string (class: char)        
        if(desc(1) ~= 'E'), ind(i) = 1; end   
    end
    points = points(find(ind>0),:);
    labels = labels(find(ind>0));               
end


if nargout>0, varargout{1} = labels; end


