% MRI Segmentation Methods
% -------------------------------------------------------------------------
% Description:
% 	MRI segmentation
% 
% Usage: 
%	[segImage] = maMRISegmentation('grow3D', 'bestt1w-us.nhdr')
%   [segImage,thr] = maMRISegmentation('otsu', 'bestt1w-us.nhdr')
%       
% Input:
%   - method: 'otsu' or 'grow3D'
%   - datafile: select datafile
%   - casestr*: case to initialize 
%   
% Output:
%   - segImage*: segmented image
%   - thr*: computed threshold for Otsu method
% 
% Comments:
% 
% Notes:
% 
% -------------------------------------------------------------------------
%% Segmentation
%   - Otsu grey-level thresholding
%   - 3D region growing

function [photoObj] = MRISegmentation(photoObj,method)

% The stuff that'd modified in here is photoObj.volume and photoObj.otsu
switch method
    case 'otsu'    
        %% Otsu thresholding                 
        [volume,sep,thr] = otsu(photoObj.nrrdMRI.data,2); 
        
        photoObj.volume = volume;
        photoObj.otsu   = thr;
    case 'grow3D'
        error('This hasn''t been implemented for the new code.  You''ll have to import it from Ziga''s old stuff');
        %% 3d region growing
        seed = [5 5 5];
        maxdist = 18.5;
        hdrMRI = file_NRRD(photoObj.mrifile,photoObj.mripath);        
        volume = mexrg3d(volume,seed,maxdist);        
        photoObj.volume = volume;        
    otherwise
end

if ~isempty(matpath), save([matpath,'segmentation-',method,'.mat'],'volume'); end

end