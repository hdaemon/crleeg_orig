% MRI data histograms 
% -------------------------------------------------------------------------
% Description:
%	MRI data histograms on spheres around given input points
% 
% Usage: 
%	photoObj = maGetHistograms( 'init', photoObj ) 
%   H = maGetHistograms( 'exec', photoObj, points)
%  	maGetHistograms( 'kill' )
%  
% Input:
%   - fcn: 'init' or 'exec' or 'kill'
%   - photoObj: the photoObj structure
% 
% Output:
%   - photoObj*: in 'init', the photoObj structure is returned
%
% Comments:
% 
% Notes:
%
% -------------------------------------------------------------------------
function [  varargout ] = GetHistograms(photoObj, fcn, varargin )

switch( fcn )
    case 'init' % constructor

        if isempty(photoObj.hcfopt), error('Error: missing "hcfopt" structure!'), end        
        if isempty(photoObj.imghdr), error('Error: missing input volume data!'), end        
                       
        r       = photoObj.hcfopt.radius;
        dr      = photoObj.hcfopt.step;
        intp    = photoObj.hcfopt.interpolation;
        binning = photoObj.hcfopt.binning;
        data    = photoObj.imghdr.data;
        
        if ( ~strcmp(intp,'cubic') && ~strcmp(intp,'linear') ...
                && ~strcmp(intp,'partvol') && ~strcmp(intp,'mexpartvol') )                           
            warning([ 'Uknown parameter for interpolation' ' "' intp '" '...
                      'Using linear interpolation instead.']), intp = 'linear';
        end                     
        
        switch intp        
            case {'nearest','linear','cubic'}, 
                disp('We''re getting here. Is this changing things?');
                [ X Y Z ] = meshgrid( photoObj.Y, photoObj.X, photoObj.Z);
                photoObj.X = X; photoObj.Y = Y; photoObj.Z = Z;
                clear X Y Z
            % case 'partvol', maPartVolInterp( 'init', X, Y, Z, data ); % init partial volume interpolation class
            case 'mexpartvol'
            otherwise error(['Error: "',intp,'" not recognized as a valid interpolation method!'])
        end
    
        % compute equidistant grid in the shape of a sphere
        % rs = -r : dr : r;
        nsmp = ceil(2*r/dr);
        if ceil(nsmp/2 - floor(nsmp/2))==0, nsmp = nsmp + 1; end
        rs = linspace(-r, r, nsmp);        
        rs2 = rs.^2;
        r2 = r^2;
        sph = zeros(length(rs)^3,3);
        i = 1;
        for x = 1 : length(rs)
            for y = 1 : length(rs)
                for z = 1 : length(rs)
                    if r2>=(rs2(x)+rs2(y)+rs2(z))
                        sph(i,:) = [rs(x),rs(y),rs(z)];
                        i = i + 1;
                    end
                end
            end
        end
        
        sph = sph(1:i-1,:); % save only points that the sphere contains
        sph = unique(sph,'rows'); % take out center point, if there are more
        xbins = linspace(min(data(:)), max(data(:)), binning); % get bin spacing        
        
        photoObj.sph = sph;
        photoObj.xbins = xbins;
        photoObj.Href = histc( data(:), xbins );
        
        % check output arguments
        if nargout > 0, varargout{1} = photoObj; end      
        
    case 'exec' % execute function 
              
        bplot = false; % dont plot by default
        
        % check input arguments              
        if nargin < 3, error('Error: Missing input points!'); end
        
        points = varargin{1};
        
        intp = photoObj.hcfopt.interpolation;
        data = photoObj.imghdr.data;
        [npoints,coord] = size(points); % get number of points
        H = zeros(npoints,length(photoObj.xbins)); % get memory for histograms
        Hpoints = zeros(size(photoObj.sph)); % get memory for point locations
        
        switch intp        
            % case 'partvol', % partial volume interpolation (maPartVolInterp)
            %    % for i = 1 : npoints
            %    for i = drange(1 : npoints)                      
            %        Hpoints(:,1) = photoObj.sph(:,1) + points(i,1);
            %        Hpoints(:,2) = photoObj.sph(:,2) + points(i,2);
            %        Hpoints(:,3) = photoObj.sph(:,3) + points(i,3); 
            %        H(i,:) = maPartVolInterp( 'exec', Hpoints, photoObj.xbins ); % get histogram 
            %        if bplot > 0, plotHistogramm( H(i,:), photoObj.xbins, i ); end
            %    end                
            case 'mexpartvol', % partial volume interpolation (mexPartVolInterp)
                 for i = 1 : npoints                
             %   for i = drange( 1 , npoints)  
                    Hpoints(:,1) = photoObj.sph(:,1) + points(i,1);
                    Hpoints(:,2) = photoObj.sph(:,2) + points(i,2);
                    Hpoints(:,3) = photoObj.sph(:,3) + points(i,3); 
                    H(i,:) = mexPartVolInterp( Hpoints, data, photoObj.sampling, photoObj.X, photoObj.Y, photoObj.Z, photoObj.xbins ); % get histogram 
                    if bplot > 0, plotHistogramm( H(i,:), photoObj.xbins, i ); end
                end                
            case {'nearest','cubic'}, % nearest, linear or cubic interpolation (interp3)
                 for i = 1 : npoints                
                %for i = drange( 1 , npoints)
                    Hpoints(:,1) = photoObj.sph(:,1) + points(i,1);
                    Hpoints(:,2) = photoObj.sph(:,2) + points(i,2);
                    Hpoints(:,3) = photoObj.sph(:,3) + points(i,3);                       
                    Hvals = interp3(photoObj.X,photoObj.Y,photoObj.Z,data,Hpoints(:,2),Hpoints(:,1),Hpoints(:,3),intp);                       
                    H(i,:) = histc( Hvals, photoObj.xbins ); % get histogram 
                    if bplot > 0, plotHistogramm( H(i,:), photoObj.xbins, i ); end
                end 
            case 'linear',
                 for i = 1 : npoints                
                %for i = drange( 1 , npoints)
                    Hpoints(:,1) = photoObj.sph(:,1) + points(i,1);
                    Hpoints(:,2) = photoObj.sph(:,2) + points(i,2);
                    Hpoints(:,3) = photoObj.sph(:,3) + points(i,3);                       
                    Hvals = trilinear([Hpoints(:,2),Hpoints(:,1),Hpoints(:,3)],photoObj.X,photoObj.Y,photoObj.Z);                       
                    H(i,:) = histc( Hvals, photoObj.xbins ); % get histogram 
                    if bplot > 0, plotHistogramm( H(i,:), photoObj.xbins, i ); end
                end                                                
            otherwise, error(['Error: "',intp,'" not recognized as a valid interpolation method.'])
        end

        % check output arguments
        if nargout > 0, varargout{1} = H; end
        
    case 'kill' % destructor function       
        % maPartVolInterp( 'kill' );
        
    otherwise
        error(['Error: class function ' ' "' fcn '" ' ' not recognized!'])
end

end

function plotHistogramm( yvals, xbins, npoint )
    
    bar( xbins, yvals );
    title(['Histogram for point #',num2str(npoint)]);
    pause
    
end

