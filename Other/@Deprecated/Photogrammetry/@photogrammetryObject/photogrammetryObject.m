classdef photogrammetryObject
  
  properties
    
    % Photogrammetry Options
    modeltr      = 'rigid';           % rigid registration
    optimization = 'powell';          % powell optimization
    costfunction = 'icp';             % icp method
    datapath     = '';
    respath      = '';
    matpath      = '';
    mripath      = '';
    mrifile      = '';                % data file to use
    labelfile    = '';                % label file to use
    eegfile      = '';                % eeg file to use
    edtfile      = '';                % eeg file to use
    reiniteeg    = true;              % reinit all eeg-related data
    bplot        = true;              % draw results by default
    skipinitreg  = false;             % dont skip initial registration
    redopreproc  = true;             % dont redo data preprocessing
    initialization = 'fiducials';     % initialize by matching fiducials
    fidfile      = 'fiducials-1.mat'; % fiducials file to use
    evaloutput   = false;             % dont export evaluation results
    evalfile     = 'real-evaluation'; % eval file to write to
    
    % Obtained from calling ReadEEGData
    posEEG
    labEEG
    posFID
    labFID
    posStart
    posFIDStart
    
    % Obtained from calling MRISegmentation
    otsu
    volume
    
    % Obtained from calling PrincipalAxes
    G1
    G2
    Np
    V
    EVCTp
    EVALp
    
    Psym
    Wsym
    MCp
    
    Pnet
    Wnet
    
    Nmri
    MCmri
    EVCTmri
    EVALmri
    
    sampling
    anglescale
    shiftscale
    X
    Y
    Z
    
    D
    DT
    T
    mxyz
    
    imghdr
    
    hcfopt
    icpopt
    cf
    
    Href
    sph
    xbins
    
    indout
    
    initTr
    optimTr
    optimTrParams
    
  end
  
  methods
    
    function obj = photogrammetryObject(varargin)      
      if length(varargin) > 1
        nvars = 1;
        while nvars < length(varargin)
          if ~ischar(varargin{nvars}),
            error('Error: field name should be a character array!');
          end
          if nvars+1 <= length(varargin),
            obj = setfield(obj,lower(varargin{nvars}),varargin{nvars+1});
          end
          nvars = nvars + 2;
        end
      end
    end
    
    function photoObj = DataPreprocessing(photoObj)
      mydisp('photoObj.DataPreprocessing >> BEGIN');
      %% Load Photogrammetry Locations and Fiducial Locations
      cd(photoObj.datapath);
      [photoObj.posEEG, photoObj.labEEG] = photogrammetryObject.ReadEEGData(photoObj.eegfile,photoObj.datapath);
      [photoObj.posFID, photoObj.labFID] = photogrammetryObject.ReadEEGData(photoObj.eegfile,photoObj.datapath,'fiducials' );
      cd Mat-Sessions
      
      %% Compute Otsu Segmentation
      mydisp('photogObj.DataPreprocessing >>  Performing Otsu segmentation...')
      photoObj = photoObj.MRISegmentation('otsu');
      
      %% Compute Principle Axes of EEG Cloud
      
      mydisp('photogObj.DataPreprocessing >>  Computing EEG points cloud principal axes...')
      %[G1, G2, photoObj.Np, photoObj.V, photoObj.EVCTp, photoObj.EVALp] = PrincipalAxes( photoObj.posEEG, 'indices', 'principal-axes' );
      [G1, G2, photoObj.Np, ~,photoObj.EVCTp,~] = PrincipalAxes( photoObj.posEEG, 'indices', 'principal-axes' );
      
      mydisp('photogObj.DataPreprocessing >>  Searching for maximal cardinality bipartite graph...')
      [photoObj.Psym, photoObj.Wsym, photoObj.MCp] = PointPairMatching( photoObj.posEEG, G1, G2, photoObj.Np ,true);
      
      photoObj.MCp = reshape(photoObj.MCp,1,3);
      photoObj.Np  = reshape(photoObj.Np,1,3);
      
      mydisp('photogObj.DataPreprocessing >>  Searching for neighbouring point pairs...')
      [photoObj.Pnet,photoObj.Wnet] = PointGrouping( photoObj.posEEG );
      
      if ~isempty(photoObj.otsu), thr = photoObj.otsu; else thr = 10; end
      %[photoObj.Nmri, photoObj.MCmri, photoObj.EVCTmri, photoObj.EVALmri] = MRISymmetryPlane( photoObj, 'simplex', thr, 32, 20 );
      [photoObj.Nmri, photoObj.MCmri, photoObj.EVCTmri, ~] = MRISymmetryPlane( photoObj, 'simplex', thr, 32, 20 );
      
      photoObj.MCmri = reshape(photoObj.MCmri,1,3); 
      photoObj.Nmri  = reshape(photoObj.Nmri,1,3);
      
      mydisp('photogObj.DataPreprocessing >>  Reading volume metadata...')
      % load header only
      hdrMRI = file_NRRD(photoObj.mrifile,photoObj.mripath);
           
      % sampling distances x-y-z
      photoObj.sampling = hdrMRI.getAspect;
      
      % apply parameter space scaling (for rotation parameters)
      % anglescale = asin((sampling*sqrt(sum(size(DT).^2))/4)^(-1));
      cubediag = 0.25*sqrt(sum((hdrMRI.sizes .* photoObj.sampling).^2));
      photoObj.anglescale = asin(inv(cubediag));
      photoObj.shiftscale = 1;
      
      % x-y-z sampling grid
      photoObj.X = photoObj.sampling(1) .* [ (-hdrMRI.sizes(1)/2+1) : hdrMRI.sizes(1)/2 ];
      photoObj.Y = photoObj.sampling(2) .* [ (-hdrMRI.sizes(2)/2+1) : hdrMRI.sizes(2)/2 ];
      photoObj.Z = photoObj.sampling(3) .* [ (-hdrMRI.sizes(3)/2+1) : hdrMRI.sizes(3)/2 ];
      
      % Save Initialization Data
      cd(photoObj.matpath)
      save InitData.mat photoObj      
            
      mydisp('photogObj.DataPreprocessing >> END');
     % if isfield(regs.data,'empty'), rmfield(regs.data,'empty'); end
    end
        
  end
  
  methods (Static=true)
    [points, varargout] = ReadEEGData(filename,datapath, varargin)
  end
end