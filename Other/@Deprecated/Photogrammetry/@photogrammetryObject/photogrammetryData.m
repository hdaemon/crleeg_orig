classdef photogrammetryData
  
  properties
  end
  
  methods
    
    function photoData = photogrammetryData(dirs,files,photoOptions)                                         
        %% Load Photogrammetry Locations and Fiducial Locations
        % THESE LOOK LIKE THEY WILL BE _EXACTLY_ THE SAME!!!!
        cd(dirs.PhotogrammetryFull);
        [photoData.posEEG, photoData.labEEG] = maReadEEGData( photoOptions.eegfile );
        [photoData.posFID, photoData.labFID] = maReadEEGData( photoOptions.eegfile, 'fiducials' );
        cd Mat-Sessions
        
        %% Compute Otsu Segmentation
        disp('  Performing Otsu segmentation...')
        ['', photoData.otsu] = maMRISegmentation('otsu',photoOptions.datafile);
        
        %% Compute Principle Axes of EEG Cloud
        
        disp('  Computing EEG points cloud principal axes...')
        [G1, G2, photoData.Np, photoData.V, photoData.EVCTp, photoData.EVALp] = maPrincipalAxes( photoData.posEEG, 'indices', 'principal-axes' );
        
        disp('  Searching for maximal cardinality bipartite graph...')
        [photoData.Psym, photoData.Wsym, photoData.MCp] = maPointPairMatching( photoData.posEEG, G1, G2, photoData.Np );
        
        photoData.MCp = reshape(photoData.MCp,1,3);
        photoData.Np  = reshape(photoData.Np,1,3);
        
        disp('  Searching for neighbouring point pairs...')
        [photoData.Pnet,photoData.Wnet] = maPointGrouping( photoData.posEEG );
        
        if isfield(data,'otsu'), thr = photoData.otsu; else thr = 10; end
        [photoData.Nmri, photoData.MCmri, photoData.EVCTmri, photoData.EVALmri] = maMRISymmetryPlane( photoOptions.datafile, 'simplex', thr, 32, 20 );
        photoData.MCmri = reshape(photoData.MCmri,1,3); photoData.Nmri = reshape(photoData.Nmri,1,3);
        
        disp('  Reading volume metadata...')
        % load header only
        CDir = cd;
        cd(mripath);
        hdrMRI = maReadNrrdHeader( photoOptions.datafile, true, false );
        cd(CDir);
        % sampling distances x-y-z
        photoData.sampling = [ sqrt(sum(hdrMRI.spacedirections(1,:).^2)),...
          sqrt(sum(hdrMRI.spacedirections(2,:).^2)),...
          sqrt(sum(hdrMRI.spacedirections(3,:).^2)) ];
        
        % apply parameter space scaling (for rotation parameters)
        % anglescale = asin((sampling*sqrt(sum(size(DT).^2))/4)^(-1));
        cubediag = 0.25*sqrt(sum((hdrMRI.sizes .* sampling).^2));
        photoData.anglescale = asin(inv(cubediag));
        photoData.shiftscale = 1;
        
        % x-y-z sampling grid
        photoData.X = photoData.sampling(1) .* [ (-hdrMRI.sizes(1)/2+1) : hdrMRI.sizes(1)/2 ];
        photoData.Y = photoData.sampling(2) .* [ (-hdrMRI.sizes(2)/2+1) : hdrMRI.sizes(2)/2 ];
        photoData.Z = photoData.sampling(3) .* [ (-hdrMRI.sizes(3)/2+1) : hdrMRI.sizes(3)/2 ];
        
        % Save Initialization Data
        save InitData.mat data
      end;
      
      % Assign output
      regs.data = data;
      
      if isfield(regs.data,'empty'), rmfield(regs.data,'empty'); end
    end;
    
  end
  
end
