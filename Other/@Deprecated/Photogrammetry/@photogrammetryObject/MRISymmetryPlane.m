% Find symmetry plane in MRI
% -------------------------------------------------------------------------
% Description:
% 	[finalABC, mc] = maMRISymmetryPlane( 'bestt1w-us.nhdr', 'simplex', 10, 32, 20 )
%
% Usage: 
%   maDisplayRegRes( 'init' )
%   maDisplayRegRes( 'draw' )
%   maDisplayRegRes( 'draw', posNew, posNear )
%   maDisplayRegRes( 'drawpoints', posNew )
%   maDisplayRegRes( 'kill' )
% 
% Input:
%   - filename: MRI dataset
%   - optimization: 'simplex' or 'steepdesc' or ...
%   - T: condition points by intensity threshold( T )
%   - xdim: new x dimension (integer, can also be a vector for multistage)
%   - q: space quantization for initial parameters estimation
% 
% Output:
%   - finalABC: parameters of mid-sagittal plane
%   - mc: center of points
%   - v*: eigenvectors of downsampled volume
%   - e*: eigenvalues of downsampled volume
%
% Comments:
% 
% Notes:
%   Implementation from Ardekani et al. (1997) - Automatic detection 
%   of the mid-sagittal plane in 3D brain images. Transactions on 
%   medical imaging, vol.16, no.6.
%
% -------------------------------------------------------------------------

function [finalABC, mc, varargout] = MRISymmetryPlane( photoObj, optimization, T, xdim, q )

mripath = photoObj.mripath;
matpath = photoObj.matpath;
filename = photoObj.mrifile;

if nargin < 5, q = 20; end
if nargin < 4, xdim = 32; end
if nargin < 3, T = 50; end
if nargin < 2, optimization = 'simplex'; end

datafile = ['maMRI-resampled-xdim',num2str(xdim(1)),'.mat'];

if exist([matpath datafile],'file')  
    disp(['Loading sampled volume "',datafile,'".'])
    load([matpath datafile],'regs');
    photoObj.D = regs.D;
    photoObj.X = regs.X;
    photoObj.Y = regs.Y;
    photoObj.Z = regs.Z;
    photoObj.D(find(isnan(photoObj.D))) = 0;
else  
    mydisp('Downsampling volume... stage 1.')
    
    hdrMRI = file_NRRD(filename,mripath); % load dataset    
    
    photoObj = maResampleImage( hdrMRI, xdim(1) ); % downsample image    
    
    regs.X = photoObj.X;
    regs.Y = photoObj.Y;
    regs.Z = photoObj.Z;
    regs.D = photoObj.D;
    save([matpath,datafile],'regs');
end

photoObj.T = T; % set intensity threshold value
%keyboard;
%% get initial plane estimate for optimization
mydisp('Finding initial estimation of plane parameters...')
[ mc, evects, eigval, photoObj ] = maGetCenterOfMassAndPrincipalAxes( photoObj, T );
[startABC, photoObj] = maGetStartingPlane( q, mc, photoObj );

%% setup & run optimization
cf = @(ABC) maSymmetryCF(ABC,photoObj); % criterion function handle
mydisp('Parameter optimization... stage 1.')
switch optimization
    case 'simplex'
        simopt = optimset( 'Display','final',...
                           'MaxFunEvals',1000,...
                           'MaxIter',200,...    
                           'TolFun',1e-3,...
                           'TolX',1e-3 ); % setup 
                       
        finalABC = fminsearch( cf, startABC, simopt ); % run  
        
    case {'bfgs','dfp','steepdesc'}
        simopt = optimset( 'LargeScale','off',...
                           'GradObj','off',...
                           'HessUpdate', optimization,...
                           'InitialHessType','identity',...
                           'Display','final',...
                           'MaxFunEvals',1000,...
                           'MaxIter',100,...    
                           'TolFun',1e-3,...
                           'TolX',1e-3 ); % setup 
                       
        finalABC = fminunc( cf, startABC, simopt ); % run
        
    otherwise
        return;
end

finalABC = finalABC ./ norm(finalABC);
if length(xdim)>1
    for i = 2 : length(xdim)
        % check if resampled volume exists
        datafile = ['maMRI-resampled-xdim',num2str(xdim(i)),'.mat'];

        if exist(datafile,'file')
            load(datafile,'photoObj');
            photoObj.D(find(isnan(photoObj.D))) = 0;
        else
            mydisp(['Downsampling volume... stage ',num2str(i),'.'])
            if ~exist('hdrMRI','var'), hdrMRI = maReadNrrdHeader( filename ); end % load dataset
            photoObj = maResampleImage( hdrMRI, xdim(i) ); % downsample image
            save([matpath,datafile],'photoObj');
        end

        [ mc, evects, eigval, photoObj ] = maGetCenterOfMassAndPrincipalAxes( photoObj, T ); % recompute center of mass    
        photoObj.T = T; % set intensity threshold value

        mydisp(['Parameter optimization... stage ',num2str(i),'.'])
        cf = @(ABC) maSymmetryCF(ABC,photoObj); % criterion function handle    
        switch optimization
            case 'simplex', finalABC = fminsearch( cf, finalABC, simopt ); % run                               
            case {'bfgs','dfp','steepdesc'}, finalABC = fminunc( cf, finalABC, simopt ); % run                             
        end        
        finalABC = finalABC ./ norm(finalABC);    
    end
end

% output other data
if nargin>2, varargout{1} = evects; end
if nargin>3, varargout{2} = eigval; end

%% compute object center
function [ mc, evects, eigval, photoObj ] = maGetCenterOfMassAndPrincipalAxes( photoObj, thr )

% get center of mass from MRI image
% M = sum(photoObj.D(:));
% mxyz = [0,0,0];
% mxyz(1) = sum(photoObj.D(:) .* photoObj.Y(:)) / M; % x-dim
% mxyz(2) = sum(photoObj.D(:) .* photoObj.X(:)) / M; % y-dim
% mxyz(3) = sum(photoObj.D(:) .* photoObj.Z(:)) / M; % z-dim
% photoObj.mxyz = mxyz;
% mc = mxyz;

bUseWeighted = false;
if bUseWeighted
    ind = find(photoObj.D>thr);
    avgX = mean(photoObj.D(ind) .* photoObj.Y(ind));
    avgY = mean(photoObj.D(ind) .* photoObj.X(ind));
    avgZ = mean(photoObj.D(ind) .* photoObj.Z(ind));
    avgW = mean(photoObj.D(ind));
    mc = [avgX avgY avgZ] ./ avgW;
    photoObj.mxyz = mc;
    % compute covariance matrix
    covmat = zeros(3,3);
    for i=1:numel(ind)
        pos = [photoObj.Y(ind(i)),photoObj.X(ind(i)),photoObj.Z(ind(i))];
        covmat = covmat + photoObj.D(ind(i)) * [pos - mc]'*[pos - mc];        
    end
    covmat = covmat ./ numel(ind);
    % get its eigen -vectors and -values
    [evects,eigval] = eig(covmat);
else
    ind = find(photoObj.D>thr);
    avgX = mean(photoObj.Y(ind));
    avgY = mean(photoObj.X(ind));
    avgZ = mean(photoObj.Z(ind));
    mc = [avgX avgY avgZ];
    photoObj.mxyz = mc;
    % compute covariance matrix
    covmat = zeros(3,3);
    for i=1:numel(ind)
        pos = [photoObj.Y(ind(i)),photoObj.X(ind(i)),photoObj.Z(ind(i))];
        covmat = covmat + [pos - mc]'*[pos - mc];        
    end
    covmat = covmat ./ numel(ind);
%    keyboard;
    % get its eigen -vectors and -values
    [evects,eigval] = eig(covmat);
end

%% compute initial plane parameters
function [ startABC, photoObj ] = maGetStartingPlane( q, mc, photoObj )

maxval = 0;
startABC = [1,0,0];

% DEBUG
% nnn = 0;
% str = zeros(1000,3);
% vals = zeros(1000,1);

% loop through a set of starting orientations
dphi = pi / (2*q);
for i = 0 : q
    phi = i*dphi; % select phi
    nj = ceil(q*sin(phi)+1e-3); % get number of theta samples
    alpha = 2*pi*rand(); % get alpha offset
    theta = linspace(alpha,alpha+pi,nj); % get theta starting positions 
    for j = 1 : nj              
        % get plane parameters
        testABC = [sin(phi)*cos(theta(j)),sin(phi)*sin(theta(j)),cos(phi)];        
        cval = maSymmetryCF( testABC, photoObj );
             
        % compute symmetry measure, find max & save its parameters       
        if cval<maxval % CF has (-) sign
            maxval = cval;
            startABC = testABC;            
        end
        
        % DEBUG
%         nnn = nnn + 1;
%         str(nnn,:) = testABC;        
%         vals(nnn) = cval;        
        
    end    
end

startABC = startABC ./ norm(startABC); % normalize output   

% DEBUG
% cmap = jet(128);
% figure, hold on
% for i = 1 : nnn
%     plot3([0,str(i,1)],[0,str(i,2)],[0,str(i,3)],'Color',cmap(floor(abs(vals(i))*128)+1,:))           
% end
% hold off

% figure
% hist(vals(find(vals~=0)))

%% criterion function for simmetry detection
function cval = maSymmetryCF( ABC, photoObj )

ABC = reshape(ABC,3,1); % normalize input vector
poff = sum(ABC(:) .* photoObj.mxyz(:)); % get plane offset
ind_DgtT = find(photoObj.D(:)>photoObj.T); % find f(i,j,k)>T
Ft = photoObj.D(ind_DgtT); % copy corresponding point values
P = [photoObj.Y(ind_DgtT),photoObj.X(ind_DgtT),photoObj.Z(ind_DgtT)]; % get (i,j,k) -> P = (xi,yj,zk) 
% Fp = (P * ABC - 1); % insert points P into plane equation
Fp = (P * ABC - poff); % insert points P into plane equation
indPgt0 = find(Fp>0); % find F(P)>0
P = P(indPgt0,:); Fp = Fp(indPgt0); Ft = Ft(indPgt0); % remove corresponding point locations and projections
Pr = P - 2 * Fp * ABC' / (norm(ABC)^2); % find reflection points

% remove points that fall out of range
ind = find(Pr(:,1)>=photoObj.X(1)); Ft = Ft(ind); Pr = Pr(ind,:);
ind = find(Pr(:,1)<=photoObj.X(end)); Ft = Ft(ind); Pr = Pr(ind,:);
ind = find(Pr(:,2)>=photoObj.Y(1)); Ft = Ft(ind); Pr = Pr(ind,:);
ind = find(Pr(:,2)<=photoObj.Y(end)); Ft = Ft(ind); Pr = Pr(ind,:);
ind = find(Pr(:,3)>=photoObj.Z(1)); Ft = Ft(ind); Pr = Pr(ind,:);
ind = find(Pr(:,3)<=photoObj.Z(end)); Ft = Ft(ind); Pr = Pr(ind,:);

% interpolation of values at points Pr
Fr = interp3(photoObj.X,photoObj.Y,photoObj.Z,photoObj.D,Pr(:,2),Pr(:,1),Pr(:,3),'linear');
ind = find(~isnan(Fr(:))); Fr = Fr(ind); Ft = Ft(ind);

% compute cross-correlation
Ft = Ft - mean(Ft);
Fr = Fr - mean(Fr);
cval = -(Ft' * Fr) / (norm(Ft) * norm(Fr));

%% image resampling
function photoObj = maResampleImage( hdrMRI, xdim )

%% get sampling grid
sdist = [ sqrt(sum(hdrMRI.spacedirections(1,:).^2)),...
          sqrt(sum(hdrMRI.spacedirections(2,:).^2)),...
          sqrt(sum(hdrMRI.spacedirections(3,:).^2)) ]; % sampling distances x-y-z

x = sdist(1)*((-hdrMRI.sizes(1)/2+1) : hdrMRI.sizes(1)/2); % x-sampling grid
y = sdist(2)*((-hdrMRI.sizes(2)/2+1) : hdrMRI.sizes(2)/2); % y-sampling grid
z = sdist(3)*((-hdrMRI.sizes(3)/2+1) : hdrMRI.sizes(3)/2); % z-sampling grid
% [ Xg,Yg,Zg ] = meshgrid( y,x,z ); % generate grid  

%% set isotropic sampling and y,z volume dimensions
sampling = sdist(1) * (hdrMRI.sizes(1) - 1) / xdim;
ydim = ceil(sdist(2) * (hdrMRI.sizes(2) - 1) / sampling);
zdim = ceil(sdist(3) * (hdrMRI.sizes(3) - 1) / sampling);

%% setup gaussian filter & filter the image
Dg = hdrMRI.data;
clear hdrMRI
r = 2; % sampling * r == FWHM
sigma = sqrt((sampling^2-sdist(1)^2) * r^2 / log(256)); % std
xl = 1.65 * sigma / sdist(1); % estimate kernel size
ks = 3; if xl > 3, ks = 5; end % set kernel size
disp(['  Gaussian filtering... kernel: [',num2str([ks ks ks]),'] sigma: ',num2str(sigma)])
Dg = smooth3(Dg,'gaussian',[ks ks ks],sigma); % smooth volume data

% %% resample the image
% Xg = x; Yg = y; Zg = z;
% disp(['  Volume resampling... x:',num2str(xdim),' y:',num2str(ydim),' z:',num2str(zdim)])
% x = sampling*((-xdim/2+1) : xdim/2); % x-sampling grid
% y = sampling*((-ydim/2+1) : ydim/2); % y-sampling grid
% z = sampling*((-zdim/2+1) : zdim/2); % z-sampling grid
% % [ X,Y,Z ] = meshgrid( x,y,z ); % generate grid  
% D = zeros(numel(x),numel(y),numel(z));
% for i = 1 : numel(z)
%     for j = 1 : numel(y)
%         for k = 1 : numel(x)
%             D(sub2ind(size(D),k,j,i)) = getTrilinearInterpVal( [x(k) y(j) z(i)], Dg, [sampling sampling sampling], Xg, Yg, Zg );
%         end
%     end
% end
% D(find(isnan(D))) = 0;
% clear Xg Yg Zg Dg
% [ X,Y,Z ] = meshgrid( y,x,z ); % generate grid  

%% resample the image
[ Xg,Yg,Zg ] = meshgrid( y,x,z ); % generate grid  
disp(['  Volume resampling... x:',num2str(xdim),' y:',num2str(ydim),' z:',num2str(zdim)])
x = sampling*((-xdim/2+1) : xdim/2); % x-sampling grid
y = sampling*((-ydim/2+1) : ydim/2); % y-sampling grid
z = sampling*((-zdim/2+1) : zdim/2); % z-sampling grid
[ X,Y,Z ] = meshgrid( y,x,z ); % generate grid  
D = interp3(Xg,Yg,Zg,Dg,X,Y,Z,'linear'); % interpolate
D(find(isnan(D))) = 0;
clear Xg Yg Zg Dg

%% fill output structure 
photoObj.D = D;
photoObj.X = X;
photoObj.Y = Y;
photoObj.Z = Z;





