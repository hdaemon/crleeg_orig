% Histogram criterion function 
% -------------------------------------------------------------------------
% Description: 
%
% Usage: 
%   cval = maICP( 'exec', points )
%  
% Input:
%   - fcn: 'init' or 'exec' or 'kill'
%   - regs: regs structure
%   - points*: 3-D point positions
% 
% Output:
%   - val*: in 'exec', the cost function value is returned
% 
% Comments:
% 
% Notes:
% 
% -------------------------------------------------------------------------
function [ photoObj ] = ICP(photoObj, fcn, varargin )

switch fcn
    case 'init' % constructor                                
        if isempty(photoObj.icpopt), error('Error: missing "icpopt" structure!'), end        
        if isempty(photoObj.DT), error('Error: missing distance map (DT)!'), end
        
                    
    case 'exec' % execute function 
        if nargin < 3, error('Error: missing input points!'); end        

        points = varargin{1};
        npoints = size(points,1);
        
        % loop through all points
        residuals = zeros(1,npoints);
        for i = 1 : npoints
            residuals(i) = getTrilinearInterpVal( points(i,:), regs.data.DT, regs.data.sampling, regs.data.X, regs.data.Y, regs.data.Z , true);
        end            
        
        switch regs.icpopt.mode
            case 'sum', cval = sum(residuals);
            case 'sad', cval = sum(abs(residuals));
            case 'ssd', cval = sum(residuals.^2);
            case 'rms', cval = sqrt(sum(residuals.^2));
            otherwise, error(['Error: "',regs.icpopt.mode,'" mode not recognized!'])
        end

        if nargout>0, varargout{1} = cval; end
        
    case 'kill' % destructor function 
        
        
    otherwise
        error(['Error: class function "',fcn,'" not recognized!'])
end

    

