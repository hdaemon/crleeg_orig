% Histogram criterion function 
% -------------------------------------------------------------------------
% Description: 
%
% Usage: 
%   regs = maHCF( 'init', regs ) 
%   KLsum = maHCF( 'exec', regs, points )
%  
% Input:
%   - fcn: 'init' or 'exec' or 'kill'
%   - regs: regs structure
%   - points: 3-D point coordinates
% 
% Output:
%   - regs*: in 'init', the regs structure is returned
%   - KLsum*: in 'exec', the cost function value is returned
% 
% Comments:
% 
% Notes:
% 
% -------------------------------------------------------------------------
function [ varargout ] = maHCF( fcn, regs, varargin )

switch fcn
    case 'init' % constructor      
        
        % init histograms
        regs = maGetHistograms( 'init', regs );
        
        % get mode of operation
        mode = regs.hcfopt.mode;        
        
        if mode.global || mode.bkgcon,
            % get global histogramm    
            if isfield(regs.data,'otsu')                
                indout = find(regs.xbins<=regs.data.otsu);
                valout = sum(regs.Href(indout));
                valin = sum(regs.Href(find(regs.xbins>regs.data.otsu)));
                quot = valin / valout;
                regs.Href(indout) = quot * regs.Href(indout);
                regs.indout = indout;
                % a = regs.data.otsu - regs.xbins(indout);
                % b = regs.xbins(indout+1) - regs.data.otsu;                         
            else
                regs.Href(1) = sum(regs.Href(2:end)); % normalize background                
            end               
            regs.Href = regs.Href + 1; % global image histogram                                                
            regs.Href = regs.Href ./ sum(regs.Href); % normalize histogram sums to 1                                    
            regs.Href = reshape(regs.Href,1,numel(regs.xbins));
        end
        
        if mode.localsym,
            % get point pairs
            if ~isfield(regs.data,'Psym'), error('Error: missing "Psym" data!'), end
            if ~isfield(regs.data,'Wsym'), error('Error: missing "Wsym" data!'), end                                  
            % stretch & normalize weights
            if isempty(regs.data.Wsym), regs.data.Wsym = ones(size(regs.data.Psym,1),1); end
            % regs.Wsym = exp(-regs.data.Wsym);             
            regs.Wsym = -regs.data.Wsym + max(regs.data.Wsym);            
            regs.Wsym = regs.Wsym / sum(regs.Wsym);                
        end
        
        if mode.localnet,
            % get point pairs
            if ~isfield(regs.data,'Psym'), error('Error: missing "Pnet" data!'), end
            if ~isfield(regs.data,'Wsym'), error('Error: missing "Wnet" data!'), end                                  
            % stretch & normalize weights
            if isempty(regs.data.Wnet), regs.data.Wnet = ones(size(regs.data.Pnet,1),1); end 
            % regs.Wnet = exp(-regs.data.Wnet); 
            regs.Wnet = -regs.data.Wnet + max(regs.data.Wnet);            
            regs.Wnet = regs.data.Wnet / sum(regs.data.Wnet); 
        end
                               
        % check output arguments
        if nargout>0, varargout{1} = regs; end  
        
    case 'exec' % execute function 
        if nargin < 3, error('Error: missing input points!'); end        
    
        % read input points
        points = varargin{1};
        
        % get mode of operation
        mode = regs.hcfopt.mode;         
        
        % get type of KL measure
        kltype = regs.hcfopt.divtype;         
        
        % compute & normalize histograms
        H = maGetHistograms( 'exec', regs, points );
        H = H + 1e-2 * ones(size(H));
        for j = 1 : size(points,1)
            H(j,:) = H(j,:) ./ sum(H(j,:)); % normalize sum to 1 (!!!)     
        end         
                
        % init Kullback-Leibner distance        
        KLsum = 0;
        
        % distance to the global reference histogramm
        if mode.global,
            for j = 1 : size(points,1)
                KLsum = KLsum + kldiv(regs.xbins,H(j,:),regs.Href,kltype);
            end   
            KLsum = KLsum / size(points,1); % normalization
        end
        
        % sum of KLdivs between single L-R histogramms
        if mode.localsym,
            for j = 1 : size(regs.data.Psym,1)
                Hleft = H(regs.data.Psym(j,1),:);
                Hright = H(regs.data.Psym(j,2),:);
                KLsum = KLsum + regs.Wsym(j) * kldiv(regs.xbins,Hleft,Hright,kltype);
            end
        end
        
        % sum of KLdivs between neighbouring histogramms        
        if mode.localnet,
            for j = 1 : size(regs.data.Pnet,1)
                Hleft = H(regs.data.Pnet(j,1),:);
                Hright = H(regs.data.Pnet(j,2),:);
                KLsum = KLsum + regs.Wnet(j) * kldiv(regs.xbins,Hleft,Hright,kltype);
            end
        end        

        % add background constraint
        if mode.bkgcon,
%             if isfield(regs,'indout'),
%                 partsum = sum(regs.Href(regs.indout));
%                 for j = 1 : size(points,1)
%                     KLsum = KLsum + kldiv(regs.xbins(regs.indout),...
%                                             H(j,regs.indout)/sum(H(j,regs.indout)),...
%                                             regs.Href(regs.indout)/partsum,...
%                                             kltype);
%                 end   
%                 KLsum = KLsum / size(points,1); % normalization                               
%             else
                lambda = log(2)/0.1; % penalty on 10% difference is 1/3 of the measure
                KLsum = KLsum + (sum(exp(lambda * abs(H(:,1)-regs.Href(1))))-size(H,1))/size(H,1);
%             end
        end                
               
        if nargout>0, varargout{1} = KLsum; end
        
    case 'kill' % destructor function         
        
    otherwise
        error(['Error: class function "',fcn,'" not recognized!'])
end

    

