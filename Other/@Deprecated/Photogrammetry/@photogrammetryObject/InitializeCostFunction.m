% Initialize cost function
% -------------------------------------------------------------------------
% Description:
% 	Initialize cost function for iterative registration
%
% Usage:
%	regs = maInitializeCostFunction( regs );
%
% Input:
%   - regs: structure with initialized settings
%
% Output:
%   - regs: structure with cost function handler
%
% Comments:
%
% Notes:
%
% -------------------------------------------------------------------------

function photoObj = InitializeCostFunction( photoObj )
  mydisp('photogObj.InitializeCostFunction >> BEGIN');
  
  matpath = photoObj.matpath;
  mripath = photoObj.mripath;
  
  %% init similarity measure
  switch photoObj.costfunction
    case 'icp'
      mydisp('photogObj.InitializeCostFunction >> Initializing ICP Cost Function');
      
      mydisp('photogObj.InitializeCostFunction >> Loading Data From DistanceTransform.mat')
      load('DistanceTransform.mat','DT');
      photoObj.DT = DT;
      clear DT;
      
      % mode = 'sum';
      % mode = 'sad';
      % mode = 'ssd';
      mode = 'rms';
      if isempty(photoObj.icpopt)
        photoObj.icpopt = GetICPOptions( 'mode', mode );
      end
      
      % initialize hcf
      photoObj = photoObj.ICP( 'init');
      
      % set function handle of criterion function
      photoObj.cf = @(points) photoObj.ICP('exec', points);
      
    case 'hcf'
      
      mydisp('photogObj.InitializeCostFunction >> Initializing HCF Cost Function');
      mode = 'global-localnet';
      
      photoObj.imghdr = file_NRRD(photoObj.mrifile,photoObj.mripath);
      
      if isempty(photoObj.hcfopt)
        photoObj.hcfopt = GetHCFOptions( 'mode', mode,...
          'radius', 10,...
          'step', min(photoObj.sampling),...
          'interpolation', 'mexpartvol',...
          'binning', 32 );
      end
      
      % initialize hcf
      photoObj = photoObj.HCF( 'init');
      
      % set function handle of criterion function
      photoObj.cf = @(points) photoObj.HCF('exec', points);
      
    otherwise
      error('ERROR: cost function not recognized!')
  end
  
  mydisp('photoObj.INitializeCostFunction >> END');
end

