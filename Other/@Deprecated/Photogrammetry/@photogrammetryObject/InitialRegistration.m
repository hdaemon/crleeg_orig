% Initial closed-form rigid registration
% -------------------------------------------------------------------------
% Description:
% 	Compute the initial closed-form rigid transformation
% 
% Usage: 
%	photoObj = maInitialRegistration( photoObj ); 
%       
% Input:
%   - photoObj: structure with initialized settings
%   
% Output:
%   - photoObj: structure with initial transformation
%
% Comments:
% 
% Notes:
% 
% -------------------------------------------------------------------------

function  photoObj = InitialRegistration( photoObj )
mydisp('photogObj.InitialRegistration >> BEGIN');

matpath = photoObj.matpath;

bplot = false;

if exist(photoObj.fidfile,'file') && strcmp(photoObj.initialization,'fiducials')
  mydisp('photogObj.InitialRegistration >> Using fiducials file');
    load(photoObj.fidfile,'coords','labels')
    
    pointset1 = zeros(size(coords));
    pointset2 = zeros(size(coords));    
    posEEG = photoObj.posEEG;
    posFID = photoObj.posFID;
    
    % find Nasion
    ind1 = find(strcmp(photoObj.labFID,'FidNz'));
    ind2 = find(strcmp(labels,'Nas'));    
    pointset1(1,:) = photoObj.posFID(ind1,:);
    pointset2(1,:) = coords(ind2,:);    
    
    % find Nasion
    ind1 = find(strcmp(photoObj.labFID,'FidT10'));
    ind2 = find(strcmp(labels,'RPA'));    
    pointset1(2,:) = photoObj.posFID(ind1,:);
    pointset2(2,:) = coords(ind2,:);  
    
    % find Nasion
    ind1 = find(strcmp(photoObj.labFID,'FidT9'));
    ind2 = find(strcmp(labels,'LPA'));    
    pointset1(3,:) = photoObj.posFID(ind1,:);
    pointset2(3,:) = coords(ind2,:);  
    
    % find Nasion
    ind1 = find(strcmp(photoObj.labFID,'Cz'));
    ind2 = find(strcmp(labels,'Cz'));    
    pointset1(4,:) = photoObj.posFID(ind1,:);
    pointset2(4,:) = coords(ind2,:);      
 
    pointset1 = pointset1 * 10;       
    pointset2 = [photoObj.X(pointset2(:,1))', photoObj.Y(pointset2(:,2))', photoObj.Z(pointset2(:,3))'];

    T = regsvd(pointset1, pointset2);
    photoObj.initTr = T;
    photoObj.posStart    =  getTransformedPoints( posEEG, T, 10 );   
    photoObj.posFIDStart =  getTransformedPoints( posFID, T, 10);
        
    bplot = true;
    if bplot, drawSurfAndAxes(photoObj, T, photoObj.posStart, 'fiducials init'); end                
%  keyboard;  
%     posStart =  getTransformedPoints( photoObj.posEEG, T, 10 );
%     maDisplayRegRes( 'draw', posStart );
   return; 
end

mydisp('photoObj.InitialRegistration >> END');
end

% 
% warning('WHY THE FUCK ARE WE GETTING HERE?  BEYOND HERE LIE DRAGONS');
% keyboard;
%     Np = photoObj.Np;
%     Nmri = photoObj.Nmri;
%     MCmri = photoObj.MCmri;
%     MCp = photoObj.MCp;    
%     % backup scales
%     shiftscale = photoObj.shiftscale;
%     anglescale = photoObj.anglescale;   
%     % reset to 1
%     photoObj.shiftscale = 1;
%     photoObj.anglescale = 1;
%     
%     s = 10;    
%            
%     photoObj.posEEG = photoObj.posEEG - repmat(MCp(:)',size(photoObj.posEEG,1),1);
%     posEEG = photoObj.posEEG;
%     
% %     [T1,s] = getRigidTransformMatrix( [0 0 0 0 0 pi], photoObj );
% %     posEEGFlip =  getTransformedPoints( posEEGOrig, T1, 1 );    
% 
%     [v1,v2] = getProjPrincipalVectors(Nmri, photoObj.EVCTmri);    
% %     [v1,v2] = getProjPrincipalVectors(Nmri, eye(3));        
%     [e1,e2] = getProjPrincipalVectors(Np, photoObj.EVCTp);
%     
%     [center,radius,residuals] = spherefit(posEEG(:,1),posEEG(:,2),posEEG(:,3));        
%     center(1:2) = 0;
%     MC = MCmri(:) + 10 * (MCp(:) - center(:));
%     
%     % select starting position
%     r = getVectRot(Np,Nmri);
%     % T = [getVectRot2mat(r),(MCmri - MCp)'; 0 0 0 1];     
%     startTr = [10 MC' maGetEulerAngles(r)];    
%     [T1,s] = getRigidTransformMatrix( startTr, photoObj );
% 
%     e1 = T1(1:3,1:3)*e1;
%     e2 = T1(1:3,1:3)*e2;        
% 
%     cfval = zeros(8,1);
%     
%     r = [Nmri(:)' acos((e1'*v1)/(norm(e1)*norm(v1)))];
%     [T2, s] = getRigidTransformMatrix( [0 0 0 maGetEulerAngles(r)], photoObj );
%     posStart =  getTransformedPoints( posEEG, T1*T2, s );
%     cfval(1) = photoObj.cf(posStart); tr{1} = T1*T2;
%     if bplot, drawSurfAndAxes(photoObj, T1*T2, posStart, 'Np, e1 to v1'), end
%     
%     r(end) = r(end) + pi;
%     [T2, s] = getRigidTransformMatrix( [0 0 0 maGetEulerAngles(r)], photoObj );
%     posStart =  getTransformedPoints( posEEG, T1*T2, s );    
%     cfval(2) = photoObj.cf(posStart); tr{2} = T1*T2;
%     if bplot, drawSurfAndAxes(photoObj, T1*T2, posStart, 'Np, e1 to v1 + pi'), end        
%     
%     r = [Nmri(:)' acos((e2'*v1)/(norm(e2)*norm(v1)))];
%     [T2, s] = getRigidTransformMatrix( [0 0 0 maGetEulerAngles(r)], photoObj );
%     posStart =  getTransformedPoints( posEEG, T1*T2, s );    
%     cfval(3) = photoObj.cf(posStart); tr{3} = T1*T2;    
%     if bplot, drawSurfAndAxes(photoObj, T1*T2, posStart, 'Np, e2 to v1'), end
%     
%     r(end) = r(end) + pi;
%     [T2, s] = getRigidTransformMatrix( [0 0 0 maGetEulerAngles(r)], photoObj );
%     posStart =  getTransformedPoints( posEEG, T1*T2, s );    
%     cfval(4) = photoObj.cf(posStart); tr{4} = T1*T2;    
%     if bplot, drawSurfAndAxes(photoObj, T1*T2, posStart, 'Np, e2 to v1 + pi'), end        
%             
%     % select starting position
%     r = getVectRot(-Np,Nmri);
%     % T = [getVectRot2mat(r),(MCmri - MCp)'; 0 0 0 1];     
%     startTr = [10 MC' maGetEulerAngles(r)];    
%     [T1,s] = getRigidTransformMatrix( startTr, photoObj );
% 
%     e1 = T1(1:3,1:3)*e1;
%     e2 = T1(1:3,1:3)*e2;        
% 
%     r = [Nmri(:)' acos((e1'*v1)/(norm(e1)*norm(v1)))];
%     [T2, s] = getRigidTransformMatrix( [0 0 0 maGetEulerAngles(r)], photoObj );
%     posStart =  getTransformedPoints( posEEG, T1*T2, s );    
%     cfval(5) = photoObj.cf(posStart); tr{5} = T1*T2;    
%     if bplot, drawSurfAndAxes(photoObj, T1*T2, posStart, '-Np, e1 to v1'), end
%     
%     r(end) = r(end) + pi;
%     [T2, s] = getRigidTransformMatrix( [0 0 0 maGetEulerAngles(r)], photoObj );
%     posStart =  getTransformedPoints( posEEG, T1*T2, s );    
%     cfval(6) = photoObj.cf(posStart); tr{6} = T1*T2;    
%     if bplot, drawSurfAndAxes(photoObj, T1*T2, posStart, '-Np, e1 to v1 + pi'), end        
%     
%     r = [Nmri(:)' acos((e2'*v1)/(norm(e2)*norm(v1)))];
%     [T2, s] = getRigidTransformMatrix( [0 0 0 maGetEulerAngles(r)], photoObj );
%     posStart =  getTransformedPoints( posEEG, T1*T2, s );
%     cfval(7) = photoObj.cf(posStart); tr{7} = T1*T2;    
%     if bplot, drawSurfAndAxes(photoObj, T1*T2, posStart, '-Np, e2 to v1'), end
%         
%     r(end) = r(end) + pi;
%     [T2, s] = getRigidTransformMatrix( [0 0 0 maGetEulerAngles(r)], photoObj );
%     posStart =  getTransformedPoints( posEEG, T1*T2, s );
%     cfval(8) = photoObj.cf(posStart); tr{8} = T1*T2;    
%     if bplot, drawSurfAndAxes(photoObj, T1*T2, posStart, '-Np, e2 to v1'), end
%         
%     [m,i] = sort(cfval);
%     photoObj.initTr = tr{i(1)};
% keyboard;
%     if strcmp(photoObj.costfunction,'icp')
%         switch( curcase )
%             case {'case000','case001','case004','case014','case017'}, photoObj.initTr = tr{i(2)};       
%             case {'case011'}, photoObj.initTr = tr{i(2)}; % tr{i(5)}    
%             case {'case002'}, photoObj.initTr = tr{i(3)};   
%             case {'case009'}, photoObj.initTr = tr{i(4)};                 
%             otherwise, photoObj.initTr = tr{i(1)}; 
%         end    
%     elseif strcmp(photoObj.costfunction,'hcf')
%         switch( curcase )
%             case {'case001','case004','case006','case008','case015','case017'}, photoObj.initTr = tr{i(2)};       
%             case {'case002','case014'}, photoObj.initTr = tr{i(3)};
%             case {'case009'}, photoObj.initTr = tr{i(8)};                             
%             case {}, photoObj.initTr = tr{i(3)};       
%             otherwise, photoObj.initTr = tr{i(1)}; 
%         end
%     else
%         error('Cost function not recognized!!!')
%     end
%     
%     posStart =  getTransformedPoints( posEEG, photoObj.initTr, s );
% %     bplot = true
%     if bplot, drawSurfAndAxes(photoObj, photoObj.initTr, posStart, 'initial registration'), end
%     keyboard;
%   
%     
%     
%     % restore scales
%     photoObj.shiftscale = shiftscale;
%     photoObj.anglescale = anglescale;           
% 
% end
    
    
function [ sig ] = getParamSweep( pset, pvals, points, photoObj)    
                
sig = ones(1,numel(pvals));
for i = 1 : numel(pvals)
    switch pset
        case 'transx', [T, s] = getRigidTransformMatrix( [pvals(i) 0 0 0 0 0], photoObj );
        case 'transy', [T, s] = getRigidTransformMatrix( [0 pvals(i) 0 0 0 0], photoObj );
        case 'transz', [T, s] = getRigidTransformMatrix( [0 0 pvals(i) 0 0 0], photoObj );
        case 'rotx', [T, s] = getRigidTransformMatrix( [0 0 0 pvals(i) 0 0], photoObj );
        case 'roty', [T, s] = getRigidTransformMatrix( [0 0 0 0 pvals(i) 0], photoObj );
        case 'rotz', [T, s] = getRigidTransformMatrix( [0 0 0 0 0 pvals(i)], photoObj );            
        otherwise, error('Error: "',pset,'" parameter for sweep unrecognized!')
    end
    
    sig(i) = photoObj.cf(getTransformedPoints( points, T, s ));
end

end
    
    
function [v1,v2] = getProjPrincipalVectors(N, EVCT)

N = N(:);
[v,i] = max(abs(N'*EVCT));

switch i
    case 1, v1 = EVCT(:,2); v2 = EVCT(:,3);
    case 2, v1 = EVCT(:,1); v2 = EVCT(:,3);
    case 3, v1 = EVCT(:,1); v2 = EVCT(:,2);
    otherwise, error('Error: not a valid coloumn!')
end
%%
v1 = v1 - (N' * v1) * N; v1 = v1 / norm(v1,2);
v2 = v2 - (N' * v2) * N; v2 = v2 / norm(v2,2);

end


function [varargout] = drawSurfAndAxes(photoObj, T, posStart, txt, Np)
    
    if nargin<5, Np = photoObj.Np; end

    if isempty(photoObj.imghdr)
        photoObj.imghdr = file_NRRD(photoObj.mrifile,photoObj.mripath); %maReadNrrdHeader( photoObj.mrifile, true, false); 
    end
    
    % display initial pose selection            
    cfval = photoObj.cf(posStart);
    DisplayRegRes(photoObj, 'draw', posStart );  
    title([txt,', val = ',num2str(cfval)]);

    Nmri = photoObj.Nmri;

    [v1,v2] = getProjPrincipalVectors(Nmri, photoObj.EVCTmri);
    [e1,e2] = getProjPrincipalVectors(Np, photoObj.EVCTp);    
    hold on
    scale = 50;       
    MC = photoObj.imghdr.sizes(:)./4;
    
    v1 = v1 * scale + MC;
    v2 = v2 * scale + MC;    
    plot3([MC(1),v1(1)],[MC(2),v1(2)],[MC(3),v1(3)],'b','LineWidth',2);
    plot3([MC(1),v2(1)],[MC(2),v2(2)],[MC(3),v2(3)],'b','LineWidth',2);            
    
    mNp = T(1:3,1:3)*Np(:) * scale + MC;
    plot3([MC(1),mNp(1)],[MC(2),mNp(2)],[MC(3),mNp(3)],'m','LineWidth',2);    
    
    mNmri = Nmri(:) * scale + MC;
    plot3([MC(1),mNmri(1)],[MC(2),mNmri(2)],[MC(3),mNmri(3)],'k','LineWidth',2);        
        
    e1 = T(1:3,1:3)*e1;
    e2 = T(1:3,1:3)*e2;
    
    e1 = e1 * scale + MC;
    e2 = e2 * scale + MC;    
    plot3([MC(1),e1(1)],[MC(2),e1(2)],[MC(3),e1(3)],'r','LineWidth',2);
    plot3([MC(1),e2(1)],[MC(2),e2(2)],[MC(3),e2(3)],'r','LineWidth',2);                
    hold off

    
    if nargin>0, varargout{1} = cfval; end

end

    
function map = maGetSimilarityMap( photoObj, decimation )
  
X = photoObj.X(1:decimation:end);
Y = photoObj.Y(1:decimation:end);
Z = photoObj.Z(1:decimation:end);

% MEMORY-INTENSE VERSION
[X,Y,Z] = meshgrid(Y,X,Z);
map = zeros(size(X)); % initialize dest image (similarity map)
for i = drange(1 : numel(X))
    map(i) = photoObj.cf([X(i),Y(i),Z(i)]); % get cf value    
end


end

%% get nearest 3 vertices to the given reference vertexes    
function E = getNearest3Vertices( p1, inx1, p2, inx2 )

    ident = ones(length(inx2),1);
    Et = zeros(length(inx1),3);
    for i = 1 : length(inx1)
        l1dist = sum(abs(p2(inx2,:) - ident * p1(i,:)),2);
        maxval = max(l1dist);
        % get first minimum
        [val,mininx] = min(l1dist);
        Et(i,1) = mininx;
        l1dist(mininx) = maxval;
        % get second minimum        
        [val,mininx] = min(l1dist);
        Et(i,2) = mininx;
        l1dist(mininx) = maxval;
        % get third minimum        
        [val,mininx] = min(l1dist);
        Et(i,3) = mininx;
%         l1dist(mininx) = maxval;
    end
    
    % construct edge matrix with point indices
    E = [inx1,inx2(Et(:,1));...
         inx1,inx2(Et(:,2));...
         inx1,inx2(Et(:,3))];  
         
     
end  
     
function [points] = getSurfacePointsFromMap(map, X, Y, Z, perc)     

[img,ind] = sort(map(:));
myind = ceil(numel(ind)*perc);
% segmap = zeros(size(map));
% segmap(ind(1:myind)) = 1;

points = zeros(myind,3);
for i = 1 : myind
    [x,y,z] = ind2sub(size(map),ind(i));
    points(i,:) = [X(x), Y(y), Z(z)];    
end    

end
     
function [P, W] = getPointCorrespondences( points, N )    

Sprod = points * N; % compute dot product
G1 = find(Sprod<0);
G2 = find(Sprod>=0); 

% get indexes of non-zero elements
inx1 = find(G1>0);
p1 = points(G1(inx1),:) - (points(G1(inx1),:) * N) * N';       

inx2 = find(G2>0);     
p2 = points(G2(inx2),:) - (points(G2(inx2),:) * N ) * N'; 

    % get edge matrix
    E12 = getNearest3Vertices( p1, inx1, p2, inx2 );
    E21 = getNearest3Vertices( p2, inx2, p1, inx1 );    
    E = [E12; E21(:,2), E21(:,1)];   
    
    % weight matrix as Euclidean distances
    E1 = p1(E(:,1),:);
    E2 = p2(E(:,2),:);    
    W = sqrt(sum((E1-E2).^2,2));        
    [W,IW] = sort(reshape(W,prod(size(W)),1));       
    
%     % plot the selected edges
%     if bplot
%         for i = 1 : length(E(:,1))
%             plot3([E1(i,1);E2(i,1)],[E1(i,2);E2(i,2)],[E1(i,3);E2(i,3)],'-g')
%         end
%     end 
    
    % get connection matrix
    C = zeros(size(E));
    for i = 1 : length(W)              
        e1 = find(C(:,1)==E(IW(i),1));
        e2 = find(C(:,2)==E(IW(i),2));
        
        if isempty(e1) && isempty(e2)
            C(i,1) = E(IW(i),1);
            C(i,2) = E(IW(i),2);
        end        
    end
    
    W = W(find(C(:,1)>0));
    C = C(find(C(:,1)>0),:);
    
%     if bplot
%         % plot the selected edges
%         E1 = p1(C(:,1),:);
%         E2 = p2(C(:,2),:);    
% 
%         for i = 1 : length(C(:,1))
%             plot3([E1(i,1);E2(i,1)],[E1(i,2);E2(i,2)],[E1(i,3);E2(i,3)],'-m','LineWidth',2)
%         end    
%     end

    % get indexes of original point array
    P = [G1(C(:,1)),G2(C(:,2))];    
        
        
end     