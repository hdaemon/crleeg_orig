%% LOAD EEG FILES

% Set variables, defaults
output_filename_EDF = 'epoched_18.edf';
number_of_EDFs = length (dir('*.ev2'));
GrandTotalEvents = 0;

for j=1:number_of_EDFs,
    filename = sprintf('%i.edf',j);
    pathfilename = filename;
    EEG = pop_biosig(pathfilename,'importevent','off','blockepoch','off');
    EEG = eeg_checkset( EEG );
    EEGstruct.EEG{j} = EEG;
end

%% LOAD SINGLE EEG, OPEN SPECIFIC EVENT FILES, BUFFER THEM
epochIter=0;
for j=1:number_of_EDFs,
    EEG = EEGstruct.EEG{j};
    N=6; % There's 6 headers in .ev2 files
    formatSpec = '%s';
    
       
    EventFileName = num2str(j);
    Extension = '.ev2';
    FullEventFileName = [EventFileName Extension];
    fileID = fopen(FullEventFileName,'r');
    C_text = textscan(fileID,formatSpec,N);
    C_data0 = textscan(fileID,'%d %f %d %d %d %f','CollectOutput',1);
    EventType = C_data0{2}';
    EventTime = C_data0{4}';
    TotalEvents = size(EventType,2);
    fclose(fileID);
    
    Event1 = [];
    for i=1:TotalEvents,
        if EventType(i) == 200001,
            Event1 = [Event1 EventTime(i)]; % ugly code
        end
    end
        
    windowsize=512;
    EventBuf = zeros(size(Event1,2),EEG.nbchan,windowsize+1+windowsize);
    for i=1:size(Event1,2)
      epochIter=epochIter+1;
      
        mark = round (Event1(i)*1025);
        start = mark-windowsize;
        stop = mark+windowsize;
        EventBuf(i,:,:) = EEG.data(:,start:stop);
        
        epochCell{epochIter} = EEG.data(:,start:stop);
    end
    
    % create average per file
    m = mean(EventBuf);
    p = squeeze(m);
    BigBuf(j,:,:) = p;
    BigCount(j)=size(Event1,2);
    GrandTotalEvents = GrandTotalEvents + size(Event1,2);
    
%     EEG.data = p;
%     EEG.pnts = size(p,2);
%     EEG = eeg_checkset(EEG); 
%     output_pathfilename_EDF = [pathname output_filename_EDF];
%     pop_writeeeg(EEGfinal,output_pathfilename_EDF, 'TYPE','EDF');
end

%% CREATE WEIGHTED GRAND AVERAGE

% Pad epochs with two time instances of zeros
for i=1:numel(epochCell)
  epochCell{i}=[epochCell{i} zeros(size(epochCell{i},1),2)];
end
ConcatenatedEpochs=cat(2,epochCell{:});

EEG.data = ConcatenatedEpochs;
EEG.pnts = size(ConcatenatedEpochs,2);
EEG = eeg_checkset(EEG);
output_pathfilename_EDF = output_filename_EDF;
pop_writeeeg(EEG,output_pathfilename_EDF, 'TYPE','EDF');