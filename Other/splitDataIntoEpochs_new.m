function cellOut = splitDataIntoEpochs(datain, varargin)
% function cellOut = splitDataIntoEpochs(datain, epochlength)
%
% Split a data matrix into epochs
%
% Optional Arguments:
%   'epochlength' : Split the data into equal sized epochs.  Will error
%                     if the data does not split evenly.
%
% THIS FUNCTION IS CURRENTLY HIGHLY INCOMPLETE!!!
%
% Written By: Damon Hyde
% Last Edited: Nov 16, 2015
% Part of the cnlEEG Project
%

p = inputParser;
p.addParamValue('epochLength',[]);

parse(p,varargin{:});

epochLength = p.Results.epochLength;

nElec = size(datain,2);
nSamp = size(datain,1);

%% Split evenly into Epochs
if mod(nSamp,epochLength)~=0
  error(['Data matrix cannot be evenly split into epochs of length ' num2str(epochLength)]);
end

datain = reshape(datain,epochLength,nSamp/epochLength,nElec);

cellOut = cell(nSamp/epochLength,1);
for i = 1:size(datain,2);
  cellOut{i} = squeeze(datain(:,i,:));
end;

end
      