function [Electrodes] = get_ElectrodeList(dirs,files,Electrodes,modelOptions)
%
% function [Electrodes] = get_ElectrodeList(dirs,files,Electrodes)
%
% Given a set of Electrodes, reads the bad electrodes file and eliminates
% bad electrodes.  Also eliminates ground electrode from usable electrode
% list.
%
% Inputs: dirs       : EEG_dirs structure
%         files      : EEG_files structure
%         Electrodes : EEG_Electrodes structure
%
% Outputs: Electrodes : EEG_Electrodes structure with updated list of bad
%                         electrodes and usable electrodes.
%
% Calls: read_BadElectrodes
%
% Called By: EEG_InversionPrep
%
% Last Modified: Damon Hyde, 04/26/2012

mydisp('Hold in get_ElectrodeList.m');
mydisp('THIS FUNCTIONALITY HAS BEEN DEPRECATED AND ROLLED INTO read_EEGData.m');
keyboard;

Electrodes.useElec = 1:128;
Electrodes.badElec = read_BadElectrodes(dirs,files);
Electrodes.useElec = setdiff(Electrodes.useElec,Electrodes.badElec);

if modelOptions.gndElectrode ~= 0
 Electrodes.useElec = setdiff(Electrodes.useElec,modelOptions.gndElectrode);
end

end