function plotEEG(EEG,eegnum,timepoint)

y = EEG(eegnum).rereferenced;

timescale = (1000/EEG(eegnum).original.header.SampleRate);
t = [1:size(y,2)]*timescale;

figure; plot(t,y);


timepoint = timepoint*timescale;
tmp = axis; hold on;plot([timepoint timepoint],[tmp(3) tmp(4)],'r','linewidth',3);

xlabel('Time (ms)');
end