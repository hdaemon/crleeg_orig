function [UseLambda] = get_BestLambda(Solution,byTimePoint,errTarget)

if ~byTimePoint
  % Get the best across all timepoints
  for idxTval = 1:size(Solution.LCurve,2);
    tmp = abs(Solution.LCurve(:,idxTval,1)-errTarget);
    Q = find(tmp==min(tmp));
    UseLambda(idxTval) = Q(1);
  end;
else
  for idxTval = 1:size(Solution.LCurve_t,2);
    for idxT = 1:size(Solution.LCurve_t,4)
      
      tmp = abs(Solution.LCurve_t(:,idxTval,1,idxT) - errTarget);
      %tmp = abs(Spikes{SpikeNum}.solutionMultiHH{k}{i,j,n}.LCurve(:,1) - errTarget);
      Q = find(tmp==min(tmp));
      UseLambda(idxTval,idxT) = Q(1);
    end;
  end
end

end