function norms = compare_ScalarSolutions(img,solSpace,imgRef,solSpaceRef,threshList)
% Given two scalar images, each defined on a cnlSolutionSpace, returns a
% set of metrics comparing them.
%
% function norms = compare_ScalarSolutions(img,solSpace,imgRef,solSpaceRef,threshList)
%
% Written By: Damon Hyde
% Last Edited: June 18, 2015
% Part of the cnlEEG Project
%


% Default is a single threshold set at zero (ie - include all voxels);
if ~exist('threshList','var'), threshList = 0; end;

% Get full 3D version of the reference image
imgRef_Full = get_FullImage(imgRef,solSpaceRef);

% Get full 3D version of the image to compare agains:
img_Full = get_FullImage(img,solSpace);

if ~all(size(imgRef_Full)==size(img_Full))
  error('Image size mismatch');
end;

% Map img onto the grid used by imgRef
gridMap = getMapGridToGrid(solSpace,solSpaceRef);
%img_Full = gridMap'*img_Full;

% Get list of image voxels in reference image space
voxInSolImg = find(sum(gridMap'*solSpace.matGridToSolSpace',2));

% Get final list of voxels to compare at.
finalVoxSet = union(solSpaceRef.Voxels,solSpace.Voxels);

% Compute the norms
norms = computeNorms(img_Full(finalVoxSet,:),imgRef_Full(finalVoxSet,:),threshList);


end


function fullImg = get_FullImage(img,solSpace)
% function fullImg = get_FullImage(img,solSpace)
%
% Given an image defined on a solution space, returns the full image in the
% 3D grid space associated with that solution space.
%

fullImg = solSpace.matGridToSolSpace'*img;

end
