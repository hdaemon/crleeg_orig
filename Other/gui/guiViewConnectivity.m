function guiViewConnectivity(nrrdSelect,solSpace,corrData)

hFig = figure;

nrrdView = nrrdSelect.view('figure',hFig);
set(nrrdView.slice.sliceAxes,'ButtonDownFcn',@pickMRIPoint)
set(nrrdView.slice.sliceAxes,'Units','Pixels');

aNRRD = convert_ImgToNrrd(zeros(size(corrData,1),1),solSpace,nrrdSelect,'row.nhdr');
aView = aNRRD.view('figure',hFig,'origin',[550 0]);
set(aView.slicecontrol.panel,'Visible','off');
%set(aView.colorcontrol.panel,'Visible','off');
set(aView.slice.sliceAxes,'ButtonDownFcn',@pickMRIPoint)
set(aView.slice.sliceAxes,'Units','Pixels');

bNRRD = convert_ImgToNrrd(zeros(size(corrData,1),1),solSpace,nrrdSelect,'column.nhdr');
bView = bNRRD.view('figure',hFig,'origin',[1100 0]);
set(bView.slicecontrol.panel,'Visible','off');
%set(bView.colorcontrol.panel,'Visible','off');
set(bView.slice.sliceAxes,'ButtonDownFcn',@pickMRIPoint)
set(bView.slice.sliceAxes,'Units','Pixels');

nrrdView.slicecontrol.updatefunction = @(a,b)linkUpdates(a,b,nrrdView,aView,bView);

  function linkUpdates(a,b,view1,view2,view3)
    view1.slice.updateSelections(a,b);
    view2.slice.updateSelections(a,b);
    view3.slice.updateSelections(a,b);
  end

  function pickMRIPoint(h,varargin)
    % function pickMRIPoint(h,varargin)
    %
    % Callback function for MRI viewing axis.  Determine the location
    % of the point clicked on in X-Y-Z space and assign that value to
    % the location of the currently selected fiducial.
    
    currAxis = nrrdView.slice.currAxis;
    if numel(currAxis)>1, keyboard; end;
    
    pos = get(get(h,'Parent'),'CurrentPoint');
    a = pos(1,1);
    b = pos(1,2);
    %slice = round(get(nrrdViewObj.slice.currSlice,'Value'));
    slice = nrrdView.slice.currSlice;
    
%     imgSize = get(nrrdView.slice.sliceAxes,'Position');
%     imgSize = imgSize(3:4);
    

            
    switch currAxis
      case 1
        x = slice;
        y = a;
        z = nrrdSelect.sizes(3)-b;
      case 2
        x = a;
        y = slice;
        z = nrrdSelect.sizes(3)-b;
      case 3
        x = a;
        y = nrrdSelect.sizes(2)-b;
        z = slice;
    end
    
    index = sub2ind(nrrdSelect.sizes,round(x),round(y),round(z));
    
    vec = zeros(nrrdSelect.nVoxels,1);
    vec(index) = 1;
    parcels = solSpace.matGridToSolSpace*vec;
    parcels = find(parcels);
    
    %disp('Updating Images');
    
    if ~isempty(parcels)
          nrrdView.slice.setCross(a,b);
      
      tmpMat = solSpace.matGridToSolSpace;
      
%       vec = zeros(size(solSpace.matGridToSolSpace,1),1);
%       vec(parcels(1)) = 1;
%       parcelImg = vec'*solSpace.matGridToSolSpace;
      newData = (tmpMat(:,index)'*corrData)*tmpMat;
      aNRRD.data = reshape(newData,aNRRD.sizes);
      aView.slice.setCross(a,b);
%        hold on;
%     plot([a a],[0 imgSize(2)],'r');
%     plot([0 imgSize(1)],[b b],'r');
%     hold off;
      
      newData = tmpMat'*(corrData*tmpMat(:,index));    
      bNRRD.data = reshape(newData,bNRRD.sizes);
      bView.slice.setCross(a,b);
%        hold on;
%     plot([a a],[0 imgSize(2)],'r');
%     plot([0 imgSize(1)],[b b],'r');
%     hold off;
      
                  
    end;   
    %loc = (nrrdMRI.spacedirections*[x y z]')' + nrrdMRI.spaceorigin;

        drawnow;
    % Now that we've got the location, let's actually do something with it.
    
  end

end