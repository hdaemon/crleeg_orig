function lapMat = surfaceLaplacian(faces)
% Compute the surface laplacian matrix, given a matrix defining the
% triangle surface.
%
%
% Written by: Damon Hyde
% Last Edited: May 23, 2016
% Part of the cnlEEG Project
%

lapMat = zeros(max(faces(:)));

for i = 1:size(faces,1)
  lapMat(faces(i,:),faces(i,:)) = 1;  
end

lapMat = lapMat - diag(diag(lapMat));
lapMat = lapMat - diag(lapMat*ones(size(lapMat,2),1));

end