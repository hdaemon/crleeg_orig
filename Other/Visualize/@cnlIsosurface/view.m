function view(obj,fig)
  % function view_Surface(obj,fig)
  %
  %
  
addFlag = false;
if ~addFlag
  clf(hfig,'reset'); % clear all graphic objects 
else
  hold on;
end;

FColor = obj.FaceColor;
FAlpha = obj.FAlpha;
EColor = obj.EdgeColor;

finalSurf.faces = obj.faces;
finalSurf.vertices = obj.vert;
finalSurf = reducepatch(finalSurf,obj.patchReduction);

hiso = patch( 'Faces',finalSurf.faces,...
              'Vertices',finalSurf.vertices,...
              'FaceColor',FColor,...
              'FaceAlpha',FAlpha,...
              'EdgeAlpha',0,...
              'EdgeColor',EColor,...
              'FaceLighting','phong'); % draw surface              
                   
axis tight equal % define the view
daspect(1./surfStruct.aspect); % set aspect ratio
   
end