classdef cnlIsosurface
  
  properties
    faces
    vert
    aspect
    X
    Y
    Z
    Xpix
    Ypix
    Zpix
    
    % Display Options
    FaceColor = [1,0.75,0.65];
    FAlpha = 0.5;
    EdgeColor = [0.7 0.7 0.7];
    patchReduction = 0.05;
    
    %
    
    
  end
  
  methods
    
    function obj = cnlIsosurface(varargin)
      
      p = inputParser;
      p.StructExpand = true;      
      addParamValue(p,'faces',[]);
      addParamValue(p,'vert',[]);
      addParamValue(p,'aspect',[]);
      addParamValue(p,'X',[]);
      addParamValue(p,'Y',[]);
      addParamValue(p,'Xpix',[]);
      addParamValue(p,'Ypix',[]);
      addParamValue(p,'Zpix',[]);
      addParamValue(p,'FaceColor',[1,0.75,0.65]);
      addParamValue(p,'FAlpha',0.5);
      addParamValue(p,'EdgeColor',[0.7 0.7 0.7]);
      addParamValue(p,'patchRed',0.05);
      
      obj.faces = p.Results.faces;
      obj.vert   = p.Results.vert;
      obj.aspect = p.Results.aspect;
      obj.X      = p.Results.X;
      obj.Y      = p.Results.Y;
      obj.Z      = p.Results.Z;
      obj.Xpix = p.Results.Xpix;
      obj.Ypix = p.Results.Ypix;
      obj.Zpix = p.Results.Zpix;
      obj.FaceColor = p.Results.FaceColor;
      obj.FAlpha    = p.Results.FAlpha;
      obj.EdgeColor = p.Results.EdgeColor;
      obj.patchReduction = p.Results.patchRed;
      
      
    end
    
  end
  
end