 function display_Recon(Recon,Patches,BrainSurfs,options)

if ~isfield(options,'surfType')
  options.surfType = 'pial';
end;

if ~isfield(options,'colormap')
  options.colormap = 'jet';
end

%% Determine Appropriate Surface to Display On
switch lower(options.surfType)
  case 'pial'
    Tris  = BrainSurfs.Pial.tris;
    Vert = BrainSurfs.Pial.vert;
    nLHv  = BrainSurfs.Pial.nLHv;
    nLHt  = BrainSurfs.Pial.nLHt;
  case 'inflated'
    Tris  = BrainSurfs.Inflated.tris;
    Vert = BrainSurfs.Inflated.vert;
    nLHv  = BrainSurfs.Inflated.nLHv;
    nLHt  = BrainSurfs.Inflated.nLHt;
  case 'white'
    Tris  = BrainSurfs.White.tris;
    Vert = BrainSurfs.White.vert;    
    nLHv  = BrainSurfs.White.nLHv;
    nLHt  = BrainSurfs.White.nLHt;
end;


%% Construct Vertex Color Map
DispSol = Recon(Patches.Nearest);

%DispSol = DispSol./Patches.PatchSizes(:);

if isfield(options,'useabs')&&(options.useabs)
  disp(['Displaying intensity information only']);
  DispSol = abs(DispSol);
end;

if isfield(options,'thresh')
  disp(['Thresholding solution at ' num2str(options.thresh) ' percent']);
  thresh = prctile(DispSol,options.thresh);
  Q = find(DispSol<thresh);
  DispSol(Q) = 0;
end;

if isfield(options,'crop');
  disp(['Cropping upper end of solution at ' num2str(options.crop) ' percent']);
  crop = prctile(DispSol,options.crop);
  Q = find(DispSol>crop);
  DispSol(Q) = crop;
end;

%% Get Surfaces for Each Hemisphere Separately
if isfield(options,'separate_LR')&&(options.separate_LR);
      TrisLeft = Tris(1:nLHt,:);
      VertLeft = Vert(1:nLHv,:);
      DispSolLeft = DispSol(1:nLHv,:);

      TrisRight = Tris(nLHt+1:end,:);
      TrisRight = TrisRight - nLHv;
      VertRight = Vert(nLHv+1:end,:);
      DispSolRight = DispSol(nLHv+1:end,:);
end



%% Open Figure and Display Patch Surface
if isfield(options,'fignum')&&(~isempty(options.fignum))
  figure(options.fignum)
else
  figure;
end;

clf;

if isfield(options,'separate_LR')&&(options.separate_LR)
  cmin = min([min(DispSolLeft) min(DispSolRight)]);
  cmax = max([max(DispSolLeft) max(DispSolRight)]);
  Clim = [cmin cmax];
  subplot(1,2,1);
  patch('Faces',TrisRight,'Vertices',VertRight,'EdgeAlpha',0,'FaceVertexCData',DispSolRight,'FaceColor','interp');
  caxis(Clim);
  axis equal; colormap(options.colormap);
  if isfield(options,'View_r')&&(~isempty(options.View_r))
    view(options.View_r);
  end;  
  subplot(1,2,2);
  patch('Faces',TrisLeft,'Vertices',VertLeft,'EdgeAlpha',0,'FaceVertexCData',DispSolLeft,'FaceColor','interp');
  caxis(Clim);
  axis equal; colormap(options.colormap);
  if isfield(options,'View_l')&&(~isempty(options.View_l))
    view(options.View_l);
  end;

else
  patch('Faces',Tris,'Vertices',Vert,'EdgeAlpha',0,'FaceVertexCData',DispSol,'FaceColor','interp');
  axis equal;
end;

%colorbar;


return;