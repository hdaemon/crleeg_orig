% function display_reconstruction(Solution,options)
%
% Function to display an EEG source localization reconstruction slice by
% slice.
function display_reconstruction(Solution,options)

SolutionPoints = options.SolutionPoints;
fullSize = options.fullSize;
if isfield(options,'slices')&&~isempty(options.slices), 
  slices = options.slices; 
else
  slices = 1:fullSize(3);
end;

if isfield(options,'fignum'), 
  fignum = options.fignum; 
  clf(fignum);
else
  figure; fignum = gcf;
end;

if isfield(options,'vecSol'),
  vecSol = options.vecSol;
else
  vecSol = true;
end;



if vecSol
  Solution = reshape(Solution,3,length(Solution)/3);
  solImage = zeros(fullSize);

  ImageSol1 = zeros(fullSize);
  ImageSol1(SolutionPoints) = (sqrt(Solution(1,:).^2 + Solution(2,:).^2 + Solution(3,:).^2));
else
  ImageSol1 = zeros(fullSize);
  ImageSol1(SolutionPoints) = ((Solution));
end;

cmin = min(ImageSol1(:));
cmax = max(ImageSol1(:));
%keyboard;
Q = any(any(ImageSol1,1),2);
Q = squeeze(Q);
Q = find(Q);

try
slices = slices(ismember(slices,Q));
catch
  keyboard;
end;


for i = slices
  figure(fignum);
  imagesc(ImageSol1(:,:,i)',[cmin cmax]); hold on; axis image;
  title(num2str(i));
  pause(0.75);
end

return;