function ViewSurface( hfig, surfStruct, addFlag, Ds )
% Given a surface crated with ExtractIsosurface, plot it
%
%

if nargin<4, Ds = []; end
if nargin<3, addFlag = false; end;

if ~addFlag
  clf(hfig,'reset'); % clear all graphic objects 
else
  hold on;
end;

if isfield(surfStruct,'FaceColor')
  FColor = surfStruct.FaceColor;
else
  FColor = [1,0.75,0.65];
  mydisp('Using Default Face Color');
end;

if isfield(surfStruct,'FAlpha');
  FAlpha = surfStruct.FAlpha;  
else
  FAlpha = 0.5;
  mydisp('Using Default Alpha Value');
end;

if isfield(surfStruct,'EdgeColor')
  EColor = surfStruct.EdgeColor;
else
  EColor = [0.7 0.7 0.7];
  mydisp('Using Default Edge Color');
end;

finalSurf.faces    = surfStruct.faces;
finalSurf.vertices = surfStruct.vert;
finalSurf = reducepatch(finalSurf,0.05);
%finalSurf

hiso = patch( 'Faces',finalSurf.faces,...
              'Vertices',finalSurf.vertices,...
              'FaceColor',FColor,...
              'FaceAlpha',FAlpha,...
              'EdgeAlpha',0,...
              'EdgeColor',EColor,...
              'FaceLighting','phong'); % draw surface              
          
% hiso = patch( 'Faces',faces,...
%               'Vertices',vert,...          
%               'FaceColor',[1,.75,.65],...
%               'EdgeColor','none'); % draw surface
         
%view(45,30), 
axis tight equal % define the view
daspect(1./surfStruct.aspect); % set aspect ratio
% % daspect(aspect) % set aspect ratio
% 
% if isempty(Ds), return, end
% lightangle(45,30); % add lighting
% set(hfig,'Renderer','zbuffer'); lighting phong

% clear  faces vert aspect
% load headsurf.mat Ds
% isonormals(Ds,hiso)
% set(hiso,'SpecularColorReflectance',0,'SpecularExponent',50)
% 
% alpha(.5)
return;