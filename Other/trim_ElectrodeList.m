function [ElecOut] = trim_ElectrodeList(ElecIn)

load('/external/crl-rdynaspro5/damon/epilepsysurgicalplanning/ClinicalElectrodeList');

Q = find(ismember(ElecIn.point_labels,ElectrodeList));

ElecOut.point_labels = ElecIn.point_labels(Q);
ElecOut.point_coords = ElecIn.point_coords(Q,:);

end