function Patients = CheckWorkToBeDone

idxCases = getIdxCases;

for idx = 1:length(idxCases)
  cd('/external/crl-rdynaspro5/damon/epilepsysurgicalplanning');
  Patients{idx}.CaseID = idxCases{idx};
  
  if exist(['Case' idxCases{idx} ],'dir')
    cd(['Case' idxCases{idx}]);
    mydisp(['Changing to Case' idxCases{idx} '/sourceanalysis/models']);
    Patients{idx}.dirExists = true;
    
    %Check that data files are there
    Patients{idx}.t1Exists = checkForFile('./data_for_analysis/bestt1w.nrrd');
    Patients{idx}.t2Exists = checkForFile('./data_for_analysis/bestt2w.nrrd');
    Patients{idx}.tensorsExist = checkForFile('./data_for_analysis/tensors.nrrd');
    
    Patients{idx}.iccExists = checkForFile('./brainseg/icc.nrrd');
    Patients{idx}.segExists = checkForFile('./brainseg/segmentation.nrrd');
    
    Patients{idx}.nmmExists = checkForFile('./common-processed/modules/parcellationNMM/ParcellationNMM.nrrd');
    Patients{idx}.ibsrExists = checkForFile('./common-processed/modules/parcellationIBSR/ParcellationIBSR.nrrd');
    
    Patients{idx}.tensorsAligned = checkForFile('./dti/clean-warped-tensors.nhdr');
    
  end;
        
end;

end % END Main Function




function exists = checkForFile(filePath)
  if exist(filePath,'file')
    exists = true;
  else
    exists = false;
  end;

end