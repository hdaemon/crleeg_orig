function [nrrdParcelOut] = parcelateOriginalSegmentation(nrrdSeg,nrrdParcel,type,nrrdName,nrrdPath)
% function [nrrdParcelOut] = parcelateOriginalSegmentation(nrrdSeg,nrrdParcel,type,nrrdName)


mydisp('Parcellating Segmentation');

%Default FileName
if ~exist('nrrdName')||isempty(nrrdName), nrrdName = 'ParcelOut.nhdr'; end;

%Default FilePath
if ~exist('nrrdPath')||isempty(nrrdPath), nrrdPath ='./'; end;

% Relabel Parcellation as Grey/White/CSF
switch lower(type)
  case 'nmm'
    Parcel = ParcellationMapping('NMM');
  case 'ibsr'
    Parcel = cnlParcellation.getMapping('IBSR');
  otherwise
    error('Unknown Parcellation Type');
end;

% Initialize Output
mydisp('Initializing Output Parcellation');
nrrdParcelOut = clone(nrrdSeg,nrrdName,nrrdPath);
nrrdParcelOut = cnlParcellation(nrrdParcelOut,[],type);
nrrdParcelOut.data = zeros(size(nrrdParcelOut.data));

% Indicies of Cortex/SubCortex/White Matter in Parcellation
mydisp('Working on cortical labels');
ParcelCortex    = ismember(nrrdParcel.data,[Parcel.cortexLabels Parcel.subcorticalLabels]);
ParcelSubCortex = ismember(nrrdParcel.data,[Parcel.subcorticalLabels]);
ParcelWhite     = ismember(nrrdParcel.data,[Parcel.whiteLabels]);
ParcelAllGrey   = ParcelCortex | ParcelSubCortex;

segCortex  = nrrdSeg.data==4;
segCortexA = segCortex & (~ParcelAllGrey); % Voxels we need nearest neighbor for
segCortexB = segCortex &   ParcelAllGrey ; % Voxels where the label matches up

% Get XYZ locations for voxels
[Xtarget Ytarget Ztarget] = ind2sub(nrrdSeg.sizes,find(segCortexA));
[X Y Z] = ind2sub(nrrdSeg.sizes,find(ParcelAllGrey));

% Do a Delaunay Triangulation and use nearestNeighbor() to find the nearest
% points
dt = DelaunayTri(X(:),Y(:),Z(:));
nearest = nearestNeighbor(dt,[Xtarget(:) Ytarget(:) Ztarget(:)]);
nearest = dt.X(nearest,:);
nearest = sub2ind(nrrdSeg.sizes,nearest(:,1),nearest(:,2),nearest(:,3));

nrrdParcelOut.data(segCortexA) = nrrdParcel.data(nearest)    ;
nrrdParcelOut.data(segCortexB) = nrrdParcel.data(segCortexB) ;

% Do it all again for the white matter
mydisp('Working on White matter Labels');
segWhite = nrrdSeg.data==7;
segWhiteA = segWhite & (~ParcelWhite);
segWhiteB = segWhite & ( ParcelWhite);

[Xtarget Ytarget Ztarget] = ind2sub(nrrdSeg.sizes,find(segWhiteA));
[X Y Z] = ind2sub(nrrdSeg.sizes,find(ParcelWhite));

dt = DelaunayTri(X(:),Y(:),Z(:));
nearest = nearestNeighbor(dt,[Xtarget(:) Ytarget(:) Ztarget(:)]);
nearest = dt.X(nearest,:);
nearest = sub2ind(nrrdSeg.sizes,nearest(:,1),nearest(:,2),nearest(:,3));

nrrdParcelOut.data(segWhiteA) = nrrdParcel.data(nearest);
nrrdParcelOut.data(segWhiteB) = nrrdParcel.data(segWhiteB);


mydisp('Done parcellating segmentation');
end
