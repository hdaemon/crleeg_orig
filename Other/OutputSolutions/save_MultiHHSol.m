function save_MultiHHSol(saveOptions,Solutions,NRRDS,fname)
%
% function save_MultiHHSol(doLambda,doHH,doTHH,doT,doCompute,errTarget,selectedTimePoint,Solutions,TemplateNRRD,fname)
%
% Saves out an array of different solutions at a particular timepoint from a multi-parametered
% Helmholtz solution.  This can also be used to save out LORETA solutions
% obtained using getSol_SpatioTemporal
%
% doCompute, doHH, doTHH, and doT are needed only for multiHH solutions.

doLambda          = saveOptions.doLambda;
errTarget         = saveOptions.errTarget;
selectedTimePoint = saveOptions.selectedTimePoint;
doCompute         = saveOptions.doCompute;
doHH              = saveOptions.doHH;
doTHH             = saveOptions.doTHH;
doT               = saveOptions.doT;


if ~exist('fname','var')||isempty(fname), fname = Solutions{1}.options.solutionName; end;

TemplateNRRD = NRRDS.DownSampledSegmentation;

for idxLam = 1:length(doLambda);
  % Save out Helmholtz Regularized Solutions
  for i = doHH
    for j = doTHH
      for n = doT
        if doCompute(i,j,n)
          %for k = doReconType
          if doLambda~=0
            UseLambda = doLambda(idxLam);
          else
            tmp = abs(Solutions{i,j,n}.LCurve(:,1,selectedTimePoint) - errTarget);
            %tmp = abs(Spikes{SpikeNum}.solutionMultiHH{k}{i,j,n}.LCurve(:,1) - errTarget);
            Q = find(tmp==min(tmp));
            UseLambda = Q(1);
            disp(['Saving out Lambda #' num2str(UseLambda) ]);
          end;
       
          if all((UseLambda>0)&(UseLambda<=length(Solutions{i,j,n}.solutions)))
            % Get Filename
            filenamebase = get_SolutionName(Solutions{i,j,n}.options);
            filenamebase = [fname '_T' num2str(selectedTimePoint)  '_L' num2str(UseLambda) '_' filenamebase];
            
            for k = 1:length(UseLambda)
              save_SolutionAsNrrd(TemplateNRRD,Solutions{i,j,n},selectedTimePoint,UseLambda(k),1,filenamebase)
            end;
          end;
          %end
        end
      end
    end
  end
end;

end