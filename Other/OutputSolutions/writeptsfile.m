function writeptsfile(fname,ptsmat)
% Author: Burak Erem

if(size(ptsmat,2)==3)
    ptsmat=ptsmat.';
else
    if(size(ptsmat,1)~=3)
        error('Error: One of the dimensions of the input matrix needs to equal 3')
    end
end

h=fopen(fname,'w+');
format='%f %f %f\n';
fprintf(h,format,ptsmat);
fclose(h);
