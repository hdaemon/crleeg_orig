function save_MatrixDecompSolutionsAsPointCloud(SolutionStruct,varargin)
% function save_MatrixDecompSolutionsAsPointCloud(SolutionStruct,['/path/to/temp/files/'])
% Author: Burak Erem
% 

PATHTOTEMPFILES=[pwd '/'];
if(numel(varargin)>0)
    PATHTOTEMPFILES=varargin{1};
end

leftmatrix=SolutionStruct.Recons{1}.solutions{1};
rightmatrix=SolutionStruct.Recons{1}.rightmatrix;

currSolutionRegion=SolutionStruct.Recons{1}.options.solutionSpace;

% % Get the size of the grid that contains all of the voxels:
% SpaceSize=currSolutionRegion.sizes;
 
% Get the list of linear indices for the source voxels:
Voxels=currSolutionRegion.Voxels;

% % Convert the linear indices of voxels to 3D indices:
% [x,y,z]=ind2sub(SpaceSize,Voxels);
% 
% pointCloud = [x(:),y(:),z(:)];

pointCloud = currSolutionRegion.getGridPoints;
pointCloud = pointCloud(Voxels,:);

map=currSolutionRegion.matGridToSolSpace(:,Voxels)';

% write matrices to specified path:
save([PATHTOTEMPFILES 'leftmat.mat'],'leftmatrix','-v7');
save([PATHTOTEMPFILES 'rightmat.mat'],'rightmatrix','-v7');
save([PATHTOTEMPFILES 'solutionmap.mat'],'map','-v7');
writeptsfile([PATHTOTEMPFILES 'voxellocations.pts'],pointCloud);

% TODO: use mfilename() to figure out where this is located, then copy the
% srn file to the specified path

[mfilepath,~,~] = fileparts(mfilename('fullpath'));
mfilepath = [mfilepath '/'];
copyfile([mfilepath 'view_MatrixDecompSolutionsAsPointCloud.srn'], [PATHTOTEMPFILES 'view_MatrixDecompSolutionsAsPointCloud.srn']);

sedcommand=sprintf('sed -e "s#PATHTO#%s#g" %sview_MatrixDecompSolutionsAsPointCloud.srn > %sview_MatrixDecompSolutionsAsPointCloud.tmp',PATHTOTEMPFILES,PATHTOTEMPFILES,PATHTOTEMPFILES);
system(sedcommand);

copyfile([PATHTOTEMPFILES 'view_MatrixDecompSolutionsAsPointCloud.tmp'],[PATHTOTEMPFILES 'view_MatrixDecompSolutionsAsPointCloud.srn']);
delete([PATHTOTEMPFILES 'view_MatrixDecompSolutionsAsPointCloud.tmp']);