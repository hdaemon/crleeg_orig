function save_MatrixDecompSolutionsAsPointCloudForMovie(SolutionStruct,varargin)
% function save_MatrixDecompSolutionsAsPointCloudForMovie(SolutionStruct,['/path/to/temp/files/'])
% Author: Burak Erem
% 

USEDEFAULTTIMES=1;

PATHTOTEMPFILES=[pwd '/'];
PATHTOIMAGES=PATHTOTEMPFILES;
for i=1:2:numel(varargin)
    if(strcmpi(varargin{i},'pathtotempfiles'))
        PATHTOTEMPFILES=varargin{i+1};
    end
    if(strcmpi(varargin{i},'pathtoimages'))
        PATHTOIMAGES=varargin{i+1};
    end
    if(strcmpi(varargin{i},'timeinterval')) % starts counting from 0
        TIMEINTERVAL=varargin{i+1};
        USEDEFAULTTIMES=0;
    end
end

leftmatrix=SolutionStruct.Recons{1}.solutions{1};
rightmatrix=SolutionStruct.Recons{1}.rightmatrix;

if(USEDEFAULTTIMES)
    TIMEINTERVAL=[0, size(rightmatrix,2)-1];
end

currSolutionRegion=SolutionStruct.Recons{1}.options.solutionSpace;

% % Get the size of the grid that contains all of the voxels:
% SpaceSize=currSolutionRegion.sizes;
 
% Get the list of linear indices for the source voxels:
Voxels=currSolutionRegion.Voxels;

% % Convert the linear indices of voxels to 3D indices:
% [x,y,z]=ind2sub(SpaceSize,Voxels);
% 
% pointCloud = [x(:),y(:),z(:)];

pointCloud = currSolutionRegion.getGridPoints;
pointCloud = pointCloud(Voxels,:);

map=currSolutionRegion.matGridToSolSpace(:,Voxels)';

% write matrices to specified path:
save([PATHTOTEMPFILES 'leftmat.mat'],'leftmatrix','-v7');
save([PATHTOTEMPFILES 'rightmat.mat'],'rightmatrix','-v7');
save([PATHTOTEMPFILES 'solutionmap.mat'],'map','-v7');
writeptsfile([PATHTOTEMPFILES 'voxellocations.pts'],pointCloud);

% TODO: use mfilename() to figure out where this is located, then copy the
% srn file to the specified path

[mfilepath,~,~] = fileparts(mfilename('fullpath'));
mfilepath = [mfilepath '/'];
copyfile([mfilepath 'view_MatrixDecompSolutionsAsPointCloudForMovie.srn'], [PATHTOTEMPFILES 'view_MatrixDecompSolutionsAsPointCloudForMovie.srn']);

sedcommand=sprintf('sed -e "s#PATHTO#%s#g" %sview_MatrixDecompSolutionsAsPointCloudForMovie.srn > %sview_MatrixDecompSolutionsAsPointCloudForMovie.tmp',PATHTOTEMPFILES,PATHTOTEMPFILES,PATHTOTEMPFILES);
system(sedcommand);

sedcommand=sprintf('sed -e "s#MOVIE_START_TIME#%i#g" %sview_MatrixDecompSolutionsAsPointCloudForMovie.tmp > %sview_MatrixDecompSolutionsAsPointCloudForMovie.tmp2',TIMEINTERVAL(1),PATHTOTEMPFILES,PATHTOTEMPFILES);
system(sedcommand);

sedcommand=sprintf('sed -e "s#MOVIE_END_TIME#%i#g" %sview_MatrixDecompSolutionsAsPointCloudForMovie.tmp2 > %sview_MatrixDecompSolutionsAsPointCloudForMovie.tmp3',TIMEINTERVAL(2),PATHTOTEMPFILES,PATHTOTEMPFILES);
system(sedcommand);

sedcommand=sprintf('sed -e "s#PATH_TO_IMAGES#%s#g" %sview_MatrixDecompSolutionsAsPointCloudForMovie.tmp3 > %sview_MatrixDecompSolutionsAsPointCloudForMovie.srn',PATHTOIMAGES,PATHTOTEMPFILES,PATHTOTEMPFILES);
system(sedcommand);

delete([PATHTOTEMPFILES 'view_MatrixDecompSolutionsAsPointCloudForMovie.tmp*']);