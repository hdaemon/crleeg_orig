function save_EEGDataAsButterflyPlotForMovie(SolutionStruct,varargin)
% function save_MatrixDecompSolutionsAsPointCloudForMovie(SolutionStruct,['/path/to/temp/files/'])
% Author: Burak Erem
% 

USEDEFAULTTIMES=1;

PATHTOIMAGES=[pwd '/'];
for i=1:2:numel(varargin)
    if(strcmpi(varargin{i},'pathtoimages'))
        PATHTOIMAGES=varargin{i+1};
    end
    if(strcmpi(varargin{i},'timeinterval')) % starts counting from 0
        TIMEINTERVAL=varargin{i+1};
        USEDEFAULTTIMES=0;
    end
end

if(USEDEFAULTTIMES)
    TIMEINTERVAL=[1 size(SolutionStruct.EEG.datafile.data,1)];
end

h=figure;
hpos=get(h,'Position');
hpos(3:4)=[696 681];
set(h,'Position',hpos)

oldscreenunits = get(h,'Units');
oldpaperunits = get(h,'PaperUnits');
oldpaperpos = get(h,'PaperPosition');
set(h,'Units','pixels');
scrpos = get(h,'Position');
newpos = scrpos/100;

for i=TIMEINTERVAL(1):TIMEINTERVAL(2)
    figure(h)
    plot(TIMEINTERVAL(1):TIMEINTERVAL(2),SolutionStruct.EEG.datafile.data(TIMEINTERVAL(1):TIMEINTERVAL(2),SolutionStruct.EEG.useElec))
    hold on
    line(i*ones(1,2),get(gca,'YLim'),'Color',[0 0 0])
    hold off
    xlim([TIMEINTERVAL(1),TIMEINTERVAL(2)])
    
    set(h,'PaperUnits','inches',...
     'PaperPosition',newpos)
    print(h,'-dpng',[PATHTOIMAGES sprintf('butterflyplot%0.7i.png',i)],'-r100')
    drawnow
    set(h,'Units',oldscreenunits,...
     'PaperUnits',oldpaperunits,...
     'PaperPosition',oldpaperpos)
end


% % write matrices to specified path:
% save([PATHTOTEMPFILES 'leftmat.mat'],'leftmatrix','-v7');
% save([PATHTOTEMPFILES 'rightmat.mat'],'rightmatrix','-v7');
% save([PATHTOTEMPFILES 'solutionmap.mat'],'map','-v7');
% writeptsfile([PATHTOTEMPFILES 'voxellocations.pts'],pointCloud);

