function save_SolutionAsRGBNrrd(nrrdTemplate,solution,TimePoint,Lambda,filename)

try
  % Set and Normalize Image Data
  data = solution.solutions{Lambda};

 map = solution.options.solutionSpace.matGridToSolSpace;
  
  if solution.options.isVectorSolution
   map = kron(map,eye(3));
  end;
  data = map'*data;

  if solution.options.isVectorSolution
    data = reshape(data,[3 size(data,1)/3 size(data,2)]);
    data = data(:,solution.options.solutionSpace.Voxels,:);
    data = reshape(data,[3*size(data,2) size(data,3)]);
  else 
    data = data(solution.options.solutionSpace.Voxels,:);
  end;
  data = data';
  data = data(TimePoint,:);
  %data = data./max(data);
  data = data./max(data);
    
  % Build NRRD And Save
  nrrdSolution = file_NRRD([filename '.nhdr'],pwd);
  nrrdSolution.content = 'EEGSolution';
  nrrdSolution.dimension = 4;
  nrrdSolution.space = 'left-posterior-superior';
  nrrdSolution.sizes = solution.options.solutionSpace.sizes;
  nrrdSolution.endian = 'little';
  nrrdSolution.encoding = 'raw';
  nrrdSolution.spaceorigin = solution.options.solutionSpace.origin;
  nrrdSolution.kinds = {'RGB-color' 'domain' 'domain' 'domain'};
  nrrdSolution.spacedirections = solution.options.solutionSpace.directions;
  
  if length(data)==3*length(solution.options.solutionSpace.Voxels);
   % error('Can''t do RGB output for vector solutions');
   % nrrdSolution.dimension = 4;
   % nrrdSolution.sizes   = [3 nrrdSolution.sizes];
   % nrrdSolution.kinds   = {'vector' nrrdSolution.kinds{:}};
    data = reshape(data,[3 length(data)/3]);
    data = sqrt(sum(data.^2,1));
    data = squeeze(data);
    
    dataout = zeros([solution.options.solutionSpace.sizes]);
    dataout(solution.options.solutionSpace.Voxels) = data;
  else    
    dataout = zeros(solution.options.solutionSpace.sizes);
    dataout(solution.options.solutionSpace.Voxels) = data;    
  end;
  
  CMap = hot(256);
  
  dataout = abs(dataout);
  
  dataout = 255*dataout./max(dataout(:));
  dataout = round(dataout);
  dataout = dataout+1;
  
  tmp = CMap(dataout,:);
  tmp = reshape(tmp',[3 size(dataout)]);
  
  nrrdSolution.sizes = [3 nrrdSolution.sizes];
  nrrdSolution.dimension = 4;
  nrrdSolution.data    = round(255*tmp);
  nrrdSolution.type = 'unsigned char';
  nrrdSolution.save;
catch
  keyboard;
end;

return;