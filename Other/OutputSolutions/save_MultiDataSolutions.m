function save_MultiDataSolutions(DATAIN,TemplateNRRD,Spikes,saveOptions)
%
% function save_MultiDataSolutions(DATAIN,TemplateNRRD,Spikes,saveOptions)
%
% Given a cell array of Spikes, uses save_Solutions to write NRRDS for each
% set of spike data


for m = 1:length(DATAIN)
  %  [path name ext] = fileparts(files.EEGData{m});
  
  % And a separate directory for each data set
  name = DATAIN(m).dirName;

  if ~exist(name,'dir')
    mkdir(name);
  end;
  cd(name);
  
  save_Solutions(TemplateNRRD,Spikes{m}.Solution,saveOptions{m});
  
  cd ..
end;


end

