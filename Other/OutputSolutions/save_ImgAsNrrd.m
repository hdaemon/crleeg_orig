function varargout = save_ImgAsNrrd(img,solSpace,templateNRRD,fname,fpath,vecToScalar)
%  function save_ImgAsNrrd(img,solSpace,templateNRRD)
%
%  Takes a vector of image values, defined on a cnlSolutionSpace, and saves
%  it out as a NRRD file

if ~exist('vecToScalar','var'), vecToScalar = false; end;
if ~exist('fpath','var'), fpath = './'; end;
if ~exist('fname','var'), error('Must supply a filename'); end;
if ~isa(templateNRRD,'file_NRRD'), error('Must supply a template NRRD'); end;

isVector = (length(img) == 3*size(solSpace.matGridToSolSpace,1));

map = solSpace.matGridToSolSpace;
gtg = getMapGridToGrid(solSpace,templateNRRD.gridSpace);

if isVector, map = kron(map,eye(3)); gtg = kron(gtg,eye(3)); end;

img = gtg'*(map'*img);

% Initialize Output NRRD as a Clone of the Template
nrrdOut = clone(templateNRRD,fname,fpath);
nrrdOut.type = 'double';
nrrdOut.content = 'EEGSolution';

% Set img field out output NRRD appropriately
if isVector
  if vecToScalar
    img = vecToScalar(img);
    imgout = reshape(imgout,templateNRRD.sizes);
  else
    nrrdOut.dimension = 4;
    nrrdOut.sizes = [ 3 nrrdOut.sizes];
    nrrdOut.kinds = {'vector' nrrdOut.kinds{:}};
    imgout = reshape(img,[3 templateNRRD.sizes]);
  end
else
  imgout = reshape(img,templateNRRD.sizes);
end

nrrdOut.data = imgout;
%nrrdOut.save;
 
% Optional output
if nargout ~= 0
  varargout{1} = nrrdOut;
end;

end

% The functions below are probably really useful utility functions to have
% around, and should perhaps be incorporated elsewhere int he code.
  
function out = vecToScalar(in)
  out = reshape(in,[3 length(in)/3]);
  out = sqrt(sum(out.^2,1));
end

  function out = scalarToVec(scalarIn)
  end