function nrrdOut = create_NrrdFromImg(img,solSpace,fname,fpath,convertVecToScalar)
% Generates a file_NRRD from an image and an associated cnlSolutionSpace
%
% function nrrdOut = CREATE_NRRDFROMIMG(img,solSpace,fname,fpath,vecToScalar)
%
% Inputs
%   img      : Input image, defined on a cnlSolutionSpace
%   solSpace : The cnlSolutionSpace the image is defined on
%   fname    : Filename for the output NRRD (Default: tmp.nhdr)
%   fpath    : Filepath fot the output NRRD (Default: ./)
%   vecToScalar: Flag to determine whether a vector image should be 
%                 collapsed into a scalar.
%
% Written By: Damon Hyde
% Last Edited: June 23, 2015
% Part of the cnlEEG Project

if ~exist('convertVecToScalar','var'), convertVecToScalar = false; end;
if ~exist('fpath','var'), fpath = './'; end;
if ~exist('fname','var'), fname = 'tmp.nhdr'; end;

% Get mapping matrix and output image size
map = solSpace.matGridToSolSpace;
nVox = size(map,1);

% Check if it looks like an unconstrained dipole image
if size(img,1)==nVox
  % Image of scalars
  isVector = false;
elseif size(img,1)==3*nVox
  % Image of x-y-z vectors
  isVector = true;
else
  % It's a size we can't deal with.
  error('Image size is incorrect');
end;

if isVector, map = kron(map,eye(3));  end;

% Map the image over
img = (map'*img);

% Check if we have a stack of images (typically, multiple timepoints)
nVec = size(img,2);
isImgStack = nVec>1;

% Initialize Output NRRD as a Clone of the Template
nrrdOut = file_NRRD(fname,fpath);
nrrdOut.type = 'double';
nrrdOut.space = 'left-posterior-superior';
nrrdOut.spaceorigin = solSpace.origin;
nrrdOut.spacedirections = solSpace.directions;
nrrdOut.endian = 'little';
nrrdOut.dimension = 3;
nrrdOut.encoding = 'gzip';

nrrdOut.sizes = solSpace.sizes;
nrrdOut.kinds = {'domain' 'domain' 'domain'};

if isVector&&(~convertVecToScalar)
    nrrdOut.dimension = nrrdOut.dimension + 1;
    nrrdOut.sizes = [3 nrrdOut.sizes];
    nrrdOut.kinds = {'vector' nrrdOut.kinds{:}};
end;

if isImgStack
    nrrdOut.dimension = nrrdOut.dimension + 1;
    nrrdOut.sizes = [nVec nrrdOut.sizes ];
    nrrdOut.kinds = {'vector' nrrdOut.kinds{:} };
end;

% Set img field out output NRRD appropriately
if isVector
  disp('Input Image is Vector Image');
  if convertVecToScalar
      disp('Converting vector scalar');
    img = vecToScalar(img);
    imgout = reshape(img,[solSpace.sizes nVec]);
  else    
    imgout = reshape(img,[3 solSpace.sizes nVec]);
  end
else
  imgout = reshape(img,[solSpace.sizes nVec]);
end

imgout = permute(imgout,[5 1 2 3 4]);

nrrdOut.data = squeeze(imgout);

end

% The functions below are probably really useful utility functions to have
% around, and should perhaps be incorporated elsewhere int he code.
  
function out = vecToScalar(in)

  nVox = size(in,1)/3;
  nVec = size(in,2);
  out = reshape(in(:),[3 nVec*nVox]);
  out = sqrt(sum(out.^2,1));
  out = reshape(out,nVox,nVec);
end

  %function out = scalarToVec(scalarIn)
  %end


