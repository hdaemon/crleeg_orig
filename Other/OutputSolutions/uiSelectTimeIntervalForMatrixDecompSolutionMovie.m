function uiSelectTimeIntervalForMatrixDecompSolutionMovie(SolutionStruct,varargin)
% function save_MatrixDecompSolutionsAsPointCloudForMovie(SolutionStruct,['/path/to/temp/files/'])
% Author: Burak Erem
% 

USEDEFAULTTIMES=1;

PATHTOTEMPFILES=[pwd '/'];
PATHTOIMAGES=PATHTOTEMPFILES;
for i=1:2:numel(varargin)
    if(strcmpi(varargin{i},'pathtotempfiles'))
        PATHTOTEMPFILES=varargin{i+1};
    end
    if(strcmpi(varargin{i},'pathtoimages'))
        PATHTOIMAGES=varargin{i+1};
    end
    if(strcmpi(varargin{i},'timeinterval'))
        TIMEINTERVAL=varargin{i+1};
        USEDEFAULTTIMES=0;
    end
end

if(USEDEFAULTTIMES)
    plot(SolutionStruct.EEG.datafile.data(:,SolutionStruct.EEG.useData))
else
    plot(SolutionStruct.EEG.datafile.data(TIMEINTERVAL(1):TIMEINTERVAL(2),SolutionStruct.EEG.useData))
end
