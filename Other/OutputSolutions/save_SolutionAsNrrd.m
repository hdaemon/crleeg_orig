function save_SolutionAsNrrd(nrrdTemplate,solution,TimePoint,Lambda,LambdaT,filename,vecToScalar)

% function save_SolutionAsNrrd(nrrdTemplate,solution,TimePoint,Lambda,LambdaT,filename,vecToScalar)
%
% Given a NRRD template, saves out the solution as a 3D NRRD.
%
% Inputs: nrrdTemplate
%         solution      : solution objects containing solution.solutions
%


% Assume we aren't converting to a scalar
if ~exist('vecToScalar'), vecToScalar = false; end;


  % Set and Normalize Image Data
  nrrdOut = convert_ImgToNrrd(solution.solutions{Lambda,LambdaT}(:,TimePoint), ...
    solution.options.solutionSpace, nrrdTemplate,filename,...
    './',vecToScalar);
  nrrdOut.save;
  
%   data = solution.solutions{Lambda,LambdaT};
%   map = solution.options.solutionSpace.matGridToSolSpace;
%   gtg = getMapGridToGrid(solution.options.solutionSpace,nrrdTemplate.gridSpace);
%   
%   if solution.options.isVectorSolution
%    map = kron(map,eye(3));
%    gtg = kron(gtg,eye(3));
%   end;
%   data = gtg'*map'*data;
% 
%   % Pick Timepoint
%   data = data(:,TimePoint);
%     
%   % Build NRRD And Save
%   nrrdSolution = clone(nrrdTemplate,[filename '.nhdr'],pwd);
%   nrrdSolution.type = 'double';
%   nrrdSolution.content = 'EEGSolution';
%   
%   if length(data)==3*prod(nrrdTemplate.sizes);
%     
%     % Build the data structure    
%     if vecToScalar
%       dataout = vecToScalar(data);      
%       mydisp(['Maximum Data Value (vecToScalar): ' num2str(max(data))]);
%       dataout = reshape(dataout,nrrdTemplate.sizes);
%     else
%       nrrdSolution.dimension = 4;
%       nrrdSolution.sizes   = [3 nrrdSolution.sizes];
%       nrrdSolution.kinds   = {'vector' nrrdSolution.kinds{:}};
%       
%       dataout = reshape(data,[3 nrrdTemplate.sizes]);            
%     end;
%     
%   else % Scalar image case
%     dataout = reshape(data,nrrdTemplate.sizes);        
%   end;
%   
%   nrrdSolution.data    = dataout;
%   mydisp(['Maximum Data Value (nrrdSolution.data): ' num2str(max(nrrdSolution.data(:)))]);
%   nrrdSolution.save;


end


function out = vecToScalar(in)
  out = reshape(in,[3 length(in)/3]);
  out = sqrt(sum(out.^2,1));
end

  function out = scalarToVec(scalarIn)
  end
