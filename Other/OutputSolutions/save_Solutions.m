function save_Solutions(TemplateNRRD,Recon,saveOptions)

%
% function save_Solutions(Solution,doLambda,doTimePoints,errTarget)
%
% Take a cell array of source localization solutions and save out the
% individual solutions as NRRDS.
%
% Inputs: TemplateNRRD : NRRD to use as the output template
%         Solution     : Cell array of solutions to output from
%         doSolution   : Which solutions to output (defaults to all of them)
%         doLambda     : Which lambdas to output (defaults to all of them)
%         doTimePoints : Which timepoints to output (defaults to all of them)
%         errTarget    : Target error when selecting a regularization value
%                         (Defaults to 0.1)
%

doByTime     = saveOptions.doByTimePoint;

doSolution   = saveOptions.doSolution;
if isempty(doSolution), doSolution = 1:numel(Recon); end;

doLambda     = saveOptions.doLambda;
if isempty(doLambda), allLambdas = true; end;

doLambdaT    = saveOptions.doLambdaT;
if isempty(doLambdaT), allLambdaT = true; end;

doTimePoints = saveOptions.doTimePoints;
if isempty(doTimePoints), doTimePoints = 1:size(Recon{1}.LCurve_t,4); end;

errTarget    = saveOptions.errTarget;

vecToScalar  = saveOptions.convertVecToScalar;

%% Set default values
if isempty(errTarget), errTarget = 0.1; end;


%% Output Solutions

  for idxSol = doSolution  % Which Solution methods
   
    % Get the current reconstruction
    CurrentRecon = Recon{idxSol};
    
    % If we're outputting all solutions,
    if exist('allLambdas')&&allLambdas
      doLambda  = size(CurrentRecon.solutions,1);
    end;
    
    if exist('allLambdaT')&&allLambdaT
      doLambdaT = size(CurrentRecon.solutions,2);
    end;
    
    for idxTime = doTimePoints    % Which TimePoints to Save Out      
      for idxLam = doLambda;
        for idxLamT = doLambdaT;
          [Lambda LambdaT] = select_Lambdas(idxLam,idxLamT,idxTime,CurrentRecon,doByTime,errTarget);          
          % If things are valid, save out the solution
          if all((Lambda>0)&(Lambda<=size(CurrentRecon.solutions,1)))& ...
              all((LambdaT>0)&(LambdaT<=size(CurrentRecon.solutions,2)))
            % Get Filename
            disp(['Saving out Spatial Lambda #' num2str(Lambda) ' Temporal Lambda #' num2str(LambdaT) ]);
            methodstr = get_SolutionName(CurrentRecon.options);             
            fname = [saveOptions.fname '_T' num2str(idxTime) '_' methodstr '_L' num2str(Lambda) '_LT' num2str(LambdaT) '.nhdr'];
                                 
            % Convert Image to NRRD
            img   = CurrentRecon.solutions{Lambda,LambdaT}(:,idxTime);
            space = CurrentRecon.options.solutionSpace;
            tnrrd = TemplateNRRD;
            nrrdOut = convert_ImgToNrrd(img,space,tnrrd,fname,'./',vecToScalar);
                                   
            % Convert to an RGB NRRD, if requested
            if saveOptions.isRGB
              mydisp('Saving solution as RGB nrrd');
              nrrdOut = nrrdOut.convertToRGB;
            end;
            
            % Save Resulting NRRD
            nrrdOut.save;
            
          else
            mydisp('Selected a Lambda index out of range?');
            keyboard;
          end;
          %end
        end
      end;
    end;
  end;


  function [Lambda LambdaT] = select_Lambdas(idxLam,idxLamT,idxTime,CurrentRecon,doByTime,errTarget)
    % Determine what lambda values to use
    if (idxLam~=0)&&(idxLamT~=0) %Both values are specified
      Lambda  = idxLam;
      LambdaT = idxLamT;
    elseif doByTime
      if (idxLam==0)&&(idxLamT~=0)
        [devnull Lambda] = min(abs(CurrentRecon.LCurve_t(:,idxLamT,1) - errTarget));
        %tmp = abs(CurrentRecon.LCurve_t(:,1,idxLamT) - errTarget);        
        %Q = find(tmp==min(tmp));
        %Lambda = Q(1);
        LambdaT = idxLamT;
      elseif (idxLam~=0)&&(idxLamT==0)
        %tmp = abs(CurrentRecon.LCurve_t(idxLam,1,:) - errTarget);
        %Q = find(tmp==min(tmp));
        %LambdaT = Q(1);
        [devnull LambdaT] = min(abs(CurrentRecon.LCurve_t(idxLam,:,1,idxTime)-errTarget));
        Lambda = idxLam;
      else
        error('Automated selection of both lambda and lambdaT not currently supported');
      end;      
      error('Choosing regularization values by time not currently supported');
    else
      if (idxLam==0)&&(idxLamT~=0)
        %tmp = abs(CurrentRecon.LCurve(:,1,idxLamT) - errTarget);        
        %Q = find(tmp==min(tmp));
        %Lambda = Q(1);
        [devnull Lambda] = min(abs(CurrentRecon.LCurve(:,idxLamT,1)-errTarget));
        LambdaT = idxLamT;
      elseif (idxLam~=0)&&(idxLamT==0)
        %tmp = abs(CurrentRecon.LCurve(idxLam,1,:) - errTarget);
        %Q = find(tmp==min(tmp));
        %LambdaT = Q(1);
        [devnull LambdaT] = min(abs(CurrentRecon.LCurve(idxLam,:,1)-errTarget));
        Lambda = idxLam;
      else
        error('Automated selection of both lambda and lambdaT not currently supported');
      end;
    end
        