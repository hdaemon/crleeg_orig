% Quick script to update the values in mask_ibsr_fixed.nhdr
%
% Assumes that you're just saved out a new mask from Seg3D as
% mask_ibsr_fixed.nrrd
%

newmask = file_NRRD('mask_ibsr_fixed.nrrd',dir_MRI);

nrrdMask.data = newmask.data;
nrrdMask.fname = 'mask_ibsr_fixed.nhdr';
nrrdMask.fpath = dir_MRI;
nrrdMask.save;

cd(newModel.dir_Model);
system('rm subParcel1000.*');

nParcelTarget = 1000;
scriptGetSubParcellations;