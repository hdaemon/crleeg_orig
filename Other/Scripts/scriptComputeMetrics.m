%% Compute Error Metrics
for idxPatient = doPatients;
  currPatient = Patients{idxPatient}
  
  transMat = currPatient.transMat;

  currPatient.Norms = cell(size(EEG));
  
  % Loop across data sets
  for idxData = 1:numel(currPatient.DataRecons)
    
    currImgRecon = currPatient.DataRecons{idxData};
    currImgOrig  = currPatient.SimData(idxData);
              
    currPatient.Norms{idxData} = computeAllMetrics(currImgRecon,currImgOrig);
    
  end; % idxData
  mydisp(['Completed computing error metrics for patient ' num2str(idxPatient)]);
  
end; %idxPatients
