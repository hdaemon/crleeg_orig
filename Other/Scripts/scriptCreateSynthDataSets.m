%% Commented Code is retained to demonstrate what needs to be set prior 
%% to calling this script.
%
% %% Read EEG Data And Match to Electrodes
% opts.model = newModel;
% opts.bad   = file_BAD('T7418-export.bad','../eeg');
% dataEEG = cnlEEGData(file_EDF('T7418-export.edf','../eeg'),opts);
% 
% %% Read in the IBSR Parcellation
% newModel.headData.nrrdIBSR = cnlParcellation('parcel_ibsr_crl.nhdr',...
%   [newModel.dir_Root '/sourceanalysis/data_for_sourceanalysis']);
% 
% nrrdMappedIBSR = newModel.headData.nrrdIBSR.mapToSegmentation(...
%   newModel.nrrdModelSeg,'ibsr','mappedIBSR.nhdr',newModel.dir_Model);
% nrrdMappedIBSR.DownSample(newModel.totalDownSample);
% 
% nrrdMask = file_NRRD('mask_ibsr.nhdr',newModel.dir_Model);
% nrrdMask.DownSample(newModel.totalDownSample);
% nrrdMappedIBSR.data = nrrdMappedIBSR.data.*nrrdMask.data;
% 
% %% Generate some simulated data sets
% regionChoices = { [66],[69 48],[49 71 78]};
% backgroundNoise = [0.01 0.02 0.05];
% measurementSNR = [ 5 10];
% 
% % Build the cortical constraint matrix
% newModel.headData.nrrdSurfNorm = file_NRRD('vec_CortOrient_crl.nhdr', ...     
%      [newModel.dir_Root '/sourceanalysis/data_for_sourceanalysis']);
% newModel.LeadField.matCollapse = build_CorticalConstraintMatrix(newModel);
% 
% savedir = './';

% Use the SVD of the real data as basis functions for simulation
[U S V] = svd(dataEEG.data_reref,'econ');
Basis = U(:,1:4)'; clear U S V;
Basis = Basis.^2;
for i = 1:size(Basis,1)
  Basis(i,:) = 50*Basis(i,:)./norm(Basis(i,:));
end
%Basis = 50*Basis./max(abs(Basis(:)));
 figure(1); plot(Basis');
%Basis = logspace(0,4,100);
Basis = 25*ones(1,4)';


clear SimData
SimData = create_MultipleSimData(nrrdParcelForDataGen,newModel.LeadField,...
  regionChoices,backgroundNoise,measurementSNR,Basis,sigma);

clear EEG
for i = 1:numel(SimData)
  dataObj.data = SimData(i).data';
   dataObj.data(:,80) = 0; % NO NOISE ON THE GROUND!!! TOTALLY SCREWS THINGS
  dataObj.labels = dataEEG.currLabels;
  opts.model = newModel;
  
  EEG(i) = cnlEEGData(dataObj,opts);
end
EEG = reshape(EEG,size(SimData));
 

% keepVox = newModel.LeadField.currSolutionSpace.Voxels;
% 
% clear EEG SimData;
% newModel.LeadField.isCollapsed = true;
% for idxRegion = 1:length(regionChoices)
%   mydisp(['Running region selection choice #' num2str(idxRegion)]);
%   regions = regionChoices{idxRegion};
%   nRegions = numel(regionChoices{idxRegion});
%   TimeSeries = createSyntheticTemporalSequence(Basis,ones(1,nRegions));
%   
%   for idxBG = 1:length(backgroundNoise)    
%     bgNoise = backgroundNoise(idxBG);
%     mydisp(['Computing with background noise level ' num2str(bgNoise)]);
%     
%     [SimSet] = createSyntheticTemporalData(nrrdParcelForDataGen,regionChoices{idxRegion},...
%         TimeSeries, bgNoise,newModel.nrrdFinalSeg,...
%         newModel.LeadField,savedir, num2str(idxBG),num2str(idxRegion));
% 
%       SimSet.fullImage = SimSet.fullImage(keepVox,:);
%       SimSet.trueImage = SimSet.trueImage(keepVox,:);    
%       SimSet.solSpace  = newModel.LeadField.currSolutionSpace;
%       
%     for idxSNR = 1:length(measurementSNR)
%       SNR = measurementSNR(idxSNR);
%       mydisp(['Computing with SNR ' num2str(SNR)]);
%            
%       Scale = @(SNR,sig,noise) norm(sig(:))/norm(noise(:))*(1/10^(SNR/10));
%       noise = randn(size(SimSet.fullData));
%       noise = noise*Scale(SNR,SimSet.fullData,noise);
%       noise(80,:) = 0;
%       
%       SimData(idxBG,idxSNR,idxRegion) = SimSet;
%       
%       dataObj.data = SimSet.fullData + noise;
%       dataObj.data = dataObj.data';
%       dataObj.labels = dataEEG.currLabels;
%                   
%       opts.model = newModel;
%       EEG(idxBG,idxSNR,idxRegion) = cnlEEGData(dataObj,opts);
%       
% %       % Set EEG structure
% %       EEG(idxRegion,idxBG,idxSNR).data = ...
% %         SimSet.fullData + noise;
% %       EEG(idxRegion,idxBG,idxSNR).dirName = ...
% %         ['SimData_R' num2str(idxRegion) '_BG' num2str(round(100*bgNoise)) '_SNR' num2str(SNR)];
% %       EEG(idxRegion,idxBG,idxSNR).rereferenced = SimSet.fullData;
% %       EEG(idxRegion,idxBG,idxSNR).rerefbias = zeros(size(SimSet.fullData));
% %       EEG(idxRegion,idxBG,idxSNR).averagereference = ...
% %         zeros(size(SimSet.fullData));
%       
%     end
%   end %background Noise
% end % regionChoices