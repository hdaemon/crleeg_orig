

%% Set the IBSR parcellation and map it to the segmentation

% Load from default location if it isn't already set in the model.
%if isempty(newModel.headData.nrrdIBSR)
%newModel.headData.nrrdIBSR = cnlParcellation('parcel_ibsr_crl.nhdr',...
%  [newModel.dir_Root '/sourceanalysis/data_for_sourceanalysis'],'ibsr');
%end;

% Find the best parcellation to be 
if ~isempty(newModel.headData.nrrdIBSR)
  nrrdMappedParcellation = newModel.headData.nrrdIBSR.mapToSegmentation(...
       newModel.nrrdModelSeg,'fname','mappedIBSR.nhdr','fpath',newModel.dir_Model);
elseif ~isempty(newModel.headData.nrrdNVM)
  nrrdMappedParcellation = newModel.headData.nrrdNVM.mapToSegmentation(...
       newModel.nrrdModelSeg,'fname','mappedNVM.nhdr','fpath',newModel.dir_Model);
elseif ~isempty(newModel.headData.nrrdNMM)
  nrrdMappedParcellation = newModel.headData.nrrdNMM.mapToSegmentation(...
       newModel.nrrdModelSeg,'fname','mappedNMM.nhdr','fpath',newModel.dir_Model);
else
  error('Subparcellations require that either headData.IBSR or headData.NMM be set');
end
map = cnlParcellation.getMapping(nrrdMappedParcellation.parcelType);

% Read in the cortical mask and apply it to the mapped parcellation
if ~exist('nrrdMask','var')
nrrdMask = file_NRRD('mask_ibsr.nhdr',newModel.dir_Data);
end;

nrrdMappedParcellation.data = nrrdMappedParcellation.data.*nrrdMask.data;

% Read In Files
nrrdMappedParcellation.data(ismember(nrrdMappedParcellation.data,map.whiteLabels)) = 0;
nrrdMappedParcellation.data(ismember(nrrdMappedParcellation.data,map.subcorticalLabels)) = 0;
nrrdMappedParcellation.data(ismember(nrrdMappedParcellation.data,map.csfLabels)) = 0;     
     
% Set the cortical constraint NRRD and build the constraint matrix
newModel.headData.nrrdSurfNorm = file_NRRD('vec_CortOrient_crl.nhdr', ...     
     [newModel.dir_Root '/sourceanalysis/data_for_sourceanalysis']);
newModel.LeadField.matCollapse =  build_CorticalConstraintMatrix(newModel);

% Read in the resulting cortical constraint nrrd
nrrdCortConst = file_NRRD('cortconst_orig.nhdr',newModel.dir_Model);

if ~exist('nParcelTarget','var')
nParcelTarget = [200 1000 5000 10000 20000];
end;
clear subParcel;

for idxP = 1:length(nParcelTarget)
  fname = ['subParcel' num2str(nParcelTarget(idxP)) '.nhdr'];
  if ~exist([newModel.dir_Model fname],'file')
    mydisp('Computing new subparcellation');
    subParcel{idxP} = nrrdMappedParcellation.subparcellateByLeadfield(...
    nrrdCortConst,'hybrid',newModel.LeadField,nParcelTarget(idxP),fname);
  else
    mydisp('Using existing subparcellation');
    subParcel{idxP} = cnlParcellation(fname,newModel.dir_Model);
  end;
  nParcels = length(unique(subParcel{idxP}.data(:)))-1;
  tmp = hist(subParcel{idxP}.data(:),0:nParcels);
  subParcelSizes{idxP} = tmp(2:end);  
end;

for i = 1:numel(subParcel)
  subParcel{i}.save;
end;

% nrrdDownSmp = clone(nrrdMappedParcellation,'nrrdDownSmpSeg.nhdr',newModel.dir_Model);
% nrrdDownSmp.data(nrrdDownSmp.data>0) = 1;
% nrrdDownSmp.DownSample([4 4 4],'segmentation');
% Q = find(nrrdDownSmp.data);
% nrrdDownSmp.data(Q) = 1:length(Q);
% 
% subParcel{numel(nParcelTarget)+1} = nrrdDownSmp;

clear solSpaces
%% Set up the appropriate solution spaces
 for i = 1:(length(subParcel))
   subParcel{i} = subParcel{i}.ensureConnectedParcels;
   solSpaces{i} = cnlParcelSolutionSpace(subParcel{i},'graph','LeadField',newModel.LeadField,'nrrdVec',nrrdCortConst,'nVec',3);  
 end;
%  solSpaces{end+1} = cnlSolutionSpace(nrrdDownSmp,find(nrrdDownSmp.data));


