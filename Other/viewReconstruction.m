function viewerOut = viewReconstruction(recon,nrrdOverlay,solNum)
% VIEWRECONSTRUCTION View a Reconstruction
%
% function viewerOut = viewReconstruction(recon,nrrdOverlay,solNum)
%
% recon.solutions
% recon.options.solutionSpace
% recon.Data
%
% Written By: Damon Hyde
% Last Edited: Oct 28, 2015


% 
% p = inputParser;
% p.addOptional('nrrdOverlay',[],@(x)isa(x,'file_NRRD')||isa(x,'function_handle'));
% p.addParamValue('showSol',1,@(x) isnumeric(x));


if ~exist('solNum','var'), solNum = 1; end;
if ~exist('nrrdOverlay','var'), nrrdOverlay = []; end;
if exist('nrrdOverlay','var')&&isnumeric(nrrdOverlay)&&(numel(nrrdOverlay)==1)
  solNum = nrrdOverlay;
end;

mydisp(['Displaying solution number ' num2str(solNum)]);
% Get the full solution on the 

viewerOut.leftMat = recon.solutions{solNum};
if ~isfield(recon,'rightmatrix')
  viewerOut.rightMat = speye(size(viewerOut.leftMat,2));
else
  viewerOut.rightMat = recon.rightmatrix;
end;

% if ~isfield(recon,'rightmatrix')
%   viewerOut.fullSol = recon.solutions{solNum};
% else
% viewerOut.fullSol = recon.solutions{solNum}*recon.rightmatrix;
% end;
viewerOut.solSpace = recon.options.solutionSpace;
data = recon.Data;

parentH = figure;

%% Set up the data viewer subwindow
viewerOut.dataViewer = uitools.cnlDataPlot('Parent',parentH,'data',data');
viewerOut.dataViewer.selectedX = 1;
%viewerOut.dataViewer.fhnd_UpdateSelectedX = @()viewerOut.viewer.updateImage;
viewerOut.dataViewer.fhnd_UpdateSelectedX = @()updateCurrImg;
viewerOut.dataViewer.units = 'normalized';
viewerOut.dataViewer.position = [0.01 0.01 0.48 0.98];

% Get the initial value for currImg
viewerOut.currImg = viewerOut.solSpace.matGridToSolSpace'*...
                    (viewerOut.leftMat*viewerOut.rightMat(:,1));
viewerOut.currImg = full(viewerOut.currImg);

%% Set up the image viewer subwindow
viewerOut.viewer = uitools.cnlImageViewer(viewerOut.solSpace.sizes,...
                                'Parent',parentH,'Size', 0.75*[530 700]);
viewerOut.viewer.title = 'foo';
viewerOut.viewer.sliceImg.imgRange = @()getImageRange(...
                                  viewerOut.viewer.colorControl.currType);
viewerOut.viewer.fhnd_getImgSlice = @(a,b)getSlice(a,b);

% Set default aspect ratio. This should really get moved into the
% cnlImageViewer code.
tmp = viewerOut.solSpace.directions;
tmp = diag(tmp'*tmp);
viewerOut.viewer.aspectRatio = tmp; 

  if isa(nrrdOverlay,'file_NRRD')
    viewerOut.viewer.fhnd_getOverlaySlice = @(a,b)nrrdOverlay.getSlice...
                              ('axis',a,'slice',b);%,...
                               %'type',viewer.overlayColor.currType,...
                               %'cmap',viewer.overlayColor.currCMap);
    viewerOut.viewer.sliceImg.overlayRange = ...
            @()nrrdOverlay.getImageRange(viewerOut.viewer.overlayColor.currType);                           
  elseif isa(nrrdOverlay,'function_handle')
    error('Functionality not yet implemented');
    viewerOut.viewer.fhnd_getOverlaySlice;
  end

viewerOut.viewer.setUILocations;
  viewerOut.viewer.units = 'normalized';
  
    
viewerOut.viewer.position = [0.51 0.01 0.48 0.98];
  
if isa(nrrdOverlay,'file_NRRD')
parentH.Position(3:4) = [1500 900];  
  else
parentH.Position(3:4) = [1500 700];
end;

  function updateCurrImg
    % Callback from the data viewer subwindow.  Computes the full volume image for
    % the newly selected timepoint and updates the viewer window
    tmpSol = viewerOut.leftMat*viewerOut.rightMat(:,viewerOut.dataViewer.selectedX);
    viewerOut.currImg = full(viewerOut.solSpace.matGridToSolSpace'*tmpSol);
    viewerOut.viewer.updateImage;
  end

  function out = getSlice(axis,slice)
    tmp = reshape(viewerOut.currImg,viewerOut.solSpace.sizes);
    switch axis
      case 1, out = squeeze(tmp(slice,:,:));
      case 2, out = squeeze(tmp(:,slice,:));
      case 3, out = squeeze(tmp(:,:,slice));
    end
  end

  function out = getImageRange(type)
    allTypes = uitools.cnlColorControl.dispTypeList;
    
    idx = 0;
    found = false;
    while ~found
      idx = idx+1;
      if idx>numel(allTypes)
        error('file_NRRD:unknownType',...
          'Unknown display type');
      end
      found = strcmpi(type,allTypes(idx).name);
    end;
    
    img = feval(allTypes(idx).fPre,viewerOut.currImg);
    img2 = feval(allTypes(idx).fPost,img);
    
    out = [min(img2(:)) max(img2(:))];
    
  end
  

end
