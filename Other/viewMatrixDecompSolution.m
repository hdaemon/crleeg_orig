function viewerOut = viewMatrixDecompSolution(recon,varargin)

% 
% p = inputParser;
% p.addOptional('nrrdOverlay',[],@(x)isa(x,'file_NRRD')||isa(x,'function_handle'));
% p.addParamValue('showSol',1,@(x) isnumeric(x));

p = inputParser;
p.addOptional('nrrdOverlay',[],@(x) isa(x,'file_NRRD'));
p.addOptional('solNum',1, @(x) validateattributes(x,{'numeric'},{'scalar'}));
p.addOptional('parent',[],@(x) isa(x,'matlab.ui.Figure'));
p.parse(varargin{:});

% if ~exist('solNum','var'), solNum = 1; end;
% if ~exist('nrrdOverlay','var'), nrrdOverlay = []; end;
% if exist('nrrdOverlay','var')&&isnumeric(nrrdOverlay)&&(numel(nrrdOverlay)==1)
%   solNum = nrrdOverlay;
% end;

nrrdOverlay = p.Results.nrrdOverlay;
solNum = p.Results.solNum;
parentH = p.Results.parent;

mydisp(['Displaying solution number ' num2str(solNum)]);
% Get the full solution on the 

viewerOut.solSpace = recon.options.solutionSpace;

viewerOut.leftMat = recon.solutions{solNum};
if ~isfield(recon,'rightmatrix')
  viewerOut.rightMat = speye(size(viewerOut.leftMat,2));
else
  viewerOut.rightMat = recon.rightmatrix;
end;

if size(viewerOut.leftMat,1)==3*size(viewerOut.solSpace.matGridToSolSpace,1)
  tmp = viewerOut.leftMat;
  tmp = reshape(tmp,3,[],size(tmp,2));
  tmp = squeeze(sqrt(sum(tmp.^2,1)));
  viewerOut.leftMat = tmp;
end


% if ~isfield(recon,'rightmatrix')
%   viewerOut.fullSol = recon.solutions{solNum};
% else
% viewerOut.fullSol = recon.solutions{solNum}*recon.rightmatrix;
% end;

data = recon.Data;

if isempty(parentH), parentH = figure; else, clf; end;
set(parentH,'Position',[1000 100 2400 800]);

%% Set up the data viewer subwindow
viewerOut.dataViewer = uitools.plots.dataexplorer(data','Parent',parentH);
viewerOut.dataViewer.units = 'normalized';
viewerOut.dataViewer.position = [0.01 0.01 0.24 0.98];
viewerOut.dataViewer.externalSync = true;

viewerOut.listenTo{1} = addlistener(viewerOut.dataViewer,'updatedOut',@(h,evt)updateCurrImg);
viewerOut.listenTo{1}.Recursive = true;

% Get the initial value for currImg
viewerOut.currImg = viewerOut.solSpace.matGridToSolSpace'*...
                    (viewerOut.leftMat*viewerOut.rightMat(:,1));
viewerOut.currImg = full(viewerOut.currImg);
viewerOut.currImg = reshape(viewerOut.currImg,viewerOut.solSpace.sizes);

viewerOut.currVol = uitools.datatype.vol3D(viewerOut.currImg,...
        'aspect',viewerOut.solSpace.aspect,...
        'name','Reconstruction');
      
      
if ~isempty(nrrdOverlay)      
overlayVol = uitools.datatype.vol3D(nrrdOverlay.data,...
  'aspect',nrrdOverlay.aspect,...
  'name', 'Overlay');
else
  overlayVol = [];
end;

allVols = [viewerOut.currVol overlayVol];

%% Set up the image viewer subwindow
for i = 1:3
  viewerOut.viewer(i) = uitools.render.volStack3DSliced(...
    [allVols]);
  viewerOut.viewer(i).render(...
    'Parent',parentH,...
    'Units','normalized',...
    'origin',[0.25*i 0.02],...
    'size',[0.24 0.96]);
end

for i = 1:numel(viewerOut.viewer(1).volumes)
viewerOut.viewer(2).volumes(i).colormap = viewerOut.viewer(1).volumes(i).colormap;
viewerOut.viewer(3).volumes(i).colormap = viewerOut.viewer(1).volumes(i).colormap;

viewerOut.viewer(2).volumes(i).pipeline = viewerOut.viewer(1).volumes(i).pipeline;
viewerOut.viewer(3).volumes(i).pipeline = viewerOut.viewer(1).volumes(i).pipeline;

% Update the listeners
viewerOut.viewer(2).volumes(i).listenTo{1} = addlistener(...
        viewerOut.viewer(1).volumes(i).colormap,'updatedOut',...
        @(h,evt) notify(viewerOut.viewer(2).volumes(i),'updatedOut'));
viewerOut.viewer(3).volumes(i).listenTo{1} = addlistener(...
        viewerOut.viewer(1).volumes(i).colormap,'updatedOut',...
        @(h,evt) notify(viewerOut.viewer(3).volumes(i),'updatedOut'));      

end;

viewerOut.viewer(1).volumes(1).pipeline{1}.functionType = 'abs';
viewerOut.viewer(1).volumes(1).colormap.transparentZero;
viewerOut.viewer(1).volumes(2).colormap.type = 'gray';

viewerOut.viewer(1).axis = 1;
viewerOut.viewer(2).axis = 2;
viewerOut.viewer(3).axis = 3;


  function updateCurrImg
    % Callback from the data viewer subwindow.  Computes the full volume image for
    % the newly selected timepoint and updates the viewer window    
    tmpSol = viewerOut.leftMat*viewerOut.rightMat(:,viewerOut.dataViewer.currIdx);
    tmpVol = (viewerOut.solSpace.matGridToSolSpace'*tmpSol);
    viewerOut.currVol.volume = reshape(tmpVol,viewerOut.solSpace.sizes);    
    drawnow;
    % Trigger the next time step.
    viewerOut.dataViewer.nextStep;
  end

end