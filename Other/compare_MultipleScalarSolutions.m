function norms = compare_MultipleScalarSolutions(img,solSpace,...
                    imgRef,solSpaceRef,threshList)
% Compare a cell array of images against a single reference.
%
% function norms = COMPARE_MULTIPLESCALARSOLUTIONS(img,solSpace,...
%                    imgRef,solSpaceRef,threshList)
%
% Given a cell array of images, each constructed on the same
% cnlSolutionSpace, computes a variety of norms 
%
% Written By: Damon Hyde
% Last Edited: June 18, 2015
% Part of the cnlEEG Project
%

% Default to no threshold.
if ~exist('threshList','var'), threshList = 0; end;

% Get number of images to compare
nImgs = numel(img);

% Do the comparisons
norms = cell(nImgs,1);
%cnlStartMatlabPool;
for idx = 1:nImgs
  disp(['Comparing image ' num2str(idx) ' of ' num2str(nImgs)]);
  norms{idx} = compare_ScalarSolutions(img{idx},solSpace,...
    imgRef,solSpaceRef,threshList);
end;

norms = reshape(norms,size(img));

end
