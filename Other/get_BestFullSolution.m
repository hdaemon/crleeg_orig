function [Sol L] = get_BestFullSolution(spacesize,voxels,Solution,timePoint,errTarget)

L = get_BestLambda(Solution,timePoint,errTarget);
Sol = Solution.solutions{L(1)}(:,timePoint);

if Solution.options.isVectorSolution
  Sol = reshape(Sol,[3 length(Sol)/3]);
  tmp = zeros([3 spacesize]);
  tmp(:,voxels) = Sol;
else
  tmp = zeros(spacesize);
  tmp(voxels) = Sol;
end;
   
Sol = tmp;
     
end