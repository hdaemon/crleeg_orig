function [EEG,ImgsOut] = simulate_CorticalPropagation(varargin)
  % Simulate cortical activations involving propagation
  %
  % function [EEG,ImgsOut] = simulate_CorticalPropagation(varargin)
  %
  % [EEG] = simulate_CorticalPropagation(lField,connGraph)
  %     lField: Leadfield matrix with constrained orientations
  %    connGraph: Connectivity graph of voxels referenced by leadfield
  %
  % [EEG,ImgsOut] =
  %     simulate_CorticalPropagation(lField,connGraph,'solSpace',solSpace)
  %     solSpace: cnlParcelSolutionSpace to project full resolution
  %                 solutions on, generating:
  %     ImgsOut: nParcel x nTime matrix 
  %
  % Written By: Damon Hyde
  % Part of the crlEEG Project
  % 2009-2017
  %

  disp('Simulating Cortical Propagation');
    
  p = inputParser;
  p.addRequired('lField',@(x) isa(x,'cnlLeadField'));
  p.addRequired('connGraph',@(x) ismatrix(x)&&(size(x,1)==size(x,2)));
  p.addParamValue('solSpace',[],@(x) isa(x,'cnlSolutionSpace'));
  p.addParamValue('pFunction',@(d,t) exp(-0.5*(d.^2)./(500*t+20)),@(x) isa(x,'function_handle'));
  p.addParamValue('seedNodes',[]);
  p.addParamValue('nSeed',1,@(x) isscalar(x));
  p.addParamValue('time',linspace(0,1,1000));
  p.parse(varargin{:});

  % Check LeadField Size
  lField = p.Results.lField;
  lField.isCollapsed = true; % Constrain directions
  
  connGraph = p.Results.connGraph;
  if size(lField,2)~=size(connGraph,1)
    error('Size(lField,2) must equal the number of nodes in the connectivity graph');
  end
    
  % Get Seed Nodes
  nNodes = size(connGraph,1);
  seedNodes = p.Results.seedNodes;
  if isempty(seedNodes)
    seedNodes = randperm(nNodes,p.Results.nSeed);      
  end;
  
  % Get Graph Connectivity
  G = graph(connGraph);

  % Get Graph Distances
  D = G.distances(seedNodes);
  
  %
  EEG = zeros(size(lField,1),numel(p.Results.time));
  ImgsOut = zeros(numel(p.Results.time),size(D,2));
  for i = 1:numel(p.Results.time)
    % Generate image for a single timepoint
    tmpImg = p.Results.pFunction(D,p.Results.time(i));
    %tmpImg(isnan(tmpImg)) = 1;   
    tmpImg = sum(tmpImg,1);
    % Compute EEG
    EEG(:,i) = lField*tmpImg';
    ImgsOut(i,:) = tmpImg;
    % Compute projected image, if requested.
    if ~isempty(p.Results.solSpace)
      
    end
  end
  
    disp('DONE');
            
end