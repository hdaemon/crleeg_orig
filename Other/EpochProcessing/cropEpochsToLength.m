function epochCell = cropEpochsToLength(epochCell, intervallength, varargin)
% function epochCell = cropEpochsToLength(epochCell, intervallength, [crop_where])
% Author: Burak Erem
% crop_where:
%   'beginning' -   interval = 1:intervallength  (DEFAULT)
%   'middle' -        interval = (1:intervallength) - ceil(intervallength/2) + ceil(epochlength/2)
%   'end' -             interval = (1:intervallength) - intervallength + epochlength
%

crop_where = 'beginning';

if(numel(varargin)>0)
  crop_where = varargin{1};
end

for i=1:numel(epochCell)
  epochlength = size(epochCell{i},2);

  switch crop_where
    case 'beginning'
    interval = 1:intervallength;
    case 'middle'
      interval = (1:intervallength) - ceil(intervallength/2) + ceil(epochlength/2);
    case 'end'
      interval = (1:intervallength) - intervallength + epochlength;
  end

  epochCell{i} = epochCell{i}(:,interval);
end
