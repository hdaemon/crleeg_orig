function EEGDataOut = create_cnlDataStructFromEDFEpochs(edffile,ev2file,epochwidth)
% 
%
% function EEGDataOut = create_cnlEEGDataFromEDFEpochs(edffile,ev2file,epochwidth)
%
% Written By: Damon Hyde
%


epochData = edffile.extract_EpochsUsingEV2(ev2file,epochwidth);

% Loop across marking types
for i = 1:numel(epochData)
  tmpData = transposeEpochCell(epochData{i});  
  [epochmat,starttimes,endtimes,epochlengths] = epochCellToMat(tmpData);
  
  dataIn.data = epochmat';
  dataIn.nEpochs = numel(tmpData);
  dataIn.epochs_start = starttimes;
  dataIn.epochs_end = endtimes;
  dataIn.labels = edffile.labels;
  
  EEGDataOut(i) = dataIn;
end;
 

end