function [epochMat, starttimes, endtimes, epochlengths] = epochCellToMat(epochCell)
% function [epochMat, starttimes, endtimes, epochlengths] = epochCellToMat(epochCell)
% Author: Burak Erem
% 

epochMat = cat(2,epochCell{:});

epochlengths = cellfun(@(val)(size(val,2)),epochCell);

starttimes=[1 cumsum(epochlengths(1:end-1))+1];
endtimes=cumsum(epochlengths);
