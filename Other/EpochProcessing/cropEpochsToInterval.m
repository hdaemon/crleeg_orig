function epochCell = cropEpochsToInterval(epochCell, interval)
% function epochCell = cropEpochsToInterval(epochCell, interval)
% Author: Burak Erem
%

for i=1:numel(epochCell)
  epochCell{i}=epochCell{i}(:,interval);
end
