function transposedEpochCell = transposeEpochCell(epochCell)
% function transposedEpochCell = transposeEpochCell(epochCell)
% Author: Burak Erem
% 

transposedEpochCell=cellfun(@transpose,epochCell,'un',0);
