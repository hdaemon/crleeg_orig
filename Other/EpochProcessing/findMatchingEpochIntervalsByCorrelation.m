function [epochCell,intervals] = findMatchingEpochIntervalsByCorrelation(epochCell, targetCorrelation)
% function epochCell = findMatchingEpochIntervalsByCorrelation(epochCell, targetCorrelation)
% Author: Burak Erem
%

% Initialize interval to the full length of each of the input epochs:
intervals = zeros(2,numel(epochCell));
for i=1:numel(epochCell)
  intervals(:,i) = [1; size(epochCell{i},2)];
end
intervals = intervals(:);

% Define the cost function:
f = @(x)(targetCorrCostFunc(epochCell, targetCorrelation, x));

% Optimize over intervals to try to achieve the target correlation pairwise between intervals:
intervals = fminsearch(f, intervals, ...
  optimset( ...
    'TolX',1e-6, ...
    'Display', 'iter', ...
    'MaxIter', prod(intervals(:)) ));

% Prepare output variables:
intervals = reshape(intervals,2,[]);

% force all intervals to be the same length:
targetintervallength=round(mean(abs(intervals(2,:)=intervals(1,:))));
intervalcenters=(intervals(2,:)-intervals(1,:))/2 + intervals(1,:);

intervals(1,:) = floor(intervalcenters-targetintervallength/2);
intervals(2,:) = ceil(intervalcenters+targetintervallength/2);

for i=1:numel(epochCell)
  intervals(1,i) = max([1,intervals(1,i)]);
  intervals(2,i) = min([intervals(2,i),size(epochCell{i},2)]);
  
  epochCell{i} = epochCell{i}(:,intervals(1,i):intervals(2,i));
end

% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % 

end

function f = targetCorrCostFunc(epochCell, targetCorrelation, intervals)
  intervals = reshape(intervals,2,[]);
  
  % force all intervals to be the same length:
  targetintervallength=round(mean(abs(intervals(2,:)=intervals(1,:))));
  intervalcenters=(intervals(2,:)-intervals(1,:))/2 + intervals(1,:);
  
  intervals(1,:) = floor(intervalcenters-targetintervallength/2);
  intervals(2,:) = ceil(intervalcenters+targetintervallength/2);
  
  epochCorrs=cell(numel(epochCell),numel(epochCell));
  for i=1:numel(epochCell)
    for j=i+1:numel(epochCell)
      epoch_i_start = max([1,intervals(1,i)]);
      epoch_j_start = max([1,intervals(1,j)]);
      epoch_i_end = min([intervals(2,i),size(epochCell{i},2)]);
      epoch_j_end = min([intervals(2,j),size(epochCell{j},2)]);
      
      epoch_i = epochCell{i}(:,epoch_i_start:epoch_i_end);
      epoch_j = epochCell{j}(:,epoch_j_start:epoch_j_end);
      
      epochCorrs{i,j}=corr(epoch_i(:),epoch_j(:));
    end
  end
  
  epochCorrs = cat(1,epochCorrs{:});
  f = norm(epochCorrs - targetCorrelation);
  
end
