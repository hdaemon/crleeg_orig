function reconOut = trimReconstructionByTargetError(reconIn, errTargets)
% TRIMRECONSTRUCTIONBYTARGETERROR
%
% function reconOut = trimReconstructionByTargetError(reconIn, errTargets)
%
% Takes the structure output doing the inverse process and removes excess 
% associated with undesireable regularization parameter values.  This is 
% achieved by setting a target error value, and finding the solutions whose
% 2-norm data fit error are closest to those values.
%
% Written By: Damon Hyde
% Last Edited: Nov 23, 2015
% Part of the cnlEEG Project
%


if ~isempty(reconIn)
  
  if isfield(reconIn,'LCurve')
    % Standard LS Solution
  reconOut = reconIn;
  reconOut.solutions = cell(length(errTargets),size(reconIn.solutions,2));
  reconOut.LCurve = zeros(length(errTargets),size(reconIn.solutions,2),2);
  reconOut.DataApprox = cell(length(errTargets),size(reconIn.solutions,2));
  %reconOut.LCurve_t = zeros(length(errTargets),size(reconIn.solutions,2),...
  %  2, size(reconIn.LCurve_t,4));
  
  for idxTempF = 1:size(reconIn.solutions,2)
    for idxTarget = 1:length(errTargets)
      [devnull keepIdx(idxTarget)] = ...
        min(abs(reconIn.LCurve(:,idxTempF,1)-errTargets(idxTarget)));
    end
    
    reconOut.solutions(:,idxTempF) = reconIn.solutions(keepIdx,idxTempF);
    reconOut.LCurve(:,idxTempF,:) = reconIn.LCurve(keepIdx,idxTempF,:);
    reconOut.DataApprox(:,idxTempF) = reconIn.DataApprox(keepIdx,idxTempF);
   % reconOut.LCurve_t  = reconIn.LCurve_t(keepIdx,idxTempF,:,:);
    
  end;
  
  elseif isfield(reconIn,'rightmatrix')
      % Matrix DecompSolutions
        
  end
  
end;

end
