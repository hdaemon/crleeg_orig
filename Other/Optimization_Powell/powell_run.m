% Testing powell, by using test10 (problem found on Edgar & Himmelblau, 1988)
%
% by Giovani Tonel (giotonel@enq.ufrgs.br)
%
% [xo,Ot,nS]=powell('test10',[-1.2, 1],0,[],[],[],[],[],300)
% 
% xo = 
%   1.0e-018 *
% 
%     0.1599
%    -0.0652
% 
% 
% Ot =  1.2742e-037
% 
% 
% nS = 87


S=@(x) 4*x(1).^2-2.*x(1).*x(2)+x(2).^2;
[xo,Ot,nS]=powell(S,[-1.2, 1],0,[],[],[],[],[],300)
% [xo,Ot,nS]=powell('test10',[-1.2, 1],0,[],[],[],[],[],300)