function StringOut = BoolToString(boolIn)

if boolIn
  StringOut = 'TRUE';
else
  StringOut = 'FALSE';
end;

return