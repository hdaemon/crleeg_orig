function [idxOut] = get_NearestNodes(x,y,z,Image,Positions)

[X Y Z] = ndgrid(x,y,z);
X = X(:); Y = Y(:); Z = Z(:);

UseNodes = find(Image>0);
Xin = X(UseNodes);
Yin = Y(UseNodes);
Zin = Z(UseNodes);

idxOut = zeros(size(Positions,1),1);
h = waitbar(0,'FOO');
for i = 1:size(Positions,1)  
  waitbar(i/size(Positions,1),h);
  dist = sqrt((Xin-Positions(i,1)).^2 + (Yin-Positions(i,2)).^2 + (Zin-Positions(i,3)).^2);
  q = find(dist==min(dist));
  if numel(q)>1,
   q = q(ceil(numel(q)*rand(1,1)));
  end;
  idxOut(i) = UseNodes(q);
end


return;