function cnlDisplayParameters(p)
% CNLDISPLAYPARAMETERS Display the parameters parsed from an inputParser
%
% Given an inputParser object, display the parameters that it parsed.
%
% Written By: Damon Hyde
% Last Edited: Nov 2015
% Part of the cnlEEG Project
%

global cnlDebug

if isempty(cnlDebug), cnlDebug = true; end;

if cnlDebug
  
  if ~isa(p,'inputParser')
    error('Input to cnlDisplayDefaults must be an inputParser');
  end
  
  names = fieldnames(p.Results);
  
  maxLength = 0;
  for i = 1:length(names)
    if length(names{i})>maxLength, maxLength = length(names{i}); end;
  end;
  
  mydisp('Parameters supplied by calling function:',[],-1,true);
  for i = 1:length(names)
    if ~ismember(names{i},p.UsingDefaults);
      localDisplay(names{i},eval(['p.Results.' names{i}]),maxLength);
    end
  end
  
  mydisp('Parameters set to defaults',[],-1,true);
  for i = 1:length(names)
    if ismember(names{i},p.UsingDefaults);
      localDisplay(names{i},eval(['p.Results.' names{i}]),maxLength);
    end
  end
  
end
end


function localDisplay(pNameIn,pVal,maxLength)
pName = blanks(maxLength);
pName(1:length(pNameIn)) = pNameIn;

if ischar(pVal)
  pValString = pVal;
elseif (numel(pVal) == 1)&&(isnumeric(pVal)||islogical(pVal))
  pValString = num2str(pVal);
elseif isempty(pVal)
  pValString = '[]';
else
  s = size(pVal);
  pValString = ['<'];
  pValString = [pValString num2str(s(1))];
  for i = 2:length(s)
    pValString = [pValString 'x' num2str(s(i))];
  end;
  pValString = [pValString '> ' class(pVal)];
end

mydisp([ pName ' : ' pValString],[],-1,true);

end