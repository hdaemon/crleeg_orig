function eListOut = readFromDAP(fname,fpath)
% function eListOut = readFromDAP(fname,fpath);
%
% Quick Helper function to parse out the electrode list from a .dap file
%
% Written By: Damon Hyde
% Last Edited: Nov 24, 2015
% Part of the cnlEEG Project
%

if ~exist('fpath','var'), fpath = './'; end;

currDIR = pwd;
cd(fpath);

fid = fopen(fname,'r');

eListOut = cell(0);

while ~feof(fid)
  currLine = fgetl(fid);
  
  if foundKeyword('LABELS START_LIST',currLine)          
    while ~foundKeyword('LABELS END_LIST',currLine)&&~feof(fid)
      currLine = fgetl(fid);
      if ~foundKeyword('LABELS END_LIST',currLine)
      eListOut{end+1} = currLine;
      end;
    end;    
  end

    if foundKeyword('LABELS_OTHERS START_LIST',currLine)          
    while ~foundKeyword('LABELS_OTHERS END_LIST',currLine)&&~feof(fid)
      currLine = fgetl(fid);
      if ~foundKeyword('LABELS_OTHERS END_LIST',currLine)
      eListOut{end+1} = currLine;
      end;
    end;    
  end
  
end

fclose(fid);
cd(currDIR);
end

function fk = foundKeyword( keyWord, cs )

csU = upper( cs );
len = length( csU );
lenKeyword = length( keyWord );

keyWordU = upper( keyWord );

fk = 0;

if ( len<lenKeyword )
  fk = 0;
  return
end

if ( strcmp( csU(1:lenKeyword),keyWordU ) )
  fk = 1;
  return
else
  fk = 0;
  return
end

end