classdef cnlMatrixHandle < handle & matlab.mixin.Copyable
  
  properties
    matrix
  end
  
  methods
    function obj = cnlMatrixHandle(matrix)
      if isnumeric(matrix)
        obj.matrix = matrix;
      else
        error('cnlMatrixHandle.matrix must be numeric');
      end;
    end;
    
    function [out] = ctranspose(in)
      out = in;
      out.matrix = out.matrix';
    end;
    
    function out = mtimes(a,b)
      if isa(a,'cnlMatrixHandle')
        C1 = a.matrix;
      else
        C1 = a;
      end;
      
      if isa(b,'cnlMatrixHandle')
        C2 = b.matrix;
      else
        C2 = b;
      end
      
      out = C1*C2;
    end
    
    function out = size(obj,dim)
      out = size(obj.matrix);
      if exist('dim')
        if (dim<=length(out))
          out = out(dim);
        else
          out = 1;
        end;
      end;
    end;
    
  end
  
end
