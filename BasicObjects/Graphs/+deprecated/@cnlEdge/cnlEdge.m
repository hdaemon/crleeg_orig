classdef cnlEdge
  properties
    edgeLabel = '';
    startNode
    endNode
  end
  
  methods
    function obj = cnlEdge(startNode,endNode,label)
      if nargin~=0
        if (numel(startNode)==1)&(numel(endNode)==1)
          if isa(startNode,'cnlNode')
            obj.startNode = startNode;
          else
            obj.startNode = cnlNode(startNode);
          end;
          
          if isa(endNode,'cnlNode')
            obj.endNode = endNode;
          else
            obj.endNode = cnlNode(endNode);
          end;
          
          if ~exist('label','var'), label = ''; end;
          obj.edgeLabel = label;
        else
          error('Array definition of cnlEdges not yet supported');
        end;
      else
        obj.startNode = cnlNode();
        obj.endNode = cnlNode();
        obj.edgeLabel = 'UndefinedEdge';
      end;
    end
            
    function out = uniqueNodeIDs(edges)
      % Return a list of all nodes used in an array of edges
      nodes = [];
      for i = 1:length(edges)
        nodes = [nodes edges(i).startNode edges(i).endNode];
      end
      out = uniqueIDs(nodes);
    end;    
    
    function out = isUnclear(edge)
      out = isUndefined(edge.startNode)|isUndefined(edge.endNode);
    end;
    
    function obj = set.edgeLabel(obj,val)
      if ischar(val)
        obj.edgeLabel = val;
      else
        error('cnlEdge.edgeLabel must be a character string');
      end;
    end
    
    function out = isUndefined(obj)
      if strcmpi(obj,'UndefinedEdge');
        out = true;
      else
        out = false;
      end;
    end;
    
  end
end