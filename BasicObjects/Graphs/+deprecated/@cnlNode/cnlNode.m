classdef cnlNode
  % classdef cnlNode
  %
  % Class for nodes in a graph.
  %
  % There is a lot of functionality here that hasn't been coded yet.
  % Automated checking for node index uniqueness, node name uniqueness,
  % etc, have not been implemented.  In the meantime, we'll just have to be
  % careful with how this class is used so that things don't screw up along
  % the way.
  
  properties
    nodeName
    nodeIdx
  end
      
  methods
    
    function obj = cnlNode(idx,name)
      
      if nargin~=0        
        if ~exist('name','var'), name = ''; end;
        
        if isa(idx,'cnlNode')
          obj = idx;
        else
          if numel(idx)~=1
            x = size(idx,1);
            y = size(idx,2);
            z = size(idx,3);
            obj(x,y,z) = cnlNode(idx(x,y,z));           
            for idxX = 1:x
              for idxY = 1:y
                for idxZ = 1:z                  
                    obj(idxX,idxY,idxZ) = cnlNode(idx(idxX,idxY,idxZ),'');                  
                end
              end
            end
          else
            obj.nodeIdx = idx;
            obj.nodeName = name;
          end
        end;      
      else
        obj.nodeName = 'UndefinedNode';
        obj.nodeIdx = [];
      end;
    end;
        
    function uniqueIDs = uniqueIDs(nodes)            
      uniqueIDs = unique(nodeList(nodes));
      uniqueIDs = uniqueIDs(:);
      uniqueIDs(isnan(uniqueIDs)) = [];
    end
    
    function nodeList = nodeList(nodes)
      nodeList = nan(size(nodes));
      for idx = 1:numel(nodes)
        if ~isempty(nodes(idx).nodeIdx)
          nodeList(idx) = nodes(idx).nodeIdx;
        end;
      end;
    end;
    
    function obj = set.nodeIdx(obj,value)
      if isnumeric(value)
        obj.nodeIdx = value;
      else
        error('nodeIdx must be numeric');
      end;
    end;
    
    function obj = seg.nodeName(obj,value)
      if isa(value,'char')
        obj.nodeName = value;
      else
        error('nodeName must be a character string');
      end;
    end;

    function out = isUndefined(obj)
      if strcmpi(obj.nodeName,'UndefinedNode');
        out = true;
      else
        out = false;
      end;
    end;
    
%     function obj = subsasgn(obj,S,val)
%       disp('Testing subsasgn for cnlNode');
%       switch S.type
%         case '()'
%         case '{}'
%         case '.'
%           eval(['obj.' S.subs ' = val']);          
%       end
%       obj = subsasgn(obj,S,val);
%       keyboard;
%     end;
    
    
  end
end
    