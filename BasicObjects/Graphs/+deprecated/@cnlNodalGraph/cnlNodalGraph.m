classdef cnlNodalGraph < cnlGraph
  %classdef cnlGraph
  %
  % Class for undirected, unweighted graphs where you want to keep track of
  % individual nodes.  Much slower to use than cnlGraph.
  
  properties
    nodes
  end
  
  methods
    function obj = cnlNodalGraph(nodes,Cmatrix)
      % Set the list of node names.  This is usually just a list of indices
      
      if ~isa(nodes,'cnlNode')
        error('nodes must be of class cnlNode');
      end
      
      if isa(Cmatrix,'cnlEdge')
        edges = Cmatrix; clear Cmatrix;
        % We've been given a list of edges, not a connectivity matrix, so
        % we need to build it first.        
        
        uniNodeIDs = uniqueIDs(nodes);
        reqNodeIDs = uniqueNodeIDs(edges);
        
        if all(ismember(reqNodeIDs,uniNodeIDs))
        % We have all the needed nodes, so let's build the connectivity
        % matrix now.
        
          Cmatrix = logical(sparse(numel(nodes),numel(nodes)));
          nList = nodeList(nodes);
          for idxE = 1:numel(edges)
              if ~isUnclear(edges(idxE))
                refX = (nList==edges(idxE).startNode.nodeIdx);
                refY = (nList==edges(idxE).endNode.nodeIdx);
                Cmatrix(refX,refY) = true;
                Cmatrix(refY,refX) = true;
              end;
          end;

        else
          keyboard;
          error(['Attempting to define edges to nodes that are not part of the graph.  Check ' ...
            'edge list.']);
        end;                             
      end;
      
      obj = obj@cnlGraph(Cmatrix);
      obj.nodes = nodes;
      
    end
    
    function obj = addEdge(obj,edge)
      error('Not yet implemented');
    end
    
    function obj = remEdge(obj,edge)
      error('Not yet implemented');
    end
    
  end
  
end

