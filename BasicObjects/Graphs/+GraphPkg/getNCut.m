function [EigVecs, EigVals] = getNCut(W,nEigVals,dataNcut)
% function [EigVecs,EigVals] = ncut(W,nEigVals,dataNcut);
% 
% Input:
%     W= symmetric similarity matrix
%     nEigVals=  number of Ncut eigenvectors computed
%     dataNcut= optional parameters
%
%     default parameters for dataNcut:
%     dataNcut.offset = 5e-1; offset in the diagonal of W
%     dataNcut.verbose = 0; 0 for verbose off mode, 1,2,3 for verbose on modes
%     dataNcut.maxiterations = 100; max number of iterations in eigensolver
%     dataNcut.eigsErrorTolerance = 1e-6; error tolerance in eigensolver
%     dataNcut.valeurMin=1e-6; % truncates any values in W less than valeurMin
% 
% Output: 
%    EigVecs= continuouse Ncut eigenvectos, size = length(W) x nEigVals
%    EigVals= Ncut eigenvalues, size = 1x nEigVals
%
% Timothee Cour, Stella Yu, Jianbo Shi, 2004.


mydisp('GraphPkg.getNCut >> Starting NCut Algorithm');

if nargin < 2
    nEigVals = 8;
end

if nargin < 3
    dataNcut.offset = 5e-1;
    dataNcut.verbose = 0;
    dataNcut.maxiterations = 300;
    dataNcut.eigsErrorTolerance = 1e-8;
    dataNcut.valeurMin=1e-6;
end

% if nargin < 3
%     dataNcut.offset = 5e-1;
%     dataNcut.verbose = 0;
%     dataNcut.maxiterations = 100;
%     dataNcut.eigsErrorTolerance = 1e-6;
%     dataNcut.valeurMin=1e-6;
% end
%keyboard;
% make W matrix sparse

% Something doesn't seem to be working right with this MEX function
mydisp('GraphPkg.getNCut >> Sparsifying W');
W = GraphPkg.sparsifyc(W,dataNcut.valeurMin);

% W = sparse(W);
% q = find(W<dataNcut.valeurMin);
% W(q) = 0;

%keyboard;
% check for matrix symmetry
if max(max(abs(W-W'))) > 1e-10 %voir (-12) 
    %disp(max(max(abs(W-W'))));
    error('W not symmetric');
end

n = size(W,1);
nEigVals = min(nEigVals,n);
offset = dataNcut.offset;

% degrees and regularization
d = sum(abs(W),2);
dr = 0.5 * (d - sum(W,2));
d = d + offset * 2;
dr = dr + offset;
W = W + spdiags(dr,0,n,n);

% Compute the random walk matrix 
Dinvsqrt = 1./sqrt(d+eps);
P = GraphPkg.spmtimesd(W,Dinvsqrt,Dinvsqrt);
clear W;

options.issym = 1;
     
if dataNcut.verbose
    options.disp = 3; 
else
    options.disp = 0; 
end
options.maxit = dataNcut.maxiterations;
options.tol = dataNcut.eigsErrorTolerance;

options.v0 = ones(size(P,1),1);
options.p = max(35,2*nEigVals); %voir
options.p = min(options.p,n);

mydisp('GraphPkg.getNCut >> Computing Eigenvectors and EigenValues');
tStart = tic;
%warning off
% [vbar,s,convergence] = eigs2(@mex_w_times_x_symmetric,size(P,1),nEigVals,'LA',options,tril(P)); 
%[vbar,s,convergence] = eigs_new(@mex_w_times_x_symmetric,size(P,1),nEigVals,'LA',options,tril(P)); 
[vbar,s,convergence] = eigs(@GraphPkg.mex_w_times_x_symmetric,size(P,1),nEigVals,'LA',options,tril(P)); 
%warning on
tElapsed = toc(tStart);
mydisp(['GraphPkg.getNCut >> Computed Eigenvectors in ' num2str(tElapsed) ' seconds']);

s = real(diag(s));
[x,y] = sort(-s); 
EigVals = -x;
vbar = vbar(:,y);
EigVecs = spdiags(Dinvsqrt,0,n,n) * vbar;

for  i=1:size(EigVecs,2)
    EigVecs(:,i) = (EigVecs(:,i) / norm(EigVecs(:,i))  )*norm(ones(n,1));
    if EigVecs(1,i)~=0
        EigVecs(:,i) = - EigVecs(:,i) * sign(EigVecs(1,i));
    end
end
