function [Ncut, NcutEigVec, NcutEigVals] = NormCut(graph,nClusters)
% [Ncut,NcutEigVec,NcutEigVals] = NormCut(graph,nClusters);
% 
% Modified from a version downloaded from
% Calls ncut to compute NcutEigenvectors and NcutEigenvalues of W with nbcluster clusters
% Then calls discretisation to discretize the NcutEigenvectors into NcutDiscrete
% Timothee Cour, Stella Yu, Jianbo Shi, 2004

mydisp('GraphPkg.NormCut >> Calling NormCut');

% Compute continuous NCut eigenvectors
[NcutEigVec, NcutEigVals] = GraphPkg.getNCut(graph,nClusters);

% Discretize
[Ncut, NcutEigVec] = GraphPkg.discretizeCut(NcutEigVec);
Ncut = full(Ncut);

mydisp('GraphPkg.NormCut >> Finished NormCut');
end