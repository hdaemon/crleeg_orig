
loc = which('GraphPkg.recompile');
[dir,~,~] = fileparts(loc);
returnDir = pwd;
cd(dir);

mex -largeArrayDims sparsifyc.cpp
mex -largeArrayDims spmtimesd.cpp
mex -largeArrayDims mex_w_times_x_symmetric.cpp

cd(returnDir);