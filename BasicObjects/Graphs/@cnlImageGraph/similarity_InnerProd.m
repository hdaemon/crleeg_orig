function [W,varargout] = similarity_Vector(cMatrix,image,varargin)
% Inner Product Weighting Function for cnlImageGraphs
%
% function W = SIMILARITY_VECTOR(cMatrix,image,varargin)
%
% Inputs:
%  cMatrix : Adjacency matrix to define connectivity in the graph
%  image   : Matrix of size nNodes X nData containing image data
%  
% Optional Parameter Value Inputs:
%  compType      : Comparison Type
%                     'innerprod' : Compute inner products
%                     'corr'      : Computer Pearson Correlations
%                   DEFAULT: innerprod
%  removeNegCorr : Flag to eliminate connectivity between nodes with 
%                   negative inner products
%                   DEFAULT: true
%  normalize     : Flag to normalize the rows of the input data
%                   DEFAULT: true
%  chunkSize     : Maximum size of computation chunks. 
%                   DEFAULT: 1000000
%
% Written By: Damon Hyde
% Last Edited: Feb 4, 2016
% Part of the cnlEEG Project
%

mydisp('Computing Weighting Using LeadField Columns');

%% Input Parsing
p = inputParser;
p.addRequired('cMatrix',  @(x) ismatrix(x)&&(size(x,1)==size(x,2)));
p.addRequired('LeadField',@(x) ismatrix(x)&&(size(x,2)==size(cMatrix,1)));
p.addParamValue('compType','innerprod');
p.addParamValue('removeNegCorr',true,@islogical);
p.addParamValue('normalize',true,@islogical);
p.addParamValue('chunkSize',1000000);
parse(p,cMatrix,LeadField);

%% Computation
[X,Y] = find(cMatrix);

% Only take the lower diagonal component
Q = X>Y;
Xred = X(Q);
Yred = Y(Q);

% There are lots of potential combinations, so the problem gets broken into
% chunks.
nVals = length(Xred);
chunkSize = p.Results.chunkSize;
nLoop = ceil(length(Xred)/chunkSize);

outX = zeros(size(Xred));
outY = zeros(size(Yred));
outV = zeros(size(Yred));

% Normalize LeadField Columns, if requested.
if p.Results.normalize
  norms = sqrt(sum(LeadField.^2,1));
  norms = repmat(norms,size(LeadField,1),1);
  LeadField = LeadField./norms;
  LeadField(norms==0) = 0;
end;

for idx = 1:nLoop
  tic
  mydisp(['Doing block ' num2str(idx)]);
  loIdx = (idx-1)*chunkSize+1;
  hiIdx = idx*chunkSize;
  
  if loIdx>nVals, loIdx = nVals; end;
  if hiIdx>nVals, hiIdx = nVals; end;
  
  Xcurr = Xred(loIdx:hiIdx);
  Ycurr = Yred(loIdx:hiIdx);
  
  matX = LeadField(:,Xcurr);
  matY = LeadField(:,Ycurr);
  Vals = sum(matX.*matY,1);
  if p.Results.removeNegCorr
    Vals(Vals<0) = 0; % Eliminate connections with negative correlation
  end;
  
  outX(loIdx:hiIdx) = Xcurr;
  outY(loIdx:hiIdx) = Ycurr;
  outV(loIdx:hiIdx) = Vals;
  mydisp(['Completed block in ' num2str(toc) ' seconds']);
end

outX2 = [outX ; outY];
outY2 = [outY ; outX];
outV2 = [outV ; outV];

out = [outX2 outY2 outV2];
out = unique(out,'rows');

W = sparse(out(:,1),out(:,2),out(:,3),size(cMatrix,1),size(cMatrix,2));

if nargout>0
  varargout{1} = out;
end;

end


% mydisp('Using image inner products to weight edges');
% [X Y] = find(graph.Cmatrix);
% dataA = image(X,:);
% dataB = image(Y,:);
% 
% innerProd = dataA.*dataB;
% innerProd = squeeze(sum(innerProd,2));
% while numel(size(innerProd))>2
%   innerProd = squeeze(sum(innerProd,2));
% end
% innerProd = sqrt(innerProd);
% W = sparse(X,Y,innerProd,size(graph,1),size(graph,2));