classdef cnlImageGraph < cnlGraph
  % function obj = cnlImageGraph(image,varargin)
  %
  % Currently, this just implements neighborhood connectivity.  This should
  % probably be extended to identify a bunch of different options for
  % connectivity/similarity.  There should probably also be functionality
  % for dealing with multidimensional data, so that there can be an array of
  % data values at each node. This is going to be necessary for doing things
  % like multimodal segmentation, similarity between cortical voxels for
  % subparcellation, etc.
  %
  % Inputs:
  %  image : An array of arbitrary dimensionality. By default,
  %             cnlImageGraph assumes that all dimensions correspond to 
  %             spatial dimensions.
  %
  % Optional Param-Value Pairs
  %
  %  imgDims  :  Define how many of the dimensions are spatial vs data. It
  %                 is assumed that the data dimensions are the first
  %                 dimensions.
  %  voxList  :  Restrict the graph to a subset of all voxels in the
  %                 image. This represents an index into the spatial
  %                 dimensions of the image.
  %  distType :  Similarity metric to use
  %                 'basic'
  %  nbrType  :  Neighborhood type to use
  %  knnVal   :  Number of nearest neighbors to return per voxel with knn
  %  degree   :  Number of steps to go when using basic neighborhood
  %
  % Written By: Damon Hyde
  % Last Edited: Feb 4, 2016
  % Part of the cnlEEG Project
  %
  
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  properties
    imSize    % Size of image
    imGrid    % Grid the voxels live on
    imVox     % List of nonzero voxels
    imVals    % Image
    refImgIntoMat
    weightType % Type of weighting applied to imVals to get the connectivity
  end
  
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  methods
    function obj = cnlImageGraph(image,varargin)     
      % Constructor for cnlImageGraph Objects
      %
      % function obj = cnlImageGraph(image,varargin) 
      %
      %
      
      mydisp('BEGIN cnlImageGraph Constructor');
      
      %% Input Parsing and Variable Setup
      p = inputParser;
      p.KeepUnmatched = true;
      p.FunctionName = 'cnlImageGraph';
      addRequired(p,'image',@isnumeric);      
      addOptional(p,'imgDims',numel(size(image)),@(x) isnumeric(x)&...
                                                  (numel(x(:))==1)& ...
                                              (x(:)<=numel(size(image))));
      addParamValue(p,'imgGrid',[]);
      addParamValue(p,'voxList', [], @(voxList) (isvector(voxList)&&(min(voxList)>0)));
      addParamValue(p,'euclidVar',1);
      addParamValue(p,'connectivity',18);
      
      % Options for the eventual call to cnlGraph.buildNeighborGraph
      % validNbrs = { 'none' 'basic' 'knn' 'innerProd' };
      % addParamValue(p,'nbrType','none',@(x) any(validatestring(x,validNbrs)));
      
      % Similarity Metric Options
      validDists = { 'basic' 'euclidean' 'innerprod' 'dipoleorientation' ...
        'leadfieldcol' 'hybrid' 'hybridretained'};
      addParamValue(p,'distType','euclidean',@(x) any(validatestring(x,validDists)));
      
      parse(p,image,varargin{:});                        
      cnlDisplayParameters(p);
                  
      imgDims      = p.Results.imgDims;
      voxList      = p.Results.voxList;
      distanceType = p.Results.distType;
      imgGrid      = p.Results.imgGrid;
                  
      %% Get Actual Image and Data Dimensions      
      imagesize = size(image);          
      datasize = 1;
      if imgDims<numel(size(image))
        datasize = imagesize(1:(end-imgDims));
      end;      
      imagesize = imagesize((end-imgDims+1):end);
      
      %% Generate an image grid, if necessary.
      % Ensure that if provided, imgGrid matches the size of the image;      
      if isempty(imgGrid),imgGrid = cnlGrid(imagesize); end;
      assert(all(imgGrid.sizes==imagesize),...
        'Passed in a cnlGrid that does not match the size of the image');      
      
      %% Reshape the Image to be of Size [nVoxels nData]      
      imageData = reshape(image,[prod(datasize) prod(imagesize)]);
      imageData = imageData';
      imageData = reshape(imageData,[prod(imagesize) datasize]);          
                               
      % Set default voxList and check that all voxels are in the image
      if isempty(voxList), voxList = 1:prod(imagesize); end;
      assert(max(voxList)<=prod(imagesize),...
                'Voxel list includes voxels outside the image');
      
      % Retain only the data we need
      imageData = imageData(voxList,:); 
     
      % Generate the initial references between image voxels and graph
      % nodes
      refImgIntoMat = zeros(prod(imagesize),1);
      refImgIntoMat(voxList) = 1:numel(voxList);      
      
      % Get spatial locations of each voxel.
      locations = imgGrid.getGridPoints;
      locations = locations(voxList,:);      
      
      %% Actually compute things
      %
      % This process could probably be reworked and streamlined somewhat.
      % The general idea is currently to get a 
                    
      % Get the initial physical connectivity
      mydisp('Computing initial connectivity');
      connectivity = getGridConnectivity(cnlGrid(imagesize),p.Results.connectivity);
      conGraph = cnlGraph(connectivity(voxList,voxList));
                
      % Get the initial weights
      mydisp('Computing initial weights');      
      W = cnlImageGraph.computeNewSimilarity(conGraph,imageData,locations,distanceType,p.Unmatched);
      conGraphW = cnlGraph(W);
      
      % Push options up to buildNeighborGraph and get the desired
      % connectivity
      mydisp('Computing desired neighborhood connectivity');
      newGraph = conGraphW.buildNeighborGraph(p.Unmatched);

      % Now that we have the desired connectivity, compute the similarity
      % metric.
      mydisp('Computing weights for new connectivity');
      W = cnlImageGraph.computeNewSimilarity(newGraph,imageData,locations,distanceType,p.Unmatched);
      
      % Finally construct the object
      obj = obj@cnlGraph(W);
      obj.imSize = imagesize;
      obj.imVox  = voxList(:);
      obj.imGrid = imgGrid;
      obj.imVals = image;
      obj.refImgIntoMat = refImgIntoMat;
            
      mydisp('END cnlImageGraph Constructor');
      
    end;
    
    function graph = mergeNodes(graph,nodelist)
      % function graph = mergeNodes(graph,nodelist)
      %
      % Overloads the cnlGraph.mergeNodes function.  When using a
      % cnlImageGraph, the nodelist to be merged denotes nodes in the
      % original image space, rather than in the graph itself.  This
      % simplifies calls
            
      mydisp('BEGIN');
      % Make sure all img nodes are in the graph somewhere
      check = all(ismember(nodelist,find(graph.refImgIntoMat)));
      
      if ~check
        error('Some nodes in nodelist are not part of the graph');
      end;
      
      % Get the references to the current rows:
      mergeRows = graph.refImgIntoMat(nodelist);
      mergeRows = unique(mergeRows);
      
      % Determine which rows we're dropping, and which we're keeping
      dropRows = mergeRows(2:end);
      keepRows = setdiff(1:graph.nNodes,dropRows);
      
      % Reference between the old row numbering and the new row numbering
      tmpIdx = zeros(1,graph.nNodes);
      tmpIdx(keepRows) = 1:length(keepRows);
      
      % Point all nodes to be merged to the one retained node
      graph.refImgIntoMat(nodelist) = mergeRows(1);
      
      % Redirect things to the new numbering      
      graph.refImgIntoMat(graph.imVox) = tmpIdx(graph.refImgIntoMat(graph.imVox));      

      % Actually collapse the rows
      graph = mergeNodes@cnlGraph(graph,mergeRows);
      mydisp('END');
    end
    
    function graph = randomLink(graph,nodelist,strength,frac)
      % function graph = randomLink(graph,nodelist,strength,frac)
      %
      % Overloaded randomLink function for cnlImageGraph class
      % Alters the functionality of cnlGraph.randomLink so that the input
      % nodelist is expected to index into the full image space, rather
      % than into the space of voxels that are part of the current graph.
      %
      warning('Why is this getting used?');
      nodelist = graph.refImgIntoMat(nodelist);      
      graph = randomLink@cnlGraph(graph,nodelist,strength,frac);
      
    end
           
  end
  
  methods (Static=true)
     W = computeNewSimilarity(graph,image,locations,distanceType,Unmatched);
     W = similarity_Dipole(cMatrix,image,locations,varargin);
     W = similarity_Euclidean(graph,image,DistCovar);
     W = similarity_InnerProd(graph,image);
     W = similarity_Vector(cMatrix,image,varargin);
  end
  
end

