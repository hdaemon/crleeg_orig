function W = similarity_Dipole(cMatrix,orientations,locations,varargin)
% Compute similarity based on location and dipole orientation
%
% function W = similarity_Dipole(cMatrix,orientations,locations)
%
% Inputs:
%   cMatrix      : Graph adjacency matrix. Size nNodes X nNodes
%   orientations : Matrix of size nNodes x nDimensions
%   locations    : Matrix of size nNodes x nDimensions
%
% Outputs:
%    W : New graph adjacency matrix.
%
% Written By: Damon Hyde
% Last Edited; March 2, 2016
% Part of the cnlEEG Project
%

mydisp('Computing weighting based on dipole orientation and relative location');

%% Input Parsing
p = inputParser;
p.FunctionName = 'cnlImageGraph.similarity_Dipole';
p.addRequired('cMatrix',      @(x) ismatrix(x) && (size(x,1)==size(x,2)));
p.addRequired('orientations', @(x) ismatrix(x) && (size(orientations,1)==size(cMatrix,1)));
p.addRequired('locations',    @(x) ismatrix(x) && (size(locations,1)==size(cMatrix,1)));
p.addParamValue('removeNegVals',true);
p.addParamValue('thetaweight',[0.75 0.75],@(x) isnumeric(x) && ( (numel(x)==1) || (numel(x)==2) ) );
parse(p,cMatrix,orientations,locations,varargin{:});

assert(size(orientations,2)==size(locations,2),...
        'Dimensionality of orientations does not match dimensionality of locations');

%% Weighting of the two angles. Should probably be symmetric.
thetaweight = p.Results.thetaweight;
if numel(thetaweight)==1, thetaweight = thetaweight*[1 1]; end;
if thetaweight(1)~=thetaweight(2),
  warning('Unbalanced angle weighting. This may produce very strange behavior');
end;
      
%% Get location connection pairs
[X,Y] = find(cMatrix);

%% Get Orientation Info from Image
v1 = orientations(X,:);
v2 = orientations(Y,:);

%% Get Location Info
locA = locations(X,:);
locB = locations(Y,:);

% Identify the plane between them
vplane = locA-locB;
dist = sqrt(sum(vplane.^2,2));
vplane = vplane./repmat(dist,1,3);

Theta1 = (abs(sum(v1.*vplane,2))); % Cosine of Angle between v1 and plane, in (0,1)
Theta2 = (abs(sum(v2.*vplane,2))); % Cosine of Angle between v2 and plane, in (0,1)
ThetaD = (sum(v1.*v2,2)); % Cosine of Angle between v1 and v2, in (-1,1)

r_v1v2 =  (ThetaD);             % in (-1,1)
r_v1vplane =  (thetaweight(1) + (Theta1)); % in (0.1,1.1)
r_v2vplane =  (thetaweight(2) + (Theta2)); % in (0.1,1.1)

r = r_v1v2.*r_v1vplane.*r_v2vplane;
%r = sqrt(r_v1vplane.*r_v2vplane);

if p.Results.removeNegVals
  r(r<0) = 0;
end;

W = sparse(X,Y,r,size(cMatrix,1),size(cMatrix,2));
W = 0.5 * (W + W'); % To ensure symmetry

mydisp('Completed computation of graph weighting');
end

%r_v1v2     = (1 + cosd(ThetaD)).^2;
%     offset = 60;
%     scale  = 0.05;
%     r_v1v2 = -atand(-scale*(180-offset)) + atand(-scale*(ThetaD-offset));
%     r_v1v2 = r_v1v2./max(r_v1v2);