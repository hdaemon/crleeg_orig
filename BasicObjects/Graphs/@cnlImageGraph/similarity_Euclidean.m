function W = similarity_Euclidean(graph,image,DistCovar)
% Euclidean Weighting Function for cnlImageGraphs
%
% function W = similarity_Euclidean(graph,DistCovar)
%
% Written By: Damon Hyde
% Last Edited: Sept 17th, 2015,
% Part of the cnlEEG Project
%

warning(['similarity_Euclidean is deprecated. Use similarity_Vector instead']);

warning(['Euclidean distance similarity metric currently isn''t '...
  'really implemented properly. ']);
[X,Y] = find(graph.Cmatrix);
dataA = image(X,:);
dataB = image(Y,:);

%     nSamp = size(image,1);
%     covarEst = (1/(nSamp-1))*(image'*image);
%     covarEst = 0.25*covarEst;

dist = dataA-dataB;
dist = dist/DistCovar;
dist = dist.*dist;
dist = squeeze(sum(dist,2));
dist = exp(-0.5*(dist));
%    dist = trace(dist);
%     while numel(size(dist))>2
%       dist = squeeze(sum(dist,2));
%     end
%    dist = sqrt(dist);
W = sparse(X,Y,dist,size(graph,1),size(graph,2));

end