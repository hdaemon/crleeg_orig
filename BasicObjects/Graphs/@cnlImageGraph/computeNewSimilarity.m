function W = computeNewSimilarity(graph,image,locations,distanceType,varargin)
% Update the connectivity matrix for a cnlImageGraph object
%
% function W = computeNewSimilarity(graph,image,locations,distanceType,Unmatched)
%
% Inputs:
%   graph         : cnlImageGraph object
%   image         : Matrix of size graph.nNodes X nData
%   locations     : Matrix of size graph.nNodes X nDimensions
%   distanceType  : Type of distance to use in computing the new similarity
%                     matrix.
%                     'basic'
%                     'innerprod'
%                     'euclidean'
%                     'dipoleorientation'
%                     'leadfieldcol'
%                     'hybrid'
%                     'hybridretained'
%
% Optional Parameter Value Inputs:
%   'LeadField' : Matrix of size nElectrodes X graph.nNodes
%
% Outputs:
%   W : Weighted adjacency matrix for the new graph
%
% Written By: Damon Hyde
% Last Edited: Feb 4, 2016
% Part of the cnlEEG Project
%

%% Input Parsing

expectedTypes = { 'basic' 'innerprod' 'euclidean' 'dipoleorientation' 'leadfieldcol' ...
             'sillymethod' 'hybrid' 'hybridretained'};
                   
p = inputParser;
p.FunctionName = 'cnlImageGraph.computeNewSimilarity';
p.KeepUnmatched = true;
p.addRequired('image',       @(x) ismatrix(x)&&(size(x,1)==graph.nNodes));
p.addRequired('locations',   @(x) ismatrix(x)&&(size(x,1)==graph.nNodes));
p.addRequired('distanceType',@(x) any(validatestring(x,expectedTypes)));
p.addParamValue('LeadField',[]);
parse(p,image,locations,distanceType,varargin{:});

LeadField = p.Results.LeadField;

%% Computation

switch lower(distanceType)
  case 'basic'
    % Don't do anything. Just use the connectivity from
    % buildNeighborGraph
    mydisp('Not doing anything to reweight connections.  Returning original connectivity matrix');
    W = graph.Cmatrix;
  case 'innerprod'
    %W = similarity_InnerProd(graph,image);
    W = cnlImageGraph.similarity_Vector(graph.Cmatrix,image,'removeNegVals',false,'normalize',false);
  case 'sillymethod'
    [X,Y] = find(graph.Cmatrix);
    dataA = image(X,:);
    dataB = image(Y,:);
    
    dist = 1./(dataA-dataB);
    
    W = sparse(X,Y,dist,size(graph,1),size(graph,2));
  case 'euclidean'
    % Compute Euclidean distance between the image values
    W = cnlImageGraph.similarity_Vector(graph.Cmatrix,image,p.Unmatched);
    %W = similarity_Euclidean(graph,image,DistCovar);
  case 'dipoleorientation'
    % Use dipole orientation and relative location.
    W = cnlImageGraph.similarity_Dipole(graph.Cmatrix,image,locations);
  case 'leadfieldcol'
    W = cnlImageGraph.similarity_Vector(graph.Cmatrix,LeadField','removeNegVals',false,'normalize',true);     
  case 'hybrid'     
    mydisp('Computing weighting using a hybrid approach of normalized leadfield columns and dipole orientations');
    Wlfield = cnlImageGraph.similarity_Vector(graph.Cmatrix,LeadField','removeNegVals',false,'normalize',true);
    %Wlfield = similarity_LeadField(graph.Cmatrix,LeadField);
    Wdipole = cnlImageGraph.similarity_Dipole(graph.Cmatrix,image,locations);
    
    W = Wlfield.*Wdipole;       
  case 'hybridretained'        
    mydisp('Computing weighting using a hybrid approach of normalized leadfield columns and dipole orientations');
    
    Wlfield = cnlImageGraph.similarity_Vector(graph.Cmatrix,LeadField','removeNegVals',false);
    %Wlfield = similarity_LeadField(graph.Cmatrix,LeadField,'removeNegCorr',false);
    Wdipole = cnlImageGraph.similarity_Dipole(graph.Cmatrix,image,locations,'removeNegVals',false);
    
    W = Wlfield.*Wdipole;       
          
end


end