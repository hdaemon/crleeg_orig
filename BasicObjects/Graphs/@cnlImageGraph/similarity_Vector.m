function [W,varargout] = similarity_Vector(cMatrix,image,varargin)
% Vector Metric Weighting Function for cnlImageGraphs
%
% function W = SIMILARITY_VECTOR(cMatrix,image,varargin)
%
% Compute similarity based as a choice of metrics applied to the data
% vectors in the image.
%
% Inputs:
%  cMatrix : Adjacency matrix to define connectivity in the graph
%  image   : Matrix of size nNodes X nData containing image data
%  
% Optional Parameter Value Inputs:
%  compType      : Comparison Type
%                     'innerprod' : Compute inner products
%                     'corr'      : Computer Pearson Correlations
%                     'distance'  : Weighted Euclidean Distance <NOT IMPLEMENTED>
%                   DEFAULT: innerprod
%  distCovar     : Covariance to apply along each image dimension. Must be
%                   either a single scalar value, or a vector with 
%                   numel(distCovar)==size(image,2)
%  removeNegVals : Flag to eliminate connectivity between nodes with 
%                   negative inner products
%                   DEFAULT: true
%  normalize     : Flag to normalize the rows of the input data
%                   DEFAULT: true
%  chunkSize     : Maximum size of computation chunks. This parameter 
%                   shouldn't need to be changed unless running on a
%                   computer with minimal memory.
%                   DEFAULT: 1000000
%
% Written By: Damon Hyde
% Last Edited: Feb 4, 2016
% Part of the cnlEEG Project
%

mydisp('Computing Vector Weighting Using Image Data');

%% Input Parsing
p = inputParser;
p.FunctionName = 'cnlImageGraph.similarity_Vector';
p.KeepUnmatched = true;
p.addRequired('cMatrix',  @(x) ismatrix(x)&&(size(x,1)==size(x,2)));
p.addRequired('image',    @(x) ismatrix(x)&&(size(x,1)==size(cMatrix,1)));
p.addParamValue('compType','innerprod');
p.addParamValue('removeNegVals',true,@islogical);
p.addParamValue('distCovar',1,@(x) isnumeric(x)&&((numel(x)==1)||(numel(x)==size(image,2))));
p.addParamValue('normalize',true,@islogical);
p.addParamValue('chunkSize',1000000);
parse(p,cMatrix,image,varargin{:});

%% Computation
[X,Y] = find(cMatrix);

% Only take the lower diagonal component
Q = X>Y;
Xred = X(Q);
Yred = Y(Q);

% There are lots of potential combinations, so the problem gets broken into
% chunks.
nVals = length(Xred);
chunkSize = p.Results.chunkSize;
nLoop = ceil(length(Xred)/chunkSize);

outX = zeros(size(Xred));
outY = zeros(size(Yred));
outV = zeros(size(Yred));

% Normalize Image Rows, if requested.
if p.Results.normalize
  norms = sqrt(sum(image.^2,2));
  norms = repmat(norms,1,size(image,2));
  image = image./norms;
  image(norms==0) = 0;
end;

% Set distance covariances
distCovar = p.Results.distCovar;
if numel(distCovar)==1, distCovar = distCovar*ones(1,size(image,2)); end;

%% Loop across chunks to get similarities for all connections
for idx = 1:nLoop
  tic
  mydisp(['Doing block ' num2str(idx)]);
  
  % Identify indices for current block
  loIdx = (idx-1)*chunkSize+1;
  hiIdx = idx*chunkSize;
  
  if loIdx>nVals, loIdx = nVals; end;
  if hiIdx>nVals, hiIdx = nVals; end;
  
  % Get A-B Pairs
  Xcurr = Xred(loIdx:hiIdx);
  Ycurr = Yred(loIdx:hiIdx);
  
  % Get Data
  matX = image(Xcurr,:);
  matY = image(Ycurr,:);

  % Compute Metric
  switch p.Results.compType
    case 'innerprod'
      Vals = sum(matX.*matY,2);
    case 'corr'
      matX = matX - repmat(mean(matX,2),1,size(matX,2));
      matY = matY - repmat(mean(matY,2),1,size(matY,2));
      Vals = sum(matX.*matY,2);
    case 'distance'      
      dist = matX-matY;
      dist = dist ./ repmat(distCovar,size(matX,1),1);
      dist = squeeze(sum(dist,2));
      Vals = exp(-p.Results.distCovar*dist);
  end;

  % Eliminate connections with negative correlation, if requested.
  if p.Results.removeNegVals
    Vals(Vals<0) = 0; 
  end;
  
  % Assign block to overall output
  outX(loIdx:hiIdx) = Xcurr;
  outY(loIdx:hiIdx) = Ycurr;
  outV(loIdx:hiIdx) = Vals;
  
  mydisp(['Completed block in ' num2str(toc) ' seconds']);
end

%% The full adjacency matrix is symmetric
outX2 = [outX ; outY];
outY2 = [outY ; outX];
outV2 = [outV ; outV];

out = [outX2 outY2 outV2];
out = unique(out,'rows');

% Build sparse adjacency matrix
W = sparse(out(:,1),out(:,2),out(:,3),size(cMatrix,1),size(cMatrix,2));

% If requested, output the matrix in sparse form
if nargout>0
  varargout{1} = out;
end;

mydisp('Completed computation of vector graph weighting');

end


% mydisp('Using image inner products to weight edges');
% [X Y] = find(graph.Cmatrix);
% dataA = image(X,:);
% dataB = image(Y,:);
% 
% innerProd = dataA.*dataB;
% innerProd = squeeze(sum(innerProd,2));
% while numel(size(innerProd))>2
%   innerProd = squeeze(sum(innerProd,2));
% end
% innerProd = sqrt(innerProd);
% W = sparse(X,Y,innerProd,size(graph,1),size(graph,2));