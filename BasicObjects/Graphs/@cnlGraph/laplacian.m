function lapMat = laplacian(graph,varargin)
  % Compute the Graph Laplacian
  %
  % function lapMat = Laplacian(graph,varargin)
  %
  % Optional Param-Value Pairs:
  %   'unweighted' : DEFAULT: FALSE
  %                    Set to true to binarize all connection weights and
  %                    return a Laplacian based on the binary adjacency
  %                    matrix.
  %   'normalized' : DEFAULT: FALSE
  %                    Set to true to return the normalized graph
  %                    Laplacian.
  %   'type'       : DEFAULT: 'sym'
  %                    'sym': 
  %                    'randwalk'
  %
  %
  % Written By: Damon Hyde
  % Last Edited: April 20, 2016
  % Part of the cnlEEG Project
  %
  
  
  %% Input Parsing
  mydisp('BEGIN Laplacian Computation','priority',5);
  p = inputParser;
  addRequired(p,'graph',@(x) isa(x,'cnlGraph'));
  addParamValue(p,'unweighted',false,@islogical);
  addParamValue(p,'normalized',false,@islogical);
  addParamValue(p,'type','sym');
  addParamValue(p,'normtype','sym');
  parse(p,graph,varargin{:});
  
  isUnweighted = p.Results.unweighted;
  isNormalized = p.Results.normalized;  
  type = p.Results.Type;
  
  %% Compute the Graph Laplacian
  if isUnweighted
    D = graph.Degree('adjacencyGraph',true);
    lapMat = graph.Degree('adjacencyGraph',true) - double(logical(graph.Cmatrix));
  else
    D = graph.Degree;
    lapMat = graph.Degree - graph.Cmatrix;
  end;
  
  %% Normalize the Laplacian, if Desired
  if isNormalized
    switch lower(type)
      case 'randwalk'
        disp('Computing Lrw');
        Dinv = inv(D);
        lapMatNorm = Dinv*lapMat;
      case 'sym'
        disp('Computing Lsym');
        tmp = sqrt(1./diag(D));
        Dsqrt = sparse(1:length(tmp),1:length(tmp),tmp);
        lapMatNorm = Dsqrt*(lapMat*Dsqrt);
    end;
  end;
  
  mydisp('END Laplacian Computation','priority', 5);
end