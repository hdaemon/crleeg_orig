function degOut = degree(graph,varargin)
  % DEGREE - Compute the degree of each graph node
  %
  % function degOut = degree(graph,varargin)
  %
  % Computes the degree of each graph node by summing across either the
  % columns ('in') or the rows ('out') of the graph adjacency matrix.
  % Outputs a vector by default, but can optionally output a matrix by
  % setting 'output' to 'matrix'
  %
  % Optional Inputs:
  %   'adjacencyGraph' : DEFAULT: FALSE
  %                       When set to TRUE, ignores the weights of the
  %                       graph edges  and only considers the existence
  %                       (or not) of an edge when computing the degree of 
  %                       each node;
  %   'direction'      : DEFAULT: 'out'
  %                       'in'  : Sum of incoming graph edges
  %                       'out' : Sum of outgoing graph edges
  %                      These two options are identical for unweighted
  %                      graphs.
  %   'output'         : DEFAULT: 'vector'
  %
  % Written By: Damon Hyde
  % Last Edited: April 20, 2016
  % Part of the cnlEEG Project
  %  
  
  %% Input Parsing
  p = inputParser;
  addParamValue(p,'adjacencyGraph',false,@islogical);
  validDirs = {'in' , 'out'};
  addParamValue(p,'direction','out',@(x) any(validatestring(x,validDirs)));
  addParamValue(p,'output','vector');
  
  parse(p,varargin{:});
  
  adjacencyGraph = p.Results.adjacencyGraph;
  direction = validatestring(p.Results.direction,validDirs);
  
  %% Determine which dimension to sum across
  if strcmpi(direction,'out'), dirIdx = 2; else dirIdx = 1; end;
  
  %% 
  if adjacencyGraph
    degVec = sum(logical(graph.Cmatrix,dirIdx));
  else
    degVec = sum(graph.Cmatrix,dirIdx);
  end;
  
  
  %% Assign the output
  switch lower(p.Results.output)
    case 'vector'
      degOut = degVec;
    case 'matrix'
      degOut = sparse(1:size(graph,1),1:size(graph,1),degVec);
    otherwise
      error('Unknown cnlGraph.degree output type'):
  end;
  
  
  %
end