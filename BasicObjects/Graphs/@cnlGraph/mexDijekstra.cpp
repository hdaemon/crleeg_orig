#include <cmath>
#include "mex.h"
#include <algorithm>
#include <vector>
#include <iostream>
#include <list>


class stackNode{
public:
  double dist;
  long idxNode;
  
  stackNode(long x, double y){
    dist = y;
    idxNode = x;
  }
  
  bool operator==(const stackNode &test){
    if (idxNode==test.idxNode){
      return true;
    } else { return false; }
  }
  
  void print(){
    std::cout << "nodeIx: " << idxNode << " at dist: " << dist << std::endl;
  }
  
  ~stackNode(){    
  }

};

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
  // Should probably put some input checking in place
  
  
  // Read weight matrix
  mwSize  nNodes  = mxGetM(prhs[0]);
  mwSize  nEdges  = mxGetNzmax(prhs[0]);
  mwIndex *rowIdx = mxGetIr(prhs[0]);
  mwIndex *jc     = mxGetJc(prhs[0]);
  double *W = mxGetPr(prhs[0]);
  
  // Read starting node
  long idxStartNode = mxGetScalar(prhs[1]);
  idxStartNode = idxStartNode - 1; // Adjust for indexing
  
  if ((idxStartNode<0)||(idxStartNode>=nNodes))
  mexErrMsgTxt("Start node outside viable range");
  
  // Read the knn parameter
  long maxNeighbors = mxGetScalar(prhs[2]);  
  
  // Initialize output
  plhs[0] = mxCreateDoubleMatrix(nNodes,1,mxREAL);
  double *dists = mxGetPr(plhs[0]);
  for (long i = 0; i<nNodes ; i++){
    dists[i] = INFINITY;
  }
  
  // Tracking our whereabouts
  bool visited[nNodes];  std::fill_n(visited,nNodes,false);
  
  // Tracking what's on the toVisit list
  bool inNodeList[nNodes]; std::fill_n(inNodeList,nNodes,false);
  
  // Create the node list
  std::list<stackNode> nodeList;
  
  // Add the startnode
  stackNode startNode(idxStartNode,0);
  nodeList.push_back(startNode);
  dists[idxStartNode] = 0;
  
  stackNode currNode(0,0), testNode(0,0);
  long idxNbrNode;
  long idxCurrNode;
  double nbrDist;
  double alt;
  std::list<stackNode>::iterator idxList = nodeList.begin();
  
  long nNeighbors = 0;
  
  while (!(nodeList.empty() | ((nNeighbors>maxNeighbors)&(maxNeighbors!=0)) ))
  {
    // Grab the index of the current top list item, the pop it off;
    currNode = *nodeList.begin();
    idxCurrNode = currNode.idxNode;
    nodeList.pop_front();
    visited[idxCurrNode] = true;
    nNeighbors++;
    
    for (mwIndex i = jc[idxCurrNode] ; i<jc[idxCurrNode+1] ; i++){     
      idxNbrNode = rowIdx[i];
      nbrDist = W[i];
      alt = dists[idxCurrNode] + nbrDist;
      
      if (alt<dists[idxNbrNode]){        
        dists[idxNbrNode] = alt;
        if (!visited[idxNbrNode]){
          stackNode testNode(idxNbrNode,alt); // build the new stack element
          
          // If it's already in the list, remove it first.
          //  (We only compared based on idxNode)
          // The IF should speed things up by not making us search through
          // the entire list each time.
          if (inNodeList[idxNbrNode]){
            nodeList.remove(testNode);
          }
          
          // Now find the right place to add it back in
          bool foundSpot = false;
          idxList = nodeList.begin();
          while (!foundSpot){
            if ((idxList==nodeList.end())||(idxList->dist>alt))
              foundSpot = true;
            else
              idxList++;
          }
          
          // Insert the node into the list at the appropriate point.
          nodeList.insert(idxList,testNode);
          
        } //if
        
      } //if
    } //for
    
  } // while()
 
  // Finally, keep only those that were one of the k-nearest neighbors.
  for (long i = 0; i<nNodes ; i++){
    if (!visited[i])
    dists[i] = INFINITY;
  }
  

  return;
}




