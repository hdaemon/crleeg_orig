function [graphOut] = buildNeighborGraph(graph,varargin)
% Construct a new graph 
%
% function [graphOut] = buildNeighborGraph(graph,varargin)
%
% Inputs:
%   graph : cnlGraph object
%
% Optional Parameter Value Inputs:
%   retType : Type of graph to return
%               'basic' : Return a binarized graph
%               'dist'  : Return a graph with distance weighted edges
%             DEFAULT: 'dist'
%   
%
% Given an input cnlGraph and variable parameters, computes a new graph
% based on the computed neighborhood connectivity.  Output connection
% weights are just the sum of the shortest path between two nodes.  Once
% the connectivity graph is established, a new set of connection weights
% should generally be computed, and the graph updated appropriately.
%
% Written By: Damon Hyde
% Last Edited: Feb 4, 2016
% Part of the cnlEEG Project
%

%% Input parsing
validReturn = {'basic' 'dist' 'none'};

p = inputParser;
p.KeepUnmatched = true; % We're just going to pass varargin on to graph.Neighbors rather than really parsing it here.
addRequired(p,'graph',@(x) isa(x,'cnlGraph'));
addParamValue(p,'retType','basic',@(x) any(validatestring(x,validReturn)));
parse(p,graph,varargin{:});

nNodes = size(graph,1);

mydisp('Building neighborhood graph');
cnlDisplayParameters(p);

%% Check if we actually need to do anything
% If we're not looking to compute new neighbors, just return the original
% graph. This is extracted from p.Unmatched because those are passed to
% graph.Neighbors, and if 'none' is selected, we want to avoid all
% unnecessary computation.
if isfield(p.Unmatched,'nbrType')&&(strcmpi(p.Unmatched.nbrType,'none'))
  graphOut = graph;
  mydisp('Requested no update to neighbors.');
  mydisp('Returning original graph');
  return;
end;
  
% Get appropriate neighbors for each node
totalNbrs = 0;
allNbrs  = cell(nNodes,1);
allDists = cell(nNodes,1);

% Compute neighbors and distances for each node in the graph
mydisp('Computing neighbors:');
tStart = tic;
parfor idxNode = 1:nNodes
  [allNbrs{idxNode}, allDists{idxNode}] = graph.nodeNeighbors(idxNode,p.Unmatched);
  totalNbrs = totalNbrs + numel(allNbrs{idxNode});  
end
mydisp(['Total time actually required: ' num2str(toc(tStart))]);

% Initialize vectors for output graph connectivity
outCol = zeros(totalNbrs,1);
outRow = zeros(totalNbrs,1);
outDist = zeros(totalNbrs,1);
idx = 0;

% Build vectors
mydisp('Building new neighbor graph');
for idxNode = 1:nNodes;
  nNew = numel(allNbrs{idxNode});
  outRow(idx+1:idx+nNew) = allNbrs{idxNode};
  outCol(idx+1:idx+nNew) = idxNode*ones(nNew,1);
  outDist(idx+1:idx+nNew) = allDists{idxNode};
  idx = idx + nNew;
end

% Get the new connectivity matrix
switch p.Results.retType
  case 'basic'  % Just return a basic connectivity graph
   newConMat = sparse(outRow,outCol,ones(size(outDist)),nNodes,nNodes);
  case 'dist'   % Return a graph weighted by total distance from the 
   newConMat = sparse(outRow,outCol,outDist,nNodes,nNodes);
end;

% Turn it into a graph
graphOut = cnlGraph(newConMat);

end

function estimateTimeRemaining(tStart,idx,finish)
  tElapsed = toc(tStart); % How long has it been
  fracDone = idx/finish; % How much have we done
  tEstRemaining = tElapsed/fracDone - tElapsed; % Estimate time remaining  
  mydisp(['Completed Node ' num2str(idx) ' of ' num2str(finish) '. Estimated time remaining: ' num2str(tEstRemaining) ' seconds'],0,true);  
end
