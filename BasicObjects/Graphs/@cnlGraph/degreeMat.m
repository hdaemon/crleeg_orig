function degMat = degreeMat(graph,varargin)
  % DEGMAT - Returns graph degree as a diagonal matrix
  %
  % Returns the degree matrix of the graph, by calling cnlGraph.degree and
  % then constructing a sparse diagonal matrix.
  %
  % Optional Inputs:
  %   'adjacencyGraph' : DEFAULT: FALSE
  %                       When set to TRUE, ignores the weights of the
  %                       graph edges  and only considers the existence
  %                       (or not) of an edge when computing the degree of 
  %                       each node;
  %   'direction'      : DEFAULT: 'out'
  %                       'in'  : Sum of incoming graph edges
  %                       'out' : Sum of outgoing graph edges
  %                      These two options are identical for unweighted
  %                      graphs.
  %
  % Written By: Damon Hyde
  % Last Edited: Nov 9, 2015;
  % Part of the cnlEEG Project
  %
  
  warning('cnlGraph.degreeMat is deprecated. Use cnlGraph.degree(''output'',''matrix'') instead');
  degMat = graph.degree(varargin{:},'output','matrix');
  
  
end