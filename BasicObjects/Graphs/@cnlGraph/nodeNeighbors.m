function [idxNbrs, dists] = nodeNeighbors(graph,nodeIdx,varargin)
% Return a list of neighbors for a specific graph node
%
% function [idxNbrs dists] = Neighbors(graph,nodeIdx,varargin)
%
% Return a list of nodes corresponding to a local neighborhood around
% nodeIdx. 
%
% NOTE: This only works for single nodes, and is a very inefficient way of
% computing neighborhoods when you want to apply it across the entire
% graph. There should hopefully be some more efficient ways of going about
% this shortly (April 2016).
%
% Inputs:
%   graph : cnlGraph objecet
%   nodeIdx : Index of node to request neighbors of
%
% Optional Parameter-Value Inputs:
%   'nbrType' : Type of neighbors to identify
%                 'none' : Just return indices of nonzero entries in the
%                           appropriate column of graph.Cmatrix
%                 'knn'  : K-Nearest Neighbors, computed by applying
%                           Dijkstra's algorithm to graph.Cmatrix
%                 'dijkstra' : Same as 'knn'
%                 'all'  : Return index to all nodes in the graph, along
%                           with current distance vector from graph.Cmatrix
%               DEFAULT: 'none'
%   'knnVal'  : Number of k-Nearest neighbors to retain
%                 DEFAULT: 10
%   'degree'  : 
%
% Written By: Damon Hyde
% Last Edited: Feb 4, 2016
% Part of the cnlEEG Project
%

%% Input parsing
expectedTypes = { 'basic' 'knn' 'dijkstra' 'none'};

p = inputParser;
%addRequired(p,'graph',@(x) isa(x,'cnlGraph')); % More testing to be added here
addRequired(p,'nodeIdx',@(x) isnumeric(x)&&(numel(x)==1));
addParamValue(p,'nbrType','none',@(x) any(validatestring(x,expectedTypes)));
addParamValue(p,'knnVal',10,@isnumeric);
addParamValue(p,'degree',1, @isnumeric);

parse(p,nodeIdx,varargin{:});

%mydisp('cnlGraph::Neighbors');
%cnlDisplayParameters(p);

degree  = p.Results.degree;
nbrType = p.Results.nbrType;
knnVal  = p.Results.knnVal;

%% Switch across neighborhood types
switch lower(nbrType)
  case 'none'
    % Just return the current connections
    dists = graph.Cmatrix(:,nodeIdx);
    idxNbrs = find(dists);
  case 'all'
    idxNbrs = 1:graph.nNodes;
  case 'basic'
    % Just find all neighbors that are within degree steps of the
    % nodeIdx
    idxNbrs = basicNeighborhood(graph,nodeIdx,degree);    
  case {'dijkstra','knn'}
    % Use Dijkstra's algorithm to compute the knn nearest neighbors.
    dists = cnlGraph.mexDijekstra(graph.Cmatrix,nodeIdx,knnVal);
    idxNbrs = find(~isinf(dists));
end

%% Set Outputs
idxNbrs = setdiff(idxNbrs,nodeIdx); % Remove the start node from the list
dists = dists(idxNbrs);

end

function idxNbrs = basicNeighborhood(graph,nodeIdx,degree)
Nbrs = nodeIdx; % We'll remove this later

% Check if we have an existing self edge
selfEdge = false;
if ismember(nodeIdx,Nbrs), selfEdge = true; end;

while degree>0
  newNbrs = find(sum(graph.Cmatrix(:,Nbrs),2));
  Nbrs = [Nbrs ; newNbrs];
  Nbrs = unique(Nbrs); % To keep this from growing out of control
  degree = degree-1;
end;

% If the node has a self edge, just return the unique list.
% Otherwise, remove the node from the list.  Both options end up with
% a sorted list.
if selfEdge
  idxNbrs = unique(Nbrs);
else
  idxNbrs = setdiff(Nbrs,nodeIdx);
end;

end