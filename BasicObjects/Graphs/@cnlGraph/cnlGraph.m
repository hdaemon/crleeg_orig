classdef cnlGraph
  % Base class for defining graph type objects
  %
  % function obj = cnlGraph(Cmatrix,varargin);
  %
  % Optional Parameter/Value Arguments:
  %     isWeighted  : DEFAULT: true
  %     isDirected  : DEFAULT: false;
  %
  % Properties:
  %   Cmatrix    : Connectivity matrix
  %   isWeighted : Flag for weighted graphs (Those with edge weights
  %                     other than 0 and 1). Unweighted graphs are
  %                     binarized, so all edges become 0 or 1.
  %                   DEFAULT: True
  %   isDirected : Flag for directed graphs (Those with nonsymmetric
  %                     connectivity graphs)
  %                   DEFAULT: False
  %
  %
  % Written By: Damon Hyde
  % Last Edited: Nov 9, 2015
  % Part of the cnlEEG Project
  %
 
    
  %properties (GetAccess = public, SetAccess = protected);
  properties
    Cmatrix
    isWeighted = true; % Assume weighted graphs
    isDirected = false;
  end
  
  properties (Dependent = true)
    nNodes;
  end
  
  methods
    function obj = cnlGraph(Cmatrix,varargin)
      % function obj = cnlGraph(Cmatrix,varargin)
      %
      %
      
      mydisp('BEGIN cnlGraph Constructor',3);
      % Input Parsing
      p = inputParser;
      addRequired(p,'Cmatrix', @(x) isnumeric(x)&&ndims(x)==2);
      addParamValue(p,'isWeighted',true ,@islogical);
      addParamValue(p,'isDirected',false,@islogical);
      parse(p,Cmatrix,varargin{:});
      
      %Cmatrix = p.Results.Cmatrix; %Probably avoiding extra computation here
      obj.isWeighted = p.Results.isWeighted;
      obj.isDirected = p.Results.isDirected;
      
      % Make sure people aren't doing something strange
      if ~obj.isDirected
        if any(any(Cmatrix-Cmatrix'))
          if any(ismember(p.UsingDefaults,'isDirected'))
            obj.isDirected = true;
          else
            error('Cmatrix must be symmetric for an undirected graph');
          end
        end
      end
      
      % If it's an unweighted matrix, reduce the matrix to unit magnitude
      % connections.  Otherwise use the values in Cmatrix to define the
      % weights
      if obj.isWeighted
        obj.Cmatrix = double(sparse(Cmatrix));
      else
        obj.Cmatrix = double(logical(sparse(CMatrix)));
      end;
      
      mydisp('END cnlGraph Constructor',3);
    end
    
    function out = size(obj,dim)
      % function out = size(obj,dim)
      %
      % Treat the size of a graph as the size of its adjacency matrix
      %
      out = size(obj.Cmatrix);
      if exist('dim','var')
        if (dim<=length(out))
          out = out(dim);
        else
          out = 1;
        end;
      end;
    end
    
    function out = get.nNodes(graph)
      % function out = get.nNodes(graph)
      %
      %
      out = size(graph.Cmatrix,1);
    end
        
    [idxNbrs, dists] = nodeNeighbors(graph,startIdx,varargin);    
    newGraph = buildNeighborGraph(graph,varargin);
    degOut = degree(graph,varargin);
    lapMat = laplacian(graph,varargin);
    
   
    
    
  end
  
  methods (Static=true)
    idxNbrs = mexDijekstra(cmatrix,startNode,maxNeighbors);
  end
  
  
  %% All Code Below This Should be Deprecated!
  methods
    function out = Neighbors(graph,varargin)
      warning('cnlGraph.Neighbors is deprecated. Use cnlGraph.nodeNeighbors instead');
      out = nodeNeighbors(graph,varargin{:});
    end;
    
    function degMat = Degree(graph,varargin)
      warning('cnlGraph.Degree is deprescated. Use cnlGraph.degree instead');
      degMat = graph.degree(varargin{:},'output','matrix');      
    end;
    
    function lapMat = Laplacian(graph,varargin)
      warning('cnlGraph.Laplacian is deprecated. Use cnlGraph.laplacian instead');
      lapMat = graph.laplacian(varargin{:});      
    end
    
    function lapMatNorm = NormLaplacian(graph,varargin)
      warning('cnlGraph.NormLaplacian is deprecated. Use cnlGraph.laplacian instead');      
      lapMatNorm = graph.Laplacian(varargin);            
    end
    
    function graph = mergeNodes(graph,nodelist)
      % function graph = mergeNodes(graph,nodelist)
      %
      %
      error('Where is cnlGraph.mergeNodes getting used?');
      mydisp('BEGIN - Merging Nodes');
      % Get self connectivity
      selfEdge = diag(graph.Cmatrix);
      selfEdge = selfEdge(nodelist);
            
      % Merge all nodes in nodelist into the first node
      colSum = sum(graph.Cmatrix(:,nodelist),2);
      rowSum = sum(graph.Cmatrix(nodelist,:),1);
            
      % If the graph is unweighted, we just want ones in the matrix
      if ~graph.isWeighted        
        colSum(find(colSum)) = 1;
        rowSum(find(rowSum)) = 1;
      end;
      
      % Only retain prexisting self edges.
      rowSum(nodelist(1)) = sum(selfEdge);      
      
      % Update the connectivity of the first (merged) node
      graph.Cmatrix(nodelist(1),:) = colSum;
      graph.Cmatrix(:,nodelist(1)) = rowSum;
      
      % Remove excess nodes
      graph.Cmatrix(nodelist(2:end),:) = [];
      graph.Cmatrix(:,nodelist(2:end)) = [];
      
      mydisp('END');
    end
    
    
    function graph = threshGraph(graph,thresh,type)
      % function graph = threshGraph(graph,thresh)
      %
      % Eliminate all graph edges whose strength is below some threshold.
      %
      error('Where is cnlGraph.threshGraph getting used?');
      mydisp('Thresholding Graph Edges',5);
      if ~exist('type','var'), type = 'gt'; end;
            
      [X Y V] = find(graph.Cmatrix);
      Q = find(V>thresh);
      X = X(Q); Y = Y(Q); V = V(Q);
      graph.Cmatrix = sparse(X,Y,V,graph.nNodes,graph.nNodes);
      mydisp('DONE',5);
    end
    
    %graph = randomLink(graph,nodelist,strength,frac);
    
    function graph = randomLink(graph,nodelist,strength,frac)
      % function graph = randomLink(graph,nodelist,strength,frac)
      %
      %  Given a cnlGraph and a list of nodes within that graph, add random
      %  links between those nodes with a preset strength.  Total number of
      %  edges added is either a fraction of the possible edges that could
      %  be added, or is a fixed number per node.
      error('Where is cnlGraph.randomLink getting used?');
      mydisp('Adding random edges',3);
      nNodes = numel(nodelist);
      
      % THIS SHOULD NOT BE HERE
      %colRef = graph.refImgIntoMat(nodelist);
      
      colRef = nodelist;
      
      if frac<1
        nPossible = 0.5*nNodes^2;
        idx = randsample(nPossible,round(frac*nPossible));
        
        % Initialize With Empty Sparse
        ToAdd = sparse(nNodes,nNodes);
        
        ToAdd(idx) = 1;
      else
        nAdd = frac*nNodes;
        
        rowAdd = repmat(1:nNodes,frac,1);
        colAdd = reshape(randsample(nNodes,nAdd,true),size(rowAdd));
        
        ToAdd = sparse(rowAdd(:),colAdd(:),ones(size(colAdd(:))),nNodes,nNodes);
      end;
      
      % Make sure its symmetric
      ToAdd = ToAdd + ToAdd';
      [x y] = find(ToAdd);
      ToAdd = sparse(x,y,ones(size(x)),nNodes,nNodes);     
      
      [Xexisting Yexisting] = find(graph.Cmatrix(colRef,colRef));
      Existing = sparse(Xexisting,Yexisting,ones(size(Xexisting)), nNodes, nNodes);
      
      ToAdd = ToAdd - speye(nNodes,nNodes);
      ToAdd = ToAdd - Existing;
      
      [x y v] = find(ToAdd);
      Q = find(v>0);
      x = x(Q); y = y(Q); v = v(Q);
      
      refX = nodelist(x); %graph.refImgIntoMat(nodelist(x));
      refY = nodelist(y); %graph.refImgIntoMat(nodelist(y));
      
      ToAdd = sparse(refX,refY,v,graph.nNodes,graph.nNodes);      
      ToAdd = strength*ToAdd;
      
      graph.Cmatrix = graph.Cmatrix + ToAdd;
      
      mydisp('Done linking',3);
    end    
  end
  
end


%