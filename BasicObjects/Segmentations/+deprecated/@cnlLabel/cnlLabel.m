classdef cnlLabel 
  properties
    value
    name
  end
  
  methods
    function obj = cnlLabel(value,name)
      if isa(value,'cnlLabel')
        obj.value = value.value;
        obj.name  = value.name;
      else
        
        if isnumeric(value)
          obj.value = value;
        else
          error('Value must be numeric');
        end;
        
        if exist('name')&ischar(name)
          obj.name = name;
        else
          obj.name = ' ';
        end                        
      end
    end
    
    function isValid = isUniqueValueList(labels)
      allValues = AllValues(labels);
      uniqueValues = unique(allValues);
      if length(allValues)==length(uniqueValues)
        isValid = true;
      else
        isValid = false;
      end;
    end;
    
    function valsOut = AllValues(labels)
      valsOut = zeros(1,length(labels));
      for i = 1:length(labels(:))
        valsOut(i) = labels(i).value;
      end
    end    
    
    function namesOut = AllNames(labels)
      namesout = cell(1,length(labels(:)));
      for i = 1:length(labels(:))
        namesOut{i} = labels(i).name;
      end;
    end
    
        
    
    function out = sort(labels)
      allVals = AllValues(labels);
      [~, idx] = sort(allVals);
      out = labels(idx);            
    end
    
    
  end
  
end

        
        
  
    