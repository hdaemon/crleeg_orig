classdef cnlLabelMap
  
  properties    
    labels = [];
  end
  
  properties (Dependent = true)
    nLabels
    allValues
  end
  
  methods
    
    function obj = cnlLabelMap(labels)
      
      if isa(labels,'cnlLabelMap')
        obj = labels;  % Hand us a cnlLabelMap, return a cnlLabelMap
      elseif ~all(isa(labels,'cnlLabel'))        
        error('All labels must be of type cnlLabel');
      end;
                 
      if isUniqueValueList(labels)
        obj.labels = labels;
      end;
    end
    
    function nLabels = get.nLabels(obj)
      nLabels = length(obj.allvalues);
    end
    
    function allValues = get.allValues(obj)
      allValues = AllValues(obj.labels);
    end;
    
    function obj = addLabel(obj,name,value)
      % If we get a value and name pair, create a new label
      if ischar(name)&&exist('value')&&isnumeric(value)
        label = cnlLabel(name,value);
      elseif isa(name,'cnlLabel')
        label = name;
      else
        error('Input to addLabel must be in the form of either addLabel(cnlLabel) or addLabel(value,name)');
      end;
      
      if isUniqueValueList([obj.labels label])
        obj.labels = [obj.labels label];
      else
        error('Attempting to add duplicate label');
      end;
      
    end
          
  end
end