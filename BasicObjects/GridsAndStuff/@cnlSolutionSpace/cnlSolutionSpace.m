classdef cnlSolutionSpace < cnlGridSpace

% classdef cnlSolutionSpace < cnlGridSpace
%
% cnlSolutionSpace is used to define a sublist of voxels on a cnlGridSpace
% where a solution should be computed.
%
% Constructor Syntax:
%   obj = cnlSolutionSpace(spaceDef,Voxels,desc);
%          spaceDef :  Either a cnlGridSpace, or a file_NRRD to identify a
%                        grid space from.
%          Voxels   :  List of voxels for the solutionSpace to be defined
%                        at.
%          desc     :  Text description of the solution space
%
% Properties:
%   Inherited:  sizes       <  cnlGridSpace < cnlGrid
%               dimension   <  cnlGridSpace < cnlGrid
%               origin      <  cnlGridSpace
%               directions  <  cnlGridSpace
%   New:    description
%           Voxels
    
  properties
    % Inherited:    
    % sizes
    % dimension
    % origin    
    % directions
    description
    Voxels    
  end
  
  properties (Dependent=true)
    nVoxels
    matGridToSolSpace
  end;
  
  properties (Hidden=true)
    matStored
  end
  
  methods
    
    function obj = cnlSolutionSpace(spaceDef,Voxels,desc)
      %
      % function obj = cnlSolutionSpace(spaceDef,Voxels,desc)
      %
      % cnlSolutionSpace constructor.
      %   Inputs:  spaceDef:  either a cnlGridSpace, or a file_NRRD to
      %                         identify a grid space from.
      %            Voxels:    List of voxels for the solutionspace to be
      %                         defined at.  Defaults to 1:prod(obj.sizes)
      %            desc:      Text description of the solution space
      %
      %
      
      obj = obj@cnlGridSpace;
      
      if nargin>0
        gridSpace = cnlSolutionSpace.parseSpaceDef(spaceDef);
        
        obj.sizes = gridSpace.sizes;
        obj.origin = gridSpace.origin;
        obj.directions = gridSpace.directions;
        
        % Parse and check voxel list
        if exist('Voxels','var')
          if (max(Voxels)>prod(obj.sizes))||(min(Voxels)<1)
            error('Trying to use solution points outside the volume');
          else
            [Voxels order] = sort(Voxels);
            if any((order(1:end-1)-order(2:end))>0)
              error('Voxel list must be sorted in ascending order');
            else
              obj.Voxels = Voxels;
            end;
          end
        else
          obj.Voxels = 1:prod(obj.sizes);
        end;
        
        if exist('desc','var')
          obj.description = desc;
        end;
        
        obj.matStored = getMatGridToSolSpace(obj);
        
      end;
    end;
    
    function inGrid = get.matGridToSolSpace(spaceIn)
      % function inGrid = get.matGridToSolSpace(spaceIn)
      %
      %
      if isempty(spaceIn.matStored)
        inGrid = getMatGridToSolSpace(spaceIn);
      else
        inGrid = spaceIn.matStored;
      end
    end
    
    function inGrid = getMatGridToSolSpace(spaceIn)
      % function inGrid = getMatGridToSolSpace(spaceIn)
      %
      %
      inGrid = speye(prod(spaceIn.sizes));
      inGrid = inGrid(spaceIn.Voxels,:);
    end
    
      
    function out = getSolutionGrid(obj)
      % function out = getSolutionGrid(obj)
      %
      %
      warning('Deprecated functionality carried over from previous object oriented code.  Use cnlSolutionSpace.getGridPoints instead')      
      keyboard;
      out = obj.getGridPoints;
    end
   
    function out = SpaceSize(obj,idx)
      % function out = SpaceSize(obj,idx)
      %
      %
      warning('Deprecated. Space size now stored in cnlSolutionSpace.sizes');
      keyboard;
      if exist('idx','var')
        out = obj.sizes(idx)
      else
        out = obj.sizes;
      end;
    end;
    
    function out = get.nVoxels(obj)
      % function out = get.nVoxels(obj)
      %
      out = length(obj.Voxels);
    end
    
%     function matOut = getNvalMapping(spaceIn,spaceOut,nVal)
%       if ~exist('nVal'), nVal = 3; end;
%       matOut = getMapGridToGrid(spaceIn,spaceOut);
%       matOut = kron(matOut,speye(nVal));
%     end
    
    matOut = getMapping(spaceIn,spaceOut);
    
    function isEqual = eq(a,b)
      assert(isa(a,'cnlSolutionSpace')&&isa(b,'cnlSolutionSpace'),...
        'Both inputs must be cnlSolutionSpace objects');
      
      isEqual = false;
      if eq@cnlGridSpace(a,b)
        if isequal(a.Voxels,b.Voxels)
          isEqual = true;
        end;
      end
      
    end
    
  end
  
  
  methods (Static=true, Access=private)
    
    function gridSpace = parseSpaceDef(spaceDef)
      % function gridSpace = parseSpaceDef(spaceDef)
      %
      if isa(spaceDef,'cnlGridSpace')
        gridSpace = cnlGridSpace(spaceDef);
      elseif isa(spaceDef,'file_NRRD')     
        gridSpace = spaceDef.gridSpace;
        %gridSpace = cnlGridSpace(spaceDef.sizes(spaceDef.domainDims), ...
        %                         spaceDef.spaceorigin,spaceDef.spacedirections);        
      else
        error(['Not sure how to extract space information from a ' class(spaceDef) ' object.  Use a cnlGridSpace or file_NRRD object instead']);        
      end
    end
            
  end
  
%   methods (Static = true)
%     function out = loadobj(obj)
%       % function out = loadobj(obj)
%       %
%       if isstruct(obj)
%         disp('Loading cnlSolutionSpace as a structure');
%         spaceDef = cnlGridSpace(obj.sizes,obj.origin,obj.directions);
%         solutionSpace = cnlSolutionSpace(spaceDef,obj.Voxels,obj.description);
%         solutionSpace.idxBy = obj.idxBy;
%         out = solutionSpace;
%       else
%         disp('Loading cnlSolutionSpace as an object');
%         out = obj;        
%       end;
%     end
%   end;
  
end