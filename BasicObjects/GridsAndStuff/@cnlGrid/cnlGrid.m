classdef cnlGrid
  %
  % classdef CNLGRID
  %
  % Class used to define a 1D, 2D, or 3D grid.
  %
  % All three grid sizes are ultimately represented in 3D space, with the
  % values for the second and third dimension set to either 1 or zero,
  % depending on how it's indexed.
  %
  % Constructor Syntax:
  %    obj = CNLGRID(sizes);
  %
  % Properties:
  %        sizes: 
  %        idxBy:  
  %    dimension: 
  %    
  % Methods:
  %     ptsOut = getGridPoints(grid,idx)
  %     mapOut = getMapGridToGrid(gridIn,gridOut,mapType);
  %  varargout = getGridConnectivity(grid,conSize);
  %
  % Written By: Damon Hyde
  % Last Edited: July 24, 2015
  % Part of the cnlEEG Project
  %
  
  properties    
    sizes
    idxBy = IndexType.startatOne;
  end
  
  properties (Dependent = true)
    dimension
  end
  
  methods    
    function obj = cnlGrid(sizes)
      if nargin>0
        
        
      if isa(sizes,'cnlGrid')
        obj.sizes = sizes.sizes;
        obj.idxBy = sizes.idxBy;
      else
        obj.sizes = sizes;        
      end;
      end;
    end
      
    function dim = get.dimension(grid)
      dim = length(grid.sizes);
    end
    
    function isEqual = eq(a,b)
      assert(isa(a,'cnlGrid')&&isa(b,'cnlGrid'),...
        'Both inputs must be cnlGrid objects');
      
      isEqual = false;
      if ( (numel(a.sizes)==numel(b.sizes)) && all(a.sizes==b.sizes) )
        if ( a.idxBy == b.idxBy )
          isEqual = true;
        end;
      end;      
        
    end
        
    out = resample(obj,resamplelevel)
    
    ptsOut = getGridPoints(grid,idx)
    
    % For obtaining a map between two grids
    mapOut = getMapGridToGrid(gridIn,gridOut,mapType);
    
    % To obtain the grid connectivity matrix, assuming adjacency only in
    % the X, Y, and Z directions
    varargout = getGridConnectivity(grid,conSize);    
    
  end
  
  methods (Static = true)    
    mapOut = getGridMap_Tent(gridIn,gridOut);
  end
  
end
