function out = resample(obj,resamplelevel)
% Generate a new grid with a different number of samples
%
% cnlGrid.RESAMPLE
%
% function out = RESAMPLE(obj,resamplelevel)
%
% Returns a cnlGrid
%
% Written By: Damon Hyde
% Last Edited: Aug 14, 2015
% Part of the cnlEEG Project
%

if numel(resampleLevel)==1, resampleLevel = resampleLevel*ones(1,obj.dimension); end;

if numel(resampleLevel)~=obj.dimension,
  error('resampleLevel must be either a single scalar or of the same length as the dimension of the grid');
end;


if all(resampleLevel<0.5*min(obj.sizes))
 newSizes = round(obj.sizes.*resampleLevel);
else
 newSizes = resampleLevel;
end;
out = cnlGrid(newSizes);
end