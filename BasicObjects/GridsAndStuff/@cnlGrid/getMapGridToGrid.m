function mapOut = getMapGridToGrid(gridIn,gridOut,mapType)
% function mapOut = getMapGridToGrid(gridIn,gridOut)
%
%


if ~(isa(gridIn,'cnlGrid')&isa(gridOut,'cnlGrid'))
  error('gridIn and gridOut must be of class cnlGrid');
end

if ~exist('mapType','var'), mapType = 'tent'; end;

switch lower(mapType)
  case 'tent'
mapOut = cnlGrid.getGridMap_Tent(gridIn.sizes,gridOut.sizes);
  case 'nearest'
end

end