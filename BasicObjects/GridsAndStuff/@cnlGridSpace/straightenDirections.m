function obj = straightenDirections(obj)
% function obj = straightenDirections(obj)
%
%
tmp = obj.directions;
tmp = tmp'*tmp;
tmp = diag(sqrt(diag(tmp)));

obj.directions = tmp;
end