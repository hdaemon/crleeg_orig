classdef cnlGridSpace < cnlGrid
% classdef cnlGridSpace < cnlGrid
%
% Class defining a cnlGrid embedded in three dimensional space.
% cnlGridSpace is used to extend cnlGrid to 3D voxelized volumes embedded
% in real space.  If a 1D or 2D space is defined, it is by default embedded
% in a fully 3D space, and values for the origin and directions properties
% will be appropriately buffered with zeros if necessary
% 
% Constructor Syntax:
%   obj = cnlGridSpace(sizes,origin,directions);
%          sizes      : 1xN    vector of integer sizes
%          origin     : 1x3    vector defining space origin
%          directions : 3xNdim matrix of length 3 vectors defining the space
%
% origin and directions are optional parameters.
%
% Properties:
%   Inherited:  sizes       < cnlGrid
%               dimension   < cnlGrid
%         New:  origin
%               directions
%               centering = 'cell'
%
% Written By: Damon Hyde
% Last Edited: June 9, 2015
% Part of the cnlEEG Project
%
  properties
    % Inherited:
    % sizes
    % dimension
    origin     
    directions
    centering = 'cell';
  end
  
  properties (Dependent = true)
    voxSize
    boundingBox
    center
    aspect
  end
  
  methods
    
    function obj = cnlGridSpace(sizes,origin,directions,centering)
      % Constructor Syntax:
      %   obj = cnlGridSpace(sizes,origin,directions);
      %         sizes      : 1xN    vector of integer sizes
      %         origin     : 1x3    vector defining space origin
      %         directions : 3xNdim matrix of length 3 vectors defining the space
      %
      % origin and directions are optional parameters.
      
      obj = obj@cnlGrid;
      
      if nargin>0
        
        if isa(sizes,'cnlGridSpace')
          obj.sizes = sizes.sizes;
          obj.origin = sizes.origin;
          obj.directions = sizes.directions;
          obj.centering = sizes.centering;
          return
        else
          obj.sizes = sizes;
          
          if ~exist('origin','var'), origin = zeros(1,3); end;
          obj.origin = origin;
          
          if ~exist('directions','var'),
            directions = zeros(3,obj.dimension); % Always embed in 3D
            directions(1:obj.dimension,1:obj.dimension) = eye(obj.dimension);
          end;
          obj.directions = directions;
        end;
        
        if exist('centering','var')
          obj.centering = centering;
        end;
      end;
    end
    
    function out = get.aspect(obj)
      out = sqrt(sum(obj.directions.^2,1));
    end
    
    function isEqual = eq(a,b)
      assert(isa(a,'cnlGridSpace')&&isa(b,'cnlGridSpace'),...
        'Both inputs must be cnlGridSpace objects');
      
      isEqual = false;
      if eq@cnlGrid(a,b)        
        if all(abs(a.origin-b.origin)<1e-2) && ...
            all(abs(a.directions(:)-b.directions(:))<1e-2)
          isEqual = true;
        end;
      end
    end
    
    %% Methods with their own m-files
    objOut = getAlternateGrid(obj,type);
    idxOut = getNearestNodes(grid,UseNodes,Positions);
    ptsOut = getGridPoints(grid,idx);
    nodeList = getNodesFromCells(grid,cellList);
    out = resample(obj,resampleLevel);
    obj = straightenDirections(obj);

    
    
    function obj = moveOrigin(obj,newOrigin)
      % function obj = moveOrigin(obj,newOrigin)
      %
      %
            warning('moveOrigin is deprecated');
      nDimNew = size(newOrigin);
      if nDimNew == 3;
        obj.origin = newOrigin;
      end;
    end
      
    function obj = set.origin(obj,newOrigin)  
      % function obj = set.origin(obj,newOrigin)  
      %
      %
      nDimNew = numel(newOrigin);
      if nDimNew == 3;
      obj.origin = newOrigin(:)';
      else
        error('New origin has incorrect dimensionality');
      end;
    end
            
    function obj = shiftOrigin(obj,shiftVector)
      % function obj = shiftOrigin(obj,shiftVector)
      %
      %
      warning('shiftOrigin is deprecated'); % Feb 2016
      nDimNew = numel(shiftVector);
      if nDimNew == 3;
        obj.origin = obj.origin + shiftVector;
      else
        error('Origin shift vector has incorrect dimensionality');
      end
    end;
       
    function dists = get.voxSize(obj)
      % function dists = GET.VOXSIZE(obj)
      %
      % Given a cnlGridSpace object, returns the size of the voxels along
      % each axes.  Output is of size [nDimensions 1].
      dists = sqrt(sum(obj.directions.^2,1));
    end;
        
    function center = get.center(obj)
      % function center = get.center(obj)
      %
      % Returns the geometric center of the bounding box for the
      % cnlGridSpace.

      bbox = obj.boundingBox;
      center = 0.125*sum(bbox,1);
    end
    
    function obj = set.center(obj,val)
      % function obj = set.center(obj)
      %
      %
      currCenter = obj.center;
      shiftVec = val - currCenter;
      mydisp(['Shifting by' num2str(shiftVec)]);
      obj.origin = obj.origin+shiftVec;
      mydisp('FOO!!');
    end
    
    function box = get.boundingBox(obj)      
      % function box = get.boundingBox(obj)      
      %
      % 
      box = getBoundingBox(obj);
    end;

    function obj = set.directions(obj,val)
      % function obj = set.directions(obj,val)
      %
      %
      if ndims(val)==2
        if size(val,2)==obj.dimension
          obj.directions = val;
        elseif obj.dimension==0
          % Hopefully just hitting this on load.
          obj.directions = val;          
        else
          error('Directions matrix needs to be an 3 \times Ndims matrix');
        end
      else
        error('Directions matrix is not a matrix');
      end      
    end
  end
  
end
