function mapOut = getMapGridToGrid(gridIn,gridOut,mapType)

% function mapOut = getMapGridToGrid(gridIn,gridOut)
%
% Get linear mapping from one cnlGridSpace to another. 
%
% THIS NEEDS A LOT OF WORK. CURRENTLY IT JUST CHECKS TO MAKE SURE THE
% ORIGINS ARE IN THE SAME PLACE, AND THEN ASSUMES THAT THE TWO GRIDS SPAN
% THE SAME SPACE, AND INTERPOLATES FROM ONE TO ANOTHER.  
%
% This should be expanded to include both tent and nearest neighbor
% mappings

if ~(isa(gridIn,'cnlGridSpace')&isa(gridOut,'cnlGridSpace'))
  error('gridIn and gridOut must be of class cnlGridSpace');
end

if ~exist('mapType','var'), mapType = 'tent'; end;

% Check To Make Sure Bounding Boxes of Node Centered Grids Match.
boxIn  = gridIn.getAlternateGrid('node').getBoundingBox;
boxOut = gridOut.getAlternateGrid('node').getBoundingBox;


if ~( all(all(round(10*boxIn)==round(10*boxOut)))|| ...
      all(all(floor(10*boxIn)==floor(10*boxOut)))|| ...
      all(all( ceil(10*boxIn)== ceil(10*boxOut))) )
  
  error(['Bounding Boxes don''t match.  Currently maps are only available '...
         'between spaces with matching bounding boxes']);
end;

mapOut = getMapGridToGrid@cnlGrid(gridIn,gridOut,mapType);


end