classdef cnlFileList < dynamicprops
  % function obj = cnlFileList(searchDir,startPat,IDs,validExts)
  %
  % Given a directory searchDir, files all files that match the pattern:
  %     name = [startPat '_' IDs{?} '_.' validExts]
  %
  %
  % This should probably all be discarded.  
  
  %% Properties
  properties
    description
    srchDirs
    fileIDs
    startPat
    validExts
    haveIt
  end
  
  %% Methods
  methods
    function obj = cnlFileList(srchDirs,startPat,IDs,validExts,desc)
      if isa(srchDirs,'cell')
        obj.srchDirs = srchDirs;
      else
        obj.srchDirs = {srchDirs};
      end
      obj.fileIDs = IDs;
      obj.startPat = startPat;
      obj.validExts = validExts;
      obj.description = desc;
      obj.haveIt = false(length(IDs),1);
      
      % Add properties
      for i = 1:length(IDs)
        obj.addprop(IDs{i});
      end
      
      % Find files to fill these properties
      findFiles(obj);
      
    end
    
    function files = findFiles(obj)
      tmp = pwd;
      
      % Initialize file structure
      files = cell(length(obj.fileIDs),1);
      for i = 1:length(obj.fileIDs)
        files{i}.fname = '';
        files{i}.fpath  = '';
      end;
      
      for k = 1:length(obj.srchDirs)
        cd(obj.srchDirs{k});
        for i = 1:length(obj.fileIDs)          
          % See if there's a file with one of the valid extensions
          for j = 1:length(obj.validExts)
            fname = [obj.startPat obj.fileIDs{i} obj.validExts{j}];            
            if exist(fname,'file')&&isempty(files{i}.fname)              
              obj.haveIt(i) = true;
              files{i} = cnlFileList.genReader(fname,pwd);
              %  files{i}.fname = fname; %cnlFileList.genReader(fname,obj.srchDirs{k});
              %  files{i}.fpath  = pwd;
            end
          end
          
          if ~isempty(files{i}.fname)
            eval(['obj.' obj.fileIDs{i} '.fname = files{i}.fname;']);
            eval(['obj.' obj.fileIDs{i} '.fpath  = files{i}.fpath;']);
          else
            eval(['obj.' obj.fileIDs{i} ' = [];']);
          end;
        end;
      end;
      cd(tmp);
    end
    
  end
  
  %% Static Methods
  methods (Static=true)
    
    
    function obj = genReader(filename,filedir)
      [~,~,ext] = fileparts(filename);
      
      switch lower(ext);
        case {'.nhdr','.nrrd'}
          obj = file_NRRD(filename,filedir);
        otherwise
          error('Unknown file extension');
      end
    end;
    
  end
  
  %%
  
end
