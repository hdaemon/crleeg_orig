classdef cnlPatient < handle

  properties
    dir_Root = [];
    dir_EEG        = 'eeg/';
    dir_PhotoGram  = 'photogrammetry/';
    dir_Models     = 'models/';
    
    Files
    Models
    Data
    
    EEG
  end
      
  properties (Constant = true, Hidden = true)
    %% Master list of options available
    validSkinSegs  = {'CRL' 'FSL' };
    validSkullSegs = {'CRL' 'BET' 'none'};
    validICCSegs   = {'CRL' 'atlas' 'none'};
    validBrainSegs = {'CRL' 'atlas' 'none'};
    validTensors   = {'CRL' 'none'};
    validCortConst = {'CRL' 'atlas' 'none'};
    validSurfNorm  = {'CRL' 'none'};
    validFinalSeg  = {'CRL' 'none'};
    validParcels   = {'NMM' 'IBSR' 'NVM' 'none'};
  end
  
  
  methods
    function obj = cnlPatient(dir)
      obj.dir_Root = dir;
      scrapeInputs(obj);
    end;
    
    function scrapeInputs(obj)
      % function scrapeInputs(obj)
      %
      % Look through the standardized directory structure and find all the
      % available NRRD and NHDR files for a particular patient
      
      MRIDir = [obj.dirRoot '/sourceanalysis/MRI/'];
      
      % Find skin segmentations      
      desc = 'All skin segmentations';
      obj.Seg.Skin  = cnlFileList(MRIDir,'skinSeg_',obj.validSkinSegs,{'.nhdr' '.nrrd'},desc);
      
      % Find Skull Segmentations      
      desc = 'All skull segmentation';
      obj.Seg.Skull = cnlFileList(MRIDir,'skullSeg_',obj.validSkullSegs,{'.nhdr' '.nrrd'},desc);
      
      % Find ICC Segmentations      
      desc = 'All ICC segmentations';
      obj.Seg.ICC   = cnlFileList(MRIDir,'iccSeg_',obj.validICCSegs,{'.nhdr' '.nrrd'},desc);
      
      % Find Brain Segmentations      
      desc = 'All brain segmentations';
      obj.Seg.Brain = cnlFileList(MRIDir,'brainSeg_',obj.validBrainSegs,{'.nhdr' '.nrrd'},desc);
      
      % Tensor Images      
      desc = 'All Tensor Images';
      obj.Tensors  = cnlFileList(MRIDir,'tensors_',obj.validTensors,{'.nhdr' '.nrrd'},desc);
      
      % CortConst      
      desc = 'Cortical Constraint NRRDs';
      obj.CortConst = cnlFileList(MRIDir,'cortconst_',obj.validCortConst,{'.nhdr' '.nrrd'},desc);
      
      % Surface Normals      
      desc = 'Surface Normals';
      obj.SurfNorm = cnlFileList(MRIDir,'surfnorm_',obj.validSurfNorm,{'.nhdr' '.nrrd'},desc);
      
      % FinalSeg      
      desc = 'Segmentation after sulci extraction';
      obj.FinalSeg = cnlFileList(MRIDir,'finalseg_',obj.validFinalSeg,{'.nhdr' '.nrrd'},desc);
      
      % Parcellations      
      desc = 'Parcellations';
      obj.Parcels = cnlFileList(MRIDir,'parcel_',obj.validParcels,{'.nhdr' '.nrrd'}, desc);
      
      
%       EEGDir = [dirRoot '/eeg'];
%       obj.EEG = findEEGFiles(MRIDir
    end
    
    function scrapeModels(obj)
    end
    
    function allModels = allPossibleModels(obj)
%       tmp.Skin = '';
%       tmp.Skull = '';
%       tmp.ICC = '';
%       tmp.Brain = '';
%       tmp.Tensors = '';
%       tmp.dirName = '';
%       tmp.canBuild = false;
%       tmp.haveBuilt = false;
%       tmp.shouldBuild = true;
%       
%       allModels(1) = tmp;
      nModels = 0;
      for idxSkin = 1:length(obj.validSkinSegs)
        tmp.Skin = obj.validSkinSegs{idxSkin};
        dirName1 = ['skin' obj.validSkinSegs{idxSkin} '_'];
        for idxSkull = 1:length(obj.validSkullSegs);
          tmp.Skull = obj.validSkullSegs{idxSkull};
          dirName2 = [dirName1 'skull' obj.validSkullSegs{idxSkull} '_'];
          for idxICC = 1:length(obj.validICCSegs)
            tmp.ICC = obj.validICCSegs{idxICC};
            dirName3 = [dirName2 'icc' obj.validICCSegs{idxICC} '_'];
            for idxBrain = 1:length(obj.validBrainSegs)
              tmp.Brain = obj.validBrainSegs{idxBrain};
              dirName4 = [dirName3 'brain' obj.validBrainSegs{idxBrain} '_'];
              for idxTensors = 1:length(obj.validTensors)
                tmp.Tensors = obj.validTensors{idxTensors};
                dirName5 = [dirName4 'tensors' obj.validTensors{idxTensors}];
                tmp.dirName = dirName5;
                tmp.canBuild = false;
                tmp.haveBuilt = false;
                tmp.shouldBuild = true;
                nModels = nModels + 1;
                allModels(nModels) = tmp;
              end
            end
          end
        end
      end
      
    end
    
  end
  
  methods (Static = true)
                    
  end
      
end
