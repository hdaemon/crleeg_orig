classdef cnlSegmentation < file_NRRD
  
  properties
    % Set what the default segmentation labels look like.
    segVals = 1:10;
    labels = { 'background' 'null' 'skull' 'gray' 'CSF' 'null ' 'white' 'null' 'null' 'null'};
     
  end
  
  methods
    
    function obj = cnlSegmentation(fname,fpath)
      
      obj = obj@file_NRRD;
      if nargin>0
      % Default path to look in
      if ~exist('fpath','var'), fpath = './'; end;
                       
      obj.loadFromFile(fname,fpath);
      
      end;
      
    end
    
    function objOut = clone(obj,fname,fpath)
      if ~exist('fname','var'), fname = 'NewSeg.nhdr'; end;      
      if ~exist('fpath','var'), fpath = './'; end;
            
      objOut = clone@file_NRRD(obj,fname,fpath);
    end    
  end
  
end