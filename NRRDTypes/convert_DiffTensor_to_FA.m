function nrrdOut = convert_DiffTensor_to_FA(nrrdIn)

assert(nrrdIn.dimension==4,'Must be a 4D NRRD');

nrrdOut = clone(nrrdIn);
nrrdOut.dimension = 3;
nrrdOut.sizes = nrrdIn.sizes(2:end);
nrrdOut.data = zeros(nrrdOut.sizes);

for i = 1:nrrdOut.sizes(1)
  disp(['Doing slice ' num2str(i)]);
  for j = 1:nrrdOut.sizes(2)
    for k = 1:nrrdOut.sizes(3)
      
      tensorData = nrrdIn.data(:,i,j,k);
      if any(tensorData)
        diffTensor = zeros(3,3);
        diffTensor(:,1)   = tensorData(1:3);
        diffTensor(1,:)   = tensorData(1:3)';
        diffTensor(2:3,2) = tensorData(4:5);
        diffTensor(2,2:3) = tensorData(4:5)';
        diffTensor(3,3)   = tensorData(6);
        
        [eigVec eigVal] = eig(diffTensor);
        
        eigVal = diag(eigVal);
        if ~isreal(eigVal), keyboard; end;
        
        meanVal = mean(eigVal);
        normVal = norm(eigVal);
        
        FA = sqrt(3/2)*norm(eigVal-meanVal)/normVal;
        
        nrrdOut.data(i,j,k) = FA;
        
      end
    end
  end
end

end