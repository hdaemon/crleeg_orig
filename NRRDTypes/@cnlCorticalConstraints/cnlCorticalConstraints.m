classdef cnlCorticalConstraints < file_NRRD

  properties
  end
  
  methods
    
    function obj = cnlCorticalConstraints(fname,fpath)
      obj = obj@file_NRRD(fname,fpath);
    end
    
  end % METHODS
  
end % CLASSDEF
    