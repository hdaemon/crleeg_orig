classdef cnlParcellation < file_NRRD
  % Extension of file_NRRD for parcellations
  %
  % classdef cnlParcellation < file_NRRD
  %
  % Constructor Syntax:
  %   function obj = cnlParcellation(fname,fpath,parcelType,readOnly)
  %
  % Properties:
  %
  % Written By: Damon Hyde
  % Last Edited: Aug 12, 2015
  % Part of the cnlEEG Project
  %
  
  properties
    parcelType = 'ibsr';
  end % PROPERTIES
  
  properties (Dependent=true)
    nParcel
  end
  
  properties (Hidden = true, Constant)
    validParcels = {'nmm' 'ibsr' 'nvm' 'none'};
  end
  
  methods
    
    function obj = cnlParcellation(fname,fpath,parcelType,readOnly)
      
      if ~exist('fname','var'), fname = []; end;
      if ~exist('fpath','var'), fpath = []; end;
      if ~exist('parcelType','var'), parcelType = 'none'; end;
      if ~exist('readOnly','var'), readOnly = false; end;
            
      obj = obj@file_NRRD(fname,fpath,readOnly);     
      obj.parcelType = parcelType;
            
    end
    
    function set.parcelType(obj,val)
      if validatestring(val,obj.validParcels)
        obj.parcelType = val;
      else
        error('Invalid parcellation type');
      end;
    end
    
    %     function objOut = clone(obj,fname,fpath)
    %       if ~exist('fname','var'), fname = 'NewParcel.nhdr'; end;
    %       if ~exist('fpath','var'), fpath = './'; end;
    %
    %       objOut = clone@file_NRRD(obj,fname,fpath);
    %     %  objOut.parcelType = obj.parcelType;
    %
    %     end
    
    function out = get.nParcel(obj)
      out = numel(unique(obj.data(:)))-1;
    end
    
    function parcelOut = clone(parcelIn,fname,fpath)
      if ~exist('fname','var'), fname = []; end;
      if ~exist('fpath','var'), fpath = []; end;
      tmpNrrd = clone@file_NRRD(parcelIn,fname,fpath);
      parcelOut = cnlParcellation(tmpNrrd,[],parcelIn.parcelType);
    end
    
    function removeWhiteMatter(parcelIn)
      % Set White Matter Parcel Labels to Zero
      %
      % function removeWhiteMatter(parcelIn)
      %
      % Uses cnlParcellation.getMapping to associated parcel labels with
      % tissue types, and sets all labels associated with the white matter
      % to zero, effectively removing them from the parcellation.
      %
      % Written By: Damon Hyde
      % Last Edited: March 10, 2016
      % Part of the cnlEEG Project
      %
      try
        map = cnlParcellation.getMapping(parcelIn.parcelType);
      catch
        warning('Could get mapping. Data unchanged');
        return
      end
      
      parcelIn.data(ismember(parcelIn.data,map.whiteLabels)) = 0;
    end
    
    function removeSubCorticalGray(parcelIn)
      % Set Subcortical Gray Matter Parcel Labels to Zero
      %
      % function removeSubCorticalGray(parcelIn)
      %
      % Uses cnlParcellation.getMapping to associated parcel labels with
      % tissue types, and sets all labels associated with subcortical gray
      % matter to zero, effectively removing them from the parcellation.
      %
      % Written By: Damon Hyde
      % Last Edited: March 10, 2016
      % Part of the cnlEEG Project
      %
      try
        map = cnlParcellation.getMapping(parcelIn.parcelType);
      catch
        warning('Could get mapping. Data unchanged');
        return
      end
      
      parcelIn.data(ismember(parcelIn.data,map.subcorticalLabels)) = 0;
    end
    
    function removeCSF(parcelIn)
      % Set CSF Parcel Labels to Zero
      %
      % function removeWhiteMatter(parcelIn)
      %
      % Uses cnlParcellation.getMapping to associated parcel labels with
      % tissue types, and sets all labels associated with the CSF
      % to zero, effectively removing them from the parcellation.
      %
      % Written By: Damon Hyde
      % Last Edited: March 10, 2016
      % Part of the cnlEEG Project
      %
      try
        map = cnlParcellation.getMapping(parcelIn.parcelType);
      catch
        warning('Could get mapping. Data unchanged');
        return
      end
      
      parcelIn.data(ismember(parcelIn.data,map.csfLabels)) = 0;
    end
    
    
    
    % Methods with their own m-file
    nrrdParcel      = fourLabelSeg(nrrdParcel,fname,fpath);
    nrrdParcel      = cortexOnly(nrrdParcel,fname,fpath);
    nrrdSubParcel   = subparcellate(nrrdParcel,varargin);
    [nrrdParcelOut] = mapToSegmentation(nrrdParcel,varargin);
    nrrdOut = subparcellateByLeadfield(nrrdIn,nrrdVec,type,LeadField,voxLField,nFinal,fName);
    parcelOut = ensureConnectedParcels(parcelIn,varargin);
    [groupOut]   = get_ParcelGroupings(nrrdParcel);
    locationsOut = get_ParcelLocations(parcelIn)
    
  end % METHODS
  
  methods (Static=true)
    % Static Methods with their own m-file
    mappingOut = getMapping(type);
    NVM  = getNVMMapping;
    IBSR = getIBSRMapping;
    NMM  = getNMMMapping;
    [CutOut] = iterateCut(cMatrix,nFinal,cutsPerLvl,avgSize,depth);
  end
  
end % CLASSDEF
