
%
% def prox_l21(Y, alpha, n_orient, shape=None, is_stft=False):
%     """Proximity operator for l21 norm.
%
%     L2 over columns and L1 over rows => groups contain n_orient rows.
%
%     It can eventually take into account the negative frequencies
%     when a complex value is passed and is_stft=True.
%
%     Parameters
%     ----------
%     Y : array, shape (n_sources, n_coefs)
%         The input data.
%     alpha : float
%         The regularization parameter.
%     n_orient : int
%         Number of dipoles per locations (typically 1 or 3).
%     shape : None | tuple
%         Shape of TF coefficients matrix.
%     is_stft : bool
%         If True, Y contains TF coefficients.
%
%     Returns
%     -------
%     Y : array, shape (n_sources, n_coefs)
%         The output data.
%     active_set : array of bool, shape (n_sources, )
%         Mask of active sources
%
%     Example
%     -------
%     >>> Y = np.tile(np.array([0, 4, 3, 0, 0], dtype=np.float), (2, 1))
%     >>> Y = np.r_[Y, np.zeros_like(Y)]
%     >>> print(Y)
%     [[ 0.  4.  3.  0.  0.]
%      [ 0.  4.  3.  0.  0.]
%      [ 0.  0.  0.  0.  0.]
%      [ 0.  0.  0.  0.  0.]]
%     >>> Yp, active_set = prox_l21(Y, 2, 2)
%     >>> print(Yp)
%     [[ 0.          2.86862915  2.15147186  0.          0.        ]
%      [ 0.          2.86862915  2.15147186  0.          0.        ]]
%     >>> print(active_set)
%     [ True  True False False]
%     """
%
%

function varargout = prox_l21(Y,alpha,n_orient,shape,is_stft)
  
  if ~exist('shape','var'), shape = []; end;
  if ~exist('is_stft','var'), is_stft = false; end;
  
  %     if len(Y) == 0:
  %         return np.zeros_like(Y), np.zeros((0,), dtype=np.bool)
  if size(Y,1) == 0
    varargout{1} = zeros(size(Y));
    return
  end;
  
  %     if shape is not None:
  %         shape_init = Y.shape
  %         Y = Y.reshape(*shape)
  %     n_positions = Y.shape[0] // n_orient
  if ~isempty(shape)
    shape_init = size(Y);
    Y = reshape(Y,shape);
  end;
  n_positions = size(Y,1)/n_orient;
  
  %% L21 Proximity Operator for Short Time Fourier Transform Signals
  %     if is_stft:
  %         rows_norm = np.sqrt(stft_norm2(Y).reshape(n_positions, -1).sum(axis=1))
  %     else:
  %         rows_norm = np.sqrt((Y * Y.conj()).real.reshape(n_positions,
  %                                                         -1).sum(axis=1))
  if is_stft
    error('STFT L21 Proximity Operator Not Yet Implemented');
  else    
    rows_norm = sqrt(sum(reshape(Y.^2,n_positions,[]),2));    
    
    tmp = reshape(Y.^2,n_orient,n_positions,[]);
    tmp = reshape(permute(tmp,[2 1 3]),n_positions,[]);
    rows_norm = sqrt(sum(tmp,2));
  end
  
  %     # Ensure shrink is >= 0 while avoiding any division by zero
  %     shrink = np.maximum(1.0 - alpha / np.maximum(rows_norm, alpha), 0.0)
  %     active_set = shrink > 0.0
  shrink = max(1-alpha./max(rows_norm,alpha),0); shrink = shrink(:);
  active_set = shrink>0;
  
  %     if n_orient > 1:
  %         active_set = np.tile(active_set[:, None], [1, n_orient]).ravel()
  %         shrink = np.tile(shrink[:, None], [1, n_orient]).ravel()
  %     Y = Y[active_set]
  %     if shape is None:
  %         Y *= shrink[active_set][:, np.newaxis]
  %     else:
  %         Y *= shrink[active_set][:, np.newaxis, np.newaxis]
  %         Y = Y.reshape(-1, *shape_init[1:])
  %     return Y, active_set
  
  if n_orient > 1
    active_set = repmat(active_set(:),1,n_orient)';
    active_set = active_set(:);
    shrink     = repmat(shrink(:),1,n_orient)';
    shrink     = shrink(:);
  end
  
  Y = Y(active_set,:);
  
  if isempty(shape)
    Y = Y.*repmat(shrink(active_set),1,size(Y,2));
  else
    tmpSize = num2cell(shape(2:end));
    Y = Y.*repmat(shrink(active_set),1,size(Y,2));
    Y = reshape(Y,[],tmpSize{:});
  end
  
  varargout{1} = Y;
  if nargout>=2, varargout{2} = active_set; end;
  
  %error('prox_l21() not yet implemented');
end