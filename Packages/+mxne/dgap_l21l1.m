
% 
% 
% def dgap_l21l1(M, G, Z, active_set, alpha_space, alpha_time, phi, phiT, shape,
%                n_orient, highest_d_obj):
%     """Duality gap for the time-frequency mixed norm inverse problem.
% 
%     Parameters
%     ----------
%     M : array, shape (n_sensors, n_times)
%         The data.
%     G : array, shape (n_sensors, n_sources)
%         Gain matrix a.k.a. lead field.
%     Z : array, shape (n_active, n_coefs)
%         Sources in TF domain.
%     active_set : array of bool, shape (n_sources, )
%         Mask of active sources.
%     alpha_space : float
%         The spatial regularization parameter.
%     alpha_time : float
%         The temporal regularization parameter. The higher it is the smoother
%         will be the estimated time series.
%     phi : instance of _Phi
%         The TF operator.
%     phiT : instance of _PhiT
%         The transpose of the TF operator.
%     shape : tuple
%         Shape of TF coefficients matrix.
%     n_orient : int
%         Number of dipoles per locations (typically 1 or 3).
%     highest_d_obj : float
%         The highest value of the dual objective so far.
% 
%     Returns
%     -------
%     gap : float
%         Dual gap
%     p_obj : float
%         Primal objective
%     d_obj : float
%         Dual objective. gap = p_obj - d_obj
%     R : array, shape (n_sensors, n_times)
%         Current residual (M - G * X)
% 
%     References
%     ----------
%     .. [1] A. Gramfort, M. Kowalski, M. Hamalainen,
%        "Mixed-norm estimates for the M/EEG inverse problem using accelerated
%        gradient methods", Physics in Medicine and Biology, 2012.
%        http://dx.doi.org/10.1088/0031-9155/57/7/1937
% 
%     .. [2] J. Wang, J. Ye,
%        "Two-layer feature reduction for sparse-group lasso via decomposition of
%        convex sets", Advances in Neural Information Processing Systems (NIPS),
%        vol. 27, pp. 2132-2140, 2014.
%     """
%     X = phiT(Z)
%     GX = np.dot(G[:, active_set], X)
%     R = M - GX
%     penaltyl1 = norm_l1_tf(Z, shape, n_orient)
%     penaltyl21 = norm_l21_tf(Z, shape, n_orient)
%     nR2 = sum_squared(R)
%     p_obj = 0.5 * nR2 + alpha_space * penaltyl21 + alpha_time * penaltyl1
% 
%     GRPhi_norm, _ = prox_l1(phi(np.dot(G.T, R)), alpha_time, n_orient)
%     GRPhi_norm = stft_norm2(GRPhi_norm.reshape(*shape)).reshape(-1, n_orient)
%     GRPhi_norm = np.sqrt(GRPhi_norm.sum(axis=1))
%     dual_norm = np.amax(GRPhi_norm)
%     scaling = alpha_space / dual_norm
%     scaling = min(scaling, 1.0)
%     d_obj = (scaling - 0.5 * (scaling ** 2)) * nR2 + scaling * np.sum(R * GX)
%     d_obj = max(d_obj, highest_d_obj)
% 
%     gap = p_obj - d_obj
%     return gap, p_obj, d_obj, R
% 
% 
function dgap_l21l1()
  error('dgap_l21l1 not implemented')
end