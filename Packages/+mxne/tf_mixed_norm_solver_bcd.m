% def _tf_mixed_norm_solver_bcd_(M, G, Z, active_set, candidates, alpha_space,
%                                alpha_time, lipschitz_constant, phi, phiT,
%                                shape, n_orient=1, maxit=200, tol=1e-8,
%                                log_objective=True, perc=None, timeit=True,
%                                verbose=None):
% 
%     # First make G fortran for faster access to blocks of columns
%     G = np.asfortranarray(G)
% 
%     n_sensors, n_times = M.shape
%     n_sources = G.shape[1]
%     n_positions = n_sources // n_orient
% 
%     Gd = G.copy()
%     G = dict(zip(np.arange(n_positions), np.hsplit(G, n_positions)))
% 
%     R = M.copy()  # residual
%     active = np.where(active_set[::n_orient])[0]
%     for idx in active:
%         R -= np.dot(G[idx], phiT(Z[idx]))
% 
%     E = []  # track primal objective function
% 
%     alpha_time_lc = alpha_time / lipschitz_constant
%     alpha_space_lc = alpha_space / lipschitz_constant
% 
%     converged = False
%     d_obj = -np.Inf
% 
%     ii = -1
%     while True:
%         ii += 1
%         for jj in candidates:
%             ids = jj * n_orient
%             ide = ids + n_orient
% 
%             G_j = G[jj]
%             Z_j = Z[jj]
%             active_set_j = active_set[ids:ide]
% 
%             was_active = np.any(active_set_j)
% 
%             # gradient step
%             GTR = np.dot(G_j.T, R) / lipschitz_constant[jj]
%             X_j_new = GTR.copy()
% 
%             if was_active:
%                 X_j = phiT(Z_j)
%                 R += np.dot(G_j, X_j)
%                 X_j_new += X_j
% 
%             rows_norm = linalg.norm(X_j_new, 'fro')
%             if rows_norm <= alpha_space_lc[jj]:
%                 if was_active:
%                     Z[jj] = 0.0
%                     active_set_j[:] = False
%             else:
%                 if was_active:
%                     Z_j_new = Z_j + phi(GTR)
%                 else:
%                     Z_j_new = phi(GTR)
%                 col_norm = np.sqrt(np.sum(np.abs(Z_j_new) ** 2, axis=0))
% 
%                 if np.all(col_norm <= alpha_time_lc[jj]):
%                     Z[jj] = 0.0
%                     active_set_j[:] = False
%                 else:
%                     # l1
%                     shrink = np.maximum(1.0 - alpha_time_lc[jj] / np.maximum(
%                                         col_norm, alpha_time_lc[jj]), 0.0)
%                     Z_j_new *= shrink[np.newaxis, :]
% 
%                     # l21
%                     shape_init = Z_j_new.shape
%                     Z_j_new = Z_j_new.reshape(*shape)
%                     row_norm = np.sqrt(stft_norm2(Z_j_new).sum())
%                     if row_norm <= alpha_space_lc[jj]:
%                         Z[jj] = 0.0
%                         active_set_j[:] = False
%                     else:
%                         shrink = np.maximum(1.0 - alpha_space_lc[jj] /
%                                             np.maximum(row_norm,
%                                             alpha_space_lc[jj]), 0.0)
%                         Z_j_new *= shrink
%                         Z[jj] = Z_j_new.reshape(-1, *shape_init[1:]).copy()
%                         active_set_j[:] = True
%                         R -= np.dot(G_j, phiT(Z[jj]))
% 
%         if log_objective:
%             if (ii + 1) % 10 == 0:
%                 Zd = np.vstack([Z[pos] for pos in range(n_positions)
%                                if np.any(Z[pos])])
%                 gap, p_obj, d_obj, _ = dgap_l21l1(
%                     M, Gd, Zd, active_set, alpha_space, alpha_time, phi, phiT,
%                     shape, n_orient, d_obj)
%                 converged = (gap < tol)
%                 E.append(p_obj)
%                 logger.info("\n    Iteration %d :: n_active %d" % (
%                             ii + 1, np.sum(active_set) / n_orient))
%                 logger.info("    dgap %.2e :: p_obj %f :: d_obj %f" % (
%                             gap, p_obj, d_obj))
%         else:
%             if (ii + 1) % 10 == 0:
%                 logger.info("\n    Iteration %d :: n_active %d" % (
%                             ii + 1, np.sum(active_set) / n_orient))
% 
%         if converged:
%             break
% 
%         if (ii == maxit - 1):
%             converged = False
%             break
% 
%         if perc is not None:
%             if np.sum(active_set) / float(n_orient) <= perc * n_positions:
%                 break
% 
%     return Z, active_set, E, converged

function tf_mixed_norm_solver_bcd()
  error('tf_mixed_norm_solver_bcd not implemented')
end