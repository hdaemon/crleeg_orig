
%
% @verbose
% def _mixed_norm_solver_bcd(M, G, alpha, lipschitz_constant, maxit=200,
%                            tol=1e-8, verbose=None, init=None, n_orient=1):
%     """Solve L21 inverse problem with block coordinate descent."""
%     # First make G fortran for faster access to blocks of columns
%     G = np.asfortranarray(G)
%


%

%

%

%
%     X = X[active_set]
%
%     return X, active_set, E

function [X,active_set,E] = mixed_norm_solver_bcd(M,G,alpha,lipschitz_constant,varargin)
  
  %% Input Parsing
  p = inputParser;
  p.addRequired('M');
  p.addRequired('G');
  p.addRequired('alpha');
  p.addRequired('lipschitz_constant');
  p.addParamValue('maxit',200,@(x) isscalar(x));
  p.addParamValue('tol',1e-8);
  p.addParamValue('verbose',false);
  p.addParamValue('init',[]);
  p.addParamValue('n_orient',1);
  p.parse(M,G,alpha,lipschitz_constant,varargin{:});
  
  maxit    = p.Results.maxit;
  tol      = p.Results.tol;
  verbose  = p.Results.verbose;
  init     = p.Results.init;
  n_orient = p.Results.n_orient;
  
  %% Get number of sensors, timepoints, and source positions
  %     n_sensors, n_times = M.shape
  %     n_sensors, n_sources = G.shape
  %     n_positions = n_sources // n_orient
  %
  % Number of sensors is never used.
  
  [~,n_times] = size(M);
  [~,n_sources] = size(G);
  [n_positions] = n_sources/n_orient;
  
  %% Initialize the source estimate and residual.
  %
  %     if init is None:
  %         X = np.zeros((n_sources, n_times))
  %         R = M.copy()
  %     else:
  %         X = init
  %         R = M - np.dot(G, X)
  if isempty(init), X = zeros(n_sources,n_times);  R = M;
              else, X = init;                      R = M - G*X;  end;
  
  
  %     E = []  # track primal objective function
  %     highest_d_obj = - np.inf
  %     active_set = np.zeros(n_sources, dtype=np.bool)  # start with full AS
  %     alpha_lc = alpha / lipschitz_constant
  %
  % The comments say start with a full active set, but setting things equal
  % to zeros amounts to starting with a zero active set, yes?
  
  E = [];
  highest_d_obj = -inf;
  active_set = false(1,n_sources);
  
  alpha_lc = alpha./lipschitz_constant;
  
  %     for i in range(maxit):
  %         for j in range(n_positions):
  %             idx = slice(j * n_orient, (j + 1) * n_orient)
  %
  %             G_j = G[:, idx]
  %             X_j = X[idx]
  %
  %             X_j_new = np.dot(G_j.T, R) / lipschitz_constant[j]
  %
  %             was_non_zero = np.any(X_j)
  %             if was_non_zero:
  %                 R += np.dot(G_j, X_j)
  %                 X_j_new += X_j
  %
  %             block_norm = linalg.norm(X_j_new, 'fro')
  %             if block_norm <= alpha_lc[j]:
  %                 X_j.fill(0.)
  %                 active_set[idx] = False
  %             else:
  %                 shrink = np.maximum(1.0 - alpha_lc[j] / block_norm, 0.0)
  %                 X_j_new *= shrink
  %                 R -= np.dot(G_j, X_j_new)
  %                 X_j[:] = X_j_new
  %                 active_set[idx] = True
  %
  %         _, p_obj, d_obj, _ = dgap_l21(M, G, X[active_set], active_set, alpha,
  %                                       n_orient)
  %         highest_d_obj = max(d_obj, highest_d_obj)
  %         gap = p_obj - highest_d_obj
  %         E.append(p_obj)
  %         logger.debug("Iteration %d :: p_obj %f :: dgap %f :: n_active %d" % (
  %                      i + 1, p_obj, gap, np.sum(active_set) / n_orient))
  %
  %         if gap < tol:
  %             logger.debug('Convergence reached ! (gap: %s < %s)' % (gap, tol))
  %             break
  
  for i = 1:maxit
    for j = 1:n_positions
      
      % Get column indices associated with the jth source location
      idxStart = (j-1)*n_orient +1;
      idxEnd = j*n_orient;
      idx = idxStart:idxEnd;
      
      % Get the leadfield and current estimate for the source at location j
      G_j = G(:,idx);
      X_j = X(idx,:);
      
      X_j_new = (G_j.'*R) / lipschitz_constant(j);
      
      was_non_zero = any(X_j);
      if was_non_zero
        R = R + G_j*X_j;
        X_j_new = X_j_new + X_j;
      end
      
      block_norm = norm(X_j_new,'fro');
      if block_norm <= alpha_lc(j)
        X(idx,:) = zeros(size(X_j));
        active_set(idx) = false;
      else
        shrink = max(1-alpha_lc(j)/block_norm,0);
        X_j_new = X_j_new*shrink;
        R = R - G_j*X_j_new;
        X(idx,:) = X_j_new;
        active_set(idx) = true;
      end
    end
    
    [~,p_obj,d_obj,~] = mxne.dgap_l21(M,G,X(active_set,:),active_set,alpha,n_orient);
    highest_d_obj = max(d_obj,highest_d_obj);
    gap = p_obj - highest_d_obj;
    E(end+1) = p_obj;
    
    err = norm(M-G*X,'fro');
    %disp(['BCD: Err:' num2str(err) ' pObj:' num2str(p_obj) ' dObj:' num2str(d_obj) ' Current Gap: ' num2str(gap)]);
    if gap<tol
      disp('Convergence Reached!');
      break;
    end;
    
  end
  
  X = X(active_set,:);
  
end