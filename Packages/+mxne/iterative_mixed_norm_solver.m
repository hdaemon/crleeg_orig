
% 
% 
% @verbose
% def iterative_mixed_norm_solver(M, G, alpha, n_mxne_iter, maxit=3000,
%                                 tol=1e-8, verbose=None, active_set_size=50,
%                                 debias=True, n_orient=1, solver='auto'):
%     """Solve L0.5/L2 mixed-norm inverse problem with active set strategy.
% 
%     Parameters
%     ----------
%     M : array, shape (n_sensors, n_times)
%         The data.
%     G : array, shape (n_sensors, n_dipoles)
%         The gain matrix a.k.a. lead field.
%     alpha : float
%         The regularization parameter. It should be between 0 and 100.
%         A value of 100 will lead to an empty active set (no active source).
%     n_mxne_iter : int
%         The number of MxNE iterations. If > 1, iterative reweighting
%         is applied.
%     maxit : int
%         The number of iterations.
%     tol : float
%         Tolerance on dual gap for convergence checking.
%     verbose : bool, str, int, or None
%         If not None, override default verbose level (see :func:`mne.verbose`
%         and :ref:`Logging documentation <tut_logging>` for more).
%     active_set_size : int
%         Size of active set increase at each iteration.
%     debias : bool
%         Debias source estimates.
%     n_orient : int
%         The number of orientation (1 : fixed or 3 : free or loose).
%     solver : 'prox' | 'cd' | 'bcd' | 'auto'
%         The algorithm to use for the optimization.
% 
%     Returns
%     -------
%     X : array, shape (n_active, n_times)
%         The source estimates.
%     active_set : array
%         The mask of active sources.
%     E : list
%         The value of the objective function over the iterations.
% 
%     References
%     ----------
%     .. [1] D. Strohmeier, Y. Bekhti, J. Haueisen, A. Gramfort,
%        "The Iterative Reweighted Mixed-Norm Estimate for Spatio-Temporal
%        MEG/EEG Source Reconstruction", IEEE Transactions of Medical Imaging,
%        Volume 35 (10), pp. 2218-2228, 2016.
%     """
%     def g(w):
%         return np.sqrt(np.sqrt(groups_norm2(w.copy(), n_orient)))
% 
%     def gprime(w):
%         return 2. * np.repeat(g(w), n_orient).ravel()
% 
%     E = list()
% 
%     active_set = np.ones(G.shape[1], dtype=np.bool)
%     weights = np.ones(G.shape[1])
%     X = np.zeros((G.shape[1], M.shape[1]))
% 
%     for k in range(n_mxne_iter):
%         X0 = X.copy()
%         active_set_0 = active_set.copy()
%         G_tmp = G[:, active_set] * weights[np.newaxis, :]
% 
%         if active_set_size is not None:
%             if np.sum(active_set) > (active_set_size * n_orient):
%                 X, _active_set, _ = mixed_norm_solver(
%                     M, G_tmp, alpha, debias=False, n_orient=n_orient,
%                     maxit=maxit, tol=tol, active_set_size=active_set_size,
%                     solver=solver, verbose=verbose)
%             else:
%                 X, _active_set, _ = mixed_norm_solver(
%                     M, G_tmp, alpha, debias=False, n_orient=n_orient,
%                     maxit=maxit, tol=tol, active_set_size=None, solver=solver,
%                     verbose=verbose)
%         else:
%             X, _active_set, _ = mixed_norm_solver(
%                 M, G_tmp, alpha, debias=False, n_orient=n_orient,
%                 maxit=maxit, tol=tol, active_set_size=None, solver=solver,
%                 verbose=verbose)
% 
%         logger.info('active set size %d' % (_active_set.sum() / n_orient))
% 
%         if _active_set.sum() > 0:
%             active_set[active_set] = _active_set
% 
%             # Reapply weights to have correct unit
%             X *= weights[_active_set][:, np.newaxis]
%             weights = gprime(X)
%             p_obj = 0.5 * linalg.norm(M - np.dot(G[:, active_set], X),
%                                       'fro') ** 2. + alpha * np.sum(g(X))
%             E.append(p_obj)
% 
%             # Check convergence
%             if ((k >= 1) and np.all(active_set == active_set_0) and
%                     np.all(np.abs(X - X0) < tol)):
%                 print('Convergence reached after %d reweightings!' % k)
%                 break
%         else:
%             active_set = np.zeros_like(active_set)
%             p_obj = 0.5 * linalg.norm(M) ** 2.
%             E.append(p_obj)
%             break
% 
%     if np.any(active_set) and debias:
%         bias = compute_bias(M, G[:, active_set], X, n_orient=n_orient)
%         X *= bias[:, np.newaxis]
% 
%     return X, active_set, E

function iterative_mixed_norm_solver()
  error('iterative_mixed_norm_solver() not yet implemented');
end