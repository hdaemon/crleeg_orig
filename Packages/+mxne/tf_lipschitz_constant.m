


% 
% 
% ###############################################################################
% # TF-MxNE
% 
% @verbose
% def tf_lipschitz_constant(M, G, phi, phiT, tol=1e-3, verbose=None):
%     """Compute lipschitz constant for FISTA.
% 
%     It uses a power iteration method.
%     """
%     n_times = M.shape[1]
%     n_points = G.shape[1]
%     iv = np.ones((n_points, n_times), dtype=np.float)
%     v = phi(iv)
%     L = 1e100
%     for it in range(100):
%         L_old = L
%         logger.info('Lipschitz estimation: iteration = %d' % it)
%         iv = np.real(phiT(v))
%         Gv = np.dot(G, iv)
%         GtGv = np.dot(G.T, Gv)
%         w = phi(GtGv)
%         L = np.max(np.abs(w))  # l_inf norm
%         v = w / L
%         if abs((L - L_old) / L_old) < tol:
%             break
%     return L

function tf_lipschitz_constant()
  error('tf_lipschitz_constant() not yet implemented');
end