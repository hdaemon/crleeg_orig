%
%
% @verbose
% def mixed_norm_solver(M, G, alpha, maxit=3000, tol=1e-8, verbose=None,
%                       active_set_size=50, debias=True, n_orient=1,
%                       solver='auto', return_gap=False):
%     """Solve L1/L2 mixed-norm inverse problem with active set strategy.
%
%     Parameters
%     ----------
%     M : array, shape (n_sensors, n_times)
%         The data.
%     G : array, shape (n_sensors, n_dipoles)
%         The gain matrix a.k.a. lead field.
%     alpha : float
%         The regularization parameter. It should be between 0 and 100.
%         A value of 100 will lead to an empty active set (no active source).
%     maxit : int
%         The number of iterations.
%     tol : float
%         Tolerance on dual gap for convergence checking.
%     verbose : bool, str, int, or None
%         If not None, override default verbose level (see :func:`mne.verbose`
%         and :ref:`Logging documentation <tut_logging>` for more).
%     active_set_size : int
%         Size of active set increase at each iteration.
%     debias : bool
%         Debias source estimates.
%     n_orient : int
%         The number of orientation (1 : fixed or 3 : free or loose).
%     solver : 'prox' | 'cd' | 'bcd' | 'auto'
%         The algorithm to use for the optimization.
%     return_gap : bool
%         Return final duality gap.
%
%     Returns
%     -------
%     X : array, shape (n_active, n_times)
%         The source estimates.
%     active_set : array
%         The mask of active sources.
%     E : list
%         The value of the objective function over the iterations.
%     gap : float
%         Final duality gap. Returned only if return_gap is True.
%
%     References
%     ----------
%     .. [1] A. Gramfort, M. Kowalski, M. Hamalainen,
%        "Mixed-norm estimates for the M/EEG inverse problem using accelerated
%        gradient methods", Physics in Medicine and Biology, 2012.
%        http://dx.doi.org/10.1088/0031-9155/57/7/1937
%
%     .. [2] D. Strohmeier, Y. Bekhti, J. Haueisen, A. Gramfort,
%        "The Iterative Reweighted Mixed-Norm Estimate for Spatio-Temporal
%        MEG/EEG Source Reconstruction", IEEE Transactions of Medical Imaging,
%        Volume 35 (10), pp. 2218-2228, 15 April 2013.
%     """
%
%
%     if np.any(active_set) and debias:
%         bias = compute_bias(M, G[:, active_set], X, n_orient=n_orient)
%         X *= bias[:, np.newaxis]
%
%     logger.info('Final active set size: %s' % (np.sum(active_set) // n_orient))
%
%     if return_gap:
%         return X, active_set, E, gap
%     else:
%         return X, active_set, E

function varargout = mixed_norm_solver(M,G,alpha,varargin)

%% Input Parsing
p = inputParser;
p.addRequired('M');
p.addRequired('G');
p.addRequired('alpha');
p.addParamValue('maxit',3000,@(x) isscalar(x));
p.addParamValue('tol',1e-8,@(x) isscalar(x));
p.addParamValue('verbose',false,@(x) islogical(x));
p.addParamValue('active_set_size',50,@(x) isscalar(x));
p.addParamValue('debias',true,@(x) islogical(x));
p.addParamValue('n_orient',1,@(x) isscalar(x));
p.addParamValue('solver','auto',@(x) ischar(x));
p.addParamValue('return_gap',false,@(x) islogical(x));
p.parse(M,G,alpha,varargin{:});

maxit   = p.Results.maxit;
tol     = p.Results.tol;
verbose = p.Results.verbose; % Unused in Matlab version?
active_set_size = p.Results.active_set_size;
debias     = p.Results.debias;
n_orient   = p.Results.n_orient;
solver     = p.Results.solver;
return_gap = p.Results.return_gap;

%% Variable Initialization
%     n_dipoles = G.shape[1]
%     n_positions = n_dipoles // n_orient
%     n_sensors, n_times = M.shape
%     alpha_max = norm_l2inf(np.dot(G.T, M), n_orient, copy=False)
%     logger.info("-- ALPHA MAX : %s" % alpha_max)
%     alpha = float(alpha)
n_dipoles = size(G,2);
n_positions = n_dipoles/n_orient;
[n_sensors,n_times] = size(M);
alpha_max = mxne.norm_l2inf(G.'*M,n_orient);

% Here, pyMNE checks for sklean, to use their coordinate descent solver.
% Since this is matlab instead, we just use their bcd approach.
%
%     has_sklearn = True
%     try:
%         from sklearn.linear_model.coordinate_descent import MultiTaskLasso  # noqa: F401,E501
%     except ImportError:
%         has_sklearn = False
%
%     if solver == 'auto':
%         if has_sklearn and (n_orient == 1):
%             solver = 'cd'
%         else:
%             solver = 'bcd'
%

if isequal(lower(solver),'auto')
  solver = 'bcd';
end

%% Configure Solver and Initialize
%     if solver == 'cd':
%         if n_orient == 1 and not has_sklearn:
%             warn('Scikit-learn >= 0.12 cannot be found. Using block coordinate'
%                  ' descent instead of coordinate descent.')
%             solver = 'bcd'
%         if n_orient > 1:
%             warn('Coordinate descent is only available for fixed orientation. '
%                  'Using block coordinate descent instead of coordinate '
%                  'descent')
%             solver = 'bcd'

if isequal(lower(solver), 'cd')
  warning('Using block coordinate descent in Matlab');
  solver = 'bcd';
end;

%     if solver == 'cd':
%         logger.info("Using coordinate descent")
%         l21_solver = _mixed_norm_solver_cd
%         lc = None
%     elif solver == 'bcd':
%         logger.info("Using block coordinate descent")
%         l21_solver = _mixed_norm_solver_bcd
%         G = np.asfortranarray(G)
%         if n_orient == 1:
%             lc = np.sum(G * G, axis=0)
%         else:
%             lc = np.empty(n_positions)
%             for j in range(n_positions):
%                 G_tmp = G[:, (j * n_orient):((j + 1) * n_orient)]
%                 lc[j] = linalg.norm(np.dot(G_tmp.T, G_tmp), ord=2)
%     else:
%         logger.info("Using proximal iterations")
%         l21_solver = _mixed_norm_solver_prox
%         lc = 1.01 * linalg.norm(G, ord=2) ** 2

if isequal(lower(solver),'cd')
  error('This should never execute');
elseif isequal(lower(solver),'bcd')
  % Use block coordinate descent
  disp('Using block coordinate descent');
  l21_solver = @mxne.mixed_norm_solver_bcd;
  if n_orient == 1
    % For single orientation leadfields, the lipschitz constant is just the
    % column norm.
    lc = sum(G.^2,1);
  else    
    lc = zeros(n_positions,1);
    for j = 1:n_positions
      % Get 2-norm of submatrix.
      colIdxStart = (j-1)*n_orient + 1;
      colIdxEnd = j*n_orient;      
      G_tmp = G(:,colIdxStart:colIdxEnd);
      lc(j) = norm(G_tmp.'*G_tmp,2);
    end
  end
else
  % Use proximal iterations with FISTA
  disp('Using proximal iterations');
  l21_solver = @mxne.mixed_norm_solver_prox;
  lc = 1.01 * norm(G,2).^2 ; 
end

%
%     if active_set_size is not None:
%         E = list()
%         highest_d_obj = - np.inf
%         X_init = None
%         active_set = np.zeros(n_dipoles, dtype=np.bool)
%         idx_large_corr = np.argsort(groups_norm2(np.dot(G.T, M), n_orient))
%         new_active_idx = idx_large_corr[-active_set_size:]
%         if n_orient > 1:
%             new_active_idx = (n_orient * new_active_idx[:, None] +
%                               np.arange(n_orient)[None, :]).ravel()
%         active_set[new_active_idx] = True
%         as_size = np.sum(active_set)
%         for k in range(maxit):
%             if solver == 'bcd':
%                 lc_tmp = lc[active_set[::n_orient]]
%             elif solver == 'cd':
%                 lc_tmp = None
%             else:
%                 lc_tmp = 1.01 * linalg.norm(G[:, active_set], ord=2) ** 2
%             X, as_, _ = l21_solver(M, G[:, active_set], alpha, lc_tmp,
%                                    maxit=maxit, tol=tol, init=X_init,
%                                    n_orient=n_orient)
%             active_set[active_set] = as_.copy()
%             idx_old_active_set = np.where(active_set)[0]
%
%             _, p_obj, d_obj, R = dgap_l21(M, G, X, active_set, alpha,
%                                           n_orient)
%             highest_d_obj = max(d_obj, highest_d_obj)
%             gap = p_obj - highest_d_obj
%             E.append(p_obj)
%             logger.info("Iteration %d :: p_obj %f :: dgap %f ::"
%                         "n_active_start %d :: n_active_end %d" % (
%                             k + 1, p_obj, gap, as_size // n_orient,
%                             np.sum(active_set) // n_orient))
%             if gap < tol:
%                 logger.info('Convergence reached ! (gap: %s < %s)'
%                             % (gap, tol))
%                 break
%
%             # add sources if not last iteration
%             if k < (maxit - 1):
%                 idx_large_corr = np.argsort(groups_norm2(np.dot(G.T, R),
%                                             n_orient))
%                 new_active_idx = idx_large_corr[-active_set_size:]
%                 if n_orient > 1:
%                     new_active_idx = (n_orient * new_active_idx[:, None] +
%                                       np.arange(n_orient)[None, :])
%                     new_active_idx = new_active_idx.ravel()
%                 active_set[new_active_idx] = True
%                 idx_active_set = np.where(active_set)[0]
%                 as_size = np.sum(active_set)
%                 X_init = np.zeros((as_size, n_times), dtype=X.dtype)
%                 idx = np.searchsorted(idx_active_set, idx_old_active_set)
%                 X_init[idx] = X
%         else:
%             warn('Did NOT converge ! (gap: %s > %s)' % (gap, tol))
%     else:
%         X, active_set, E = l21_solver(M, G, alpha, lc, maxit=maxit,
%                                       tol=tol, n_orient=n_orient, init=None)
%         if return_gap:
%             gap = dgap_l21(M, G, X, active_set, alpha, n_orient)[0]

if ~(active_set_size==-1)
  %% Fixed Active Set Size
  % More initialization
  E = [];
  highest_d_obj = -inf;  
  X_init = [];
  
  % Initialize Active Set with backprojection of the data.  
  active_set = logical(zeros(n_dipoles,1));
  [~,idx_large_corr] = sort(mxne.groups_norm2(G.'*M,n_orient));
  new_active_idx = idx_large_corr(((end-active_set_size)+1):end);
  if n_orient > 1
    % Get indexing for multiple orientations
    new_active_idx = ((n_orient * repmat(new_active_idx(:)-1,1,n_orient)) ...
      + repmat(1:n_orient,numel(new_active_idx(:)),1))';
    new_active_idx = new_active_idx(:);
  end
  active_set(new_active_idx) = true;  
  as_size = sum(active_set);
    
  for k = 1:maxit
    % Lipschitz Constants for Current Active Set
    if isequal(lower(solver),'bcd')      
      lc_tmp = lc(active_set(1:n_orient:end));
    else
      lc_tmp = 1.01 * norm(G(:,active_set),2).^2;
    end
    
    % Solve l21 problem.
    [X, as,~] = l21_solver(M,G(:,active_set),alpha,lc_tmp,...
                            'maxit',maxit,'tol',tol,...
                            'init',X_init,'n_orient',n_orient,...
                            'verbose',verbose);
    active_set(active_set) = as;
    idx_old_active_set = find(active_set);
    
    [~,p_obj,d_obj,R] = mxne.dgap_l21(M,G,X,active_set,alpha,n_orient);
    
    highest_d_obj = max(d_obj,highest_d_obj);
    gap = p_obj - highest_d_obj;
    E(end+1) = p_obj;
    if verbose
    disp(['Iteration: ' num2str(k+1) ...
      ' p_obj: ' num2str(p_obj) ...
      ' dgap: ' num2str(gap)...
      ' n_active_start:' num2str(as_size/n_orient) ...
      ' n_active_end:' num2str(sum(active_set)/n_orient)]);
    end;
    
    if gap < tol
      disp('Convergence reached');
      break;
    end;
    
    if k < maxit
      %%
      [~,idx_large_corr] = sort(mxne.groups_norm2(G.'*R,n_orient));
      new_active_idx = idx_large_corr(((end-active_set_size)+1):end);
      if n_orient > 1
        new_active_idx = ((n_orient * repmat(new_active_idx(:)-1,1,n_orient)) ...
          + repmat(1:n_orient,numel(new_active_idx(:)),1))';
        new_active_idx = new_active_idx(:);
      end
      active_set(new_active_idx) = true;
      [idx_active_set] = find(active_set);
      as_size = sum(active_set);
      X_init = zeros(as_size,n_times);
      idx = find(ismember(idx_old_active_set,idx_active_set));
      X_init(idx,:) = X;
    else
      warning('Did NOT converge!');
    end
  end
else
  %% Undefined Active Set Size
  [X, active_set,E] = l21_solver(M,G,alpha,lc,...
                                  'maxit',maxit,'tol',tol,...
                                  'n_orient',n_orient);
  if return_gap
    gap = dgap_l21(M,G,X,active_set,alpha,n_orient);
    gap = gap(1);
  end
end

if any(active_set)&&debias
  bias = mxne.compute_bias(M,G(:,active_set),X,'n_orient',n_orient);
    
end

if return_gap
  varargout = {X, active_set, E, gap};
else
  varargout = {X, active_set, E};
end;

end