% 
% 
% class _PhiT(object):
%     """Have phi.T istft as callable w/o using a lambda that does not pickle."""
% 
%     def __init__(self, tstep, n_freq, n_step, n_times):  # noqa: D102
%         self.tstep = tstep
%         self.n_freq = n_freq
%         self.n_step = n_step
%         self.n_times = n_times
% 
%     def __call__(self, z):  # noqa: D105
%         return istft(z.reshape(-1, self.n_freq, self.n_step), self.tstep,
%                      self.n_times)

classdef PhiT
  
  methods
    
    function obj = PhiT()
    end
  end
end