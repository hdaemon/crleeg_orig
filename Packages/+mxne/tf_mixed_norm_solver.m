% 
% 
% @verbose
% def tf_mixed_norm_solver(M, G, alpha_space, alpha_time, wsize=64, tstep=4,
%                          n_orient=1, maxit=200, tol=1e-8, log_objective=True,
%                          active_set_size=None, debias=True, return_gap=False,
%                          verbose=None):
%     """Solve TF L21+L1 inverse solver with BCD and active set approach.
% 
%     Parameters
%     ----------
%     M : array, shape (n_sensors, n_times)
%         The data.
%     G : array, shape (n_sensors, n_dipoles)
%         The gain matrix a.k.a. lead field.
%     alpha_space : float
%         The spatial regularization parameter.
%     alpha_time : float
%         The temporal regularization parameter. The higher it is the smoother
%         will be the estimated time series.
%     wsize: int
%         length of the STFT window in samples (must be a multiple of 4).
%     tstep: int
%         step between successive windows in samples (must be a multiple of 2,
%         a divider of wsize and smaller than wsize/2) (default: wsize/2).
%     n_orient : int
%         The number of orientation (1 : fixed or 3 : free or loose).
%     maxit : int
%         The number of iterations.
%     tol : float
%         If absolute difference between estimates at 2 successive iterations
%         is lower than tol, the convergence is reached.
%     log_objective : bool
%         If True, the value of the minimized objective function is computed
%         and stored at every 10th iteration.
%     debias : bool
%         Debias source estimates.
%     return_gap : bool
%         Return final duality gap.
%     verbose : bool, str, int, or None
%         If not None, override default verbose level (see :func:`mne.verbose`
%         and :ref:`Logging documentation <tut_logging>` for more).
% 
%     Returns
%     -------
%     X : array, shape (n_active, n_times)
%         The source estimates.
%     active_set : array
%         The mask of active sources.
%     E : list
%         The value of the objective function at each iteration. If log_objective
%         is False, it will be empty.
%     gap : float
%         Final duality gap. Returned only if return_gap is True.
% 
%     References
%     ----------
%     .. [1] A. Gramfort, D. Strohmeier, J. Haueisen, M. Hamalainen, M. Kowalski
%        "Time-Frequency Mixed-Norm Estimates: Sparse M/EEG imaging with
%        non-stationary source activations",
%        Neuroimage, Volume 70, pp. 410-422, 15 April 2013.
%        DOI: 10.1016/j.neuroimage.2012.12.051
% 
%     .. [2] A. Gramfort, D. Strohmeier, J. Haueisen, M. Hamalainen, M. Kowalski
%        "Functional Brain Imaging with M/EEG Using Structured Sparsity in
%        Time-Frequency Dictionaries",
%        Proceedings Information Processing in Medical Imaging
%        Lecture Notes in Computer Science, Volume 6801/2011, pp. 600-611, 2011.
%        DOI: 10.1007/978-3-642-22092-0_49
%     """
%     n_sensors, n_times = M.shape
%     n_sensors, n_sources = G.shape
%     n_positions = n_sources // n_orient
% 
%     n_step = int(ceil(n_times / float(tstep)))
%     n_freq = wsize // 2 + 1
%     n_coefs = n_step * n_freq
%     shape = (-1, n_freq, n_step)
%     phi = _Phi(wsize, tstep, n_coefs)
%     phiT = _PhiT(tstep, n_freq, n_step, n_times)
% 
%     if n_orient == 1:
%         lc = np.sum(G * G, axis=0)
%     else:
%         lc = np.empty(n_positions)
%         for j in range(n_positions):
%             G_tmp = G[:, (j * n_orient):((j + 1) * n_orient)]
%             lc[j] = linalg.norm(np.dot(G_tmp.T, G_tmp), ord=2)
% 
%     logger.info("Using block coordinate descent with active set approach")
%     X, Z, active_set, E, gap = _tf_mixed_norm_solver_bcd_active_set(
%         M, G, alpha_space, alpha_time, lc, phi, phiT, shape, Z_init=None,
%         n_orient=n_orient, maxit=maxit, tol=tol,
%         log_objective=log_objective, verbose=None)
% 
%     if np.any(active_set) and debias:
%         bias = compute_bias(M, G[:, active_set], X, n_orient=n_orient)
%         X *= bias[:, np.newaxis]
% 
%     if return_gap:
%         return X, active_set, E, gap
%     else:
%         return X, active_set, E

function tf_mixed_norm_solver()
  error('tf_mixed_norm_solver not implemented');
end




 

   


