% 
% @verbose
% def _mixed_norm_solver_cd(M, G, alpha, lipschitz_constant, maxit=10000,
%                           tol=1e-8, verbose=None, init=None, n_orient=1):
%     """Solve L21 inverse problem with coordinate descent."""
%     from sklearn.linear_model.coordinate_descent import MultiTaskLasso
% 
%     n_sensors, n_times = M.shape
%     n_sensors, n_sources = G.shape
% 
%     if init is not None:
%         init = init.T
% 
%     clf = MultiTaskLasso(alpha=alpha / len(M), tol=tol / sum_squared(M),
%                          normalize=False, fit_intercept=False, max_iter=maxit,
%                          warm_start=True)
%     clf.coef_ = init
%     clf.fit(G, M)
% 
%     X = clf.coef_.T
%     active_set = np.any(X, axis=1)
%     X = X[active_set]
%     gap, p_obj, d_obj, _ = dgap_l21(M, G, X, active_set, alpha, n_orient)
%     return X, active_set, p_obj
% 

function mixed_norm_solver_cd()
   % This will probably never be ported, because it relies on sklearn, and
   % porting THAT is way beyond what I'm willing to do.
   error('mixed_norm_solver_cd() not yet implemented');
end
