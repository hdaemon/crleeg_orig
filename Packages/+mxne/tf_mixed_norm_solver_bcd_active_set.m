

% 
% 
% @verbose
% def _tf_mixed_norm_solver_bcd_active_set(M, G, alpha_space, alpha_time,
%                                          lipschitz_constant, phi, phiT, shape,
%                                          Z_init=None, n_orient=1, maxit=200,
%                                          tol=1e-8, log_objective=True,
%                                          verbose=None):
% 
%     n_sensors, n_times = M.shape
%     n_sources = G.shape[1]
%     n_positions = n_sources // n_orient
% 
%     Z = dict.fromkeys(np.arange(n_positions), 0.0)
%     active_set = np.zeros(n_sources, dtype=np.bool)
%     active = []
%     if Z_init is not None:
%         if Z_init.shape != (n_sources, shape[1] * shape[2]):
%             raise Exception('Z_init must be None or an array with shape '
%                             '(n_sources, n_coefs).')
%         for ii in range(n_positions):
%             if np.any(Z_init[ii * n_orient:(ii + 1) * n_orient]):
%                 active_set[ii * n_orient:(ii + 1) * n_orient] = True
%                 active.append(ii)
%         if len(active):
%             Z.update(dict(zip(active, np.vsplit(Z_init[active_set],
%                      len(active)))))
% 
%     E = []
% 
%     candidates = range(n_positions)
%     d_obj = -np.inf
% 
%     while True:
%         Z_init = dict.fromkeys(np.arange(n_positions), 0.0)
%         Z_init.update(dict(zip(active, Z.values())))
%         Z, active_set, E_tmp, _ = _tf_mixed_norm_solver_bcd_(
%             M, G, Z_init, active_set, candidates, alpha_space, alpha_time,
%             lipschitz_constant, phi, phiT, shape, n_orient=n_orient,
%             maxit=1, tol=tol, log_objective=False, perc=None,
%             verbose=verbose)
%         E += E_tmp
% 
%         active = np.where(active_set[::n_orient])[0]
%         Z_init = dict(zip(range(len(active)), [Z[idx] for idx in active]))
%         candidates_ = range(len(active))
%         Z, as_, E_tmp, converged = _tf_mixed_norm_solver_bcd_(
%             M, G[:, active_set], Z_init,
%             np.ones(len(active) * n_orient, dtype=np.bool),
%             candidates_, alpha_space, alpha_time,
%             lipschitz_constant[active_set[::n_orient]], phi, phiT, shape,
%             n_orient=n_orient, maxit=maxit, tol=tol,
%             log_objective=log_objective, perc=0.5, verbose=verbose)
%         active = np.where(active_set[::n_orient])[0]
%         active_set[active_set] = as_.copy()
%         E += E_tmp
% 
%         converged = True
%         if converged:
%             Zd = np.vstack([Z[pos] for pos in range(len(Z)) if np.any(Z[pos])])
%             gap, p_obj, d_obj, _ = dgap_l21l1(
%                 M, G, Zd, active_set, alpha_space, alpha_time,
%                 phi, phiT, shape, n_orient, d_obj)
%             logger.info("\ndgap %.2e :: p_obj %f :: d_obj %f :: n_active %d"
%                         % (gap, p_obj, d_obj, np.sum(active_set) / n_orient))
%             if gap < tol:
%                 logger.info("\nConvergence reached!\n")
%                 break
% 
%     if active_set.sum():
%         Z = np.vstack([Z[pos] for pos in range(len(Z)) if np.any(Z[pos])])
%         X = phiT(Z)
%     else:
%         n_step = shape[2]
%         n_freq = shape[1]
%         Z = np.zeros((0, n_step * n_freq), dtype=np.complex)
%         X = np.zeros((0, n_times))
% 
%     return X, Z, active_set, E, gap

function tf_mixed_norm_solver_bcd_active_set()
  error('tf_mixed_norm_solver_bcd_active_set not implemented');
end