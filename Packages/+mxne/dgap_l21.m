
% 
% 
% def dgap_l21(M, G, X, active_set, alpha, n_orient):
%     """Duality gap for the mixed norm inverse problem.
% 
%     Parameters
%     ----------
%     M : array, shape (n_sensors, n_times)
%         The data.
%     G : array, shape (n_sensors, n_active)
%         The gain matrix a.k.a. lead field.
%     X : array, shape (n_active, n_times)
%         Sources.
%     active_set : array of bool, shape (n_sources, )
%         Mask of active sources.
%     alpha : float
%         The regularization parameter.
%     n_orient : int
%         Number of dipoles per locations (typically 1 or 3).
% 
%     Returns
%     -------
%     gap : float
%         Dual gap.
%     p_obj : float
%         Primal objective.
%     d_obj : float
%         Dual objective. gap = p_obj - d_obj.
%     R : array, shape (n_sensors, n_times)
%         Current residual (M - G * X).
% 
%     References
%     ----------
%     .. [1] A. Gramfort, M. Kowalski, M. Hamalainen,
%        "Mixed-norm estimates for the M/EEG inverse problem using accelerated
%        gradient methods", Physics in Medicine and Biology, 2012.
%        http://dx.doi.org/10.1088/0031-9155/57/7/1937
%     """


% 
%     gap = p_obj - d_obj
%     return gap, p_obj, d_obj, R
function [gap,varargout] = dgap_l21(M,G,X,active_set,alpha,n_orient)
  %     GX = np.dot(G[:, active_set], X)
%     R = M - GX
%     penalty = norm_l21(X, n_orient, copy=True)
%     nR2 = sum_squared(R)
%     p_obj = 0.5 * nR2 + alpha * penalty
   GX = G(:,active_set)*X;
   R = M - GX;
   
   penalty = mxne.norm_l21(X,n_orient);
   nR2 = norm(R,'fro')^2;
   p_obj = 0.5 * nR2 + alpha*penalty;
      
%     dual_norm = norm_l2inf(np.dot(G.T, R), n_orient, copy=False)
%     scaling = alpha / dual_norm
%     scaling = min(scaling, 1.0)
%     d_obj = (scaling - 0.5 * (scaling ** 2)) * nR2 + scaling * np.sum(R * GX)
   
   dual_norm = mxne.norm_l2inf(G.'*R,n_orient);
   scaling = alpha/dual_norm;
   scaling = min(scaling,1.0);
   d_obj = (scaling - 0.5*(scaling.^2)) * nR2 + scaling*sum(sum(R.*GX,1),2);
   
   gap = p_obj - d_obj;   
  
   if nargout>=2, varargout{1} = p_obj; end;
   if nargout>=3, varargout{2} = d_obj; end;
   if nargout>=4, varargout{3} = R; end;
  
end