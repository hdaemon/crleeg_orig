

%
% @verbose
% def _mixed_norm_solver_prox(M, G, alpha, lipschitz_constant, maxit=200,
%                             tol=1e-8, verbose=None, init=None, n_orient=1):
%     """Solve L21 inverse problem with proximal iterations and FISTA."""


function [out,varargout] = mixed_norm_solver_prox(M,G,alpha,lipschitz_constant,varargin)
  p = inputParser;
  p.addRequired('M');
  p.addRequired('G');
  p.addRequired('alpha');
  p.addRequired('lipschitz_constant');
  p.addParamValue('maxit',200);
  p.addParamValue('tol',1e-8);
  p.addParamValue('verbose',false);
  p.addParamValue('init',[]);
  p.addParamValue('n_orient',1);
  p.parse(M,G,alpha,lipschitz_constant,varargin{:});
  
  maxit = p.Results.maxit;
  tol = p.Results.tol;
  verbose = p.Results.verbose;
  init = p.Results.init;
  n_orient = p.Results.n_orient;
  
  %% Get number of sensors, sources, and times
  %     n_sensors, n_times = M.shape
  %     n_sensors, n_sources = G.shape
  %
  
  [n_sensors,n_times] = size(M);
  [n_sensors,n_sources] = size(G);
  
  %% THis is 
  %     if n_sources < n_sensors:
  %         gram = np.dot(G.T, G)
  %         GTM = np.dot(G.T, M)
  %     else:
  %         gram = None
  
  if n_sources < n_sensors
    gram = G.'*G;
    GTM = G.'*M;
  else
    gram = [];
  end;
  
  %
  %     if init is None:
  %         X = 0.0
  %         R = M.copy()
  %         if gram is not None:
  %             R = np.dot(G.T, R)
  %     else:
  %         X = init
  %         if gram is None:
  %             R = M - np.dot(G, X)
  %         else:
  %             R = GTM - np.dot(gram, X)
  
  if isempty(init)
    X = 0;
    R = M;
    if ~isempty(gram)
      R = G.'*R;
    end
  else
    X = init;
    if isempty(gram)
      R = M - G*X;
    else
      R = GTM - gram*X;
    end
  end;
  
  %
  %     t = 1.0
  %     Y = np.zeros((n_sources, n_times))  # FISTA aux variable
  %     E = []  # track primal objective function
  %     highest_d_obj = - np.inf
  %     active_set = np.ones(n_sources, dtype=np.bool)  # start with full AS
  
  t = 1;
  Y = zeros(n_sources,n_times);
  E = [];
  highest_d_obj = -inf;
  active_set = true(n_sources,1);
  
  %
  %     for i in range(maxit):
  %         X0, active_set_0 = X, active_set  # store previous values
  %         if gram is None:
  %             Y += np.dot(G.T, R) / lipschitz_constant  # ISTA step
  %         else:
  %             Y += R / lipschitz_constant  # ISTA step
  %         X, active_set = prox_l21(Y, alpha / lipschitz_constant, n_orient)
  %
  %         t0 = t
  %         t = 0.5 * (1.0 + sqrt(1.0 + 4.0 * t ** 2))
  %         Y.fill(0.0)
  %         dt = ((t0 - 1.0) / t)
  %         Y[active_set] = (1.0 + dt) * X
  %         Y[active_set_0] -= dt * X0
  %         Y_as = active_set_0 | active_set
  %
  %         if gram is None:
  %             R = M - np.dot(G[:, Y_as], Y[Y_as])
  %         else:
  %             R = GTM - np.dot(gram[:, Y_as], Y[Y_as])
  %
  %         _, p_obj, d_obj, _ = dgap_l21(M, G, X, active_set, alpha, n_orient)
  %         highest_d_obj = max(d_obj, highest_d_obj)
  %         gap = p_obj - highest_d_obj
  %         E.append(p_obj)
  %         logger.debug("p_obj : %s -- gap : %s" % (p_obj, gap))
  %         if gap < tol:
  %             logger.debug('Convergence reached ! (gap: %s < %s)' % (gap, tol))
  %             break
  
  for i = 1:maxit
    X0 = X;
    active_set_0 = active_set;
    
    if isempty(gram)
      Y = Y + (G.'*R)./lipschitz_constant;
    else
      Y = Y + R./lipschitz_constant;
    end
    
    [X, active_set] = mxne.prox_l21(Y,alpha./lipschitz_constant,n_orient);
    
    t0 = t;
    t = 0.5 * (1 + sqrt(1 + 4*t^2));
    Y = zeros(size(Y));
    dt = ((t0 - 1)./t);
    Y(active_set,:) = (1 + dt) * X;
    Y(active_set_0,:) = Y(active_set_0,:) - dt*X0;
    Y_as = active_set_0 | active_set;
    
    if isempty(gram)
      R = M - G(:,Y_as)*Y(Y_as,:);
    else
      R = GTM - gram(:,Y_as)*Y(Y_as,:);
    end
    
    [~,p_obj,d_obj] = mxne.dgap_l21(M,G,X,active_set,alpha,n_orient);
    
    highest_d_obj = max(d_obj,highest_d_obj);
    gap = p_obj - highest_d_obj;
    E(end+1) = p_obj;
    
    if verbose
      disp(['L21: R:' num2str(norm(R,'fro')/norm(M,'fro')) ' p_obj:' num2str(p_obj) ' d_obj:' ...
              num2str(d_obj) ' gap:' num2str(gap)]);
    end
    
    if gap<tol
      break;
    end;
    
    
  end
  
  
  
  %     return X, active_set, E
  out = X;
  if nargout>=2, varargout{1} = active_set; end;
  if nargout>=3, varargout{2} = E; end;
  
  
end
