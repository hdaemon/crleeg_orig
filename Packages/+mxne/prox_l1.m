% def prox_l1(Y, alpha, n_orient):
%     """Proximity operator for l1 norm with multiple orientation support.
% 
%     Please note that this function computes a soft-thresholding if
%     n_orient == 1 and a block soft-thresholding (L2 over orientation and
%     L1 over position (space + time)) if n_orient == 3. See also [1]_.
% 
%     Parameters
%     ----------
%     Y : array, shape (n_sources, n_coefs)
%         The input data.
%     alpha : float
%         The regularization parameter.
%     n_orient : int
%         Number of dipoles per locations (typically 1 or 3).
% 
%     Returns
%     -------
%     Y : array, shape (n_sources, n_coefs)
%         The output data.
%     active_set : array of bool, shape (n_sources, )
%         Mask of active sources.
% 
%     References
%     ----------
%     .. [1] A. Gramfort, D. Strohmeier, J. Haueisen, M. Hamalainen, M. Kowalski
%        "Time-Frequency Mixed-Norm Estimates: Sparse M/EEG imaging with
%        non-stationary source activations",
%        Neuroimage, Volume 70, pp. 410-422, 15 April 2013.
%        DOI: 10.1016/j.neuroimage.2012.12.051
% 
%     Example
%     -------
%     >>> Y = np.tile(np.array([1, 2, 3, 2, 0], dtype=np.float), (2, 1))
%     >>> Y = np.r_[Y, np.zeros_like(Y)]
%     >>> print(Y)
%     [[ 1.  2.  3.  2.  0.]
%      [ 1.  2.  3.  2.  0.]
%      [ 0.  0.  0.  0.  0.]
%      [ 0.  0.  0.  0.  0.]]
%     >>> Yp, active_set = prox_l1(Y, 2, 2)
%     >>> print(Yp)
%     [[ 0.          0.58578644  1.58578644  0.58578644  0.        ]
%      [ 0.          0.58578644  1.58578644  0.58578644  0.        ]]
%     >>> print(active_set)
%     [ True  True False False]
%     """
%     n_positions = Y.shape[0] // n_orient
%     norms = np.sqrt((Y * Y.conj()).real.T.reshape(-1, n_orient).sum(axis=1))
%     # Ensure shrink is >= 0 while avoiding any division by zero
%     shrink = np.maximum(1.0 - alpha / np.maximum(norms, alpha), 0.0)
%     shrink = shrink.reshape(-1, n_positions).T
%     active_set = np.any(shrink > 0.0, axis=1)
%     shrink = shrink[active_set]
%     if n_orient > 1:
%         active_set = np.tile(active_set[:, None], [1, n_orient]).ravel()
%     Y = Y[active_set]
%     if len(Y) > 0:
%         for o in range(n_orient):
%             Y[o::n_orient] *= shrink
%     return Y, active_set

function out = prox_l1()
  out = 0;
  error('prox_l1() not yet implemented');
end