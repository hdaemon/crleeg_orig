% 
% 
% def norm_l1_tf(Z, shape, n_orient):
%     """L1 norm for TF."""
%     if Z.shape[0]:
%         n_positions = Z.shape[0] // n_orient
%         Z_ = np.sqrt(np.sum((np.abs(Z) ** 2.).reshape((n_orient, -1),
%                      order='F'), axis=0))
%         Z_ = Z_.reshape((n_positions, -1), order='F').reshape(*shape)
%         l1_norm = (2. * Z_.sum(axis=2).sum(axis=1) - np.sum(Z_[:, 0, :],
%                    axis=1) - np.sum(Z_[:, -1, :], axis=1))
%         l1_norm = l1_norm.sum()
%     else:
%         l1_norm = 0.
%     return l1_norm

function norm_l1_tf()
  error('norm_l1_tf not implemented');
end