% 
% def norm_l21(A, n_orient, copy=True):
%     """L21 norm."""
%     if A.size == 0:
%         return 0.0
%     if copy:
%         A = A.copy()
%     return np.sum(np.sqrt(groups_norm2(A, n_orient)))
% 
function out = norm_l21(A,n_orient)
 if prod(size(A))==0, out = 0; return; end;
 out = sum(sqrt(mxne.groups_norm2(A,n_orient)));
end
