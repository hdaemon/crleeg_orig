% def groups_norm2(A, n_orient):
%     """Compute squared L2 norms of groups inplace."""
%     n_positions = A.shape[0] // n_orient
%     return np.sum(np.power(A, 2, A).reshape(n_positions, -1), axis=1)
% 

function out = groups_norm2(A, n_orient)
  % Compute 2-norm for each group.
  %
  % Inputs
  % ------
  %   A : Matrix of Size (n_groups*n_orient by N)
  %   n_orient: Number of basis functions per group
  %
  %
  % THIS ASSUMES THAT THE ROWS OF A INDEX ACROSS n_orient FIRST, FOLLOWED
  % BY n_positions!!!
  %
  n_positions = size(A,1)/n_orient;
 % out = sum(reshape(A.^2,n_positions,[]),2);
  
  tmp = reshape(sum(A.^2,2),n_orient,[]);
  out = sum(tmp,1);
  out = out(:);

end
