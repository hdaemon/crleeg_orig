% 
% def norm_l2inf(A, n_orient, copy=True):
%     """L2-inf norm."""
%     if A.size == 0:
%         return 0.0
%     if copy:
%         A = A.copy()
%     return sqrt(np.max(groups_norm2(A, n_orient)))
% 
function out = norm_l2inf(A,n_orient)
 if numel(A)==0, out = 0; return; end;
 out = sqrt(max(mxne.groups_norm2(A,n_orient)));
end