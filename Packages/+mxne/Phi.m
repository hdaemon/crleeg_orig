
% 
% class _Phi(object):
%     """Have phi stft as callable w/o using a lambda that does not pickle."""
% 
%     def __init__(self, wsize, tstep, n_coefs):  # noqa: D102
%         self.wsize = wsize
%         self.tstep = tstep
%         self.n_coefs = n_coefs
% 
%     def __call__(self, x):  # noqa: D105
%         return stft(x, self.wsize, self.tstep,
%                     verbose=False).reshape(-1, self.n_coefs)

classdef Phi
  
  methods
    
    function obj = Phi()
    end
  end
  
end