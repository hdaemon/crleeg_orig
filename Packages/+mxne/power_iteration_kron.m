% def power_iteration_kron(A, C, max_iter=1000, tol=1e-3, random_state=0):
%     """Find the largest singular value for the matrix kron(C.T, A).
% 
%     It uses power iterations.
% 
%     Parameters
%     ----------
%     A : array
%         An array
%     C : array
%         An array
%     max_iter : int
%         Maximum number of iterations
%     random_state : int | RandomState | None
%         Random state for random number generation
% 
%     Returns
%     -------
%     L : float
%         largest singular value
% 
%     Notes
%     -----
%     http://en.wikipedia.org/wiki/Power_iteration
%     """
%     AS_size = C.shape[0]
%     rng = check_random_state(random_state)
%     B = rng.randn(AS_size, AS_size)
%     B /= linalg.norm(B, 'fro')
%     ATA = np.dot(A.T, A)
%     CCT = np.dot(C, C.T)
%     L0 = np.inf
%     for _ in range(max_iter):
%         Y = np.dot(np.dot(ATA, B), CCT)
%         L = linalg.norm(Y, 'fro')
% 
%         if abs(L - L0) < tol:
%             break
% 
%         B = Y / L
%         L0 = L
%     return L

function power_iteration_kron()
   error('power_iteration_kron() not implemented');
end