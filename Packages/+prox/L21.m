function vecOut = L21(vecIn,lambda,weightVec)
% Proximity operator for the L21 norm


assert(validateattributes(vecIn,{'numeric'},{'matrix'}),'ERROR');
          
if ~exist('weightVec','var'), weightVec = ones(size(vecIn)); end;
assert(all(size(vecIn,1)==size(weightVec,1)),'Weight vector must be the same size as vecIn');

YsNrm = sum(vecIn.^2,2);

rowScale = max(0, ( 1 - (lambda*sqrt(weightVec))./YsNrm )) );

vecOut = vecIn.*repmat(rowScale,1,size(vecIn,2));



end


