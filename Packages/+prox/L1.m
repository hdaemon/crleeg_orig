function vecOut = L1(vecIn,lambda,weightVec)
% Proximity operator for the L1 norm


validateattributes(vecIn,{'numeric'},{'vector'});
          
if ~exist('weightVec','var'), weightVec = ones(size(vecIn)); end;
assert(all(size(vecIn)==size(weightVec)),'Weight vector must be the same size as vecIn');

vecOut = sign(vecIn).*softThresh(abs(vecIn)-lambda*weightVec);

end


function out = softThresh(in)

out = in;
out(out<0) = 0;

end