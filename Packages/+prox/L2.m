function vecOut = L2(vecIn,lambda,weightVec)
% Proximity operator for the L2 norm


validateattributes(vecIn,{'numeric'},{'vector'});
          
if ~exist('weightVec','var'), weightVec = ones(size(vecIn)); end;
assert(all(size(vecIn)==size(weightVec)),'Weight vector must be the same size as vecIn');

vecOut = vecIn.*(1./(1 + lambda*weightVec));



end