classdef cnlClickableImage 
  % classdef cnlClickableImage
  %
  %
  % Written By: Damon Hyde
  % Last Edited: March 2015
  % Part of the cnlEEG Project
  %
  
  properties
    fhnd_MainPlot
    fhnd_ButtonDwn
  end
  
  methods
    
    function objOut = cnlClickableImage(varargin)
      allH = image(varargin{:});
      set(allH,'buttonDwnFcn',{@buttonDown,allH});
    
  