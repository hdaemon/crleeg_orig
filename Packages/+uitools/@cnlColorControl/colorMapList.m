function maps = colorMapList
% COLORMAPLIST Return list of available colormaps
%
% function maps = colorMapList

maps(1).name = 'jet';
maps(2).name = 'gray';
maps(3).name = 'hot';
maps(4).name = 'parula';
maps(5).name = 'hsv';
maps(6).name = 'bone';
maps(7).name = 'cool';
maps(8).name = 'spring';
maps(9).name = 'summer';
maps(10).name = 'autumn';
maps(11).name = 'winter';
maps(12).name = 'copper';
maps(13).name = 'pink';
maps(14).name = 'lines';
maps(15).name = 'colorcube';
maps(16).name = 'prism';


end