function types = dispTypeList
% DISPTYPELIST Return a list of possible display types
%
% function types = dispTypeList
%
% Written By: Damon Hyde
% April 2015
% Part of the cnlEEG Project
%


types(1).name = 'true';
types(1).fPre  = @(x)x;
types(1).fPost = @(x)x;

types(2).name = 'norm';
types(2).fPre  = @(x)x.^2;
types(2).fPost = @(x)sqrt(x);

types(3).name = 'abs';
types(3).fPre  = @(x)abs(x);
types(3).fPost = @(x)x;

types(4).name = 'parcel';
types(4).fPre  = @(x)mod(x,30);
types(4).fPost = @(x)x;

types(5).name = 'exp';
types(5).fPre  = @(x)exp(x);
types(5).fPost = @(x)x;

types(6).name = 'log';
types(6).fPre  = @(x)logFunc(x);
types(6).fPost = @(x)x;


end

function x = logFunc(x)
  x = abs(x);
  Q = (x==0);
  x = log10(x);
  x(Q) = 0;  
end


  