classdef cnlColorControl < uitools.cnlUIObj
  % uitools class for controlling display colormaps
  %
  % classdef cnlColorControl < uitools.cnlUIObj
  %
  % Properties:
  %
  % Written By: Damon Hyde
  % Last Edited: Aug 12, 2015
  % Part of the cnlEEG Project
  %
  
  properties
    updatefunction
  end
  
  properties (Dependent = true)
    currCMap
    currType
  end
  
  properties (Hidden = true)
    colorMapSel
    dispTypeSel
  end
  
  methods
    
    function objOut = cnlColorControl(varargin)
      
      % Set up input parsing to make sure the defaults passed to cnlUIObj
      % are appropriate for a cnlColorControl panel
      p = inputParser;
      addParamValue(p,'parent',[]);
      addParamValue(p,'origin',[335 10]);
      addParamValue(p,'size',[200 75]);
      addParamValue(p,'units','pixels');
      addParamValue(p,'title','Color Control');
      parse(p,varargin{:});
      
      % Initialize superclass
      objOut = objOut@uitools.cnlUIObj('parent',p.Results.parent, ...
                  'origin',p.Results.origin,'size',p.Results.size,....
                  'title',p.Results.title,'units',p.Results.units);     
      
      % Get an easier reference to the main panel.
      dispControl = objOut.panel;

      % Set up the color map selection tool
      cmapList = uitools.cnlColorControl.colorMapList;
      colorMapLabel = uicontrol('Style','text','Parent',dispControl,...
        'String',['Colormap:'],'Position',[5 5 75 22]);
      objOut.colorMapSel = uicontrol('Style','popupmenu','Parent',dispControl,...
        'String',{cmapList.name},...
        'CallBack',@objOut.updateImage, ...
        'Value',2,'Position', [80 5 100 22]);
      
      % Set up the display type selection tool
      dispList = uitools.cnlColorControl.dispTypeList;
      dispTypeLabel = uicontrol('Style','text','Parent',dispControl,...
        'String',['Display:'],'Position',[5 30 75 22]);
      objOut.dispTypeSel = uicontrol('Style','popupmenu','Parent',dispControl,...
        'String',{dispList.name},...
        'CallBack',@objOut.updateImage,...
        'Value',3,'Position', [80 30 100 22]);
   
      % Set everything that's INSIDE the main panel to use normalized
      % units.  Main control of sizing is thus passed to objOut.normalized
      % (ie: it's all linked to the settings of the parent uipanel)
      set(colorMapLabel,'FontUnits','normalized');
      set(colorMapLabel,'Units','normalized');
      set(dispTypeLabel,'FontUnits','normalized');
      set(dispTypeLabel,'Units','normalized');
      set(objOut.colorMapSel,'Units','normalized');
      set(objOut.colorMapSel,'FontUnits','normalized');
      set(objOut.dispTypeSel,'Units','normalized');
      set(objOut.dispTypeSel,'FontUnits','normalized');
      set(objOut.panel,'Position',[objOut.origin objOut.size]);
      set(objOut.panel,'visible','on'); 
      
    end    
    
    function updateImage(obj,h,EventData)
      if ~isempty(obj.updatefunction)
      obj.updatefunction();
      end;
    end
    
    function out = get.currCMap(obj)
      % function out = get.currCMap(obj)
      %
      %
      
      curr = get(obj.colorMapSel,'Value');
      allMaps = uitools.cnlColorControl.colorMapList;
      out = feval(allMaps(curr).name,256);            
    end
    
    function set.currCMap(obj,val)
      % function set.currCMap(obj,val)
      %
      %
      allMaps = uitools.cnlColorControl.colorMapList;
      
      idx = 0;
      found = false;
      while ~found
        idx = idx+1;
        
        if idx>numel(allMaps)
          error('cnlColorControl:unknownColorMap',...
            'Unknown color map name');
        end
        
        found = strcmpi(val,allMaps(idx).name);
      end;
      set(obj.colorMapSel,'Value',idx);            
    end
    
    function out = get.currType(obj)
      % function out = get.currType(obj)
      %            
      %
      
      allTypes = uitools.cnlColorControl.dispTypeList;
      out = allTypes(get(obj.dispTypeSel,'Value')).name;
    end
    
    function set.currType(obj,val)
      % function set.currType(obj)
      %
      %
      
      allTypes = uitools.cnlColorControl.dispTypeList;
      
      idx = 0;
      found = false
      while ~found
        idx = idx+1;
        if idx>numel(allTypes)
          error('cnlColorControl:unknownType',...
            'Unknown display type');
        end
        found = strcmpi(val,allTypes(idx).name);
      end;
      set(obj.dispTypeSel,'Value',idx);
    end
      
    
  end
  
  methods (Static = true)
    types = dispTypeList;
    m,aps = colorMapList;
  end
  
  
end
