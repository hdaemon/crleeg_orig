function updateImage(obj)
% cnlMiniPlot.UPDATEIMAGE : Update displayed image
%
% function updateImage(obj)
%
% Written By: Damon Hyde
% Last Edited: June 23, 2015
% Part of the cnlEEG Project
%

if isempty(obj.data), return; end;

if numel(obj.xVals)==1
  plotOpts = 'x';
else
  plotOpts = '';
end

% Update Data Plot
axes(obj.axes);
plot(obj.xVals,obj.data,plotOpts);

% Plot Vertical Lines to Denote Selection Region
YLim = get(obj.axes,'YLim');
hold on;
XVal1 = obj.xVals(obj.windowStart);
obj.vLineLeft = plot([XVal1 XVal1], [YLim(1) YLim(2)],'r');
set(obj.vLineLeft,'linewidth',2);
XVal2 = obj.xVals(obj.windowEnd);
obj.vLineRight = plot([XVal2 XVal2], [YLim(1) YLim(2)],'r');
set(obj.vLineRight,'linewidth',2);
hold off;

XLim(1) = obj.xVals(1);
XLim(2) = obj.xVals(end);

if XLim(1)==XLim(2)
  XLim(1) = XLim(1) - 0.1;
  XLim(2) = XLim(2) + 0.1;
end;

axis([XLim(1) XLim(2) YLim(1) YLim(2)]);

% Evaluate callback function, if provided
notify(obj,'updatedImg');
if ~isempty(obj.fhnd_UpdateCallback)&&isa(obj.fhnd_UpdateCallback,'function_handle')
  feval(obj.fhnd_UpdateCallback);
end;

end