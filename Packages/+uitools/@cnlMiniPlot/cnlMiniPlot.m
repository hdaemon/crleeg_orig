classdef cnlMiniPlot < uitools.cnlUIObj
  % CNLMINIPLOT Mini data plot with range selection
  %
  % A cnlUI object for selecting a subset of a longer time series.
  %
  % Properties
  %   
  %
  % Written By: Damon Hyde
  % April 2015
  % Part of the cnlEEG Project
  %
  
  properties
    xVals
    
    fhnd_UpdateCallback;
    
  end
  
  properties (Dependent=true)
    windowData
    windowRange
    windowIdx
    windowXVals
    fullRange
  end
  
  properties (Hidden=true)
    axes
    windowStart = 1;
    windowSize
  end
  
  properties (Hidden=true,Dependent=true)
    windowEnd;
  end
  
  events
    updatedImg
  end
  
  properties (Hidden=true,SetAccess=protected)
    data
        
    % Properties related to zooming in/out
    zoomLabel
    plusButton
    minusButton
    zoomScale = 0.1;
    
    % Properties related to shifting view window
    shiftLabel
    shiftLeft
    shiftRight
    shiftScale = 0.1;
    
    % Properties related to Manually Selecting the Window
    selStart
    selEnd
    
    % Data vis properties
    vLineLeft
    vLineRight
  end
    
  methods
    
    function objOut = cnlMiniPlot(varargin)
      % function objOut = cnlMiniPlot(varargin)
      %
      %
      
      % Input Parsing
      p = inputParser;
      p.KeepUnmatched = true;
      addParamValue(p,'parent',[]);
      addParamValue(p,'origin',[10 10]);
      addParamValue(p,'size',[700 100]);
      addParamValue(p,'units','pixels');
      addParamValue(p,'title','');
      addParamValue(p,'data',[]);
      addParamValue(p,'fhnd',[]);
      addParamValue(p,'xVals',[]);
      parse(p,varargin{:});
      
      % Initialize parent class
      objOut = objOut@uitools.cnlUIObj('parent',p.Results.parent,...
        'origin',p.Results.origin,'size',p.Results.size,...
        'title',p.Results.title,'units',p.Results.units);
      
      % Set figure size
      uitools.setMinFigSize(gcf,objOut.origin,objOut.size,5);
      
      % Read data from input parser
      objOut.data = p.Results.data;
      objOut.xVals = p.Results.xVals;
      
      % Initialize Axes and UI Controls
      objOut.initialize;
      
      % Set Initial Window Size
      objOut.windowStart = 1;
      objOut.windowSize  = min([max([75 0.1*size(objOut.data,1)]) size(objOut.data,1)]);
      
      % Update Image
      objOut.updateImage;
      
    end
            
    function set.xVals(obj,val)
      assert(isempty(val)||isvector(val),'xVals must be in vector form');
      assert(isempty(val)||(numel(val)==size(obj.data,1)),...
        'Number of values must match size(obj.data,1)');
      
      obj.xVals = val;
    end
    
    function out = get.xVals(obj)
      if isempty(obj.xVals)
        out = 1:size(obj.data,1);
      else
        out = obj.xVals;
      end;
    end;
    
    function out = get.windowEnd(obj)      
      out = obj.windowStart + obj.windowSize;
    end;
    
    function set.windowStart(obj,val)
      % function set.windowStart(obj,val)
      %
      %
      
      % Window can't start before the first sample
      if val<1, val = 1; end;
      
      % Window can't start after the last sample
      if val>size(obj.data,1), val = size(obj.data,1); end;
      
      obj.windowStart = round(val);
      
      % Resize window if necessary
      if obj.windowEnd>size(obj.data,1)
        obj.windowSize = size(obj.data,1) - obj.windowStart;
      end
    end
    
    function set.windowSize(obj,val)
      
      % If size has become zero, try to make it larger
      if val==0, val = 0.1*size(obj.data,1);end;
      
      % If we provide a negative value, don't change anything.
      if (val<=0),  val = obj.windowSize; end
      
      % Always try to change size by at least one sample
      delta = val - obj.windowSize;
      if abs(delta)<1, delta = sign(delta); end;
      newSize = obj.windowSize + round(delta);
      
      if isempty(newSize), newSize = 1; end;
      if newSize<1, newSize = 1; end;
      
      % If the window will fall off the end of the plot, push the start
      % point back
      if (obj.windowStart+newSize)>size(obj.data,1)
        obj.windowStart = size(obj.data,1)-newSize;
      end;
      
      % Check again.  This time shrink the window size if necessary
      if (obj.windowStart+newSize)>size(obj.data,1)
        newSize = size(obj.data,1)-1;
      end
      
      obj.windowSize = newSize;
    end;
    
    function dataOut = get.windowData(obj)
      dataOut = obj.data(obj.windowIdx,:);
    end
    
    function rangeOut = get.windowRange(obj)
      windowMin = min(obj.windowData(:));
      windowMax = max(obj.windowData(:));
      rangeOut = [windowMin windowMax];
    end
    
    function idx = get.windowIdx(obj)   
      
      idx = obj.windowStart:(obj.windowStart+obj.windowSize-1);
    end
    
    function out = get.data(obj)
      % If obj.data is a function handle, execute it.
      if isa(obj.data,'function_handle')
        out = feval(obj.data);
      else
        out = obj.data;
      end;
    end;   
    
    % Methods with their own mfiles.
    initialize(obj);
    initializeSelectButtons(obj);
    initializeShiftControls(obj);
    initializeZoomControls(obj);
    leftShift(obj);
    rightShift(obj);
    out = sampleNum(obj,val);
    setEnd(obj);
    setStart(obj);
    updateImage(obj);
    zoomMinus(obj);
    zoomPlus(obj);
    
    
  end
  
end
