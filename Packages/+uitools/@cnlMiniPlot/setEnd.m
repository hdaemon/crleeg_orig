function setEnd(obj)
% SETEND Callback for "Set End" Button
%
% uitools.cnlMiniPlot
%
% function setEnd(obj)
%
% Waits for buttonpress, and identifies the X-value associated with the
% clicked point, to change the window size in a cnlMiniPlot viewer.
%
% Written By: Damon Hyde
% Last Edited: Aug 14, 2015
% Part of the cnlEEG Project
%

% Wait for button press and fetch location
k = 1;
while k==1
  k = waitforbuttonpress;
end
newPos = get(obj.axes,'CurrentPoint');

% Update Window Size
obj.windowSize = obj.sampleNum(newPos(1,1))-obj.windowStart;

% Update Image
obj.updateImage;

end