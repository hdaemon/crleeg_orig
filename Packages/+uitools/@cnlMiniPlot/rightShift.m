function rightShift(obj)
% RIGHTSHIFT Callback for right shift button.
%
% function rightShift(obj)

%disp('Shifting right');
newStart = obj.windowStart + (obj.shiftScale)*obj.windowSize;
if round(newStart)==obj.windowStart,newStart = obj.windowStart + 1; end;
obj.windowStart = newStart;
obj.updateImage;
end