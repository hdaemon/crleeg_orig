function zoomPlus(obj)
% ZOOMPLUS Callback for zoom minus button.
%
% function zoomPlus(obj)
%
%
%
obj.windowSize = (1-obj.zoomScale)*obj.windowSize;
obj.updateImage;
end