function setStart(obj)
% SETSTART Callback for "Set Start" button
%
% function setStart(obj)
%

%disp('Setting time');
%disp('Waiting for button press')
k = 1;
while k==1
  k = waitforbuttonpress;
end;
newPos = get(obj.axes,'CurrentPoint');

obj.windowStart = obj.sampleNum(newPos(1,1));

obj.updateImage;
end