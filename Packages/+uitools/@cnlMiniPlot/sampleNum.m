function nearest = sampleNum(obj,val)
% SAMPLENUM Given an X-value, returns index of nearest sample
%
% function nearest = sampleNum(obj,val)
%

nearest = abs(val-obj.xVals);
nearest = find(nearest==min(nearest));

end