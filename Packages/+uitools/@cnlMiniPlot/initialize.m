function initialize(obj)
% INITIALIZE Initialize cnlMiniPlot UI
%
% function initialize(obj)
%
% Full initialization of the cnlMiniPlot UI object. 
%
% Written By: Damon Hyde
% April 2015
% Part of the cnlEEG Project
%

if ishandle(obj.panel)
  obj.axes = axes('Parent',obj.panel,'Units','normalized',...
    'Position',[0.05 0.45 0.93 0.5]);
  
  obj.initializeZoomControls;
  obj.initializeShiftControls;
  obj.initializeSelectButtons;
  
  obj.windowStart = 1;
  obj.windowSize  = 0.1*size(obj.data,1);
else
  warning('cnlMiniPlot:panelNotInitialized',...
    ['Cannot initialize controls.  Main uipanel has not been initialized']);
end

end