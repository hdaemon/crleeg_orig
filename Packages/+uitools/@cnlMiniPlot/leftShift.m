function leftShift(obj)
% LEFTSHIFT Callback for left shift button.
%
% function leftShift(obj)
%
%
%disp('Shifting left');
newStart = obj.windowStart - (obj.shiftScale)*obj.windowSize;
if round(newStart)==obj.windowStart, newStart = obj.windowStart -1; end;
obj.windowStart = newStart;
obj.updateImage;
end