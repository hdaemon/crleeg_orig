classdef cnlDataPlot < uitools.cnlUIObj
  % CNLDATAPLOT Data viewer/explorer for large data sets.
  %
  % classdef CNLDATAPLOT < uitools.cnlUIObj
  %
  % Basic Usage:
  %   CNLDATAPLOT('data',DATA,'labels',LABELS)
  % With:
  %    DATA : nTime X nElectrode Data Array
  %  LABELS : nElectrode Element cell array of electrode labels
  %
  % Written By: Damon Hyde
  % April 2015
  % Part of the cnlEEG project.
  %
  
  % Observable Input Data
  properties (SetObservable,AbortSet)
    % Data
    data
    labels
    xVals
  end
  
  properties
    selectedX
    
    fhnd_UpdateSelectedX;
    
    range % Current range selected in miniplot
  end
  
  properties (Hidden=true)
    mainaxes % Main Plot window
    mainplot
    
    miniplot % Mini Data Viewer/Selection
    
    zoomSlider
    selectSlider
    scaleSlider    
    
    playControls
    listenerIdx
  end
  
  properties (Constant=true, Hidden = true)
    WINDOW_SHIFT_FORWARD = 0.75;
    WINDOW_SHIFT_BACKWARD = 0.75;
    
  end
  
  methods
    
    function objOut = cnlDataPlot(varargin)
      % CNLDATAPLOT 2d Plot of Matrix Data
      %
      % function objOut = cnlDataPlot(varargin)
      
      warning(['cnlDataPlot is deprecated and will be removed in a future version. ' ...
               'Use uitools.plots.dataexplorer instead']);
      %% Input Parsing
      p = inputParser;
      p.KeepUnmatched = true;
      addRequired(p,'data');
      addOptional(p,'labels',[]);
      addParamValue(p,'parent',[]);
      addParamValue(p,'origin',[10 10]);
      addParamValue(p,'size',[700 700]);
      addParamValue(p,'title','');
      %addParamValue(p,'data',[]);
      %addParamValue(p,'labels',[]);
      addParamValue(p,'units','pixels');
      addParamValue(p,'fhnd',[]);
      addParamValue(p,'xVals',[]);
      parse(p,varargin{:});
      
      %% Set up initial cnlUIObj
      objOut = objOut@uitools.cnlUIObj('parent',p.Results.parent,...
        'origin',p.Results.origin,'size',p.Results.size,...
        'title',p.Results.title,'Units',p.Results.units);
      
      uitools.setMinFigSize(gcf,objOut.origin,objOut.size,5);
      
      objOut.data   = p.Results.data;
      objOut.labels = p.Results.labels;
      objOut.xVals  = p.Results.xVals;
      
      %% Initialize Mini Data Plot
      % Point its data to the data in the cnlDataPlot object, and it's
      % update callback to update the image in the main frame.
      objOut.miniplot = uitools.cnlMiniPlot(...
        'Parent',objOut.panel,...
        'Units','normalized',...
        'origin',[0.01 0.01],...
        'size', [0.98 0.2],...
        'data',@objOut.data);
      objOut.listenerIdx{1} = addlistener(objOut.miniplot,'updatedImg',...
        @(obj,evt) objOut.updateImage);
      
      %% Set up the play controls
      objOut.playControls = uitools.cnlPlayControls(...
        'Parent',objOut.panel,...
        'Units','normalized',...
        'origin',[0.39 0.215],...
        'size',[0.6 0.06],...
        'range',[1 size(objOut.data,1)]);
      objOut.listenerIdx{2} = addlistener(objOut.playControls,'updatedIdx',...
        @(obj,evt) objOut.updateImage);      
           
      %% Set up the main data plot using a toggle plot
      objOut.mainplot = uitools.plots.toggle(...
         objOut.miniplot.windowData,...
        'Parent',objOut.panel,...
        'Origin',[0.01 0.23],...
        'Size',[0.98 0.78],...
        'Units','normalized',...
        'xvals', objOut.miniplot.windowIdx,...
        'yrange',[min(objOut.data(:)) max(objOut.data(:))],...
        'labels',objOut.labels);
                                   
      set(objOut.mainplot.objAxes,'ButtonDownFcn', @(h,evt) objOut.pickXVal(h,evt));             
      set(get(objOut.panel,'Parent'),'KeyPressFcn',@objOut.keyPress)
      
      objOut.playControls.currIdx = 1;
      objOut.units = 'normalized';
    end
    
    function pickXVal(obj,h,varargin)
      % Callback assigned to data plot to select a particular timepoint
      %
      % function PICKXVAL(obj,h,varargin)
      %
      % Select a specific timepoint using Matlab's internal functionality
      % for selecting a point in a figure
      %
      % Written By: Damon Hyde
      % Last Edited: Aug 17 2015
      % Part of the cnlEEG Project
      %
      
      pos = get(obj.mainplot.objAxes,'CurrentPoint');
%       if isa(h,'matlab.graphics.axis.Axes')
%         pos = get(obj.mainaxes,'CurrentPoint');
%       else
%         pos = get(get(h,'Parent'),'CurrentPoint');
%       end
      
      xVals = obj.xVals(obj.range);
      
      idx = find(abs(xVals-pos(1))==min(abs(xVals-pos(1))));
            
      obj.playControls.currIdx = obj.range(idx);
    end
    
    function keyPress(obj,h,evt)
      % function keyPress(obj,h,evt)
      %
      % Callback for handling arrow keys in the main plot figure;
      %
      % Written By: Damon Hyde
      % Last Edited: Aug 17, 2015
      % Part of the cnlEEG Project
      %
      
      switch evt.Key
        case 'downarrow'
          obj.splitScale = obj.mainplot.scale*1.1;
          obj.mainplot.updateImage;
        case 'uparrow'
          obj.splitScale = obj.mainplot.scale*0.9;
          obj.mainplot.updateImage;
        case 'leftarrow'
          obj.miniplot.leftShift;
        case 'rightarrow'
          obj.miniplot.rightShift;
      end
    end    
    
    function set.xVals(obj,vals)
      % function set.xVals(obj,vals)
      %
      % Ensures that xVals stays consistent between plot and miniplot
      obj.xVals = vals;
      if ishandle(obj.miniplot)
        obj.miniplot.xVals = xVals;
      end;
    end;
    
    function out = get.xVals(obj)
      % function out = get.xVals(obj)
      %
      % Sets default values for xVals if not otherwise available
      if isempty(obj.xVals)
        out = 1:size(obj.data,1);
      else
        out = obj.xVals;
      end;
    end;
    
    function out = get.range(obj)
      % function out = get.range(obj)
      %
      % Fetch range to display from the embedded cnlminiplot object.
      start = obj.miniplot.windowStart;
      finish = obj.miniplot.windowEnd;
      out = [start:finish];
    end;
    
    function set.range(obj,val)
      assert(val(end)>val(1),'Val(1) must be less than Val(end)');
      
      % Update the miniplot
      obj.miniplot.windowStart = val(1);
      obj.miniplot.windowSize = val(end)-val(1);
      obj.miniplot.updateImage;
    end
    
        
    function updateImage(obj)
      % function updateImage(obj)
      %
      % Update displayed image in a cnlDataPlot window
      %
      % Written By: Damon Hyde
      % Last Edited; Aug 17, 2015
      % Part of the cnlEEG Project
      %
      
      obj.mainplot.data   = obj.miniplot.windowData;
      obj.mainplot.xvals  = obj.miniplot.windowIdx;
      obj.mainplot.yrange = [min(obj.data(:)) max(obj.data(:))];
      obj.mainplot.labels = obj.labels;
      obj.mainplot.updateImage;
                        
      % Display a vertical line at the currently selected X value
%       if ~isempty(obj.playControls.currIdx)
%         if ismember(obj.playControls.currIdx,obj.range)
%           xVal = obj.xVals(obj.playControls.currIdx);
%           yRange = get(obj.mainaxes,'YLim');
%           axes(obj.mainaxes); hold on;
%           plot([xVal xVal],yRange,'r',...
%             'ButtonDownFcn',get(obj.mainaxes,'ButtonDownFcn'));
%           set(obj.mainaxes,'ButtonDownFcn',get(obj.mainplot(1),'ButtonDownFcn'));
%           hold off;
%         end
%       end
      
    end
    
  end
  
end
