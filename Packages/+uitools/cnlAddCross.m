classdef (Abstract) cnlAddCross < handle
  % classdef (Abstract) CNLADDCROSS
  %
  % THIS CLASS IS REALLY HORRIBLY DESIGNED AND OMG WHAT WAS I THINKING?
  %
  % This is an abstract class which can be added to other classes to
  % include functionality for making axes clickable, with a resulting
  % crosshair plotted on the axis.
  %
  % Text below needs updating:
  %
  % The following abstract properties need to be defined by any child
  % class:
  %   axes
  %   currSlice
  %
  % Two function handles can be set:
  %  fhnd_crossCallback: An additional callback function that will be
  %                       called whenever the cross location is changed.
  %
  % Three parameters are used to define the cross location.  All three are
  % configured such that they can either be set to a static value, or
  % assigned as function handles, which will be evaluated each time the
  % property is accessed.
  %  crossX : This is the location of the vertical line along the X-axis
  %  crossY : This is the location of the horizontal line along the Y-axis
  %  crossSlices : These are the slices in which the cross should actually
  %                  show up.
  %
  % Example:
  %
  % Written By: Damon Hyde
  % Last Edited: March 2015
  % Part of the cnlEEG Project
  %
  
  properties
    crossX
    crossY
    crossSlices
  end
  
  properties (Hidden = true)
    fhnd_crossCallback    
  end
  
  properties (Abstract)
    axes    
  end
  
  properties (Abstract, SetAccess=protected)
    currSlice
  end
  
  methods
    
    function objOut = cnlAddCross(varargin)
      % function objOut = CNLADDCROSS(varargin)
      %
      
      % Input parsing
      p = inputParser;
      addParamValue(p,'crossCallback',[]);
      parse(p,varargin{:});
      
      % Assign function handles.
      objOut.fhnd_crossCallback = p.Results.crossCallback;
      
    end
    
    function out = get.crossX(obj)
      % function out = GET.CROSSX(obj)
      %
      if isa(obj.crossX,'function_handle')
        out = feval(obj.crossX);
      else
        out = obj.crossX;
      end;
    end
    
    function out = get.crossY(obj)
      % function out = GET.CROSSY(obj)
      %
      if isa(obj.crossY,'function_handle')
        out = feval(obj.crossY);
      else
        out = obj.crossY;
      end
    end
    
    function out = get.crossSlices(obj)
      % function out = GET.CROSSSLICES(obj)
      %
      
      if isa(obj.crossSlices,'function_handle')
        out = feval(obj.crossSlices);
      else
        out = obj.crossSlices;
      end
    end
    
    function setCross(obj,x,y,slices)
      % function SETCROSS(obj,x,y,slice)
      %
      % Set the display location for the
      
      % If not list is provided, just use the same function 
      if ~exist('slices','var'), slices = @()obj.currSlice; end;
      
      % Convert into image coordinates
      obj.crossX = x;
      obj.crossY = y;
      obj.crossSlices = slices;
      obj.updateImage;
    end
    
    function clearCross(obj)
      % function CLEARCROSS(obj)      
      obj.crossX = [];
      obj.crossY = [];
      obj.crossSlices = [];
    end
    
    function drawCross(obj)
      % function DRAWCROSS(obj)
      %
      % Draws a set of crosshairs in the current axes, with X and Y
      % locations defined by obj.crossX and obj.crossY.  If either is
      % empty, the associated line is not drawn. obj.crossSlices defines
      % which slice the lines should appear in.
      
      imgX = get(obj.axes,'XLim');
      imgY = get(obj.axes,'YLim');
      
      v = obj.crossX;
      h = obj.crossY;
      s = obj.crossSlices;
      
      % Check if the current slice is one we should be displaying in.
      if ismember(obj.currSlice,s)
        axes(obj.axes);
        hold on;
        
        % If it's defined, plot the vertical
        if ~isempty(v);
          for idx = 1:length(v)
            plot([v(idx) v(idx)],[imgY(1) imgY(2)],'r');
          end;
        end
        
        % If it's defined, plot the horizontal
        if ~isempty(h)
          for idx = 1:length(h)
            plot([imgX(1) imgX(2)],[h(idx) h(idx)],'r');
          end;
        end
        
        hold off;
      end
    end
    
  end
end

