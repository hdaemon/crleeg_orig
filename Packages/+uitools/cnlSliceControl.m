classdef cnlSliceControl < uitools.cnlUIObj
  % classdef cnlSliceControl < handle
  %
  %  Object class for constructing a basic X/Y/Z and slice selection tool
  %  for use with visualizing things.
  %
  %  Properties:
  %   parent    : UI object to construct the control inside
  %   origin    : location within parent to place control
  %   size  : Size of control window (default 320x40)
  %   imageSize : Size of the image to be accessed
  %   updatefunction: function handle to call when axis and slice
  %                   selections are changed.  This is done by calling:
  %                   obj.updatefunction(obj.selectedAxis,obj.selectedSlice)
  %
  %  Read Only Properties:
  %   selectedAxis  : Currently Selected Axis
  %   selectedSlice : Currently Selected Slice
  %
  
  properties
 %   parent
 %   origin
 %   size = [340 45];
 %   normalized  = 0;    
    imageSize
    updatefunction

   % selectedAxis = 1;
  end
  
%   properties (SetAccess = protected)
%     selectedAxis  = 1
%   end
  
  properties (Dependent = true)
    selectedAxis;
    selectedSlice
  end;
  
  properties (Hidden = true)
    % These are all Matlab UI objects
 %   panel
    axSel
    b1
    b2
    b3
    sliceSelect
    sliceLabel
  end
  
  methods
    
    function objOut = cnlSliceControl(imageSize,varargin) %parent,imageSize,origin,size,labels)
      
      % Set up input parsing to make sure the defaults passed to cnlUIObj
      % are appropriate for a cnlSliceControl panel
      p = inputParser;
      p.addParamValue('parent',[]);      
      p.addParamValue('origin',[0 0],@(x) isvector(x)&&numel(x)==2);
      p.addParamValue('size',[330 50]);
      p.addParamValue('units','pixels');
      p.addParamValue('labels',{'X' 'Y' 'Z'});      
      p.addParamValue('title','Slice Selection');
      parse(p,varargin{:});
      
      % Initialize Superclass
      objOut = objOut@uitools.cnlUIObj('parent',p.Results.parent,...
                  'origin',p.Results.origin,'size',p.Results.size, ...
                  'title',p.Results.title,'units',p.Results.units);
                  
      labels = p.Results.labels;
            
      if numel(labels)~=numel(imageSize)
        error('cnlSliceControl:SizeMismatch',...
          'Number of labels must equal the number of dimensions in imageSize');
      end;
        
      if numel(imageSize)>3, error('imageSize needs to be of dimension 3 or less'); end;
      objOut.imageSize = imageSize;

      % Easier reference to master panel
      controlPanel = objOut.panel;
            
      % Draw Button Panel to Control Axis Selection
      objOut.axSel = uibuttongroup( 'Parent',controlPanel,'Units','Pixels',...
        'Position', [5 5 100 27],'SelectionChangeFcn',@objOut.selectAxes);
      if length(labels)>=1
      objOut.b1 = uicontrol(objOut.axSel,'Style','radiobutton',...
        'String', labels{1}, 'Position', [3 0 33 22]);
      end;
      if length(labels)>=2
      objOut.b2 = uicontrol(objOut.axSel,'Style','radiobutton', ...
        'String', labels{2}, 'Position', [33 0 33 22]);
      end;
      if length(labels)==3
      objOut.b3 = uicontrol(objOut.axSel,'Style','radiobutton', ...
        'String', labels{3}, 'Position', [63 0 33 22]);
      end;
      set(objOut.axSel,'Visible','on')
      
      % Draw Slider for slice selection and text to report current/max slice
      objOut.sliceSelect = uicontrol('Style','slider',...
        'Parent', controlPanel, ...
        'Max',objOut.imageSize(1),'Min',1,'Value',1,...
        'Callback',@objOut.selectSlice, ...
        'SliderStep',[1/imageSize(1) 10*(1/imageSize(1))],...
        'Position', [115 5 150 22]);
      
      objOut.sliceLabel = uicontrol('Style','text',...
        'Parent',controlPanel,...
        'String',[num2str(round(get(objOut.sliceSelect,'Value'))) '/' num2str(imageSize(1))],...
        'Position', [265 5 60 22]);

      % Set all units on all objects inside the panel to normalized
      set(objOut.axSel,'Units','normalized');
      set(objOut.axSel,'FontUnits','normalized');
      set(objOut.b1,'Units','normalized');
      set(objOut.b1,'FontUnits','normalized');
      set(objOut.b2,'Units','normalized');
      set(objOut.b2,'FontUnits','normalized');
      set(objOut.b3,'Units','normalized');
      set(objOut.b3,'FontUnits','normalized');
      set(objOut.sliceSelect,'Units','normalized');
      set(objOut.sliceLabel,'Units','normalized');
      set(objOut.sliceLabel,'FontUnits','normalized');
      set(objOut.panel,'Position',[objOut.origin objOut.size]);
      set(objOut.panel,'visible','on');
      objOut.updateImage;
      
    end;

%     function set.imageSize(obj,val)
%       obj.imageSize = val;
%       set(obj.sliceSelect,'Max',imageSize(obj.selectedAxis));
%     end
    
%     function set.origin(obj,val)
%       obj.origin = val;
%       if ishandle(obj.panel)
%         tmp = get(obj.panel,'Units');
%         set(obj.panel,'Units','Pixels');
%         set(obj.panel,'Position',[obj.origin obj.size]);
%         set(obj.panel,'Units',tmp);
%       end
%     end
%       
%     function set.size(obj,val)
%       obj.size = val;
%       if ishandle(obj.panel)
%         tmp = get(obj.panel,'Units');
%         set(obj.panel,'Units','Pixels');
%         set(obj.panel,'Position',[obj.origin obj.size]);
%         set(obj.panel,'Units',tmp);
%       end;
%     end       
%     
%     function set.normalized(obj,val)
%       if ishandle(obj.panel)
%         if val==true
%           obj.normalized = true;
%           set(obj.panel,'Units','normalized');
%           set(obj.panel,'FontUnits','normalized');
%         else
%           obj.normalized = false;
%           set(obj.panel,'Units','pixels');
%           set(obj.panel,'FontUnits','points');
%         end;
%       end;
%     end    
    
    function setSliceSelRange(obj)
      set(obj.sliceSelect,'Max',obj.imageSize(obj.selectedAxis));
      set(obj.sliceSelect,'Value',1);
      set(obj.sliceSelect,'SliderStep',...
        [1/obj.imageSize(obj.selectedAxis) 10*(1/obj.imageSize(obj.selectedAxis))]);
    end
    
    function setSliceLabel(obj,slice)
      set(obj.sliceLabel,'String',[num2str(slice) '/' num2str(obj.imageSize(obj.selectedAxis))]);
    end
       
    function out = get.selectedSlice(obj)
      if ~isempty(obj.sliceSelect)
        out = round(get(obj.sliceSelect,'Value'));
      else % Error catching in case sliceSelect hasn't been defined yet
        out = 1;
      end;
    end
    
    function set.selectedSlice(obj,val)
      set(obj.sliceSelect,'Value',val);
      if ishandle(obj.axSel)
        obj.setSliceLabel(obj.selectedSlice);
        set(obj.sliceLabel,'String',[num2str(obj.selectedSlice) '/' num2str(obj.imageSize(obj.selectedAxis))]);             
        obj.updateImage;
      end
    end
    
    function selectSlice(obj,h,EventData)
      %obj.selectedSlice = obj.selectedSlice;
      set(obj.sliceLabel,'String',[num2str(obj.selectedSlice) '/' num2str(obj.imageSize(obj.selectedAxis))]);     
      obj.updateImage;
    end    
    
    function out = get.selectedAxis(obj)
      switch get(get(obj.axSel,'SelectedObject'),'String')
        case get(obj.b1,'String'), out = 1;
        case get(obj.b2,'String'), out = 2;
        case get(obj.b3,'String'), out = 3;
        otherwise, out = 0;
      end
    end
    
    function set.selectedAxis(obj,val)
      if ishandle(obj.axSel)
        % Make sure the selected object is consistent
        switch val
          case 1, set(obj.axSel,'SelectedObject',obj.b1);            
          case 2, set(obj.axSel,'SelectedObject',obj.b2);            
          case 3, set(obj.axSel,'SelectedObject',obj.b3);            
        end
        
        % Set slice range, labels, and update the image.
        obj.setSliceSelRange;
        obj.setSliceLabel(1);
        obj.updateImage;
      end
    end
    
    function selectAxes(obj,h,EventData)
%       switch get(get(h,'SelectedObject'),'String')
%         case 'X', obj.selectedAxis = 1;
%         case 'Y', obj.selectedAxis = 2;
%         case 'Z', obj.selectedAxis = 3;
%       end
     obj.setSliceSelRange;
     obj.setSliceLabel(1);
     obj.updateImage;
    end
    

    
    function updateImage(obj)
     % disp(['Running update function.  Axis: ' num2str(obj.selectedAxis) ...
     %   '  Slice: ' num2str(obj.selectedSlice)]);
      if isa(obj.updatefunction,'function_handle')
        obj.updatefunction(obj.selectedAxis,obj.selectedSlice);
      else
        %disp('Update function not defined');
      end;
    end            
  end
end
