classdef selectXYZSlice < uitools.baseobjs.gui
  % UI Object for Selecting and X-Y-Z Slice
  %
  % classdef selectXYZSlice < uitools.baseobjs.gui
  %
  %
  
  properties
    imageSize
  end
  
  properties (Dependent = true)
    selectedAxis;
    selectedSlice;
  end
  
  properties (Access=protected)
    axSel
    buttons
    sliceSelect
    sliceLabel
  end
  
  methods
    
    function obj = selectXYZSlice(imageSize,varargin)
      % Set up input parsing to make sure the defaults passed to cnlUIObj
      % are appropriate for a cnlSliceControl panel
      p = inputParser;
      p.addParamValue('parent',[]);      
      p.addParamValue('origin',[0 0],@(x) isvector(x)&&numel(x)==2);
      p.addParamValue('size',[330 50]);
      p.addParamValue('units','pixels');
      p.addParamValue('labels',{'X' 'Y' 'Z'});      
      p.addParamValue('title','Slice Selection');
      parse(p,varargin{:});
      
      % Initialize Superclass
      obj = obj@uitools.baseobjs.gui('parent',p.Results.parent,...
                  'origin',[0 0 ],'size',[330 50], ...
                  'title',p.Results.title,'units','pixels');
                  
      labels = p.Results.labels;
            
      if numel(labels)~=numel(imageSize)
        error('cnlSliceControl:SizeMismatch',...
          'Number of labels must equal the number of dimensions in imageSize');
      end;
        
      if numel(imageSize)>3, error('imageSize needs to be of dimension 3 or less'); end;
      obj.imageSize = imageSize;
            
      % Draw Button Panel to Control Axis Selection
      obj.axSel = uibuttongroup( 'Parent',obj.panel,'Units','Pixels',...
        'Position', [5 5 100 27],'SelectionChangeFcn',@obj.selectAxis);
      if length(labels)>=1
      obj.buttons(1) = uicontrol(obj.axSel,'Style','radiobutton',...
        'String', labels{1}, 'Position', [3 0 33 22]);
      end;
      if length(labels)>=2
      obj.buttons(2) = uicontrol(obj.axSel,'Style','radiobutton', ...
        'String', labels{2}, 'Position', [33 0 33 22]);
      end;
      if length(labels)==3
      obj.buttons(3) = uicontrol(obj.axSel,'Style','radiobutton', ...
        'String', labels{3}, 'Position', [63 0 33 22]);
      end;
      set(obj.axSel,'Visible','on')
      
      % Draw Slider for slice selection and text to report current/max slice
      obj.sliceSelect = uicontrol('Style','slider',...
        'Parent', obj.panel, ...
        'Max',obj.imageSize(1),'Min',1,'Value',ceil(obj.imageSize(1)/2),...
        'Callback',@obj.selectSlice, ...
        'SliderStep',[1/imageSize(1) 10*(1/imageSize(1))],...
        'Position', [115 5 150 22]);
      
      obj.sliceLabel = uicontrol('Style','text',...
        'Parent',obj.panel,...
        'String',[num2str(round(get(obj.sliceSelect,'Value'))) '/' num2str(imageSize(1))],...
        'Position', [265 5 60 22]);

      % Set all units on all objects inside the panel to normalized
      set(obj.axSel,'Units','normalized');
      set(obj.axSel,'FontUnits','normalized');
      set(obj.buttons(1),'Units','normalized');
      set(obj.buttons(1),'FontUnits','normalized');
      set(obj.buttons(2),'Units','normalized');
      set(obj.buttons(2),'FontUnits','normalized');
      set(obj.buttons(3),'Units','normalized');
      set(obj.buttons(3),'FontUnits','normalized');
      set(obj.sliceSelect,'Units','normalized');
      set(obj.sliceLabel,'Units','normalized');
      set(obj.sliceLabel,'FontUnits','normalized');
      obj.units = p.Results.units;
      obj.origin = p.Results.origin;
      obj.size = p.Results.size;
      set(obj.panel,'Position',[obj.origin obj.size]);
      set(obj.panel,'visible','on');
            
    end;

    %% Callbacks for Axis and Slice Selection
    function selectSlice(obj,h,EventData)
      % Called when the slider is updated      
      obj.setSliceLabel(obj.selectedSlice);
      notify(obj,'updatedOut');
    end    
    
    function selectAxis(obj,h,EventData)
     % Called when the X-Y-Z Radio Buttons Are Changed
     obj.setSliceSelRange;     
     notify(obj,'updatedOut');
    end
        
    function set.imageSize(obj,val)
      obj.imageSize = val;
      if ~isempty(obj.sliceSelect)&&ishandle(obj.sliceSelect)&&isvalid(obj.sliceSelect)
        obj.setSliceSelRange;
      end
    end
    
    function setSliceSelRange(obj)
      initialSlice = ceil(obj.imageSize(obj.selectedAxis)/2);
      set(obj.sliceSelect,'Max',obj.imageSize(obj.selectedAxis));
      set(obj.sliceSelect,'Value',initialSlice);
      set(obj.sliceSelect,'SliderStep',...
        [1/obj.imageSize(obj.selectedAxis) 10*(1/obj.imageSize(obj.selectedAxis))]);
      obj.setSliceLabel(initialSlice);
    end
    
    function setSliceLabel(obj,slice)
      set(obj.sliceLabel,'String',[num2str(slice) '/' num2str(obj.imageSize(obj.selectedAxis))]);
    end
       
    %% Get/Set Selected Alice
    function out = get.selectedSlice(obj)
      if ~isempty(obj.sliceSelect)
        out = round(get(obj.sliceSelect,'Value'));
      else % Error catching in case sliceSelect hasn't been defined yet
        out = 1;
      end;
    end
    
    function set.selectedSlice(obj,val)    
      assert(isscalar(val),'Slice selection myust be a scalar');
      if ~isequal(obj.selectedSlice,val)        
        set(obj.sliceSelect,'Value',val);
        if ishandle(obj.axSel)
          obj.setSliceLabel(obj.selectedSlice);
          set(obj.sliceLabel,'String',...
            [num2str(obj.selectedSlice) '/' num2str(obj.imageSize(obj.selectedAxis))]);
        end
        notify(obj,'updatedOut');
      end;
    end
        
    %% Get/Set Selected Axis
    function out = get.selectedAxis(obj)
      switch get(get(obj.axSel,'SelectedObject'),'String')
        case get(obj.buttons(1),'String'), out = 1;
        case get(obj.buttons(2),'String'), out = 2;
        case get(obj.buttons(3),'String'), out = 3;
        otherwise, out = 0;
      end
    end
    
    function set.selectedAxis(obj,val)
      if ~isempty(obj.axSel)&&ishandle(obj.axSel)&&isvalid(obj.axSel)
        % Make sure the selected object is consistent
        newSelection = obj.buttons(val);
        
        if ~isequal(get(obj.axSel,'SelectedObject'),newSelection)
          set(obj.axSel,'SelectedObject',newSelection);
                
          % Set slice range, labels, and update the image.
          obj.setSliceSelRange;        
          notify(obj,'updatedOut');
        end;
      end
    end
    

            
  end
end

    
    