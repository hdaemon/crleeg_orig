classdef dualbutton < uitools.baseobjs.gui
  % UI Object for Dual Button Panel
  %
  % Create a uiPanel with two buttons in it.
  %
  % Written By: Damon Hyde
  % Last Edited: Jun 8, 2016
  % Part of the cnlEEG Project
  %
  
  properties (Dependent=true)
    leftLabel
    rightLabel
  end
  
  properties (Access=protected)
    LButton
    RButton
  end
  
  events
    leftPushed
    rightPushed
  end
  
  methods
    
    function obj = dualbutton(varargin)
      
      p = uitools.controls.dualbutton.parseInputs(varargin{:});
      
      obj = obj@uitools.baseobjs.gui(...
        'parent',p.Results.parent,...
        'origin',p.Results.origin,...
        'size',p.Results.size,...
        'title',p.Results.title,...
        'Units',p.Results.units);
      
      obj.LButton = uicontrol(...
        'Parent',obj.panel,...
        'Style','pushbutton',...
        'String',p.Results.leftLabel,...
        'Units','normalized',...
        'Position', [0.02 0.02 0.47 0.96]);
      set(obj.LButton,'Callback',@(h,evt) notify(obj,'leftPushed'));
      
      obj.RButton = uicontrol(...
        'Parent',obj.panel,...
        'Style','pushbutton',...
        'String',p.Results.rightLabel,...
        'Units','normalized',...        
        'Position', [0.51 0.02 0.47 0.96]);
      set(obj.RButton,'Callback',@(h,evt) notify(obj,'rightPushed'));
      
        
      
    end
    
    function out = get.leftLabel(obj)
      out = get(obj.LButton,'String');
    end
    
    function out = get.rightLabel(obj)
      out = get(obj.RButton,'String');
    end
    
    function set.leftLabel(obj,val)
      set(obj.LButton,'String',val);
    end
    
    function set.rightLabel(obj,val)
      set(obj.RButton,'String',val);
    end;
    
  end
  
  methods (Access=protected,Static=true)
    function p = parseInputs(varargin)
      p = inputParser;
      addParamValue(p,'parent',[]);
      addParamValue(p,'origin',[10 10]);
      addParamValue(p,'size',[200,50]);
      addParamValue(p,'title','');
      addParamValue(p,'units','pixels');
      addParamValue(p,'leftLabel','<-');
      addParamValue(p,'rightLabel','->');
      parse(p,varargin{:});
    end
  end
  
end
