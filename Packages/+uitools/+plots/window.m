classdef window < uitools.baseobjs.gui
  % Plot for Selecting a Sub-Window of Time Data
  %
  % classdef window < uitools.baseobjs.gui
  %
  % p = uitools.plots.window(data,varargin)
  %
  % Inputs:
  %   data : nSamples x nChannels Data Matrix
  %
  % Param-Value Pairs:
  %   xVals : x-Axis Values for Each of the N Samples
  %
  % Events:
  %   updatedOut : Notified when the displayed image is updated through
  %                 a call to updateImage.
  %
  % Written By: Damon Hyde
  % Last Edited: June 8, 2016
  % Part of the cnlEEG Project
  %
  
  properties
    data
    xVals
  end
  
  properties (Hidden = true)
    axes
  end
  
  properties (Dependent = true)
    % Tracking of Window Size/Location
    windowStart % Value stored in storedVals.windowStart
    windowEnd   % Value stored in storedVals.windowEnd
    windowSize  % Computed from windowStart and windowEnd
    
    windowData
    windowRange
    windowIdx
    windowXVals
    
  end
  
  properties (Hidden=true,SetAccess=protected)
    storedVals
    
    zoomButtons
    zoomScale = 0.1;
    
    shiftButtons
    shiftScale = 0.1;
    
    selButtons
    
    vLine
    
  end
  
  methods
    function obj = window(varargin)
      %% uitools.plots.window object constructor
      
      %% Input Parsing
      p = uitools.plots.window.parseInputs(varargin{:});
      
      %% Initialize Base Object
      obj = obj@uitools.baseobjs.gui(...
        'parent',p.Results.parent,...
        'origin',p.Results.origin,...
        'size',p.Results.size,...
        'title',p.Results.title,...
        'units',p.Results.units);
      
      %% Initialize Axes
      obj.axes = axes(...
        'Parent',obj.panel,...
        'Units','normalized',...
        'Position',[0.03 0.5 0.94 0.48]);
      
      %% Initialize Zoom Buttons
      obj.zoomButtons = uitools.controls.dualbutton(...
        'parent',obj.panel,...
        'origin',[0.01 0.02],...
        'units','normalized',...
        'size',[0.32 0.45],...
        'leftLabel','-',...
        'rightLabel','+',...
        'title','Zoom');
      obj.listenTo{end+1} = addlistener(obj.zoomButtons,'leftPushed',...
        @(h,evt) obj.adjustZoom(+1));
      obj.listenTo{end+1} = addlistener(obj.zoomButtons,'rightPushed',...
        @(h,evt) obj.adjustZoom(-1));
      
      %% Initialize Shift Buttons
      obj.shiftButtons = uitools.controls.dualbutton(...
        'parent',obj.panel,...
        'origin',[0.34 0.02],...
        'size',[0.32 0.45],...
        'units','normalized',...
        'title','Shift');
      obj.listenTo{end+1} = addlistener(obj.shiftButtons,'leftPushed',...
        @(h,evt) obj.shiftWindow(-1));
      obj.listenTo{end+1} = addlistener(obj.shiftButtons,'rightPushed',...
        @(h,evt) obj.shiftWindow(+1));
      
      %% Initialize Select Buttons
      obj.selButtons = uitools.controls.dualbutton(...
        'parent',obj.panel,...
        'origin',[0.67 0.02],...
        'size',[0.32 0.45],...
        'units','normalized',...
        'title','Select',...
        'leftLabel','Start',...
        'rightLabel','End');
      obj.listenTo{end+1} = addlistener(obj.selButtons,'leftPushed',...
        @(h,evt) obj.setStart);
      obj.listenTo{end+1} = addlistener(obj.selButtons,'rightPushed',...
        @(h,evt) obj.setEnd);
      
      %% Adjust figure size
      uitools.setMinFigSize(gcf,obj.origin,obj.size,5);
      
      %% Initialize stored values
      obj.storedVals.windowStart = 1;
      obj.storedVals.windowEnd   = 1;
      
      %% Set Data, xVals, and initial window size, then draw
      obj.data = p.Results.data;
      obj.xVals = p.Results.xVals;
      obj.windowSize = size(obj.data,1);
      obj.updateImage;
      
    end
    
    
    %% Button Callback Functions
    function shiftWindow(obj,val)
      shift = val*obj.shiftScale*obj.windowSize;
      
      % Always shift by at least one sample
      if floor(shift)==0, shift = sign(shift); end;
      
      boundLeft = (obj.windowStart+shift <= 0);
      boundRght = (obj.windowEnd  +shift > size(obj.data,1));
      
      currSize = obj.windowSize;
      if boundLeft
        obj.windowStart = 1;
        obj.windowEnd   = obj.windowStart + currSize - 1;
      elseif boundRght
        obj.windowStart = size(obj.data,1) - currSize + 1;
        obj.windowEnd = size(obj.data,1);
      else                    
        obj.windowStart = obj.windowStart + shift;
        obj.windowEnd = obj.windowEnd + shift;
      end;
      
      obj.updateImage;
    end
    
    function adjustZoom(obj,val)
      obj.windowSize = (1+val*obj.zoomScale)*obj.windowSize;
      obj.updateImage;
    end
    
    function nearest = sampleNum(obj,val)
      nearest = abs(val-obj.xVals);
      nearest = find(nearest==min(nearest));
    end;
        
    function setEnd(obj)
      k = 1;
      while k==1
        k = waitforbuttonpress;
      end
      newPos = get(obj.axes,'CurrentPoint');
      
      obj.windowEnd = obj.sampleNum(newPos(1,1));  
      obj.updateImage;
    end
    
    function setStart(obj)
      k = 1;
      while k==1
        k = waitforbuttonpress;
      end
      newPos = get(obj.axes,'CurrentPoint');            
            
      obj.windowStart = obj.sampleNum(newPos(1,1));      
      obj.updateImage;
    end;
    
    
    function set.xVals(obj,val)
      if ~isempty(obj.data)
        assert(isempty(val)||(numel(val)==size(obj.data,1)),...
          'Number of xVals must match number of samples');
        obj.xVals = val;
      end
    end
    
    function out = get.xVals(obj)
      if isempty(obj.xVals)
        out = 1:size(obj.data,1);
      else
        out = obj.xVals;
      end
    end
    
    %% Methods for Window Size Setting
    function set.windowStart(obj,val)
      
      % Window Always Starts at 1 if data is empty
      if isempty(obj.data), obj.windowStart = 1; return; end;
      
      % Make sure the window start point is and integer within range
      val = round(min(size(obj.data,1),max(1,val)));
            
      % Update Stored Value
      if val<=obj.storedVals.windowEnd
        obj.storedVals.windowStart = val;      
      end;
      
    end
    
    function out = get.windowStart(obj)
      out = obj.storedVals.windowStart;
    end
    
    function set.windowEnd(obj,val)
      % Window always ends at 1 if data is empty
      if isempty(obj.data), obj.windowEnd = 1; return; end;
      
      % Make sure window end point is an integer within range.
      val = round(min(size(obj.data,1),max(1,val)));
      
      if val>=obj.storedVals.windowStart
        obj.storedVals.windowEnd = val;   
      end
    end
    
    function out = get.windowEnd(obj)
      out = obj.storedVals.windowEnd;
    end;
        
    function set.windowSize(obj,val)
      
      % Do nothing if it's a negative value
      if (val<=0), return; end;
      
      currSize = obj.storedVals.windowEnd - obj.storedVals.windowStart + 1;
      
      delta = val - currSize;
      if abs(delta)<1, delta = sign(delta); end;
      
      newSize = currSize + round(delta);
      
      obj.windowEnd = obj.windowStart + newSize - 1;
     
    end
    
    function out = get.windowSize(obj)
      out = obj.storedVals.windowEnd - obj.storedVals.windowStart + 1;
    end
    
    %% Methods for Getting the Windowed Data
    function out = get.windowData(obj)
      out = obj.data(obj.windowStart:obj.windowEnd,:);
    end
   
    function out = get.windowXVals(obj)
      out = obj.xVals(obj.windowStart:obj.windowEnd);
    end
    
    function out = get.windowRange(obj)
    end
    
    function out = get.windowIdx(obj)
    end
  
    %% Methods for Updating the Data Plot   
    function updateImage(obj)
      if isempty(obj.data), return; end;
           
      axes(obj.axes);
      
      if (size(obj.data,1) < 10000)
      uitools.plots.butterfly(obj.data,'ax',obj.axes,'xvals',obj.xVals);
      else
        useIdx = round(linspace(1,size(obj.data,1),10000));
        useIdx = unique(useIdx);
        uitools.plots.butterfly(obj.data(useIdx,:),'ax',obj.axes,'xvals',obj.xVals(useIdx));
      end
      
      YLim = get(obj.axes,'YLim');
      hold on;
      XVal1 = obj.xVals(obj.windowStart);
      XVal2 = obj.xVals(obj.windowEnd);
      obj.vLine{1} = plot([XVal1 XVal1],[YLim(1) YLim(2)],'r');
      set(obj.vLine{1},'linewidth',2);
      obj.vLine{2} = plot([XVal2 XVal2],[YLim(1) YLim(2)],'r');
      set(obj.vLine{2},'linewidth',2);
      hold off;
      
      XLim(1) = obj.xVals(1); XLim(2) = obj.xVals(end);
      if (XLim(1)==XLim(2)),
        XLim(1) = XLim(1) - 0.1;
        XLim(2) = XLim(2) + 0.1;
      end;
      
      axis([XLim(1) XLim(2) YLim(1) YLim(2)]);      
      notify(obj,'updatedOut');
      
    end
    
    
    
  end
  
  methods (Static=true,Access=protected)
    function p = parseInputs(varargin)
      p = inputParser;
      p.addRequired('data');
      p.addParamValue('parent',[]);
      p.addParamValue('origin',[10 10]);
      p.addParamValue('size',[600 200]);
      p.addParamValue('units','pixels');
      p.addParamValue('title','');
      p.addParamValue('xVals',[]);
      
      parse(p,varargin{:});
    end
  end
  
end
