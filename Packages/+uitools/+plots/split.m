function plotOut = split(data,varargin)
  % Split Data Plot With Labels
  %
  % Inputs:
  %   data (required): (nSamples X nChannels data matrix)
  %   ax   (optional): (Axis to plot to)
  %
  % Param-Value Inputs:
  %   'xvals'    : Values along X-axis (otherwise assumed to be 1:N)
  %   'yrange'   : Range of values to display on Y-Axis
  %   'scale'    : Scaling factor 
  %   'labels'   : Labels for each channel
  %
  % Written By: Damon Hyde
  % Last Edited; May 24, 2016
  % Part of the cnlEEG Project
  %
  
  %% Input Parsing
  p = inputParser;
  p.addRequired('data');
  p.addOptional('ax',[],@(x) ishghandle(x)&&strcmpi(get(x,'type'),'axes'))
  p.addParamValue('xvals',1:size(data,1),@(x) isempty(x)||(isvector(x)&&numel(x)==size(data,1)))
  p.addParamValue('yrange',[min(data(:)) max(data(:))],@(x) isvector(x)&&(numel(x)==2));
  p.addParamValue('scale',1,@(x) isnumeric(x)&&numel(x)==1);
  p.addParamValue('labels',[],@(x) isempty(x)||iscellstr(x));
  p.parse(data,varargin{:});
  
  ax = p.Results.ax;
  xvals = p.Results.xvals;
  if isempty(xvals), xvals = 1:size(data,1); end;
  yrange = p.Results.yrange;
  labels = p.Results.labels;
  scale = p.Results.scale;
  
  % If no axis provided, open a new figure.
  if isempty(ax), figure; ax = axes; end;
  axes(ax);
    
  % Get data range and scale
  delta = yrange(2)-yrange(1);
  delta = delta*scale;
  
  % Scale Data
  data = data./delta;
  
  % Plot things.
  hold on;
  for i = 1:size(data,2)
    offset = size(data,2) - (i -1);    
    plotOut(i) = plot(xvals,data(:,i)+offset,'k',...
      'ButtonDownFcn',get(ax,'ButtonDownFcn'));
    set(ax,'ButtonDownFcn',get(plotOut(i),'ButtonDownFcn'));
  end;

  axis([xvals(1) xvals(end) 0 size(data,2) + 1]);
  
  ticks = 1:size(data,2);  
  if isempty(labels), labels = 1:size(data,2); end
  set(ax,'YTick',ticks);
  if exist('flip')
    set(ax,'YTickLabel',flip(labels));
  else
    % Compatibility w/ earlier matlab versions.
    set(ax,'YTickLabel',flipdim(labels,2));
  end
end

