classdef dataexplorer < uitools.baseobjs.gui
  % Tool for exploring time series data
  %
  % classdef dataexplorer < uitools.baseobjs.gui
  %
  % Written By: Damon Hyde
  % Last Edited: June 10, 2016
  % Part of the cnlEEG Project
  %
  
  
  properties (Dependent=true)
    currIdx
    externalSync
  end
    
  properties (Dependent=true, Hidden=true)
    selectedX    
  end
  
  properties (SetAccess=protected)
    miniplot
    toggleplot
    playcontrols
    vLine
  end
  
  methods
    
    function obj = dataexplorer(varargin)
      p = uitools.plots.dataexplorer.parseInputs(varargin{:});
      
      % There are currently two ways to input labels
      if ~isempty(p.Results.optlabels), 
        labels = p.Results.optlabels;
      else
        if ~isempty(p.Results.labels)
        warning('uitools.plots.dataeexplorer(data,labels,varargin) is the new correct syntax');
        end;
        labels = p.Results.labels;
      end;
      
      %% Initialize Base Object
      obj = obj@uitools.baseobjs.gui(...
        'parent',p.Results.parent,...
        'origin',p.Results.origin,...
        'size',[600 800],...
        'title',p.Results.title,...
        'units','pixels');
      
      %% Initialize Mini Plot
      obj.miniplot = uitools.plots.window(...
        p.Results.data,...
        'parent',obj.panel,...
        'origin',[0 0],...
        'size',[600 100],...
        'title',p.Results.title);
      
      obj.toggleplot = uitools.plots.toggle(...
        obj.miniplot.windowData,...
        'parent',obj.panel,...
        'origin',[0 100],...
        'size',[600 700],...
        'units','pixels',...
        'labels',labels,...      
        'xvals',obj.miniplot.windowXVals);
      
      obj.playcontrols = uitools.controls.timeplay(...
        'parent',obj.panel,...
        'origin',[150 110],...
        'size',[450 30],...
        'units','pixels',...
        'range',[1 size(obj.miniplot.data,1)]);
        
      obj.listenTo{end+1} = ...
        addlistener(obj.miniplot,'updatedOut',@(h,evt)obj.updateImage);
      obj.listenTo{end+1} = ...
        addlistener(obj.playcontrols,'updatedOut',@(h,evt)obj.updatedIdx);      
      obj.listenTo{end+1} = ...
        addlistener(obj.toggleplot,'updatedOut',@(h,evt) obj.updateLine);    
      
      set(obj.toggleplot.axes,'ButtonDownFcn', @(h,evt) obj.pickXVal(h,evt));
      
      set(get(obj.panel,'Parent'),'KeyPressFcn',@obj.keyPress);
      
      uitools.setMinFigSize(gcf,obj.origin,obj.size,5);
      
      obj.miniplot.units = 'normalized';
      obj.toggleplot.units = 'normalized';
      obj.playcontrols.units = 'normalized';
      
      obj.size  = p.Results.size;
      obj.units = p.Results.units;
            
      obj.updateImage;
        
    end
    
    function set.externalSync(obj,val)
      if val
        obj.playcontrols.externalSync = true;
        obj.listenTo{2}.Recursive = true;
      else
        obj.playcontrols.externalSync = false;
        obj.listenTo{2}.Recursive = false;
      end
    end
    
    function out = get.externalSync(obj)
      out = obj.playcontrols.externalSync;
    end;
    
    function nextStep(obj)
      obj.playcontrols.nextStep;
    end;
    
    function out = get.selectedX(obj)
      warning('dataexplorer.selectedX is a deprecated property. Use dataexplorer.currIdx instead');
      out = obj.currIdx;
    end
    
    function out = get.currIdx(obj)
      out = [];
      if ~isempty(obj.playcontrols)
      out = obj.playcontrols.currIdx;      
      end;
    end;
    
    function set.currIdx(obj,val)
      if ~isempty(obj.playcontrols)
        obj.playcontrols.currIdx = val;
      end;
    end
    
    function updatedIdx(obj)
      % Callback when obj.playcontrols.currIdx updates
      %
      %
      shiftLeft = obj.playcontrols.currIdx<obj.miniplot.windowStart;
      shiftRight = obj.playcontrols.currIdx>obj.miniplot.windowEnd;
      if ~(shiftLeft||shiftRight)
        obj.updateLine;
      elseif shiftLeft
        obj.miniplot.shiftWindow(-5);
      elseif shiftRight
        obj.miniplot.shiftWindow(+5);
      end            
      notify(obj,'updatedOut');
      
    end
    
    function pickXVal(obj,h,varargin)
      pos = get(obj.toggleplot.axes,'CurrentPoint');
      
      xvals = obj.toggleplot.xvals;
      idx = find(abs(xvals-pos(1))==min(abs(xvals-pos(1))));
      idx = idx(1);
      range = obj.miniplot.windowStart:obj.miniplot.windowEnd;
      obj.playcontrols.currIdx = range(idx);         
    end
    
    function updateLine(obj)
      range = obj.miniplot.windowStart:obj.miniplot.windowEnd;
      if ~isempty(obj.vLine)&&ishandle(obj.vLine)        
        delete(obj.vLine);        
      end;
      if ~isempty(obj.playcontrols.currIdx)
        if ismember(obj.playcontrols.currIdx,range)
          xVal = obj.miniplot.xVals(obj.playcontrols.currIdx);
          yRange = get(obj.toggleplot.axes,'YLim');
          axes(obj.toggleplot.axes); hold on;
          tmp = get(obj.toggleplot.axes,'ButtonDownFcn');
          obj.vLine = plot([xVal xVal],yRange,'r','linewidth',2,...
            'ButtonDownFcn',tmp);
          set(obj.toggleplot.axes,'ButtonDownFcn',tmp);
          hold off;
            
        end
      end      
    end
    
    function updateImage(obj)
      
      data = obj.miniplot.windowData;
      xVals = obj.miniplot.windowXVals;
      
      if ( numel(xVals)>10000 )
        pick = round(linspace(1,numel(xVals),10000));
        pick = unique(pick);
        data = data(pick,:);
        xVals = xVals(pick);
      end;
      
      obj.toggleplot.data = data;
      obj.toggleplot.xvals = xVals;
      obj.toggleplot.updateImage;                        
    end
    
        function keyPress(obj,h,evt)
      % function keyPress(obj,h,evt)
      %
      % Callback for handling arrow keys in the main plot figure;
      %
      % Written By: Damon Hyde
      % Last Edited: Aug 17, 2015
      % Part of the cnlEEG Project
      %
      
      switch evt.Key
        case 'downarrow'
          obj.toggleplot.scale = obj.toggleplot.scale*1.1;
          obj.updateImage;
        case 'uparrow'
          obj.toggleplot.scale = obj.toggleplot.scale*0.9;
          obj.updateImage;
        case 'leftarrow'
          obj.miniplot.shiftWindow(-1);
        case 'rightarrow'
          obj.miniplot.shiftWindow(+1);
      end
    end    
    
    
  end
  
  methods (Static=true,Access=protected)
    function p = parseInputs(varargin)
      p = inputParser;
      p.addRequired('data');
      p.addOptional('optlabels',[],@(x) iscellstr(x));
      p.addParamValue('parent',[]);
      p.addParamValue('origin',[10 10]);
      p.addParamValue('size',[610 812]);
      p.addParamValue('units','pixels');
      p.addParamValue('title','');
      p.addParamValue('xVals',[]);
      p.addParamValue('labels',[]);
      parse(p,varargin{:});
    end
  end
  
end
    