classdef toggle < uitools.baseobjs.gui
  % Plot with toggle between butterfly and split plotting
  %
  % classdef toggle < uitools.cnlUIObj
  %
  % Written By: Damon Hyde
  % Last Edited: June 1, 2016
  % Part of the cnlEEG Project
  %  
  
  properties (SetObservable,AbortSet)
    data
    xvals
    yrange
    scale
    labels
    doSplit = false;
  end
  
  properties
    axes
    plot    
  end
  
  properties (Hidden=true)
    toggleBtn    
    shiftBtn
    displayChannels
  end
  
  methods
    
    function obj = toggle(data,varargin)
   
      %% Input Parsing
      p = inputParser;
      p.addRequired('data',@(x) isnumeric(x)&&ismatrix(x));
      addParamValue(p,'parent',[]);
      addParamValue(p,'origin',[10 10]);
      addParamValue(p,'size',[600 600]);
      addParamValue(p,'units','pixels');
      addParamValue(p,'title','');
      p.addParamValue('ax',[],@(x) ishghandle(x)&&strcmpi(get(x,'type'),'axes'));
      p.addParamValue('xvals',1:size(data,1),@(x) isempty(x)||(isvector(x)&&numel(x)==size(data,1)));
      p.addParamValue('yrange',[min(data(:)) max(data(:))],@(x) isvector(x)&&(numel(x)==2));
      p.addParamValue('scale',1,@(x) isnumeric(x)&&numel(x)==1);
      p.addParamValue('labels',[],@(x) isempty(x)||iscellstr(x));
      
      parse(p,data,varargin{:});
                  
      %% Initialize cnlUIObj
      obj = obj@uitools.baseobjs.gui('parent',p.Results.parent,...
        'origin',p.Results.origin,'size',p.Results.size,...
        'title',p.Results.title,'units',p.Results.units);
      
      %% Add the Toggle Button
      obj.toggleBtn = uicontrol('Parent',obj.panel,...
        'Style','pushbutton',...
        'String','Toggle Split View',...
        'Units','pixels',...
        'Position',[5 5 125 30]);
      set(obj.toggleBtn,'Callback',@(h,evt)obj.toggleSplit);
      set(obj.toggleBtn,'Units','normalized');
      pos = get(obj.toggleBtn,'Position');
      
      %% Set up the Plot Axis
      axisYstart = pos(2)+pos(4)+0.05;
      axisYsize = 0.99 - axisYstart;
      
      obj.axes = axes('Parent',obj.panel,'Units','Normalized',...
        'Position',[0.05 axisYstart 0.9 axisYsize]); 
      
      obj.shiftBtn(1) = uicontrol('Parent',obj.panel,...
        'Style','pushbutton',...
        'String','<-',...
        'Units','normalized',...
        'Position',[0.96 axisYstart 0.02 axisYsize/2-0.02],...
        'Callback',@(h,evt) obj.shiftDisplayed(1));
      
      obj.shiftBtn(2) = uicontrol('Parent',obj.panel,...
        'Style','pushbutton',...
        'String','->',...
        'Units','normalized',...
        'Position',[0.96 axisYstart+axisYsize/2 0.02 axisYsize/2-0.02],...
        'Callback',@(h,evt) obj.shiftDisplayed(-1));
              
      %% Set Property Values
      obj.data   = p.Results.data;
      obj.xvals  = p.Results.xvals;      
      obj.yrange = p.Results.yrange;
      obj.scale  = p.Results.scale;
      obj.labels = p.Results.labels;
            
      nDisp = 30;
      if size(obj.data,2)<nDisp, nDisp = size(obj.data,2); end;
      obj.displayChannels = 1:nDisp;
      
      %% Do Initial Display of Plot
      obj.updateImage;
    end
    
    function shiftDisplayed(obj,shiftDir)      
      dispIdx = obj.displayChannels;
      shift = round(0.25*numel(dispIdx))*shiftDir;
      maxIdx = size(obj.data,2);     
      tmp = dispIdx + shift;
      if any(tmp-maxIdx>0), shift = shift-(max(tmp)-maxIdx); end;
      if any(tmp<1), shift = shift - (min(tmp) - 1); end;
      obj.displayChannels = dispIdx + shift;      
      obj.updateImage;
    end
    
    function toggleSplit(obj)
      % Function toggle between a split and butterfly plot
      obj.doSplit = ~obj.doSplit;
      obj.updateImage;
    end
    
    function out = get.labels(obj)
      % Provides default 1:N labeling if labels haven't been provided
      if isempty(obj.labels)
        out = cell(1,size(obj.data,2));
        for i = 1:numel(out)
          out{i} = num2str(i);
        end;
      else
        out = obj.labels;
      end
    end
        
    function updateImage(obj)
      axes(obj.axes);
      cla;                 
                  
      if obj.doSplit % Do a split plot
        dispChan = obj.displayChannels;
        obj.plot = uitools.plots.split(obj.data(:,dispChan),obj.axes,...
          'xvals',obj.xvals,'yrange',obj.yrange,...
          'scale',obj.scale,'labels',obj.labels(dispChan));
      else % Just do a butterfly plot
        obj.plot = uitools.plots.butterfly(obj.data,obj.axes,...
          'xvals',obj.xvals,'yrange',obj.yrange);
      end;
         
      notify(obj,'updatedOut');
    end
    
  end
  
end