function plotOut = butterfly(data,varargin)
% Butterfly data plot
%
% plotOut = BUTTERFLY(data,varargin)
%
% Required Inputs:
%   data :  Nsamples x Nchannels Data Matrix
% 
% Optional Inputs:
%   'ax'     : Handle to a matlab axis to display in
%   'xvals'  : Values to display along X-Axis
%   'yrange' : Range to use for Y-Axis
%
% This object exists mostly to maintain a consistent structure across the
% entire uitools package. It additionally ensures that the resulting plot
% and axes keep the same button down function that the axis had prior to
% doing the plot.
%
% Written By: Damon Hyde
% Last Edited: May 24, 2016
% Part of the cnlEEG Project
%

%% Input Parsing
p = inputParser;
p.addRequired('data');
p.addOptional('ax',[],@(x) ishghandle(x)&&strcmpi(get(x,'type'),'axes'))
p.addParamValue('xvals',1:size(data,1),@(x) isempty(x)||isvector(x)&&numel(x)==size(data,1));
p.addParamValue('yrange',[min(data(:)) max(data(:))],@(x) isvector(x)&&(numel(x)==2));
p.parse(data,varargin{:});

ax = p.Results.ax;
xvals = p.Results.xvals;
yrange = p.Results.yrange;

if isempty(xvals), xvals = 1:size(data,1); end;
if isempty(ax), figure; ax = axes; end;
axes(ax);

if numel(xvals)==1, plotOpts = 'kx';
else                plotOpts = 'k';  end;

plotOut = plot(xvals,data,plotOpts,'ButtonDownFcn',get(ax,'ButtonDownFcn'));
set(ax,'ButtonDownFcn',get(plotOut(1),'ButtonDownFcn'));

XLim = [xvals(1) xvals(end)];
if XLim(1)==XLim(2), XLim = XLim + [-0.1 0.1]; end;

YLim = max(abs(yrange));

axis([XLim(1) XLim(2) -YLim YLim]);

ticks = linspace(0,YLim,6);
ticks = [-flipdim(ticks(2:end),2) ticks];
labels = 10^(-2)*round(ticks*10^2);
set(ax,'YTick',ticks);
set(ax,'YTickLabel',labels);

end

