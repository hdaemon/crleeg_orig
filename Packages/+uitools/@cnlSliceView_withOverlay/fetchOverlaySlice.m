function out = fetchOverlaySlice(obj)
% FETCHOVERLAYSLICE Fetch a slice from the overlay image
%
% function out = fetchOverlaySlice(obj)
%

if isa(obj.fhnd_getOverlaySlice,'function_handle')
  out = obj.fhnd_getOverlaySlice(obj.currAxis,obj.currSlice);
else
  out = zeros(size(obj.fetchSlice));
end;

end