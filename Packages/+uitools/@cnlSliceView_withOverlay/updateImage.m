function updateImage(obj)
% UPDATEIMAGE Update the displayed image in a cnlSliceView_withOverlay
%
% function updateImage(obj)
%
% Fetches the current slice of the image, parses it, and converts it to
% RGB.  Repeats the process for the overlay image, if provided, and then
% combines them using the obj.overlayAlpha value into a single RGB image
% that is then displayed in the current panel axes.
%
% Written By: Damon Hyde
% April 2015
% Part of the cnlEEG Project
%


% Get the overlay image slice and get index into colormap
imgOver = obj.fetchSlice;
imgOver = obj.parseSlice(imgOver,obj.imgType);
imgOver = obj.convertToCMapIdx(imgOver,obj.imgRange,obj.imgCMap);
%imgOver = obj.convertToRGB(imgOver);

% Get the main image slice and get index into colormap
imgUnder = obj.fetchOverlaySlice;
imgUnder = obj.parseSlice(imgUnder,obj.overlayType);
if any(imgUnder(:))
  imgUnder = obj.convertToCMapIdx(imgUnder,obj.overlayRange,obj.overlayMap);
else
  imgUnder = zeros(size(imgOver));
end;

imgUnder = imgUnder + size(obj.imgCMap,1) + 1;

% Fetch primary alpha value
if isa(obj.overlayAlpha,'function_handle');
  alpha = feval(obj.overlayAlpha);
else
  alpha = obj.overlayAlpha;
end;

totalCmap = [obj.imgCMap ; obj.overlayMap];

alphaMap = alpha*(imgOver~=0);

% Image magnitude map
% imgMag = sqrt(sum(imgOver.^2,3));
% maxMag = max(imgMag(:));
% 
% if maxMag~=0
%   imgOverMag = repmat(imgMag./maxMag,1,1,3);
% else
%   imgOverMag = zeros([size(imgMag) 3]);
% end
% 
% totImg = (1-alpha.*imgOverMag).*imgUnder + (alpha.*imgOverMag).*imgOver;

axes(obj.axes);
%newImg = image(totImg,'Parent',obj.axes,'ButtonDownFcn',get(obj.axes,'ButtonDownFcn'));
imgUnder = image(imgUnder,'Parent',obj.axes,'ButtonDownFcn',get(obj.axes,'ButtonDownFcn'));
hold on;
imgOver = image(imgOver,'Parent',obj.axes,'ButtonDownFcn',get(imgUnder,'ButtonDownFcn'));
hold off;
colormap(totalCmap);
set(imgOver,'AlphaData',alphaMap);
set(obj.axes,'ButtonDownFcn',get(imgUnder,'ButtonDownFcn'));
obj.drawCross;
axis off;
end
