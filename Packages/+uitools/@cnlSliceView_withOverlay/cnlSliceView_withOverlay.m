classdef cnlSliceView_withOverlay < uitools.cnlSliceView
  % CNLSLICEVIEW_WITHOVERLAY 2D Slice viewer with overlay image
  %
  % classdef cnlSliceView_withOverlay < cnlSliceView
  %
  % Adds overlay functionality to cnlSliceView
  %
  % NOTE: This object is dumb, in that it doesn't try to check and make
  % sure that the image and the overlay are in the same space.  it just
  % blindly tries to access the same set of indices in the overlay that it
  % does for the image.  It is up to the user to make sure that the image
  % and the overlay exist in the same space.
  %
  % Written By: Damon Hyde
  % March 2015
  % Part of the cnlEEG project
  %
  
    properties
      fhnd_getOverlaySlice
      overlayAlpha = 0.5
      overlayRange
      overlayMap = jet(256);
      overlayType = 'true';
    end
    
    methods
      
      function objOut = cnlSliceView_withOverlay(varargin)
        % CNLSLICEVIEW_WITHOVERLAY Slice viewer with overlay image
        %
        % function objOut = cnlSliceView_withOverlay(varargin)
        %        
        objOut = objOut@uitools.cnlSliceView(varargin{:});
        
        p = inputParser;
        p.KeepUnmatched = true;
        addParamValue(p,'fhnd_Overlay',[]);
        addParamValue(p,'overlayAlpha',0.5);
        parse(p,varargin{:});
        
        objOut.fhnd_getOverlaySlice = p.Results.fhnd_Overlay;
        objOut.overlayAlpha = p.Results.overlayAlpha;
                        
      end
          
      function out = get.overlayRange(obj)
        if isa(obj.overlayRange,'function_handle')
          out = feval(obj.overlayRange);
        else
          out = obj.overlayRange;
        end;
      end
      
      function out = get.overlayMap(obj)
        if isa(obj.overlayMap,'function_handle')
          out = feval(obj.overlayMap);
        else
          out = obj.overlayMap;
        end
      end
      
      function out = get.overlayType(obj)
        if isa(obj.overlayType,'function_handle')
          out = feval(obj.overlayType);
        else
          out = obj.overlayType;
        end
      end
      
      function set.overlayAlpha(obj,val)
        % SET.overlayAlpha Set the alpha value for the overlay image
        %
        % function set.overlayAlpha(obj,val)
        %
        obj.overlayAlpha = val;
        obj.updateImage;
      end
      

      
 
      
    end
end
