classdef vol3D < handle
  % Object class for 3D Volumes
  %
  %
  % Properties:
  %   volume : 
  %   range
  %   size
  %   name
  %   orientation
  %   aspect
  %   displaytype
  %
  
  
  properties
    name
    volume
    orientation
    aspect
  end
  
  properties (Dependent = true)   
    range    
    volSize
  end
      
  properties (Access = protected)    
    range_internal
    range_override
    % This really ought to be using a cnlGridSpace under the hood
  end;
  
  events
    updatedOut
  end
  
  methods
    
    function obj = vol3D(varargin)
      p = inputParser;
      p.addOptional('volume',[]);
      p.addParamValue('orientation','left-posterior-superior',@(x) ischar(x));
      p.addParamValue('name','VOL',@(x) ischar(x));      
      p.addParamValue('overriderange',[],@(x) isnumeric(x)&&(numel(x)==2));
      p.addParamValue('aspect',[1 1 1],@(x) isnumeric(x)&&isvector(x)&&(numel(x)==3));
      p.parse(varargin{:});
            
      obj.volume      = p.Results.volume;
      obj.name        = p.Results.name;     
      obj.orientation = p.Results.orientation;       
      obj.aspect      = p.Results.aspect;
      obj.range_override = p.Results.overriderange;
    end
      
    function out = get.volume(obj)
      out = obj.volume;
    end;
    
    function set.volume(obj,val)
      if isempty(val), obj.volume = []; return; end;
      assert(numel(size(val))==3,'vol3D is for three dimensional volumes');
      assert(isnumeric(val),'vol3D requires a numeric input');
      if ~isequal(obj.volume,val)
        obj.volume = val;    
        obj.updateRange;
        notify(obj,'updatedOut');
      end;      
    end;  
    
    function set.aspect(obj,val)
      assert(isnumeric(val)&&(numel(val)==3),...
        'Aspect ratio for vol3D must be a 1x3 vector');
      if ~isequal(obj.aspect,val)
        obj.aspect = val(:)';
        notify(obj,'updatedOut');
      end;
    end
        
    function overrideRange(obj,val)
      assert(isnumeric(val)&&(numel(val)==2),...
        'Range must be a 1x2 numeric vector');
      if ~isequal(obj.range_override,val)
        obj.range_override = val;
        notify(obj,'updatedOut');
      end
    end
        
    function out = get.range(obj)
      if isempty(obj.range_override)
        out = obj.range_internal;
      else
        out = obj.range_override;
      end;
    end;
    
    function out = get.volSize(obj)
      out = size(obj.volume);
    end;
                 
    function out = getSlice(obj,axis,slice)
      % Get a slice from the volume
      assert(~isempty(axis)&&ismember(axis,[1 2 3]),...
        'Invalid axis identifier');
      s = obj.volSize(axis);
      assert((slice>0)&&(slice<=s),...
        'Selected slice out of range');
      switch(axis)
        case 1
          out = obj.volume(slice,:,:);
        case 2
          out = obj.volume(:,slice,:);
        case 3
          out = obj.volume(:,:,slice);
      end;      
    end
    
  end    
  
  methods (Access=protected)
    function updateRange(obj)
      % Exclude infinite and non-number values
      Qnan = isnan(obj.volume(:));
      Qinf = isinf(obj.volume(:));
      valid = ~Qnan & ~Qinf;
      obj.range_internal = [min(obj.volume(valid)) max(obj.volume(valid))];
    end;
  end
  
end
      
 
  