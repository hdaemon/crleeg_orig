classdef cnl3DSurfViewer < uitools.cnlUIObj
  % CNL3DSURFVIEWER Object for visualizing data on 3D surfaces
  %
  %
  % Written By: Damon Hyde
  % Last Edited: Oct 9, 2015
  % Part of the cnlEEG Project
  %
  
  properties
    surfData
    surfCData
  end
  
  properties (Hidden=true)
    axes
    surfImage
    
    imgType = 'true';
    imgRange;
    imgCMap = jet(256);
    imgAlpha = 0.5;
  end
  
  methods
    
    function objOut = cnl3DSurfViewer(varargin)
      
      p = inputParser;
      p.addRequired('surfData');
      p.addRequired('surfCData');
      p.addParamValue('parent',[],@(h) ishandle(h));
      p.addParamValue('origin',[5 0],@(x) isvector(x) && numel(x)==2);
      p.addParamValue('size',[530 700]);
      p.addParamValue('title','DEFAULTTITLE');
      p.addParamValue('cmap','jet');
      p.addParamValue('dispType',[]);
      p.addParamValue('overalpha',0.5);
      parse(p,varargin{:});
      
      if isempty(p.Results.parent),parent = figure;
      else parent = p.Results.parent; end;
       
      objOut = objOut@uitools.cnlUIObj('parent',parent,...
        'title',p.Results.title,...
        'origin',p.Results.origin,'size',[530 700]);
      
      objOut.surfData = p.Results.surfData;
      objOut.surfCData = p.Results.surfCData;
      
      uitools.setMinFigSize(gcf,objOut.origin,objOut.size+25,5);
      
      objOut.axes = axes('Parent',objOut.panel,'Units','Normalized',...
                          'Position',[0.01 0.01 0.98 0.98]);
      objOut.updateImage;
    end
    
    function out = get.imgRange(obj)
      if isa(obj.imgRange,'function_handle')
        out = feval(obj.imgRange);
      else
        out = obj.imgRange;
      end;
    end  
    
    function out = get.imgCMap(obj)
      if isa(obj.imgCMap,'function_handle')
        out = feval(obj.imgCMap);
      else
        out = obj.imgCMap;
      end
    end
    
    function out = get.surfData(obj)
      if isa(obj.surfData,'function_handle')
        out = feval(obj.surfData);
      else
        out = obj.surfData;
      end
      
      if ~(isfield(out,'vert')&&isfield(out,'tri'))
        error('surfData must have both .vert and .tri fields');
      end
      
    end
    
    function out = get.surfCData(obj)
      if isa(obj.surfCData,'function_handle')
        out = feval(obj.surfCData)
      else
        out = obj.surfCData;
      end
      out = out(:);
    end
    
    function updateImage(obj)
      surfData  = obj.surfData;
      surfCData = obj.surfCData;
      % Some error/format checking here could be good?
      
      cDataRGB = uitools.convertImgToRGB(surfCData,obj.imgCMap,obj.imgRange);
      cDataRGB = squeeze(cDataRGB);
      
      imgRange = obj.imgRange;
      if isempty(imgRange), imgRange = [min(surfCData) max(surfCdata)]; end;
      
      
      
      axis(obj.axes);
      axis equal;
      axis vis3d;
      hpatch = patch ( 'vertices',surfData.vert,'faces',surfData.tri,...
                            'FaceVertexCData',2*surfCData);
      set ( hpatch,'EdgeColor','none','FaceColor','interp',...
                   'FaceLighting','phong','DiffuseStrength',0.8 );
      camlight right;
      lighting phong;
     % colorbar;      
    end
     
    
  end
  
end