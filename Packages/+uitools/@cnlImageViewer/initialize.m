function initialize(obj)
% INITIALIZE Initialize viewer and control objects
%
% Written By: Damon Hyde
% April 2015
% Part of the cnlEEG Project
%

% Initialize the slice image
obj.sliceImg = uitools.cnlSliceView_withOverlay(...
  'parent',obj.panel);
obj.sliceImg.title = '';

% Initialize the slice controls
obj.sliceControl = uitools.cnlSliceControl(obj.imageSizes,...
  'parent',obj.panel,'origin',[0 0]);
%obj.sliceControl.updatefunction = @(a,b)obj.sliceImg.updateSelections(a,b);
obj.sliceControl.updatefunction = @(a,b)obj.updateSliceSelection(a,b);
obj.sliceControl.selectedAxis = 1;

% Initialize color controls
obj.colorControl = uitools.cnlColorControl('parent',obj.panel,...
  'origin',[0 0]);
obj.colorControl.updatefunction = @()obj.updateImage;
obj.colorControl.title = 'Color Control';
obj.sliceImg.imgCMap = @()obj.colorControl.currCMap;
obj.sliceImg.imgType = @()obj.colorControl.currType;

% Initialize overlay color controls
obj.overlayColor = uitools.cnlColorControl('parent',obj.panel,...
  'origin',[0 0]);
obj.overlayColor.updatefunction = @()obj.updateImage;
obj.overlayColor.title = 'Overlay Color';
obj.overlayColor.visible = 'off';
obj.sliceImg.overlayMap = @()obj.overlayColor.currCMap;
obj.sliceImg.overlayType = @()obj.overlayColor.currType;

% Initialize overlay alpha control
obj.alphaControl = uitools.cnlSliderPanel([0 1],...
  'parent',obj.panel, 'origin', [0 0],'title','Overlay Alpha');
obj.alphaControl.updatefunction = @(x)updateAlpha(x);
obj.alphaControl.selectedValue = 0.5;
obj.alphaControl.visible = 'off';

  function updateAlpha(val)   
   obj.sliceImg.overlayAlpha = val;
   obj.updateImage;
  end

end