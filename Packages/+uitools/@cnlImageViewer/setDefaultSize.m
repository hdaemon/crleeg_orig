function setDefaultSize(obj)
% SETDEFAULTSIZE Set default interface sizes
%
% function SETDEFAULTSIZE(obj)
%
% Set the size of a cnlImageViewer object to its default. This sets the
% overall size of the uipanel to be 530x700 pixels, with controls inside
% sized accordingly.  This leaves space for slice selection, color
% selection, and overlay color and alpha selection.  After calling this
% method, all sub-uiobjects are left with normalized units.
%
% Written By: Damon Hyde
% April 2015
% Part of the cnlEEG Project
%

obj.setAll('units','pixels');

obj.size = [530 700];

obj.sliceImg.origin = [5 166];
obj.sliceImg.size = [520 520];

obj.sliceControl.size = [300 50];
obj.sliceControl.origin = [ 5 5];

obj.colorControl.size = [200 78];
obj.colorControl.origin = [325 5];

obj.overlayColor.size = [200 78];
obj.overlayColor.origin = [325 83];

obj.alphaControl.size = [300 40];
obj.alphaControl.origin = [5 60];

obj.setAll('units','normalized');

end