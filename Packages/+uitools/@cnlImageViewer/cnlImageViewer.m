classdef cnlImageViewer < uitools.cnlUIObj
  % CNLIMAGEVIEWER Object for visualizing 3D images in one view.
  %
  % Takes a cnlSliceView_withOverlay object, puts it inside a larger panel,
  % and adds slice control options.
  %
    
  properties
   imageSizes 
   aspectRatio
   fhnd_getImgSlice   
   fhnd_getOverlaySlice      
  end
  
  properties (Hidden=true)
   sliceImg
   sliceControl
   colorControl
   overlayColor
   alphaControl           
  end
  
  methods
    
    function objOut = cnlImageViewer(varargin)
      
      % Input Parsing
      p = inputParser;
      p.addRequired('imageSizes', @(x)isvector(x) && numel(x)==3);
      p.addParamValue('parent',[],@(h) ishandle(h));
      p.addParamValue('origin',[5 0],@(x) isvector(x) && numel(x)==2);
      p.addParamValue('size',[530 700]);
      p.addParamValue('title','DEFAULTTITLE');
      p.addParamValue('cmap','jet');
      p.addParamValue('dispType',[]);
      p.addParamValue('overalpha',0.5);
      parse(p,varargin{:});
      
      if isempty(p.Results.parent),parent = figure;
      else parent = p.Results.parent; end;
              
      % Initialize the cnlUIObj (ie: the main uipanel)
      objOut = objOut@uitools.cnlUIObj('parent',parent,...
        'title',p.Results.title,...
        'origin',p.Results.origin,'size',[530 700]);
      uitools.setMinFigSize(gcf,objOut.origin,objOut.size,5);
      
      % Initialize UI Controls
      objOut.imageSizes = p.Results.imageSizes;        
      objOut.title = p.Results.title;
      objOut.initialize;           
      objOut.colorControl.currCMap = p.Results.cmap;                  
            
      objOut.setUILocations;
      if isempty(objOut.fhnd_getOverlaySlice),
        objOut.alphaControl.selectedValue = 1;
      end;
      objOut.size = p.Results.size;
    end
    
%     function updateImage(obj)
%       obj.sliceImg.updateImage;
%     end;
    
    function set.fhnd_getImgSlice(obj,val)
      obj.fhnd_getImgSlice = val;
      if isa(obj.sliceImg,'handle')
      obj.sliceImg.fhnd_getSlice = val;
      end;
    end
    
    function updateImgAspect(obj)
      if ~isempty(obj.aspectRatio)       
      switch obj.sliceControl.selectedAxis
        case 1, daspect(obj.sliceImg.axes,1./obj.aspectRatio([2 3 1]));
        case 2, daspect(obj.sliceImg.axes,1./obj.aspectRatio([1 3 2]));
        case 3, daspect(obj.sliceImg.axes,1./obj.aspectRatio([1 2 3]));
      end
      
      
      end;
    end
    
    function updateImage(obj)
      obj.sliceImg.updateImage;
      obj.updateImgAspect;
           
    end
    
    function updateSliceSelection(obj,axis,slice)
      obj.sliceImg.updateSelections(axis,slice);
      obj.updateImgAspect;
    end
    
    function set.fhnd_getOverlaySlice(obj,val)
      obj.fhnd_getOverlaySlice = val;
      if isa(obj.sliceImg,'handle')
        obj.sliceImg.fhnd_getOverlaySlice = val;
        obj.overlayColor.visible = 'on';
        obj.alphaControl.visible = 'on';
      end
    end
    
    
    function set.imageSizes(obj,val)
      obj.imageSizes = val;
      obj.sliceControl.imageSize = val;
    end;
    
  end
  
end