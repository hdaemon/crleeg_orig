function setUILocations(obj)

% Determine where the slice image origin should be, based on what controls
% are turned on.
sliceOrigin = [5 166];
if checkViz(obj.sliceControl), sliceOrigin = [5 62];  end
if checkViz(obj.colorControl), sliceOrigin = [5 88];  end
if checkViz(obj.alphaControl), sliceOrigin = [5 100]; end;
if checkViz(obj.overlayColor), sliceOrigin = [5 166]; end

% Store current overall size and units for later
tmpUnits = obj.units;
tmpSize  = obj.size;

% Reset the viewer size to the default
obj.setDefaultSize;

% Shift the image viewer to the appropriate origin (measured in pixels)
obj.setAll('units','pixels');
obj.sliceImg.origin = sliceOrigin;
obj.size = [530 sliceOrigin(2)+535];
obj.setAll('units','normalized');


% Set overall viewer to its final size.
obj.setAll('units','normalized');
obj.units = tmpUnits;
obj.size = tmpSize;

end


function out = checkViz(obj)

  test = obj.visible;
  if strcmpi(test,'on')
    out = true;
  else
    out = false;
  end;

end