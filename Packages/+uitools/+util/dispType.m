classdef dispType
      
  properties
    name
    fPre
    fPost
  end
      
  enumeration
    true('true',@(x) x, @(x) x);
    norm('norm',@(x) x.^2, @(x) sqrt(x));
    abs('abs',@(x) abs(x), @(x) x);
    parcel('parcel',@(x) round(10*rand(size(x))), @(x) x);
    exp('exp',@(x) exp(x), @(x) x);
    log('log',@(x) uitools.util.dispType.logFunc(x), @(x) x);    
  end;
  
  methods
    
    function type = dispType(name,pre,post)
      % DISPTYPELIST Return a list of possible display types
      %
      % function types = dispTypeList
      %
      % Written By: Damon Hyde
      % April 2015
      % Part of the cnlEEG Project
      %
      type.name = name;
      type.fPre = pre;
      type.fPost = post;
              
    end
    
    
  end
  
  methods (Static=true)
   function x = logFunc(x)
    x = abs(x);
    Q = (x==0);
    x = log10(x);
    x(Q) = 0;
   end
  end
  
end
  
  
  
  