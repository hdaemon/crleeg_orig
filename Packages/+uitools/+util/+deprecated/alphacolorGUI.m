classdef alphacolorGUI < uitools.baseobjs.gui
  % GUI class for alphacolor objects
  %
  %
  
  properties    
    axes
    map
    radio    
    radiobuttons
    range
    buttons
    interptype
    zerozero
    transparent
    opaque
    shift
  end
  
  events
    closedGUI
  end
  
  methods
    
    function obj = alphacolorGUI(map,varargin)
      
      p = inputParser;
      p.addParamValue('parent',[]);
      p.addParamValue('origin',[0 0],@(x) isvector(x)&numel(x)==2);
      p.addParamValue('size',[400 400]);
      p.addParamValue('name','');
      p.addParamValue('units','pixels');
      p.parse(varargin{:});
            
      assert(exist('map','var')&&isa(map,'uitools.util.alphacolor'),...
        'Input must be a uitools.alphacolor object');
                               
      obj = obj@uitools.baseobjs.gui(...
        'Parent',p.Results.parent,...
        'units','pixels',...
        'origin',p.Results.origin,...
        'size',p.Results.size,...
        'title',['ColorMap Editor:' p.Results.name]);
      
      set(obj.parent,'DeleteFcn',@(h,evt)delete(obj));
      
      obj.map = map;            
      
      obj.axes = axes(...
          'Parent',obj.panel,...
          'Units','normalized',...
          'Position',[0.03 0.6 0.94 0.37],...
          'ButtonDownFcn',@(h,evt) obj.selectAlpha);
      
      obj.radio  = uibuttongroup(...
        'Parent',obj.panel,...
        'Units','normalized',...
        'Position',[0.03 0.03 0.3 0.54],...
        'SelectionChangeFcn',@(h,evt)obj.updateColormap);                  
      cmaps = uitools.util.colorMapList;                
        
      for i = 1:numel(cmaps)
        obj.radiobuttons(i) = uicontrol(obj.radio,...
          'Style','radiobutton',...
          'String',cmaps(i).name,...
          'Units','normalized',...
          'Position',[0.01 1-0.1*i 0.98 0.1]);
        if strcmpi(cmaps(i).name,obj.map.type)
          set(obj.radio,'SelectedObject',obj.radiobuttons(i));
        end;
      end
      set(obj.radio,'Visible','on');
      
      baseXLoc = 0.35;
      baseYLoc = 0.5;
      offsetY = 0.07;
      sizeY = 0.06;
      
      orderAlpha05 = 4;
      orderRange = 0 ;
      orderZeroTransp = 1;
      orderTransparent = 2;
      orderOpaque = 3;
      orderInterp = 5;
      orderShift = 6;
      
      obj.buttons(1) = uicontrol(...
        'Parent',obj.panel,...
        'Style','pushbutton',...
        'String','Set Alpha = 0.5',...
        'Units','normalized',...
        'Position',[baseXLoc baseYLoc-orderAlpha05*offsetY 0.63 sizeY],...
        'Callback',@(h,evt) obj.resetAlpha);
      
      obj.range(1) = uicontrol(...
         'Parent', obj.panel,...
         'Style','Text',...
         'String','Range:',...
         'Units','normalized',...
         'Position',[baseXLoc baseYLoc-orderRange*offsetY 0.2 sizeY]);
      
      obj.range(2) = uicontrol(...
         'Parent',obj.panel,...
         'Style','edit',...        
         'Units','normalized',...
         'Position',[baseXLoc+0.2 baseYLoc-orderRange*offsetY 0.2 sizeY],...
         'Callback',@(h,evt) obj.updateRange);
       
      obj.range(3) = uicontrol(...
         'Parent',obj.panel,...
         'Style','edit',...        
         'Units','normalized',...
         'Position',[baseXLoc+0.43 baseYLoc-orderRange*offsetY 0.2 sizeY],...
         'Callback',@(h,evt) obj.updateRange);
      
     obj.interptype(1) = uicontrol(...
       'Parent',obj.panel,...
       'Style','text',...
       'Units','normalized',...
       'Position',[baseXLoc baseYLoc-orderInterp*offsetY 0.25 sizeY],...
       'String','InterpType:');
       
      obj.interptype(2) = uicontrol(...
        'Parent', obj.panel,...
        'Style','popupmenu',...
        'Units','normalized',...
        'Position',[baseXLoc+0.27 baseYLoc-orderInterp*offsetY 0.33 sizeY],...
        'String',{'linear','nearest','spline','cubic'},...
        'Callback',@(h,evt) obj.updateColormap);
      
      obj.zerozero = uicontrol(...
        'Parent',obj.panel,...
        'Style','pushbutton',...
        'String','Make Zero Transparent',...
        'Units','normalized',...
        'Position',[baseXLoc baseYLoc-orderZeroTransp*offsetY 0.63 sizeY],...
        'Callback',@(h,evt) obj.transparentZero);
      
      obj.transparent = uicontrol(...
        'Parent',obj.panel,...
        'Style','pushbutton',...
        'String','Make Transparent',...
        'Units','normalized',...
        'Position',[baseXLoc baseYLoc-orderTransparent*offsetY 0.63 sizeY],...
        'Callback',@(h,evt) obj.makeTransparent);
      
      obj.opaque = uicontrol(...
        'Parent',obj.panel,...
        'Style','pushbutton',...
        'String','Make Opaque',...
        'Units','normalized',...
        'Position',[baseXLoc baseYLoc-orderOpaque*offsetY 0.63 sizeY],...
        'Callback',@(h,evt) obj.makeOpaque);
       
      obj.shift(1) = uicontrol(...
        'Parent',obj.panel,...
        'Style','pushbutton',...
        'String','Shift -',...
        'Units','normalized',...
        'Position',[baseXLoc baseYLoc-orderShift*offsetY 0.3 sizeY],...
        'Callback',@(h,evt) obj.shiftTransp(-0.05));
      
      obj.shift(2) = uicontrol(...
        'Parent',obj.panel,...
        'Style','pushbutton',...
        'String','Shift +',...
        'Units','normalized',...
        'Position',[baseXLoc+0.33 baseYLoc-orderShift*offsetY 0.3 sizeY],...
        'Callback',@(h,evt) obj.shiftTransp(0.05));                    
      
      if ( numel(map.range)==2 )
        obj.getRangeFromColormap;        
      end;
       
      % Update the plot whenever the underlying colormap is modified.
      obj.listenTo{end+1} = addlistener(obj.map,'updatedOut',...
        @(h,evt) obj.updatePlot);
      obj.updatePlot;
    end
  
    function updateInterp(obj)
      string = get(obj.interptype(2),'String');
      idx = get(obj.interptype(2),'Value');
      obj.map.interptype = string{idx};
    end
    
    function updateRange(obj)
      obj.map.range(1) = str2num(get(obj.range(2),'String'));
      obj.map.range(2) = str2num(get(obj.range(3),'String'));
    end       
    
    function getRangeFromColormap(obj)
      set(obj.range(2),'String',num2str(obj.map.range(1)));
      set(obj.range(3),'String',num2str(obj.map.range(2)));
    end;
    
    function selectAlpha(obj)
      
      selType = get(obj.parent,'SelectionType');
      
      % Find the list of currently selected points.
      alphaData = obj.map.storedVals.alpha;
      XLim = get(obj.axes,'XLim');
      origXData = linspace(XLim(1),XLim(2),obj.map.depth);
      XData = origXData(alphaData(:,1));
      YData = 1.5 - alphaData(:,2);
            
      delta = round(0.05*obj.map.depth);
      
      axesUnits = get(obj.axes,'Units');
      set(obj.axes,'Units','Normalized');      
      pos = get(obj.axes,'CurrentPoint');
      
      % Get info about selected point and assume it's new
      isNewPt = true;
      newX = pos(1,1);
      newY = 1.5 - pos(1,2);
      
      % Find proximity to existing points
      dist = abs(XData - newX);
      [minVal,idx] = min(dist);
      
      % Correct for offset in image
      [~,newX] = min(abs(origXData-newX));
      
      % If it's less than 10% of the total distance, match to closest point
      if minVal<delta, isNewPt = false; end;
        
      
      if ~isNewPt&&(ismember(alphaData(idx,1),[1 obj.map.depth]))
        alphaData(idx,2) = newY;
      else
              
      switch selType
        case 'normal'
          if isNewPt             
            alphaData(end+1,:) = [newX newY];           
          else
            alphaData(idx,1) = newX;
            alphaData(idx,2) = newY;
          end
          alphaData = sortrows(alphaData, 1);
        case 'alt'
          if ~isNewPt
            alphaData(idx,:) = []; %Remove the point if right clicking and we're close enough.          
          end;
      end;
      end;
      
      obj.map.alpha = alphaData;
      set(obj.axes,'Units',axesUnits);
      %obj.map.alpha = 1.5-pos(1,2);
      obj.updateColormap;
    end
    
    function resetAlpha(obj)
      obj.map.alpha = 0.5;
      obj.updateColormap;
    end;
    
    function shiftTransp(obj,val)
      alphaData = obj.map.storedVals.alpha;
      alphaData(:,2) = alphaData(:,2)+val;
      alphaData(alphaData(:,2)>1,2) = 1;
      alphaData(alphaData(:,2)<0,2) = 0;
      obj.map.alpha = alphaData;
      obj.updateColormap;
    end
    
    function transparentZero(obj)
      alphaData = obj.map.storedVals.alpha;
      if ~ismember(alphaData(:,1),2)
      alphaData(end+1,:) = [2 alphaData(1,2)];
      else
        alphaData(ismember(alphaData(:,1),2),2) = alphaData(1,2);
      end;
      alphaData(1,2) = 0;
      alphaData = sortrows(alphaData,1);
      obj.map.alpha = alphaData;
      obj.updateColormap;
    end
    
    function makeTransparent(obj)
      alphaData = obj.map.storedVals.alpha;
      alphaData(:,2) = 0;      
      obj.map.alpha = alphaData;
      obj.updateColormap;
    end
    
    function makeOpaque(obj)
      alphaData = obj.map.storedVals.alpha;
      alphaData(:,2) =  1;      
      obj.map.alpha = alphaData;
      obj.updateColormap;
    end
    
    function updatePlot(obj)
      
      if ~isvalid(obj), return; end;
      obj.getRangeFromColormap;
      axesDwnFcn = get(obj.axes,'ButtonDownFcn');
      axes(obj.axes); cla;
      i = image(permute(obj.map.cmap,[3 1 2]),'Parent',obj.axes);      
      set(i,'ButtonDownFcn',axesDwnFcn);
      axis off;
      XLim = get(gca,'XLim'); 
      hold on;
      
      % Plot the Line
      XData = linspace(XLim(1),XLim(2),obj.map.depth);
      YData = 1.5 - obj.map.alpha;
      p = plot(XData,YData,'k','LineWidth',2,'ButtonDownFcn',axesDwnFcn);
      
      % Plot the Points   
      alphaData = obj.map.storedVals.alpha;
      XData = XData(alphaData(:,1));
      YData = 1.5 - alphaData(:,2);
      plot(XData,YData,'kx');
      set(obj.axes,'ButtonDownFcn',axesDwnFcn);
            
    end
    
    function updateColormap(obj)
      selected = get(get(obj.radio,'SelectedObject'),'String');      
      obj.map.type = selected;
      string = get(obj.interptype(2),'String');
      idx = get(obj.interptype(2),'Value');
      obj.map.interptype = string{idx};      
      obj.map.range(1) = str2num(get(obj.range(2),'String'));            
      obj.map.range(2) = str2num(get(obj.range(3),'String'));
    end
    
    function delete(obj)
      notify(obj,'closedGUI');
      if ishandle(obj.parent)
       delete(obj.parent);     
      end;
    end;
    
  end  
end
      