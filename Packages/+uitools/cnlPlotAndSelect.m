classdef cnlPlotAndSelect
  % classdef cnlPlotAndSelect
  %
  % Class for creating plots with clickable functionality.  
  
  properties
    fhnd_ButtonDwn
  end
  
  methods
    
    function objOut = cnlPlotAndSelect(varargin)
      allH = plot(varargin{:});
      set(allH,'buttonDwnFcn',{@buttonDown,allH});
    end
    
    function out = buttonDown(obj,h,eData,allH)
      pNum = find(h==allH);
      if ~isempty(obj.fhnd_ButtonDwn)
       obj.fhnd_ButtonDwn(pNum,allH);
      end
    end
    
  end
  
end
    

