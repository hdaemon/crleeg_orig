classdef cnl4DViewer < uitools.cnlImageViewer
  
  properties
    fhnd_get4DSlice
    fourthSize 
  end
  
  properties (Hidden=true)
    imageViewer
    fourthControl
    
  end
  
  methods
  function objOut = cnl4DViewer(varargin)
      % Input Parsing
      p = inputParser;
      p.addRequired('imageSizes', @(x)isvector(x) && numel(x)==4);
      p.addParamValue('parent',[],@(h) ishandle(h));
      p.addParamValue('origin',[5 0],@(x) isvector(x) && numel(x)==2);
      p.addParamValue('size',[530 700]);
      p.addParamValue('title','DEFAULTTITLE');
      p.addParamValue('cmap','jet');
      p.addParamValue('dispType',[]);
      p.addParamValue('overalpha',0.5);
      parse(p,varargin{:});
      
      if isempty(p.Results.parent),parent = figure;
      else parent = p.Results.parent; end;      
      
      % Initialize viewer object
      objOut = objOut@uitools.cnlImageViewer(p.Results.imageSizes(2:4),...
                    'parent',parent,...
                    'title',p.Results.title,'origin',p.Results.origin,...
                    'size',p.Results.size,'cmap',p.Results.cmap,...
                    'dispType',p.Results.dispType,...
                    'overalpha',p.Results.overalpha);
                  
      objOut.fourthControl.range = [1 p.Results.imageSizes(1)];
      objOut.fourthControl.sliderstep = [1/p.Results.imageSizes(1) ...
                              10/p.Results.imageSizes(1)];
                  keyboard;
      
  end
  end
  
end