function initialize(obj)
%
%

% Initialize main viewer components
obj.initialize@uitools.cnlImageViewer;

% Initialize fourth dimension selection tool
obj.fourthControl = uitools.cnlSliderPanel([0 1],...
  'parent',obj.panel,'origin',[0 0],'title','4D-Selection');

end