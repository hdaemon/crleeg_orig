classdef cnl3DView < handle
  %
  % classdef cnl3DView < handle
  %
  % Object class for doing 3D renderings
  %
    
  properties
    parent
    origin
    size
    
    normalized = 0;
  end;
  
  methods
    
    function objOut = cnl3DView(varargin)
      p = inputParser;
      addParamValue(p,'parent',[]);
      addParamValue(p,'origin',[0 0]);
      addParamValue(p,'size',[525 525]);
      addParamValue(p,'fhnd',[]);
      addParamValue(p,'title','');
      parse(p,varargin{:});
      
      objOut.parent = p.Results.parent;
      objOut.origin = p.Results.origin;
      objOut.size   = p.Results.size;      
      objOut.fhnd_getSlice = p.Results.fhnd;
      title = p.Results.title;
           
      if isempty(objOut.parent), objOut.parent = gcf; end;
      
      % Initialize a uipanel to hold the image
      objOut.panel = uipanel('Title',title,'Parent',objOut.parent,...
                                  'Units','pixels',...
                  'Position', [objOut.origin 0 0 ] + [0 0 objOut.size]);
      
      % Make sure the figure is large enough to see it all
      uitools.setMinFigSize(gcf,objOut.origin,objOut.size+25,5);
                                
      % Set up the axes
      objOut.axes = axes('Parent',objOut.panel,'Units',...
                              'Normalized','Position',[0.01 0.01 0.98 0.98]);      
      objOut.updateImage;            
      
    end