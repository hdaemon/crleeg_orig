classdef gui < handle
  % CNLUIOBJ Base class for UI objects in cnlEEG
  %
  % classdef cnlUIObj < handle
  %
  % cnlUIObj is a basic object class to act as the parent for UI objects that
  % require passing a bunch of figure/parent/panel/axes handles around.
  % This object type just provides a container for these.
  %
  % This is really just a CNL specific wrapper for a uipanel object, since
  % (as best I can find in April 2015) you cannot inherit from uicontrols
  % objects.
  %
  % Recommended usage for child class constructors:
  %
  %       p = inputParser;
  %       p.KeepUnmatched = true;
  %       addParamValue(p,'parent',[]);
  %       addParamValue(p,'origin',[10 10]);
  %       addParamValue(p,'size',[700 700]);
  %       addParamValue(p,'title','');
  %       addParamValue(p,'data',[]);
  %       addParamValue(p,'fhnd',[]);
  %       parse(p,varargin{:});
  %
  %       objOut = objOut@uitools.cnlUIObj('parent',p.Results.parent,...
  %                 'origin',p.Results.origin,'size',p.Results.size,...
  %                 'title',p.Results.title);
  %
  % This way, the inputparser can be used to set default values for a
  % specific child class without having to define them at runtime.
      
  properties (Dependent=true)
    parent
    origin
    size
    position
    title
    units
    visible
  end
  
  properties (Hidden = true)
    panel
    listenTo = cell(0);
  end;
  
  properties (Hidden = true, Dependent = true)
    normalized;  
  end
  
  events
    updatedOut
  end
  
  methods
    
    function uiObj = gui(varargin)
      % CNLUIOBJ Base class for UI objects in cnlEEG
      %
      % function uiObj = cnlUIObj(varargin)
      %
      p = uitools.baseobjs.gui.parserVals;      
      parse(p,varargin{:});
      
      % Set parent, default to a new figure
      if isempty(p.Results.parent)
        parent = figure;
        pos = get(parent,'Position');
        set(parent,'Position',[pos(1:2) p.Results.size+2*p.Results.origin]);
      else
        parent = p.Results.parent;
      end;
           
      dispControl = uipanel('Title',p.Results.title,'Parent',parent,...
        'Units',p.Results.units,'Position',[p.Results.origin p.Results.size]);
      uiObj.panel = dispControl;      
    end;
    
    function out = get.parent(obj)
      out = get(obj.panel,'parent');
    end;
    
    function delete(obj)
      delete(obj.panel);
    end;
    
    function out = get.title(obj)
      if ishandle(obj.panel)
        out = get(obj.panel,'title');
      else
        out = [];
      end;
    end
    
    function set.title(obj,val)      
      if ishandle(obj.panel)
        set(obj.panel,'Title',val);
      else
        warning('cnlUIObj panel has not been initialized');
      end
    end
    
    function out = get.visible(obj)
      if ishandle(obj.panel)
        out = get(obj.panel,'visible');
      else
        out = false;
      end
    end
    
    function set.visible(obj,val)
      if ishandle(obj.panel)
        set(obj.panel,'Visible',val); 
      else
        warning('cnlUIObj panel has not been initialized');
      end
    end
    
    function out = get.units(obj)
      if ishandle(obj.panel)
      out = get(obj.panel,'Units');
      else
        out = [];
      end
    end
    
    function set.units(obj,val)
      % SET.UNITS
      %
      %            
      if ishandle(obj.panel)
        set(obj.panel,'Units',val);
      end;
    end
    
    function out = get.origin(obj)
      if ishandle(obj.panel)
        out = get(obj.panel,'Position');
        out = out(1:2);
      else
        out = [];
      end
    end
    
    function set.origin(obj,val)      
      if ishandle(obj.panel)        
        set(obj.panel,'Position',[val obj.size]);        
      end
    end
    
    function out = get.size(obj)
      if ishandle(obj.panel)
        out = get(obj.panel,'Position');
        out = out(3:4);
      else
        out = [];
      end
    end
        
    function set.size(obj,val)     
      if ishandle(obj.panel)        
        set(obj.panel,'Position',[obj.origin val]);        
      end;
    end
    
    function set.position(obj,val)
      if ishandle(obj.panel)
        set(obj.panel,'Position',val);
      end
    end
    
    function out = get.position(obj)
      if ishandle(obj.panel)
        out = get(obj.panel,'Position');
      else
        out = [];
      end
    end
    
        
    
    function out = get.normalized(obj)
      if ishandle(obj.panel)
        out = strcmpi(get(obj.panel,'Units'),'normalized');      
      else
        out = [];
      end
    end
    
    function set.normalized(obj,val)      
      if ishandle(obj.panel)        
        if val==true          
          set(obj.panel,'Units','normalized');
          set(obj.panel,'FontUnits','normalized');
        else          
          set(obj.panel,'Units','pixels');
          set(obj.panel,'FontUnits','points');
        end;
      end;
    end
    
  end
  
  methods (Access=protected,Static=true)
    function p = parserVals()
      p = inputParser;
      p.KeepUnmatched = true;
      p.CaseSensitive = false;
      p.addParamValue('parent',[]);
      p.addParamValue('origin',[10 10],@(x) isvector(x)&&(numel(x)==2));
      p.addParamValue('size',[500 500]);
      p.addParamValue('title','DEFAULTTITLE');
      p.addParamValue('units','pixels');
      p.addParamValue('normalized',true);
    end;
    
  end
  
end
