classdef cnlSliderPanel < uitools.cnlUIObj
  % classdef cnlSliderPanel < handle
  %
  %
  
  properties
    range
    updatefunction
  end
  
  properties (Dependent = true)
    selectedValue
  end
  
  properties (Hidden = true)
    sliceSelect
    sliceLabel
  end
  
  methods
    
    function objOut = cnlSliderPanel(varargin)
      % CNLSLIDERPANEL
      %
      % function objOut = cnlSliderPanel(varargin)
      %
    
      p = inputParser;
      p.addRequired('range');
      p.addParamValue('parent',[]);      
      p.addParamValue('origin',[0 0],@(x) isvector(x)&&numel(x)==2);
      p.addParamValue('size',[275 50]);           
      p.addParamValue('title','Slider Control');
      p.addParamValue('start',0)
      parse(p,varargin{:});
      
      % Initialize Superclass
      objOut = objOut@uitools.cnlUIObj('parent',p.Results.parent,...
                'origin',p.Results.origin,'size',p.Results.size,...
                'title',p.Results.title);
      
      objOut.range = p.Results.range;                          
      controlPanel = objOut.panel;
      
       % Draw Slider for slice selection and text to report current/max slice
      %delta = (objOut.range(2)-objOut.range(1))/100;
      objOut.sliceSelect = uicontrol('Style','slider',...
        'Parent', controlPanel, ...
        'Max',objOut.range(2),'Min',objOut.range(1),'Value',objOut.range(1),...
        'Callback',@objOut.selectSlice, ...
        'SliderStep',[0.01 0.1],...
        'Units','normalized',...
        'Position', [0.05 0.05 0.7 0.9]);
      
      objOut.sliceLabel = uicontrol('Style','text',...
        'Parent',controlPanel,...
        'String',[num2str((get(objOut.sliceSelect,'Value'))) '/' num2str(objOut.range(2))],...
        'Units','normalized',...
        'Position', [0.75 0.05 0.2 0.9]);
      
            % Set all units on all objects inside the panel to normalized      
      set(objOut.sliceSelect,'Units','normalized');
      set(objOut.sliceLabel,'Units','normalized');
      set(objOut.sliceLabel,'FontUnits','normalized');
      set(objOut.panel,'Position',[objOut.origin objOut.size]);
      set(objOut.panel,'visible','on');
      objOut.selectedValue = p.Results.start;
      objOut.updateImage;
    end
    
    function set.range(obj,val)
      % SET.RANGE
      %
      % function set.range(obj,val)
      %
      obj.range = val;
      if isa(obj.sliceSelect,'uicontrol')
        if obj.selectedValue>obj.range(2)
          obj.selectedValue = obj.range(2);
        elseif obj.selectedValue<obj.range(1)
          obj.selectedValue = obj.range(1);
        end;
        obj.setSliceLabel(obj,obj.selectedValue);
        set(obj.sliceSelect,'Max',obj.range(2),'Min',obj.range(1));        
      end
    end
    
    function out = get.range(obj)
      % GET.RANGE
      %
      
      if isa(obj.range,'function_handle')
        out = feval(obj.range);
      else
        out = obj.range;
      end
      
    end
    
    function setSliceLabel(obj,slice)
      % SETSLICELABEL
      %
      % function setSliceLabel(obj,slice)
      %
      valSlice = (1/100)*round(slice*100);
      valMax = (1/100)*round(obj.range(2)*100);
      set(obj.sliceLabel,'String',[num2str(valSlice) '/' num2str(valMax)]);
    end
       
    function out = get.selectedValue(obj)
      % GET.SELECTEDVALUE
      %
      % function out = get.selectedValue(obj)
      %
      if ~isempty(obj.sliceSelect)
        out = (get(obj.sliceSelect,'Value'));
      else % Error catching in case sliceSelect hasn't been defined yet
        out = 1;
      end;
    end
    
    function set.selectedValue(obj,val)
      % SET.SELECTEDVALUE
      %
      % function set.selectedValue(obj,val)
      %
      set(obj.sliceSelect,'Value',val);      
      obj.setSliceLabel(obj.selectedValue);       
      obj.updateImage;      
    end
    
    function selectSlice(obj,h,EventData)
      % SELECTSLICE
      %
      % function selectSlice(obj,h,EventData)
      %
      obj.setSliceLabel(obj.selectedValue);      
      obj.updateImage;
    end               
        
    function updateImage(obj)
      % UPDATEIMAGE
      %
      % function updateImage(obj)
      %
           
      if isa(obj.updatefunction,'function_handle')
        obj.updatefunction(obj.selectedValue);
      else
        %disp('Update function not defined');
      end;
    end 
    
  end
end
