% 

%%
close all; clear all; clear classes; dbstop if error
fDir= '/common/projects5/epilepsysurgicalplanning/processed/Case195/scan04_20141123/sourceanalysis/data_for_sourceanalysis/';
nrrdT1 = file_NRRD('MRI_T1.nrrd',fDir);
nrrdT2 = file_NRRD('MRI_T2.nrrd',fDir);
nrrdSeg = file_NRRD('seg_brain_crl.nrrd',fDir);

vol1 = uitools.render.vol3DSliced(nrrdT1.data,'aspect',nrrdT1.aspect);
vol2 = uitools.render.vol3DSliced(nrrdSeg.data,'aspect',nrrdSeg.aspect);

figure; ax = axes; vol1.renderToAxes(ax);
addlistener(vol1,'updatedOut', @(h,evt) vol1.renderToAxes(ax));

return;

 imgStack = uitools.render.volStack3DSliced({nrrdSeg.data nrrdT1.data nrrdT2.data },...
   {'seg_brain_crl.nrrd' 'MRI_T1.nrrd' 'MRI_T2.nrrd' });
 imgStack.axis = 1;
 imgStack.slice = 30; 
 render = imgStack.render;


%figure; ax = axes; imgStack.renderToAxes(ax); axis off tight;
%imgStack.listenTo{end+1} = ...
% addlistener(imgStack,'updatedOut', @(h,evt) imgStack.renderToAxes(ax,true));

%%
