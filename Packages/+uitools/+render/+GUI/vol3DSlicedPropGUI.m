classdef vol3DSlicedPropGUI < uitools.baseobjs.gui
  
  properties (Access=protected)
    GUIStack
  end
  
  methods
    
    function obj = vol3DSlicedPropGUI(volume,varargin)
      
      p = inputParser;
      p.addRequired('volume',@(x) isa(x,'uitools.render.vol3DSliced'));
      p.addParamValue('parent',[]);
      p.addParamValue('origin',[10 10],@(x) isvector(x)&numel(x)==2);
      p.addParamValue('size',[420 400]);
      p.addParamValue('units','pixels');
      p.parse(volume,varargin{:});
      
      nPipe = numel(volume.pipeline);
      
      obj = obj@uitools.baseobjs.gui(...
        'Parent',p.Results.parent,...
        'units','pixels',...
        'origin',p.Results.origin,...
        'size',[420 52*nPipe+20],...
        'title',['Display Property Editor:' volume.name]);
      
      set(obj.parent,'DeleteFcn',@(h,evt)delete(obj));            
            
      for i = 1:numel(volume.pipeline)
        yOrigin = 52*nPipe - 52*i + 5;
       % try
         obj.GUIStack{i} = volume.pipeline{i}.GUI(...
                    'parent',obj.panel,...
                    'Position',[10 yOrigin 400 50]);
       % catch
       %   keyboard;
       %   warning(['No GUI for Pipeline Element']);
       % end
      end
            
    end
    
  end
  
end