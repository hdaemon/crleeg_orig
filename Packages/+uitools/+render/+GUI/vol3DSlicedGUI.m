classdef vol3DSlicedGUI < uitools.baseobj
  % GUI for vol3DSliced and volStack3DSliced Object
  %
  % classdef vol3DSlicedGUI < uitools.baseobj
  %
  % Opens a two-tab UIPanel for the display of a vol3DSliced object
  %
  % Tab 1 Includes Slice Selection and Visualization
  % Tab 2 Shows all layers of the image, with buttons to open GUIs for
  %         display option and colormap control.
  %
  
  properties
    axes
    volume
  end
  
  properties (Dependent)
    axis
    slice
  end
  
  properties (Hidden = true)    
    sliceControl
    vizType
    vizWindow
    vizDepth
  end;
  
  properties (Access=protected)
    tabCont
    tabs
    imgControls
    addRemove
  end
  
  methods
    
    function obj = vol3DSlicedGUI(vol3D,varargin)
      
      %% Initialize Main Panel
      obj = obj@uitools.baseobj(varargin{:});
      
      obj.tabCont = uitabgroup('Parent',obj.panel);
      obj.tabs(1) = uitab(obj.tabCont,'Title','Image');
      obj.tabs(2) = uitab(obj.tabCont,'Title','Controls');
      
      %% Input Checking
      assert(isa(vol3D,'uitools.render.vol3DSliced')||...
        isa(vol3D,'uitools.render.volStack3DSliced'),...
        'First input must be a uitools.render.vol3DSliced object');
      obj.volume = vol3D;
      
      %% Set up Plot Axis
      obj.axes = axes('Parent',obj.tabs(1),...
        'Units', 'normalized',...
        'Position', [0.02 0.11 0.96 0.87]);
      
      %% Set up Slice Control
      obj.sliceControl = uitools.controls.selectXYZSlice(...
        obj.volume.size,...
        'Parent',obj.tabs(1),...
        'Units','normalized',...
        'origin',[0.02 0],...
        'size',[0.98 0.1]);
      
      %       obj.vizDepth = uicontrol(...
      %         'Style','edit',...
      %         'Parent',obj.tabs(1),...
      %         'Units','normalized',...
      %         'Position',[0.64 0.11 0.3 0.05],...
      %         'String',num2str(obj.volume.vizdepth),...
      %         'Callback',@(h,evt) obj.updateVizType,...
      %         'Visible','off');
      %
      %       obj.vizType = uicontrol(...
      %         'Style','popupmenu',...
      %         'Parent',obj.tabs(1),...
      %         'Units','normalized',...
      %         'Position',[0.02 0.11 0.3 0.05],...
      %         'String',{'SLICE','MAX','MIN','SUM','MEAN'},...
      %         'Callback',@(h,evt) obj.updateVizType);
      %
      %       obj.vizWindow = uicontrol(...
      %         'Style','popupmenu',...
      %         'Parent',obj.tabs(1),...
      %         'Units','normalized',...
      %         'Position',[0.32 0.11 0.3 0.05],...
      %         'String',{'ALL','GAUSS','SQUARE'},...
      %         'Callback',@(h,evt) obj.updateVizType,...
      %         'Visible','off');
      
      %% Fix Figure Size
      uitools.setMinFigSize(gcf,obj.origin,obj.size,5);
      
      %% Do initial render
      obj.volume.axis  = 1;
      obj.volume.slice = 1;
      obj.volume.renderToAxes(obj.axes,true);
      axis off tight;
      
      obj.initializeControlTab;
      
      %% Set Listeners
      % When the volume is updated, update the image.
      obj.listenTo{end+1} = ...
        addlistener(obj.volume,'updatedOut',@(h,evt) obj.volume.renderToAxes(obj.axes,true));
      % When the slice selection is updated, update the volume.
      obj.listenTo{end+1} = ...
        addlistener(obj.sliceControl,'updatedOut',@(h,evt) obj.updateSlice);
      
      obj.updateSlice;
    end
   
  
    function initializeControlTab(obj)
      if ~isempty(obj.imgControls)
        delete(obj.imgControls)
      end;
      obj.imgControls = uitools.util.imgDispProp.empty;
      
      if isa(obj.volume,'vol3DSliced')
        prop = obj.volume;
      else
        prop = obj.volume.volumes;
      end;
      
      for i = 1:numel(prop)
        obj.imgControls(i) = uitools.util.imgDispProp(...
          'Parent',obj.tabs(2),...
          'Units','normalized',...
          'Origin',[0.02 0.98-(i*0.11)],...
          'Size',[0.98 0.1],...
          'Name',prop(i).name);
        obj.listenTo{end+1} = addlistener(obj.imgControls(i),'editCMap',...
          @(h,evt) obj.editColorMap(i));
        obj.listenTo{end+1} = addlistener(obj.imgControls(i),'editDispProp',...
          @(h,evt) obj.editDispProp(i));
        %       obj.listenTo{end+1} = addlistener(obj.imgControls(i),'visUpdated',...
        %         @(h,evt) obj.setVisible(i));
        %       obj.listenTo{end+1} = addlistener(obj.imgControls(i),'typeUpdated',...
        %         @(h,evt) obj.setDisplayType(i));
      end;
      
      obj.addRemove(1) = uicontrol('Style','pushbutton',...
        'Parent',obj.tabs(2),...
        'Units','normalized',...
        'Position',[0.02 0.02 0.47 0.05],...
        'String','Add Layer');
      
      obj.addRemove(2) = uicontrol('Style','pushbutton',...
        'Parent',obj.tabs(2),...
        'Units','normalized',...
        'Position',[0.51 0.02 0.47 0.05],...
        'String','Remove Layer');
    end
  end
  
  methods (Access=protected)
    function updateImgAspect(obj)
      if ~isempty(obj.volume.aspect)
        switch obj.volume.axis
          case 1, daspect(obj.axes,1./obj.volume.aspect([2 3 1]));
          case 2, daspect(obj.axes,1./obj.volume.aspect([1 3 2]));
          case 3, daspect(obj.axes,1./obj.volume.aspect([1 2 3]));
        end
      end;
    end
    
    function updateSlice(obj)
      
      if ( obj.volume.axis ~= obj.sliceControl.selectedAxis)
        obj.volume.axis  = obj.sliceControl.selectedAxis;
        obj.updateImgAspect;
      end;
      if ( obj.volume.slice ~= obj.sliceControl.selectedSlice )
        obj.volume.slice = obj.sliceControl.selectedSlice;
      end;
    end
    %     function setDisplayType(obj,i)
    %       if isa(obj.volume,'vol3DSliced')
    %         obj.volume.displaytype = obj.imgControls(i).displaytype;
    %       else
    %         obj.volume.volumes(i).displaytype = obj.imgControls(i).displaytype;
    %       end;
    %     end;
    %
    function setVisible(obj,i)
      if isa(obj.volume,'vol3DSliced')
        obj.volume.displayOn = obj.imgControls(i).isVisible;
      else
        obj.volume.volumes(i).displayOn = ...
          obj.imgControls(i).isVisible;
      end
    end
    
    %     function updateVizType(obj)
    %       string = get(obj.vizType,'String');
    %       idx = get(obj.vizType,'Value');
    %       obj.volume.viztype = string{idx};
    %
    %
    %       if isequal(obj.volume.viztype,'slice')
    %         set(obj.vizWindow,'Visible','off');
    %       else
    %         set(obj.vizWindow,'Visible','on');
    %       end
    %
    %       window = get(obj.vizWindow,'String');
    %       idx = get(obj.vizWindow,'Value');
    %       obj.volume.vizwindow = window{idx};
    %
    %       if isequal(obj.volume.vizwindow,'all')
    %         set(obj.vizDepth,'Visible','off');
    %       else
    %         set(obj.vizDepth,'Visible','on');
    %       end;
    %
    %       depth = get(obj.vizDepth,'String');
    %       obj.volume.vizdepth = str2num(depth);
    %
    %     end
    
    function editColorMap(obj,i)
      if isa(obj.volume,'uitools.render.vol3DSliced')
        obj.volume.colormap.edit('name',obj.volume.name);
      else
        obj.volume.volumes(i).colormap.edit('name',obj.volume.volumes(i).name);
      end;
    end;
    
    function editDispProp(obj,i)
      if isa(obj.volume,'uitools.render.vol3DSliced')
        obj.volume.dispPropGUI;
      else
        obj.volume.volumes(i).dispPropGUI;
      end;
    end
    
  end
  
  
end
