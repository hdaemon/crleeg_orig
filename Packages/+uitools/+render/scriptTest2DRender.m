% A script demonstrating how the uitools package can be used to render
% 2-dimensional images.
%

%%
close all; clear all; clear classes;
img1 = uitools.render.image2D(repmat(1:100,100,1));
img2 = uitools.render.image2D(repmat((1:100)',1,100));

imgStack = uitools.render.imageStack2D([img1 img1]);

img1.colormap.alpha = 0.5;

figure; ax = axes; imgStack.renderToAxes(ax); axis off tight;
l = addlistener(imgStack,'updatedOut',@(h,evt) imgStack.renderToAxes(ax,true));

imgStack.images(2) = img2;

img2.colormap.alpha = 0.5;

figure; ax = axes; imgStack.renderToAxes(ax); axis off tight;

%%
clear all; close all; clear classes;
cd /common/projects5/epilepsysurgicalplanning/processed/Case195/scan04_20141123/sourceanalysis/data_for_sourceanalysis
nrrdT1 = file_NRRD('MRI_T1.nrrd');
nrrdSeg = file_NRRD('seg_brain_crl.nrrd');

imgSlice1 = nrrdT1.getSlice('axis',3,'slice',100); %squeeze(nrrdT1.data(84,:,:));
imgSlice2 = nrrdSeg.getSlice('axis',3,'slice',100); %squeeze(nrrdSeg.data(84,:,:));

imgSlice1 = uitools.util.orientMRSlice(imgSlice1,3,nrrdT1.space);
imgSlice2 = uitools.util.orientMRSlice(imgSlice2,3,nrrdT1.space);

img1 = uitools.render.image2D(imgSlice1);
img2 = uitools.render.image2D(imgSlice2);

imgStack = uitools.render.imageStack2D([img1 img2]);

figure; ax = axes; imgStack.renderToAxes(ax); axis off tight;
l = addlistener(imgStack,'updatedOut',@(h,evt) imgStack.renderToAxes(ax,true));

