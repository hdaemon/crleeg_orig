classdef image2D < handle
  % Object for displaying 2D images
  %
  % Combines a 2D image, represented as a matrix, with a
  % uitools.util.alphacolor object to provide rendering of
  %
  % Written By: Damon Hyde
  % Part of the cnlEEG Project
  % 2016
  %
  
  
  properties
    image
    colormap
  end
  
  properties (Access=protected,Dependent = true)
    rgbimage
    alphaimage
  end
  
  properties (Hidden = true)
    listenTo = cell(0);
    displayOn = true;
  end
  
  events
    updatedOut
  end
  
  methods
    
    function obj = image2D(varargin)
      p = inputParser;
      p.addOptional('image',[]);
      p.addOptional('cmap',uitools.util.alphacolor,@(x) isa(x,'uitools.util.alphacolor'));
      p.parse(varargin{:});
      
      %assert(logical(exist('image','var')),'Must provide input image');
      obj.image = p.Results.image;
      obj.colormap = p.Results.cmap;
      
      % When the colormap is updated, notify those watching this image.
      obj.listenTo{1} = addlistener(obj.colormap,'updatedOut',...
        @(h,evt) notify(obj,'updatedOut'));
    end
    
    function rgb = get.rgbimage(obj)
      [rgb,~] = obj.colormap.img2rgb(obj.image);
    end
    
    function alpha = get.alphaimage(obj)
      [~,alpha] = obj.colormap.img2rgb(obj.image);
    end;
    
    function set.image(obj,val)
      % If it's empty, we're either initializing an empty object, or
      % clearing the image value.
      if isempty(val), obj.image = val; return; end;
      
      % Validate Input
      assert(isnumeric(val)&&ismatrix(val),...
        'Input image must be a 2D array');
      
      % Only update if obj.image is changing.
      if ~isequal(obj.image,val)
        obj.image = val;
        notify(obj,'updatedOut');
      end;
    end;
    
    function set.colormap(obj,val)
      assert(isa(val,'uitools.util.alphacolor'),...
        'Colormap must be a uitools.util.alphacolor object');
      obj.colormap = val;
      notify(obj,'updatedOut');
    end;
    
    function renderToAxes(obj,ax,clearfirst)
      if obj.displayOn
      assert(isa(ax,'matlab.graphics.axis.Axes'),...
        'Must define axes to render in');
      
      if ~exist('clearfirst','var'), clearfirst = false; end;
      
      if isempty(obj.image),
        warning('Image field is empty. Nothing to render');
        return;
      end
      
      axes(ax);
      if clearfirst, cla; end;
      hold on;
      [rgb, alpha] = obj.colormap.img2rgb(obj.image);
      tmp = get(ax,'ButtonDownFcn');
      image(rgb,'AlphaData',alpha,'ButtonDownFcn',tmp);
      set(ax,'ButtonDownFcn',tmp);
      hold off;
      end;
    end
    
    function render(obj,varargin)
      p = inputParser;
      p.addOptional('ax',[],@(x) isa(x,'matlab.graphics.axis.Axes'));
      p.addOptional('clearfirst',false,@(x) islogical(x));
      p.parse(varargin{:});
      
      ax = p.Results.ax;
      if isempty(ax); figure; ax = gca; end;
      obj.renderToAxes(ax,p.Results.clearfirst);
      axis off tight;
      obj.listenTo{end+1} = ...
        addlistener(obj,'updatedOut',@(h,evt) obj.renderToaAxes(ax));
    end;
    
  end
  
end


