classdef vol3DSliced < handle
  % Render a slice of a 3D rectangular volume
  %
  % Written By: Damon Hyde
  % Part of the cnlEEG Project
  % 2016
  %
  
  properties (Dependent=true)    
    axis
    slice
    size
    colormap
    name    
    orientation
    aspect    
    volumeObj
  end
  
  properties
    inputVol
  end
   
  properties (Hidden = true)    
    displayOn = true;
    pipeline
  end
  
  properties (Access=protected)       
    axis_pvt
    slice_pvt        
    
    imgRenderer
    GUI
    propGUI
  end
  
  properties (Access=protected,Dependent=true)
    displayVol
    sliceImg    
  end
  
  properties (Hidden = true)
    listenTo = cell(0);
  end
  
  events
    updatedOut
  end
  
  methods
    
    function obj = vol3DSliced(varargin)
      p = inputParser;
      p.addOptional('volume',[]);
      p.addOptional('cmap',uitools.util.alphacolor,@(x) isa(x,'uitools.util.alphacolor'));
      p.addParamValue('orientation','left-posterior-superior',@(x) ischar(x));
      p.addParamValue('name','VOL',@(x) ischar(x));
      p.addParamValue('aspect',[1 1 1],@(x) isnumeric(x)&&isvector(x)&&(numel(x)==3));
      p.addParamValue('displaytype','true',@(x) ischar(x));
      p.addParamValue('viztype','none',@(x) ischar(x));
      p.addParamValue('vizwindow','all',@(x) ischar(x));
      p.addParamValue('vizdepth',10,@(x) isscalar(x));
      p.addParamValue('axis',1,@(x) isscalar(x));
      p.addParamValue('slice',1,@(x) isscalar(x));
      p.parse(varargin{:});
      
      obj.imgRenderer = uitools.render.image2D([],p.Results.cmap);
      
      if isa(p.Results.volume,'uitools.datatype.vol3D')
        % Already have the vol3D object
        obj.inputVol = p.Results.volume;
      else
        % Old method of calling this
        obj.inputVol = uitools.datatype.vol3D(p.Results.volume,...
          'orientation',p.Results.orientation,...
          'aspect',p.Results.aspect,...
          'name',p.Results.name);                        
      end;
      
      obj.pipeline{1} = cnlPLMod.VOL.staticVolFcn(obj.inputVol,...
        'functionType',p.Results.displaytype);
      obj.pipeline{2} = cnlPLMod.VOL.movingVolFcn(obj.pipeline{1});
      obj.pipeline{2}.savesPrecomputedOutput = true;
            
      
      obj.colormap.range = obj.volumeObj.range;
     % obj.viztype = p.Results.viztype;
     % obj.vizwindow = p.Results.vizwindow;
     % obj.vizdepth = p.Results.vizdepth;
            
      obj.axis  = p.Results.axis;
      obj.slice = p.Results.slice;
      obj.listenTo{1} = addlistener(obj.colormap,'updatedOut',...
        @(h,evt) notify(obj,'updatedOut'));
      obj.listenTo{2} = addlistener(obj.pipeline{end},'outputUpdated',...
        @(h,evt) obj.updateImg);
      
    end
    
    function updateImg(obj)
      obj.colormap.range = obj.volumeObj.range;
      notify(obj,'updatedOut');
    end
    
    %% Get/Set Volume
    function out = get.volumeObj(obj)
      out = obj.pipeline{end}.output;
    end;
    
    function set.volumeObj(obj,val)
      obj.inputVol = val;
    end;
    
    function set.inputVol(obj,val)
      assert(isa(val,'uitools.datatype.vol3D'),...
        'Input must be a uitools.datatype.vol3D object');
      obj.inputVol = val;
    end;
              
%     function set.displaytype(obj,val)    
%       obj.pipeline{1}.functionType = val;                 
%     end    
    
    %% Get/Set From the Display Volume
    function out = get.size(obj)
      out = obj.volumeObj.volSize;
    end;
    
    function out = get.name(obj)
      out = obj.volumeObj.name;
    end;
    
    function out = get.orientation(obj)
      out = obj.volumeObj.orientation;
    end;
    
    function out = get.aspect(obj)
      out = obj.volumeObj.aspect;
    end;
    
    %% Get/Set Colormap
    function out = get.colormap(obj)
      % obj.colormap just outputs the current colormap used by the image
      % renderer
      if isempty(obj.imgRenderer), out = []; return; end;
      out = obj.imgRenderer.colormap;
    end;
    
    function set.colormap(obj,val)
      % Setting the colormap updates the colormap used by the 2D image
      % renderer.
      if isempty(obj.imgRenderer),
        warning('No image renderer present. Object probably not initialized correctly');
        return;
      end
      obj.imgRenderer.colormap = val;      
    end
    
    %% Get/Set Selected Axis
    function out = get.axis(obj)
      out = obj.axis_pvt;
    end
    
    function set.axis(obj,val)
      assert(ismember(val,[1 2 3]),'obj.axis must be in the set {1,2,3}');
      if ~isequal(obj.axis_pvt,val)
        obj.axis_pvt = val;
        if obj.slice>obj.volumeObj.volSize(obj.axis)
          obj.slice = 1;
        end;
        obj.pipeline{2}.axis = obj.axis_pvt;
        notify(obj,'updatedOut');
      end;
    end
    
    %% Get/Set Selected Slice
    function out = get.slice(obj)
      out = obj.slice_pvt;
    end
    
    function set.slice(obj,val)
      if ~isempty(obj.volumeObj)
        min = 1;
        max = obj.volumeObj.volSize(obj.axis);
        if ( val < min ), val = min; end;
        if ( val > max ), val = max; end;
        if ~isequal(obj.slice_pvt,val)
          obj.slice_pvt = val;
          notify(obj,'updatedOut');
        end
      end
    end;
    
    %% Turn Display On/Off
    function set.displayOn(obj,val)
      if ~isequal(obj.displayOn,val)
        obj.displayOn = val;
        notify(obj,'updatedOut');
      end;
    end;
        
    %% Get the Correct Slice from the Output Volume
    function out = get.sliceImg(obj)
       out = obj.volumeObj.getSlice(obj.axis,obj.slice);       
    
      out = squeeze(out);
      out = uitools.util.orientMRSlice(out,obj.axis,obj.volumeObj.orientation);
    end
    
    
    function varargout = dispPropGUI(obj,varargin)
      if ~isempty(obj.propGUI)&&isvalid(obj.propGUI)&&...
          ishandle(obj.propGUI.parent)&&isvalid(obj.propGUI.parent)
        figure(obj.propGUI.parent);        
      else
        obj.propGUI =  uitools.render.GUI.vol3DSlicedPropGUI(obj,varargin{:});        
      end;
      if nargout>0
        varargout{1} = obj.propGUI;
      end;
    end;
    
    function renderToAxes(obj,ax,clearfirst)
      % Render Image from Selected Slice/Axis Pair to A Particular Axes
      %
      % renderToAxes(obj,ax,clearfirst)
      assert(isa(ax,'matlab.graphics.axis.Axes'),...
        'Must define axes to render in');
      
      if ~isvalid(ax), return; end;
      if ~exist('clearfirst','var'), clearfirst = false; end;
      
      obj.imgRenderer.image = obj.sliceImg;
      if obj.displayOn
      obj.imgRenderer.renderToAxes(ax,clearfirst);
      end;
    end
    
    function render(obj,varargin)
      if ~isempty(obj.GUI)&&isvalid(obj.GUI)&&...
          ishandle(obj.GUI.parent)&&isvalid(obj.GUI.parent),
        % If a GUI is already open, raise that figure.
        figure(obj.GUI.parent);
      else
        obj.GUI = uitools.render.GUI.vol3DSlicedGUI(obj,varargin{:});
        
      end;
      if nargout>0, varargout{1} = obj.GUI; end;                 
    end;
    
    function setSymmetricTrueRedBlue(obj)
      % Utility method for a common visualization configuration
      obj.pipeline{1}.functionType = 'true';
      obj.colormap.resetAlpha(1);
      obj.colormap.makeSymmetric;
      obj.colormap.type = 'redblue';
      obj.colormap.transparentZero;      
    end
    
  end
  
end


