classdef imageStack2D < handle
  % Extends the image2D object type to work for multiple images
  %
  % Used for rendering a number of 2D images in a single stack
  %
  % Input:
  %  images : An array of uitools.render.image2D objects.
  %
  % Written By: Damon Hyde
  % Part of the cnlEEG Project
  % 2016
  %
  
  properties
    images
  end
  
  properties (Hidden = true)
    listenTo = cell(0);
  end
  
  events
    updatedOut
  end
  
  methods
    
    function obj = imageStack2D(images)
      if nargin>0
      obj.images = images;
      end;
    end
    
    function updateListeners(obj)
      obj.listenTo = cell(0);
      for i = 1:numel(obj.images)
        obj.listenTo{end+1} = ...
          addlistener(obj.images(i),'updatedOut',@(h,evt) notify(obj,'updatedOut'));
      end
    end
    
    function set.images(obj,val)
      assert(isempty(val)||isa(val,'uitools.render.image2D'),...
        'Input images must be uitools.render.image2D objects');
      obj.images = val;
      obj.updateListeners;
      notify(obj,'updatedOut');
    end
    
    function renderToAxes(obj,ax,clearfirst)
      assert(isa(ax,'matlab.graphics.axis.Axes'),...
        'Must define axes to render in');
      
      % Skip rendering if it's an invalid axis;
      if ~isvalid(ax), return; end;
      if ~exist('clearfirst','var')
        clearfirst = false;
      end;
      
      if clearfirst
        axes(ax); cla;
      end;
      
      for i = 1:numel(obj.images)
        obj.images(i).renderToAxes(ax);
      end
      
    end
    
    function render(obj,varargin)
      p = inputParser;
      p.addOptional('ax',[],@(x) isa(x,'matlab.graphics.axis.Axes'));
      p.addOptional('clearfirst',false,@(x) islogical(x));
      p.parse(varargin{:});
      
      ax = p.Results.ax;
      if isempty(ax); figure; ax = gca; end;
      obj.renderToAxes(ax,p.Results.clearfirst);
      axis off tight;
      obj.listenTo{end+1} = ...
        addlistener(obj,'updatedOut',@(h,evt) obj.renderToaAxes(ax));
    end;
    
  end
  
end

