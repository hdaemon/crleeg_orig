classdef volStack3DSliced < handle
  % Renderer for multiple 3D volume images
  %
  %
  
  
  properties
    % An array of uitools.render.vol3DSliced objects.
    volumes    
  end
  
  properties (Dependent=true)
    % These get directly linked to the axis and slice parameters of each
    % individual volume
    axis
    slice
    size
    name
    aspect
    orientation
    renderorder
   % viztype
   % vizwindow
   % vizdepth
  end
  
  properties (Access=protected)
    GUI
    renderorder_internal
  end
  
  properties (Hidden = true)
    listenTo = cell(0);
  end;
  
  events
    updatedOut
  end
  
  methods
    
    function obj = volStack3DSliced(varargin)
      
      %% Input Parsing
      p = inputParser;
      p.addOptional('volumes',[]);
      p.addOptional('names',[],@(x) ischar(x)||iscellstr(x));
      p.addParamValue('aspect',[1 1 1], @(x) isnumeric(x)&isvector(x)&numel(x)==3);
      p.addParamValue('axis',1,@(x) isscalar(x));
      p.addParamValue('slice',1,@(x) isscalar(x));
      p.addParamValue('viztype','none',@(x) ischar(x));
      p.parse(varargin{:});
      
      %% Check Names and Set Defaults if Necessary
      if ~isempty(p.Results.names)
        assert(numel(p.Results.names)==numel(p.Results.volumes),...
          'Number of names must match number of volumes');
        names = p.Results.names;
      else
        names = cell(0);
        for i = 1:numel(p.Results.volumes)
          names{i} = ['Volume ' num2str(i)];
        end
      end
      
      %% Set the Volumes
      volumes = p.Results.volumes;      
      if isa(volumes,'uitools.render.vol3DSliced')
        obj.volumes = volumes;
      elseif isa(volumes,'uitools.datatype.vol3D')
        obj.volumes = uitools.render.vol3DSliced.empty;
        for i = 1:numel(volumes)
          obj.volumes(i) = uitools.render.vol3DSliced(volumes(i),'aspect',p.Results.aspect);
        end;
      elseif isa(volumes,'cell')
        % Construct image stack.
        obj.volumes = uitools.render.vol3DSliced.empty;
        for i = 1:numel(volumes)
          obj.volumes(i) = uitools.render.vol3DSliced(volumes{i},'name',names{i},'aspect',p.Results.aspect);
        end;
      end;
      
      obj.axis = p.Results.axis;
      obj.slice = p.Results.slice;
      %obj.viztype = p.Results.viztype;
    end
    
%     function out = get.viztype(obj)
%       out = obj.volumes(1).viztype;
%     end
%     
%     function set.viztype(obj,val)
%       for i = 1:numel(obj.volumes)
%         obj.volumes(i).viztype = val;
%       end
%     end
%     
%     function out = get.vizwindow(obj)
%       out = obj.volumes(1).vizwindow;
%     end
%     
%     function set.vizwindow(obj,val)
%       for i = 1:numel(obj.volumes)
%         obj.volumes(i).vizwindow = val;
%       end
%     end
%     
%     function out = get.vizdepth(obj)
%       out = obj.volumes(1).vizdepth;
%     end
%     
%     function set.vizdepth(obj,val)
%       for i = 1:numel(obj.volumes)
%         obj.volumes(i).vizdepth = val;
%       end
%     end
    
    %% Get/Set Image Aspect
    function out = get.aspect(obj)
      if isempty(obj.volumes), out = [1 1 1]; return;end;
      out = obj.volumes(1).aspect;
    end;
    
    function set.aspect(obj,val)
      if isempty(obj.volumes), out = []; return; end;
      for i = 1:numel(obj.volumes);
        obj.volumes(i).aspect = val;
      end
    end
    
    %% Get Image Size
    function out = get.size(obj)
      if isempty(obj.volumes),out = [];return; end;
      out = obj.volumes(1).size;
    end;
    
    function out = get.orientation(obj)
      if isempty(obj.volumes), out = []; return; end;
      out = obj.volumes(1).orientation;
    end;
    
    %% Get/Set Selected Axis
    function out = get.axis(obj)
      if isempty(obj.volumes), out = []; return; end;
      out = obj.volumes(1).axis;
    end;
    
    function set.axis(obj,val)
      for i = 1:numel(obj.volumes)
        obj.volumes(i).axis = val;        
      end
      if ~isempty(obj.GUI)
          obj.GUI.sliceControl.selectedAxis = val;
      end
    end
    
    %% Get/Set Selected Slice
    function out = get.slice(obj)
      out = obj.volumes(1).slice;
    end;
    
    function set.slice(obj,val)
      for i = 1:numel(obj.volumes)
        obj.volumes(i).slice = val;        
      end;
      if ~isempty(obj.GUI)
        obj.GUI.sliceControl.selectedSlice = val;
      end;
    end
    
    function set.renderorder(obj,val)
      assert(numel(val)==numel(obj.volumes),...
        'Rendering ordering length must match number of volumes');
      assert(all(ismember(1:numel(obj.volumes),val)),...
        'All volumes must be listed in render order');
      obj.renderorder_internal = val;
    end
    
    function out = get.renderorder(obj)
      if isempty(obj.renderorder_internal)
        out = 1:numel(obj.volumes);
      end
    end
    
    function updateListeners(obj)
      obj.listenTo = cell(0);
      for i = 1:numel(obj.volumes)
        obj.listenTo{end+1} = ...
          addlistener(obj.volumes(i),'updatedOut',@(h,evt) notify(obj,'updatedOut'));
      end
    end
    
    function set.volumes(obj,val)
      assert(isempty(val)||isa(val,'uitools.render.vol3DSliced'),...
        'Input images must be uitools.render.vol3DSliced objects');
      
      sizes = cellfun(@(x) size(x),{obj.volumes},'UniformOutput',false);
      testSizes = all(cellfun(@(x) isequal(sizes{1},x),sizes));
      
      assert(testSizes,'All volume sizes must be the same');
      
      obj.volumes = val;
      obj.updateListeners;
      notify(obj,'updatedOut');
    end
    
    function renderToAxes(obj,ax,clearfirst)
      assert(isa(ax,'matlab.graphics.axis.Axes'),...
        'Must define axes to render in');
      
      % Skip rendering if it's an invalid axis;
      if ~isvalid(ax), return; end;
      if ~exist('clearfirst','var')
        clearfirst = false;
      end;
      
      if clearfirst
        axes(ax); cla;
      end;
      
      for i = 1:numel(obj.volumes)
        obj.volumes(numel(obj.volumes)-i+1).renderToAxes(ax);
      end
      
    end
    
    function varargout = render(obj,varargin)
      if ~isempty(obj.GUI)&&isvalid(obj.GUI)&&...
          ishandle(obj.GUI.parent)&&isvalid(obj.GUI.parent),
        % If a GUI is already open, raise that figure.
        figure(obj.GUI.parent);
      else
        obj.GUI = uitools.render.GUI.vol3DSlicedGUI(obj,varargin{:});
        
      end;
      if nargout>0, varargout{1} = obj.GUI; end;
    end;
    
  end
  
end