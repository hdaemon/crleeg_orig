function updateSelections(obj,axis,slice)
% UPDATESELECTIONS Update selected axis and slice
%
% function updateSelections(obj,axis,slice)
%

% Clear crosshairs info if we're changing display axes
if ~(axis==obj.currAxis)
  obj.clearCross;
end;

obj.currAxis = axis;
obj.currSlice = slice;

% If we've defined an alternate image update function, run it.
% Otherwise, run the local updateImage method.
if ~isempty(obj.fhnd_linkedUpdate)&&isa(obj.fhnd_linkedUpdate,'function_handle')
  feval(obj.fhnd_linkedUpdate);
else
  obj.updateImage;
end;
end