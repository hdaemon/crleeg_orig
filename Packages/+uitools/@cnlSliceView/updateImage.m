function updateImage(obj)
% UPDATEIMAGE Update image in cnlSliceView viewer
%
% function updateImage(obj)
%
% Fetches the current slice of the image, parses it, and converts it to
% RGB, then displays it in the current panel axes.
%
% Written By: Damon Hyde
% April 2015
% Part of the cnlEEG Project
%

% Fetch slice, parse image, and convert to RGB
img = fetchSlice(obj);
img = obj.parseSlice(img);
img = obj.convertToRGB(img);
img = obj.convertToCMapIdx(img,obj.imgRange,obj.imgCMap);

% Draw Image
axes(obj.axes);
obj.sliceImg = image(img,'Parent',obj.axes,...
                         'ButtonDownFcn',get(obj.axes,'ButtonDownFcn'));
colormap(obj.imgCMap);
set(obj.axes,'ButtonDownFcn',get(obj.sliceImg,'ButtonDownFcn'));

% Draw crosshairs, if desired.
obj.drawCross;
axis off;
end