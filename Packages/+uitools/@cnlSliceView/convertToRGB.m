function RGBout = convertToRGB(obj,slice,cmap,imgRange)
% CONVERTTORGB Convert a 2D image to RGB
%
% function RGBout = convertToRGB(obj,slice,cmap,imgRange)
%
% Inputs: 
%   obj      : cnlSliceView Object
%   slice    : Slice Image to Convert to RGB
%   cmap     : <optional> Color map to use (default: jet(256));
%   imgRange : <optional> Image range to display 
%               (default: [min(slice(:)) max(slice(:))])
%
% Written By: Damon Hyde
% Last Edited: April 2015
% Part of the cnlEEG Project
%

% If scaling is not provided, just use the max/min of the slice
if ~exist('imgRange','var')||isempty(imgRange)
if isempty(obj.imgRange), 
  imgRange = [min(slice(:)) max(slice(:))];
else
  imgRange = obj.imgRange;
end;
end;

imgMin = imgRange(1);
imgMax = imgRange(2);

% If a colormap isn't provided, use the one from the cnlSliceView object
if ~exist('cmap','var'), cmap = obj.imgCMap; end;

% If it's still empty, default to jet(256)
if isempty(cmap), cmap = jet(256); end;

% set NaN values to zero.
slice(isnan(slice)) = 0;
slice(isinf(slice)) = 0;

% Change things so they look right.
if imgMax~=0
  RGBout = (slice-imgMin)*(size(cmap,1)-1)/(imgMax-imgMin);
else
  RGBout = zeros(size(slice));
end;
RGBout = round(RGBout);
RGBout = RGBout+1;

% Generate the actual RGB image.
%RGBout = cmap(RGBout,:);
RGBout = reshape(RGBout,[size(slice) 3]);

end

