classdef cnlSliceView < uitools.cnlUIObj & uitools.cnlAddCross
  % CNLSLICEVIEW Viewer object for 2D slice images
  %
  % classdef cnlSliceView < handle
  %
  % Object class for doing basic slice visualizations.
  %
  % Usage:
  %    objOut = cnlSliceView(varargin)
  %
  % Optional input variables:
  %    parent -> handle object to parent uipanel to
  %    origin -> location of uipanel in figure
  %    size   -> size of uipanel (in pixels)
  %    fhnd   -> function handle to call when fetching slices.  Needs to be
  %                of the form  f(currAxis,currSlice), and return a 2d
  %                image
  %    title  -> Title for the uipanel
  %
  % Other properties which can be set outside the constructor:
  %    normalized -> Flag to declare pixel or normalized units
  %    currSlice  -> current slice to display
  %    currAxis   -> current axis to display
  %
  
  properties
    fhnd_getSlice
    fhnd_linkedUpdate
%     imgMin = [];
%     imgMax = [];
    imgRange;
    imgCMap = jet(256);
    imgType = 'true';
  end
  
  properties (SetAccess = protected)
    currSlice = 1;
    currAxis  = 1;    
  end
  
  properties (Hidden = true)
    axes
    sliceImg
  end
  
  methods
    function objOut = cnlSliceView(varargin)
      
      p = inputParser;
      p.KeepUnmatched = true;
      addParamValue(p,'parent',[]);
      addParamValue(p,'origin',[0 0]);
      addParamValue(p,'size',[525 525]);
      addParamValue(p,'title','');
      addParamValue(p,'fhnd',[]);      
      parse(p,varargin{:});
      
      objOut = objOut@uitools.cnlUIObj('parent',p.Results.parent,...
                'origin',p.Results.origin,'size',p.Results.size,...
                'title',p.Results.title);
      objOut = objOut@uitools.cnlAddCross;              
              
      objOut.fhnd_getSlice = p.Results.fhnd;
                  
      % Make sure the figure is large enough to see it all
      uitools.setMinFigSize(gcf,objOut.origin,objOut.size+25,5);
                                
      % Set up the axes
      objOut.axes = axes('Parent',objOut.panel,'Units',...
                              'Normalized','Position',[0.01 0.01 0.98 0.98]);      
      objOut.updateImage;                  
    end  
    
    function out = get.imgType(obj)
      if isa(obj.imgType,'function_handle')
        out = feval(obj.imgType);
      else
        out = obj.imgType;
      end
    end
                  
    function out = get.imgCMap(obj)
      if isa(obj.imgCMap,'function_handle')
        out = feval(obj.imgCMap);
      else
        out = obj.imgCMap;
      end
    end
    
%     function out = get.imgMin(obj)
%       if isa(obj.imgMin,'function_handle')
%         out = feval(obj.imgMin);
%       else
%         out = obj.imgMin;
%       end;
%     end
%     
%     function out = get.imgMax(obj)
%       if isa(obj.imgMax,'function_handle')
%         out = feval(obj.imgMax);
%       else
%         out = obj.imgMax;
%       end
%     end
    
    function out = get.imgRange(obj)
      if isa(obj.imgRange,'function_handle')
        out = feval(obj.imgRange);
      else
        out = obj.imgRange;
      end;
    end            
    

    
    out = parseSlice(obj,img,type);   
    out = convertToRGB(obj,img,map,range);    
    updateSelections(obj,axis,slice);
    
  end
  
  methods (Static=true)
    out = convertToCMapIdx(imgIn,range,cMap);
  end

  
end
  