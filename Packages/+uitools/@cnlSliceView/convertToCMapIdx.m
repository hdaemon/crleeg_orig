function imgOut = convertToCMapIdx(imgIn,range,cMap)
% Convert an image into a reference into a colormap
%
% function imgOut = CONVERTTOCMAPIDX(imgIn,range,cMap)
%
% Inputs: 
%   imgIn : Input Image
%   range : Range of color map. If empty, uses [min(ImgIn(:)) max(imgIn(:))]
%   cMap  : Colormap
%
% Outputs:
%   imgOut : Image mapped into the colormap space
%
% Written By: Damon Hyde
% Last Edited: Jan 31, 2016
% Part of the cnlEEG Project
%

if ~exist('cMap','var'), cMap = jet(256); end;
if ~exist('range','var')||isempty(range),
  range = [min(imgIn(:)) max(imgIn(:))];
end;

% INTs don't work here.
imgIn = double(imgIn);
range = double(range);
imgOut = round(((imgIn-range(1))./(range(2)-range(1)))*size(cMap,1));

imgOut(imgOut<0) = 0;
imgOut(imgOut>size(cMap,1)) = size(cMap,1);

end