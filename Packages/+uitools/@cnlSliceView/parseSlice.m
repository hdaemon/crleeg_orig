function [sliceOut] = parseSlice(obj,slice,type)
% PARSESLICE Parse slice for viewing
%
% function [sliceOut] = parseSlice(obj,slice,type)
%
% Written By: Damon Hyde
% April 2015
% Part of the cnlEEG Project


if ~exist('type','var'), type = obj.imgType; end;

%mydisp('Parsing slice');
allTypes = uitools.cnlColorControl.dispTypeList;

idx = 0;
found = false;
while ~found
  idx = idx+1;
  if idx>numel(allTypes)
    error('cnlColorControl:unknownType',...
      'Unknown display type');
  end
  found = strcmpi(type,allTypes(idx).name);
end;

slice = feval(allTypes(idx).fPre,slice);
if ~ismatrix(slice)
  slice = squeeze(sum(slice,1));
end
slice = feval(allTypes(idx).fPost,slice);

slice = squeeze(slice);

% Orient image appropriately (this needs some fixing)
sliceOut = orientimage(slice,obj.currAxis);

end

function slice = orientimage(slice,axis)
% ORIENTIMAGE Given a slice image, appropriately orient it
%
% This assumes that the 3D image is oriented as left-posterior-superior
%
%

slice = permute(slice,[2 1]);

%Sag and Cor images need to have the first dimension flipped
if ismember(axis,[1 2]);
  if isempty(which('flip'))
    % Compatibility with older matlab versions
    slice = flipdim(slice,1);
  else
    slice = flip(slice,1);
  end;
end;

end