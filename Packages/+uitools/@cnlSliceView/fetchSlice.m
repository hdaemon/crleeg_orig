function out = fetchSlice(obj)
% function out = fetchSlice(obj)
%
% Execute the function handle in obj.fhnd_getSlice.  If this isn't a
% function handle, return zero.
%
% Written By: Damon Hyde
% Last Edited: Aug 14, 2015
% Part of the cnlEEG project
%
if isa(obj.fhnd_getSlice,'function_handle')
  out = obj.fhnd_getSlice(obj.currAxis,obj.currSlice);
else
  out = 0;
end;
end