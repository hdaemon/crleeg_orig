classdef module < handle & matlab.mixin.Copyable
  
  properties
    input
  end
  
  methods
    
    function moduleObj = module(varargin)
      
      if isa(varargin{1},'module')
        moduleObj.input = varargin{1}.input;
      end
      
      
      moduleObj.input    = pipelineInput([],'types','file_NRRD');
      moduleObj.input(2) = pipelineInput([],'types','double');
      
      
      
    end
    
    function set.input(obj,val)
      keyboard;
      if isa(val,'pipelineInput')
        obj.input = val;
      else
        obj.input.input = val;
      end;
    end
    
  end
  
end

    