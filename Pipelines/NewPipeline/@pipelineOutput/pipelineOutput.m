classdef pipelineOutput < handle & matlab.mixin.Copyable
  
  properties
    type
    recomputeFunction
    
  end
  
  properties (Hidden = true)
    savesPrecomputedOutput
  end
  
  properties (Dependent = true)
    value
  end
  
  properties (Access=protected)
    precomputedOutput;
    needsRecompute;
  end
  
  events
    outputUpdated
  end
  
  methods
    
    function outputObj = pipelineOutput(varargin)
    end
    
    function out = get.value(obj)
      % function out = get.output(obj)
      %
      % Compute the output of the pipeline module.
      %
      % IF obj.savePrecomputedOutput is:
      %   TRUE:
      %   IF obj.needsRecompute is:
      %     TRUE: Executes obj.Execute and stores it in
      %            obj.precomputedOutput
      %     FALSE: Outputs obj.precomputedOutput
      %   FALSE:
      %     Runs obj.Execute and returns the output
      %
      
      % If Disabled, Just Pass The Input Through
      if obj.disabled, out = obj.input; return; end;
      
      % If input is empty, return an empty array.
      if isempty(obj.inputobj), out = []; return; end;
      
      if obj.savesPrecomputedOutput
        % If precomputed output is saved, check if it needs to be
        % recomputed.
        if ~obj.needsRecompute
          % Use precomputed output
          out = obj.getPrecomputedOutput;
        else
          % Recompute if necessary
          tmp = obj.Execute;
          obj.setPrecomputedOutput(tmp);
          out = obj.getPrecomputedOutput;
          obj.needsRecompute = false;
        end;
      else
        % Otherwise, just run it.
        out = obj.Execute;
      end;      
    end
    
    %% Default Set/Get Methods for obj.precomputedOutput.
    % Using these allows child classes to overload them and modify the
    % set/get operations for the precomputed output. This functionality is
    % NOT available with Matlab dependent properties.
    function out = getPrecomputedOutput(obj)
      out = obj.precomputedOutput;
    end
    
    function setPrecomputedOutput(obj,val)
      obj.precomputedOutput = val;
    end    
    
    function flagRecomputeAndNotify(obj)
      % Flags the output for recomputation, and notifies downstream objects
      % that it has changed. Recomputation waits until the output is
      % actually requested.
      %
      obj.needsRecompute = true;
      notify(obj,'outputUpdated');
    end
    
  end
  
end
