function obj = subsasgn(obj,s,varargin)
   switch s(1).type
      case '.'
        % Dot references works normally
        obj = builtin('subsasgn',obj,s,varargin{:});         
      case '()'
         if length(s) == 1
            % Implement obj(indices) = varargin{:};
            singleNumericInput = numel(s(1).subs{1})==1&&...
                                  isnumeric(s(1).subs{1});
            notInputValue = ~isa(varargin{1},'pipelineInput')&&...
                                isnumeric(s(1).subs{1});
            if singleNumericInput&&notInputValue         
                obj(s(1).subs{1}).inputVal = varargin{1};              
            else                
             obj = builtin('subsasgn',obj,s,varargin{:});
            end;         
         else
            % Use built-in for any other expression
            obj = builtin('subsasgn',obj,s,varargin);
         end       
      case '{}'
        obj = builtin('subsasgn',obj,s,varargin);         
      otherwise
         error('Not a valid indexing expression')
   end
end