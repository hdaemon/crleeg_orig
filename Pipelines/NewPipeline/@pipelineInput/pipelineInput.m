classdef pipelineInput < handle
  % Class for handling pipeline inputs
  %
  % classdef pipelineInput < handle
  %
  % Provides input checking and monitoring of input modification for
  % pipeline objects in crlEEG.
  %
  % Properties
  %   types : List of valid input types
  %   trueInput : The input as provided to the object
  %   inputCheckFunction : Provides functionality for additional input 
  %                         checking beyond the basic class validation done
  %                         by this object type.
  %  
  % Dependent Properties:
  %   input : Input after initial preprocessing. Behavior depends on the
  %             class of obj.trueInput:
  %             cnlPipelineModule: obj.input returns obj.trueInput.output
  %             pipelineInput    : obj.input returns obj.trueInput.input
  %             function_handle  : obj.input returns feval(obj.trueInput)
  %             other            : obj.input returns obj.trueInput
  %
  % Events:
  %   inputModified : Triggered whenever obj.input is assigned a new value.
  %
  
  properties
    types
  end
  
  properties (GetAccess=public,SetAccess=protected)
    trueInput
  end
  
  properties
    inputCheckFunction
  end
  
  properties (Dependent = true, Hidden = true)
    typeString
    value
  end
  
  properties (Access=protected)
    inputListener
  end
  
  events
    inputModified
  end
  
  methods
    
    function inputObj = pipelineInput(varargin)
      
      p = inputParser;
      p.addOptional('value',[]);
      p.addParamValue('types',{},@(x) ischar(x)||iscellstr(x));
      p.addParamValue('checkfcn',[],@(x) isa(x,'function_handle'));
      p.parse(varargin{:});
            
      inputObj.types = p.Results.types;
      inputObj.value = p.Results.value;
      inputObj.inputCheckFunction = p.Results.checkfcn;
            
    end
      
    function set.types(obj,val)
      % Check input type assignment. Reformat as cell array of strings if
      % provided with a string.
      %
      assert(ischar(val)||iscellstr(val),'Invalid input type assignment');
      if iscellstr(val)
        obj.types = val;
      else
        obj.types = {val};
      end
    end
    
    function out = get.typeString(obj)
      if isempty(obj.types)
        out = []; return;
      end;
      out = obj.types{1};
      for i = 2:numel(obj.types)
        out = [out ', ' obj.types{i}];
      end;
    end
    
    
    function set.value(obj,val)
      % function set.value(obj,val)
      %
      % Ensure that the input matches one of obj.types
      
      assert(obj.isValidInputType(val),...
        ['value type (%s) does not match any value in'...
        ' obj.types: (%s)'],class(val), obj.typeString);
      if ~isequal(obj.value,val)
        % If a check function is provided, run it on the new value.
        if ~isempty(obj.inputCheckFunction)
            if obj.inputCheckFunction(val)
             obj.trueInput = val;
            else
              error('Failed input check function');
            end              
        else
          obj.trueInput = val;
        end
        
        obj.createInputListener;
        
        obj.notify('inputModified');        
      end;
    end    
      
    
    function out = get.value(obj)
      % function out = get.value(obj)
      %
      %
      
      if isa(obj.trueInput,'cnlPipelineModule')
        % If the value in another pipeline module, fetch output from that
        % module.
        out = obj.trueInput.output(1);
      elseif isa(obj.trueInput,'pipelineInput')
        out = obj.trueInput.value;
      elseif isa (obj.trueInput,'pipelineOutput')
        out = obj.trueInput.value;
      elseif isa(obj.trueInput,'function_handle')
        % If the input is a function handle, evaluate the function handle
        out = feval(obj.trueInput);
      else
        % Otherwise, use the provided input value
        out = obj.trueInput;
      end;
    end
    
  end
  
  methods (Access=protected)
    
     function createInputListener(obj,useEvent)
      % For specific input types, create a listener to watch for
      % updates to the input.
      obj.inputListener = [];
      
      if ~exist('useEvent','var'), useEvent = []; end;
      
      if isempty(useEvent)
        if ismember('outputUpdated',events(obj.trueInput))
          % The input object is a cnlPipelineModule
          useEvent = 'outputUpdated';
        elseif ismember('updatedOut',events(obj.trueInput));
          useEvent = 'updatedOut';
        end
      end;
      
      if ~isempty(useEvent)
        obj.inputListener = addlistener(obj.trueInput,useEvent,...
          @(src,evt) recomputeAndNotify(obj,src,evt));
      end
    end
    
    function isValid = isValidInputType(obj,val)
      % function isValid = isValidInputType(obj,val)
      %
      % Check that input value matches one of obj.inputTypes
      
      % Empty inputs are valid
      if isempty(val), isValid = true; return; end;
      
      % If inputTypes haven't been specified, accept only empty inputs
      if isempty(obj.types), isValid = isempty(val); return; end;
      
      if isa(val,'cnlPipelineModule')
        % If the input comes from a cnlPipelineModule, check that the
        % output type of that module matches the required input type
        isValid = any(cellfun(@(x) isequal(val.outputType,x),obj.types));
      elseif isa(val,'function_handle')
        % If it's a function handle, check that the output of evaluating it
        % is either a valid inputType, or empty (which we assume to mean
        % the function isn't fully configured yet).
        isValid = any(cellfun(@(x) isequal(feval(val),x),obj.types));
        isValid = isValid||isempty(feval(val));
      else
        % If the input is not from another pipeline module, check that it
        % matches one of the provided inputTypes
        isValid = any(cellfun(@(x) isa(val,x),obj.types));
      end;
    end
  end
  
end
