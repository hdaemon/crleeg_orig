%%
% This is just a basic script showing some of the ways that the new
% pipeline objects can be used. 
%
% Two primary ways are shown:
%
clear all;

% Read In Some Data and Electrode Labels
dir_EEG = '/common/projects2/epilepsysurgicalplanning/processed/Case174/20130624_PreOp/sourceanalysis/eeg/clinical/fromJur';
Dat(1) = file_DAT('C7_interictal_epoched_ASCII.dat',dir_EEG);
electrodeLabels = readFromDAP('C7_ictal_epoched_ASCII.rs3',dir_EEG);

% Get the Data into a cnlLabelledData Object
%
% There should probably be an additional object, perhaps a rewrite of the
% cnlEEGData object, that enables the 
dataIn = cnlLabelledData(Dat(1).data,electrodeLabels);

% Keep only a few of the electrodes. This directly calls the .output method
% of the mods_EEG.elec.retain module, to immediately run the computations,
% and keep only the final output. The module object is destroyed after
% calling.... ok, so it looks like Matlab doesn't actually like it when you
% do that, and you actually have to complete the call to the constructor,
% get the output and reassign it.
%
retainElectrodes = {'Fp1' 'F7' 'T3' 'T5' 'O1' 'F3' 'C3' 'P3' 'TP9' ...
                    'Fz' 'Cz' 'Fp2' 'F8' 'T4' 'T6' 'O2' 'F4' 'C4' 'P4' 'TP10' 'Pz' 'FT9' 'FT10'}; 
dataIn = cnlPLMod.EEG.elec.retain.staticFun(dataIn,retainElectrodes);

% Split the data evenly into epochs of length 1025
foo{1} = cnlPLMod.EEG.epochs.splitIntoEvenly(dataIn,1025);

% As an alternate way of using the modules, you can import the entire EEG
% subpackage of cnlPLMod.  Unfortunately, it does not appear that it is
% possible to import things in such a way that you could call EEG.elec,
% because you cannot do "import cnlPLMod.EEG"
%
import cnlPLMod.EEG.*

foo{4} = epochs.cropToInterval(foo{1},[200 700]);
foo{2} = elec.remove(foo{4},{'Fpz'});
foo{3} = elec.rereference(foo{2},'refType','reref','refElec','Cz');
foo{5} = epochs.average(foo{3});

dataDESI = foo{3}.output; dataDESI.description = 'Data for use with DESI';
dataAvg  = foo{5}.output; dataAvg.description = 'Averaged Spike Data';

clear import