classdef retain < cnlPipelineModule
  % Provide a list of electrodes to retain from a cnlLabelledData object
  %
  % This is the companion method to removeElec
  %
  % obj = cnlPLMod.EEG.elec.retain(input,varargin)
  %
  % Inputs:
  %   input: cnlLabelledData Object to Operate On (optional at
  %             construction)
  %
  % Options:
  %   'toRetain' : Cell array of electrodes names to retain
  %  
  % Output:
  %   cnlLabelled Data Object Containing only the requested electrodes.
  %
  %
  % Written By: Damon Hyde
  % Last Edited: June 17, 2016
  % Part of the cnlEEG Project
  %
  
  properties
    % Put module specific properties and options here
    toRetain = {};
  end
  
  methods
    
    function obj = retain(input,varargin)
      
      % Set up the basic object
      obj = obj@cnlPipelineModule;
      obj.inputTypes = {'cnlLabelledData'};% Input types as a cell array here
      obj.outputType = 'cnlLabelledData';% Output type here
      obj.savesPrecomputedOutput = false;% Configure saved output here
      
      if nargin>0
      
        p = inputParser;
        p.addOptional('input',[],@(x) obj.isValidInputType(x));
        p.addOptional('toRetain',[],@(x) isempty(x)||iscellstr(x));
        parse(p,input,varargin{:});
        
      % Set the input
      obj.input = p.Results.input;
      obj.toRetain = p.Results.toRetain;
      
      end;
    end
    
    function out = Execute(obj)
      % Code to run the module goes here
      input = obj.input;
      if ~isempty(input)
        outIdx = ismember(input.labels,obj.toRetain);                
        outLabels = input.labels(outIdx);
        out = input(outLabels);
      else
        out = [];
      end;      
    end;

   function guiObj = makeGUI(obj)      
        guiObj = uipanel(...          
          'Title','EEG.elec.retain',...
          'Position',[10 10 400 50]);      
    end    
    
  end
  
  %% Protected Methods
  methods (Access=protected)
    function out = getPrecomputedOutput(obj)
      % This is an optional method that reimplements what's already the
      % default in cnlPipelineModule
      out = obj.Execute;
    end
    
     function groups = getPropertyGroups(obj)
      % Overload for matlab.mixin.CustomDisplay
      
      if ~isscalar(obj)
        groups = getPropertyGroups@matlab.mixin.CustomDisplay(obj)
      else
      list = struct;
      list.input = obj.input;
      %list.inputTypes = obj.inputTypes;
      list.toRetain = obj.toRetain;
      %list.outputType = obj.outputType;
      try
        list.output = obj.output;
      catch        
        list.output = 'Output Computation Failed';
      end;
      
      groups = matlab.mixin.util.PropertyGroup(list);
      end;      
     end   
    
  end
  
  %% Static Methods
  methods (Static=true)
    function out = staticFun(varargin)
      % Call cnlPLMod.EEC.elec.retain as a static function.
      tmp = cnlPLMod.EEG.elec.retain(varargin{:});
      out = tmp.output;
    end
  end
  
  %% Static Protected Methods
  methods (Access=protected,Static=true)
    function out = checkInput(val)
      % Method to check and validate the input
      out = val;
    end        
  end
end
   
      
      