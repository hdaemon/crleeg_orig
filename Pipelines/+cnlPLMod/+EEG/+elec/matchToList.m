classdef matchToList < cnlPipelineModule
  % Reorder cnlLabelledData to match provided electrode list
  %
  % classdef matchToList < cnlPipelineModule
  %
  % obj = matchToList(input,varargin)
  %
  % Inputs:
  %  input : cnlLabelledData object
  %
  % Optional Inputs:
  %  toMatch : Cell array of string with electrode labels
  %
  % Output:
  %  output : cnlLabelledData object
  %  idxInputToOutput:
  %  idxMatchToOutput
  %
  % Written By: Damon Hyde
  % Part of the cnlEEG Project
  % 2016
  %
  
  properties
    % Put module specific properties and options here
    toMatch;
  end
  
  properties (Dependent=true,Hidden);
    idxInputToOutput
    idxMatchToOutput
  end
  
  methods
    
    function obj = matchToList(input,varargin)
      
      % Set up the basic object
      obj = obj@cnlPipelineModule;
      obj.inputTypes = {'cnlLabelledData'};% Input types as a cell array here
      obj.outputType = 'cnlLabelledData';% Output type here
      obj.savesPrecomputedOutput = false;% Configure saved output here
      
      if nargin>0
       p = inputParser;
       p.addOptional('input',[],@(x) obj.isValidInputType(x));
       p.addOptional('toMatch',[],@(x) iscellstr(x));
       p.parse(input,varargin{:});
                      
      % Set the input
      obj.input = input;
      obj.toMatch = p.Results.toMatch;
      end;
    end
    
    function out = Execute(obj)      
      in = obj.input;
      out = [];
      
      if ~isempty(in)
        if isempty(obj.toMatch), out = in; return; end;
        
        outLabels = intersect(obj.toMatch,in.labels,'stable');
        
        out = in(outLabels);
      end            
    end;
    
    function set.toMatch(obj,val)
      if ~isequal(obj.toMatch,val)
        obj.toMatch = val;
        notify(obj,'outputUpdated');
      end;
    end
    
     function out = get.idxInputToOutput(obj)
       error('Hasn''t been updated yet');
      for idx = 1:numel(obj.Output)
        tmp = find(strcmp(obj.Output{idx},obj.Input));
        assert(numel(tmp)==1,'ERROR - Multiple Matches in List');
        out(idx) = tmp;
      end
    end;
    
    function out = get.idxMatchToOutput(obj)
      error('Hasn''t been updated yet');
      if ~isempty(obj.Match)
        for idx = 1:numel(obj.Output)
          tmp = find(strcmp(obj.Output{idx},obj.Match));
          assert(numel(tmp)==1,'ERROR - Multiple Matches in List');
          out(idx) = tmp;
        end;
      else
        out = 1:numel(obj.Output);
      end;
    end
    
   function guiObj = makeGUI(obj)      
        guiObj = uipanel(...          
          'Title','EEG.elec.matchToList',...
          'Units','pixels',...
          'Position',[10 10 400 50]);      
    end    
    
  end
  
  %% Protected Methods
  methods (Access=protected)
    function out = getPrecomputedOutput(obj)
      % This is an optional method that reimplements what's already the
      % default in cnlPipelineModule
      out = obj.Execute;
    end
  end
  
  %% Static Protected Methods
  methods (Access=protected,Static=true)
    function out = checkInput(val)
      % Method to check and validate the input
      out = val;
    end
  end
end
   
      
      