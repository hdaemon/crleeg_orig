classdef remove < cnlPipelineModule
  % Remove a list of electrodes from a cnlLabelledData object
  %
  % Param-Values:
  %   'toRemove' : Cell array of strings with electrode labels to remove
  %
  % Written By: Damon Hyde
  % Last Edited: May 24, 2016
  % Part of the cnlEEG Project
  %
  
  properties
    % Put module specific properties and options here
    toRemove = {};
  end
  
  methods
    
    function obj = remove(input,varargin)
      
      % Set up the basic object
      obj = obj@cnlPipelineModule;
      obj.inputTypes = {'cnlLabelledData'};% Input types as a cell array here
      obj.outputType = 'cnlLabelledData';% Output type here
      obj.savesPrecomputedOutput = false;% Configure saved output here
      
      if nargin>0
        p = inputParser;
        p.addOptional('input',[],@(x) obj.isValidInputType(x));
        p.addOptional('toRemove',[],@(x) iscellstr(x));
        parse(p,input,varargin{:});
        
        % Set the input
        obj.input = p.Results.input;
        obj.toRemove = p.Results.toRemove;
        
      end;
    end
    
    function set.toRemove(obj,val)
      if isempty(val), obj.toRemove = cell(0,1); return; end;
      assert(iscellstr(val),'Electrode list must be a cell array of strings');
      if ~isequal(sort(obj.toRemove),sort(val))
        % Remove considers electrode lists to be identical if they contain
        % the same elements. Order does not matter.
        obj.toRemove = val;
        notify(obj,'outputUpdated');
      end;
    end
    
    function out = Execute(obj)
      % Code to run the module goes here
      input = obj.input;
      if ~isempty(input)
        if isempty(obj.toRemove), out = input; return; end;
        idx = ~ismember(input.labels,obj.toRemove);
        outLabels = input.labels(idx);
        out = input(outLabels);
      else
        out = [];
      end;
    end;

   function guiObj = makeGUI(obj)      
        guiObj = uipanel(...          
          'Title','EEG.elec.remove',...
          'Position',[10 10 400 50]);      
    end    
    
  end
  
  %% Static Methods
  methods (Static=true)
    function out = staticFun(input,varargin)
      tmp = mods_EEG.removeElec(input,varargin{:});
      out = tmp.output;
    end
  end;
  
  %% Protected Methods
  methods (Access=protected)
    function out = getPrecomputedOutput(obj)
      % This is an optional method that reimplements what's already the
      % default in cnlPipelineModule
      out = obj.Execute;
    end
    
%     function groups = getPropertyGroups(obj)
%       % Overload for matlab.mixin.CustomDisplay
%       
%       if ~isscalar(obj)
%         groups = getPropertyGroups@matlab.mixin.CustomDisplay(obj)
%       else
%         list = struct;
%         list.input = obj.input;
%         %list.inputTypes = obj.inputTypes;
%         list.toRemove = obj.toRemove;
%         %list.outputType = obj.outputType;
%         try
%           list.output = obj.output;
%         catch
%           list.output = 'Output Computation Failed';
%         end;
%         
%         groups = matlab.mixin.util.PropertyGroup(list);
%       end;
%     end
    
  end
  
  %% Static Protected Methods
  methods (Access=protected,Static=true)
    function out = checkInput(val)
      % Method to check and validate the input
      out = val;
    end
        
  end
end


