classdef rerefandelim < cnlPipelineModule
  % Rereference and remove the reference electrode from the data
  %
  % Written By: Damon Hyde
  % Last Edited: May 2, 2016
  % Part of the cnlEEG Project
  %
  
  properties (Dependent = true)
    % Put module specific properties and options here
    refElec
  end
  
  properties (SetAccess=protected)
    mod_reref;
    mod_elim;
  end;
  
  methods
    
    function obj = rerefandelim(input,varargin)
      
      % Set up the basic object
      obj = obj@cnlPipelineModule;
      obj.inputTypes = {'cnlLabelledData'}; % Input types as a cell array here
      obj.outputType = 'cnlLabelledData';% Output type here
      obj.savesPrecomputedOutput = false; % Configure saved output here
      
      % Connect Input to First interior module;
      obj.mod_reref = cnlPLMod.EEG.rereference;
      obj.mod_reref.input = @() obj.input;
      obj.mod_reref.refType = 'reref';
      
      % Connect input of 2nd module with output of first
      obj.mod_elim = cnlPLMod.EEG.removeElec;
      obj.mod_elim.input = obj.mod_reref;
                        
      if nargin>0
      
      % Set the input
      obj.input = input;
                  
      end;
    end
    
    function out = Execute(obj)
      % Code to run the module goes here
      out = obj.mod_elim.output;
    end;
    
    function out = get.refElec(obj)
      out = obj.mod_reref.refElec;
    end
    
    function set.refElec(obj,val)
      obj.mod_reref.refElec = val;
      obj.mod_elim.toRemove = val;
    end;
    
   function guiObj = makeGUI(obj)      
        guiObj = uipanel(...          
          'Title','EEG.elec.rerefandelim',...
          'Position',[10 10 400 50]);      
    end    
    
    
  end
  
  %% Protected Methods
  methods (Access=protected)
    function out = getPrecomputedOutput(obj)
      % This is an optional method that reimplements what's already the
      % default in cnlPipelineModule
      out = obj.Execute;
    end
    
    function groups = getPropertyGroups(obj)
      % Overload for matlab.mixin.CustomDisplay
      
      if ~isscalar(obj)
        groups = getPropertyGroups@matlab.mixin.CustomDisplay(obj)
      else
      list = struct;
      list.input = obj.input;      
      list.refElec = obj.refElec;
      
      try
        list.output = obj.output;
      catch        
        list.output = 'Output Computation Failed';
      end;
      
      groups = matlab.mixin.util.PropertyGroup(list);
      end;      
     end 
    
  end
  
  %% Static Protected Methods
  methods (Access=protected,Static=true)
    function out = checkInput(val)
      % Method to check and validate the input
      out = val;
    end
    
  end
end
   
      
      