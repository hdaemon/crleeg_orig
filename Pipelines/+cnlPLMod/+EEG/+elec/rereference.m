classdef rereference < cnlPipelineModule
  % Apply a new referencing scheme to a cnlLabelledData object
  %
  % classdef rereference < cnlPipelineModule
  %
  % obj = rereference(input,varargin);
  %
  % Inputs:
  %   input : cnlLabelledData object to rereference
  %
  % Optional Input Parameters
  %    'refType' :  DEFAULT: 'recorded'
  %    'refElec' :  DEFAULT: 'Cz'
  % 
  % Written By: Damon Hyde
  % Last Edited: May 2, 2016
  % Part of the cnlEEG Project
 
  properties
    % Put module specific properties and options here
    refType;
    refElec;
  end
  
  methods
    
    function obj = rereference(input,varargin)
      
      % Set up the basic object
      obj = obj@cnlPipelineModule;
      obj.inputTypes = {'cnlLabelledData'};% Input types as a cell array here
      obj.outputType = 'cnlLabelledData';% Output type here
      obj.savesPrecomputedOutput = false;% Configure saved output here
      
      % Parse and assign the variable input arguments.
      p = obj.parseInputs(varargin{:});
      obj.refType = p.Results.refType;
      obj.refElec = p.Results.refElec;
      
      if nargin>0
        % Set the input
        obj.input = input;
      end;
    end
    
    function set.refType(obj,val)
      % If the reference type changes, notify those listening.
      if ~isequal(obj.refType,val)
      obj.refType = val;
      notify(obj,'outputUpdated');
      end;
    end;
    
    function set.refElec(obj,val)
      % If the reference electrode changes, notify those listening
      if ~isequal(obj.refElec,val)
        obj.refElec = val;
        notify(obj,'outputUpdated');
      end;
    end
    
    
    function out = Execute(obj)
      % Code to run the module goes here
      
      in = obj.input;
      
      if ( ~isempty(in) )
        switch (obj.refType)
          case 'recorded'
            % Just output the input data
            out = in;
          case 'avgref'
            % Apply the Average Reference.
            RefSig = in.data;
            RefSig = mean(RefSig,1);
            Data = in.data;
            Data = Data - repmat(RefSig,size(Data,1),1);
            out = cnlLabelledData(Data,in.labels);
            out = out.setEpochs('start',in.epochs_start,...
                          'finish',in.epochs_end);
            out.description = [in.description '_rerefavg'];         

        case 'reref'
            % Rereference to a Specific Electrode
            
            if isempty(obj.refElec)
              warning('Reference Electrode Not Set');
              out = []; return;
            end;
            
            if ismember(obj.refElec,in.labels)
              RefSig = in(obj.refElec).data;                            
              tmp = in.data;
              if size(RefSig,1)==1
                RefSig = repmat(RefSig,size(tmp,1),1);
              end;
              
              Data = tmp - RefSig;
              out = cnlLabelledData(Data,in.labels);
              out = out.setEpochs('start',in.epochs_start,...
                            'finish',in.epochs_end);
              out.description = [in.description '_reref' obj.refElec];
            else
              warning('Reference Electrode Node Missing From Input Set');
              out = [];
            end;
          otherwise
            error('Unknown referencing type');
        end
      else
        warning('Empty Input');
        out = [];
      end
    end;

    
   function guiObj = makeGUI(obj)      
        guiObj = uipanel(...          
          'Title','EEG.elec.rereference',...
          'Position',[10 10 400 50]);      
    end    
    
  end
  
  
  %% Static Methods
  %   methods (Static=true)
  % %       function out = staticFun(input,varargin)
  % %       p = mods_EEG.rereference.parseInputs(varargin{:});
  % %
  % %       tmp = mods_EEG.rereference;
  % %       tmp.refType = p.Results.refType;
  % %       tmp.refElec = p.Results.refElec;
  % %       tmp.input = input;
  % %       out = tmp.output;
  % %
  % %       end
  % end
  
   
  %% Static Protected Methods
  methods (Access=protected,Static=true)
    function out = checkInput(val)
      % Method to check and validate the input
      out = val;
    end
    
    function p = parseInputs(varargin)
      p = inputParser;      
      p.addParamValue('refType','recorded');
      p.addParamValue('refElec','Cz');
      p.parse(varargin{:});
    end
    
    
  end
end


