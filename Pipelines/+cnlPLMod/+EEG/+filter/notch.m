classdef notch < cnlPLMod.EEG.filter.generalized
  % A cnlPLMod.EEG.filter.generalized implementation of a notch filter
  %
  % Default Values:
  %   FilterOrder : 2
  %   HalfPowerFrequency1 : 58
  %   HalfPowerFrequency2 : 62
  %   DesignMethod = 'butter'
  %   SampleRate = 1
  %
  % Written By: Damon Hyde
  % Part of the cnlEEG Project
  % 2016
  %
  
  properties
    % Put module specific properties and options here
    FilterOrder = 2;
    HalfPowerFrequency1 = 58;
    HalfPowerFrequency2 = 62;
    DesignMethod = 'butter';
    SampleRate = 1
  end
  
  properties (Access=private)
    rebuildFilter = true;
  end;
  
  methods
    
    function obj = notch(input,varargin)
      
      % Set up the basic object
      obj = obj@cnlPLMod.EEG.filter.generalized;
      obj.inputTypes = {'cnlLabelledData'}; % Input types as a cell array here
      obj.outputType = 'cnlLabelledData';   % Output type here      
      obj.savesPrecomputedOutput = false;% Configure saved output here
      
      if nargin>0
       %% Input Parsing
       p = inputParser;
       p.addOptional('input',[],@(x) obj.isValidInputType(x));
       p.addParamValue('FilterOrder',2,@(x) isnumeric(x)&isscalar(x));
       p.addParamValue('HalfPowerFrequency1',[],@(x) isnumeric(x)&&isscalar(x));
       p.addParamValue('HalfPowerFrequency2',[],@(x) isnumeric(x)&&isscalar(x));
       p.addParamValue('DesignMethod','butter',@(x) ischar(x));
       p.addParamValue('SampleRate',[]);
       % Other input parameters here
       p.parse(input,varargin{:});
       
       %% Set Parameters
       obj.input = input;
       obj.FilterOrder         = p.Results.FilterOrder;
       obj.HalfPowerFrequency1 = p.Results.HalfPowerFrequency1;
       obj.HalfPowerFrequency2 = p.Results.HalfPowerFrequency2;
       obj.DesignMethod        = p.Results.DesignMethod;
       obj.SampleRate          = p.Results.SampleRate;
       
      end;
    end
    
    function out = Execute(obj)
      % Code to run the module goes here
      if obj.rebuildFilter, obj.buildFilter; end;
      out = obj.Execute@cnlPLMod.EEG.filter.generalized;
    end;
    
    function set.FilterOrder(obj,val)
      assert(isempty(val)||(isnumeric(val)&&isscalar(val)),...
        'obj.FilterOrder must be a scalar numeric value');
      obj.FilterOrder = val;
      if ~isequal(obj.FilterOrder,val), obj.setRebuildFlag; end;      
    end
    
    function set.HalfPowerFrequency1(obj,val)
      assert(isempty(val)||(isnumeric(val)&&isscalar(val)),...
        'obj.HalfPowerFrequency1 must be a scalar numeric value');
      obj.HalfPowerFrequency1 = val;
      if ~isequal(obj.HalfPowerFrequency1,val), obj.setRebuildFlag; end;      
    end
    
    function set.HalfPowerFrequency2(obj,val)
      assert(isempty(val)||(isnumeric(val)&&isscalar(val)),...
        'obj.HalfPowerFrequency2 must be a scalar numeric value');
      obj.HalfPowerFrequency2 = val;
      if ~isequal(obj.HalfPowerFrequency2,val), obj.setRebuildFlag; end;      
    end
    
    function set.DesignMethod(obj,val)
      assert(ischar(val),'obj.DesignMethod must be a character string');
      obj.DesignMethod = val;
      if ~isequal(obj.DesignMethod,val), obj.setRebuildFlag; end;      
    end
    
    function set.SampleRate(obj,val)
      assert(isempty(val)||(isnumeric(val)&&isscalar(val)),...
        'obj.SampleRate must be a scalar numeric value');
      obj.SampleRate = val;
      if ~isequal(obj.SampleRate,val), obj.setRebuildFlag; end;      
    end
    
    function guiObj = makeGUI(obj)
        
        guiObj = uipanel(...
          'Units','pixels',...
          'Title','EEG.filter.notch',...
          'Position',[10 10 400 75]);
                
        orderText = uicontrol(...
          'Style','text',...
          'Parent',guiObj,...
          'Units','pixels',...
          'String','Filter Order:',...
          'HorizontalAlignment','left',...
          'Position',[10 35 80 20]);
        set(orderText,'Units','normalized');
        setappdata(guiObj,'orderText',orderText);
                
        orderUI = uicontrol(...
          'Style','edit',...
          'Parent',guiObj,...
          'Units','pixels',...
          'Position',[100 35 30 25],...         
          'Callback',@(h,evt) obj.setOrderFromGUI);
        set(orderUI,'Units','normalized');
        setappdata(guiObj,'orderUI',orderUI);

        sampleRateText = uicontrol(...
          'Style','text',...
          'Parent',guiObj,...
          'Units','pixels',...
          'String','Sample Rate:',...
          'HorizontalAlignment','left',...
          'Position',[10 5 90 20]);
        set(sampleRateText,'Units','normalized');
        setappdata(guiObj,'sampleRateText',sampleRateText);
                
        sampleRateUI = uicontrol(...
          'Style','edit',...
          'Parent',guiObj,...
          'Units','pixels',...
          'Position',[100 5 60 25],...         
          'Callback',@(h,evt) obj.setSampleRateFromGUI);
        set(sampleRateUI,'Units','normalized');
        setappdata(guiObj,'sampleRateUI',sampleRateUI);           
        
        F1Text = uicontrol(...
          'Style','text',...
          'Parent',guiObj,...
          'Units','pixels',...
          'String','F_Half 1:',...
          'HorizontalAlignment','left',...
          'Position',[170 35 60 20]);
        set(F1Text,'Units','normalized');
        setappdata(guiObj,'F1Text',F1Text);
                
        F1UI = uicontrol(...
          'Style','edit',...
          'Parent',guiObj,...
          'Units','pixels',...
          'Position',[230 35 30 25],...         
          'Callback',@(h,evt) obj.setF1FromGUI);
        set(F1UI,'Units','normalized');
        setappdata(guiObj,'F1UI',F1UI);        
        
        F2Text = uicontrol(...
          'Style','text',...
          'Parent',guiObj,...
          'Units','pixels',...
          'HorizontalAlignment','left',...
          'String','F_Half 2:',...
          'Position',[270 35 60 20]);
        set(F2Text,'Units','normalized');
        setappdata(guiObj,'F2Text',F2Text);
                
        F2UI = uicontrol(...
          'Style','edit',...
          'Parent',guiObj,...
          'Units','pixels',...
          'Position',[330 35 30 25],...         
          'Callback',@(h,evt) obj.setF2FromGUI);
        set(F2UI,'Units','normalized');
        setappdata(guiObj,'F2UI',F2UI);   
        
        DesignText = uicontrol(...
          'Style','text',...
          'Parent',guiObj,...
          'Units','pixels',...
          'HorizontalAlignment','left',...
          'String','Design Type:',...
          'Position',[170 5 100 20]);
        set(DesignText,'Units','normalized');
        setappdata(guiObj,'DesignText',DesignText);                              
    end
            
  end
  
  %% Protected Methods
  methods (Access=protected)

    function syncGUI(obj)
      if obj.validGUI
        set(getappdata(obj.gui,'orderUI'),'String',num2str(obj.FilterOrder));
        set(getappdata(obj.gui,'sampleRateUI'),'String',num2str(obj.SampleRate));
        set(getappdata(obj.gui,'F1UI'),'String',num2str(obj.HalfPowerFrequency1));
        set(getappdata(obj.gui,'F2UI'),'String',num2str(obj.HalfPowerFrequency2));    
      end
    end
    
    function setSampleRateFromGUI(obj)
      obj.SampleRate = str2double(...
        get(getappdata(obj.gui,'sampleRateUI'),'String'));
    end
    
    function setF1FromGUI(obj)
      obj.HalfPowerFrequency1 = str2double(...
        get(getappdata(obj.gui,'F1UI'),'String'));
    end
    
    function setF2FromGUI(obj)
      obj.HalfPowerFrequency2 = str2double(...
        get(getappdata(obj.gui,'F2UI'),'String'));      
    end
    
    function setOrderFromGUI(obj)
      obj.FilterOrder = str2double(...
        get(getappdata(obj.gui,'orderUI'),'String'));
    end
                
    function buildFilter(obj)
      assert(~isempty(obj.HalfPowerFrequency1),...
        'obj.HalfPowerFrequency1 must be set to build the filter');
      assert(~isempty(obj.HalfPowerFrequency2),...
        'obj.HalfPowerFrequency2 must be set to build the filter');
      assert(~isempty(obj.SampleRate),...
        'obj.SampleRate must be set to build the filter');      
      
      obj.filter_pvt = designfilt('bandstopiir','FilterOrder',obj.FilterOrder,...
        'HalfPowerFrequency1',obj.HalfPowerFrequency1,...
        'HalfPowerFrequency2',obj.HalfPowerFrequency2,...
        'DesignMethod',obj.DesignMethod,'SampleRate',obj.SampleRate);
      obj.rebuildFilter = false;
    end
    
    function setRebuildFlag(obj)
      obj.rebuildFilter = true;
      notify(obj,'outputUpdated');
      obj.syncGUI;
    end;
    
    function setfilter(obj,val)
      error('cnlPLMod.EEG.filter.notch does not allow direct setting of the filter');
    end
    
  end
  
  %% Static Protected Methods
  methods (Access=protected,Static=true)
    function out = checkInput(val)
      % Method to check and validate the input
      out = val;
    end
  end
end
   
      
      