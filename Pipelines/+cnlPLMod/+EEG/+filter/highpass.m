classdef highpass < cnlPLMod.EEG.filter.generalized
  % A cnlPLMod.EEG.filter.generalized implementation of a highpass filter
  %
  % The combination of Filter Order, Passband Frequency, and Passband
  % Ripple causes designfilt to construct a Chebyshev filter.
  %
  % Written By: Damon Hyde
  % Part of the cnlEEG Project
  % 2016
  %
  
  properties
    % Put module specific properties and options here
    FilterOrder = 2;
    PassbandFrequency;
    PassbandRipple = 0.1;    
    SampleRate    
  end
  
  properties (Access=private)
    rebuildFilter = true;
  end;
  
  methods
    
    function obj = highpass(input,varargin)
      
      % Set up the basic object
      obj = obj@cnlPLMod.EEG.filter.generalized;
      obj.inputTypes = {'cnlLabelledData'}; % Input types as a cell array here
      obj.outputType = 'cnlLabelledData';   % Output type here      
      obj.savesPrecomputedOutput = false;% Configure saved output here
      
      if nargin>0
       %% Input Parsing
       p = inputParser;
       p.addOptional('input',[],@(x) obj.isValidInputType(x));
       p.addParamValue('FilterOrder',2,@(x) isnumeric(x)&isscalar(x));
       p.addParamValue('PassbandFrequency',[],@(x) isnumeric(x)&&isscalar(x));
       p.addParamValue('PassbandRipple',0.1,@(x) isnumeric(x)&&isscalar(x));       
       p.addParamValue('SampleRate',[]);
       % Other input parameters here
       p.parse(input,varargin{:});
       
       %% Set Parameters
       obj.input = input;
       obj.FilterOrder         = p.Results.FilterOrder;
       obj.PassbandFrequency = p.Results.PassbandFrequency;
       obj.PassbandRipple = p.Results.PassbandRipple;       
       obj.SampleRate          = p.Results.SampleRate;
       
      end;
    end
    
    function out = Execute(obj)
      % Code to run the module goes here
      if obj.rebuildFilter, obj.buildFilter; end;
      out = obj.Execute@cnlPLMod.EEG.filter.generalized;
    end;
    
    function set.FilterOrder(obj,val)
      assert(isempty(val)||(isnumeric(val)&&isscalar(val)),...
        'obj.FilterOrder must be a scalar numeric value');      
      if ~isequal(obj.FilterOrder,val), obj.setRebuildFlag; end;      
      obj.FilterOrder = val;
    end
    
    function set.PassbandFrequency(obj,val)
      assert(isempty(val)||(isnumeric(val)&&isscalar(val)),...
        'obj.PassbandFrequency must be a scalar numeric value');      
      if ~isequal(obj.PassbandFrequency,val), obj.setRebuildFlag; end;      
      obj.PassbandFrequency = val;
    end
    
    function set.PassbandRipple(obj,val)
      assert(isempty(val)||(isnumeric(val)&&isscalar(val)),...
        'obj.PassbandRipple must be a scalar numeric value');      
      if ~isequal(obj.PassbandRipple,val), obj.setRebuildFlag; end;      
      obj.PassbandRipple = val;
    end   
    
    function set.SampleRate(obj,val)
      assert(isempty(val)||(isnumeric(val)&&isscalar(val)),...
        'obj.SampleRate must be a scalar numeric value');
      
      if ~isequal(obj.SampleRate,val), obj.setRebuildFlag; end;      
      obj.SampleRate = val;
    end
    
    function guiObj = makeGUI(obj)       
        guiObj = uipanel(...
          'Units','pixels',...
          'Title','EEG.filter.highpass (Chebyshev)',...
          'Position',[10 10 400 75]);                
        
        orderText = uicontrol(...
          'Style','text',...
          'Parent',guiObj,...
          'Units','pixels',...
          'String','Filter Order:',...
          'HorizontalAlignment','left',...
          'Position',[10 35 80 20]);
        set(orderText,'Units','normalized');
        setappdata(guiObj,'orderText',orderText);
                
        orderUI = uicontrol(...
          'Style','edit',...
          'Parent',guiObj,...
          'Units','pixels',...
          'Position',[100 35 30 25],...         
          'Callback',@(h,evt) obj.setOrderFromGUI);
        set(orderUI,'Units','normalized');
        setappdata(guiObj,'orderUI',orderUI);

        sampleRateText = uicontrol(...
          'Style','text',...
          'Parent',guiObj,...
          'Units','pixels',...
          'String','Sample Rate:',...
          'HorizontalAlignment','left',...
          'Position',[10 5 90 20]);
        set(sampleRateText,'Units','normalized');
        setappdata(guiObj,'sampleRateText',sampleRateText);
                
        sampleRateUI = uicontrol(...
          'Style','edit',...
          'Parent',guiObj,...
          'Units','pixels',...
          'Position',[100 5 60 25],...         
          'Callback',@(h,evt) obj.setSampleRateFromGUI);
        set(sampleRateUI,'Units','normalized');
        setappdata(guiObj,'sampleRateUI',sampleRateUI);           
        
        PBandText = uicontrol(...
          'Style','text',...
          'Parent',guiObj,...
          'Units','pixels',...
          'String','Passband Frequency:',...
          'HorizontalAlignment','left',...
          'Position',[170 35 150 20]);
        set(PBandText,'Units','normalized');
        setappdata(guiObj,'PBandText',PBandText);
                
        PBandUI = uicontrol(...
          'Style','edit',...
          'Parent',guiObj,...
          'Units','pixels',...
          'Position',[330 35 50 25],...         
          'Callback',@(h,evt) obj.setPBandFromGUI);
        set(PBandUI,'Units','normalized');
        setappdata(guiObj,'PBandUI',PBandUI);        
        
        RippleText = uicontrol(...
          'Style','text',...
          'Parent',guiObj,...
          'Units','pixels',...
          'HorizontalAlignment','left',...
          'String','Passband Ripple:',...
          'Position',[170 5 150 20]);
        set(RippleText,'Units','normalized');
        setappdata(guiObj,'RippleText',RippleText);
                
        RippleUI = uicontrol(...
          'Style','edit',...
          'Parent',guiObj,...
          'Units','pixels',...
          'Position',[330 5 50 25],...         
          'Callback',@(h,evt) obj.setRippleFromGUI);
        set(RippleUI,'Units','normalized');
        setappdata(guiObj,'RippleUI',RippleUI);   
                                   
    end        
    
  end
  
  %% Protected Methods
  methods (Access=protected)

    function setSampleRateFromGUI(obj)
      obj.SampleRate = str2double(...
        get(getappdata(obj.gui,'sampleRateUI'),'String'));
    end
    
    function setOrderFromGUI(obj)
      obj.FilterOrder = str2double(...
        get(getappdata(obj.gui,'orderUI'),'String'));
    end
    
    function setPBandFromGUI(obj)
      obj.PassbandFrequency = str2double(...
        get(getappdata(obj.gui,'PBandUI'),'String'));
    end
    
    function setRippleFromGUI(obj)
      obj.PassbandRipple = str2double(...
        get(getappdata(obj.gui,'RippleUI'),'String'));
    end
    
    function syncGUI(obj)
      if obj.validGUI
        set(getappdata(obj.gui,'orderUI'),'String',num2str(obj.FilterOrder));
        set(getappdata(obj.gui,'PBandUI'),'String',num2str(obj.PassbandFrequency));
        set(getappdata(obj.gui,'RippleUI'),'String',num2str(obj.PassbandRipple));
      end
    end
    
    function buildFilter(obj)
      disp(['Rebuilding filter']);
      assert(~isempty(obj.PassbandFrequency),...
        'obj.PassbandFrequency must be set to build the filter');
      assert(~isempty(obj.PassbandRipple),...
        'obj.PassbandRipple must be set to build the filter');
      assert(~isempty(obj.SampleRate),...
        'obj.SampleRate must be set to build the filter');      
      
      obj.filter_pvt = designfilt('highpassiir','FilterOrder',obj.FilterOrder,...
        'PassbandFrequency',obj.PassbandFrequency,...
        'PassbandRipple',obj.PassbandRipple,...
        'SampleRate',obj.SampleRate);
      obj.rebuildFilter = false;
    end
    
    function setRebuildFlag(obj)
      obj.rebuildFilter = true;
      notify(obj,'outputUpdated');
    end;
    
    function setfilter(obj,val)
      error('cnlPLMod.EEG.filter.highpass does not allow direct setting of the filter');
    end
    
  end
  
  %% Static Protected Methods
  methods (Access=protected,Static=true)
    function out = checkInput(val)
      % Method to check and validate the input
      out = val;
    end
  end
end
   
      
      