classdef generalized < cnlPipelineModule
  
  properties (Dependent=true)
    filter
  end
  
  properties (Access=protected)
    filter_pvt
  end
  
  methods
    
    function obj = generalized(input,varargin)
      
      % Set up the basic object
      obj = obj@cnlPipelineModule;
      obj.inputTypes = {'cnlLabelledData'}; % Input types as a cell array here
      obj.outputType = 'cnlLabelledData';% Output type here
      obj.savesPrecomputedOutput = false;% Configure saved output here
      
      if nargin>0
        p = inputParser;
        p.addOptional('input',[],@(x) obj.isValidInputType(x));
        p.addOptional('filter',[],@(x) isa(x,'digitalFilter'));
        p.parse(input,varargin{:});
        
        % Set the input
        obj.input = input;
        obj.filter = p.Results.filter;
      
      end;
    end
    
    function out = get.filter(obj)
      out = obj.filter_pvt;
    end;
    
    function set.filter(obj,val)
      obj.setfilter(val);      
    end;
        
    function out = Execute(obj)
      % Code to run the module goes here
      
      in = obj.input;
      epochs = in.epochs;
      
      for i = 1:numel(epochs)
        epochs{i} = filtfilt(obj.filter_pvt,epochs{i}')';
      end
      out = cnlLabelledData(epochs,in.labels);
    end;
    
    function guiObj = makeGUI(obj)      
        guiObj = uipanel(...
          'Units','pixels',...
          'Title','EEG.filter.generalized',...
          'Position',[10 10 400 50]);
        
        editButton = uicontrol(...
          'Parent',guiObj,...
          'Style','pushbutton',...
          'String','Edit w/ designfilt',...
          'Units','Normalized',...
          'Position',[0.02 0.1 0.47 0.8],...
          'Callback',@(h,evt) obj.editFilter);
        setappdata(guiObj,'editButton',editButton);
        
        visButton = uicontrol(...
          'Parent',guiObj,...
          'Style','pushbutton',...
          'String','Visualize w/ fvtool',...
          'Units','normalized',...
          'Position',[0.51 0.1 0.47 0.8],...
          'Callback',@(h,evt) obj.visFilter);
        setappdata(guiObj,'visButton',visButton);                
    end
    
    
  end
  
  %% Protected Methods
  methods (Access=protected)
    function out = getPrecomputedOutput(obj)
      % This is an optional method that reimplements what's already the
      % default in cnlPipelineModule
      out = obj.Execute;
    end
    
    function editFilter(obj)
      if isa(obj.filter_pvt,'digitalFilter')
        designfilt(obj.filter_pvt);
      else
        obj.filter_pvt = designfilt;
      end
    end
    
    function visFilter(obj)
      fvtool(obj.filter_pvt);
    end;
    
    function setfilter(obj,val)
      % Function to be overloaded in subclasses in order to modify access
      % to obj.filter.
      if isempty(val), obj.filter_pvt = []; return; end;
      
      assert(isa(val,'digitalFilter'),...
        'obj.filter must be a digitalFilter object');
      if ~isequal(obj.filter_pvt,val)
        obj.filter_pvt = val;
        notify(obj,'outputUpdated');
      end;
    end
    
  end
  
  %% Static Protected Methods
  methods (Access=protected,Static=true)
    function out = checkInput(val)
      % Method to check and validate the input
      out = val;
    end
  end
end
   
      
      