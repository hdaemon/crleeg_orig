classdef difference < cnlPipelineModule
  % Returns the signal difference between two cnlLabelledData objects
  %
  % Written By: Damon Hyde
  % Part of the cnlEEG Project
  % 2016
  %
  
  
  properties
    toRemove;
  end
  
  methods
    
    %% Object Constructor
    function obj = difference(input,varargin)
      
      % Set up the basic object
      obj = obj@cnlPipelineModule;
      obj.inputTypes = {'cnlLabelledData'}; % Input types as a cell array here
      obj.outputType = 'cnlLabelledData';   % Output type here
      obj.savesPrecomputedOutput = false; % Configure saved output here
      
      p = obj.parseInputs(varargin{:});
      obj.toRemove = p.Results.toRemove;
            
      if nargin>0      
      % Set the input
      obj.input = input;      
      end;
    end
    
    %% Execute Method
    function out = Execute(obj)
      % Code to run the module goes here
      
      in = obj.input;
      if isempty(obj.toRemove), out = in; return; end;
      
      if isa(obj.toRemove,'cnlLabelledData'),
        remove = obj.toRemove.data;
      elseif isa(obj.toRemove,'cnlPipelineModule')
        remove = obj.toRemove.output.data;
      else
        remove = obj.toRemove;
      end
      
      assert(all(size(remove)==size(in.data)),...
        'Mismatch in data sizes');
      
      dataOut = in.data - remove;
      
      out = cnlLabelledData(dataOut,in.labels,...
                        'epochstart',in.epochs_start,...
                        'epochend',in.epochs_end);            
      
    end;
    
    function guiObj = makeGUI(obj)      
        guiObj = uipanel('Title','EEG.signal.difference',...
                         'Position',[10 10 400 20]);              
    end
    
  end
  
  %% Protected Methods
  methods (Access=protected)

  end
  
  %% Static Protected Methods
  methods (Access=protected,Static=true)
    function out = checkInput(val)
      % Method to check and validate the input
      out = val;
    end
    
    function p = parseInputs(varargin)
      p = inputParser;
      p.addOptional('toRemove',[]);
      p.parse(varargin{:});     
    end
    
  end
end
   
      
      