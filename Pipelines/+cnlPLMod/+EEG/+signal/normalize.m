classdef normalize < cnlPipelineModule
  
  properties
    % Put module specific properties and options here
    normalizeAcross = 'channel';
  end
  
  %% Public Methods
  methods
    
    function obj = normalize(input,varargin)
      
      % Set up the basic object
      obj = obj@cnlPipelineModule;
      obj.inputTypes = {'cnlLabelledData'}; % Input types as a cell array here
      obj.outputType = 'cnlLabelledData';   % Output type here
      obj.savesPrecomputedOutput = false;   % Configure saved output here
      
      if nargin>0
       p = inputParser;
       p.addOptional('input',[],@(x) obj.isValidInputType(x));
       % Other input parameters here
       p.parse(input,varargin{:});
       
       % Set the input
       obj.input = input;      
      end;
    end
    
    function out = Execute(obj)
      
      % Always run this first to prevent repeated execution of earlier
      % pipeline modules.
      
      in = obj.input;
      
      % Code to run the module goes here
      
            
    end;
    
  end
  
  %% Protected Methods
  methods (Access=protected)

  end
  
  %% Static Protected Methods
  methods (Access=protected,Static=true)
    function out = checkInput(val)
      % Method to check and validate the input
      out = val;
    end
  end
end
   
      
      