classdef buffer < cnlPipelineModule
  % EEG Pipeline Buffer Module
  %
  % Marginally useless module; This just buffers the output.
  %
  
  properties
    % Put module specific properties and options here
  end
  
  methods
    
    function obj = buffer(input,varargin)
      
      % Set up the basic object
      obj = obj@cnlPipelineModule;
      obj.inputTypes = {'cnlLabelledData'}; % Input types as a cell array here
      obj.outputType = 'cnlLabelledData';   % Output type here
      obj.savesPrecomputedOutput = true;   % Configure saved output here
      
      if nargin>0
       p = inputParser;
       p.addOptional('input',[],@(x) obj.isValidInputType(x));
       % Other input parameters here
       p.parse(input,varargin{:});
       
       % Set the input
       obj.input = input;      
      end;
    end
    
    function out = Execute(obj)
      % Buffer doesn't do anything             
      out = obj.input;            
    end;
    
       function guiObj = makeGUI(obj)      
        guiObj = uipanel(...          
          'Title','EEG.signal.buffer',...
          'Position',[10 10 400 50]);      
    end  
    
  end

  
  %% Static Protected Methods
  methods (Access=protected,Static=true)
    function out = checkInput(val)
      % Method to check and validate the input
      out = val;
    end
  end
end
   
      
      