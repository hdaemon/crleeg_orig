classdef temporalmean < cnlPipelineModule
  % Pipeline module to return temporal mean
  %
  % classdef temporalmean < cnlPipelineModule
  %
  % obj = cnlPLMod.EEG.signal.temporalmean(input,varargin)
  %
  % Inputs:
  % -------
  %   input : cnlLabelledData object
  % 
  % Optional Inputs:
  % ----------------
  %  byEpoch    : true/FALSE Flag to remove mean epoch by epoch
  %  fullOutput : TRUE/false Flag to make output data size equal to input.
  %
  % Written By: Damon Hyde
  % Part of the cnlEEG Project
  % 2016
  
  properties    
    byEpoch = true;
    fullOutput = true; 
  end
  
  methods
    
    function obj = temporalmean(input,varargin)
      
      % Set up the basic object
      obj = obj@cnlPipelineModule;
      obj.inputTypes = {'cnlLabelledData'}; % Input types as a cell array here
      obj.outputType = 'cnlLabelledData';   % Output type here
      obj.savesPrecomputedOutput = false;% Configure saved output here
      
      if nargin>0
        p = inputParser;
        p.addOptional('input',[],@(x) obj.isValidInputType(x));
        p.addParamValue('byEpoch',false,@(x) islogical(x));
        p.addParamValue('fullOutput',true,@(x) islogical(x));
        p.parse(input,varargin{:});
        
        % Set the input
        obj.input = p.Results.input;
        obj.byEpoch = p.Results.byEpoch;
        obj.fullOutput = p.Results.fullOutput;
      
      end;
    end
    
    function out = Execute(obj)
      % Code to run the module goes here
      
      in = obj.input;
      
      if obj.byEpoch
        % Temporal mean by epoch
        epochs = in.epochs;
        for i = 1:numel(epochs);
          currEpoch = epochs{i};
          tmpMean = mean(currEpoch,2);   
          if obj.fullOutput
            tmpMean = repmat(tmpMean,1,size(currEpoch,2));
          end
          epochMeans{i} = tmpMean;
        end
        out = cnlLabelledData(epochMeans,in.labels);
      else
        % Temporal mean across all epochs
        outData = mean(in.data,2);
        if obj.fullOutput
          % Duplicate mean across time, and retain input epoch information.
          outData = repmat(outData,1,size(in.data,2));
          out = cnlLabelledData(outData,in.labels,...
                              'epochstart',in.epochs_start,...
                              'epochend'  ,in.epochs_end);
        else
          % Epoch information is lost because time has been compressed to a
          % single sample.
          out = cnlLabelledData(outData,in.labels);
        end
        
      end
      
    end;

   function guiObj = makeGUI(obj)      
        guiObj = uipanel(...          
          'Title','EEG.signal.temporalmean',...
          'Position',[10 10 400 50]);      
    end      
    
  end
    
  %% Static Protected Methods
  methods (Access=protected,Static=true)
    function out = checkInput(val)
      % Method to check and validate the input
      out = val;
    end
  end
end
   
      
      
