classdef concatenate < cnlPipelineModule
  % Concatenate two or more cnlLabelledData objects
  %
  %
  % Written By: Damon Hyde
  % Last Edited: July 11, 2016
  % Part of the cnlEEG Project
  %
  
  properties
  end
  
  methods
    
    function obj = concatenate(varargin)
      
         % Set up the basic object
      obj = obj@cnlPipelineModule;
      obj.inputTypes = {'cnlLabelledData'}; % Input types as a cell array here
      obj.outputType = 'cnlLabelledData'; % Output type here
      obj.savesPrecomputedOutput = false; % Configure saved output here
      
      if nargin>0
      
      % Set the input
      obj.input = varargin{1};
      
      end;
    end
    
    function out = Execute(obj)
      % Code to run the module goes here
      
      input = obj.input;
      % This error checking is superfluous?
      if numel(input)>1                
        for i = 1:numel(input)
          assert(isa(input(i),'cnlLabelledData'),...
            'Cannot concatenated cnlLabelledData with other object types');
        end;
      else
        assert(isa(input,'cnlLabelledData'));
        out = input;
        return;
      end
      
      labels = input(1).labels;
      
      outEpochs = cell(0);
      for i = 1:numel(input)
        assert(isequal(labels,input(i).labels),...
          ['When vertically concatenating cnlLabelledData objects, all ' ...
          'sets of labels must be equal']);
        outEpochs = [outEpochs input(i).data_epoched];        
      end
      
      out = cnlLabelledData(outEpochs,input(1).labels);
    end;

   function guiObj = makeGUI(obj)      
        guiObj = uipanel(...          
          'Title','EEG.epochs.concatenate',...
          'Position',[10 10 400 50]);      
    end    
    
  end
  
  %% Protected Methods
  methods (Access=protected)
    function out = getPrecomputedOutput(obj)
      % This is an optional method that reimplements what's already the
      % default in cnlPipelineModule
      out = obj.Execute;
    end
  end
  
  %% Static Protected Methods
  methods (Access=protected,Static=true)
    function out = checkInput(val)
      % Method to check and validate the input
      out = val;
    end
  end
end