classdef cropToLength < cnlPipelineModule
  % Crop all epochs to a specific length
  %
  % classdef cropToLength < cnlPipeilneModule
  % 
  % Usage:
  %   foo = cnlPLMod.EEG.epochs.cropToLength(INPUT,LENGTH,varargin)
  %
  % Inputs
  %   INPUT
  %   LENGTH
  %   
  % Parameter-Value Pairs
  %   where : 'beginning' 'middle' or 'end
  %
  % Written By: Damon Hyde
  % Last Edited: Aug 16, 2016
  % Part of the cnlEEG Project
  %
  
  
  properties
    % Put module specific properties and options here
    intervalLength
    cropWhere
  end
  
  methods
    
    function obj = cropToLength(input,varargin)
      
      % Set up the basic object
      obj = obj@cnlPipelineModule;
      obj.inputTypes = {'cnlLabelledData'}; % Input types as a cell array here
      obj.outputType = 'cnlLabelledData';   % Output type here      
      
      if nargin>0
        
        p = inputParser;
        p.addOptional('input',[],@(x) obj.isValidInputType(x));
        p.addOptional('length',[],@(x) isnumeric(x)&&(numel(x)==1));
        p.addParamValue('where','beginning');
        parse(p,input,varargin{:});
        
        % Set the input
        obj.input = p.Results.input;
        obj.intervalLength = p.Results.length;
        obj.cropWhere = p.Results.where;
      
      end;
    end
    
    function out = Execute(obj)
      
      % Return empty set when intervallength is empty
      if isempty(obj.intervallength), out = []; return; end
      
      input = obj.input;
      
      epochs = obj.data_epoched;
      
      for i = 1:numel(epochs)
        epochlength = size(epochs{i},2);
        
        switch obj.cropWhere
          case 'beginning'
            interval = 1:obj.intervalLength;
          case 'middle'
            interval = 1:obj.intervalLength - ceil(obj.intervallength/2) + ceil(epochlength/2);
          case 'end'
            interval = 1:obj.intervalLength - obj.intervalLength + epochlength;
          otherwise
            error('Unknown crop option');
        end
        
        assert( (min(interval)>0) && (max(interval)<=epochlength),...
          'Requested interval is outside range of epoch');
        
        epochs{i} = epochs{i}(:,interval);
      end
      
      out = cnlLabelledData(epochs,input.labels);
    end;
    
    function set.intervalLength(obj,val)
      assert(isnumeric(val)&&(numel(val)==1),...
        'intervalLength must be a single numeric value');
      if ~isequal(obj.intervalLength,val)
      obj.intervalLength = val;
      notify(obj,'outputUpdated');
      end;
    end;
    
    function set.cropWhere(obj,val)
      VALID_CROP = {'beginning' 'middle' 'end'};
      tmpWhere = validatestring(val,VALID_CROP);
      if ~isequal(obj.cropWhere,tmpWhere)
        obj.cropWhere = tmpWhere;
        notify(obj,'outputUpdated');
      end;
    end;

   function guiObj = makeGUI(obj)      
        guiObj = uipanel(...          
          'Title','EEG.epochs.cropToLength',...
          'Position',[10 10 400 50]);      
    end    
    
  end
  
  %% Protected Methods
  methods (Access=protected)
    function out = getPrecomputedOutput(obj)
      % This is an optional method that reimplements what's already the
      % default in cnlPipelineModule
      out = obj.Execute;
    end
  end
  
  %% Static Protected Methods
  methods (Access=protected,Static=true)
    function out = checkInput(val)
      % Method to check and validate the input
      out = val;
    end
  end
end
   
      
      