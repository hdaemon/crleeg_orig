classdef average < cnlPipelineModule
  % cnlPipelineModule to average across epochs
  %
  % Only works if all epoch lengths are the same
  %
  %
  
  methods
    
    function obj = average(input,varargin)
      
      obj = obj@cnlPipelineModule;
      obj.inputTypes = {'cnlLabelledData'};
      obj.outputType = 'cnlLabelledData';
      
      if nargin>0
        obj.input = input;
      end;
    end
    
    function out = Execute(obj)
      
      input = obj.input;
      
      if ~isempty(input)
        assert(all(input.epochs_len==input.epochs_len(1)),...
          'All epochs must be the same length in order to average');
        
        tmp = input.epochs;
        data = cat(3,tmp{:});
        data = mean(data,3);
        
        out = cnlLabelledData(data,input.labels);
        out.description = [input.description '_avgepochs'];
        out.sampleRate = input.sampleRate;        
      else
        out = [];
      end;
    end
    
    function guiObj = makeGUI(obj)      
        guiObj = uipanel(...          
          'Title','EEG.epochs.average',...
          'Position',[10 10 400 50]);      
    end
    
  end
  
  methods (Access=protected,Static=true)
    function out = checkInput(val)
      out = val;
    end;
  end
  
  
end



