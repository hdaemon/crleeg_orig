classdef cropToInterval < cnlPipelineModule
  % Crop all epochs to a specific interval
  %
  % classdef cropToInterval < cnlPipelineModule
  %
  % Usage: 
  %   foo = mods_EEG.epochs.cropToInterval(INPUT,INTERVAL)
  %
  % Both inputs are optional.
  %
  % Written By: Damon Hyde
  % Last Edited: May 25, 2016
  % Part of the cnlEEG project
  %
  
  properties
    % Put module specific properties and options here
    interval
  end
  
  methods
    
    function obj = cropToInterval(varargin)
      
      % Set up the basic object
      obj = obj@cnlPipelineModule;
      obj.inputTypes = {'cnlLabelledData'};% Input types as a cell array here
      obj.outputType = 'cnlLabelledData';% Output type here
      
      
      if nargin>0
      
        p = inputParser;
        p.addOptional('input',[],@(x) obj.isValidInputType(x));
        p.addOptional('interval',[],@(x) isnumeric(x)&&(numel(x)==2));
        parse(p,varargin{:});
        
      % Set the input
      obj.input = p.Results.input;
      obj.interval = p.Results.interval;
      
      end;
    end
    
    function set.interval(obj,val)
      if isempty(val); obj.interval = []; return; end;
      assert(isnumeric(val)&&(numel(val)==2));      
      if ~isequal(obj.interval,val)
        obj.interval = val;
        notify(obj,'outputUpdated');
      end;
    end;
    
    function out = Execute(obj)
      input = obj.input;
      
      % Code to run the module goes here
      if ~isempty(input)&&~isempty(obj.interval)
        data = input.epochs;
        for i = 1:numel(data)
          try
            data{i} = data{i}(:,obj.interval(1):obj.interval(2));
          catch
            error('Interval out of range for one or more epochs');
          end;
        end;
        
        out = cnlLabelledData(data,input.labels);
        out.description = [out.description '_cropToInterval'];
        
      else
        out = [];
      end;
    end;

   function guiObj = makeGUI(obj)      
        guiObj = uipanel(...          
          'Title','EEG.epochs.cropToInterval',...
          'Position',[10 10 400 50]);      
    end    
    
  end
  
  %% Protected Methods
  methods (Access=protected)
    function out = getPrecomputedOutput(obj)
      % This is an optional method that reimplements what's already the
      % default in cnlPipelineModule
      out = obj.Execute;
    end
  end
  
  %% Static Protected Methods
  methods (Access=protected,Static=true)
    function out = checkInput(val)
      % Method to check and validate the input
      out = val;
    end
  end
end
   
      
      