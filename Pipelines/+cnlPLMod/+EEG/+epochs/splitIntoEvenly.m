classdef splitIntoEvenly < cnlPipelineModule
  % Split a cnlLabelledData object into epochs of even length
  %
  % Usage:
  %   foo = mods_EEG.epochs.SPLITINTOEVENLY(INPUT,LENGTH)
  %   OUTPUT = foo.output;
  %
  % Length is technically an optional parameter, but the output will be
  % empty unless a value is defined.
  %
  % Written By: Damon Hyde
  % Last Edited: May 24, 2016
  % Part of the cnlEEG Project
  %
  
  properties
    targetLength
    unevenSplit = 'error';
  end
  
  properties ( Constant )
    SPLIT_TYPES = {'error' 'drop' 'divide'};
  end
  
  methods
    
    function obj = splitIntoEvenly(input,varargin)
      
      obj = obj@cnlPipelineModule;
      obj.inputTypes = {'cnlLabelledData'};
      obj.outputType = 'cnlLabelledData';
      
      if nargin>0
        if obj.isValidInputType(input)
          obj.input = input;
          p = obj.parseInputs(varargin{:});
        else
          p = obj.parseInputs(input,varargin{:});
        end;
        
        obj.input = input;
        obj.targetLength = p.Results.targetLength;
        obj.unevenSplit = p.Results.unevensplit;
      end;
    end
    
    function set.targetLength(obj,val)
      assert(isnumeric(val)&&numel(val)==1,...
        'targetLength must be a singular numeric value');
      if ~isequal(obj.targetLength,val)
        obj.targetLength = val;
        notify(obj,'outputUpdated');
        obj.syncGUI;
      end;      
    end
    
    function set.unevenSplit(obj,val)
      val = lower(val);
      assert(ismember(val,{'error' 'drop' 'divide'}),...
        'obj.unevenSplit must be ''error'' ''drop'' or ''divide''');
      if ~isequal(obj.unevenSplit,val)
        obj.unevenSplit = val;
        notify(obj,'outputUpdated');
        obj.syncGUI;
      end;
    end;
    
    function out = Execute(obj)      
      input = obj.input;
      
      if ~isempty(input)        
        datain = input.epochs;
        
        cellOut = cell(0,1);
        % Loop across epochs
        for idxEpoch = 1:numel(datain)
          currEpoch = datain{idxEpoch};
          [nChan nSamp] = size(currEpoch);
          
          nCells = floor(nSamp/obj.targetLength);
          % Switch by division strategy
          switch obj.unevenSplit
            case {'error','drop'}
              % Either error out if the epoch can't be evenly divided, or
              % drop samples at the end.                            
              assert(isequal(obj.unevenSplit,'drop')||...
                     (nSamp==nCells*obj.targetLength),...
                     ['Data cannot be evenly split into epochs of length '...
                      num2str(obj.targetLength) ' without dropping samples']);
                                                      
                                 
              cellLengths = obj.targetLength*ones(1,nCells);             
              currEpoch = currEpoch(:,1:nCells*obj.targetLength);
              tmpCell = mat2cell(currEpoch,nChan,cellLengths);                                                
            case 'divide'
              % Increase target length so all samples are included
              approxLengths = obj.targetLength*ones(1,nCells);
              nSampRemain = nSamp - obj.targetLength*nCells;
              
              remSampDist = hist(1:nSampRemain,nCells);
              approxLengths = approxLengths+remSampDist;
              
              tmpCell = mat2cell(currEpoch,nChan,approxLengths);
            otherwise              
              error('Unknown division strategy. SHOULDN''T BE GETTING HERE');                            
          end;          
          cellOut(end+1:end+numel(tmpCell)) = tmpCell(:);
        end;
        labels = input.labels;
        out = cnlLabelledData(cellOut,labels);
      else
        out = [];
      end
    end
    
    function guiObj = makeGUI(obj)            
        guiObj = uipanel(...
          'Units','pixels',...
          'Title','splitIntoEvenly',...
          'Position',[10 10 400 50]);
        
        lengthText = uicontrol(...
          'Style','text',...
          'Parent',guiObj,...
          'Units','normalized',...
          'String','Target Length:',...
          'Position',[0.02 0.25 0.25 0.5]);
        setappdata(guiObj,'lengthText',lengthText);
                
        lengthUI = uicontrol(...
          'Style','edit',...
          'Parent',guiObj,...
          'Units','normalized',...
          'Position',[0.27 0.25 0.2 0.5],...         
          'Callback',@(h,evt) obj.setLengthFromGUI);
        setappdata(guiObj,'lengthUI',lengthUI);
        
        splitText = uicontrol(...
          'Style','text',...
          'Parent',guiObj,...
          'Units','normalized',...
          'String','Split Type:',...
          'Position',[0.5 0.25 0.25 0.5]);
        setappdata(guiObj,'splitText',splitText);        
                        
        splitUI = uicontrol(...
          'Style','popupmenu',...
          'Parent',guiObj,...
          'Units','normalized',...
          'String',obj.SPLIT_TYPES,...          
          'Position',[0.75 0.35 0.23 0.5],...
          'Callback',@(h,evt) obj.setSplitTypeFromGUI);
        setappdata(guiObj,'splitUI',splitUI);                                
    end
    
  end
  
  methods (Access=protected)
    function out = getPrecomputedOutput(obj)
      out = obj.Execute;
    end
    
    function setSplitTypeFromGUI(obj)      
      types = obj.SPLIT_TYPES;
      idx = get(getappdata(obj.gui,'splitUI'),'Value');
      obj.unevenSplit = types{idx};      
    end
    
    function setLengthFromGUI(obj)      
      obj.targetLength = str2double(get(getappdata(obj.gui,'lengthUI'),'String'));      
    end
    
    function syncGUI(obj)
      % If there's an open, valid, GUI, update the values in it from the
      % current object values.
      if obj.validGUI
        idx = find(cellfun(@(x) strcmpi(x,obj.unevenSplit),obj.SPLIT_TYPES));
        set(getappdata(obj.gui,'splitUI'),'Value',idx);
        set(getappdata(obj.gui,'lengthUI'),'String',num2str(obj.targetLength));
      end;
    end
    
  end
  
  methods(Access=protected, Static=true)
    function out = checkInput(val)
      out = val;
    end
    
    function p = parseInputs(varargin)
      p = inputParser;
      p.addOptional('targetLength',[],@(x) isnumeric(x)&&(numel(x)==1));
      p.addOptional('unevensplit','error',@(x) ischar(x));
      parse(p,varargin{:});
    end
    
  end
end

