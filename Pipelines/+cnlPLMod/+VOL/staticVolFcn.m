classdef staticVolFcn < cnlPipelineModule
  % Apply a non-modifing function to a volume
  %
  % Non-modifying functions are those that apply a function to each element
  % of the volume independently.
  %
  % These functions are stored in cnlPLMod.VOL.staticVolFcnTypes
  %
  %
  
  properties
    % Put module specific properties and options here
    functionType
  end
  
  
  %% Public Methods
  methods
    
    function obj = staticVolFcn(input,varargin)
      
      % Set up the basic object
      obj = obj@cnlPipelineModule;
      obj.inputTypes = {'uitools.datatype.vol3D'}; % Input types as a cell array here
      obj.outputType = 'uitools.datatype.vol3D';   % Output type here
      obj.savesPrecomputedOutput = false;   % Configure saved output here
      
      if nargin>0
        p = inputParser;
        p.addOptional('input',[],@(x) obj.isValidInputType(x));
        p.addParamValue('functiontype','true',@(x) ischar(x));
        p.addParamValue('saveOutput',false,@(x) islogical(x));
        % Other input parameters here
        p.parse(input,varargin{:});
        
        % Set the input
        obj.input = input;
        obj.functionType = p.Results.functiontype;
      end;
    end
    
    function set.functionType(obj,val)
      dispType = cnlPLMod.VOL.staticVolFcnTypes.(val);
      
      if ~isequal(obj.functionType,val)
        % Only trigger the notification if the type is changing.
        obj.functionType = dispType;
        obj.needsRecompute = true;
        notify(obj,'outputUpdated');
        obj.syncGUI;
      end;
    end
    
    function out = Execute(obj)
      % Apply a set of functions
      
      % Always run this first to prevent repeated execution of earlier
      % pipeline modules.
      in = obj.input;
      
      % Code to run the module goes here
      if isempty(in), out = []; return; end;
      if isempty(obj.functionType), out = []; return; end;
      
      tmpOut = feval(obj.functionType.fPre,obj.input.volume);
      tmpOut = feval(obj.functionType.fPost,tmpOut);
      out = uitools.datatype.vol3D(tmpOut,...
        'orientation',in.orientation,...
        'name',in.name,'aspect',in.aspect);
    end;
    
    function guiObj = makeGUI(obj,varargin)              
       
        guiObj = uipanel(...
          'Units','pixels',...
          'Position',[10 10 400 50]);
        
        typeText = uicontrol('Style','text',...
          'Parent',guiObj,...
          'Units','normalized',...
          'String','DisplayType:',...
          'Position',[0.2 0.25 0.25 0.5]);
        setappdata(guiObj,'typeText',typeText);
        
        fcnNames = obj.getFcnNames;
        findIdx = find(cellfun(@(x) strcmpi(x,obj.functionType),fcnNames));
        
        typeUI = uicontrol(...
          'Style','popupmenu',...
          'Parent',guiObj,...
          'Units','normalized',...
          'String',obj.getFcnNames,...
          'Value',findIdx,...
          'Position',[0.5 0.35 0.48 0.5],...
          'Callback',@(h,evt) obj.setFcnTypeFromGUI);
        setappdata(guiObj,'typeUI',typeUI);
              
    end;
    
  end
  
  %% Protected Methods
  methods (Access=protected)
    function setFcnTypeFromGUI(obj)
      Names = obj.getFcnNames;
      idx = get(getappdata(obj.gui,'typeUI'),'Value');
      obj.functionType = Names{idx};
    end;
    
    function syncGUI(obj)
      if obj.validGUI
         fcnNames = obj.getFcnNames;
        findIdx = find(cellfun(@(x) strcmpi(x,obj.functionType),fcnNames));
        set(getappdata(obj.gui,'typeUI'),'Value',findIdx);
      end
    end
  end
  
  %% Static Protected Methods
  methods (Static=true)
    function names = getFcnNames
      types = enumeration('cnlPLMod.VOL.staticVolFcnTypes');
      names = {types.name};
    end
    
  end;
  
  methods(Access=protected, Static=true)
    
    function out = checkInput(val)
      % Method to check and validate the input
      out = val;
    end
  end
end




