classdef movingVolFcnGUI < uitools.baseobjs.gui
  
  properties
    functionTypeSelect
    windowDepthSelect
  end
  
  properties (Access=protected, Dependent = true)
    functionType
    windowDepth
  end
  
  properties (Access=protected)
    fcn
  end
  
  methods
    
    function obj = movingVolFcnGUI(fcnIn,varargin)
      
      p = inputParser;
      p.addRequired('fcnIn',@(x) isa(x,'cnlPLMod.VOL.movingVolFcn'));
      p.addParamValue('parent',[]);
      p.addParamValue('origin',[10 10 ],@(x) isvector(x)&&numel(x)==2);
      p.addParamValue('size',[400 50]);
      p.addParamValue('units','pixels');
      p.parse(fcnIn,varargin{:});
      
      obj = obj@uitools.baseobjs.gui(...
        'Parent',p.Results.parent,...
        'units',p.Results.units,...
        'origin',p.Results.origin,...
        'size',p.Results.size,...
        'title','Moving Volume Function');
      
      obj.fcn = fcnIn;
      
      obj.functionTypeSelect = uicontrol(...
        'Style','popupmenu',...
        'Parent',obj.panel,...
        'Units','normalized',...
        'Position',[0.02 0.35 0.5 0.5],...
        'String',{'NONE','MAX','MIN','SUM','MEAN'},...
        'Callback',@(h,evt) obj.updateVizType);      
      
      obj.windowDepthSelect = uicontrol(...
        'Style','edit',...
        'Parent',obj.panel,...
        'Units','normalized',...
        'Position',[0.65 0.25 0.33 0.5],...
        'String',num2str(obj.fcn.windowDepth),...
        'Callback',@(h,evt) obj.updateVizType,...
        'Visible','on');            
      
%       obj.vizWindow = uicontrol(...
%         'Style','popupmenu',...
%         'Parent',obj.tabs(1),...
%         'Units','normalized',...
%         'Position',[0.32 0.11 0.3 0.05],...
%         'String',{'ALL','GAUSS','SQUARE'},...
%         'Callback',@(h,evt) obj.updateVizType,...
%         'Visible','off');
      
    end
    
    function updateVizType(obj)
      string = get(obj.functionTypeSelect,'String');
      idx = get(obj.functionTypeSelect,'Value');      
      obj.fcn.functionType = string{idx};
      
      depth = get(obj.windowDepthSelect,'String');
      obj.fcn.windowDepth = str2num(depth);
    end
    
    function out = get.functionType(obj)
      if ~isempty(obj.fcn)
        out = obj.fcn.functionType;
      end
    end
    
    function out = get.windowDepth(obj)
      if ~isempty(obj.fcn)
        out = obj.fcn.windowDepth;
      end
    end
  end
end