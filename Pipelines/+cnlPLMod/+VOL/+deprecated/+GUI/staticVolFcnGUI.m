classdef staticVolFcnGUI < uitools.baseobjs.gui
  
  properties
    functionType
  end
  
  properties (Access=protected)
    fcn
    names
    typeText
    typeUI
  end
    
  methods
    
    function obj = staticVolFcnGUI(fcnIn,varargin)
      
      p = inputParser;
      p.addRequired('fcnIn',@(x) isa(x,'cnlPLMod.VOL.staticVolFcn'));
      p.addParamValue('parent',[]);
      p.addParamValue('origin',[10 10 ],@(x) isvector(x)&&numel(x)==2);
      p.addParamValue('size',[400 50]);
      p.addParamValue('units','pixels');      
      p.parse(fcnIn,varargin{:});
      
      obj = obj@uitools.baseobjs.gui(...
        'Parent',p.Results.parent,...
        'units',p.Results.units,...
        'origin',p.Results.origin,...
        'size',p.Results.size,...
        'title','Voxelwise Volume Function');
      
      obj.fcn = fcnIn;
            
      obj.typeText = uicontrol('Style','text',....
        'Parent',obj.panel,...
        'Units','normalized',...
        'String','DisplayType:',...
        'Position',[0.2 0.25 0.25 0.5]);        
      obj.typeUI = uicontrol('Style','popupmenu',...
        'Parent',obj.panel,...
        'Units','normalized',...
        'String',obj.names,...
        'Position',[0.5 0.35 0.48 0.5],...
        'Callback',@(h,evt) obj.updateFunctionType);      
      
    end
    
    function out = get.names(obj)
      dispType = enumeration('cnlPLMod.VOL.staticVolFcnTypes');
      out = {dispType.name};
    end;
    
  end
  
  methods (Access=protected)
    
    function updateFunctionType(obj)
      Names = obj.names;
      idx = get(obj.typeUI,'Value');
      obj.fcn.functionType = Names{idx};
    end
    
  end
    
end
    