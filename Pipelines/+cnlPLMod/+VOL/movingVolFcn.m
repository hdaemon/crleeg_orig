classdef movingVolFcn < cnlPipelineModule
  % Apply Matlab's mov*** functions to vol3D objects
  %
  % Non-modifying functions are those that apply a function to each element
  % of the volume independently.
  %
  % These functions are stored in cnlPLMod.VOL.convVolFcnTypes
  %
  %
  
  properties
    % Put module specific properties and options here
    functionType
    windowDepth
    axis
  end  
  
  properties (Access=protected)
    storedVols
  end
  
  %% Public Methods
  methods
    
    function obj = movingVolFcn(input,varargin)
      
      % Set up the basic object
      obj = obj@cnlPipelineModule;
      obj.inputTypes = {'uitools.datatype.vol3D'}; % Input types as a cell array here
      obj.outputType = 'uitools.datatype.vol3D';   % Output type here
      obj.savesPrecomputedOutput = false;   % Configure saved output here
      
      if nargin>0
        p = inputParser;
        p.addOptional('input',[],@(x) obj.isValidInputType(x));
        p.addParamValue('axis',1);
        p.addParamValue('windowDepth',10);
        p.addParamValue('functiontype','none',@(x) ischar(x));
        p.addParamValue('saveOutput',false,@(x) islogical(x));
        % Other input parameters here
        p.parse(input,varargin{:});
        
        % Set the input
        obj.input = input;
        obj.functionType = p.Results.functiontype;
        obj.axis = p.Results.axis;
        obj.windowDepth = p.Results.windowDepth;
      end;
    end
    
    function set.functionType(obj,val)
      assert(ismember(lower(val),{'none' 'min' 'max' 'sum' 'mean'}))
      
      if ~isequal(obj.functionType,lower(val))
        % Only trigger the notification if the type is changing.
        obj.functionType = lower(val);
        obj.needsRecompute = true;
        notify(obj,'outputUpdated');
      end;
    end
    
    function set.axis(obj,val)
      assert(ismember(val,[1 2 3]),'Invalid axis label');
      if ~isequal(obj.axis,val)
        obj.axis = val;
        % if ~isequal(obj.functionType,'none')
        %   % Never trigger an update if using the 'none' function type.
        % obj.needsRecompute = true;
        % notify(obj,'outputUpdated');
        % end;
      end;
    end
    
    function set.windowDepth(obj,val)
      assert(isnumeric(val)&&isscalar(val),...
        'obj.windowDepth must be numeric and scalar');
      if ~isequal(obj.windowDepth,val)
        obj.windowDepth = val;
        obj.needsRecompute = true;
        notify(obj,'outputUpdated');
      end
    end
    
    function guiObj = makeGUI(obj)              
        guiObj = uipanel(...
          'Units','pixels',...
          'Position',[10 10 400 50]);
        
        fcnNames = {'NONE','MAX','MIN','SUM','MEAN'};
        findIdx = find(cellfun(@(x) strcmpi(x,obj.functionType),fcnNames));
        functionTypeSelect = uicontrol(...
          'Style','popupmenu',...
          'Parent',guiObj,...
          'Units','normalized',...
          'Position',[0.02 0.35 0.5 0.5],...
          'String',fcnNames,...
          'Value',findIdx,...
          'Callback',@(h,evt) obj.updateVizTypeFromGUI);
                                        
        setappdata(guiObj,'functionTypeSelect',functionTypeSelect);
        
        windowDepthSelect = uicontrol(...
          'Style','edit',...
          'Parent',guiObj,...
          'Units','normalized',...
          'Position',[0.65 0.25 0.33 0.5],...
          'String',num2str(obj.windowDepth),...
          'Callback',@(h,evt) obj.updateVizTypeFromGUI,...
          'Visible','on');
        setappdata(guiObj,'windowDepthSelect',windowDepthSelect);               
    end;
    
    function out = Execute(obj)
      % Apply a set of functions
      
      % Always run this first to prevent repeated execution of earlier
      % pipeline modules.
      in = obj.input;
      
      % Code to run the module goes here
      if isempty(in), out = []; return; end;
      if isempty(obj.functionType), out = []; return; end;
      
      if isequal(lower(obj.functionType),'none')
        out = in; return;
      end;
      
      obj.storedVols = [];
      switch lower(obj.functionType)
        case 'none'
          % Don't do anything to the input
          out = in;
          return;
        case 'max'
          for i = 1:3
            outV = movmax(in.volume,obj.windowDepth,i);
            out(i) = uitools.datatype.vol3D(outV,'orientation',in.orientation,...
              'name',in.name,'aspect',in.aspect);
          end;
        case 'min'
          for i = 1:3
            outV = movmin(in.volume,obj.windowDepth,i);
            out(i) = uitools.datatype.vol3D(outV,'orientation',in.orientation,...
              'name',in.name,'aspect',in.aspect);
          end;
        case 'sum'
          for i = 1:3
            outV = movsum(in.volume,obj.windowDepth,i);
            lo = min(outV(:)); hi = max(outV(:));
            out(i) = uitools.datatype.vol3D(outV,'orientation',in.orientation,...
              'name',in.name,'aspect',in.aspect);
          end;
          for i = 1:3
            out(i).overrideRange([min(lo(:)) max(hi(:))]);
          end;
        case 'mean'
          for i = 1:3
            outV = movmean(in.volume,obj.windowDepth,i);
            lo = min(outV(:)); hi = max(outV(:));
            out(i) = uitools.datatype.vol3D(outV,'orientation',in.orientation,...
              'name',in.name,'aspect',in.aspect);
          end;
          for i = 1:3
            out(i).overrideRange([min(lo(:)) max(hi(:))]);
          end;
      end
      
    end;
    
  end
  
  
  
  %% Protected Methods
  methods (Access=protected)
    function out = getPrecomputedOutput(obj)
      if isequal(obj.functionType,'none')
        out = obj.precomputedOutput;
      else
        out = obj.precomputedOutput(obj.axis);
      end;
    end;
    
    function updateVizTypeFromGUI(obj)
      assert(ishghandle(obj.gui),...
        'Calling the GUI callback without the gui enabled!');
      fTypeSelect = getappdata(obj.gui,'functionTypeSelect');
      winDepthSelect = getappdata(obj.gui,'windowDepthSelect');
      
      string = get(fTypeSelect,'String');
      idx = get(fTypeSelect,'Value');
      obj.functionType = string{idx};
      
      depth = get(winDepthSelect,'String');
      obj.windowDepth = str2double(depth);
      
    end
  end
  
  %% Static Protected Methods
  methods (Access=protected,Static=true)
    function out = checkInput(val)
      % Method to check and validate the input
      out = val;
    end
  end
end




