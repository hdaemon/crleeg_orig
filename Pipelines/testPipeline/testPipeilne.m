%%
test = cnlLabelledData(randn(5,100)',{'A' 'B' 'C' 'D' 'E'});
testEpoch = cnlLabelledData({randn(10,5) randn(10,5) randn(10,5)},...
                            {'A' 'B' 'C' 'D' 'E'});
rr = mods_EEG.rereference;
rem = mods_EEG.removeElec;
remmean = mods_EEG.removeTemporalMeanByEpoch;
