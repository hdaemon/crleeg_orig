classdef testModule_Add < cnlPipelineModule
  
  properties
    addVal = 0;
  end;
  
  
  methods
    
    function obj = testModule_Add(input,addVal)
      obj = obj@cnlPipelineModule;
      obj.inputTypes = {'double'};
      obj.outputType = 'double';
      
      obj.addVal = addVal;
      
      obj.savesPrecomputedOutput = false;
      
      obj.input = input;     
    end;
    
    function out = Execute(obj)
      out = obj.input + obj.addVal; 
    end
  
  end
  
  methods (Access=protected)
    function out = getPrecomputedOutput(obj)
      disp(['Running this one']);
      out = Execute(obj);
    end;     
  end
  
  methods (Static=true)       
    function out = checkInput(val)
      out = val;
    end;
    
  end
  
end

        
      